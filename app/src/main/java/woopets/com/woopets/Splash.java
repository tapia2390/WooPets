package woopets.com.woopets;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import woopets.com.woopets.Comandos.ComandoCategorias;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Oferente.LoguinOferente;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.cliente.ClienteRegistroExitoso;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.CmdParams;
import woopets.com.woopets.cliente.Comandos.CmdPayU;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCliente;
import woopets.com.woopets.cliente.Comandos.ComandoCliente.OnComandoClienteChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoCompras;
import woopets.com.woopets.cliente.Comandos.ComandoCompras.OnComandoComprasListener;
import woopets.com.woopets.cliente.Comandos.ComandoOferentes;
import woopets.com.woopets.cliente.Comandos.ComandoOferentes.OnComandoOferentesChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoRegistroCliente;
import woopets.com.woopets.cliente.Comandos.ComandoRegistroCliente.OnComandoRegistroClienteChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTipos;
import woopets.com.woopets.cliente.Comandos.ComandoTipos.OnComandoTiposChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTips;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;

import static com.facebook.login.widget.ProfilePictureView.TAG;

public class Splash extends Activity implements OnComandoTiposChangeListener, OnComandoRegistroClienteChangeListener, ComandoCategorias.OnComandoCategoriasChangeListener{

    final Context context = this;
    TextView textView4;
    PackageInfo pInfo;
    ComandoCategorias comandoCategorias;
//s
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    ComandoRegistroCliente comandoRegistrarte ;
    ModelCliente modelc = ModelCliente.getInstance();

    Modelo modelo = Modelo.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


        new ComandoTips().getPolitica();
        CmdParams.getDepartamentos();

        comandoCategorias = new ComandoCategorias(this);
        comandoRegistrarte = new ComandoRegistroCliente(this);
        //actividad splash


        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }



        textView4 = (TextView)findViewById(R.id.textView4);
        textView4.setText("Versión "+pInfo.versionName + " ("+ pInfo.versionCode + ")");
        modelo.versionName = ""+pInfo.versionName;
        if(estaConectado()){

            precargarDatos();

            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    final FirebaseUser user = firebaseAuth.getCurrentUser();

                    //mAuth.signOut();
                    if (user != null) {


                        new ComandoCategorias(new ComandoCategorias.OnComandoCategoriasChangeListener() {
                            @Override
                            public void cargoCategorias() {
                                Log.v("categorias","categorias");

                            }

                            @Override
                            public void cargoTipoUsuario(String tipo) {
                                if (tipo.equals("OFERENTE")) {

                                    Intent i = new Intent(getApplicationContext(), LoguinOferente.class);
                                    startActivity(i);
                                    finish();

                                } else if (tipo.equals("CLIENTE")) {
                                    ingresarComoCliente(user);

                                } else {

                                    mAuth.signOut();
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                    finish();

                                }

                            }

                            @Override
                            public void actualizoSistema() {
                                Log.v("system","system");
                            }
                        }).getTipoUsuario(user.getUid());  //Mover este comando de 'Categorias" a Usuario


                    } else {
                       Intent i = new Intent(getApplicationContext(), MainActivity.class);
                       startActivity(i);
                       overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
                       finish();
                    }
                }
            };

        }else{
            showAlertSinInternet();
        }
    }

    public void precargarDatos() {

        ComandoTipos cmTipos = new ComandoTipos(this);
        cmTipos.getRazasGato();
        cmTipos.getRazasPerro();
        CmdParams.getParams();
        comandoCategorias.getCategorias();
        comandoCategorias.getSytem();

    }

    private void preCargar() {

        String tipo = "";
        Mascota mascota = modelc.getMascotaActiva();
        if (mascota != null) {
            tipo = mascota.tipo;
        }


        new ComandoCalificacion(null).getMisCalificaciones(modelc.cliente.id);
        new ComandoTips().getTips();
        new ComandoTips().getPolitica();
        new ComandoAyuda(null).getInicales();
        CmdNoti.getInstance().getNotificaciones();
        CmdPayU.checkMovimientosPayU();


        Direccion direccion = modelc.getDireccionActiva();

        new ComandoCompras(new OnComandoComprasListener() {
            @Override
            public void cargoNuevaCompra() {

            }

            @Override
            public void cargoModificacionCompra() {

            }
        }).getComprasAbiertas(modelc.cliente.id);

        new ComandoCompras(new OnComandoComprasListener() {
            @Override
            public void cargoNuevaCompra() {

            }

            @Override
            public void cargoModificacionCompra() {

            }
        }).getComprasCerradas(modelc.cliente.id);



        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {
                /*Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                startActivity(i);
                finish();*/
            }

            @Override
            public void cargoPublicacion() {
                ///Pilas aca no puede ir nada// o jamas se va a llamar
            }
        }).getLast6Destacados(tipo);

        if (direccion != null && direccion.ubicacion != null) {

            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {

                }

                @Override
                public void cargoPublicacion() {
                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                    startActivity(i);
                    finish();

                }
            }).getOferentesCercanos(direccion.ubicacion);

        } else {
            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {
                    ///Pilas aca no puede ir nada// o jamas se va a llamar
                }

                @Override
                public void cargoPublicacion() {
                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }
            }).getTodasPublicaciones(tipo);


        }
    }


    public void ingresarComoCliente(final FirebaseUser user ){

        modelc.cliente.id = user.getUid();
        String token = FirebaseInstanceId.getInstance().getToken();

        CmdNoti.getInstance().actualizarTokenDevice(token);


        String provider;

        if (user.getProviders().size() > 0) {
            provider =  user.getProviders().get(0);

            if (provider.equals("password")) {
                modelc.tipoLogeo = "normal";
            }else{
                modelc.tipoLogeo = "faceb";
            }
        } else {
            modelc.tipoLogeo = "anonimo";
        }


        if(modelc.tipoLogeo.equals("faceb")){

            comandoRegistrarte.enviarRegistro3(""+user.getDisplayName(), ""+user.getUid(), user.getEmail());
        }

        if(modelc.tipoLogeo.equals("normal")){
            comandoRegistrarte.enviarRegistro2(user.getUid());
        }

        if(modelc.tipoLogeo.equals("anonimo")){
            comandoRegistrarte.enviarRegistro2(user.getUid());
        }

        new ComandoOferentes(new OnComandoOferentesChangeListener() {
            @Override
            public void cargoMiniOferentes() {
                continuarCarga(user.getUid());
            }
        }).getMiniOferentes();




    }


    private void continuarCarga(String uid) {

        new ComandoCliente(new OnComandoClienteChangeListener() {
            @Override
            public void cargoCliente() {


                if (modelc.cliente.tieneDatosBasicos() || modelc.tipoLogeo.equals("anonimo")) {
                    preCargar();
                } else {
                    Intent i = new Intent(getApplicationContext(), ClienteRegistroExitoso.class);
                    startActivity(i);
                    finish();
                }

            }

            @Override
            public void clienteNoExiste() {

            }

        }).getCliente(uid);

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            Log.v("cerrar","cerrar");
        }
        return false;
    }

    //validacion conexion internet
    protected Boolean estaConectado(){
        if(conectadoWifi()){
            Log.v("wifi","Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        }else{
            if(conectadoRedMovil()){
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            }else{
                showAlertSinInternet();
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }


    //validacion wifi
    protected Boolean conectadoWifi(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }



    protected Boolean conectadoRedMovil(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }



    public void showAlertSinInternet(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle(""+getString(R.string.sinInternet));

        // set dialog message
        alertDialogBuilder
                .setMessage(""+getString(R.string.sinInternetDescripcion))
                .setCancelable(false)
                .setPositiveButton("Reintentar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        Intent inte  = new Intent(getBaseContext(),Splash.class);
                        startActivity(inte);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public void opcionesClienteOferente(){

        //Se hizo ajuste para que ahora si ya esta logeado pase directamnete al home, y que ya no muestre el login.

        /* Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
        finish();*/
    }

    @Override
    public void cargoCategorias() {
        Log.v("categorias","categorias" + modelo.listaCategorias.size());
        opcionesClienteOferente();
    }

    @Override
    public void cargoTipoUsuario(String tipo) {

    }

    @Override
    public void actualizoSistema() {
        Log.v("system","system");
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mAuth==null || mAuthListener ==null){
            return;
        }else{
            mAuth.addAuthStateListener(mAuthListener);
        }


    }

    @Override
    public void onStop() {
        super.onStop();

        if(mAuth==null || mAuthListener ==null){
            return;
        }else{
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void informacionRegistroCliente() {


    }

    @Override
    public void errorInformacionRegistroCliente() {


    }


}
