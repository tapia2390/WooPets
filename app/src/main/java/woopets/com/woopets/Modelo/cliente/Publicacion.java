package woopets.com.woopets.Modelo.cliente;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion.OnComandoCalificacionChangeListener;

/**
 * Created by andres on 10/22/17.
 */

public class Publicacion implements  Comparable<Publicacion> {


    public String id = null;
    public boolean activo = true;
    public String categoria = "";
    public String subCategoria = null;
    public String descripcion = "";
    public boolean destacado = false;
    public String fechaCreacion = "";
    public ImageFire imagenDestacado;
    public ArrayList<ImageFire> fotosAlta = new ArrayList<>();
    public ArrayList<ImageFire> fotosMedia = new ArrayList<>();
    public ArrayList<ImageFire> fotosBaja = new ArrayList<>();

    public ArrayList<Calificacion> calificaciones = null;
    public long duracion = 0;
    public String duracionMedida = "";
    public ArrayList<Horario> horarios = new ArrayList<>();
    public  String idOferente  = "";
    public  String nombre  = "";
    public String precio  = "";
    public boolean servicio = false;
    public boolean servicioEnDomicilio = false;
    public long stock  = 0;
    public String target  = "";
    public long timestamp;
    public boolean borrada = false;
    public  Oferente oferente = new Oferente();
    public float distancia = -1;



    public Publicacion(String idPublicacion){
        imagenDestacado = new ImageFire();
        imagenDestacado.nombre = "destacado";
        imagenDestacado.extension = ".jpg";
        imagenDestacado.folderStorage = "productos/" +  idPublicacion;
        id = idPublicacion;
    }



    public void addImageFireParaFoto(int pos){
        ImageFire foto  = new ImageFire();
        foto.nombre = "Foto" + pos;
        foto.extension = ".png";
        foto.folderStorage = "productos/" +  id;
        fotosBaja.add(foto);

        ImageFire fotoM  = new ImageFire();
        fotoM.nombre = "Foto" + pos + "_medio";
        fotoM.extension = ".png";
        fotoM.folderStorage = "productos/" +  id;
        fotosMedia.add(fotoM);

        ImageFire fotoA  = new ImageFire();
        fotoA.nombre = "Foto" + pos + "_alto";
        fotoA.extension = ".png";
        fotoA.folderStorage = "productos/" +  id;
        fotosAlta.add(fotoA);


    }


    public boolean esParaPerro(){
        return target.contains("Perro");
    }

    public boolean esParaGato(){
        return target.contains("Gato");
    }

    public boolean esParaAve(){
        return target.contains("Ave");
    }

    public boolean esParaPez(){
        return target.contains("Pez");
    }

    public boolean esParaRoedor(){
        return target.contains("Roedor");
    }

    public boolean esParaExotico(){
        return target.contains("Exótico");
    }


    public String getTipos(){

        String[] parts = target.split(",");

        if (parts.length == 0 || parts.length == 1 || parts.length == 2){
            return target;
        }

        if (parts.length > 2){
            return "Para varias mascotas";
        }


        return "";
    }




    public Double getCalificacion(OnComandoCalificacionChangeListener listener){

        if (calificaciones == null) {
            ComandoCalificacion cm = new ComandoCalificacion(listener);
            cm.getCalificaciones(id);
            return  0.0;
        }

        if (calificaciones.size() == 0 ) {
            return  0.0;
        }

        Double acum = 0.0;

        for (Calificacion cali:calificaciones){
            acum += cali.calificacion;
        }

        return acum/calificaciones.size();

    }




    public int porcentajeCalificaciones(int nota){

        float cont = 0;

        if (calificaciones == null){
            return 100;
        }


        for (Calificacion cali:calificaciones){
            if (cali.calificacion == nota) {
                cont++;
            }
        }
        if (calificaciones.size() == 0 ) {
            return 100;
        }

        return  (int)(100 - ((cont/(float)calificaciones.size()) * 100));

    }



    @Override
    public int compareTo(@NonNull Publicacion publicacion) {
        return (this.oferente.distancia < publicacion.oferente.distancia ? -1 :
                (this.oferente.distancia == publicacion.oferente.distancia ? 0 : 1));
    }


    public String horaDiaValidoParaServicio(String fecha, String hora){


       for (Horario hor:horarios){
           if (hor.diaEnHorario(fecha,hora)){
               if (hor.horaEnHorario(fecha,hora)){
                   return "";
               }
               else {
                   if (hor.sinJornadaContinua) {
                       return "Para el día " + hor.getDiaEnHorario(fecha, hora) + " no tenemos servicio a la hora agendada. ";
                   }
                   else {
                       return "Para el día " + hor.getDiaEnHorario(fecha, hora) + " no tenemos servicio a la hora agendada. Cerramos entre 12 y 2 pm ";

                   }
               }
           }
       }

       return "Los días " + horarios.get(0).getDiaEnHorario(fecha, hora) + " no se presta el servicio";     //Si llega aca es porque no es valido la hora o el dia

    }
}
