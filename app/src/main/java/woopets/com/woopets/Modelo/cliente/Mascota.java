package woopets.com.woopets.Modelo.cliente;

import java.util.ArrayList;
import java.util.Date;

import woopets.com.woopets.Modelo.ImageFire;
import woopets.com.woopets.cliente.Comandos.ComandoAlarmas;
import woopets.com.woopets.cliente.Comandos.ComandoCliente;

/**
 * Created by andres on 10/13/17.
 */

public class Mascota {

    public String id = "";
    public boolean activa = true;
    public Date fechaNacimiento = null;
    public String genero = "Chico";
    public String nombre = "";
    public String raza = "";
    public String tipo = "";
    public String uid = "";
    public ImageFire imageFire;

    public ArrayList<Alarma> alarmas = new ArrayList<>();




    public Mascota(String uid, String idMascota){
        this.uid = uid;
        imageFire = new ImageFire();
        imageFire.nombre = "FotoTuMascota";
        imageFire.extension = "";
        imageFire.folderStorage = "mascotas/" + uid + "/" + idMascota;

    }

    public String getId() {

        return id;
    }

    public void setId(String id){
        this.id = id;


    }

    public void eliminarAlarma(String uid, String idAlarma){

        new ComandoAlarmas().eliminarAlarma(uid,id, idAlarma);

        int index = 0;
        int pos=-1;
        for (Alarma alarma: alarmas){
            if (alarma.id.equals(idAlarma)) {
                pos = index;
            }
            index++;
        }

        alarmas.remove(pos);

    }


    public boolean tieneAlarmaActiva(){
        for (Alarma alarma: alarmas){
            if (alarma.activada) {
                return true;
            }
        }
        return false;
    }


    public int getMaxIdAlarmas(){
        int max = 0;
        for (Alarma alarma:alarmas){
            max = Math.max(max, alarma.idAlarmaManager);

        }
        return max;
    }


    public Alarma getAlarmaById(String id){
        for (Alarma alarma: alarmas){
            if (alarma.id.equals(id)) {
                return alarma;
            }
        }

        return null;
    }

}
