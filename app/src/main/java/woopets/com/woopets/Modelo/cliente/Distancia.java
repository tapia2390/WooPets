package woopets.com.woopets.Modelo.cliente;

import android.location.Location;

/**
 * Created by andres on 12/18/17.
 */

public class Distancia {

    public String idOferente = "";
    public float metros;


    public Distancia(String idOferente, float metros){
        this.idOferente = idOferente;
        this.metros = metros;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof Distancia){

            Distancia dis = (Distancia) obj;
            if (dis.idOferente.equals(idOferente)){
                return true;
            }
        }
        return false;

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.idOferente != null ? this.idOferente.hashCode() : 0);
        return hash;
    }

}
