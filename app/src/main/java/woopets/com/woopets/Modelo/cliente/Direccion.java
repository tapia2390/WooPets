package woopets.com.woopets.Modelo.cliente;

/**
 * Created by andres on 10/2/17.
 */

public class Direccion {

    public String id = "";
    public String direccion = "";
    public String nombre = "";
    public Boolean porDefecto = false;
    public Integer posicion = 0;
    public Ubicacion ubicacion = new Ubicacion();
    public String ciudad = "Bogotá";
    public String estado = "Cundinamarca";



    public boolean tieneUbicacion(){

        if (ubicacion.latitud != null && ubicacion.latitud != 0 &&
                ubicacion.longitud != null && ubicacion.longitud != 0 ){
            return true;
        }
        return false;

    }


}
