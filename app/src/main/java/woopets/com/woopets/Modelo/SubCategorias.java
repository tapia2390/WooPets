package woopets.com.woopets.Modelo;

import java.util.ArrayList;

/**
 * Created by tacto on 29/08/17.
 */

public class SubCategorias {

    String idNombre;
    boolean estado;


    public String getIdNombre() {
        return idNombre;
    }

    public void setIdNombre(String idNombre) {
        this.idNombre = idNombre;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}