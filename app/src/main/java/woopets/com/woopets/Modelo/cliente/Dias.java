package woopets.com.woopets.Modelo.cliente;

/**
 * Created by andres on 10/22/17.
 */

enum Dias {

    domingo,lunes,martes,miercoles,jueves,viernes,sabado,festivos;

    public String descripcionLarga(){
        switch (this) {
            case domingo:return "Domingo";
            case lunes:return "Lunes";
            case martes:return "Martes";
            case miercoles:return "Miércoles";
            case jueves:return "Jueves";
            case viernes:return "Viernes";
            case sabado:return "Sábado";
            case festivos:return "Festivos";

        }
        return "";

    }


    public String descripcionCorta(){
        switch (this) {
            case domingo:return "Dom";
            case lunes:return "Lun";
            case martes:return "Mar";
            case miercoles:return "Mié";
            case jueves:return "Jue";
            case viernes:return "Vie";
            case sabado:return "Sáb";
            case festivos:return "Fes";

        }
        return "";

    }

}
