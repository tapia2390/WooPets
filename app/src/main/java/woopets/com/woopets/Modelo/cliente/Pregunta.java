package woopets.com.woopets.Modelo.cliente;

import android.support.annotation.NonNull;

/**
 * Created by andres on 11/7/17.
 */

public class Pregunta implements  Comparable<Pregunta>{

    public String fechaPregunta = "";
    public String fechaRespuesta = "";
    public String idCliente = "";
    public String idOferente = "";
    public String idPublicacion = "";
    public String id = "";
    public String pregunta = "";
    public String respuesta = "";
    public long timestamp = 0;

    @Override
    public int compareTo(@NonNull Pregunta pregunta) {
        return (this.timestamp < pregunta.timestamp ? 1 :
                (this.timestamp == pregunta.timestamp ? 0 : -1));
    }
}
