package woopets.com.woopets.Modelo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.concurrent.RejectedExecutionException;

/**
 * Created by andres on 10/18/17.
 */

public class ImageFire {


    public String nombre = "";
    public String prefijo = "";
    public int versionFirebase = 1;
    public int versionLocal = 0;
    public String extension = "";
    public String folderStorage = "";
    protected Bitmap foto = null;
    public String estado = "READY";


    public interface OnImageFireChange {
        void cargoImagen(String tipo);

    }

    private ImageFire.OnImageFireChange mListener;

    public ImageFire(OnImageFireChange mListener){

        this.mListener = mListener;
    }

    public ImageFire(){

    }

    public  void setListener(OnImageFireChange mListener){
        this.mListener= mListener;
    }


    public String getIdImagen() {

        String idImg = prefijo + eliminarTildes(nombre).replaceAll("\\s","");
        return idImg;
    }


    public String getNombreImagen() {

        String nombreImagen = prefijo + eliminarTildes(nombre).replaceAll("\\s","");
        nombreImagen  = nombreImagen + extension;
        return nombreImagen;
    }

    public String getPathEnStorage(){
        return folderStorage + "/" + getNombreImagen();
    }


    public void setFoto(Bitmap foto) {
        this.foto= foto;
        //TODO: Salvar en el disco
    }

    public Bitmap getFoto(String identificador){

        //TODO:Asegurar que las versiones estan guaradas y cargadas

        if (estado.equals("READY")){
            estado = "DOWNLOAD";
        }else{
            return foto;
        }


        boolean yallamado = false;

        if (versionFirebase > versionLocal) {
            getFotoFromFirebase(identificador);
            yallamado = true;

        }

        if (foto != null) {
            return  foto;
        }

        //// TODO: 10/18/17 intentar cargar la imagen desde disco.


       return null;

    }



    private  void getFotoFromFirebase(final String identificador){

        FirebaseStorage storage = FirebaseStorage.getInstance(); //gs://ensaladerastaging.appspot.com
        StorageReference storageRef = storage.getReference();
        final String path = getPathEnStorage();
        StorageReference fotoRef = storageRef.child(path);

        final long ONE_MEGABYTE = 1024 * 1024;

        Log.i("FOTOFIREBASE PIDIENDO:",path);

        try {
            fotoRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    try {

                        Bitmap foto=  BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                        if(foto !=null){
                            setFoto(foto);
                            versionLocal = versionFirebase;
                            if (mListener == null){
                                estado = "READY";
                                Log.v("exception","Listener esta NULO!");
                            }else{
                                estado = "READY";
                                mListener.cargoImagen(identificador);
                                Log.i("FOTOFIREBASE:",path);
                            }


                        }
                    }catch (Exception e){
                        Log.v("exeption","exeption"+e.getMessage());
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                    Log.v("exeption","exeption"+exception.getMessage());
                }
            });

           }catch (RejectedExecutionException r) {

                Log.i("DOWONLOAD", "Paró de bajar: ");

           }





    }




    //eliminar tildes
    private static final String ORIGINAL
            = "ÁáÉéÍíÓóÚúÑñÜü";
    private static final String REPLACEMENT
            = "AaEeIiOoUuNnUu";
    public  String eliminarTildes(String str) {
        if (str == null) {
            return null;
        }
        char[] array = str.toCharArray();
        for (int index = 0; index < array.length; index++) {
            int pos = ORIGINAL.indexOf(array[index]);
            if (pos > -1) {
                array[index] = REPLACEMENT.charAt(pos);
            }
        }
        return new String(array);
    }



}
