package woopets.com.woopets.Modelo.cliente;

/**
 * Created by andres on 10/25/17.
 */

public class Calificacion {

    public String idCalificacion = "";
    public int calificacion = 0;
    public String comentario = "";
    public String fecha = "";
    public String idCliente = "";
    public String idOferente = "";
    public String idPublicacion = "";
    public String idCompra = "";
    public long timestamp = 0;

    public Calificacion(){

    }

}
