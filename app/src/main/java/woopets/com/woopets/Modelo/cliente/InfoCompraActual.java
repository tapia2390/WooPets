package woopets.com.woopets.Modelo.cliente;

import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/30/17.
 */

public class InfoCompraActual {

    public String idPublicacion = "";
    public int cantidad = 1;
    public String fecha = "";
    public String hora = "";
    public String metodo = "Efectivo";
    public String direccion = "";
    public String departamento = "";


    public InfoCompraActual() {

    }

    public void reset(){
        idPublicacion = "";
        cantidad = 1;
        fecha = "";
        hora = "";
        metodo = "Efectivo";
        direccion = "";
    }


    public int getValor(){
        ModelCliente modelc = ModelCliente.getInstance();
        Publicacion publi =  modelc.getPublicacionById(idPublicacion);

        return Integer.valueOf(publi.precio) * cantidad;

    }


}
