package woopets.com.woopets.Modelo.cliente;

/**
 * Created by andres on 11/16/17.
 */

public class Solicitud {

    public String id;
    public String enunciado;
    public String estado = "Abierta";
    public String fechaGeneracion = "";
    public String fechaCierre = "";
    public String respuesta = "";
    public String idCliente = "";
    public long timestamp = 0;
    public String numeroSeguimiento = "";
    public String tipoSolicitud;


    public String getTipoLargo(){
        if (tipoSolicitud.equals("P")){
            return "Pregunta";
        }

        if (tipoSolicitud.equals("Q")){
            return "Queja";
        }

        if (tipoSolicitud.equals("R")){
            return "Reclamo";
        }

        if (tipoSolicitud.equals("S")){
            return "Solicitud";
        }
        return  "";
    }



}
