package woopets.com.woopets.Modelo.cliente;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by andres on 11/26/17.
 */

public class Alarma {

    public static  int SEMANAL = 7;
    public static  int QUINCENAL = 14;
    public static int MENSUAL = 30;
    public static int BIMENSUAL = 60;
    public static int ANUAL = 365;


    public String id = "";
    public boolean activada;
    public String tipoRecordatorio = "";
    public String nombre = "";
    public String hora = "";
    public String frecuencia = "";
    public String fechaInicio = "";
    public String fechaFin = "";

    public int idAlarmaManager = 0;



    public Date getInicio(){

        String string = fechaInicio + " " + hora;
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        Date date = new Date();
        try {
            date = format.parse(string);
        }catch (ParseException p){

        }

        System.out.println(date);
        return date;

    }

    /*public Date getInicio(){

    }*/

}
