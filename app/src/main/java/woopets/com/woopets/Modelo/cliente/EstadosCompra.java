package woopets.com.woopets.Modelo.cliente;

/**
 * Created by andres on 11/6/17.
 */



public enum EstadosCompra {
    PENDIENTE("Pendiente"),
    CONFIRMADA("Confirmada"),
    ENTREGADA("Entregada"),
    RECHAZADA("Rechazada"),
    CERRADA("Cerrada"),
    ;

    private final String text;

    /**
     * @param text
     */
    private EstadosCompra(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }

}
