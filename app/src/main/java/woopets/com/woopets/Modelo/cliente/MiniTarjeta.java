package woopets.com.woopets.Modelo.cliente;

/**
 * Created by andres on 11/18/17.
 */

public class MiniTarjeta {

    public String token = "";
    public String lastFour = "";
    public int cuotas = 1;
    public boolean activo = false;
    public String id = "";
    public String pais = "";
    public String inicialesPais = "";
    public String franquicia = "";
    public String correo = "";
    public String direccion = "";
    public String ciudad = "";
    public String departamento = "";
    public String nombre = "";
    public String telefono = "";
    public String cedula = "";
    public String referenceCode = "";       //Al tokenizar se crea un id de firebase
    public String numero = "";
    public String securityCode = "";
    public String expirationDate = "";
    public String ip = "";
    public String  estado = "APPROVED";
    public String responseCode = "";


}
