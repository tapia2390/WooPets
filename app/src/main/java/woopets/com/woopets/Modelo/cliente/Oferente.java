package woopets.com.woopets.Modelo.cliente;

import java.util.ArrayList;

/**
 * Created by andres on 10/31/17.
 */

public class Oferente {

    public String id = "";
    public String direccion = "";
    public String razonSocial = "";
    public String telefono = "";
    public String celular = "";
    public String correo = "";
    public String web = "";
    public ArrayList<Horario> horarios = new ArrayList<>();
    public float distancia = -1;

    public Oferente() {



    }

}
