package woopets.com.woopets.Modelo;

import java.util.ArrayList;

/**
 * Created by tacto on 29/08/17.
 */

public class Categorias {

    String  idNombre = "";
    String nombre = "";
    String imagen = "";
    boolean servicio = false;
    int posicion = 0;
    public ArrayList<SubCategorias> subcategorias = new ArrayList<SubCategorias>();
    public String getIdNombre() {
        return idNombre;
    }

    public void setIdNombre(String idNombre) {
        this.idNombre = idNombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public boolean getServicio() {
        return servicio;
    }

    public void setServicio(boolean servicio) {
        this.servicio = servicio;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }
}
