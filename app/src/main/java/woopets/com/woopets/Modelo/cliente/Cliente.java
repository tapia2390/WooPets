package woopets.com.woopets.Modelo.cliente;

import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 9/29/17.
 */

public class Cliente {

    public String nombre = "";
    public String apellido = "";
    public String correo = "";
    public String id = "";
    public String cedula = "";
    public String celular = "";
    public String metodoPago = "Sin definir";
    public String tokenDevice = "";



    public boolean tieneDatosBasicos(){

        if ( nombre.equals("")  || apellido.equals("") ) {
            return false;
        }
        return true;

    }


    public Map getMapaToPagoInicial(MiniTarjeta mini){

        ModelCliente modelo = ModelCliente.getInstance();
        Direccion dir = modelo.getDireccionActiva();

        Map<String, Object> dirEnvio = new HashMap<>();
        dirEnvio.put("street1", dir.direccion);
        dirEnvio.put("city", dir.ciudad);
        dirEnvio.put("state", dir.estado);
        dirEnvio.put("phone", celular);


        Map<String, Object> dirCobro = new HashMap<>();
        dirCobro.put("street1", mini.direccion);
        dirCobro.put("city", mini.ciudad);
        dirCobro.put("state", mini.departamento);
        dirCobro.put("phone", mini.telefono);


        Map<String, Object> tarjeta = new HashMap<>();
        tarjeta.put("number", mini.numero);
        tarjeta.put("securityCode", mini.securityCode);
        tarjeta.put("expirationDate", mini.expirationDate);
        tarjeta.put("name", mini.nombre);
        tarjeta.put("INSTALLMENTS_NUMBER", mini.cuotas);
        tarjeta.put("paymentMethod", mini.franquicia);
        tarjeta.put("ipAddress", mini.ip);


        Map<String, Object> data = new HashMap<>();
        data.put("referenceCode", mini.referenceCode);
        data.put("fullName", mini.nombre );
        data.put("emailAddress", mini.correo );
        data.put("contactPhone", celular );
        data.put("dniNumber", mini.cedula );         //cedula
        data.put("tokenDevice", modelo.cliente.tokenDevice );
        data.put("dirEnvio", dirEnvio );
        data.put("dirCobro", dirCobro );
        data.put("tarjeta", tarjeta );

        return  data;


    }


    public Map getMapaToPagoToken(Compra compra, MiniTarjeta mini ){

        ModelCliente modelo = ModelCliente.getInstance();
        Direccion dir = modelo.getDireccionActiva();

        Map<String, Object> dirEnvio = new HashMap<>();
        dirEnvio.put("street1", dir.direccion);
        dirEnvio.put("city", dir.ciudad);
        dirEnvio.put("state", dir.estado);
        dirEnvio.put("phone", celular);


        Map<String, Object> dirCobro = new HashMap<>();
        dirCobro.put("street1", mini.direccion);
        dirCobro.put("city", mini.ciudad);
        dirCobro.put("state", mini.departamento);
        dirCobro.put("phone", mini.telefono);

        Map<String, Object> tarjeta = new HashMap<>();
        tarjeta.put("creditCardTokenId", mini.token);
        tarjeta.put("INSTALLMENTS_NUMBER", mini.cuotas);
        tarjeta.put("paymentMethod", mini.franquicia);
        tarjeta.put("ipAddress", mini.ip);

        Map<String, Object> producto = new HashMap<>();
        producto.put("valor", compra.valor);
        producto.put("cantidad", compra.cantidad);


        Map<String, Object> data = new HashMap<>();
        data.put("referenceCode", compra.id);
        data.put("fullName", mini.nombre );
        data.put("emailAddress", mini.correo );
        data.put("contactPhone", celular );
        data.put("dniNumber", mini.cedula );         //cedula
        data.put("dirEnvio", dirEnvio );
        data.put("dirCobro", dirCobro );
        data.put("tarjeta", tarjeta );
        data.put("producto", producto );


        return  data;


    }



    public Map getMapaToTokenizar(MiniTarjeta mini){

        ModelCliente modelo = ModelCliente.getInstance();

        Map<String, Object> data = new HashMap<>();
        data.put("name", mini.nombre );
        data.put("identificationNumber", mini.cedula );
        data.put("paymentMethod", mini.franquicia );
        data.put("dniNumber", mini.cedula );         //cedula
        data.put("number", mini.numero );
        data.put("expirationDate", mini.expirationDate );

        return  data;


    }





}


