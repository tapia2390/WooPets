package woopets.com.woopets.Modelo.cliente;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import woopets.com.woopets.Oferente.ModelOferente.Utility;

/**
 * Created by andres on 10/22/17.
 */

public class Horario {

    public String dias = "";

    public String horaCierre  = "";
    public String horaInicio  = "";
    public boolean sinJornadaContinua = true;


    public ArrayList<Dias> diasActivos = new ArrayList<>();
    public String nombreArbol = "";

    public Horario(){

    }

    public boolean diaEnHorario(String f, String hora){

        Date fecha = Utility.convertStringConHoraToDate(f + " " + hora);

        Locale spanishLocale=new Locale("es", "ES");
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE", spanishLocale); // the day of the week spelled out completely
        System.out.println(simpleDateformat.format(fecha));

        if (Pattern.compile(Pattern.quote(simpleDateformat.format(fecha)), Pattern.CASE_INSENSITIVE).matcher(dias).find()){
            return  true;
        }

        return false;

    }

    public boolean horaEnHorario(String f , String hora) {

        Date abre  = Utility.convertStringConHoraToDate(f + " " + horaInicio);
        Date cierra  = Utility.convertStringConHoraToDate(f + " " + horaCierre);
        Date propuesta  = Utility.convertStringConHoraToDate(f + " " + hora);

       if (propuesta.after(abre) && propuesta.before(cierra)) {
           return true;
       }

       if (propuesta.compareTo(abre) == 0){
           return true;
       }



       return  false;

    }




    public String getDiaEnHorario(String f, String hora){

        Date fecha = Utility.convertStringConHoraToDate(f + " " + hora);

        Locale spanishLocale=new Locale("es", "ES");
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE", spanishLocale); // the day of the week spelled out completely
        System.out.println(simpleDateformat.format(fecha));

        return  simpleDateformat.format(fecha);

    }


}
