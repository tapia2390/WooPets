package woopets.com.woopets;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.List;

import woopets.com.woopets.Comandos.ComandoCategorias;
import woopets.com.woopets.Comandos.ComandoCategorias.OnComandoCategoriasChangeListener;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.Ubicacion;
import woopets.com.woopets.Oferente.LoguinOferente;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.TapsHome;
import woopets.com.woopets.cliente.ClienteRegistro;
import woopets.com.woopets.cliente.ClienteRegistroBasicos;
import woopets.com.woopets.cliente.ClienteRegistroExitoso;
import woopets.com.woopets.cliente.Cliente_login;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.CmdParams;
import woopets.com.woopets.cliente.Comandos.CmdParams.OnSystemListener;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCliente;
import woopets.com.woopets.cliente.Comandos.ComandoCliente.OnComandoClienteChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoCompras;
import woopets.com.woopets.cliente.Comandos.ComandoCompras.OnComandoComprasListener;
import woopets.com.woopets.cliente.Comandos.ComandoOferentes;
import woopets.com.woopets.cliente.Comandos.ComandoOferentes.OnComandoOferentesChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoRegistroCliente;
import woopets.com.woopets.cliente.Comandos.ComandoRegistroCliente.OnComandoRegistroClienteChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTipos;
import woopets.com.woopets.cliente.Comandos.ComandoTipos.OnComandoTiposChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTips;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.menu.PoliticaUso;

import static com.facebook.login.widget.ProfilePictureView.TAG;

public class MainActivity extends Activity implements OnComandoTiposChangeListener, OnComandoCategoriasChangeListener, OnComandoRegistroClienteChangeListener {


    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    ModelCliente modelc = ModelCliente.getInstance();
    ComandoRegistroCliente comandoRegistrarte;
    private ProgressDialog progressDialog;
    Modelo modelo = Modelo.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        comandoRegistrarte = new ComandoRegistroCliente(this);
        progressDialog = new ProgressDialog(this);

        Button politica = findViewById(R.id.politica);
        politica.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), PoliticaUso.class);
                startActivity(i);
            }
        });


        if (estaConectado()) {

            precargarDatos();

            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    final FirebaseUser user = firebaseAuth.getCurrentUser();


                    if (user != null) {

                        progressDialog.setMessage("Validando la información, por favor espere...");
                        progressDialog.show();

                        new ComandoCategorias(new ComandoCategorias.OnComandoCategoriasChangeListener() {
                            @Override
                            public void cargoCategorias() {


                            }

                            @Override
                            public void cargoTipoUsuario(String tipo) {
                                if (tipo.equals("OFERENTE")) {

                                    progressDialog.dismiss();

                                } else if (tipo.equals("CLIENTE")) {
                                    ingresarComoCliente(user);
                                    progressDialog.dismiss();
                                } else {
                                    progressDialog.dismiss();
                                    mAuth.signOut();
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                    finish();

                                }

                            }

                            @Override
                            public void actualizoSistema() {

                            }
                        }).getTipoUsuario(user.getUid());  //Mover este comando de 'Categorias" a Usuario


                    } else {
                        // Users is signed out
                        Log.d(TAG, "onAuthStateChanged:signed_out" + user + "no logueado");
                        //Toast.makeText(getApplication(),"...."+user+"no logueado", Toast.LENGTH_SHORT).show();
                    }
                }
            };
        } else {
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            showAlertSinInternet();
            return;
        }


    }


    public void ingresarComoCliente(final FirebaseUser user) {

        CmdParams.getSytem(this, new OnSystemListener() {
            @Override
            public void estaActualizado() {
                seguirIngresoCliente(user);
            }

            @Override
            public void puedeActualizar() {
                String ms = "Disfruta de  nuestra última versión, por favor descárgala.";
                hacerDialogoCliente(ms,true, user);
            }

            @Override
            public void debeActualizar() {
                String ms = "Disfruta de  nuestra última versión, por favor descárgala.";
                hacerDialogoCliente(ms,false, user);
            }

        });

    }


    private void seguirIngresoCliente(final FirebaseUser user){

        modelc.cliente.id = user.getUid();
        String token = FirebaseInstanceId.getInstance().getToken();

        CmdNoti.getInstance().actualizarTokenDevice(token);

        String provider;

        if (user.getProviders().size() > 0) {
            provider = user.getProviders().get(0);

            if (provider.equals("password")) {
                modelc.tipoLogeo = "normal";
            } else {
                modelc.tipoLogeo = "faceb";
            }
        } else {
            modelc.tipoLogeo = "anonimo";
        }


        if (modelc.tipoLogeo.equals("faceb")) {
            comandoRegistrarte.enviarRegistro3("" + user.getDisplayName(), "" + user.getUid(), user.getEmail());
        }

        if (modelc.tipoLogeo.equals("normal")) {
            comandoRegistrarte.enviarRegistro2(user.getUid());
        }

        if (modelc.tipoLogeo.equals("anonimo")) {
            comandoRegistrarte.enviarRegistro2(user.getUid());
        }

        new ComandoOferentes(new OnComandoOferentesChangeListener() {
            @Override
            public void cargoMiniOferentes() {
                continuarCarga(user.getUid());
            }
        }).getMiniOferentes();


    }




    private void continuarCarga(String uid) {

        new ComandoCliente(new OnComandoClienteChangeListener() {
            @Override
            public void cargoCliente() {


                if (modelc.cliente.tieneDatosBasicos() || modelc.tipoLogeo.equals("anonimo")) {
                    preCargar();
                } else {
                    Intent i = new Intent(getApplicationContext(), ClienteRegistroExitoso.class);
                    startActivity(i);
                    finish();
                }

            }

            @Override
            public void clienteNoExiste() {

            }

        }).getCliente(uid);

    }



    private void preCargar() {

        String tipo = "";
        Mascota mascota = modelc.getMascotaActiva();
        if (mascota != null) {
            tipo = mascota.tipo;
        }


        new ComandoCalificacion(null).getMisCalificaciones(modelc.cliente.id);
        new ComandoTips().getTips();
        new ComandoTips().getPolitica();
        new ComandoAyuda(null).getInicales();
        CmdNoti.getInstance().getNotificaciones();


        Direccion direccion = modelc.getDireccionActiva();

        new ComandoCompras(new OnComandoComprasListener() {
            @Override
            public void cargoNuevaCompra() {

            }

            @Override
            public void cargoModificacionCompra() {

            }
        }).getComprasAbiertas(modelc.cliente.id);

        new ComandoCompras(new OnComandoComprasListener() {
            @Override
            public void cargoNuevaCompra() {

            }

            @Override
            public void cargoModificacionCompra() {

            }
        }).getComprasCerradas(modelc.cliente.id);


        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {
                /*Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                startActivity(i);
                finish();*/
            }

            @Override
            public void cargoPublicacion() {
                ///Pilas aca no puede ir nada// o jamas se va a llamar
            }
        }).getLast6Destacados(tipo);

        if (direccion != null && direccion.ubicacion != null) {

            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {

                }

                @Override
                public void cargoPublicacion() {
                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                    startActivity(i);
                    finish();

                }
            }).getOferentesCercanos(direccion.ubicacion);

        } else {
            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {
                    ///Pilas aca no puede ir nada// o jamas se va a llamar
                }

                @Override
                public void cargoPublicacion() {
                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }
            }).getTodasPublicaciones(tipo);


        }
    }


    public void ingresarComoOferente(FirebaseUser user) {


    }


    public void loguinoferente(View v) {
        validacionVerciones();

    }


    public void didTapRegistrarse(View v) {
        Intent i = new Intent(getApplicationContext(), ClienteRegistro.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
        finish();

    }


    public void didTapIngresar(View v) {
        Intent i = new Intent(getApplicationContext(), Cliente_login.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
        finish();
    }


    //validacion conexion internet
    protected Boolean estaConectado() {
        if (conectadoWifi()) {
            Log.v("wifi", "Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        } else {
            if (conectadoRedMovil()) {
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            } else {
                showAlertSinInternet();
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    protected Boolean conectadoWifi() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }


    public void showAlertSinInternet() {

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                MainActivity.this);

        // set title
        alertDialogBuilder.setTitle("Sin Internet");

        // set dialog message
        alertDialogBuilder
                .setMessage("Sin Conexión a Internet")
                .setCancelable(false)
                .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent inte = new Intent(getBaseContext(), ClienteRegistro.class);
                        startActivity(inte);
                    }
                });

        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void saltarRegistro(View v) {
       /* Intent i = new Intent(getApplicationContext(), HomeCliente.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
        finish();
       */
        signInAnonymously();
    }

    public void precargarDatos() {

        ComandoTipos cmTipos = new ComandoTipos(this);
        cmTipos.getRazasGato();
        cmTipos.getRazasPerro();
        CmdParams.getDepartamentos();


    }


    private void signInAnonymously() {

        progressDialog.setMessage("Cargado ...");
        progressDialog.show();
        // [START signin_anonymously]
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInAnonymously:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            registrarAnonimo(user);

                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // [START_EXCLUDE]
                        //hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END signin_anonymously]
    }


    public void registrarAnonimo(final FirebaseUser user) {

        String versionName = BuildConfig.VERSION_NAME;


        new ComandoRegistroCliente(new OnComandoRegistroClienteChangeListener() {
            @Override
            public void informacionRegistroCliente() {
                ///En teoria se llama onAuthStateChanged entonces por alla entra.
            }

            @Override
            public void errorInformacionRegistroCliente() {


            }
        }).registroInicialCliente("anonimo@mail.com", user.getUid(), versionName, true);


    }


    @Override
    public void onStart() {
        super.onStart();
        if (mAuth == null || mAuthListener == null) {
            return;
        } else {
            mAuth.addAuthStateListener(mAuthListener);
        }


    }

    @Override
    public void onStop() {
        super.onStop();


        if (mAuth == null || mAuthListener == null) {
            return;
        } else {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void cargoCategorias() {


    }

    @Override
    public void cargoTipoUsuario(String tipo) {


    }

    @Override
    public void actualizoSistema() {

    }

    @Override
    public void informacionRegistroCliente() {

    }

    @Override
    public void errorInformacionRegistroCliente() {

    }



    public void validacionVerciones() {
        if (modelo.existeActualizacion(this)) {

            String ms = "Disfruta de la última versión de WooPets, por favor descárgala.";

            if (modelo.sistema.getUpdateMandatorio()) {
                HacerDialogo(ms, false);

                return;
            } else {
                HacerDialogo(ms, true);
                return;
            }

        } else {
            Intent i = new Intent(getApplicationContext(), LoguinOferente.class);
            startActivity(i);
            finish();
        }
    }

    //alesrt validacion verciones
    private void HacerDialogo(String msj, final boolean cancel) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // asignar el titulo
        alertDialogBuilder.setTitle("Nueva Actualización");

        // asignar el mensaje
        alertDialogBuilder
                .setMessage(msj)
                .setCancelable(cancel)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        irAAppStore();

                    }
                });


       /* if (cancel) {

            alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });
        }*/

        // se crea el dialogo
        AlertDialog alertDialog = alertDialogBuilder.create();

        // sse muestra
        alertDialog.show();
    }

    private void hacerDialogoCliente(String msj, final boolean cancel, final FirebaseUser user) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // asignar el titulo
        alertDialogBuilder.setTitle("Nueva Actualización");

        // asignar el mensaje
        alertDialogBuilder
                .setMessage(msj)
                .setCancelable(cancel)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        irAAppStore();

                    }
                });


        if (cancel) {

            alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    seguirIngresoCliente(user);    // Aca continua la accion si no quiere actualizar (cliente)

                }
            });
        }

        // se crea el dialogo
        AlertDialog alertDialog = alertDialogBuilder.create();

        // sse muestra
        alertDialog.show();
    }



    private void irAAppStore() {

        String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            finish();
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            finish();
        }
    }
}
