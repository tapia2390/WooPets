package woopets.com.woopets.tpaga.Model;

/**
 * Created by tacto on 11/08/17.
 */

public class TransactionInfo {

 String authorizationCode;
    String status;

    public String getAuthorizationCode() {
        return authorizationCode;
    }


    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
