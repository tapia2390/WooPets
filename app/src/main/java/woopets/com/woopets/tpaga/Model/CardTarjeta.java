package woopets.com.woopets.tpaga.Model;

/**
 * Created by tacto on 9/08/17.
 */

public class CardTarjeta {

    String cardHolderName;
    String cvc;
    String expirationMonth;
    String expirationYear;
    String primaryAccountNumber;

    public static  CardTarjeta create(String cardHolderName, String cvc, String expirationMonth, String expirationYear, String primaryAccountNumber){
        CardTarjeta cardTarjeta  = new CardTarjeta();
        cardTarjeta.cardHolderName  =cardHolderName;
        cardTarjeta.cvc  =cvc;
        cardTarjeta.expirationMonth  =expirationMonth;
        cardTarjeta.expirationYear  =expirationYear;
        cardTarjeta.primaryAccountNumber  =primaryAccountNumber;
        return cardTarjeta;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }
    public String getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    public String getPrimaryAccountNumber() {
        return primaryAccountNumber;
    }

    public void setPrimaryAccountNumber(String primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }
}
