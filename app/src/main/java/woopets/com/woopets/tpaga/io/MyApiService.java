package woopets.com.woopets.tpaga.io;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import woopets.com.woopets.tpaga.Model.CardTarjeta;
import woopets.com.woopets.tpaga.Model.CustomerCard;
import woopets.com.woopets.tpaga.Model.CustomerCardResponse;
import woopets.com.woopets.tpaga.Model.CustomerCliente;
import woopets.com.woopets.tpaga.Model.CustomerResponse;
import woopets.com.woopets.tpaga.Model.TPPAgo;
import woopets.com.woopets.tpaga.Model.TPPAgoResponse;
import woopets.com.woopets.tpaga.Model.TarjetaResponse;


/**
 * Created by tacto on 8/08/17.
 */

public interface MyApiService {


    void onConnectionTimeout();

//anotaciones @POST GET etc..
    @POST("customer")
    Call<CustomerResponse> addCustomerPost(@Body CustomerCliente appInfo);

    @POST("tokenize/credit_card")
    Call<TarjetaResponse> addTarjetaPost(@Body CardTarjeta appInfo);

    @POST("customer/{customer_id}/credit_card_token")
    Call<CustomerCardResponse> addCustomerCard(@Path("customer_id") String customer_id, @Body CustomerCard appInfo);


    @POST("charge/credit_card")
    Call<TPPAgoResponse> generarPago(@Body TPPAgo appInfo);
}
