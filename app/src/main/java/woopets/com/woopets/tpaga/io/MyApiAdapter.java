package woopets.com.woopets.tpaga.io;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import woopets.com.woopets.BuildConfig;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.tpaga.Network.TPagaInterceptor;

/**
 * Created by tacto on 8/08/17.
 */

public class MyApiAdapter {


    private static  ModelCliente modelc = ModelCliente.getInstance();

    private static MyApiService API_SERVICE;// la interface

    private static final String TAG = "TpagaAPI";
    protected static final String SANDBOX_TPAGA_API_HOST = "" + modelc.tpaga.endPoint;
    public static final int SANDBOX = 0x3e8;
    public static final int PRODUCTION = 0x7d0;


    protected static final String tpagaPrivateApiKeym = ""+ modelc.tpaga.privateKey;

    public static MyApiService getApiService() { //utiliza patron singelton se utiliza una instancia

        // Creamos un interceptor y le indicamos el log level a usar
       // HttpLoggingInterceptor logging = new HttpLoggingInterceptor();//va a mostrar lo resultados de la peticion el codigo de respuetsa
        //logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Asociamos el interceptor a las peticiones
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel((BuildConfig.DEBUG) ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        httpClient.addInterceptor(logging);

        httpClient.addInterceptor(new TPagaInterceptor(tpagaPrivateApiKeym));

        //si api service es null se va instaciar de lo contrario va a devolver el objeto
        if (API_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(SANDBOX_TPAGA_API_HOST)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.connectTimeout(5, TimeUnit.MINUTES)
                            .writeTimeout(5, TimeUnit.MINUTES)
                            .readTimeout(5, TimeUnit.MINUTES).build()) // <-- usamos el log level
                    .build();
            API_SERVICE = retrofit.create(MyApiService.class);
        }

        return API_SERVICE;
    }

}
