package woopets.com.woopets.cliente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import woopets.com.woopets.Modelo.Categorias;
import woopets.com.woopets.Modelo.SubCategorias;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/14/17.
 */

public class ComandoTipos {



    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    public interface OnComandoTiposChangeListener {

        void cargoCategorias();
    }

    //interface del listener de la actividad interesada
    private OnComandoTiposChangeListener  mListener;


    public ComandoTipos(OnComandoTiposChangeListener mListener){

        this.mListener = mListener;
    }


    public void getRazasGato() {

        DatabaseReference ref = database.getReference("listados/razasGato/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelc.razasGato.clear();
                for (DataSnapshot raza : snap.getChildren()){
                  modelc.razasGato.add(raza.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void getRazasPerro() {

        DatabaseReference ref = database.getReference("listados/razasPerro/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelc.razasPerro.clear();
                for (DataSnapshot raza : snap.getChildren()){
                    modelc.razasPerro.add(raza.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void getCategorias(){

        DatabaseReference ref = database.getReference("listados/categorias/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                //modelo.listaCategorias.clear();
                for (DataSnapshot listcCategoria : snap.getChildren()){

                    boolean servicio =  (boolean) listcCategoria.child("servicio").getValue();
                    Categorias categorias = new Categorias();
                    categorias.setIdNombre(listcCategoria.getKey());
                    categorias.setImagen(listcCategoria.child("imagen").getValue().toString());
                    categorias.setNombre(listcCategoria.child("nombre").getValue().toString());

                    categorias.setServicio(servicio);

                    //nuevo arbol
                    DataSnapshot snapSubCategorias;
                    snapSubCategorias = (DataSnapshot) listcCategoria.child("subcategoria/");
                    for (DataSnapshot subcategoria : snapSubCategorias.getChildren()){

                        SubCategorias subCategorias = new SubCategorias();

                        boolean estado = (boolean) subcategoria.getValue();
                        if(estado){
                            subCategorias.setIdNombre(subcategoria.getKey());
                            subCategorias.setEstado(estado);
                            categorias.subcategorias.add(subCategorias);
                        }

                    }
                    //modelo.listaCategorias.add(categorias);
                    mListener.cargoCategorias();

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }



    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoTiposChangeListener sDummyCallbacks = new OnComandoTiposChangeListener()
    {
        @Override
        public void cargoCategorias()
        {}


    };
}
