package woopets.com.woopets.cliente;

import android.Manifest.permission;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.Ubicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCompras;
import woopets.com.woopets.cliente.Comandos.ComandoCompras.OnComandoComprasListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoRegistroCliente;
import woopets.com.woopets.cliente.Comandos.ComandoRegistroCliente.OnComandoRegistroClienteChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTips;

public class ClienteRegistroBasicos extends Activity implements OnComandoRegistroClienteChangeListener, GoogleApiClient.ConnectionCallbacks, com.google.android.gms.location.LocationListener, OnConnectionFailedListener {

    LinearLayout layoutDir1;
    LinearLayout layoutDir2;
    LinearLayout layoutDir3;
    LinearLayout layoutUbi1;
    LinearLayout layoutUbi2;
    LinearLayout layoutUbi3;
    ScrollView scroll;


    String dirBuscada = "UNO";

    Double lat1 = 0.0;
    Double lat2 = 0.0;
    Double lat3 = 0.0;
    Double lon1 = 0.0;
    Double lon2 = 0.0;
    Double lon3 = 0.0;


    boolean mostrandoDir2 = false;
    boolean mostrandoDir3 = false;

    ImageView btnMasMenos1;
    ImageView btnMasMenos2;
    ImageView btnMasMenos3;

    ImageView imgConDireccion, imgProveedor, imgIngreso;

    EditText dir1;
    EditText dir2;
    EditText dir3;
    EditText nombredir1;
    EditText nombredir2;
    EditText nombredir3;

    EditText nombre;
    EditText apellido;
    TextView correo, telefono;


    private FusedLocationProviderClient locCliente;
    LocationRequest locationRequest;
    FusedLocationProviderApi fusedLocationProviderApi;
    GoogleApiClient googleApiClient;

    ModelCliente modelc = ModelCliente.getInstance();
    Cliente cli = modelc.cliente;
    private boolean mLocationPermissionGranted = false;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private Location mLastKnownLocation;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cliente_registro_basicos);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        scroll = (ScrollView) this.findViewById(R.id.scroll);
        locCliente = LocationServices.getFusedLocationProviderClient(this);

        btnMasMenos1 = (ImageView) findViewById(R.id.imageButtonAdicinal1);
        btnMasMenos2 = (ImageView) findViewById(R.id.imageButtonAdicinal2);
        btnMasMenos3 = (ImageView) findViewById(R.id.imageButtonAdicinal3);

        imgIngreso = (ImageView) findViewById(R.id.imgIngreso);
        imgConDireccion = (ImageView) findViewById(R.id.imgConDireccion);
        imgProveedor = (ImageView) findViewById(R.id.imgProveedor);


        layoutDir2 = (LinearLayout) this.findViewById(R.id.layoutdir2);
        layoutDir2.setVisibility(LinearLayout.GONE);
        layoutUbi2 = (LinearLayout) this.findViewById(R.id.layoutubi2);
        layoutUbi2.setVisibility(LinearLayout.GONE);


        layoutDir3 = (LinearLayout) this.findViewById(R.id.layoutdir3);
        layoutDir3.setVisibility(LinearLayout.GONE);
        layoutUbi3 = (LinearLayout) this.findViewById(R.id.layoutubi3);
        layoutUbi3.setVisibility(LinearLayout.GONE);

        dir1 = (EditText) findViewById(R.id.dir1);
        dir2 = (EditText) findViewById(R.id.dir2);
        dir3 = (EditText) findViewById(R.id.dir3);

        nombredir1 = (EditText) findViewById(R.id.nombredir1);
        nombredir2 = (EditText) findViewById(R.id.nombredir2);
        nombredir3 = (EditText) findViewById(R.id.nombredir3);

        nombre = (EditText) findViewById(R.id.nombre);
        apellido = (EditText) findViewById(R.id.apellido);
        correo = (TextView) findViewById(R.id.correo);
        telefono = (TextView) findViewById(R.id.telefono);


        nombre.setText(cli.nombre);
        apellido.setText(cli.apellido);
        telefono.setText(cli.celular);


        getLocationPermission();

        refrescarDatos();


        getLocation();

    }


    public void refrescarDatos(){

        if (modelc.tipoLogeo.equals("faceb")) {
            imgIngreso.setImageResource(R.drawable.imgfbok);

        }

        if (modelc.getTarjetaActiva() != null) {
            imgProveedor.setImageResource(R.drawable.imgtarjetaok);
        }


        if (modelc.direcciones.size() > 0) {
            imgConDireccion.setImageResource(R.drawable.imgubicacionok);
            Direccion duno = modelc.direcciones.get(0);
            dir1.setText(duno.direccion);
            nombredir1.setText(duno.nombre);
        }

        if (modelc.direcciones.size() > 1) {
            Direccion ddos = modelc.direcciones.get(1);
            dir2.setText(ddos.direccion);
            nombredir2.setText(ddos.nombre);

            layoutDir2.setVisibility(LinearLayout.VISIBLE);
            layoutUbi2.setVisibility(LinearLayout.VISIBLE);
            mostrandoDir2 = true;
            scroll.fullScroll(View.FOCUS_DOWN);

        }

        if (modelc.direcciones.size() > 2) {
            Direccion dtres = modelc.direcciones.get(2);
            dir3.setText(dtres.direccion);
            nombredir3.setText(dtres.nombre);
            layoutDir3.setVisibility(LinearLayout.VISIBLE);
            layoutUbi3.setVisibility(LinearLayout.VISIBLE);
            mostrandoDir3 = true;
            scroll.fullScroll(View.FOCUS_DOWN);
        }


        if (modelc.cliente.correo.endsWith("nocompartiocorreoconFB") && (modelc.tipoLogeo.equals("faceb"))) {
            correo.setText("");
            correo.setVisibility(View.GONE);
        }else{
            correo.setText(modelc.cliente.correo);
        }


    }


    @Override
    protected void onStart() {
        super.onStart();
        getLocation();
    }



    public void didTapAgregarQuitar1(View v) {

        if (mostrandoDir2) {
            if (mostrandoDir3) {
                btnMasMenos1.setImageResource(R.drawable.btnmenos);
                btnMasMenos2.setImageResource(R.drawable.btnmas);

                //Aca toca subir los datos que tenia abajo al de arriba y en realidad quitamos el 3
                layoutDir3.setVisibility(LinearLayout.GONE);
                layoutUbi3.setVisibility(LinearLayout.GONE);
                mostrandoDir3 = false;
                dir1.setText(dir2.getText());
                dir2.setText(dir3.getText());
                dir3.setText("");

                nombredir1.setText(nombredir2.getText());
                nombredir2.setText(nombredir3.getText());
                nombredir3.setText("");

                lat1 = lat2;
                lat2 = lat3;
                lat3 = 0.0;

                lon1 = lon2;
                lon2 = lon3;
                lon3 = 0.0;


            } else {
                btnMasMenos1.setImageResource(R.drawable.btnmas);
                //No esta mostrando la 3 asi que fresco la quita
                layoutDir2.setVisibility(LinearLayout.GONE);
                layoutUbi2.setVisibility(LinearLayout.GONE);
                mostrandoDir2 = false;
                dir1.setText(dir2.getText());
                dir2.setText("");
                nombredir1.setText(nombredir2.getText());
                nombredir2.setText("");

                lat1 = lat2;
                lat2 = 0.0;

                lon1 = lon2;
                lon2 = 0.0;

            }
        } else {
            btnMasMenos1.setImageResource(R.drawable.btnmenos);
            layoutDir2.setVisibility(LinearLayout.VISIBLE);
            layoutUbi2.setVisibility(LinearLayout.VISIBLE);
            mostrandoDir2 = true;
            scroll.fullScroll(View.FOCUS_DOWN);
        }

    }


    public void didTapAgregarQuitar2(View v) {

        if (mostrandoDir3) {
            btnMasMenos2.setImageResource(R.drawable.btnmas);
            //Aca toca subir los datos que tenia abajo al de arriba y en realidad quitamos el 3
            layoutDir3.setVisibility(LinearLayout.GONE);
            layoutUbi3.setVisibility(LinearLayout.GONE);
            mostrandoDir3 = false;
            dir2.setText(dir3.getText());
            dir3.setText("");
            nombredir2.setText(nombredir3.getText());
            nombredir3.setText("");
            lat2 = lat3;
            lat3 = 0.0;

            lon2 = lon3;
            lon3 = 0.0;


        } else {
            btnMasMenos2.setImageResource(R.drawable.btnmenos);
            //el icono debe estar en + asi ponemos la 3
            layoutDir3.setVisibility(LinearLayout.VISIBLE);
            layoutUbi3.setVisibility(LinearLayout.VISIBLE);
            mostrandoDir3 = true;
            scroll.fullScroll(View.FOCUS_DOWN);
        }


    }

    public void didTapAgregarQuitar3(View v) {

        btnMasMenos2.setImageResource(R.drawable.btnmas);
        layoutDir3.setVisibility(LinearLayout.GONE);
        layoutUbi3.setVisibility(LinearLayout.GONE);
        mostrandoDir3 = false;
        dir3.setText("");
        lat3 = 0.0;
        lon3 = 0.0;

    }


    public void didTapOKContinuar(View v) {
        if (nombre.getText().toString().length() < 3) {
            nombre.setError("Debes incluir tu nombre");
            nombre.requestFocus();
            return;
        }

        if (apellido.getText().toString().length() < 3) {
            apellido.setError("Debes incluir tu apellido");
            apellido.requestFocus();
            return;
        }

        if (telefono.getText().toString().length() == 7 || telefono.getText().toString().length() == 10) {

        } else {
            telefono.setError("El télefono esta muy largo o muy corto");
            telefono.requestFocus();
            return;

        }



        if (!checkLocationOn()) {
            dir1.setError("Debes activar Ubicación (el gps)  en tu dispositivo.");
        }


        if (dir1.getText().toString().length() < 4) {
            dir1.setError("Debes incluir al menos una dirección");
            dir1.requestFocus();
            return;
        }

        if (nombredir1.getText().toString().length() < 2) {
            nombredir1.setError("Nombre del lugar demasiado corto o vacío");
            nombredir1.requestFocus();
            return;
        }

        if (mostrandoDir2) {
            if (dir2.getText().toString().length() < 4) {
                dir2.setError("Debes incluir la dirección");
                dir2.requestFocus();
                return;
            }

            if (nombredir2.getText().toString().length() < 2) {
                nombredir2.setError("Nombre del lugar demasiado corto o vacío");
                nombredir2.requestFocus();
                return;
            }
        }

        if (mostrandoDir3) {
            if (dir3.getText().toString().length() < 4) {
                dir3.setError("Debes incluir la dirección");
                dir3.requestFocus();
                return;
            }

            if (nombredir3.getText().toString().length() < 2) {
                nombredir3.setError("Nombre del lugar demasiado corto o vacío");
                nombredir3.requestFocus();
                return;
            }
        }


        updateDirecciones();


        ComandoRegistroCliente cmm = new ComandoRegistroCliente(this);
        cmm.actualizarDatosBasicos();


        if (modelc.compraActual.idPublicacion.equals("")) {

            if (modelc.publicaciones.size() == 0) {
                preCargar();
            }else {
                googleApiClient.disconnect();

               // Intent i = new Intent(getApplicationContext(), ClienteCrearTarjeta.class);
               // i.putExtra("ORIGEN", "REGISTRO");
               // startActivity(i);
               // finish();

                Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }


        } else {
            googleApiClient.disconnect();
            finish();
        }



    }


    public void updateDirecciones() {

        ModelCliente modelc = ModelCliente.getInstance();
        modelc.cliente.nombre = nombre.getText().toString();
        modelc.cliente.apellido = apellido.getText().toString();
        modelc.cliente.celular = telefono.getText().toString();


        if (modelc.direcciones.size() > 0) {
            Direccion dir = modelc.direcciones.get(0);
            dir.nombre = nombredir1.getText().toString();
            dir.direccion = dir1.getText().toString();
            if (lat1 != 0) {
                dir.ubicacion.latitud = lat1;
                dir.ubicacion.longitud = lon1;
            }
            if (!dir.tieneUbicacion()){
                dir.ubicacion.latitud = modelc.lastLatitud;
                dir.ubicacion.longitud = modelc.lastLongitud;
            }
            dir.posicion = 1;


        } else {
            Direccion direccion1 = new Direccion();
            direccion1.nombre = nombredir1.getText().toString();
            direccion1.direccion = dir1.getText().toString();
            modelc.direcciones.add(direccion1);

            if (lat1 != 0) {
                direccion1.ubicacion.latitud = lat1;
                direccion1.ubicacion.longitud = lon1;
            }
            if (!direccion1.tieneUbicacion()){
                direccion1.ubicacion.latitud = modelc.lastLatitud;
                direccion1.ubicacion.longitud = modelc.lastLongitud;
            }
            direccion1.posicion = 1;


        }


        if (mostrandoDir2) {
            if (modelc.direcciones.size() > 1) {
                Direccion dir = modelc.direcciones.get(1);
                dir.nombre = nombredir2.getText().toString();
                dir.direccion = dir2.getText().toString();
                if (lat2 != 0) {
                    dir.ubicacion.latitud = lat2;
                    dir.ubicacion.longitud = lon2;
                }
                if (!dir.tieneUbicacion()){
                    dir.ubicacion.latitud = modelc.lastLatitud;
                    dir.ubicacion.longitud = modelc.lastLongitud;
                }
                dir.posicion = 2;


            } else {

                Direccion direccion2 = new Direccion();
                direccion2.nombre = nombredir2.getText().toString();
                direccion2.direccion = dir2.getText().toString();
                if (lat2 != 0) {
                    direccion2.ubicacion.latitud = lat2;
                    direccion2.ubicacion.longitud = lon2;
                }
                if (!direccion2.tieneUbicacion()){
                    direccion2.ubicacion.latitud = modelc.lastLatitud;
                    direccion2.ubicacion.longitud = modelc.lastLongitud;
                }
                direccion2.posicion = 2;
                modelc.direcciones.add(direccion2);
            }
        }


        if (mostrandoDir3) {
            if (modelc.direcciones.size() > 2) {
                Direccion dir = modelc.direcciones.get(2);
                dir.nombre = nombredir3.getText().toString();
                dir.direccion = dir3.getText().toString();
                if (lat3 != 0) {
                    dir.ubicacion.latitud = lat3;
                    dir.ubicacion.longitud = lon3;
                }
                if (!dir.tieneUbicacion()){
                    dir.ubicacion.latitud = modelc.lastLatitud;
                    dir.ubicacion.longitud = modelc.lastLongitud;
                }
                dir.posicion = 3;



            } else {
                Direccion direccion3 = new Direccion();
                direccion3.nombre = nombredir3.getText().toString();
                direccion3.direccion = dir3.getText().toString();
                if (!direccion3.tieneUbicacion()){
                    direccion3.ubicacion.latitud = modelc.lastLatitud;
                    direccion3.ubicacion.longitud = modelc.lastLongitud;
                }
                direccion3.posicion = 3;
                modelc.direcciones.add(direccion3);
            }

        }


        if (!mostrandoDir3 && modelc.direcciones.size() > 2) {
            modelc.direcciones.remove(2);
        }

        if (!mostrandoDir2 && modelc.direcciones.size() > 1) {
            modelc.direcciones.remove(1);
        }

    }

    public void didTapUsarGPS1(View v) {

        if (!abrirMapa()){
            return;
        }

        dirBuscada = "UNO";
        //getLocation();

        lat1 = modelc.lastLatitud;
        lon1 = modelc.lastLongitud;

        if (modelc.direcciones.size() == 0 ) {
            Direccion direccion1 = new Direccion();
            direccion1.nombre = "";
            direccion1.direccion = "";
            direccion1.ubicacion.latitud = lat1;
            direccion1.ubicacion.longitud = lon1;
            direccion1.posicion = 1;
            modelc.direcciones.add(direccion1);
        }

        Intent i = new Intent(getApplicationContext(), MapsActivityCurrentPlace.class);
        i.putExtra("POSDIR", 0);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);


    }

    public void didTapUsarGPS2(View v) {
        if (!abrirMapa()){
            return;
        }

        dirBuscada = "DOS";
        //getLocation();

        lat2 = modelc.lastLatitud;
        lon2 = modelc.lastLongitud;

        if (modelc.direcciones.size() < 2 ) {
            Direccion direccion2 = new Direccion();
            direccion2.nombre = "";
            direccion2.direccion = "";
            direccion2.ubicacion.latitud = lat2;
            direccion2.ubicacion.longitud = lon2;
            direccion2.posicion = 2;
            modelc.direcciones.add(direccion2);
        }



        Intent i = new Intent(getApplicationContext(), MapsActivityCurrentPlace.class);
        i.putExtra("POSDIR",1);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }

    public void didTapUsarGPS3(View v) {

        if (!abrirMapa()){
            return;
        }

        dirBuscada = "TRES";
        //getLocation();
        lat3 = modelc.lastLatitud;
        lon3 = modelc.lastLongitud;

        if (modelc.direcciones.size() < 3 ) {
            Direccion direccion3 = new Direccion();
            direccion3.nombre = "";
            direccion3.direccion = "";
            direccion3.ubicacion.latitud = lat3;
            direccion3.ubicacion.longitud = lon3;
            direccion3.posicion = 3;
            modelc.direcciones.add(direccion3);
        }

        Intent i = new Intent(getApplicationContext(), MapsActivityCurrentPlace.class);
        i.putExtra("POSDIR",2);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }



    public boolean abrirMapa() {

        if (!checkLocationOn()) {

            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                    ClienteRegistroBasicos.this);

            // set title
            alertDialogBuilder.setTitle("¡Lo sentimos!");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Active su Ubicación (gps) en su dispositivo para poder continuar")
                    .setNegativeButton("Cancelar", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            return;
                        }
                    })
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                            return;

                        }
                    });

            // create alert dialog
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            return false;

        }
        return true;

    }


    /////////////////////
    /////////////////////


    private void getLocation() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(100);
        locationRequest.setFastestInterval(50);
        fusedLocationProviderApi = LocationServices.FusedLocationApi;
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }


    @Override
    public void informacionRegistroCliente() {

    }

    @Override
    public void errorInformacionRegistroCliente() {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //Toast.makeText(this, "MAPA: conectado", Toast.LENGTH_SHORT).show();
        if (ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        try {
            fusedLocationProviderApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }catch (Exception e){

        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Toast.makeText(this, "MAPA: cdesonectado", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onLocationChanged(Location location) {
        //Toast.makeText(this, "location getted", Toast.LENGTH_SHORT).show();

        modelc.lastLatitud = location.getLatitude();
        modelc.lastLongitud = location.getLongitude();

        if (dirBuscada.equals("UNO")) {
            lon1 = location.getLongitude();
            lat1 = location.getLatitude();
        }

        if (dirBuscada.equals("DOS")) {
            lon2 = location.getLongitude();
            lat2 = location.getLatitude();
        }

        if (dirBuscada.equals("TRES")) {
            lon3 = location.getLongitude();
            lat3 = location.getLatitude();
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Fallo en la conexión:" + connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }


    public void didTapFondo(View v) {


        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch(Exception e) {

        }
    }


    private void preCargar() {

        String tipo = "";
        Mascota mascota = modelc.getMascotaActiva();
        if (mascota != null ) {
            tipo = mascota.tipo;
        }


        Ubicacion ubicacion =  modelc.getDireccionActiva().ubicacion;

        new ComandoCompras(new OnComandoComprasListener() {
            @Override
            public void cargoNuevaCompra() {

            }

            @Override
            public void cargoModificacionCompra() {

            }
        }).getComprasAbiertas(modelc.cliente.id);

        new ComandoCompras(new OnComandoComprasListener() {
            @Override
            public void cargoNuevaCompra() {

            }

            @Override
            public void cargoModificacionCompra() {

            }
        }).getComprasCerradas(modelc.cliente.id);


        new ComandoCalificacion(null).getMisCalificaciones(modelc.cliente.id);
        new ComandoTips().getTips();
        new ComandoTips().getPolitica();
        new ComandoAyuda(null).getInicales();
        CmdNoti.getInstance().getNotificaciones();


        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {
                //Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                //startActivity(i);
                //finish();

            }

            @Override
            public void cargoPublicacion() {
                ///Pilas aca no puede ir nada// o jamas se va a llamar
            }
        }).getLast6Destacados(tipo);


        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {

            }

            @Override
            public void cargoPublicacion() {
                googleApiClient.disconnect();
                //Intent i = new Intent(getApplicationContext(), ClienteCrearTarjeta.class);
                //i.putExtra("ORIGEN", "REGISTRO");
                //startActivity(i);
                //finish();

                Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();



            }
        }).getOferentesCercanos(ubicacion);


    }


    @Override
    protected void onResume() {
        super.onResume();
        refrescarDatos();


    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            getDeviceLocation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }




    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    getDeviceLocation();

                }
            }
        }
        //updateLocationUI();
    }


    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = locCliente.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();


                            if (mLastKnownLocation != null) {

                                //Toast.makeText(getApplicationContext(), "loc getted", Toast.LENGTH_SHORT).show();

                                modelc.lastLatitud = mLastKnownLocation.getLatitude();
                                modelc.lastLongitud = mLastKnownLocation.getLongitude();


                                return;
                            }

                        } else {

                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }


    public boolean checkLocationOn(){


        final Context context = this.getBaseContext();
        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            return false;

        }

        return true;

    }




}
