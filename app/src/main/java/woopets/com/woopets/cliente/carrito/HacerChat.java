package woopets.com.woopets.cliente.carrito;

        import android.app.Activity;
        import android.os.Bundle;
        import android.support.design.widget.FloatingActionButton;
        import android.support.design.widget.Snackbar;
        import android.support.v7.app.AppCompatActivity;
        import android.support.v7.widget.Toolbar;
        import android.view.View;
        import android.widget.TextView;

        import woopets.com.woopets.Modelo.cliente.Chat;
        import woopets.com.woopets.Modelo.cliente.Compra;
        import woopets.com.woopets.Modelo.cliente.Mensaje;
        import woopets.com.woopets.Modelo.cliente.Pregunta;
        import woopets.com.woopets.Modelo.cliente.Publicacion;
        import woopets.com.woopets.Oferente.ModelOferente.Utility;
        import woopets.com.woopets.R;
        import woopets.com.woopets.cliente.Comandos.ComandoChat;
        import woopets.com.woopets.cliente.Comandos.ComandoPreguntas;
        import woopets.com.woopets.cliente.ModelCliente;


public class HacerChat extends Activity {


    TextView pregunta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hacer_pregunta);

        pregunta = (TextView) findViewById(R.id.pregunta);

    }


    public void didTapEnviarPregunta(View v){



        if (pregunta.getText().toString().equals("")) {
            finish();
            return;
        }
        ModelCliente modelc = ModelCliente.getInstance();
        String idCompra = getIntent().getStringExtra("IDCOMPRA");
        Compra compra =  modelc.getCompraById(idCompra);
        Publicacion publi = modelc.getPublicacionById(compra.idPublicacion);

        Chat chat = new Chat();
        chat.idCompra =  idCompra;
        chat.id = new ComandoChat(null).getLlave();
        chat.emisor =  modelc.cliente.id;
        chat.fechaMensaje = Utility.getFechaHora();
        chat.mensaje =  pregunta.getText().toString();
        chat.receptor = compra.idOferente;


        modelc.addChat(chat);

        new ComandoChat(null).ponerChat(chat);


        Mensaje msg = new Mensaje();

        msg.idOferente = publi.idOferente;
        msg.idCompra = compra.id;
        msg.infoAdicional = publi.nombre;
        msg.mensaje = chat.mensaje;
        msg.tipo = "chat";
        msg.titulo = "Nuevo mensaje";


        new ComandoChat(null).enviarPushNotification(msg);


        finish();

    }

}
