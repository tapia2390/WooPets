package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.Oferente;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.ModelCliente.OnModelClienteListener;

public class AgendarServicio extends Activity {


    EditText fechaServcio, horaServicio;
    TextView duracion;
    Dialog popUp;

    Date fecha;
    private int mHour, mMinute;
    ModelCliente modelc = ModelCliente.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendar_servicio);

        fechaServcio = (EditText) findViewById(R.id.fecha);
        horaServicio = (EditText) findViewById(R.id.hora);
        fechaServcio.setInputType(InputType.TYPE_NULL);
        horaServicio.setInputType(InputType.TYPE_NULL);
        duracion = (TextView) findViewById((R.id.duracion));
        popUp = new Dialog(this);


        if (savedInstanceState != null &&  modelc.publicaciones.size() == 0) {


            final String idPublicacion = savedInstanceState.getString("IDPUBLICACION");
            modelc.compraActual.cantidad  = savedInstanceState.getInt("CANTIDAD");
            modelc.compraActual.idPublicacion = idPublicacion;
            modelc.compraActual.fecha = savedInstanceState.getString("fechaServicio");
            modelc.compraActual.hora = savedInstanceState.getString("horaServicio");



            modelc.reiniciarCarga(idPublicacion, new OnModelClienteListener() {
                @Override
                public void terminoPrecarga() {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fechaServcio.setText( modelc.compraActual.fecha);
                            horaServicio.setText( modelc.compraActual.hora);
                            Publicacion publi = modelc.getPublicacionById(idPublicacion);
                            duracion.setText("Recuerda, la duración de este servicio es de: " + publi.duracion + " " + publi.duracionMedida );
                        }
                    });

                }
            });
            return;

        }

        Publicacion publi = modelc.getPublicacionById(modelc.compraActual.idPublicacion);
        duracion.setText("Recuerda, la duración de este servicio es de: " + publi.duracion + " " + publi.duracionMedida );


    }


    public void onSaveInstanceState(Bundle outState) {
        outState.putString("IDPUBLICACION", modelc.compraActual.idPublicacion);
        outState.putString("fechaServicio", modelc.compraActual.fecha);
        outState.putString("horaServicio", modelc.compraActual.hora);
        outState.putString("CANTIDAD", "1");


        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }



    public void didTapFecha(View v){

        fechaServcio.setError(null);

        final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Calendar newCalendar =  Calendar.getInstance();

        DatePickerDialog StartTime = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fecha = newDate.getTime();
                fechaServcio.setText(dateFormat.format(newDate.getTime()));
                horaServicio.requestFocus();


            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));



        StartTime.setTitle("Fecha del servicio");
        StartTime.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
        StartTime.show();
    }


    public void didTapHora(View v) {

        horaServicio.setError(null);
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String hora, minutos;

                        if (hourOfDay == 0) {
                            hora = "12";
                        }
                        else if (hourOfDay == 13) {
                            hora = "1";
                        }else if (hourOfDay == 14){
                            hora = "2";
                        }
                        else if (hourOfDay == 15){
                            hora = "3";
                        }
                        else if (hourOfDay == 16){
                            hora = "4";
                        }
                        else if (hourOfDay == 17){
                            hora = "5";
                        }
                        else if (hourOfDay == 18){
                            hora = "6";
                        }
                        else if (hourOfDay == 19){
                            hora = "7";
                        }
                        else if (hourOfDay == 20){
                            hora = "8";
                        }
                        else if (hourOfDay == 21){
                            hora = "9";
                        }
                        else if (hourOfDay == 22){
                            hora = "10";
                        }
                        else if (hourOfDay == 23){
                            hora = "11";
                        }
                        else if (hourOfDay == 24){
                            hora = "12";
                        }
                        else {
                            hora = "" + hourOfDay;
                        }


                        if (minute < 10 ){
                            minutos = "0" + minute;
                        }else{
                            minutos = "" + minute;
                        }

                        if (hourOfDay > 11) {
                            horaServicio.setText(hora + ":" + minutos + " PM");
                        }
                        else {
                            horaServicio.setText(hora + ":" + minutos + " AM");
                        }

                    }
                }, mHour, mMinute, false);

        timePickerDialog.show();

    }

    public void didTapAceptar(View v) {

        Publicacion publi = modelc.getPublicacionById(modelc.compraActual.idPublicacion);


        modelc.compraActual.fecha = fechaServcio.getText().toString();
        modelc.compraActual.hora  = horaServicio.getText().toString();

        if (modelc.compraActual.fecha.length() < 5 ) {
            fechaServcio.setError("Debes ingresar una fecha");
            return;
        }

        if (modelc.compraActual.hora.length() < 3) {
            horaServicio.setError("Debes ingresar la hora");
            return;
        }


        String res = publi.horaDiaValidoParaServicio(modelc.compraActual.fecha, modelc.compraActual.hora);

        if (!res.equals("")){
            TextView cerrar, textoPrincipal;
            TextView diasUno, diasDos, horaUno, horaDos, jornadaUno, jornadaDos;
            LinearLayout  layHoraDos;

            popUp.setContentView(R.layout.ver_mas);


            cerrar = popUp.findViewById(R.id.cerrar);
            textoPrincipal  = popUp.findViewById(R.id.textoPrincipal);
            diasUno = popUp.findViewById(R.id.diasUno);
            diasDos = popUp.findViewById(R.id.diasDos);
            horaUno = popUp.findViewById(R.id.hora1);
            horaDos = popUp.findViewById(R.id.hora2);
            jornadaUno = popUp.findViewById(R.id.jornadaUno);
            jornadaDos = popUp.findViewById(R.id.jornadaDos);
            layHoraDos = popUp.findViewById(R.id.layHorarioDos);


            if (publi.horarios.size() > 0 ) {
                Horario hUno = publi.horarios.get(0);
                diasUno.setText(hUno.dias);
                horaUno.setText(hUno.horaInicio + " : " + hUno.horaCierre );
                if (hUno.sinJornadaContinua) {
                    jornadaUno.setText("Cerramos entre 12 y 2:00 PM");
                }

            }
            if (publi.horarios.size() > 1) {
                Horario hDos = publi.horarios.get(1);
                diasDos.setText(hDos.dias);
                horaDos.setText(hDos.horaInicio + " : " + hDos.horaCierre );
                if (hDos.sinJornadaContinua) {
                    jornadaDos.setText("Cerramos entre 12 y 2:00 PM");
                }
            }

            if (publi.horarios.size() == 1) {
                layHoraDos.setVisibility(View.GONE);
            }

            textoPrincipal.setText(res);



            cerrar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    popUp.dismiss();
                }
            });

            popUp.show();


            return;
        }




        Intent i = new Intent(getApplicationContext(), ConfirmacionCompra.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }


}
