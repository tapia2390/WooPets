package woopets.com.woopets.cliente.alarmas;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.firebase.storage.UploadTask.TaskSnapshot;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.CrearMascota;
import woopets.com.woopets.cliente.ListaMacotasAdapter;
import woopets.com.woopets.cliente.ListaMacotasAdapter.CrearMascotaAdapterCallback;
import woopets.com.woopets.cliente.ModelCliente;

public class ListaAlarmas extends  Activity  {

    private ListView mListView;
    private ListaAlarmasAdapter mAdapter;

    String idMascota;


    ModelCliente modelc = ModelCliente.getInstance();


    int indexAlerta = -1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_alarmas);

        idMascota = getIntent().getStringExtra("IDMASCOTA");

        Mascota mascota = modelc.getMascotaById(idMascota);

        mAdapter = new ListaAlarmasAdapter(this,mascota);
        mListView = (ListView) findViewById(R.id.lista);
        mListView.setAdapter(mAdapter);

        TextView nombre = (TextView) findViewById(R.id.lbmascota);
        nombre.setText(mascota.nombre);



    }


    public void didTapNuevaAlarma(View v) {

        idMascota = getIntent().getStringExtra("IDMASCOTA");

        Mascota mascota = modelc.getMascotaById(idMascota);

        Intent i = new Intent(getApplicationContext(), CrearAlarma.class);
        i.putExtra("IDMASCOTA",mascota.id);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        //finish();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAdapter.notifyDataSetChanged();

    }
}
