package woopets.com.woopets.cliente.Comandos;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import woopets.com.woopets.Modelo.cliente.Distancia;
import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.MiniOferente;
import woopets.com.woopets.Modelo.cliente.Oferente;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Modelo.cliente.Ubicacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 2/12/18.
 */

public class ComandoOferentes {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    public interface OnComandoOferentesChangeListener {

        void cargoMiniOferentes();

    }


    private ComandoOferentes.OnComandoOferentesChangeListener mListener;

    public ComandoOferentes(ComandoOferentes.OnComandoOferentesChangeListener mListener){

        this.mListener = mListener;
    }


    public void getMiniOferentes(){
        DatabaseReference ref = database.getReference("oferentes/");

        modelc.miniOferentes.clear();

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                for (DataSnapshot miniSnap :snap.getChildren()) {
                    modelc.miniOferentes.add(readMiniOferente(miniSnap));

                }
                mListener.cargoMiniOferentes();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }



    public MiniOferente readMiniOferente(DataSnapshot snap) {

        MiniOferente ofer = new MiniOferente();

        ofer.id = snap.getKey();
        String estado  = snap.child("aprobacionMyPet").getValue().toString();

        if (estado.equals("Aprobado")){
           ofer.activo = true;
        } else{
            ofer.activo = false;
        }


        return  ofer;

    }




    public static ComandoOferentes.OnComandoOferentesChangeListener dummy = new ComandoOferentes.OnComandoOferentesChangeListener() {


        @Override
        public void cargoMiniOferentes() {

        }


    };



}

    


