package woopets.com.woopets.cliente;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Pattern;

import woopets.com.woopets.BuildConfig;
import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.Ubicacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCliente;
import woopets.com.woopets.cliente.Comandos.ComandoCompras;
import woopets.com.woopets.cliente.Comandos.ComandoCompras.OnComandoComprasListener;
import woopets.com.woopets.cliente.Comandos.ComandoOferentes;
import woopets.com.woopets.cliente.Comandos.ComandoOferentes.OnComandoOferentesChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoRegistroCliente;
import woopets.com.woopets.cliente.Comandos.ComandoTips;

public class ClienteRegistro extends Activity implements ComandoCliente.OnComandoClienteChangeListener, ComandoRegistroCliente.OnComandoRegistroClienteChangeListener{

    EditText email, password;
    ProgressBar progressBar;
    Button entrar;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    ModelCliente modelc = ModelCliente.getInstance();
    //Modelo modelo = Modelo.getInstance();
    private static final String TAG ="AndroidBash";
    private CallbackManager callbackManager;
    public Cliente cliente;
    private ProgressDialog mProgressDialog;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mRef;
    ComandoRegistroCliente comandoRegistrarte ;
    ComandoCliente comandoCliente;
    final Context context = this;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_cliente_registro);


        comandoCliente = new ComandoCliente(this);
        comandoRegistrarte = new ComandoRegistroCliente(this);


        email =(EditText)findViewById(R.id.email);
        password =(EditText)findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        entrar = (Button) findViewById(R.id.registro);


        mRef = database.getReference("clientes/");

        if(estaConectado()){
            //Toast.makeText(getApplication(),"Conectado a internet", Toast.LENGTH_SHORT).show();

            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {

                        if (user.isAnonymous()){
                            return;
                        }

                        modelc.cliente.id = user.getUid();

                        String provider =  user.getProviders().get(0);

                        if (provider.equals("password")) {
                            modelc.tipoLogeo = "normal";
                        }else{
                            modelc.tipoLogeo = "faceb";
                        }

                        if (modelc.tipoLogeo.equals("faceb")){
                            for (UserInfo userInfo : user.getProviderData()) {
                                if (modelc.cliente.nombre == null && userInfo.getDisplayName() != null) {
                                    modelc.cliente.nombre = userInfo.getDisplayName();
                                    modelc.cliente.correo = userInfo.getEmail();
                                }
                                    /*if (profileUri == null && userInfo.getPhotoUrl() != null) {
                                        profileUri = userInfo.getPhotoUrl();
                                    }*/
                            }

                            comandoRegistrarte.enviarRegistro3(""+user.getDisplayName(), ""+user.getUid(), user.getEmail());


                        }

                        if(modelc.tipoLogeo.equals("normal")){
                            comandoRegistrarte.enviarRegistro2(user.getUid());
                        }


                        comandoCliente.getCliente(user.getUid());

                        //Toast.makeText(getApplication(),"Cargando Cliente  " + user.getUid() , Toast.LENGTH_LONG).show();

                    } else {
                        // Users is signed out
                        Log.d(TAG, "onAuthStateChanged:signed_out" + " no logueado");
                        //Toast.makeText(getApplication(),"...."+user+"no logueado", Toast.LENGTH_SHORT).show();
                    }

                }
            };
        }else{
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            showAlertSinInternet();
            return;
        }



        //FaceBook
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.button_facebook_login);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                modelc.tipoLogeo="faceb";
                Log.d(TAG, "facebook:onSuccess:" + loginResult);


                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                Log.v("facebook", "facebook"+error.getMessage());
            }
        });

        email.clearFocus();
        password.clearFocus();


    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,
                resultCode, data);
    }




    @Override
    public void onStart() {
        super.onStart();
        if(mAuth==null || mAuthListener ==null){
            return;
        }else{
            mAuth.addAuthStateListener(mAuthListener);
        }


    }


    @Override
    public void onStop() {
        super.onStop();

        if(mAuth==null || mAuthListener ==null){
            return;
        }else{
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }



    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }


    public void IrIngresar(View v){
        Intent i = new Intent(getApplicationContext(), Cliente_login.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }



    //validacion conexion internet
    protected Boolean estaConectado(){
        if(conectadoWifi()){
            Log.v("wifi","Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        }else{
            if(conectadoRedMovil()){
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            }else{
                showAlertSinInternet();
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    protected Boolean conectadoWifi(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }



    public void registroCliente(View v){
        if (!Utility.isEmailValid(email.getText().toString()) || password.getText().toString().equals("")){
            android.app.AlertDialog.Builder dialogo1 = new android.app.AlertDialog.Builder(this);
            dialogo1.setTitle("Campos Erroneos");
            dialogo1.setMessage("Todos los campos son obligatorios");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    return;
                }
            });
            dialogo1.show();
            return;
        }


        if (mAuth.getCurrentUser() != null && mAuth.getCurrentUser().isAnonymous()){
            linkAccount();
        }else {
            hacerRegistro();
        }


    }


    private void linkAccount() {

        // Create EmailAuthCredential with email and password
        AuthCredential credential = EmailAuthProvider.getCredential(email.getText().toString(), password.getText().toString());

        // Link the anonymous user to the email credential
        showProgressDialog();

        linkConCredenciales(credential);


    }



    public void linkConCredenciales(final AuthCredential credential){
        mAuth.getCurrentUser().linkWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "linkWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();

                            modelc.cliente.id = user.getUid();

                            String provider =  user.getProviders().get(0);

                            if (provider.equals("password")) {
                                modelc.tipoLogeo = "normal";
                            }else{
                                modelc.tipoLogeo = "faceb";
                            }


                            //Cuando viene de anonimo esta fue la unica que funciona pata coger el diplayName
                            if (modelc.tipoLogeo.equals("faceb")){
                                for (UserInfo userInfo : user.getProviderData()) {
                                    if (userInfo.getDisplayName() != null) {
                                        modelc.cliente.nombre = userInfo.getDisplayName();

                                    }

                                }

                                comandoRegistrarte.enviarRegistro3(modelc.cliente.nombre, user.getUid(), user.getEmail());
                                modelc.cliente.correo = user.getEmail();


                            }

                            if(modelc.tipoLogeo.equals("normal")){
                                comandoRegistrarte.enviarRegistro2(user.getUid());
                                modelc.cliente.nombre = "";
                                modelc.cliente.correo = email.getText().toString();
                                modelc.cliente.apellido = "";
                                modelc.cliente.cedula = "";
                                modelc.cliente.celular  = "";
                                comandoRegistrarte.actualizarDatosBasicos();
                            }

                            comandoCliente.getCliente(user.getUid());


                        } else {

                            String error= "Problema  al registrarse ";
                            try {
                                error = ((FirebaseAuthUserCollisionException) task.getException()).getErrorCode();
                            }
                            catch (ClassCastException c){

                            }
                            if (error.equals("ERROR_CREDENTIAL_ALREADY_IN_USE")){
                                Toast.makeText(getApplicationContext(), "Esas credenciales ya estan asociadas a otra cuenta.",
                                        Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(getApplicationContext(), error,
                                        Toast.LENGTH_SHORT).show();
                            }

                            Log.w(TAG, "linkWithCredential:failure", task.getException());

                            LoginManager.getInstance().logOut();

                        }

                        // [START_EXCLUDE]
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END link_credential]


    }




    public void hacerRegistro(){

        showProgressDialog();


        mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            //checking if success
                            Log.d(TAG, "signInWithEmail:onComplete:"+task.isSuccessful());

                            FirebaseAuthException ex = (FirebaseAuthException) task.getException();
                            if (ex!=null){
                                Log.v("error re", "registro error"+ex.getLocalizedMessage());

                                //Toast.makeText(getApplicationContext(), "Ocurrio un error al registrarse, intente nuevamente.", Toast.LENGTH_LONG).show();
                                System.out.print(""+ex.getLocalizedMessage());

                                String error = ex.getErrorCode();

                                mProgressDialog.dismiss();
                                Log.v("log re","error re"+error);
                                if(error.equals("ERROR_EMAIL_ALREADY_IN_USE")){
                                    Toast.makeText(getApplicationContext(), "Una cuenta con ese correo ya  existe.", Toast.LENGTH_LONG).show();
                                }

                                if(error.equals("ERROR_WEAK_PASSWORD")){
                                    Toast.makeText(getApplicationContext(), " Contraseña muy debil.", Toast.LENGTH_LONG).show();
                                }

                                return;
                            }


                            if(task.isSuccessful()){
                               createNewUser(task.getResult().getUser());


                            }else{
                                //display some message here
                                Toast.makeText(getApplicationContext(),"error de registro, intentelo de nuevo",Toast.LENGTH_LONG).show();
                            }


                            mProgressDialog.dismiss();
                        }
                    });


    }

    private void createNewUser(FirebaseUser userFromRegistration) {

        String versionName = BuildConfig.VERSION_NAME;
        String userId = userFromRegistration.getUid();

        comandoRegistrarte.registroInicialCliente(email.getText().toString(),userId, versionName, false );
        //Toast.makeText(getApplicationContext(), "Se envio el registro ", Toast.LENGTH_LONG).show();
        email.setText("");
        password.setText("");

        Intent i  = new Intent(getApplicationContext(), ClienteRegistro.class);
        startActivity(i);
        finish();

    }

    private void createNewUserFB(FirebaseUser user) {


        String versionName = BuildConfig.VERSION_NAME;
        String userId = user.getUid();

        comandoRegistrarte.registroInicialClienteFB(user.getEmail(), user.getDisplayName(), userId, versionName );
        //Toast.makeText(getApplicationContext(), "Se envio el registro ", Toast.LENGTH_LONG).show();
        email.setText("");
        password.setText("");


        Intent i  = new Intent(getApplicationContext(), ClienteRegistroExitoso.class);
        startActivity(i);
        finish();

    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Esperando...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }



    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            //mProgressDialog.dismiss();
        }
    }





    //fin facebook

    private void handleFacebookAccessToken(final AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        showProgressDialog();

        if (mAuth.getCurrentUser() != null && mAuth.getCurrentUser().isAnonymous()){
            AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
            linkConCredenciales(credential);
            return;
        }

        mProgressDialog.setMessage("Registrandose en WooPets...");

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {

                            String error= "Problema con la configuración. ";
                            hideProgressDialog();
                            mProgressDialog.dismiss();
                            LoginManager.getInstance().logOut();
                            if (task.getException() instanceof FirebaseException){
                                error = ((FirebaseException) task.getException()).toString();
                                Log.e("ERROR CON FACE-FIRE", error);
                                //progressDialog.dismiss();
                                //return;
                            }
                            try {
                                error = ((FirebaseAuthException) task.getException()).getErrorCode();
                            }
                            catch (ClassCastException c){
                                c.printStackTrace();
                            }

                            if (error.equals("ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL")){
                                Toast.makeText(getApplicationContext(), "Una cuenta con ese correo ya existe.",
                                        Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(getApplicationContext(), error,
                                        Toast.LENGTH_SHORT).show();
                            }

                        }else{

                            mProgressDialog.setMessage(" Cargando vista de registro...");
                            String uid=task.getResult().getUser().getUid();
                            String name=task.getResult().getUser().getDisplayName();
                            String email=task.getResult().getUser().getEmail();

                            Log.v("datos usuario","datos usuario"+uid+name+email);

                            FirebaseUser user = task.getResult().getUser();

                            //createNewUserFB(user);

                        }

                        hideProgressDialog();
                    }
                });
    }



    public void showAlertSinInternet(){

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("Sin Internet");

        // set dialog message
        alertDialogBuilder
                .setMessage("Sin Conexión a Internet")
                .setCancelable(false)
                .setPositiveButton("Reintentar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        Intent inte  = new Intent(getBaseContext(),ClienteRegistro.class);
                        startActivity(inte);
                    }
                });

        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }


    public static boolean checkValidEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern) && email.length() > 0){
            return true;
        }
        return false;
    }


    @Override
    public void cargoCliente() {


        new ComandoOferentes(new OnComandoOferentesChangeListener() {
            @Override
            public void cargoMiniOferentes() {
                continuarCarga();
            }
        }).getMiniOferentes();




    }

    public void continuarCarga(){

        if (modelc.tipoLogeo.equals("faceb")){
            if ( !modelc.cliente.tieneDatosBasicos() ){
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                createNewUserFB(user);
                return;
            }
            else{
                preCargarFull();
                return;
            }

        }


        if ( modelc.cliente.tieneDatosBasicos() ) {
            preCargar();
        } else {
            Intent i = new Intent(getApplicationContext(),ClienteRegistroExitoso.class);
            startActivity(i);
            finish();
        }


    }


    @Override
    public void clienteNoExiste() {

    }

    private void preCargar(){

        Mascota mascota =  modelc.getMascotaActiva();
        Ubicacion ubicacion =  modelc.getDireccionActiva().ubicacion;
        if ( mascota != null) {
            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {
                    //Intent i = new Intent(getApplicationContext(),HomeCliente.class);
                    //startActivity(i);
                    //finish();
                }

                @Override
                public void cargoPublicacion() {
                    ///Pilas aca no puede ir nada// o jamas se va a llamar
                }
            }).getLast6Destacados(mascota.tipo);


            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {

                }

                @Override
                public void cargoPublicacion() {

                }
            }).getOferentesCercanos(ubicacion);

           /* new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {
                    ///Pilas aca no puede ir nada// o jamas se va a llamar
                }

                @Override
                public void cargoPublicacion() {
                    Intent i = new Intent(getApplicationContext(),HomeCliente.class);
                    startActivity(i);
                    finish();
                }
            }).getTodasPublicaciones(mascota.tipo);*/


        }


    }


    private void preCargarFull() {

        String tipo = "";
        Mascota mascota = modelc.getMascotaActiva();
        if (mascota != null) {
            tipo = mascota.tipo;
        }


        new ComandoCalificacion(null).getMisCalificaciones(modelc.cliente.id);
        new ComandoTips().getTips();
        new ComandoTips().getPolitica();
        new ComandoAyuda(null).getInicales();
        CmdNoti.getInstance().getNotificaciones();


        Direccion direccion = modelc.getDireccionActiva();

        new ComandoCompras(new OnComandoComprasListener() {
            @Override
            public void cargoNuevaCompra() {

            }

            @Override
            public void cargoModificacionCompra() {

            }
        }).getComprasAbiertas(modelc.cliente.id);

        new ComandoCompras(new OnComandoComprasListener() {
            @Override
            public void cargoNuevaCompra() {

            }

            @Override
            public void cargoModificacionCompra() {

            }
        }).getComprasCerradas(modelc.cliente.id);



        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {
                /*Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                startActivity(i);
                finish();*/
            }

            @Override
            public void cargoPublicacion() {
                ///Pilas aca no puede ir nada// o jamas se va a llamar
            }
        }).getLast6Destacados(tipo);

        if (direccion != null && direccion.ubicacion != null) {

            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {

                }

                @Override
                public void cargoPublicacion() {
                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                    startActivity(i);
                    finish();

                }
            }).getOferentesCercanos(direccion.ubicacion);

        } else {
            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {
                    ///Pilas aca no puede ir nada// o jamas se va a llamar
                }

                @Override
                public void cargoPublicacion() {
                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                    startActivity(i);
                    finish();
                }
            }).getTodasPublicaciones(tipo);




        }
    }






    @Override
    public void informacionRegistroCliente() {

    }

    @Override
    public void errorInformacionRegistroCliente() {

    }
}
