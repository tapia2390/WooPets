package woopets.com.woopets.cliente.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import woopets.com.woopets.Modelo.cliente.Solicitud;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.ModelCliente;

public class AyudaDetalle extends Activity {


    ModelCliente modelCliente = ModelCliente.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ayuda_detalle);


        String idSolicitud = getIntent().getStringExtra("IDSOLICITUD");
        Solicitud sol = modelCliente.getSolicitudById(idSolicitud);

        TextView tipo = (TextView) findViewById(R.id.tipo);
        TextView enunciado = (TextView) findViewById(R.id.enunciado);
        TextView numero = (TextView) findViewById(R.id.numero);
        TextView fechaSolicitud = (TextView) findViewById(R.id.fechaSolicitud);
        TextView respuesta = (TextView) findViewById(R.id.respuesta);
        TextView fechaCierre = (TextView) findViewById(R.id.fechaCierre);
        TextView estado = (TextView) findViewById(R.id.estado);
        LinearLayout layer = (LinearLayout) findViewById(R.id.layerRespuesta);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


        tipo.setText(sol.getTipoLargo());
        enunciado.setText(sol.enunciado);
        numero.setText(sol.numeroSeguimiento);
        fechaSolicitud.setText(sol.fechaGeneracion);
        respuesta.setText(sol.respuesta);
        fechaCierre.setText(sol.fechaCierre);
        estado.setText(sol.estado);

        layer.setVisibility(View.GONE);
        if (!sol.estado.equals("Abierta")){
            layer.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }
}
