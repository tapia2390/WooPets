package woopets.com.woopets.cliente.Comandos;


import com.firebase.client.FirebaseError;
import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.Transaction.Handler;
import com.google.firebase.database.Transaction.Result;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Modelo.cliente.Solicitud;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/25/17.
 */

public class ComandoAyuda {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    
    public interface OnComandoAyudaListener {

        void cargoSolicitud();
        void reservoNumero(int newNumero);

    }


    private OnComandoAyudaListener mListener;


    public ComandoAyuda(OnComandoAyudaListener mListener){

        this.mListener = mListener;
    }



    private Solicitud readSolicitud(DataSnapshot snap) {

        Solicitud sol = snap.getValue(Solicitud.class);
        sol.id = snap.getKey();
        return sol;

    }



    public void getSolicitudes(final String idCliente){

        DatabaseReference ref = database.getReference("solicitudes");

        Query query = ref.orderByChild("idCliente").equalTo(idCliente);
        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {
                Solicitud sol = readSolicitud(snap);
                modelc.addSolicitud(sol);
                mListener.cargoSolicitud();

            }

            @Override
            public void onChildChanged(DataSnapshot snap, String s) {
                Solicitud sol = readSolicitud(snap);
                modelc.addSolicitud(sol);
                mListener.cargoSolicitud();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        };

        query.addChildEventListener(listener);
        modelc.hijosListener.put(query,listener);

    }





    public void crearSolicitud(Solicitud sol) {

        DatabaseReference ref = database.getReference("solicitudes/");
        ref = ref.child(ref.push().getKey());

        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("enunciado", sol.enunciado);
        mapa.put("estado",  sol.estado);
        mapa.put("idCliente", sol.idCliente);
        mapa.put("numeroSeguimiento", sol.numeroSeguimiento);
        mapa.put("fechaGeneracion", Utility.getFechaHora());
        mapa.put("timestamp", ServerValue.TIMESTAMP);
        mapa.put("tipoSolicitud", sol.tipoSolicitud);
        ref.setValue(mapa);

    }

    public void reservarNumeroPregunta(String inicial){
        DatabaseReference ref = database.getReference("listados/tiposSolicitudes/"+inicial+"/consecutivo");


        ref.runTransaction(new Handler() {
            @Override
            public Result doTransaction(MutableData mutableData) {
                Object o = mutableData.getValue();

                if (o == null){
                    return Transaction.success(mutableData);
                }
                int numero = Integer.valueOf(o.toString());

                mutableData.setValue(numero + 1);

                
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean commited, DataSnapshot snap) {
                if (commited){
                    mListener.reservoNumero(Integer.valueOf(snap.getValue().toString()));

                }

            }
        });



    }

    public void getInicales(){


        DatabaseReference ref = database.getReference("listados/tiposSolicitudes/P/siglaConsecutivo");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelc.inicialP = (String) snap.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        DatabaseReference ref1 = database.getReference("listados/tiposSolicitudes/Q/siglaConsecutivo");
        ref1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelc.inicialQ = (String) snap.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        DatabaseReference ref2 = database.getReference("listados/tiposSolicitudes/R/siglaConsecutivo");
        ref2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelc.inicialR = (String) snap.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        DatabaseReference ref3 = database.getReference("listados/tiposSolicitudes/S/siglaConsecutivo");
        ref2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelc.inicialS = (String) snap.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }





    public static ComandoAyuda.OnComandoAyudaListener dummy = new ComandoAyuda.OnComandoAyudaListener() {


        @Override
        public void cargoSolicitud() {

        }

        @Override
        public void reservoNumero(int newNumero) {

        }


    };



}




