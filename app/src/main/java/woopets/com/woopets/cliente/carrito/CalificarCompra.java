package woopets.com.woopets.cliente.carrito;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.Mensaje;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoChat;
import woopets.com.woopets.cliente.ModelCliente;

public class CalificarCompra extends Activity {

    ImageView star1, star2, star3,star4,star5;
    EditText comentario;
    Compra compra;
    ModelCliente modelc = ModelCliente.getInstance();
    int calificacion= 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calificar_compra);

        star1 = (ImageView) findViewById(R.id.estre1);
        star2 = (ImageView) findViewById(R.id.estre2);
        star3 = (ImageView) findViewById(R.id.estre3);
        star4 = (ImageView) findViewById(R.id.estre4);
        star5 = (ImageView) findViewById(R.id.estre5);

        comentario =  (EditText) findViewById(R.id.comentario);

        String idCompra = getIntent().getStringExtra("IDCOMPRA");
        compra = modelc.getCompraById(idCompra);


    }


    public void didTapEnviar(View v){

        Calificacion cali = new Calificacion();
        cali.idCompra = compra.id;
        cali.calificacion = calificacion;
        cali.comentario = comentario.getText().toString();
        cali.idCliente = modelc.cliente.id;
        cali.idOferente = compra.idOferente;
        cali.idPublicacion = compra.idPublicacion;

        Publicacion publi = modelc.getPublicacionById(compra.idPublicacion);

        new ComandoCalificacion(null).calificarMiCompra(cali);

        modelc.misCalificaciones.add(cali);

        Mensaje msg = new Mensaje();

        msg.idOferente = compra.idOferente;
        msg.idPublicacion = compra.idPublicacion;
        msg.idCompra = compra.id;
        msg.infoAdicional = publi.nombre;
        msg.tipo = "ventaCalificada";
        msg.titulo = "Venta calificada";
        if (publi.servicio ) {
            msg.mensaje = "La venta de tu servicio " + publi.nombre + " ha sido calificada";
        } else {
            msg.mensaje = "La venta de tu producto " + publi.nombre + " ha sido calificada";
        }



        new ComandoChat(null).enviarPushNotification(msg);

        finish();


    }



    public void didTapS1(View v){
        calificacion = 1;
        pintarEstrellas(1);
    }

    public void didTapS2(View v){
        calificacion = 2;
        pintarEstrellas(2);
    }
    public void didTapS3(View v){
        calificacion = 3;
        pintarEstrellas(3);
    }
    public void didTapS4(View v){
        calificacion = 4;
        pintarEstrellas(4);
    }
    public void didTapS5(View v){
        calificacion = 5;
        pintarEstrellas(5);
    }



    private void pintarEstrellas(double calificacion){
        pintarApagarPrenderEstrella(star1,1,calificacion);
        pintarApagarPrenderEstrella(star2,2,calificacion);
        pintarApagarPrenderEstrella(star3,3,calificacion);
        pintarApagarPrenderEstrella(star4,4,calificacion);
        pintarApagarPrenderEstrella(star5,5,calificacion);
    }

    private void pintarApagarPrenderEstrella(ImageView star, double pos, double nota){

        if (nota >= pos) {
            star.setImageResource(R.drawable.imgestrellacompleta);
            return;
        }

        if (Math.abs(nota-pos) <= 0.5) {
            star.setImageResource(R.drawable.imgestrellamedia);
            return;
        }

        star.setImageResource(R.drawable.imgestrellavacia);

    }
}
