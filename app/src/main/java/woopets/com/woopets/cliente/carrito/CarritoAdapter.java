package woopets.com.woopets.cliente.carrito;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.Item;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;


/**
 * Created by andres on 10/31/17.
 */

public class CarritoAdapter  extends BaseAdapter {


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private OnImageFireChange listener;
    private final LayoutInflater mInflater;


    public CarritoAdapter(Context c, OnImageFireChange listener) {
        mContext = c;
        this.listener = listener;
        mInflater = LayoutInflater.from(c);


    }

    @Override
    public int getCount() {
        modelc.sortMisCompras();
        return modelc.carritos.size();
    }

    @Override
    public Object getItem(int i) {
        return modelc.carritos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {


            convertView = mInflater.inflate(R.layout.item_carrito,parent, false);
            TextView valor = (TextView) convertView.findViewById(R.id.valor);
            ImageView foto = (ImageView) convertView.findViewById(R.id.foto);
            TextView total = (TextView) convertView.findViewById(R.id.total);
            TextView nombre = (TextView) convertView.findViewById(R.id.nombre);
            TextView btnComprar =  (TextView) convertView.findViewById(R.id.btnComprar);
            TextView btnEliminar =  (TextView) convertView.findViewById(R.id.btnEliminar);
            LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.linear);


        final ItemCarrito  item = modelc.carritos.get(i);
        final Publicacion publi = modelc.getPublicacionById(item.idPublicacion);
        publi.fotosBaja.get(0).setListener(listener);


        btnComprar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, DetallePublicacion.class);
                i.putExtra("IDPUBLICACION",publi.id);
                mContext.startActivity(i);
                ((Activity)mContext).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

            }
        });

        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, DetallePublicacion.class);
                i.putExtra("IDPUBLICACION",publi.id);
                mContext.startActivity(i);
                ((Activity)mContext).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });

        btnEliminar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                modelc.eliminarPublicacionDelCarrito(publi.id);
                CarritoAdapter.this.notifyDataSetChanged();

            }
        });


        if (publi == null) {
            return  convertView;
        }


        nombre.setText(publi.nombre);
        valor.setText(Utility.convertToMoney(publi.precio) + " (" + item.cantidadCompra + ")" );
        int costo = Integer.valueOf(publi.precio) * item.cantidadCompra;
        total.setText(Utility.convertToMoney(costo));

        Bitmap fotoBitmap = publi.fotosBaja.get(0).getFoto("PUBLICACION");

        if (fotoBitmap != null) {
            foto.setImageBitmap(fotoBitmap);
        }

        return convertView;

    }



}
