package woopets.com.woopets.cliente.detallecompra;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.Item;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;


/**
 * Created by andres on 10/31/17.
 */

public class ListaPreguntasAdapter  extends BaseAdapter {


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private final LayoutInflater mInflater;

    ArrayList<Pregunta> preguntas ;


    public ListaPreguntasAdapter(Context c, ArrayList<Pregunta> preguntas) {
        mContext = c;
        mInflater = LayoutInflater.from(c);

       this.preguntas = preguntas;

    }

    @Override
    public int getCount() {
        return preguntas.size();
    }

    @Override
    public Object getItem(int i) {
        return preguntas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {



        Pregunta pregu = preguntas.get(i);


        if (pregu.id.equals("HEADER MIAS")){
            convertView =  mInflater.inflate(R.layout.header_rojo,parent, false);
            TextView titulo =  (TextView) convertView.findViewById(R.id.texto);
            titulo.setText("Mis preguntas");


        } else if  (pregu.id.equals("HEADER RESTO")){
            convertView =  mInflater.inflate(R.layout.header_rojo,parent, false);
            TextView titulo =  (TextView) convertView.findViewById(R.id.texto);
            titulo.setText("Preguntas de otros usuarios");

        } else {

            convertView = mInflater.inflate(R.layout.item_lista_preguntas,parent, false);

            TextView horaPregunta = (TextView) convertView.findViewById(R.id.horaPregunta);
            TextView horaRespuesta =(TextView) convertView.findViewById(R.id.horaRespuesta);
            TextView pregunta = (TextView) convertView.findViewById(R.id.pregunta);
            TextView respuesta = (TextView) convertView.findViewById(R.id.respuesta);
            TextView palRespuesta = (TextView) convertView.findViewById(R.id.layerPalRespuesta);
            LinearLayout layerRespuesta = (LinearLayout) convertView.findViewById(R.id.layerRespuesta);


            horaPregunta.setText(pregu.fechaPregunta);

            respuesta.setText(pregu.respuesta);
            pregunta.setText(pregu.pregunta);

            if (pregu.respuesta.equals("") ){
                palRespuesta.setText("Aún sin respuesta");
                layerRespuesta.setVisibility(View.GONE);
                horaRespuesta.setVisibility(View.GONE);
            } else {
                palRespuesta.setText("Respuesta");
                horaRespuesta.setText(pregu.fechaRespuesta);
                horaRespuesta.setVisibility(View.VISIBLE);
                layerRespuesta.setVisibility(View.VISIBLE);
            }

        }


        return convertView;

    }



}
