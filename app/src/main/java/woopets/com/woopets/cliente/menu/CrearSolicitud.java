package woopets.com.woopets.cliente.menu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import woopets.com.woopets.Modelo.cliente.Solicitud;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda.OnComandoAyudaListener;
import woopets.com.woopets.cliente.ModelCliente;

public class CrearSolicitud extends Activity {

    TextView tipo,comentario;
    ModelCliente modelc = ModelCliente.getInstance();

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_solicitud);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

         tipo  = (TextView) findViewById(R.id.tipo);
         comentario = (TextView) findViewById(R.id.consulta);

        progressDialog = new ProgressDialog(this);


    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }


    public void didTapTipo(View v){
        showOpcionesTipo();

    }


    private void showOpcionesTipo() {
        final String[] items = {"Pregunta","Queja","Reclamo", "Solicitud"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tipo de solicitud");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                String stipo =  items[item];
                tipo.setText(stipo);


            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }



    public void didTapEnviar(View v){
        if (comentario.getText().equals("") || tipo.getText().equals("Tipo de solicitud")){
            return;
        }

        progressDialog.setMessage("Enviando la solicitud...");
        progressDialog.show();



        new ComandoAyuda(new OnComandoAyudaListener() {
            @Override
            public void cargoSolicitud() {

            }

            @Override
            public void reservoNumero(int newNumero) {

                Solicitud sol  = new Solicitud();
                sol.idCliente = modelc.cliente.id;
                sol.enunciado = comentario.getText().toString();
                sol.tipoSolicitud = modelc.getInicialeParaTipo(tipo.getText().toString());
                String dosIniciales = modelc.getInicialesParaTipo(tipo.getText().toString());
                sol.numeroSeguimiento = Utility.llenarCeros(dosIniciales,newNumero);
                new ComandoAyuda(null).crearSolicitud(sol);
                finish();
                progressDialog.dismiss();


            }

        }).reservarNumeroPregunta(modelc.getInicialeParaTipo(tipo.getText().toString()));

    }




}
