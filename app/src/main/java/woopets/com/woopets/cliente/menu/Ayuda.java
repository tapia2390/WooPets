package woopets.com.woopets.cliente.menu;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.ClienteRegistroBasicos;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda.OnComandoAyudaListener;
import woopets.com.woopets.cliente.ListaMacotasAdapter;
import woopets.com.woopets.cliente.ModelCliente;

public class Ayuda extends Activity {

    private ListView mListView;
    private AyudaAdapter mAdapter;

    ModelCliente modelc = ModelCliente.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ayuda);

        mAdapter = new AyudaAdapter(this);
        mListView = (ListView) findViewById(R.id.lista);
        mListView.setAdapter(mAdapter);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

        new ComandoAyuda(new OnComandoAyudaListener() {
            @Override
            public void cargoSolicitud() {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void reservoNumero(int newNumero) {
                
            }
        }).getSolicitudes(modelc.cliente.id);


    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }


    public void didTapNuevaSolitud(View v){

        Intent i = new Intent(getApplicationContext(), CrearSolicitud.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }


}
