package woopets.com.woopets.cliente.alarmas;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import woopets.com.woopets.Modelo.cliente.Alarma;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoAlarmas;
import woopets.com.woopets.cliente.ModelCliente;

import static woopets.com.woopets.R.id.editText;
import static woopets.com.woopets.R.id.hora;

public class CrearAlarma extends Activity {


    TextView tipo, horaAlarma, frecuencia, fechaInicio, fechaFin;
    EditText nombre;
    FrameLayout fondoTipo, fondoFrecuencia;
    ModelCliente modelc = ModelCliente.getInstance();

    Mascota mascota;
    Alarma alarma = null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_alarma);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        String idMascota = getIntent().getStringExtra("IDMASCOTA");
        mascota = modelc.getMascotaById(idMascota);


        tipo = (TextView) findViewById(R.id.tipo);
        horaAlarma = (TextView) findViewById(R.id.hora);
        frecuencia = (TextView) findViewById(R.id.frecuencia);
        fechaInicio = (TextView) findViewById(R.id.fechaInicio);
        fechaFin = (TextView) findViewById(R.id.fechaFin);
        fondoTipo = (FrameLayout) findViewById(R.id.fondotipo);
        fondoFrecuencia = (FrameLayout) findViewById(R.id.fondofrecuencia);
        nombre = (EditText) findViewById(R.id.nombre);



        if (getIntent().hasExtra("IDALARMA")){

            String idAlarma = getIntent().getStringExtra("IDALARMA");
            alarma  = mascota.getAlarmaById(idAlarma);

            if (alarma != null) {
                tipo.setText(alarma.frecuencia);
                horaAlarma.setText(alarma.hora);
                frecuencia.setText(alarma.frecuencia);
                fechaInicio.setText(alarma.fechaInicio);
                fechaFin.setText(alarma.fechaFin);
                nombre.setText(alarma.nombre);

            }

        }


        fondoTipo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] items = {"Antipulgas","Baño","Desparasitante", "Vacuna"};

                AlertDialog.Builder builder = new AlertDialog.Builder(CrearAlarma.this);
                builder.setTitle("Tipo de Alarma");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        String stipo =  items[item];
                        tipo.setText(stipo);


                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });



        fondoFrecuencia.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                final String[] items = {"Semanal","Quincenal","Mensual", "Bimensual", "Anual"};

                AlertDialog.Builder builder = new AlertDialog.Builder(CrearAlarma.this);
                builder.setTitle("Tipo de solicitud");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        String stipo =  items[item];
                        frecuencia.setText(stipo);


                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        nombre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focused) {
                InputMethodManager keyboard = (InputMethodManager) CrearAlarma.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (focused)
                    keyboard.showSoftInput(nombre, 0);
                else
                    keyboard.hideSoftInputFromWindow(nombre.getWindowToken(), 0);
            }
        });


    }




    public void  didTapHora(View v ){

        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String hora, minutos;

                        if (hourOfDay == 0) {
                            hora = "12";
                        }
                        else {
                            hora = "" + hourOfDay;
                        }

                        if (minute == 0) {
                            minutos = "00";
                        }
                        else {
                            minutos = "" + minute;
                        }

                        if (hourOfDay > 11) {
                            horaAlarma.setText(hora+ ":" + minutos + " PM");
                        }
                        else {
                            horaAlarma.setText(hora + ":" + minutos + " AM");
                        }

                    }
                }, mHour, mMinute, false);

        timePickerDialog.show();


    }



    public void  didFechaInicio(View v ){
        final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }catch (Exception e){

        }

        Calendar newCalendar =  Calendar.getInstance();

        DatePickerDialog StartTime = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                //fecha = newDate.getTime();
                fechaInicio.setText(dateFormat.format(newDate.getTime()));


            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));



        StartTime.setTitle("Fecha de inicio");
        StartTime.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
        StartTime.show();
    }




    public void  didFechaFin(View v ){

        final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }catch (Exception e){

        }

        Calendar newCalendar =  Calendar.getInstance();

        DatePickerDialog StartTime = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                //fecha = newDate.getTime();
                fechaFin.setText(dateFormat.format(newDate.getTime()));


            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        StartTime.setTitle("Fecha de finalización");
        StartTime.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
        StartTime.show();

    }



    public void didTapAceptar(View v){
        Alarma newAlarma = new Alarma();
        newAlarma.id = new ComandoAlarmas().getLLave();
        newAlarma.activada = true;
        newAlarma.nombre = nombre.getText().toString();
        newAlarma.frecuencia = frecuencia.getText().toString();
        newAlarma.hora = horaAlarma.getText().toString();
        newAlarma.fechaInicio = fechaInicio.getText().toString();
        newAlarma.fechaFin = fechaFin.getText().toString();
        newAlarma.tipoRecordatorio = tipo.getText().toString();
        newAlarma.idAlarmaManager = modelc.getIdNuevaAlarma();


        if (alarma != null ){
            newAlarma.id = alarma.id;
            newAlarma.idAlarmaManager = alarma.idAlarmaManager;
            newAlarma.activada = alarma.activada;
            new ComandoAlarmas().updateAlarma(modelc.cliente.id,mascota.id,newAlarma);
            finish();

        } else{
            mascota.alarmas.add(newAlarma);
            new ComandoAlarmas().crearAlarma(modelc.cliente.id,mascota.id,newAlarma);

            finish();
        }




    }

    public void didTapCancelar(View v){
        finish();

    }


    public void iniciarAlarma(){




    }




}
