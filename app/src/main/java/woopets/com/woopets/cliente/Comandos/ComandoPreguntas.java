package woopets.com.woopets.cliente.Comandos;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/25/17.
 */

public class ComandoPreguntas {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    public interface OnComandoPreguntasListener {

        void cargoPregunta(String idPublicacion);

    }


    private ComandoPreguntas.OnComandoPreguntasListener mListener;

    public ComandoPreguntas(ComandoPreguntas.OnComandoPreguntasListener mListener){

        this.mListener = mListener;
    }


    private Pregunta readPregunta(DataSnapshot snap) {


        Pregunta pregu = snap.getValue(Pregunta.class);
        pregu.id = snap.getKey();
        return pregu;
    }



    public void getPreguntasByIdPublicacion(final String idPublicacion){

        DatabaseReference ref = database.getReference("preguntas");

        Query query = ref.orderByChild("idPublicacion").equalTo(idPublicacion);
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {


                for (DataSnapshot snapPregunta : snap.getChildren()) {
                    Pregunta pregu = readPregunta(snapPregunta);
                    modelc.addPregunta(pregu);
                }

                mListener.cargoPregunta(idPublicacion);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        };

        query.addValueEventListener(listener);

        modelc.hijosListenerValue.put(query,listener);

    }

    public String getLlave(){

        DatabaseReference ref = database.getReference("preguntas/");
        return ref.push().getKey();
    }

    public static String getLlaveFirebase(){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("preguntas/");
        return ref.push().getKey();
    }


    public void hacerPreguntaAPublicacion(Pregunta pregu) {

        DatabaseReference ref = database.getReference("preguntas/" + pregu.id);


        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("fechaPregunta", Utility.getFechaHora());
        mapa.put("idCliente",  pregu.idCliente);
        mapa.put("idOferente", pregu.idOferente);
        mapa.put("idPublicacion", pregu.idPublicacion);
        mapa.put("pregunta", pregu.pregunta);
        mapa.put("timestamp", ServerValue.TIMESTAMP);

        ref.setValue(mapa);

    }




    public static ComandoPreguntas.OnComandoPreguntasListener dummy = new ComandoPreguntas.OnComandoPreguntasListener() {


        @Override
        public void cargoPregunta(String idPublicacion) {

        }



    };



}




