package woopets.com.woopets.cliente.Comandos;

        import android.util.Log;

        import com.firebase.client.ServerValue;
        import com.google.firebase.auth.FirebaseAuth;
        import com.google.firebase.database.ChildEventListener;
        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.DatabaseReference.CompletionListener;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.Query;
        import com.google.firebase.database.ValueEventListener;

        import java.text.DateFormat;
        import java.text.SimpleDateFormat;
        import java.util.Date;
        import java.util.HashMap;
        import java.util.Map;

        import woopets.com.woopets.Modelo.cliente.Cliente;
        import woopets.com.woopets.Modelo.cliente.Compra;
        import woopets.com.woopets.Modelo.cliente.Direccion;
        import woopets.com.woopets.Modelo.cliente.Horario;
        import woopets.com.woopets.Modelo.cliente.InfoCompraActual;
        import woopets.com.woopets.Modelo.cliente.Mascota;
        import woopets.com.woopets.Modelo.cliente.Mensaje;
        import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
        import woopets.com.woopets.Modelo.cliente.Oferente;
        import woopets.com.woopets.Modelo.cliente.PagoTarjetaCliente;
        import woopets.com.woopets.Modelo.cliente.Publicacion;
        import woopets.com.woopets.Modelo.cliente.Ubicacion;
        import woopets.com.woopets.Oferente.ModelOferente.Modelo;
        import woopets.com.woopets.Oferente.ModelOferente.Utility;
        import woopets.com.woopets.cliente.Comandos.ComadoPago.OnComadoPagoListener;
        import woopets.com.woopets.cliente.ModelCliente;
        import woopets.com.woopets.payU.PagoPayU;

/**
 * Created by andres on 10/22/17.
 */

public class ComadoPago {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();



    public interface OnComadoPagoListener {
        void didPagoExitoso();
        void didPagoFallido();
        void didRegistroExitoso();
        void didRegistroFallido();
    }



    private ComadoPago.OnComadoPagoListener mListener;

    public ComadoPago(ComadoPago.OnComadoPagoListener mListener){

        this.mListener = mListener;
    }



    public void crearOrden(final Compra newCompra, boolean conTarjeta) {

        DatabaseReference ref = database.getReference("compras/abiertas/"+newCompra.id);
        final Publicacion publi = modelc.getPublicacionById(newCompra.idPublicacion);

        final Map<String, Object> compra = new HashMap<String, Object>();

        compra.put("fecha", Utility.getFechaHora());
        compra.put("idCliente", modelc.cliente.id);
        compra.put("idOferente",  publi.idOferente);
        compra.put("metodoPago", newCompra.metodoPago);
        compra.put("valor", newCompra.valor);
        compra.put("timestamp", ServerValue.TIMESTAMP);
        compra.put("Dispositivo", "Android");


        int contador  = 1;

        Map<String, Object> pedido = new HashMap<String, Object>();

        for(int i = 0; i < 1; i++) {

            Map<String, Object> nuevoItem = new HashMap<String, Object>();
            nuevoItem.put("cantidad",newCompra.cantidad);
            nuevoItem.put("departamento",newCompra.departamento);
            nuevoItem.put("direccion",newCompra.direccion);
            nuevoItem.put("estado","Pendiente");
            nuevoItem.put("idPublicacion",newCompra.idPublicacion);
            nuevoItem.put("reprogramada",false);
            nuevoItem.put("servicio", publi.servicio);
            if (publi.servicio) {
                nuevoItem.put("fechaServicio", newCompra.fechaServicio);
            }

            pedido.put(""+contador,nuevoItem);
            contador = contador+1;
        }

        if (conTarjeta && newCompra.servicio){
            MiniTarjeta mini = modelc.getTarjetaActiva();
            Map<String, Object> tarjeta = new HashMap<String, Object>();
            tarjeta.put("cuotasTarjeta",mini.cuotas);
            tarjeta.put("idTarjeta",mini.id);
            tarjeta.put("tokenTarjeta", mini.token);
            tarjeta.put("ciudad", mini.ciudad);
            tarjeta.put("departamento", mini.departamento);
            tarjeta.put("correo", mini.correo);
            tarjeta.put("franquicia", mini.franquicia);
            tarjeta.put("telefono", mini.telefono);
            tarjeta.put("cedula", mini.cedula);
            tarjeta.put("direccion", mini.direccion);
            tarjeta.put("nombre", mini.nombre);

            compra.put("datosTarjeta",tarjeta);
        }



        compra.put("pedido",pedido);


        ref.setValue(compra, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    mListener.didPagoFallido();
                } else {   //Pago exitoso

                    Mensaje msg = new Mensaje();

                    msg.idCompra = newCompra.id;
                    msg.idOferente = publi.idOferente;
                    msg.infoAdicional = publi.nombre;
                    if (publi.servicio){
                        msg.mensaje = "Felicitaciones te han reservado el servicio " + publi.nombre ;
                    } else {
                        msg.mensaje = "Felicitaciones has vendido el producto " + publi.nombre ;
                    }

                    msg.tipo = "ventaRealizada";
                    msg.titulo = "Venta realizada";


                    new ComandoChat(null).enviarPushNotification(msg);


                    mListener.didPagoExitoso();
                }
            }
        });

    }




    public void registrarPago(String idCompra, PagoTarjetaCliente pago){

        DatabaseReference ref = database.getReference("pagosCliente/" + idCompra);

        Map<String, Object> nuevoItem = new HashMap<String, Object>();
        nuevoItem.put("Descripcion",pago.Descripcion);
        nuevoItem.put("authorizationCode",pago.authorizationCode);
        nuevoItem.put("idCompra",pago.idCompra);
        nuevoItem.put("idPago",pago.idPago);
        nuevoItem.put("idTarjeta",pago.idTarjeta);
        nuevoItem.put("metodoPago", "Tarjeta");
        nuevoItem.put("paymentTransaction", pago.paymentTransaction);
        nuevoItem.put("timestamp", ServerValue.TIMESTAMP);


        ref.setValue(nuevoItem, new CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            mListener.didRegistroFallido();
                        } else {
                            mListener.didRegistroExitoso();
                        }
                    }
                }
        );

    }



    public void registrarPagoOferente(PagoTarjetaCliente pago, Compra compra){

        DatabaseReference ref = database.getReference("pagosOferente");
        ref = ref.child(ref.push().getKey());


        DatabaseReference key = referencia.push();

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("hh:mm a");
        System.out.println("Hora: "+hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: "+dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: "+hourdateFormat.format(date));



        int setHora = 0;
        String string = ""+hourFormat.format(date);
        String[] parts = string.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];

        if(part2.equals("a.") || part2.equals("a.m.") || part2.equals("a. m.")|| part2.equals("A.")){
            part2 = "AM";
        }
        if(part2.equals("p.") ||  part2.equals("p.m.") ||  part2.equals("p. m.") || part2.equals("P.")){
            part2 = "PM";
        }


        Map<String, Object> nuevoItem = new HashMap<String, Object>();
        nuevoItem.put("fechaPago", dateFormat.format(date)+" "+part1+" "+part2);
        nuevoItem.put("Descripcion",pago.Descripcion);
        nuevoItem.put("authorizationCode",pago.authorizationCode);
        nuevoItem.put("idPublicacion",compra.idPublicacion);
        nuevoItem.put("idPago",pago.idPago);
        nuevoItem.put("idTarjeta",pago.idTarjeta);
        nuevoItem.put("metodoPago", "Tarjeta");
        nuevoItem.put("paymentTransaction", pago.paymentTransaction);
        nuevoItem.put("timestamp", ServerValue.TIMESTAMP);


        ref.setValue(nuevoItem, new CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            mListener.didRegistroFallido();
                        } else {
                            mListener.didRegistroExitoso();
                        }
                    }
                }
        );

    }





    public static ComadoPago.OnComadoPagoListener dummy = new ComadoPago.OnComadoPagoListener() {


        @Override
        public void didPagoExitoso() {

        }

        @Override
        public void didPagoFallido() {

        }

        @Override
        public void didRegistroExitoso() {

        }

        @Override
        public void didRegistroFallido() {

        }
    };



}

    

