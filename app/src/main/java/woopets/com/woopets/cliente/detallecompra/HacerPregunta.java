package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import woopets.com.woopets.Modelo.cliente.Mensaje;
import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoChat;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas;
import woopets.com.woopets.cliente.ModelCliente;

public class HacerPregunta extends Activity {


    TextView pregunta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hacer_pregunta);

        pregunta = (TextView) findViewById(R.id.pregunta);

    }


    public void didTapEnviarPregunta(View v){


        if (pregunta.getText().toString().equals("")) {
            finish();
            return;
        }




        ModelCliente modelc = ModelCliente.getInstance();
        String idPublicacion = getIntent().getStringExtra("IDPUBLICACION");
        Publicacion publi =  modelc.getPublicacionById(idPublicacion);




        Pregunta pregu = new Pregunta();
        pregu.pregunta = pregunta.getText().toString();
        pregu.idPublicacion = publi.id;
        pregu.idOferente = publi.idOferente;
        pregu.idCliente = modelc.cliente.id;
        pregu.fechaPregunta = Utility.getFechaHora();
        pregu.id = new ComandoPreguntas(null).getLlave();
        modelc.addPregunta(pregu);
        new ComandoPreguntas(null).hacerPreguntaAPublicacion(pregu);


        Mensaje msg = new Mensaje();

        msg.idOferente = publi.idOferente;
        msg.idPublicacion = publi.id;
        msg.infoAdicional = publi.nombre;
        msg.mensaje = "Tienes una pregunta en una de tus publicaciones";
        msg.tipo = "pregunta-respuesta";
        msg.titulo = "Nueva pregunta";


        new ComandoChat(null).enviarPushNotification(msg);



        //Ahora se muestra dialogo
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                HacerPregunta.this);

        alertDialogBuilder.setTitle("¡Envío exitoso!");

        // set dialog message
        alertDialogBuilder
                .setMessage("Tu pregunta ha sido enviada a la persona encargada. Más adelante te llegará una notificación informándote su respuesta")
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        finish();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

}
