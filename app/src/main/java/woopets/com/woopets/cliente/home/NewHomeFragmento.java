package woopets.com.woopets.cliente.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.Distancia;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.ClienteRegistro;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.CmdPayU;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCompras;
import woopets.com.woopets.cliente.Comandos.ComandoCompras.OnComandoComprasListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTips;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ListaMascotas;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.busquedas.BuscarGeneral;
import woopets.com.woopets.cliente.detallecompra.CambiarDireccion;

import static com.facebook.FacebookSdk.getApplicationContext;


public class NewHomeFragmento extends Fragment implements OnImageFireChange, OnClickListener {


    private ListView listaView;

    ModelCliente modelc = ModelCliente.getInstance();
    FiltroCategoriaAdapter listAdapter;
    DestacadosPageAdapter pageAdapter;
    Mascota mascota;
    ImageView foto;
    TextView nombre;
    TextView textoBarraRojaUbicacion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View vi = inflater.inflate(R.layout.fragment_new_home_fragmento, container, false);

        listaView = (ListView) vi.findViewById(R.id.lista);
        listAdapter = new FiltroCategoriaAdapter(getContext(), "", this, false);
        listaView.setAdapter(listAdapter);


        /*if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            ((Activity)getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

        }*/


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        nombre = (TextView)vi.findViewById(R.id.nombre) ;
        foto = (ImageView) vi.findViewById(R.id.foto);

        FrameLayout buscar = (FrameLayout) vi.findViewById(R.id.buscarBarra);


        buscar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), BuscarGeneral.class);
                startActivity(i);
                ((Activity)getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });

        pintarMascota();




        ///Click sobre la foto
        foto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!modelc.tipoLogeo.equals("anonimo")) {

                    Intent i = new Intent(getApplicationContext(), ListaMascotas.class);
                    startActivity(i);
                    ((Activity) getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                }

            }
        });

        nombre.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!modelc.tipoLogeo.equals("anonimo")) {

                    Intent i = new Intent(getApplicationContext(), ListaMascotas.class);
                    startActivity(i);
                    ((Activity) getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                }
            }
        });



        // header Destacados
        FrameLayout layDestacados =  (FrameLayout) View.inflate(getContext(), R.layout.layer_destacados, null);
        ViewPager pager = (ViewPager)layDestacados.findViewById(R.id.pager);
        if (modelc.destacados.size() > 0) {
            pageAdapter = new DestacadosPageAdapter(getContext(),this);

            TabLayout tabpuntos = (TabLayout) layDestacados.findViewById(R.id.tab_puntos);
            tabpuntos.setupWithViewPager(pager, true);

            pager.setAdapter(pageAdapter);
            listaView.addHeaderView(layDestacados,null,false);

        } else {
            pager.setVisibility(View.GONE);
        }



        ViewGroup.LayoutParams params1 = layDestacados.getLayoutParams();
        if (params1 != null) {
            params1.width= ViewGroup.LayoutParams.MATCH_PARENT;
            params1.height = (int)(size.y * 0.42);
        } else {
            params1 = new ViewGroup.LayoutParams((int) (size.x), (int) (size.y * 0.42));
        }
        layDestacados.setLayoutParams(params1);



        // header Categorias

        HorizontalScrollView cateView =  (HorizontalScrollView) View.inflate(getContext(), R.layout.layout_categorias, null);
        listaView.addHeaderView(cateView);

        //////////Barra de categorias
        ((LinearLayout)cateView.findViewById(R.id.optAccesorios)).setOnClickListener(this);
        ((LinearLayout)cateView.findViewById(R.id.optAmiguitos)).setOnClickListener(this);
        ((LinearLayout)cateView.findViewById(R.id.optGuarderia)).setOnClickListener(this);
        ((LinearLayout)cateView.findViewById(R.id.optMedicamentos)).setOnClickListener(this);
        ((LinearLayout)cateView.findViewById(R.id.optNutricion)).setOnClickListener(this);
        ((LinearLayout)cateView.findViewById(R.id.optPaseador)).setOnClickListener(this);
        ((LinearLayout)cateView.findViewById(R.id.optSalud)).setOnClickListener(this);
        ((LinearLayout)cateView.findViewById(R.id.optLindos)).setOnClickListener(this);

        ///////////////////

        ViewGroup.LayoutParams params = cateView.getLayoutParams();
        if (params != null) {
            params.width= ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = (int)(size.y * 0.15);
        } else {
            params = new ViewGroup.LayoutParams((int) (size.x), (int) (size.y * 0.15));
        }
        cateView.setLayoutParams(params);

        Mascota mascota = modelc.getMascotaActiva();

        if (modelc.tipoLogeo.equals("anonimo") && mascota == null) {
            LinearLayout sinregistro = (LinearLayout) View.inflate(getContext(), R.layout.header_sinregistro, null);
            TextView texto = (TextView)sinregistro.findViewById(R.id.texto);
            ImageView foto = (ImageView)sinregistro.findViewById(R.id.foto);


            texto.setText("Productos populares");
            foto.setImageResource(R.drawable.btnperfil);

            sinregistro.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getApplicationContext(), ClienteRegistro.class);
                    startActivity(i);
                    ((Activity)getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                    //((Activity)getContext()).finish();

                }
            });
            listaView.addHeaderView(sinregistro,null,false);

        }
        else if (mascota == null){
            LinearLayout sinregistro = (LinearLayout) View.inflate(getContext(), R.layout.header_con_registro_sin_mascota, null);
            TextView texto = (TextView)sinregistro.findViewById(R.id.texto);
            ImageView foto = (ImageView)sinregistro.findViewById(R.id.foto);

            texto.setText("Productos populares");
            foto.setImageResource(R.drawable.btnperfil);

            sinregistro.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getApplicationContext(),ListaMascotas.class);
                    startActivity(i);
                    ((Activity)getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

                }
            });
            listaView.addHeaderView(sinregistro,null,false);


        }
        else {
            Direccion dir = modelc.getDireccionActiva();

            if (dir  == null) {
                LinearLayout barra = (LinearLayout) View.inflate(getContext(), R.layout.header_rojo, null);
                TextView texto = (TextView)barra.findViewById(R.id.texto);
                texto.setText("Productos sugeridos para tu mascota");
                listaView.addHeaderView(barra,null,false);
            }
            else {
                LinearLayout barra = (LinearLayout) View.inflate(getContext(), R.layout.header_rojo_geolocalizado, null);
                textoBarraRojaUbicacion = (TextView)barra.findViewById(R.id.texto);
                textoBarraRojaUbicacion.setText(dir.nombre);
                listaView.addHeaderView(barra,null,false);

                LinearLayout barraRoja = (LinearLayout)barra.findViewById(R.id.headermilugar);
                barraRoja.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i = new Intent(getApplicationContext(),CambiarDireccion.class);
                        startActivity(i);
                        i.putExtra("ORIGEN","HOME");
                        ((Activity)getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

                    }
                });


            }

        }




        return  vi;

    }

/*
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }*/


    @Override
    public void cargoImagen(String tipo) {

        if (tipo.equals("MASCOTA")) {
            Bitmap fotoBit = mascota.imageFire.getFoto("MASCOTA");

            if (foto != null) {
                foto.setImageBitmap(fotoBit);
            }
            return;
        }

        listAdapter.notifyDataSetChanged();
        pageAdapter.notifyDataSetChanged();

    }




    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.optNutricion: irCategorias("Nutrición");
                break;
            case R.id.optAccesorios: irCategorias("Accesorios");
                break;
            case R.id.optGuarderia: irCategorias("Guardería 5 patas");
                break;
            case R.id.optAmiguitos: irCategorias("Amiguitos en el cielo");
                break;
            case R.id.optMedicamentos: irCategorias("Medicamentos");
                break;
            case R.id.optPaseador: irCategorias("Paseador");
                break;
            case R.id.optLindos: irCategorias("Lind@ y Limpi@");
                break;
            case R.id.optSalud: irCategorias("Vamos al médico");
                break;

        }

    }


    public void irCategorias(String categoria){

        Intent i = new Intent(getApplicationContext(),FiltroCategoria.class);
        i.putExtra("CATEGORIA", categoria);
        startActivity(i);
        ((Activity)getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

    }


    public void pintarMascota(){


        mascota = modelc.getMascotaActiva();

        if (mascota == null){
            nombre.setText("Mascota sin registrar");
            foto.setImageResource(R.drawable.imgmascotasinregistro);
        } else {
            nombre.setText(mascota.nombre);

            Bitmap fotoBit = mascota.imageFire.getFoto("MASCOTA");
            mascota.imageFire.setListener(this);

            if (foto != null) {
                foto.setImageBitmap(fotoBit);
            }

        }

    }


    public void pintarUbicacion(){
        Direccion dir = modelc.getDireccionActiva();

        if (dir  == null) {

            //texto.setText("Productos sugeridos para tu mascota");
            //listaView.addHeaderView(barra,null,false);
        }
        else {
            if (textoBarraRojaUbicacion!= null){
                textoBarraRojaUbicacion.setText(dir.nombre);
            }

        }

    }


    @Override
    public void onResume() {
        super.onResume();
        mascota = modelc.getMascotaActiva();
        pintarMascota();
        pintarUbicacion();
        listAdapter.notifyDataSetChanged();

    }


}
