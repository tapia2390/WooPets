package woopets.com.woopets.cliente;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnScrollChangeListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Space;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.firebase.storage.UploadTask.TaskSnapshot;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.InformacionProductoServicioOferente;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.Oferente.MyPublicacionesFoto;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoCliente;
import woopets.com.woopets.cliente.Comandos.ComandoCliente.OnComandoClienteChangeListener;




public class CrearMascota extends Activity implements OnComandoClienteChangeListener {


    RelativeLayout layRaza;

    Space spacio;


    EditText nombre, tipoMascota, tipoRaza, tipoGenero, fechaNacimiento, edad;
    Date fecha;

    ImageView foto;
    Bitmap original;

    ModelCliente modelc = ModelCliente.getInstance();

    String[] razas = {""};

    String userChoosenTask="";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Uri uriSavedImage1;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1 ;

    private ProgressDialog progressDialog;

    ScrollView scroll;

    String modo = "CREAR";
    String idMascota = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_mascota);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        nombre = (EditText) findViewById(R.id.nombre);
        tipoMascota = (EditText) findViewById(R.id.txt_tipomascota);
        tipoRaza = (EditText) findViewById(R.id.txt_tipoRaza);
        tipoGenero =  (EditText) findViewById(R.id.txt_tipoGenero);
        fechaNacimiento = (EditText)  findViewById(R.id.fechaNacimiento);
        edad = (EditText)  findViewById(R.id.edad);
        foto = (ImageView) findViewById(R.id.fotoMascota);


        layRaza = (RelativeLayout) findViewById(R.id.listaRaza);
        spacio =  (Space) findViewById(R.id.spacio);

        scroll = (ScrollView)  findViewById(R.id.scroller);

        progressDialog = new ProgressDialog(this);


        if (getIntent().hasExtra("MODO")) {
            modo = "EDITAR";
            idMascota = getIntent().getStringExtra("IDMASCOTA");

            final Mascota mascota = modelc.getMascotaById(idMascota);
            //foto.setImageDrawable(getDrawable(R.drawable.btnfotomascota));

            Bitmap bmasc =  mascota.imageFire.getFoto("MASCOTA");
            if (bmasc != null) {
                foto.setImageBitmap(bmasc);
            }

            mascota.imageFire.setListener(new OnImageFireChange() {
                @Override
                public void cargoImagen(String tipo) {
                    if (tipo.equals("MASCOTA")){
                        Bitmap bfoto = mascota.imageFire.getFoto("MASCOTA");

                        if (bfoto != null) {
                            foto.setImageBitmap(bfoto);
                        }
                    }
                }
            });

            Bitmap bfoto = mascota.imageFire.getFoto("MASCOTA");

            if (bfoto != null) {

                foto.setImageBitmap(bfoto);
            }

            nombre.setText(mascota.nombre);
            tipoMascota.setText(mascota.tipo);
            tipoRaza.setText(mascota.raza);
            tipoGenero.setText(mascota.genero);
            fechaNacimiento.setText(Utility.convertDateToString(mascota.fechaNacimiento));
            edad.setText(getEdad(Utility.convertDateToCalendar(mascota.fechaNacimiento)));

        }




        scroll.getViewTreeObserver().addOnScrollChangedListener(new OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {

                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

            }
        });


        foto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();

            }
        });
    }




    private void showOpcionesTipo() {
        final String[] items = {"Ave","Exótico","Gato", "Perro", "Pez", "Roedor","Otro"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tipo de mascota");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                String tipo =  items[item];
                tipoMascota.setText(tipo);
                tipoRaza.setText("");
                razas = modelc.getRazas(tipo);

                layRaza.setVisibility(View.VISIBLE);
                spacio.setVisibility(View.VISIBLE);

                if (razas.length == 0 || razas.length == 1 ) {

                    layRaza.setVisibility(View.GONE);
                    spacio.setVisibility(View.GONE);

                }


            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void showOpcionesRaza() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tipo");
        builder.setItems(razas, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection

                String raza =  razas[item];
                tipoRaza.setText(raza);


            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }



    private void showOpcionesGenero() {


        final String[] generos  = new String[]{"Chico","Chica"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Genero");
        builder.setItems(generos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection

                String genero =  generos[item];
                tipoGenero.setText(genero);
                fechaNacimiento.requestFocus();


            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }


    public void tipoMascota(View v){

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
        showOpcionesTipo();

    }


    public void tipoRaza(View v){

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
        showOpcionesRaza();

    }


    public void tipoGenero(View v){

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
        showOpcionesGenero();

    }

    public void didTapFecha(View v){
        final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);

        Calendar newCalendar =  Calendar.getInstance();

        DatePickerDialog StartTime = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fecha = newDate.getTime();
                fechaNacimiento.setText(dateFormat.format(newDate.getTime()));
                edad.setText(getEdad(newDate));


            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));



        StartTime.setTitle("Fecha de nacimiento");
        StartTime.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        StartTime.show();
    }


    private String getEdad(Calendar newDate){
        long diff = new Date().getTime() - newDate.getTime().getTime()  ;
        long dias = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        int anios = (int)Math.floor(dias / 365);
        long resto  = dias % 365;
        int meses = (int)Math.floor(resto / 30);

        if (anios > 0 ){
            return "Edad: " + anios + " Años " + meses + " meses";
        }
        else {
             return "Edad: " + meses + " meses";
        }


    }





    private void selectImage() {

        final CharSequence[] items = { "Tomar foto","Selecionar imagen","Cancelar" };

        AlertDialog.Builder builder = new AlertDialog.Builder(CrearMascota.this);
        builder.setTitle("Seleccionar foto");
        AlertDialog.Builder cancelar = builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                boolean result = Utility.checkPermission(CrearMascota.this);
                if (items[item].equals("Tomar foto")) {
                    userChoosenTask = "Take Photo";
                    if (result) {
                        cameraIntent();
                    }


                } else if (items[item].equals("Selecionar imagen")) {
                    userChoosenTask = "Choose from Library";
                    if (result){
                        galleryIntent();
                    }

                } else if (items[item].equals("Cancelar")) {
                    dialog.dismiss();
                }
            }


        });
        builder.show();
    }


    //permiso camara v6
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }


    /// manejo de camara
    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent()
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent,SELECT_FILE);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        }
    }


    //galeri
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;

        File destination = null;

        if (data != null) {
            try {
                    bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                    destination = new File(Environment.getExternalStorageDirectory(),
                            System.currentTimeMillis() + ".jpg");
                    int nh = (int) ( bm.getHeight() * (512.0 / bm.getWidth()) );
                    Bitmap scaled = Bitmap.createScaledBitmap(bm, 512, nh, true);

                    foto.setImageBitmap(scaled);
                    original = scaled;
                    uriSavedImage1 = Uri.fromFile(destination);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }



    }


    //camara
    private void onCaptureImageResult(Intent data) {
        Bitmap bm=null;
        File destination = null;

        //variabale imageview para verificar el tap

        if (data != null) {
            try {
                bm = (Bitmap) data.getExtras().get("data");
                destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");


                //foto.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                //foto.setImageDrawable(getCroppedBitmap(bm));
                foto.setImageBitmap(bm);

                original = bm;
                uriSavedImage1 = Uri.fromFile(destination);

            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }



    }




    public void didTapAceptar(View v) {

        if (nombre.getText().toString().length() < 2) {
            nombre.setError("Debes incluir tu nombre");
            nombre.requestFocus();
            return;
        }

        if (tipoMascota.getText().toString().length() < 2) {
            tipoMascota.setError("Debes incluir tu tipo de mascota");
            tipoMascota.requestFocus();
            return;
        }

        if (tipoGenero.getText().toString().length() < 2) {
            tipoGenero.setError("Debes incluir el genero de tu mascota");
            tipoGenero.requestFocus();
            return;
        }

        if (fecha == null && fechaNacimiento.getText().toString().length() < 8) {
            fechaNacimiento.setError("Debes seleccionar la fecha de nacimiento");
            fechaNacimiento.requestFocus();
            return;

        }



        progressDialog.setMessage("Cargando la información Por favor, espere...");
        progressDialog.show();

        if (modo.equals("CREAR")){
            crearNueva();
        }
        else{
            actualizarActual();
        }


    }


    public void crearNueva(){
        String idMascotaNew = FirebaseDatabase.getInstance().getReference().push().getKey();

        Mascota newMacota = new Mascota(modelc.cliente.id, idMascotaNew);
        newMacota.fechaNacimiento = fecha;
        newMacota.genero = tipoGenero.getText().toString();
        newMacota.raza = tipoRaza.getText().toString();
        newMacota.activa = true;
        newMacota.nombre = nombre.getText().toString();
        newMacota.tipo = tipoMascota.getText().toString();
        newMacota.id = idMascotaNew;

        modelc.mascotas.add(newMacota);

        ComandoCliente cm = new ComandoCliente(this);
        cm.crearMascota(newMacota);
        cm.activarMascota(idMascotaNew,modelc.cliente.id);
        if (original != null ) {
            newMacota.imageFire.setFoto(original);
            uploadImage(idMascotaNew);
        }
        else {
            progressDialog.dismiss();

            Intent i = new Intent(getApplicationContext(), HomeCliente.class);
            startActivity(i);
            finish();

        }


    }

    public void actualizarActual(){

        idMascota = getIntent().getStringExtra("IDMASCOTA");

        Mascota mascota = modelc.getMascotaById(idMascota);
        mascota.nombre = nombre.getText().toString();
        mascota.activa = true;
        if (fecha != null) {
            mascota.fechaNacimiento = fecha;
        }

        mascota.genero = tipoGenero.getText().toString();
        mascota.raza = tipoRaza.getText().toString();
        mascota.nombre = nombre.getText().toString();
        mascota.tipo = tipoMascota.getText().toString();
        mascota.id = idMascota;

        ComandoCliente cm = new ComandoCliente(this);
        cm.updateMascota(mascota);
        cm.activarMascota(idMascota,modelc.cliente.id);
        if (original != null ) {
            mascota.imageFire.setFoto(original);
            uploadImage(mascota.id);
        }
        else {
            progressDialog.dismiss();

            Intent i = new Intent(getApplicationContext(), HomeCliente.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();

        }





    }



    public void didTapCancelar(View v) {

        Intent i = new Intent(getApplicationContext(), HomeCliente.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        finish();

    }


    @Override
    public void cargoCliente() {


    }

    @Override
    public void clienteNoExiste() {

    }


    private void uploadImage(String idMascota) {


        FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference storageReference = storage.getReference().child("mascotas/" + modelc.cliente.id + "/" + idMascota + "/FotoTuMascota");


            foto.setDrawingCacheEnabled(true);
            foto.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            foto.layout(0, 0, foto.getMeasuredWidth(), foto.getMeasuredHeight());
            foto.buildDrawingCache();

            //Bitmap bitmap = Bitmap.createBitmap(foto.getDrawingCache());
            Bitmap bitmap = original;

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            byte[] data = outputStream.toByteArray();

            UploadTask uploadTask = storageReference.putBytes(data);

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(getApplicationContext(), "Error, no pudo subir la foto. Tiene acceso a Internet", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }).addOnSuccessListener(new OnSuccessListener<TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();

                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                    startActivity(i);
                    finish();

                }
            });


    }



    public void didTapFondo(View v){

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);

    }



}
