package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ClienteRegistro;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion.OnComandoCalificacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoCompras;
import woopets.com.woopets.cliente.Comandos.ComandoCompras.OnComandoComprasListener;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas.OnComandoPreguntasListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTipos;
import woopets.com.woopets.cliente.Comandos.ComandoTipos.OnComandoTiposChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTips;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.ModelCliente.OnModelClienteListener;
import woopets.com.woopets.cliente.home.DestacadosPageAdapter;
import woopets.com.woopets.cliente.home.PublicacionPageAdapter;

public class DetallePublicacion extends Activity implements OnImageFireChange {


    private PublicacionPageAdapter pageAdapter;
    ModelCliente modelc = ModelCliente.getInstance();

    TextView nombre, descripcion, valor, cantidad, diponible, diasUno, diasDos, horaUno, horaDos, jornadaUno, jornadaDos, textoBotonPagar, duracion, duracionMedida ;
    TextView carrito, numeroPreguntas, pregunta;
    ImageView star1, star2, star3,star4,star5, favorito;
    ImageView circuloPerro, circuloGato,  circuloAve,  circuloPez,  circuloRoedor,  circuloExotico;


    LinearLayout layProducto, layServicio, layHoraUno, layHoraDos, layDuracion ;
    ViewPager pager;

    private int cantidadPedida = 1;

    String idPublicacion;
    Publicacion publi;

    boolean reinicio = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_publicacion);


        pager = (ViewPager) findViewById(R.id.pager);
        nombre = (TextView) findViewById(R.id.nombre);
        descripcion = (TextView) findViewById(R.id.descripcion);
        valor = (TextView) findViewById(R.id.valor);
        cantidad = (TextView) findViewById(R.id.cantidad);
        diponible = (TextView) findViewById(R.id.disponible);
        diasUno = (TextView) findViewById(R.id.diasUno);
        diasDos = (TextView) findViewById(R.id.diasDos);
        horaUno = (TextView) findViewById(R.id.hora1);
        horaDos = (TextView) findViewById(R.id.hora2);
        jornadaUno = (TextView) findViewById(R.id.jornadaUno);
        jornadaDos = (TextView) findViewById(R.id.jornadaDos);
        textoBotonPagar = (TextView) findViewById(R.id.textoBotonPagar);
        duracion = (TextView) findViewById(R.id.duracion);
        duracionMedida = (TextView) findViewById(R.id.duracionMedida);
        carrito = (TextView) findViewById(R.id.carrito);
        numeroPreguntas = (TextView) findViewById(R.id.numeroPreguntas);
        pregunta = (TextView) findViewById(R.id.pregunta);


        star1 = (ImageView) findViewById(R.id.estre1);
        star2 = (ImageView) findViewById(R.id.estre2);
        star3 = (ImageView) findViewById(R.id.estre3);
        star4 = (ImageView) findViewById(R.id.estre4);
        star5 = (ImageView) findViewById(R.id.estre5);
        favorito = (ImageView) findViewById(R.id.favoritos);

        circuloPerro = findViewById(R.id.circuloPerro);
        circuloGato = findViewById(R.id.circuloGato);
        circuloAve = findViewById(R.id.circuloAve);
        circuloPez = findViewById(R.id.circuloPez);
        circuloRoedor = findViewById(R.id.circuloRoedor);
        circuloExotico = findViewById(R.id.circuloExotico);



        layProducto = (LinearLayout) findViewById(R.id.lay_producto);
        layServicio = (LinearLayout) findViewById(R.id.lay_servicio);
        layHoraUno = (LinearLayout) findViewById(R.id.layHorarioUno);
        layHoraDos = (LinearLayout) findViewById(R.id.layHorarioDos);
        layDuracion  = (LinearLayout) findViewById(R.id.layDuracion);


        if (savedInstanceState != null  &&  modelc.publicaciones.size() == 0) {

            Log.i("SIG","DETALLE Publicacion  num=" + modelc.publicaciones.size() );

            reinicio = true;
            nombre.setText("");
            descripcion.setText("");
            valor.setText(Utility.convertToMoney(0));
            idPublicacion = savedInstanceState.getString("IDPUBLICACION");
            cantidad.setText(savedInstanceState.getString("CANTIDAD"));
            cantidadPedida = Integer.parseInt(cantidad.getText().toString());

            modelc.reiniciarCarga(idPublicacion, new OnModelClienteListener() {
                @Override
                public void terminoPrecarga() {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pintarVista(idPublicacion);

                        }
                    });

                }
            });
            return;

        }


        idPublicacion = getIntent().getStringExtra("IDPUBLICACION");


        pintarVista(idPublicacion);



    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString("IDPUBLICACION", idPublicacion);
        outState.putString("CANTIDAD", cantidad.getText().toString());


        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }




    private void pintarVista(String idPublicacion){

        publi = modelc.getPublicacionById(idPublicacion);


        pageAdapter = new PublicacionPageAdapter(getBaseContext(),this,publi);
        pager.setAdapter(pageAdapter);
        TabLayout tabpuntos = (TabLayout) findViewById(R.id.tab_puntos);
        tabpuntos.setupWithViewPager(pager, true);


        nombre.setText(publi.nombre);
        descripcion.setText(publi.descripcion);
        valor.setText(Utility.convertToMoney(publi.precio));



        new ComandoPreguntas(new OnComandoPreguntasListener() {
            @Override
            public void cargoPregunta(String idPublicacion) {
                pintarPreguntasExistentes();

            }
        }).getPreguntasByIdPublicacion(publi.id);


        if (!publi.esParaPerro()){
            circuloPerro.setVisibility(View.GONE);
        }

        if (!publi.esParaGato()){
            circuloGato.setVisibility(View.GONE);
        }


        if (!publi.esParaAve()){
            circuloAve.setVisibility(View.GONE);
        }

        if (!publi.esParaPez()){
            circuloPez.setVisibility(View.GONE);
        }

        if (!publi.esParaRoedor()){
            circuloRoedor.setVisibility(View.GONE);
        }

        if (!publi.esParaExotico()){
            circuloExotico.setVisibility(View.GONE);
        }




        if (publi.servicio == false) {               // Esto es un producto
            if (publi.stock <= 0 ){
                diponible.setText("Inventario agotado");
                diponible.setTextColor(getResources().getColor(R.color.colorRojo));
                diponible.setTextSize(13);
            }
            else{
                diponible.setText(publi.stock+"");
            }

            layServicio.setVisibility(View.GONE);
            layDuracion.setVisibility(View.GONE);

        }

        else {                                       //Esto es un servicio
            textoBotonPagar.setText("Agendar y Comprar");
            pregunta.setText("¿Tienes dudas acerca de este servicio?");
            duracion.setText(publi.duracion+"");
            duracionMedida.setText(publi.duracionMedida);

            layProducto.setVisibility(View.GONE);
            carrito.setVisibility(View.GONE);
            if (publi.horarios.size() > 0 ) {
                Horario hUno = publi.horarios.get(0);
                diasUno.setText(hUno.dias);
                horaUno.setText(hUno.horaInicio + " : " + hUno.horaCierre );
                if (hUno.sinJornadaContinua) {
                    jornadaUno.setText("Cerramos entre 12 y 2:00 PM");
                }

            }
            if (publi.horarios.size() > 1) {
                Horario hDos = publi.horarios.get(1);
                diasDos.setText(hDos.dias);
                horaDos.setText(hDos.horaInicio + " : " + hDos.horaCierre );
                if (hDos.sinJornadaContinua) {
                    jornadaDos.setText("Cerramos entre 12 y 2:00 PM");
                }
            }

            if (publi.horarios.size() == 1) {
                layHoraDos.setVisibility(View.GONE);
            }

        }



        if (modelc.estaPublicacionEncarrito(idPublicacion)) {
            carrito.setText("Eliminar del carrito");
        } else {
            carrito.setText("Eliminar del carrito");
        }


        pintarEstrellas(publi.getCalificacion(new OnComandoCalificacionChangeListener() {
            @Override
            public void cargoCalificaciones() {
                pintarEstrellas(publi.getCalificacion(null));
            }
        }));


        pintarCarrito();
        pintarFavorito();
        pintarPreguntasExistentes();


    }



    private void pintarCarrito(){
        if (modelc.estaPublicacionEncarrito(idPublicacion)) {
            SpannableString content = new SpannableString("Eliminar del carrito");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            carrito.setText(content);
        } else {
            SpannableString content = new SpannableString("Añadir al carrito");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            carrito.setText(content);
        }

    }


    private void pintarFavorito(){
        if (modelc.estaPublicacionEnFavoritos(idPublicacion)) {
            favorito.setImageResource(R.drawable.btnfavorito);
        } else {
            favorito.setImageResource(R.drawable.btnnofavorito);
        }

    }


    public void didTapMenosUno(View v) {
        cantidadPedida--;
        if (cantidadPedida < 1) {
            cantidadPedida = 1;
        }
        cantidad.setText(cantidadPedida+"");

    }

    public void didTapMasUno(View v) {
        cantidadPedida++;
        cantidad.setText(cantidadPedida+"");
    }



    private void pintarEstrellas(double calificacion){
        pintarApagarPrenderEstrella(star1,1,calificacion);
        pintarApagarPrenderEstrella(star2,2,calificacion);
        pintarApagarPrenderEstrella(star3,3,calificacion);
        pintarApagarPrenderEstrella(star4,4,calificacion);
        pintarApagarPrenderEstrella(star5,5,calificacion);
    }


    private void pintarApagarPrenderEstrella(ImageView star, double pos, double nota){

        if (nota >= pos) {
            star.setImageResource(R.drawable.imgestrellacompleta);
            return;
        }

        if (Math.abs(nota-pos) <= 0.5) {
            star.setImageResource(R.drawable.imgestrellamedia);
            return;
        }

        star.setImageResource(R.drawable.imgestrellavacia);


    }


    @Override
    public void cargoImagen(String tipo) {

        if (tipo.equals("PUBLICACION")) {

            pageAdapter.notifyDataSetChanged();
        }
    }


    public void didTapComprar(View v){


        MiniTarjeta mini = modelc.getTarjetaActiva();

        if (mini != null && mini.estado.equals("PENDING") ){
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(DetallePublicacion.this);
            builder.setTitle("Tarjeta pendiente");
            builder.setMessage("La tarjeta inscrita está en proceso de validación por parte de PayU. Espera una notificación o valida más tarde su estado.");

            // add the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int j) {

                    return;

                }
            });



            // create and show the alert dialog
            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();
            return;

        }




        modelc.compraActual.cantidad = cantidadPedida;
        modelc.compraActual.idPublicacion =  publi.id;

        if (modelc.compraActual.getValor() < modelc.params.montoMinimoPedido){

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    DetallePublicacion.this);

            // set title
            alertDialogBuilder.setTitle("Advertencia");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Para realizar una compra debes tener un pedido minimo de " +   Utility.convertToMoney(modelc.params.montoMinimoPedido))
                    .setCancelable(false)
                    .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.cancel();
                            return;
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
            return;

        }



        if (modelc.tipoLogeo.equals("anonimo")){
            Intent i = new Intent(getApplicationContext(), ClienteRegistro.class);
            startActivity(i);
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            return;
        }




        if (publi.servicio) {
            Intent i = new Intent(getApplicationContext(), AgendarServicio.class);
            startActivity(i);
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

        }
        else {
            Intent i = new Intent(getApplicationContext(), ConfirmacionCompra.class);
            startActivity(i);
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);


        }


    }


    public void didTapCarrito(View v){

        if (modelc.estaPublicacionEncarrito(idPublicacion)) {
                modelc.eliminarPublicacionDelCarrito(idPublicacion);
        } else {

            ItemCarrito item = new ItemCarrito();
            item.idPublicacion = idPublicacion;
            item.cantidadCompra = cantidadPedida;
            item.servicio = false;

            modelc.adicionarItemAlCarrito(item);

        }

        pintarCarrito();

    }

    public void didTapFavorito(View v){

        if (modelc.estaPublicacionEnFavoritos(idPublicacion)) {
            modelc.eliminarPublicacionDeFavoritos(idPublicacion);
        } else {
            modelc.adicionarItemAFavoritos(idPublicacion);
        }

        pintarFavorito();

    }


    private void pintarPreguntasExistentes(){
        ArrayList<Pregunta> preguntas = modelc.getPreguntasByIdPublicacion(publi.id);

        if (preguntas.size() == 1) {
            numeroPreguntas.setText("Ver la pregunta realizada");
        }

        if (preguntas.size() > 1) {
            numeroPreguntas.setText("Ver las " + preguntas.size() + " preguntas realizadas");
        }

    }

    public void didTapPreguntas(View v){
        Intent i = new Intent(getApplicationContext(), ListaPreguntas.class);
        i.putExtra("IDPUBLICACION", idPublicacion);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }


    public void didTapNuevaPregunta(View view){

        if (modelc.tipoLogeo.equals("anonimo")){
            Intent i = new Intent(getApplicationContext(), ClienteRegistro.class);
            startActivity(i);
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            return;
        }

        String idPublicacion = getIntent().getStringExtra("IDPUBLICACION");
        Intent i = new Intent(getApplicationContext(), HacerPregunta.class);
        i.putExtra("IDPUBLICACION", idPublicacion);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }

    public void didTapPerfil(View view) {

        String idPublicacion = getIntent().getStringExtra("IDPUBLICACION");
        Intent i = new Intent(getApplicationContext(), DetalleVendedor.class);
        i.putExtra("IDPUBLICACION", idPublicacion);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }

    public void  didTapVerResena(View v){

        String idPublicacion = getIntent().getStringExtra("IDPUBLICACION");
        Intent i = new Intent(getApplicationContext(), ListaResenas.class);
        i.putExtra("IDPUBLICACION", idPublicacion);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }





}
