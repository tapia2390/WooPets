

package woopets.com.woopets.cliente.Comandos;

import android.util.Log;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.EstadosCompra;
import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.InfoCompraActual;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.Oferente;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Modelo.cliente.Ubicacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.cliente.Comandos.ComandoCompras.OnComandoComprasListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/22/17.
 */

public class ComandoCompras {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    public interface OnComandoComprasListener {

        void cargoNuevaCompra();
        void cargoModificacionCompra();

    }


    private ComandoCompras.OnComandoComprasListener mListener;

    public ComandoCompras(ComandoCompras.OnComandoComprasListener mListener){

        this.mListener = mListener;
    }



    public void getComprasAbiertas(String idCliente){

        DatabaseReference ref = database.getReference("compras/abiertas");


        Query query = ref.orderByChild("idCliente").equalTo(idCliente);
        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {
                modelc.addCompra(readCompra(snap));
                mListener.cargoNuevaCompra();

            }

            @Override
            public void onChildChanged(DataSnapshot snap, String s) {
                modelc.addCompra(readCompra(snap));
                mListener.cargoModificacionCompra();
            }

            @Override
            public void onChildRemoved(DataSnapshot snap) {

            }

            @Override
            public void onChildMoved(DataSnapshot snap, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        query.addChildEventListener(listener);
        modelc.hijosListener.put(query,listener);

    }


    public void getComprasCerradas(String idCliente){
        DatabaseReference ref = database.getReference("compras/cerradas");

        Query query = ref.orderByChild("idCliente").equalTo(idCliente);
        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {
                modelc.addCompra(readCompra(snap));
                mListener.cargoNuevaCompra();

            }

            @Override
            public void onChildChanged(DataSnapshot snap, String s) {
                modelc.addCompra(readCompra(snap));
                mListener.cargoModificacionCompra();
            }

            @Override
            public void onChildRemoved(DataSnapshot snap) {

            }

            @Override
            public void onChildMoved(DataSnapshot snap, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        query.addChildEventListener(listener);
        modelc.hijosListener.put(query,listener);



    }


    public void recharzarCompra(String idCompra, String motivo){

        DatabaseReference ref = database.getReference("compras/abiertas/" + idCompra + "/pedido/1/motivoRechazo");
        ref.setValue(motivo);
        ref = database.getReference("compras/abiertas/" + idCompra + "/pedido/1/fechaRechazo");
        ref.setValue(Utility.getFechaHora());
        ref = database.getReference("compras/abiertas/" + idCompra + "/pedido/1/estado");
        ref.setValue(EstadosCompra.PENDIENTE.toString());

    }



    public void moverCompraACerradas(String idCompra){


        DatabaseReference refEstado = database.getReference("compras/abiertas/" + idCompra + "/pedido/1/estado");
        refEstado.setValue(EstadosCompra.CERRADA.toString());
        DatabaseReference reffecha= database.getReference("compras/abiertas/" + idCompra + "/pedido/1/fechaCierre");
        reffecha.setValue(Utility.getFechaHora());

        DatabaseReference ref = database.getReference("compras/abiertas/" + idCompra);
        DatabaseReference refHistorico = database.getReference("compras/cerradas/" + idCompra);
        moverRegistro(ref,refHistorico);

    }




    public void moverRegistro(final DatabaseReference  fromPath, final DatabaseReference toPath){
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                toPath.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError !=null){
                            Log.d("error","Error al mover registros Cerradas");
                        }else{
                            fromPath.removeValue();
                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.d("moveFirebaseRecord()", "Error intentando mover compra a Cerradas");
            }
        });
    }





    private Compra readCompra(DataSnapshot snap) {

        Compra compra = new Compra();
        compra.id = snap.getKey();

        compra.fecha= (String) snap.child("fecha").getValue();
        compra.idOferente = (String) snap.child("idOferente").getValue();
        compra.metodoPago = (String) snap.child("metodoPago").getValue();
        compra.timestamp = (long) snap.child("timestamp").getValue();
        compra.valor = ((Long) snap.child("valor").getValue()).intValue();

        DataSnapshot pedido = snap.child("pedido/1");

        compra.cantidad = ((Long) pedido.child("cantidad").getValue()).intValue();
        compra.direccion = (String) pedido.child("direccion").getValue();
        compra.estado = (String) pedido.child("estado").getValue();
        if (pedido.hasChild("reprogramada")) {
            compra.reprogramada = (boolean) pedido.child("reprogramada").getValue();
        }

        compra.servicio = (boolean) pedido.child("servicio").getValue();
        compra.idPublicacion = (String) pedido.child("idPublicacion").getValue();

        //De una vez cargamos la publicacion
        agregarPublicacion(compra.idPublicacion);


        if (pedido.hasChild("fechaServicio")) {
            compra.fechaServicio = (String) pedido.child("fechaServicio").getValue();
        }

        if (snap.hasChild("fechaCierre")) {
            compra.fechaCierre = (String) snap.child("fechaCierre").getValue();
        }

        return compra;

    }



    public void agregarPublicacion(String idPublicacion){
        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {

            }

            @Override
            public void cargoPublicacion() {

            }
        }).getPublicacion(idPublicacion);
    }




    public static ComandoCompras.OnComandoComprasListener dummy = new ComandoCompras.OnComandoComprasListener() {


        @Override
        public void cargoNuevaCompra() {

        }

        @Override
        public void cargoModificacionCompra() {

        }


    };





}

    

