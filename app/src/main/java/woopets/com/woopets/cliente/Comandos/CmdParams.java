package woopets.com.woopets.cliente.Comandos;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import woopets.com.woopets.Modelo.Sistema;
import woopets.com.woopets.Modelo.cliente.Params;
import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.cliente.ModelCliente;


/**
 * Created by andres on 3/24/18.
 */

public class CmdParams {


    public static  void getParams(){

        final ModelCliente modelo = ModelCliente.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();


        DatabaseReference ref = database.getReference("params");//ruta path
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelo.params =  snap.getValue(Params.class);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public static  void getDepartamentos(){

        final ModelCliente modelo = ModelCliente.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();


        DatabaseReference ref = database.getReference("listados/colombia");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snap) {

                for (DataSnapshot dep : snap.getChildren()) {
                    String nombreDepartamento = dep.child("departamento").getValue().toString();
                    for (DataSnapshot ciudad : dep.child("ciudades").getChildren()) {
                         modelo.departamentos.put(ciudad.getKey(),nombreDepartamento);
                    }

                }




            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public interface OnSystemListener {

        void estaActualizado();
        void puedeActualizar();
        void debeActualizar();

    }


    public static void getSytem(final Activity act , final OnSystemListener listener){


        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("system/");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {


                Long androideBuildlong = (Long)snap.child("androideBuild").getValue();
                modelo.sistema.setBuild(androideBuildlong.intValue());

                modelo.sistema.setLink(snap.child("androideLink").getValue().toString());
                boolean double1 = (boolean)snap.child("androideUpdateMandatorio").getValue();
                modelo.sistema.setUpdateMandatorio(double1);

                Long  entero2 = (Long) snap.child("androideVersion").getValue();
                modelo.sistema.setVersion(entero2);

                Sistema miSis = tomarDatosSistema(act);

                if (miSis.getBuild() < modelo.sistema.getBuild()) {
                    if (modelo.sistema.getUpdateMandatorio()){
                        listener.debeActualizar();
                    }
                    else{
                        listener.puedeActualizar();
                    }
                } else {
                    listener.estaActualizado();

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {


            }
        });

    }


    public static Sistema tomarDatosSistema(Activity act) {
        Sistema sis = new Sistema();
        try {
            PackageInfo info = act.getPackageManager().getPackageInfo(act.getPackageName(), 0);

            sis.setBuild(Integer.parseInt("" + info.versionCode));

        } catch (Exception e) {
            sis.setBuild(1);
            sis.setVersion(1);
        }

        return sis;
    }



}
