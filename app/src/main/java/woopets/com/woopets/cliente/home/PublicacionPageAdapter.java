
package woopets.com.woopets.cliente.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire;
import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ClienteRegistro;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;
import woopets.com.woopets.cliente.detallecompra.ZoomProducto;

/**
 * Created by andres on 10/24/17.
 */

public class PublicacionPageAdapter  extends PagerAdapter {


    private ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private OnImageFireChange listener;


    private ArrayList<ImageFire> destacadosMedia;
    private ArrayList<ImageFire> destacadosBaja;

    ImageView fotoDestacado;
    String idPublicacion ;

    public PublicacionPageAdapter(Context context, OnImageFireChange listener, Publicacion publi) {
        mContext = context;
        this.listener = listener;
        destacadosMedia = publi.fotosMedia;
        destacadosBaja = publi.fotosBaja;
        idPublicacion = publi.id;

    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {


        final int pos = position;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        FrameLayout layout = (FrameLayout) inflater.inflate(R.layout.layout_destacado_page, container, false);
        fotoDestacado = (ImageView) layout.findViewById(R.id.foto);

        Bitmap foto = destacadosBaja.get(position).getFoto("PUBLICACION");
        if (foto!= null) {
            fotoDestacado.setImageBitmap(foto);
        }

        Bitmap foto2 = destacadosMedia.get(position).getFoto("PUBLICACION");
        destacadosMedia.get(position).setListener(listener);

        if (foto2 != null) {
            fotoDestacado.setImageBitmap(foto2);

            fotoDestacado.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                        Intent i = new Intent(mContext, ZoomProducto.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("IDPUBLICACION", idPublicacion);
                        i.putExtra("POS",pos);
                        mContext.startActivity(i);

                }
            });

        }


        container.addView(layout);

        return layout;


    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return destacadosMedia.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}
