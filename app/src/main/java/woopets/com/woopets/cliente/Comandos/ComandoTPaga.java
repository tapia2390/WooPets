package woopets.com.woopets.cliente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.TPaga;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.cliente.ModelCliente;


/**
 * Created by andres on 11/19/17.
 */


public class ComandoTPaga {

    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");



    public interface OnComandoTPagaListener {



    }


    private OnComandoTPagaListener mListener;


    public ComandoTPaga(OnComandoTPagaListener mListener){

        this.mListener = mListener;
    }


    


    public void actualizarDatosTPaga(TPaga tpaga) {

        Cliente cli = modelc.cliente;

        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/datosTpaga");

        Map<String, Object> mapa = new HashMap<String, Object>();


        mapa.put("nombres",tpaga.nombres);
        mapa.put("apellidos",tpaga.apellidos);
        mapa.put("telefono",tpaga.telefono);
        mapa.put("correo",tpaga.correo);
        mapa.put("idClienteTpaga",tpaga.idClienteTpaga);

        ref.updateChildren(mapa);


    }

    public void updateCuotas(String idTarjeta, int cuotas){
        Cliente cli = modelc.cliente;

        if (idTarjeta.equals("")){
            return;
        }

        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/tarjetas/" + idTarjeta + "/cuotas");
        ref.setValue(cuotas);

    }

    public void updateCuotasOferente(String idTarjeta, int cuotas){

        if (idTarjeta.equals("")){
            return;
        }

        DatabaseReference ref = database.getReference("oferentes/"+ modelo.uid + "/tarjetas/" + idTarjeta + "/cuotas");
        ref.setValue(cuotas);

    }


    public void desactivarTarjeta(String idTarjeta){
        Cliente cli = modelc.cliente;

        if (idTarjeta.equals("")){
            return;
        }

        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/tarjetas/" + idTarjeta + "/activo");
        ref.setValue(false);

    }

    public void desactivarTarjetaOferente(String idTarjeta){

        if (idTarjeta.equals("")){
            return;
        }

        DatabaseReference ref = database.getReference("oferentes/"+modelo.uid+"/tarjetas/" + idTarjeta + "/activo");
        ref.setValue(false);

    }


    public void setRegistroCredidCard(final MiniTarjeta mini){

        Cliente cli = modelc.cliente;


        final DatabaseReference ref = database.getReference("clientes/"+cli.id + "/tarjetas/" + mini.id + "/" );

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("activo",true);
        map.put("cuotas", mini.cuotas);
        map.put("franquicia", "" + mini.franquicia);


        if (mini.estado.equals("APPROVED")) {
            //map.put("token", ""+ mini.token);
            //map.put("lastFour", ""+ mini.lastFour);
            //map.put("expirationDate", "");
            //map.put("numero", "");
        }

        if (mini.estado.equals("PENDING")) {
            //map.put("lastFour", "");
            //map.put("expirationDate", mini.expirationDate);
            map.put("numero", mini.numero);
        }

        if (mini.estado.equals("DECLINED")) {
            map.put("token", "");
            //map.put("lastFour", "");
            //map.put("expirationDate", mini.expirationDate);
            map.put("numero", mini.numero);
            map.put("activo",false);
        }



        map.put("nombre", ""+mini.nombre);
        map.put("cedula", ""+mini.cedula);
        map.put("ciudad", ""+mini.ciudad);
        map.put("direccion", ""+mini.direccion);
        map.put("departamento", ""+mini.departamento);
        map.put("correo", ""+mini.correo);
        map.put("telefono", ""+mini.telefono);
        map.put("estado", ""+mini.estado);


        ref.updateChildren(map);


    }




    public void setRegistroCredidCardOferente(final MiniTarjeta mini){


        Modelo modelo = Modelo.getInstance();


        final DatabaseReference ref = database.getReference("oferentes/"+modelo.uid + "/tarjetas/" + mini.id + "/" );

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("activo",true);
        map.put("cuotas", mini.cuotas);
        map.put("franquicia", "" + mini.franquicia);

        if (mini.estado.equals("APPROVED")) {
            map.put("token", ""+mini.token);
            map.put("lastFour", ""+mini.lastFour);
        }

        if (mini.estado.equals("PENDING")) {
            map.put("token", "");
            map.put("lastFour", "");
            map.put("fechaExpiracion", mini.expirationDate);
        }


        map.put("nombre", ""+mini.nombre);
        map.put("cedula", ""+mini.cedula);
        map.put("ciudad", ""+mini.ciudad);
        map.put("direccion", ""+mini.direccion);
        map.put("departamento", ""+mini.departamento);
        map.put("correo", ""+mini.correo);
        map.put("telefono", ""+mini.telefono);
        map.put("estado", ""+mini.estado);


        ref.setValue(map);


    }


 

    


    public String getLLave(){

        return  referencia.push().getKey();
    }

    


    public static OnComandoTPagaListener  dummy = new OnComandoTPagaListener() {

 
    };






}

