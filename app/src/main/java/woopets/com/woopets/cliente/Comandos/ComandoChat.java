package woopets.com.woopets.cliente.Comandos;


import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.Chat;
import woopets.com.woopets.Modelo.cliente.Mensaje;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/25/17.
 */

public class ComandoChat {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    public interface OnComandoChatListener {

        void cargoChat(String idPublicacion);

    }


    private ComandoChat.OnComandoChatListener mListener;

    public ComandoChat(OnComandoChatListener mListener){

        this.mListener = mListener;
    }


    private Chat readChat(DataSnapshot snap) {
        Chat chat = snap.getValue(Chat.class);
        chat.id = snap.getKey();
        return chat;
    }



    public void getChatsByIdCompra(final String idCompra){

        DatabaseReference ref = database.getReference("chats");

        Query query = ref.orderByChild("idCompra").equalTo(idCompra);

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {


                for (DataSnapshot snapChat : snap.getChildren()) {
                    Chat chat = readChat(snapChat);
                    modelc.addChat(chat);
                }

                mListener.cargoChat(idCompra);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        };

        query.addValueEventListener(listener);
        modelc.hijosListenerValue.put(query,listener);

    }


    public void marcarComoLeido(String idChat){

        DatabaseReference ref = database.getReference("chats/" + idChat + "/visto");
        ref.setValue(true);


    }

    public String getLlave(){

        DatabaseReference ref = database.getReference("Chats/");
        return ref.push().getKey();
    }



    public void ponerChat(Chat chat) {

        DatabaseReference ref = database.getReference("chats/" + chat.id);

        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("emisor", chat.emisor);
        mapa.put("fechaMensaje",  Utility.getFechaHora());
        mapa.put("idCompra", chat.idCompra);
        mapa.put("mensaje", chat.mensaje);
        mapa.put("receptor", chat.receptor);
        mapa.put("timestamp", ServerValue.TIMESTAMP);
        mapa.put("visto", false);
        ref.setValue(mapa);

    }



    public void enviarPushNotification(final Mensaje msg){


        DatabaseReference ref = database.getReference("oferentes/" + msg.idOferente + "/tokenDevice");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                String tokenDevice = (String) snap.getValue();
                DatabaseReference ref = database.getReference("mensajes/" + referencia.push().getKey());
                ref.setValue(armarMensaje(msg, tokenDevice));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private Map<String, Object> armarMensaje(Mensaje msg, String tokenDevice){

        Map<String, Object> mapToken = new HashMap<String, Object>();
        mapToken.put(tokenDevice,true);

        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("visto", false);
        mapa.put("titulo",msg.titulo);
        mapa.put("tipo", msg.tipo);
        mapa.put("timestamp", ServerValue.TIMESTAMP);
        mapa.put("mensaje", msg.mensaje);
        mapa.put("infoAdicional", msg.infoAdicional);
        mapa.put("idOferente", msg.idOferente);

        if (!msg.idCompra.equals("")){
            mapa.put("idCompra", msg.idCompra);
        }

        if (!msg.idPublicacion.equals("")){
            mapa.put("idPublicacion", msg.idPublicacion);
        }

        mapa.put("tokens", mapToken);

        return mapa;

    }




    public static ComandoChat.OnComandoChatListener dummy = new ComandoChat.OnComandoChatListener() {


        @Override
        public void cargoChat(String idPublicacion) {

        }



    };



}







