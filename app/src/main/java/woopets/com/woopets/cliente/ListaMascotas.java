package woopets.com.woopets.cliente;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.firebase.storage.UploadTask.TaskSnapshot;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ListaMacotasAdapter.CrearMascotaAdapterCallback;

public class ListaMascotas extends Activity implements OnImageFireChange, CrearMascotaAdapterCallback {

    private ListView mListView;
    private ListaMacotasAdapter mAdapter;


    String userChoosenTask="";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Uri uriSavedImage1;

    ModelCliente modelc = ModelCliente.getInstance();

    Bitmap original;

    private ProgressDialog progressDialog;

    int indexMascota = -1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_mascotas);

        progressDialog = new ProgressDialog(this);

        mAdapter = new ListaMacotasAdapter(this, this, this);
        mListView = (ListView) findViewById(R.id.mascota_list_view);

        mListView.setAdapter(mAdapter);


    }


    public void didTapNuevaMascota(View v) {

        Intent i = new Intent(getApplicationContext(), CrearMascota.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        finish();

    }



    @Override
    public void cargoImagen(String tipo) {

            mAdapter.notifyDataSetChanged();

    }

    @Override
    public void cambiarFoto(int pos) {

        indexMascota = pos;
        selectImage();

    }



    public void subirFoto(){

        progressDialog.setMessage("Cargando la información Por favor, espere...");
        progressDialog.show();

        Mascota mascota = modelc.mascotas.get(indexMascota);

        if (original != null ) {
            mascota.imageFire.setFoto(original);
            uploadImage(mascota.id);
        }

    }




    private void selectImage() {

        final CharSequence[] items = { "Tomar foto","Selecionar imagen","Cancelar" };

        AlertDialog.Builder builder = new AlertDialog.Builder(ListaMascotas.this);
        builder.setTitle("Seleccionar foto");
        AlertDialog.Builder cancelar = builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                boolean result = Utility.checkPermission(ListaMascotas.this);
                if (items[item].equals("Tomar foto")) {
                    userChoosenTask = "Take Photo";
                    if (result) {
                        cameraIntent();
                    }


                } else if (items[item].equals("Selecionar imagen")) {
                    userChoosenTask = "Choose from Library";
                    if (result){
                        galleryIntent();
                    }

                } else if (items[item].equals("Cancelar")) {
                    dialog.dismiss();
                }
            }


        });
        builder.show();
    }


    //permiso camara v6
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }


    /// manejo de camara
    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }


    private void galleryIntent()
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent,SELECT_FILE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        }
    }


    //galeri
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;

        File destination = null;


        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                original = bm;
                uriSavedImage1 = Uri.fromFile(destination);
                subirFoto();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    //camara
    private void onCaptureImageResult(Intent data) {
        Bitmap bm=null;
        File destination = null;


        if (data != null) {
            try {
                bm = (Bitmap) data.getExtras().get("data");
                destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");


                original = bm;
                uriSavedImage1 = Uri.fromFile(destination);

                subirFoto();

            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

    }



    private void uploadImage(String idMascota) {


        FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference storageReference = storage.getReference().child("mascotas/" + modelc.cliente.id + "/" + idMascota + "/FotoTuMascota.jpg");


        //Bitmap bitmap = Bitmap.createBitmap(foto.getDrawingCache());
        Bitmap bitmap = original;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        byte[] data = outputStream.toByteArray();


        UploadTask uploadTask = storageReference.putBytes(data);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(getApplicationContext(), "Error, no pudo subir la foto. Tiene acceso a Internet", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                mAdapter.notifyDataSetChanged();
            }
        }).addOnSuccessListener(new OnSuccessListener<TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                mAdapter.notifyDataSetChanged();

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        Intent i = new Intent(getApplicationContext(), HomeCliente.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }
}
