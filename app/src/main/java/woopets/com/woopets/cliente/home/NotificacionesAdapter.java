package woopets.com.woopets.cliente.home;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.text.Html.ImageGetter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.Item;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Notificacion;
import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.carrito.CarritoAdapter;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;
import woopets.com.woopets.cliente.detallecompra.ListaPreguntas;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * Created by andres on 10/31/17.
 */

public class NotificacionesAdapter  extends BaseAdapter {


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private final LayoutInflater mInflater;



    public NotificacionesAdapter(Context c) {
        mContext = c;
        mInflater = LayoutInflater.from(c);

    }

    @Override
    public int getCount() {
        return modelc.notificaciones.size();
    }

    @Override
    public Object getItem(int i) {
        return modelc.notificaciones.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {


        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_notificaciones,parent, false);
            holder = new ViewHolder();
            holder.btnBorrar = (FrameLayout) convertView.findViewById(R.id.borrar);
            holder.tipo = (TextView) convertView.findViewById(R.id.tipo);
            holder.nombre = (TextView) convertView.findViewById(R.id.nombre);
            holder.senal = (ImageView) convertView.findViewById(R.id.senal);
            holder.foto = (ImageView) convertView.findViewById(R.id.foto);
            holder.frameSenal = (FrameLayout) convertView.findViewById(R.id.frameSenal);
            holder.general = (LinearLayout) convertView.findViewById(R.id.layoutGeneral);
            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }


        TextView tipo = holder.tipo;
        final TextView nombre = holder.nombre;
        final ImageView senal = holder.senal;
        final ImageView foto = holder.foto;

        FrameLayout borrar = holder.btnBorrar;
        FrameLayout frameSenal = holder.frameSenal;
        LinearLayout general = holder.general;

        final Publicacion publi;
        final Notificacion noti = modelc.notificaciones.get(i);
        Compra compra = modelc.getCompraById(noti.idCompra);
        if (compra == null){
            publi = modelc.getPublicacionById(noti.idPublicacion);
        }else {
            publi = modelc.getPublicacionById(compra.idPublicacion);

        }



        if (noti.visto) {
            senal.setVisibility(View.GONE);
        }
        else {
            senal.setVisibility(View.VISIBLE);
        }


        if (noti.titulo.equals("Pregunta respondida")){
            foto.setImageResource(R.drawable.imgpreguntanotificaciones);

        } else {
            foto.setImageResource(R.drawable.imgventa);
        }

        if (noti.tipo.startsWith("Cliente") ){
            foto.setImageResource(R.drawable.imgtarjetanotificaciones);
            nombre.setText(noti.mensaje);
        }


        borrar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                modelc.eliminarNotificacion(noti.id);
                NotificacionesAdapter.this.notifyDataSetChanged();
            }
        });

        general.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!noti.visto){
                    senal.setVisibility(View.GONE);
                    modelc.marcarNotificacionComoVisto(noti.id);
                }

                if (noti.tipo.startsWith("Cliente") ){
                    return;
                }

                if (noti.titulo.equals("Pregunta respondida")){

                    Intent i = new Intent(getApplicationContext(),ListaPreguntas.class);
                    i.putExtra("IDPUBLICACION",publi.id);
                    mContext.startActivity(i);

                } else {

                    Intent i = new Intent(getApplicationContext(),HomeCliente.class);
                    i.putExtra("TAB","CARRITO");
                    mContext.startActivity(i);
                }

            }
        });


        tipo.setText(noti.titulo);

        if (publi == null) {
            return  convertView;
        }

        nombre.setText(publi.nombre);

        return convertView;

    }


    private static class ViewHolder {

        public FrameLayout btnBorrar;
        public FrameLayout frameSenal;
        public TextView tipo;
        public TextView nombre;
        public ImageView senal;
        public ImageView foto;
        public LinearLayout general;


    }


}

