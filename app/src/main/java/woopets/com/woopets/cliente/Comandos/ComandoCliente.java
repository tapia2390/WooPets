package woopets.com.woopets.cliente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Alarma;
import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.TPaga;
import woopets.com.woopets.Modelo.cliente.Ubicacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.ModelCliente;


/**
 * Created by andres on 10/1/17.
 */

public class ComandoCliente {

    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");



    public interface OnComandoClienteChangeListener {

        void cargoCliente();
        void clienteNoExiste();

        //void errorInformacionRegistroCliente();

    }


    private ComandoCliente.OnComandoClienteChangeListener mListener;

    public ComandoCliente(OnComandoClienteChangeListener mListener){

        this.mListener = mListener;
    }




    public void getCliente(final String uid){

        if(uid.equals("")){
            return;
        }

        DatabaseReference ref = database.getReference();

        ref = database.getReference("clientes/"+uid);


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {


                Cliente usu = new Cliente();
                usu.id = uid;
                if (snap.child("correo").exists()) {
                    usu.correo = snap.child("correo").getValue().toString();
                }
                modelc.cliente = usu;


               ///Los usuarios anonimos pueden tener carrito, favoritos y mascotas.
                if (snap.child("carrito").getValue() != null) {
                    DataSnapshot snapCar = snap.child("carrito");
                    for (DataSnapshot carro : snapCar.getChildren()) {
                        ItemCarrito item = readCarrito(carro);
                        modelc.addCarrito(item);
                        agregarPublicacion(item.idPublicacion);

                    }
                }

                if (snap.child("favoritos").getValue() != null) {
                    DataSnapshot snapCar = snap.child("favoritos");
                    for (DataSnapshot fav : snapCar.getChildren()) {
                        modelc.addFavorito(fav.getKey());
                        agregarPublicacion(fav.getKey());
                    }

                }

                if(snap.child("mascotas").getValue() != null){
                    DataSnapshot snapMas = snap.child("mascotas");

                    modelc.mascotas.clear();

                    for (DataSnapshot dir : snapMas.getChildren()){

                        Mascota mascota = new Mascota(uid, dir.getKey());

                        if (!dir.child("genero").exists()){
                            continue;
                        }

                        mascota.genero = dir.child("genero").getValue().toString();
                        mascota.id = dir.getKey();
                        mascota.nombre = dir.child("nombre").getValue().toString();
                        mascota.raza = dir.child("raza").getValue().toString();
                        mascota.tipo = dir.child("tipo").getValue().toString();
                        mascota.activa = (boolean)dir.child("activa").getValue();

                        try {
                            Date f = df.parse(dir.child("fechaNacimiento").getValue().toString());
                            mascota.fechaNacimiento = f;

                        } catch (Exception e) {
                            mascota.fechaNacimiento = new Date();
                        }

                        modelc.mascotas.add(mascota);

                        if(dir.child("alertas").getValue() != null){
                            DataSnapshot snapAlerta = dir.child("alertas");

                            for (DataSnapshot alerta : snapAlerta.getChildren()){

                                Alarma ala = alerta.getValue(Alarma.class);
                                mascota.alarmas.add(ala);

                            }

                        }

                    }

                }


                if(snap.child("nombre").getValue() == null) {

                    mListener.cargoCliente();
                    return;
                }

                usu.nombre = snap.child("nombre").getValue().toString();
                if (snap.child("apellido").exists()) {
                    usu.apellido = snap.child("apellido").getValue().toString();
                }
                if (snap.hasChild("metodoDePago")) {
                    usu.metodoPago = snap.child("metodoDePago").getValue().toString();
                }

                if (snap.hasChild("documento")){
                    usu.cedula = snap.child("documento").getValue().toString();
                }

                if (snap.child("celular").exists()) {
                    usu.celular = snap.child("celular").getValue().toString();
                }

                if (snap.child("tokenDevice").exists()) {
                    usu.tokenDevice = snap.child("tokenDevice").getValue().toString();
                }

                if(snap.child("direcciones").getValue() != null){
                    DataSnapshot snapDir = snap.child("direcciones");

                    for (DataSnapshot dir : snapDir.getChildren()){

                        Direccion direccion = new Direccion();

                        direccion.direccion = dir.child("direccion").getValue().toString();
                        direccion.id = dir.getKey();
                        direccion.nombre = dir.child("nombre").getValue().toString();
                        direccion.porDefecto = (boolean)dir.child("porDefecto").getValue();
                        direccion.posicion = ((Long) dir.child("posicion").getValue()).intValue();

                        if(dir.child("ubicacion").getValue() != null){
                            DataSnapshot snapUbi = dir.child("ubicacion");
                            Ubicacion ubi = new Ubicacion();
                            ubi.latitud = (Double) snapUbi.child("lat").getValue();
                            ubi.longitud = (Double) snapUbi.child("lon").getValue();
                            direccion.ubicacion = ubi;
                        }
                        modelc.direcciones.add(direccion);

                    }

                }


                if (snap.child("tarjetas").getValue() != null) {
                    DataSnapshot snapTar = snap.child("tarjetas");
                    for (DataSnapshot tar : snapTar.getChildren()) {
                        MiniTarjeta mini = tar.getValue(MiniTarjeta.class);
                        mini.id = tar.getKey();
                        modelc.tarjetas.add(mini);
                    }
                }


                if (snap.child("datosTpaga").getValue() != null) {
                    DataSnapshot snapCar = snap.child("datosTpaga");
                    modelc.tpaga = snapCar.getValue(TPaga.class);
                    getLlavesTPaga();

                }




                mListener.cargoCliente();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void agregarPublicacion(String idPublicacion){
        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {

            }

            @Override
            public void cargoPublicacion() {

            }
        }).getPublicacion(idPublicacion);
    }



    public ItemCarrito readCarrito(DataSnapshot snap){

        ItemCarrito item = snap.getValue(ItemCarrito.class);
        item.id = snap.getKey();
        return item;

    }


    public  void updateMetodoPago(String metodo){

        Cliente cli = modelc.cliente;
        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/metodoDePago");
        ref.setValue(metodo);


    }


    public void crearMascota(Mascota newMascota) {

        Cliente cli = modelc.cliente;

        if (newMascota.nombre.equals("")){
            return;
        }

        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/mascotas/"+newMascota.id);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Map<String, Object> mascota = new HashMap<String, Object>();

        mascota.put("activa", true);
        mascota.put("fechaNacimiento", dateFormat.format(newMascota.fechaNacimiento));
        mascota.put("foto","FotoTuMascota");
        mascota.put("genero",newMascota.genero);
        mascota.put("nombre",newMascota.nombre);
        mascota.put("genero",newMascota.genero);
        mascota.put("raza", newMascota.raza);
        mascota.put("tipo", newMascota.tipo);

        ref.setValue(mascota);

    }

    public void updateMascota(Mascota actualMascota) {

        Cliente cli = modelc.cliente;

        if (actualMascota.nombre.equals("")){
            return;
        }

        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/mascotas/"+actualMascota.id);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Map<String, Object> mascota = new HashMap<String, Object>();

        mascota.put("activa", true);
        mascota.put("fechaNacimiento", dateFormat.format(actualMascota.fechaNacimiento));
        mascota.put("foto","FotoTuMascota");
        mascota.put("genero",actualMascota.genero);
        mascota.put("nombre",actualMascota.nombre);
        mascota.put("genero",actualMascota.genero);
        mascota.put("raza", actualMascota.raza);
        mascota.put("tipo", actualMascota.tipo);

        ref.updateChildren(mascota);

    }


    public void activarMascota(String idMascota, String idCliente){

        for (Mascota masc:modelc.mascotas){
            masc.activa = false;
            if (masc.id.equals(idMascota)){
                masc.activa = true;
            }

            DatabaseReference ref = database.getReference("clientes/"+idCliente+"/mascotas/"+masc.id+"/activa");
            ref.setValue(false);

        }

        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/mascotas/"+idMascota+"/activa");
        ref.setValue(true);

    }

    public void activarDireccion(String idDireccion, String idCliente){

        for (Direccion dir:modelc.direcciones){
            dir.porDefecto = false;
            if (dir.id.equals(idDireccion)){
                dir.porDefecto = true;
            }

            DatabaseReference ref = database.getReference("clientes/"+idCliente+"/direcciones/"+dir.id+"/porDefecto");
            ref.setValue(false);

        }

        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/direcciones/"+idDireccion+"/porDefecto");
        ref.setValue(true);

    }





    public void eliminarMascota(String idMascota, String idCliente) {

        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/mascotas/"+idMascota);
        ref.removeValue();
    }


    public void eliminarDireccion(String idDireccion, String idCliente) {

        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/direcciones/"+idDireccion);
        ref.removeValue();
    }




    public String getLLave(){

        return  referencia.push().getKey();
    }

    public void adicionarAlCarrito(ItemCarrito item) {

        Cliente cli = modelc.cliente;


        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/carrito/"+item.id);

        Map<String, Object> mapa = new HashMap<String, Object>();

        mapa.put("cantidadCompra", item.cantidadCompra);
        mapa.put("idPublicacion", item.idPublicacion);
        mapa.put("servicio", false);

        ref.setValue(mapa);

    }

    public void eliminarDelCarrito(String idCarrito) {

        Cliente cli = modelc.cliente;


        DatabaseReference ref = database.getReference("clientes/" + cli.id + "/carrito/" + idCarrito);


        ref.removeValue();

    }

    public void adicionarAFavoritos(String idPublicacion) {

        Cliente cli = modelc.cliente;

        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/favoritos/"+idPublicacion);

        ref.setValue(true);

    }


    public void eliminarDeFavoritos(String idPublicacion) {

        Cliente cli = modelc.cliente;

        DatabaseReference ref = database.getReference("clientes/" + cli.id + "/favoritos/" + idPublicacion);

        ref.removeValue();

    }

    public static OnComandoClienteChangeListener  dummy = new OnComandoClienteChangeListener() {

        @Override
        public void cargoCliente()
        {}

        @Override
        public void clienteNoExiste() {

        }

    };


    private void getLlavesTPaga(){

        DatabaseReference ref = database.getReference();

        ref = database.getReference("params");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelc.tpaga.endPoint = (String) snap.child("tpendpoint").getValue();
                modelc.tpaga.publicKey = (String) snap.child("tppublica").getValue();
                modelc.tpaga.privateKey = (String) snap.child("tpprivada").getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }



}
