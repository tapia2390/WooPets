package woopets.com.woopets.cliente.busquedas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ListaMacotasAdapter.CrearMascotaAdapterCallback;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.carrito.FavoritosAdapter;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;

/**
 * Created by andres on 1/2/18.
 */

public class BuscarGeneralAdapter extends BaseAdapter implements Filterable {


    private Context mContext;
    private LayoutInflater mInflater;
    private OnImageFireChange listener;
    ModelCliente modelc = ModelCliente.getInstance();
    public ArrayList<Publicacion> filtro = new ArrayList<>();
    public ArrayList<Publicacion> datos = new ArrayList<>();
    ValueFilter valueFilter;



    public BuscarGeneralAdapter(Context context, OnImageFireChange listener, ArrayList<Publicacion> publis){

        this.listener = listener;
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        filtro = publis;
        datos = publis;

    }


    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Object getItem(int i) {
        return datos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        convertView = mInflater.inflate(R.layout.item_buscar, parent, false);
        TextView valor = (TextView) convertView.findViewById(R.id.valor);
        ImageView foto = (ImageView) convertView.findViewById(R.id.foto);
        TextView nombre = (TextView) convertView.findViewById(R.id.nombre);
        TextView tipo = (TextView) convertView.findViewById(R.id.tipo);

        LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.linear);

        final Publicacion publi = datos.get(i);


        publi.fotosBaja.get(0).setListener(listener);



        if (publi == null) {
            return  convertView;
        }


        nombre.setText(publi.nombre);
        valor.setText(Utility.convertToMoney(publi.precio));
        tipo.setText(publi.getTipos());


        Bitmap fotoBitmap = publi.fotosBaja.get(0).getFoto("PUBLICACION");

        if (fotoBitmap != null) {
            foto.setImageBitmap(fotoBitmap);
        }

        return convertView;

    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }


    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                List filterList = new ArrayList();
                for (int i = 0; i < filtro.size(); i++) {
                    if ((filtro.get(i).nombre.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(filtro.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = filtro.size();
                results.values = filtro;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            datos = (ArrayList<Publicacion>) results.values;
            modelc.filtro = datos;
            notifyDataSetChanged();
        }

    }


}
