package woopets.com.woopets.cliente.menu;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.text.Html.ImageGetter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.Item;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Notificacion;
import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Modelo.cliente.Solicitud;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.ProponerMascota;
import woopets.com.woopets.cliente.carrito.CarritoAdapter;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;
import woopets.com.woopets.cliente.detallecompra.ListaPreguntas;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * Created by andres on 10/31/17.
 */

public class AyudaAdapter  extends BaseAdapter {


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private final LayoutInflater mInflater;



    public AyudaAdapter(Context c) {
        mContext = c;
        mInflater = LayoutInflater.from(c);

    }

    @Override
    public int getCount() {
        return modelc.solicitudes.size();
    }

    @Override
    public Object getItem(int i) {
        return modelc.solicitudes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {


        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_solicitud,parent, false);
            holder = new ViewHolder();
            holder.tipo = (TextView) convertView.findViewById(R.id.tipo);
            holder.enunciado = (TextView) convertView.findViewById(R.id.enunciado);
            holder.numero = (TextView) convertView.findViewById(R.id.numero);
            holder.fechaSolicitud = (TextView) convertView.findViewById(R.id.fechaSolicitud);
            holder.fechaCierre = (TextView) convertView.findViewById(R.id.fechaCierre);
            holder.estado = (TextView) convertView.findViewById(R.id.estado);
            holder.layerFondo = (LinearLayout) convertView.findViewById(R.id.layerFondo);
            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }


        TextView tipo = holder.tipo;
        TextView enunciado = holder.enunciado;
        TextView numero = holder.numero;
        TextView fechaSolicitud = holder.fechaSolicitud;
        TextView fechaCierre = holder.fechaCierre;
        TextView estado = holder.estado;
        LinearLayout fondo = holder.layerFondo;

        final Solicitud sol =  modelc.solicitudes.get(i);


        tipo.setText(sol.tipoSolicitud);
        enunciado.setText(sol.enunciado);
        numero.setText(sol.numeroSeguimiento);
        fechaSolicitud.setText(sol.fechaGeneracion);
        fechaCierre.setText(sol.fechaCierre);
        estado.setText(sol.estado);

        if (sol.estado.equals("Abierta")) {
            fechaCierre.setVisibility(View.GONE);
        }
        else {
            fechaCierre.setVisibility(View.VISIBLE);
        }


        fondo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AyudaDetalle.class);
                i.putExtra("IDSOLICITUD", sol.id);
                mContext.startActivity(i);
            }
        });



        return convertView;

    }


    private static class ViewHolder {

        public TextView tipo;
        public TextView enunciado;
        public TextView fechaSolicitud;
        public TextView fechaCierre;
        public TextView estado;
        public TextView numero;
        public LinearLayout layerFondo;

    }
}


