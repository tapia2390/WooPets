package woopets.com.woopets.cliente.home;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.busquedas.BuscarGeneral;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FiltroCategoria extends AppCompatActivity implements OnImageFireChange {


    private ListView listaView;


    ModelCliente modelc = ModelCliente.getInstance();
    FiltroCategoriaAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtro_categoria);

        final String categoria = getIntent().getStringExtra("CATEGORIA");

        listaView = (ListView) findViewById(R.id.lista);

        adapter = new FiltroCategoriaAdapter(this, categoria, this, false);
        listaView.setAdapter(adapter);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        Mascota mascota = modelc.getMascotaActiva();
        String nombre = "";

        if (mascota != null){
            nombre = mascota.nombre;
        }


        ImageView buscar = findViewById(R.id.buscar);
        buscar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),BuscarGeneral.class);
                i.putExtra("CATEGORIA", categoria);
                startActivity(i);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

            }
        });



        //Titulo y buscar

        TextView nombreCategoria = (TextView) findViewById(R.id.nombreCategoria);
        ImageView fotoCategoria = (ImageView) findViewById(R.id.foto);

        String titulo = categoria;
        if (categoria.equals("Medicamentos")) {
            titulo = "Higiene y Medicamentos";
        }
        nombreCategoria.setText(titulo);

        switch (categoria) {
            case "Accesorios": fotoCategoria.setImageResource(R.drawable.imgaccesorios);
                break;
            case "Nutrición": fotoCategoria.setImageResource(R.drawable.imgnutricion);
                break;
            case "Lind@ y Limpi@": fotoCategoria.setImageResource(R.drawable.imghigiene);
                break;
            case "Amiguitos en el cielo": fotoCategoria.setImageResource(R.drawable.imgfuneraria);
                break;
            case "Guardería 5 patas": fotoCategoria.setImageResource(R.drawable.imgguarderia);
                break;
            case "Medicamentos": fotoCategoria.setImageResource(R.drawable.imgmedicamentos);
                break;
            case "Paseador": fotoCategoria.setImageResource(R.drawable.imgpaseador);
                break;
            case "Vamos al médico": fotoCategoria.setImageResource(R.drawable.imgsalud);

        }

        /*
        ArrayList<Publicacion> publicacionesSugeridas = new ArrayList<>();

        if (mascota != null){
            publicacionesSugeridas = modelc.getPublicacionesXCategoriaPeroConMascota(categoria,mascota);
        }


        if (publicacionesSugeridas.size() > 0 && !modelc.tipoLogeo.equals("anonimo")){

            // header 1
            LinearLayout tituloView = (LinearLayout) View.inflate(this, R.layout.header_rojo, null);
            TextView texto = (TextView)tituloView.findViewById(R.id.texto);
            texto.setText("Productos sugeridos para " + nombre);
            listaView.addHeaderView(tituloView,null,false);


            /// header 2
            RecyclerView recyclerView;
            RecyclerViewAdapter RecyclerViewHorizontalAdapter;
            LinearLayoutManager HorizontalLayout ;

            recyclerView =  (RecyclerView) View.inflate(this, R.layout.relative_layout_para_barra_hor, null);

            RecyclerViewHorizontalAdapter = new RecyclerViewAdapter(getBaseContext(),categoria);

            HorizontalLayout = new LinearLayoutManager(FiltroCategoria.this, LinearLayoutManager.HORIZONTAL,false);
            recyclerView.setLayoutManager(HorizontalLayout);

            recyclerView.setAdapter(RecyclerViewHorizontalAdapter);

            ViewGroup.LayoutParams params = recyclerView.getLayoutParams();
            if (params != null) {
                params.width= size.x;
                params.height = (int)(size.y * 0.28);
            } else
                params = new ViewGroup.LayoutParams((int)(size.x), (int)(size.y * 0.28));


            recyclerView.setLayoutParams(params);

            listaView.addHeaderView(recyclerView,null,false);

        }*/


        // header 3
        LinearLayout tituloView2 = (LinearLayout) View.inflate(this, R.layout.header_rojo, null);
        TextView texto2 = (TextView)tituloView2.findViewById(R.id.texto);
        texto2.setText("Productos en esta categoria para " + nombre);
        if (modelc.tipoLogeo.equals("anonimo")) {
            texto2.setText("Productos populares en esta categoría");
        }

        listaView.addHeaderView(tituloView2,null,false);

    }


    @Override
    public void cargoImagen(String tipo) {
        adapter.notifyDataSetChanged();

    }
}
