package woopets.com.woopets.cliente.Comandos;

import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.firebase.client.FirebaseError;
import com.firebase.client.ServerValue;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.FirebaseFunctionsException;
import com.google.firebase.functions.HttpsCallableResult;

import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Chat;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.payU.PagoPayU;
import woopets.com.woopets.payU.PendientePayU;

/**
 * Created by andres on 8/13/18.
 */

public class CmdPayU {



    public static void registrarPendiente(PendientePayU pendiente){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("pendientePayU/" + pendiente.id);

        Map<String, Object> nuevoItem = new HashMap<String, Object>();
        nuevoItem.put("state", pendiente.state);
        nuevoItem.put("fechaCreacion", Utility.getFechaHora());
        nuevoItem.put("orderId", pendiente.orderId);
        nuevoItem.put("tokenDevice", pendiente.tokenDevice);
        nuevoItem.put("tipo", pendiente.tipo);
        nuevoItem.put("uid", pendiente.uid);
        nuevoItem.put("timestamp", ServerValue.TIMESTAMP);
        nuevoItem.put("transactionId", pendiente.transactionId);

        ref.setValue(nuevoItem);

    }


    public static void checkMovimientosPayU(){

        ModelCliente modelc = ModelCliente.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("pendientePayU");

        Query query = ref.orderByChild("uid").equalTo(modelc.cliente.id);

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {
                PendientePayU pen = readPendiente(snap);

                if (pen.state.equals("DECLINED")){
                    finalizarRechazo(pen);
                    return;
                }

                if (pen.state.equals("APPROVED")){
                    finalizarReembolsoYToken(pen);
                    return;
                }

            }

            @Override
            public void onChildChanged(DataSnapshot snap, String s) {
                PendientePayU pen = readPendiente(snap);

                if (pen.state.equals("DECLINED")){
                    finalizarRechazo(pen);
                    return;
                }

                if (pen.state.equals("APPROVED")){
                    finalizarReembolsoYToken(pen);
                    return;
                }


            }

            @Override
            public void onChildRemoved(DataSnapshot snap) {


            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        query.addChildEventListener(listener);

        modelc.hijosListener.put(query,listener);



    }


    private static PendientePayU readPendiente(DataSnapshot snap) {
        PendientePayU pen = new PendientePayU();
        pen.state = snap.child("state").getValue().toString();
        pen.uid = snap.getKey();
        //pen.valor = snap.child("state").getValue().toString();
        pen.transactionId = snap.child("transactionId").getValue().toString();
        pen.tokenDevice = snap.child("tokenDevice").getValue().toString();
        pen.tipo = snap.child("tipo").getValue().toString();
        pen.orderId = snap.child("orderId").getValue().toString();
        pen.uid = snap.child("uid").getValue().toString();
        pen.state = snap.child("state").getValue().toString();
        return pen;
    }

    private static MiniTarjeta readTarjetaMini(DataSnapshot snap) {
        MiniTarjeta pen = snap.getValue(MiniTarjeta.class);
        pen.id = snap.getKey();
        return pen;
    }



    public static void finalizarRechazo(PendientePayU pen){

        ModelCliente modelc = ModelCliente.getInstance();
        MiniTarjeta mini = modelc.getTarjetaActiva();


        if (mini == null ){
            return;
        }

        if (mini.estado.equals("DECLINED") || mini.estado.equals("APPROVED")){
            return;
        }

        mini.estado = "DECLINED";
        mini.activo = false;
        new ComandoTPaga(null).setRegistroCredidCard(mini);


        //TODO pendiente borrar  de pendientesPayU
    }


    public interface OnCheckTarjetas {
        void gotError(String error);
        void gotDeclined(String razon);
        void gotPending(MiniTarjeta mini);
        void gotApproved(MiniTarjeta mini);


    }


    public static void checkTarjetasPayU(final String idTarjeta, String quienes, String uid,final OnCheckTarjetas listener){

        final ModelCliente modelc = ModelCliente.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference(quienes + "/" + uid + "/tarjetas"  );


        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snap, @Nullable String s) {
                if (snap.getValue() == null  || !snap.getKey().equals(idTarjeta)){
                    return;
                }



                MiniTarjeta mini = readTarjetaMini(snap);

                if (!mini.activo){
                    return;
                }

                if (mini.estado.equals("DECLINED")){
                    listener.gotDeclined(mini.responseCode);
                    return;
                }

                if (mini.estado.equals("APPROVED")){
                    listener.gotApproved(mini);
                    return;
                }

                if (mini.estado.equals("PENDING")){
                    listener.gotPending(mini);
                    return;
                }

                if (mini.estado.equals("ERROR")){
                    listener.gotError(mini.responseCode);
                    return;
                }


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snap, @Nullable String s) {


            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




    }


    public interface OnCheckPagos {
        void pagoAprobado(PagoPayU pago);
        void pagoReachazado(String razon);
        void pagoPendiente(PagoPayU pago);
        void pagoConError(String razon);


    }

    public static void checkPagos(final String referenceCode, final OnCheckPagos listener){

        ModelCliente modelc = ModelCliente.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("compras/abiertas/" + referenceCode + "/pago"  );


        ref.addValueEventListener(new com.google.firebase.database.ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snap) {

                if (snap.getValue() == null) {
                    return;
                }


                PagoPayU pago = new PagoPayU();

                pago.estado = snap.child("estado").getValue().toString();
                pago.orderId = snap.child("orderId").getValue().toString();
                pago.responseCode = snap.child("responseCode").getValue().toString();
                pago.transactionId = snap.child("transactionId").getValue().toString();
                pago.authorizationCode = snap.child("authorizationCode").getValue().toString();


                if (pago.estado.equals("APPROVED")){
                    listener.pagoAprobado(pago);
                }
                if (pago.estado.equals("DECLINED")){
                    listener.pagoReachazado(pago.responseCode);
                }
                if (pago.estado.equals("PENDING")){
                    listener.pagoPendiente(pago);
                }

                if (pago.estado.equals("ERROR")){
                    listener.pagoConError(pago.responseCode);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }



    public static  void finalizarReembolsoYToken(PendientePayU pen){

        final ModelCliente modelc = ModelCliente.getInstance();

        MiniTarjeta mini = modelc.getTarjetaActiva();

        if ( mini == null ){
            return;
        }


        if (mini.estado.equals("DECLINED") || mini.estado.equals("APPROVED")){
            return;
        }


        mini.estado = "APPROVED";
        new ComandoTPaga(null).setRegistroCredidCard(mini);


        reembolsarPago(pen);


    }



    private static Task<Map> reembolsarPago(PendientePayU pen) {

        FirebaseFunctions mFunctions = FirebaseFunctions.getInstance();

        Map<String, Object> data = new HashMap<>();
        data.put("idOrdenPayU", pen.orderId);
        data.put("transactionId", pen.transactionId);



        return mFunctions
                .getHttpsCallable("reembolsarPago")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        Map result = (Map) task.getResult().getData();
                        return result;
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     return;
                                                 }



                                                 Map result = (Map)task.getResult().get("transactionResponse");

                                                 if (result == null){
                                                   //  Toast.makeText(getApplicationContext(),"Fallo al hacer el reverso, pruebas?",Toast.LENGTH_LONG).show();

                                                 }else {

                                                     //Toast.makeText(getApplicationContext(), "Reembolso: " + result.get("state").toString(), Toast.LENGTH_LONG).show();
                                                 }


                                             }
                                         }
                );
    }



    private static Task<Map> tokenizar(Map data, final MiniTarjeta mini) {

        final ModelCliente modelc = ModelCliente.getInstance();
        FirebaseFunctions mFunctions = FirebaseFunctions.getInstance();

        return mFunctions
                .getHttpsCallable("tokenizando")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        try{
                            Map result = (Map) task.getResult().getData();
                            return result;
                        }catch (Exception e){
                            return null;
                        }
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     return;
                                                 }

                                                 Map result = (Map)task.getResult().get("creditCardToken");

                                                 mini.token = result.get("creditCardTokenId").toString();
                                                 mini.lastFour = result.get("maskedNumber").toString();
                                                 mini.estado = "APPROVED";


                                                 modelc.apagarTarjetas();
                                                 modelc.tarjetas.add(mini);
                                                 new ComandoTPaga(null).setRegistroCredidCard(mini);


                                             }
                                         }
                );
    }

}
