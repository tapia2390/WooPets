package woopets.com.woopets.cliente.alarmas;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import java.util.Calendar;
import java.util.Date;

import woopets.com.woopets.Modelo.cliente.Alarma;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Jaison on 20/06/17.
 */

public class NotificationScheduler
{



    public static void cancelReminder(Context context,Class<?> cls, int idManager)
    {
        // Disable a receiver

        ComponentName receiver = new ComponentName(context, cls);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

        Intent intent1 = new Intent(context, cls);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, idManager, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        am.cancel(pendingIntent);
        pendingIntent.cancel();
    }



    public static void setRecordatorio(Context context, Class<?> cls, int frecuencia, Date fechaInicio, int idManager)
    {

        // Enable a receive

        Calendar calendar = toCalendar(fechaInicio);
        Calendar ahora = Calendar.getInstance();

        if(calendar.before(ahora)){
            calendar.add(Calendar.DATE,frecuencia);
        }



        ComponentName receiver = new ComponentName(context, cls);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);


        Intent intent1 = new Intent(context, cls);
        intent1.putExtra("IDMANAGER",idManager);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, idManager, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        am.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY+frecuencia, pendingIntent );

    }

    public void cancelAll(Context context,Class<?> cls){
        ModelCliente modelc = ModelCliente.getInstance();

        for (Mascota mascota:modelc.mascotas){
            for (Alarma alarma: mascota.alarmas){
                cancelReminder(context, cls, alarma.idAlarmaManager);
            }
        }


    }

    public static void reiniciarTodo(Context context,Class<?> cls){
        ModelCliente modelc = ModelCliente.getInstance();

        if (modelc.mascotas.size() == 0) {
            setSimpleReminder(context,cls);
        }

        for (Mascota mascota:modelc.mascotas){

            for (Alarma alarma: mascota.alarmas){
                setRecordatorio(context, cls,7, alarma.getInicio(), alarma.idAlarmaManager);
            }
        }

    }



    public static void setSimpleReminder(Context context,Class<?> cls)
    {

        // Enable a receiver

        ComponentName receiver = new ComponentName(context, cls);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);


        Intent intent1 = new Intent(context, cls);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent1, 0);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        //am.setInexactRepeating(AlarmManager.RTC_WAKEUP, setcalendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        am.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime()+ 4000, 4000, pendingIntent );

    }



    public static void showNotification(Context context,Class<?> cls, int idManager)
    {

        String titulo, contenido;
        ModelCliente modelc  = ModelCliente.getInstance();

        Alarma alarma = modelc.getAlarmaByIdManager(idManager);

        if (alarma == null){
            titulo = "WooPets";
            contenido = "Tu mascota te necesita";
        }else {
            titulo = "Tu mascota te necesita";
            contenido = alarma.nombre;
        }




        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent notificationIntent = new Intent(context, cls);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(cls);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(idManager, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        Notification notification = builder.setContentTitle(titulo)
                .setContentText(contenido)
                .setAutoCancel(true)
                .setSound(alarmSound)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentIntent(pendingIntent).build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(idManager, notification);


    }


    public static Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

}
