package woopets.com.woopets.cliente.Comandos;

        import android.support.annotation.NonNull;
        import android.util.Log;

        import com.firebase.client.ServerValue;
        import com.google.firebase.auth.FirebaseAuth;
        import com.google.firebase.database.ChildEventListener;
        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.DatabaseReference.CompletionListener;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.MutableData;
        import com.google.firebase.database.Query;
        import com.google.firebase.database.Transaction;
        import com.google.firebase.database.Transaction.Handler;
        import com.google.firebase.database.Transaction.Result;
        import com.google.firebase.database.ValueEventListener;

        import java.text.DateFormat;
        import java.text.SimpleDateFormat;
        import java.util.Date;
        import java.util.HashMap;
        import java.util.Map;

        import woopets.com.woopets.Modelo.cliente.Cliente;
        import woopets.com.woopets.Modelo.cliente.Compra;
        import woopets.com.woopets.Modelo.cliente.Direccion;
        import woopets.com.woopets.Modelo.cliente.Horario;
        import woopets.com.woopets.Modelo.cliente.InfoCompraActual;
        import woopets.com.woopets.Modelo.cliente.Mascota;
        import woopets.com.woopets.Modelo.cliente.Mensaje;
        import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
        import woopets.com.woopets.Modelo.cliente.Oferente;
        import woopets.com.woopets.Modelo.cliente.Publicacion;
        import woopets.com.woopets.Modelo.cliente.Ubicacion;
        import woopets.com.woopets.Oferente.ModelOferente.Modelo;
        import woopets.com.woopets.Oferente.ModelOferente.Utility;
        import woopets.com.woopets.cliente.Comandos.ComandoInventario.OnComandoInventarioListener;
        import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/22/17.
 */

public class ComandoInventario {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();



    public interface OnComandoInventarioListener {
        void reservoInventario();
        void insuficienteInventario();
        void adicionoInventario();
    }



    private OnComandoInventarioListener mListener;

    public ComandoInventario(OnComandoInventarioListener mListener){

        this.mListener = mListener;
    }




    public void reservarCantidad(String idPublicacion, final int cantidad){
        DatabaseReference ref = database.getReference("productos/"+idPublicacion+"/stock");


        ref.runTransaction(new Handler() {
            @Override
            public Result doTransaction(MutableData mutableData) {
                Object o = mutableData.getValue();

                if (o == null){
                    return Transaction.success(mutableData);
                }
                int numero = Integer.valueOf(o.toString());

                if (numero >= cantidad){
                    mutableData.setValue(numero - cantidad);
                    return Transaction.success(mutableData);
                }

                return  Transaction.abort();

            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean commited, DataSnapshot snap) {
                if (commited){
                    mListener.reservoInventario();

                }
                else {
                    mListener.insuficienteInventario();
                }

            }
        });



    }


    public void adicionarCantidad(String idPublicacion, final int cantidad){

        DatabaseReference ref = database.getReference("productos/"+idPublicacion+"/stock");


        ref.runTransaction(new Handler() {
            @Override
            public Result doTransaction(MutableData mutableData) {
                Object o = mutableData.getValue();

                if (o == null){
                    return Transaction.success(mutableData);
                }
                int numero = Integer.valueOf(o.toString());


                mutableData.setValue(numero + cantidad);

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean commited, DataSnapshot snap) {
                if (commited){
                    mListener.adicionoInventario();

                }
            }
        });



    }

    public static void checkInventario(final Compra compra, final Publicacion publi){

        final ModelCliente modelc = ModelCliente.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("productos/"+compra.idPublicacion+"/stock");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snap) {
                long stock = (long) snap.getValue();
                if (stock > modelc.params.inventarioMinimo ) {
                    return;
                }

                if (stock > 0) {
                    Mensaje msg = new Mensaje();
                    msg.idOferente = compra.idOferente;
                    msg.idPublicacion = compra.idPublicacion;
                    msg.idCompra = compra.id;
                    msg.infoAdicional = publi.nombre;
                    msg.tipo = "inventarioBajo";
                    msg.titulo = "Bajo inventario";
                    msg.mensaje = "El producto " + publi.nombre + " se encuentra bajo de inventario";
                    new ComandoChat(null).enviarPushNotification(msg);
                    return;
                }


                //Se le agotó
                Mensaje msg = new Mensaje();

                msg.idOferente = compra.idOferente;
                msg.idPublicacion = compra.idPublicacion;
                msg.idCompra = compra.id;
                msg.infoAdicional = publi.nombre;
                msg.tipo = "inventarioAgotado";
                msg.titulo = "Inventario Agotado";
                msg.mensaje = "El producto " + publi.nombre + " se encuentra agotado";
                new ComandoChat(null).enviarPushNotification(msg);
                


            }

            @Override
            public void onCancelled(@NonNull DatabaseError snap) {

            }
        });


    }







    public static OnComandoInventarioListener dummy = new OnComandoInventarioListener() {


        @Override
        public void reservoInventario() {

        }

        @Override
        public void insuficienteInventario() {

        }

        @Override
        public void adicionoInventario() {

        }
    };



}

    

