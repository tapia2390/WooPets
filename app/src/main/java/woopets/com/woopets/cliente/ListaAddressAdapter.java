package woopets.com.woopets.cliente;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.alarmas.CrearAlarma;
import woopets.com.woopets.cliente.alarmas.ListaAlarmas;
import woopets.com.woopets.cliente.detallecompra.ConfirmacionCompra;


/**
 * Created by andres on 10/15/17.
 */

public class ListaAddressAdapter extends BaseAdapter {


    private Context mContext;
    private LayoutInflater mInflater;
    ModelCliente modelc = ModelCliente.getInstance();
    List<Address> direcciones;
    private  int posDir = 0 ;


    public ListaAddressAdapter(Context context, List<Address> direcciones, int posDir){

        this.direcciones = direcciones;
        this.posDir = posDir;
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return direcciones.size();
    }

    @Override
    public Object getItem(int i) {
        return direcciones.get(i);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }


    @Override
    public View getView(final int i, View view, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.item_geo_direccion, parent, false);
        final TextView dir =  (TextView) rowView.findViewById(R.id.direccion);

        dir.setText(direcciones.get(i).getAddressLine(0));

        dir.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


                if (posDir < modelc.direcciones.size()) {

                    modelc.direcciones.get(posDir).direccion = dir.getText().toString();
                    String ciudad =  adressToCiudad(direcciones.get(i));
                    modelc.direcciones.get(posDir).estado = modelc.getDepartamento(ciudad);
                    modelc.direcciones.get(posDir).ciudad = ciudad;

                }

                ((Activity)mContext).finish();
            }
        });

        return rowView;
    }

    public String adressToCiudad(Address ladir){
        return  ladir.getLocality();
    }


    public String adressToEstado(Address ladir){
        return  ladir.getAdminArea();
    }


}

