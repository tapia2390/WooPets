package woopets.com.woopets.cliente;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import woopets.com.woopets.Comandos.ComandoCategorias;
import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Modelo.ImageFire;
import woopets.com.woopets.Modelo.cliente.Alarma;
import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Chat;
import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.InfoCompraActual;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.MiniOferente;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.Notificacion;
import woopets.com.woopets.Modelo.cliente.PagoTarjetaCliente;
import woopets.com.woopets.Modelo.cliente.Params;
import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Modelo.cliente.Solicitud;
import woopets.com.woopets.Modelo.cliente.TPaga;
import woopets.com.woopets.Oferente.LoguinOferente;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCliente;
import woopets.com.woopets.cliente.Comandos.ComandoCliente.OnComandoClienteChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoCompras;
import woopets.com.woopets.cliente.Comandos.ComandoCompras.OnComandoComprasListener;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas.OnComandoPreguntasListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoRegistroCliente;
import woopets.com.woopets.cliente.Comandos.ComandoTPaga;
import woopets.com.woopets.cliente.Comandos.ComandoTipos;
import woopets.com.woopets.cliente.Comandos.ComandoTipos.OnComandoTiposChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTips;

/**
 * Created by andres on 9/29/17.
 */

public class ModelCliente {
    private static final ModelCliente ourInstance = new ModelCliente();

    public static ModelCliente getInstance() {
        return ourInstance;
    }


    public Cliente cliente = new Cliente();
    public String tipoLogeo = "";
    public ArrayList<Direccion> direcciones = new ArrayList<Direccion>();
    public ArrayList<Mascota> mascotas = new ArrayList<>();
    public Map<String, String> departamentos = new HashMap<>();

    public ArrayList<String> razasGato = new ArrayList<>();
    public ArrayList<String> razasPerro = new ArrayList<>();


    public ArrayList<Publicacion> destacados = new ArrayList<>();
    public ArrayList<Publicacion> publicaciones = new ArrayList<>();
    public ArrayList<Publicacion> filtro = new ArrayList<>();


    public InfoCompraActual compraActual = new InfoCompraActual();  //Datos de la compra o servicio que se esta haciendo
    public PagoTarjetaCliente pagoActual = new PagoTarjetaCliente();


    public ArrayList<ItemCarrito> carritos = new ArrayList<>();
    public ArrayList<String> favoritos = new ArrayList<>();


    public ArrayList<Compra> misCompras = new ArrayList<>();
    public ArrayList<Calificacion> misCalificaciones = new ArrayList<>();

    public ArrayList<Pregunta> preguntas = new ArrayList<>();
    public ArrayList<Chat> chats = new ArrayList<>();
    public ArrayList<Notificacion> notificaciones = new ArrayList<>();

    public ArrayList<ImageFire> tips = new ArrayList<>();
    public ArrayList<Solicitud> solicitudes = new ArrayList<>();

    public String inicialP = "PP";
    public String inicialQ = "QQ";
    public String inicialR = "RR";
    public String inicialS = "SS";

    public TPaga tpaga = new TPaga();
    public ArrayList<MiniTarjeta> tarjetas = new ArrayList<>();

    public String politica = "";

    public HashMap<Query, ChildEventListener> hijosListener = new HashMap<>();
    public HashMap<Query, ValueEventListener> hijosListenerValue = new HashMap<>();
    public HashMap<DatabaseReference, ChildEventListener> hijosListenerRef = new HashMap<>();

    public Params params = new Params();

    public Double lastLatitud;
    public Double lastLongitud;

    public ArrayList<MiniOferente> miniOferentes = new ArrayList<>();


    public interface OnModelClienteListener {

        void terminoPrecarga();

    }


    private ModelCliente() {


    }


    public void  pararListeners(){
        for (Map.Entry<Query, ChildEventListener> entry : hijosListener.entrySet()) {
            Query ref = entry.getKey();
            ChildEventListener listener = entry.getValue();
            ref.removeEventListener(listener);

        }

        for (Map.Entry<Query, ValueEventListener> entry : hijosListenerValue.entrySet()) {
            Query ref = entry.getKey();
            ValueEventListener listener = entry.getValue();
            ref.removeEventListener(listener);

        }

        for (Map.Entry<DatabaseReference, ChildEventListener> entry : hijosListenerRef.entrySet()) {
            Query ref = entry.getKey();
            ChildEventListener listener = entry.getValue();
            ref.removeEventListener(listener);

        }


    }


    public String getDepartamento(String ciudad){
        if (!departamentos.containsKey(ciudad)){
            return  "";
        }

        return  departamentos.get(ciudad);

    }

    public String getInicialesParaTipo(String tipo ){

        if (tipo.equals("Pregunta")){
            return inicialP;
        }
        if (tipo.equals("Queja")){
            return inicialQ;
        }
        if (tipo.equals("Reclamo")){
            return inicialR;
        }
        if (tipo.equals("Solicitud")){
            return inicialS;
        }
        return "!!";

    }

    public String getInicialeParaTipo(String tipo ){

        if (tipo.equals("Pregunta")){
            return "P";
        }
        if (tipo.equals("Queja")){
            return "Q";
        }
        if (tipo.equals("Reclamo")){
            return "R";
        }
        if (tipo.equals("Solicitud")){
            return "S";
        }
        return  "!";

    }

    public Direccion getDireccionActiva() {
        for (Direccion dir: direcciones) {
            if (dir.porDefecto) {
                return dir;
            }
        }

        if (direcciones.size() > 0 ) {
            return direcciones.get(0);
        } else{
            return  null;}
    }


    public Mascota getMascotaActiva() {


        for (Mascota mascota: mascotas){
            if (mascota.activa) {
                return  mascota;
            }
        }
        return  null;

    }

    public void activarMascota(String idMascota) {


        new ComandoCliente(null).activarMascota(idMascota,cliente.id);


        for (Mascota mascota: mascotas){
            if (mascota.id.equals(idMascota)) {
                mascota.activa = true;
            }
            else {
                mascota.activa = false;
            }
        }

    }

    public void activarDireccion(String idDireccion) {


        new ComandoCliente(null).activarDireccion(idDireccion,cliente.id);


        for (Direccion dir: direcciones){
            if (dir.id.equals(idDireccion)) {
                dir.porDefecto = true;
            }
            else {
                dir.porDefecto = false;
            }
        }

    }


    public void activarCualquierMascota() {

        if (mascotas.size()==0){
            return;
        }

        Mascota mascota = mascotas.get(0);

        activarMascota(mascota.id);

    }


    public void activarCualquierDireccion() {

        if (direcciones.size()==0){
            return;
        }

        Direccion dir = direcciones.get(0);

        activarDireccion(dir.id);

    }


    public void eliminarMascota(String idMascota){

        new ComandoCliente(null).eliminarMascota(idMascota,cliente.id);

        int index = 0;
        int pos=-1;
        for (Mascota mascota: mascotas){
            if (mascota.id.equals(idMascota)) {
                pos = index;
            }
            index++;
        }

        mascotas.remove(pos);

    }


    public void eliminarDireccion(String idDireccion){

        new ComandoCliente(null).eliminarDireccion(idDireccion,cliente.id);

        int index = 0;
        int pos=-1;
        for (Direccion dir: direcciones){
            if (dir.id.equals(idDireccion)) {
                pos = index;
            }
            index++;
        }
        direcciones.remove(pos);
    }



    public void eliminarNotificacion(String idMensaje){

        CmdNoti.getInstance().eliminarMensaje(idMensaje);

        int index = 0;
        int pos=-1;
        for (Notificacion noti: notificaciones){
            if (noti.id.equals(idMensaje)) {
                pos = index;
            }
            index++;
        }
        notificaciones.remove(pos);
    }



    public void marcarNotificacionComoVisto(String idMensaje){

        CmdNoti.getInstance().marcarComoVista(idMensaje);

        for (Notificacion noti: notificaciones){
            if (noti.id.equals(idMensaje)) {
                noti.visto = true;
            }
        }

    }



    public int notificacionesSinVer(){

        int contar = 0;
        for (Notificacion noti: notificaciones){
            if (!noti.visto) {
                contar++;
            }
        }
        return  contar;

    }

/*
    public ArrayList<Publicacion> getPublicacionesXTipo(String categoria){

        ArrayList<Publicacion> filtro = new ArrayList<>();
        for (Publicacion publi: publicaciones) {
            if (publi.categoria.contains(categoria)) {
                filtro.add(publi);
            }
        }

        return new ArrayList<>(filtro.subList(0,filtro.size()));
    }


    public ArrayList<Publicacion> getPublicacionesXTipoYMascota(String categoria, Mascota mascota){

        ArrayList<Publicacion> filtro = new ArrayList<>();
        for (Publicacion publi: publicaciones) {
            if (publi.categoria.contains(categoria) && publi.target.contains(mascota.tipo)) {
                filtro.add(publi);
            }
        }

        return new ArrayList<>(filtro.subList(0,filtro.size()));
    }
*/



    public ArrayList<Publicacion> getPublicacionesXCategoria(String categoria){

        if (categoria.equals("")){

            ArrayList<Publicacion> filtro = new ArrayList<>();
            for (Publicacion publi: publicaciones) {

                    filtro.add(publi);
            }

            return new ArrayList<>(filtro.subList(0,filtro.size()));

        }


        ArrayList<Publicacion> filtro = new ArrayList<>();
        for (Publicacion publi: publicaciones) {
            if (!publi.categoria.equals(categoria)) {
                filtro.add(publi);
            }
        }

        return new ArrayList<>(filtro.subList(0,filtro.size()));
    }


    public ArrayList<Publicacion> getPublicacionesXCategoriaPeroConMascota(String categoria, Mascota mascota){

        if (categoria.equals("")){

            ArrayList<Publicacion> filtro = new ArrayList<>();
            for (Publicacion publi: publicaciones) {
                if (publi.target.contains(mascota.tipo)) {
                    filtro.add(publi);
                }
            }

            return new ArrayList<>(filtro.subList(0,filtro.size()));

        }


        ArrayList<Publicacion> filtro = new ArrayList<>();
        for (Publicacion publi: publicaciones) {
            if (publi.categoria.contains(categoria) && publi.target.contains(mascota.tipo)) {
                filtro.add(publi);
            }
        }

        return new ArrayList<>(filtro.subList(0,filtro.size()));
    }


    public ArrayList<Publicacion> getPublicacionesParaFiltroMasEnEstaCategoria(String categoria, Mascota mascota){


        ArrayList<Publicacion> filtro = new ArrayList<>();
        for (Publicacion publi: publicaciones) {
            if (publi.categoria.contains(categoria) && !publi.target.contains(mascota.tipo)) {
                filtro.add(publi);
            }
        }

        return new ArrayList<>(filtro.subList(0,filtro.size()));
    }


    public void addDestacado (Publicacion publi) {

        int index = getIndexDeDestacado(publi);
        if (index  > 0 ) {
            destacados.set(index,publi);
            return;
        }

        destacados.add(publi);
/*
        while (destacados.size() > 6){
            destacados.remove(0);
        }*/


    }


    public void deleteDestacado (Publicacion publi) {

        int index = getIndexDeDestacado(publi);
        if (index  > 0 ) {
            destacados.remove(index);
        }

    }

    public boolean checkEstadoOferente(String idOferente){
        for (MiniOferente mini:miniOferentes){
            if (mini.id.equals(idOferente)){
                if (mini.activo){
                    return true;
                } else {
                    return false;
                }

            }

        }
        return false;
    }


    public void addPublicacion (Publicacion publi) {


        if (!checkEstadoOferente(publi.idOferente)){
            return;
        }

        int index = getIndexDePublicacion(publi);
        if (index  >= 0 ) {
            publicaciones.set(index,publi);
            return;
        }


        publicaciones.add(publi);

    }

    public void addCarrito (ItemCarrito itemCarrito) {

        int index = getIndexDeCarrito(itemCarrito);
        if (index  >= 0 ) {
            carritos.set(index,itemCarrito);
            return;
        }

        carritos.add(itemCarrito);

    }

    public void addFavorito (String idPublicacion) {

        int index = getIndexDeFavorito(idPublicacion);
        if (index  >= 0 ) {
            favoritos.set(index,idPublicacion);
            return;
        }

        favoritos.add(idPublicacion);

    }

    public void addPregunta (Pregunta pregu) {

        int index = getIndexDePregunta(pregu);
        if (index  >= 0 ) {
            preguntas.set(index,pregu);
            return;
        }

        preguntas.add(pregu);

        Collections.sort(preguntas);

    }

    public void addSolicitud (Solicitud sol) {

        int index = getIndexDeSolicitud(sol);
        if (index  >= 0 ) {
            solicitudes.set(index,sol);
            return;
        }

        solicitudes.add(sol);

    }


    public void addChat (Chat chat) {

        int index = getIndexDeChat(chat);
        if (index  >= 0 ) {
            chats.set(index,chat);
            return;
        }

        chats.add(chat);

    }


    public void addCompra (Compra compra) {

        int index = getIndexDeCompra(compra);
        if (index  >= 0 ) {
            misCompras.set(index,compra);
            return;
        }

        misCompras.add(0,compra);
        sortMisCompras();



    }

    public void addNotificacion (Notificacion noti) {

        int index = getIndexDeNotificacion(noti);
        if (index  >= 0 ) {
            notificaciones.set(index,noti);
            return;
        }
        notificaciones.add(noti);

    }

    public MiniTarjeta getTarjetaActiva(){
        for (MiniTarjeta mini:tarjetas){
            if (mini.activo) {
                return  mini;
            }
        }
        return null;
    }


    public void apagarTarjetas(){
        for (MiniTarjeta mini:tarjetas){
            new ComandoTPaga(null).desactivarTarjeta(mini.id);
            mini.activo = false ;
        }

    }



    public void deletePublicacion (Publicacion publi) {

        int index = getIndexDePublicacion(publi);
        if (index  > 0 ) {
            publicaciones.remove(index);
        }

    }


    private int getIndexDeDestacado(Publicacion publi) {
        int index = 0;
        for (Publicacion publicacion:destacados) {
            if (publicacion.id.equals(publi.id) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }


    private int getIndexDePublicacion(Publicacion publi) {
        int index = 0;
        for (Publicacion publicacion: publicaciones) {
            if (publicacion.id.equals(publi.id) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }

    private int getIndexDeCarrito(ItemCarrito itemCarrito) {
        int index = 0;
        for (ItemCarrito item: carritos) {
            if (item.id.equals(itemCarrito.id) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }

    private int getIndexDeFavorito(String idPublicacion) {
        int index = 0;
        for (String id: favoritos) {
            if (id.equals(idPublicacion) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }

    private int getIndexDePregunta(Pregunta newpregu) {
        int index = 0;
        for (Pregunta pregu: preguntas) {
            if (pregu.id.equals(newpregu.id) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }

    private int getIndexDeSolicitud(Solicitud sol) {
        int index = 0;
        for (Solicitud solicitud: solicitudes) {
            if (solicitud.id.equals(sol.id) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }

    private int getIndexDeChat(Chat newChat) {
        int index = 0;
        for (Chat chat: chats) {
            if (chat.id.equals(newChat.id) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }


    private int getIndexDeCompra(Compra comp) {
        int index = 0;
        for (Compra compra: misCompras) {
            if (compra.id.equals(comp.id) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }


    private int getIndexDeNotificacion(Notificacion not) {
        int index = 0;
        for (Notificacion noti: notificaciones) {
            if (noti.id.equals(not.id) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }

    public  Mascota getMascotaById(String idMascota) {
        for (Mascota mascota:mascotas) {
            if (mascota.id.equals(idMascota) ){
                return mascota;
            }
        }
        return  null;
    }



    public  Publicacion getPublicacionById(final String idPublicacion) {

        try {
            for (Publicacion publicacion:publicaciones) {
                if (publicacion.id.equals(idPublicacion) ){
                    return publicacion;
                }
            }

        } catch (ConcurrentModificationException e){
            return  null;
        }

        return  null;
    }



    public  Solicitud getSolicitudById(String idSolicitud) {
        for (Solicitud sol:solicitudes) {
            if (sol.id.equals(idSolicitud) ){
                return sol;
            }
        }
        return  null;
    }


    public  Compra getCompraById(String idCompra) {
        for (Compra compra:misCompras) {
            if (compra.id.equals(idCompra) ){
                return compra;
            }
        }
        return  null;
    }



    public void adicionarItemAlCarrito(ItemCarrito item) {
        item.id = new ComandoCliente(null).getLLave();
        carritos.add(item);
        new ComandoCliente(null).adicionarAlCarrito(item);

    }

    public void adicionarItemAFavoritos(String idPublicacion) {
        favoritos.add(idPublicacion);
        new ComandoCliente(null).adicionarAFavoritos(idPublicacion);

    }


    public boolean estaPublicacionEncarrito(String idPublicacion){
        for (ItemCarrito item: carritos) {
            if (item.idPublicacion.equals(idPublicacion)){
                return true;
            }
        }
        return false;
    }

    public boolean estaPublicacionEnFavoritos(String idPublicacion){
        for (String item: favoritos) {
            if (item.equals(idPublicacion)){
                return true;
            }
        }
        return false;
    }

    public void eliminarPublicacionDelCarrito(String idPublicacion){
        int index = 0;
        boolean encontro = false;
            for (ItemCarrito item: carritos) {
                if (item.idPublicacion.equals(idPublicacion)){
                    new ComandoCliente(null).eliminarDelCarrito(item.id);
                    encontro = true;
                    break;
                }
                index++;
        }

        if (encontro) {
            carritos.remove(index);
        }

    }


    public void eliminarPublicacionDeFavoritos(String idPublicacion){
        int index = 0;
        boolean encontro = false;
        for (String item: favoritos) {
            if (item.equals(idPublicacion)){
                new ComandoCliente(null).eliminarDeFavoritos(item);
                encontro = true;
            }
            index++;
        }

        if (encontro) {
            favoritos.remove(--index);
        }

    }


    public int getMiCalificacionToCompra(String idCompra){
        for (Calificacion cali: misCalificaciones){
            if (cali.idCompra.equals(idCompra)){
                return cali.calificacion;
            }
        }
        return -1;
    }

    public Calificacion getCalificacionToCompra(String idCompra){
        for (Calificacion cali: misCalificaciones){
            if (cali.idCompra.equals(idCompra)){
                return cali;
            }
        }
        return null;
    }


    public ArrayList<Pregunta> getPreguntasByIdPublicacion(String idPublicacion){
        ArrayList<Pregunta> resp = new ArrayList<>();

        for (Pregunta pregu : preguntas){
            if (pregu.idPublicacion.equals(idPublicacion)){
                resp.add(pregu);
            }
        }

        return  resp;
    }


    public ArrayList<Pregunta> getPreguntasByIdPublicacionSoloMias(String idPublicacion){
        ArrayList<Pregunta> resp = new ArrayList<>();

        for (Pregunta pregu : preguntas){
            if (pregu.idPublicacion.equals(idPublicacion) && pregu.idCliente.equals(cliente.id)){
                resp.add(pregu);
            }
        }

        return  resp;
    }


    public ArrayList<Pregunta> getPreguntasByIdPublicacionTodasMenosMias(String idPublicacion){
        ArrayList<Pregunta> resp = new ArrayList<>();

        for (Pregunta pregu : preguntas){
            if (pregu.idPublicacion.equals(idPublicacion) && !pregu.idCliente.equals(cliente.id)){
                resp.add(pregu);
            }
        }

        return  resp;
    }


    public  ArrayList<Pregunta> getByIdPublicacionOrdenadas(String idPublicacion){
        ArrayList<Pregunta> resp = new ArrayList<>();
        ArrayList<Pregunta> mias = getPreguntasByIdPublicacionSoloMias(idPublicacion);
        ArrayList<Pregunta> resto = getPreguntasByIdPublicacionTodasMenosMias(idPublicacion);

        if (mias.size() > 0) {
            Pregunta cabeza = new Pregunta();
            cabeza.id = "HEADER MIAS";
            resp.add(cabeza);
        }

        for (Pregunta pregu : mias){
            resp.add(pregu);
        }

        if (resto.size() > 0) {
            Pregunta otra = new Pregunta();
            otra.id = "HEADER RESTO";
            resp.add(otra);
        }

        for (Pregunta pregu : resto){
            resp.add(pregu);
        }

        return  resp;
    }



    public ArrayList<Chat> getChatsByIdCompra(String idCompra){
        ArrayList<Chat> resp = new ArrayList<>();

        for (Chat chat : chats){
            if (chat.idCompra.equals(idCompra)){
                resp.add(chat);
            }
        }

        return  resp;
    }


    public int sinLeer(String idCompra){
        ArrayList<Chat> resp = new ArrayList<>();

        int sinLeer = 0;
        for (Chat chat : chats){
            if (chat.idCompra.equals(idCompra) && !chat.visto ){
                sinLeer++;
            }
        }

        return  sinLeer;
    }

    public String[] getRazas(String tipo) {


        if (tipo.equals("Gato")){

            String[] array = new String[razasGato.size()];

            for (int i = 0; i < razasGato.size(); i++) {
                array[i] = razasGato.get(i);
            }
            return array;

        }

        if (tipo.equals("Perro")){

            String[] array = new String[razasPerro.size()];

            for (int i = 0; i < razasPerro.size(); i++) {
                array[i] = razasPerro.get(i);
            }
            return array;

        }

        return new String[]{""};

    }


    public int getIdNuevaAlarma(){

        int num = 0;
        for (Mascota mascota:mascotas){
            num = num + mascota.alarmas.size();
        }

        return num + 1;
    }

    public int getMaxIdAlarmas(){
        int max = 0;

        for (Mascota mascota:mascotas){
            max = Math.max(max, mascota.getMaxIdAlarmas());
        }

        return  max;
    }


    public Alarma getAlarmaByIdManager(int idManager){

        for (Mascota mascota:mascotas){
            for (Alarma alarma : mascota.alarmas){
                if (alarma.idAlarmaManager == idManager){
                    return  alarma;
                }
            }
        }

        return null;
    }



    public boolean reiniciarCarga(final String idPublicacion, final OnModelClienteListener listener){

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        ComandoTipos cmTipos = new ComandoTipos(null);
        cmTipos.getRazasGato();
        cmTipos.getRazasPerro();

        if (user == null) {
            return false;
        }



        new ComandoCategorias(new ComandoCategorias.OnComandoCategoriasChangeListener() {
            @Override
            public void cargoCategorias() {


            }

            @Override
            public void cargoTipoUsuario(String tipo) {
                if (tipo.equals("CLIENTE")) {
                    ingresarComoCliente(user, idPublicacion, listener);

                }

            }

            @Override
            public void actualizoSistema() {

            }
        }).getTipoUsuario(user.getUid());  //Mover este comando de 'Categorias" a Usuario

        return true;

    }


    public void ingresarComoCliente(FirebaseUser user , final String idPublicacion, final OnModelClienteListener listener){

        cliente.id = user.getUid();
        String token = FirebaseInstanceId.getInstance().getToken();

        CmdNoti.getInstance().actualizarTokenDevice(token);

        String provider;

        if (user.getProviders().size() > 0) {
            provider =  user.getProviders().get(0);

            if (provider.equals("password")) {
                tipoLogeo = "normal";
            }else{
                tipoLogeo = "faceb";
            }
        } else {
            tipoLogeo = "anonimo";
        }


        new ComandoCliente(new OnComandoClienteChangeListener() {
            @Override
            public void cargoCliente() {

               preCargar(idPublicacion, listener);

            }

            @Override
            public void clienteNoExiste() {

            }

        }).getCliente(user.getUid());

    }

    private void preCargar(String idPublicacion, final OnModelClienteListener listener) {

        String tipo = "";
        Mascota mascota = getMascotaActiva();
        if (mascota != null) {
            tipo = mascota.tipo;
        }


        new ComandoCalificacion(null).getMisCalificaciones(cliente.id);
        new ComandoTips().getTips();
        new ComandoTips().getPolitica();
        new ComandoAyuda(null).getInicales();
        CmdNoti.getInstance().getNotificaciones();


        Direccion direccion = getDireccionActiva();


        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {

            }

            @Override
            public void cargoPublicacion() {
                ///Pilas aca no puede ir nada// o jamas se va a llamar
            }
        }).getLast6Destacados(tipo);

        if (direccion != null && direccion.ubicacion != null) {

            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {

                }

                @Override
                public void cargoPublicacion() {
                    listener.terminoPrecarga();

                }
            }).getOferentesCercanos(direccion.ubicacion, idPublicacion);

        } else {
            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {
                    ///Pilas aca no puede ir nada// o jamas se va a llamar
                }

                @Override
                public void cargoPublicacion() {

                }
            }).getTodasPublicaciones(tipo);


            new ComandoCompras(new OnComandoComprasListener() {
                @Override
                public void cargoNuevaCompra() {

                }

                @Override
                public void cargoModificacionCompra() {

                }
            }).getComprasAbiertas(cliente.id);

            new ComandoCompras(new OnComandoComprasListener() {
                @Override
                public void cargoNuevaCompra() {

                }

                @Override
                public void cargoModificacionCompra() {

                }
            }).getComprasCerradas(cliente.id);


        }
    }


    public void sortMisCompras(){

        Collections.sort(misCompras, new Comparator<Compra>() {
            @Override
            public int compare(Compra m1, Compra m2) {
                if (m1.timestamp < m2.timestamp){
                    return  1;
                }else {
                    return -1;
                }
            }
        });

    }


    public void sortMisNotificaciones(){

        Collections.sort(notificaciones, new Comparator<Notificacion>() {
            @Override
            public int compare(Notificacion m1, Notificacion m2) {
                if (m1.timestamp < m2.timestamp){
                    return  1;
                }else {
                    return -1;
                }
            }
        });

    }



}
