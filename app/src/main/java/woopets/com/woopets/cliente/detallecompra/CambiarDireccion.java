package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ClienteRegistroBasicos;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;

public class CambiarDireccion extends Activity {


    ModelCliente modelc = ModelCliente.getInstance();
    private ListView listaView;
    CambiarDireccionAdapter adapter;
    String origen = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_direccion);


        listaView = (ListView) findViewById(R.id.lista);
        adapter = new CambiarDireccionAdapter(this,this);
        listaView.setAdapter(adapter);

        if (getIntent().hasExtra("ORIGEN")){
            origen = getIntent().getStringExtra("ORIGEN");
        }


    }


    public void didTapNuevaDireccion(View v) {
        Intent i = new Intent(getApplicationContext(), ClienteRegistroBasicos.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {

        if (origen.equals("HOME")){
            Intent i = new Intent(getApplicationContext(), HomeCliente.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            //overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            finish();
        }

        else {
            finish();
        }

    }
}
