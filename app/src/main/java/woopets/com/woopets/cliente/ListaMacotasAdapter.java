package woopets.com.woopets.cliente;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.alarmas.CrearAlarma;
import woopets.com.woopets.cliente.alarmas.ListaAlarmas;
import woopets.com.woopets.cliente.detallecompra.ConfirmacionCompra;


/**
 * Created by andres on 10/15/17.
 */

public class ListaMacotasAdapter extends BaseAdapter {


    private Context mContext;
    private LayoutInflater mInflater;
    ModelCliente modelc = ModelCliente.getInstance();
    private OnImageFireChange listener;
    CrearMascotaAdapterCallback mListener = sDummyCallbacks;
    ImageView fotoMascota;
    FrameLayout botonActivar;


    public ListaMacotasAdapter(Context context, OnImageFireChange listener, CrearMascotaAdapterCallback mListener){

        this.listener = listener;
        this.mListener = mListener;

        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount() {
        return modelc.mascotas.size();
    }

    @Override
    public Object getItem(int i) {
        return modelc.mascotas.get(i);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.item_lista_mascotas, parent, false);
        TextView nombre =  (TextView) rowView.findViewById(R.id.nombre);
        TextView edad = (TextView) rowView.findViewById(R.id.fecha);
        fotoMascota = (ImageView) rowView.findViewById(R.id.foto);
        botonActivar =  rowView.findViewById(R.id.botonActivar);
        LinearLayout layoutGeneral = (LinearLayout) rowView.findViewById(R.id.layoutGeneral);
        final ImageView seleccionado = (ImageView) rowView.findViewById(R.id.activo);
        FrameLayout btnBorrar = (FrameLayout) rowView.findViewById(R.id.borrar);
        FrameLayout fonfoAlarma = (FrameLayout) rowView.findViewById(R.id.fondoAlarma);
        final ImageView imgAlarma = (ImageView) rowView.findViewById(R.id.imgAlarma);


        final Mascota mascota = modelc.mascotas.get(i);
        mascota.imageFire.setListener(listener);
        Bitmap foto = mascota.imageFire.getFoto("MASCOTA");

        if (foto != null) {
            //fotoMascota.setImageDrawable(getCroppedBitmap(foto));
            fotoMascota.setImageBitmap(foto);
        }

        nombre.setText(mascota.nombre);
        edad.setText(mascota.genero);

        botonActivar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionado.setImageResource(R.drawable.imgactivo);
                modelc.activarMascota(mascota.id);
                ListaMacotasAdapter.this.notifyDataSetChanged();
            }
        });


        btnBorrar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                alertDialogBuilder.setTitle("¡Atención!");
                // Icon Of Alert Dialog
                alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
                // Setting Alert Dialog Message
                alertDialogBuilder.setMessage("¿Seguro deseas borrar estos datos?");
                alertDialogBuilder.setCancelable(false);

                alertDialogBuilder.setPositiveButton("Si, Borrar", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        modelc.eliminarMascota(mascota.id);
                        if (mascota.activa){
                            modelc.activarCualquierMascota();
                        }
                        ListaMacotasAdapter.this.notifyDataSetChanged();

                    }
                });

                alertDialogBuilder.setNegativeButton("No, Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();





            }
        });

        fonfoAlarma.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, ListaAlarmas.class);
                i.putExtra("IDMASCOTA",mascota.id);
                mContext.startActivity(i);


            }
        });

        final int j = i;
        layoutGeneral.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                //mListener.cambiarFoto(j);  Asi era antes de permitor editar foto en la misma vista de crear

                Intent i = new Intent(mContext, CrearMascota.class);
                i.putExtra("IDMASCOTA",mascota.id);
                i.putExtra("MODO","EDITAR");
                mContext.startActivity(i);
            }
        });

        fotoMascota.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                //mListener.cambiarFoto(j);  Asi era antes de permitor editar foto en la misma vista de crear

                Intent i = new Intent(mContext, CrearMascota.class);
                i.putExtra("IDMASCOTA",mascota.id);
                i.putExtra("MODO","EDITAR");
                mContext.startActivity(i);
            }
        });




        seleccionado.setImageResource(R.drawable.imginactivo);
        if (mascota.activa){
            seleccionado.setImageResource(R.drawable.imgactivo);
        }


        if (mascota.tieneAlarmaActiva()){
            imgAlarma.setImageResource(R.drawable.btnalarmaon);
        }

        return rowView;
    }






    /**
     * Se define la interfaz del callback
     */
    public interface CrearMascotaAdapterCallback
    {
        void cambiarFoto(int pos);
    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static CrearMascotaAdapterCallback sDummyCallbacks = new CrearMascotaAdapterCallback()
    {
        @Override
        public void cambiarFoto(int pos) {

        }


    };




}
