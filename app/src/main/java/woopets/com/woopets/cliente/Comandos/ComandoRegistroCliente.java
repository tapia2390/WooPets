package woopets.com.woopets.cliente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Oferente.Comandos.ComandoRegistroOferente.OnComandoRegistroOferenteChangeListener;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 9/28/17.
 */

public class ComandoRegistroCliente {


    //traer ui
    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    public interface OnComandoRegistroClienteChangeListener {

        void informacionRegistroCliente();
        void errorInformacionRegistroCliente();

    }

    //interface del listener de la actividad interesada
    private ComandoRegistroCliente.OnComandoRegistroClienteChangeListener mListener;

    public ComandoRegistroCliente(OnComandoRegistroClienteChangeListener mListener){

        this.mListener = mListener;
    }


    public void actualizarDatosBasicos(){

        Cliente cli = modelc.cliente;

        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/");

        Map<String, Object> mapa = new HashMap<String, Object>();

        mapa.put("correo",cli.correo);
        mapa.put("nombre", cli.nombre);
        mapa.put("apellido", cli.apellido);
        mapa.put("documento", cli.cedula);
        mapa.put("celular", cli.celular);


        Map<String, Object> direcciones = new HashMap<String, Object>();


        int i = 0;
        for (Direccion dir : modelc.direcciones) {
            Map<String, Object> minidir = new HashMap<String, Object>();
            minidir.put("direccion",dir.direccion);
            minidir.put("nombre",dir.nombre);

            if (i == 0 ) {
                minidir.put("porDefecto", true);
            }
            else {
                minidir.put("porDefecto", false);
                }
            i++;

            minidir.put("posicion",dir.posicion );

            Map<String, Object> ubi = new HashMap<String, Object>();
            ubi.put("lat",dir.ubicacion.latitud);
            ubi.put("lon",dir.ubicacion.longitud);
            minidir.put("ubicacion",ubi);

            if (dir.id == "") {
                dir.id =  ref.push().getKey();

            }
            direcciones.put(dir.id,minidir);

        }

        mapa.put("direcciones",direcciones);

        ref.updateChildren(mapa);


    }


    public void registroInicialCliente(String correo, String idUsu, String versionAndroid, boolean anonimo) {

        DatabaseReference key = referencia.push();

        Date date = new Date();
        DateFormat hourFormat = new SimpleDateFormat("hh:mm a");
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

        DatabaseReference ref = database.getReference("clientes/"+idUsu+"/");

        Map<String, Object> enviarRegistro = new HashMap<String, Object>();

        enviarRegistro.put("correo", correo);
        enviarRegistro.put("fechaUltimoLogeo", dateFormat.format(date));
        enviarRegistro.put("horaUltimoLogeo", hourFormat.format(date));

        enviarRegistro.put("systemDevice", "ANDROID" );
        enviarRegistro.put("version", versionAndroid);
        if (anonimo) {
            enviarRegistro.put("anonimo", true);
            enviarRegistro.put("nombre", "");
            enviarRegistro.put("apellido", "");
            enviarRegistro.put("celular", "");
            enviarRegistro.put("documento", "");
        }

        ref.setValue(enviarRegistro, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {

                    mListener.informacionRegistroCliente();
                } else {
                    mListener.errorInformacionRegistroCliente();
                }
            }
        });

    }


    public void registroInicialClienteFB(String correo, String nombre, String idUsu, String versionAndroid) {

        //DatabaseReference key = referencia.push();

        Date date = new Date();
        DateFormat hourFormat = new SimpleDateFormat("hh:mm a");
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        //DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

        DatabaseReference ref = database.getReference("clientes/"+idUsu+"/");

        if (correo.equals("")){
            correo = nombre.replaceAll("\\s+", "");
            correo = correo + "@nocompartiocorreoconFB";
        }


        Map<String, Object> enviarRegistro = new HashMap<String, Object>();

        enviarRegistro.put("correo", correo);
        enviarRegistro.put("fechaUltimoLogeo", dateFormat.format(date));
        enviarRegistro.put("horaUltimoLogeo", hourFormat.format(date));
        enviarRegistro.put("systemDevice", "ANDROID" );
        enviarRegistro.put("version", versionAndroid);
        enviarRegistro.put("nombre", nombre);
        enviarRegistro.put("logeo", "facebook");
        enviarRegistro.put("apellido", "");
        enviarRegistro.put("celular", "");
        enviarRegistro.put("documento", "");


        ref.updateChildren(enviarRegistro, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {

                    mListener.informacionRegistroCliente();
                } else {
                    mListener.errorInformacionRegistroCliente();
                }
            }
        });

    }


    public  void enviarRegistro2(final String uid){
        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("hh:mm a");
        System.out.println("Hora: "+hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: "+dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: "+hourdateFormat.format(date));

        final DatabaseReference ref = database.getReference("clientes/"+uid+"/fechaUltimoLogeo/" );//ruta path
        ref.setValue(""+dateFormat.format(date));

        final DatabaseReference ref2 = database.getReference("clientes/"+uid+"/horaUltimoLogeo/" );//ruta path
        ref2.setValue(""+hourFormat.format(date));

        final DatabaseReference ref3 = database.getReference("clientes/"+uid+"/systemDevice/" );//ruta path
        ref3.setValue("Android");

    }


    //Pilas pensada para solo llamar cuando se regitro por facebook
    public  void enviarRegistro3(final String nombre, final String uid, String correo){
        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("hh:mm a");
        System.out.println("Hora: "+hourFormat.format(date));
        //Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: "+dateFormat.format(date));
        //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: "+hourdateFormat.format(date));

        final DatabaseReference refs = database.getReference("clientes/"+uid+"/nombre/" );//ruta path
        refs.setValue(""+nombre);

        if (correo == null) {
            correo = "";
        }


        if (correo.equals("")){
            correo = nombre.replaceAll("\\s+", "");
            correo = correo + "@nocompartiocorreoconFB";
        }

        final DatabaseReference ref0 = database.getReference("clientes/"+uid+"/correo/" );//ruta path
        ref0.setValue(correo);


        final DatabaseReference ref = database.getReference("clientes/"+uid+"/fechaUltimoLogeo/" );//ruta path
        ref.setValue(""+dateFormat.format(date));

        final DatabaseReference ref2 = database.getReference("clientes/"+uid+"/horaUltimoLogeo/" );//ruta path
        ref2.setValue(""+hourFormat.format(date));

        final DatabaseReference ref3 = database.getReference("clientes/"+uid+"/systemDevice/" );//ruta path
        ref3.setValue("Android");

        final DatabaseReference ref4 = database.getReference("clientes/"+uid+"/logeo/" );//ruta path
        ref4.setValue("facebook");


    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static woopets.com.woopets.Oferente.Comandos.ComandoRegistroOferente.OnComandoRegistroOferenteChangeListener sDummyCallbacks = new woopets.com.woopets.Oferente.Comandos.ComandoRegistroOferente.OnComandoRegistroOferenteChangeListener()
    {
        @Override
        public void informacionRegistroOferente()
        {}

        @Override
        public void errorInformacionRegistroOferente()
        {}

    };
}

