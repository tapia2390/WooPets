package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TextView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion.OnComandoCalificacionChangeListener;
import woopets.com.woopets.cliente.ModelCliente;

public class ListaResenas extends Activity {




    ModelCliente modelc = ModelCliente.getInstance();
    private ListView listaView;
    ListaResenasAdapter adapter;


    ImageView star1, star2, star3,star4,star5;
    TextView calificacion, numeroResenas;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_resenas);




        TextView porL1, porV1;
        TextView porL2, porV2;
        TextView porL3, porV3;
        TextView porL4, porV4;
        TextView porL5, porV5;

        calificacion = (TextView) findViewById(R.id.calificacion);
        numeroResenas = (TextView) findViewById(R.id.numeroResenas);

        star1 = (ImageView) findViewById(R.id.estre1);
        star2 = (ImageView) findViewById(R.id.estre2);
        star3 = (ImageView) findViewById(R.id.estre3);
        star4 = (ImageView) findViewById(R.id.estre4);
        star5 = (ImageView) findViewById(R.id.estre5);



        porL1 = (TextView) findViewById(R.id.porL1);
        porV1 = (TextView) findViewById(R.id.porV1);

        porL2 = (TextView) findViewById(R.id.porL2);
        porV2 = (TextView) findViewById(R.id.porV2);

        porL3 = (TextView) findViewById(R.id.porL3);
        porV3 = (TextView) findViewById(R.id.porV3);

        porL4 = (TextView) findViewById(R.id.porL4);
        porV4 = (TextView) findViewById(R.id.porV4);

        porL5 = (TextView) findViewById(R.id.porL5);
        porV5 = (TextView) findViewById(R.id.porV5);

        ModelCliente modelc  = ModelCliente.getInstance();

        String idPublicacion = getIntent().getStringExtra("IDPUBLICACION");
        final Publicacion publi = modelc.getPublicacionById(idPublicacion);

        Double cal = publi.getCalificacion(new OnComandoCalificacionChangeListener() {
            @Override
            public void cargoCalificaciones() {

                Double cal = publi.getCalificacion(null);
                cal = Math.round(cal * 2) / 2.0;
                calificacion.setText((String.format ("%.1f", cal)));
                numeroResenas.setText(publi.calificaciones.size() + " reseñas");
            }
        });

        cal = Math.round(cal * 2) / 2.0;
        calificacion.setText((String.format ("%.1f", cal)));
        if (publi.calificaciones != null){
            numeroResenas.setText( publi.calificaciones.size() +  " reseñas");
        }


        TableLayout.LayoutParams params11 =  new TableLayout.LayoutParams();
        params11.weight  = publi.porcentajeCalificaciones(5);
        params11.height = LayoutParams.WRAP_CONTENT;
        params11.width = LayoutParams.MATCH_PARENT;

        TableLayout.LayoutParams params12 =  new TableLayout.LayoutParams();
        params12.weight  = 100 - publi.porcentajeCalificaciones(5);
        params12.height = LayoutParams.WRAP_CONTENT;
        params12.width = LayoutParams.MATCH_PARENT;



        porL1.setLayoutParams(params11);
        porV1.setLayoutParams(params12);


        //////4

        TableLayout.LayoutParams params21 =  new TableLayout.LayoutParams();
        params21.weight  = publi.porcentajeCalificaciones(4);
        params21.height = LayoutParams.WRAP_CONTENT;
        params21.width = LayoutParams.MATCH_PARENT;

        TableLayout.LayoutParams params22 =  new TableLayout.LayoutParams();
        params22.weight  = 100 - publi.porcentajeCalificaciones(4);
        params22.height = LayoutParams.WRAP_CONTENT;
        params22.width = LayoutParams.MATCH_PARENT;

        porL2.setLayoutParams(params21);
        porV2.setLayoutParams(params22);

        //////3

        TableLayout.LayoutParams params31 =  new TableLayout.LayoutParams();
        params31.weight  = publi.porcentajeCalificaciones(3);
        params31.height = LayoutParams.WRAP_CONTENT;
        params31.width = LayoutParams.MATCH_PARENT;

        TableLayout.LayoutParams params32 =  new TableLayout.LayoutParams();
        params32.weight  = 100 - publi.porcentajeCalificaciones(3);
        params32.height = LayoutParams.WRAP_CONTENT;
        params32.width = LayoutParams.MATCH_PARENT;

        porL3.setLayoutParams(params31);
        porV3.setLayoutParams(params32);


        //////2

        TableLayout.LayoutParams params41 =  new TableLayout.LayoutParams();
        params41.weight  = publi.porcentajeCalificaciones(2);
        params41.height = LayoutParams.WRAP_CONTENT;
        params41.width = LayoutParams.MATCH_PARENT;

        TableLayout.LayoutParams params42 =  new TableLayout.LayoutParams();
        params42.weight  = 100 - publi.porcentajeCalificaciones(2);
        params42.height = LayoutParams.WRAP_CONTENT;
        params42.width = LayoutParams.MATCH_PARENT;

        porL4.setLayoutParams(params41);
        porV4.setLayoutParams(params42);


        //////1

        TableLayout.LayoutParams params51 =  new TableLayout.LayoutParams();
        params51.weight  = publi.porcentajeCalificaciones(1);
        params51.height = LayoutParams.WRAP_CONTENT;
        params51.width = LayoutParams.MATCH_PARENT;

        TableLayout.LayoutParams params52 =  new TableLayout.LayoutParams();
        params52.weight  = 100 - publi.porcentajeCalificaciones(1);
        params52.height = LayoutParams.WRAP_CONTENT;
        params52.width = LayoutParams.MATCH_PARENT;

        porL5.setLayoutParams(params51);
        porV5.setLayoutParams(params52);


        pintarEstrellas(publi.getCalificacion(null));



        ///Listado


        ArrayList<Calificacion> calificacions = publi.calificaciones;

        listaView = (ListView) findViewById(R.id.lista);
        adapter = new ListaResenasAdapter(getBaseContext(),calificacions);
        listaView.setEmptyView(findViewById(R.id.vacio));
        listaView.setAdapter(adapter);


    }


    private void pintarEstrellas(double calificacion){

        pintarApagarPrenderEstrella(star1,1,calificacion);
        pintarApagarPrenderEstrella(star2,2,calificacion);
        pintarApagarPrenderEstrella(star3,3,calificacion);
        pintarApagarPrenderEstrella(star4,4,calificacion);
        pintarApagarPrenderEstrella(star5,5,calificacion);
    }


    private void pintarApagarPrenderEstrella(ImageView star, double pos, double nota){

        if (nota >= pos) {
            star.setImageResource(R.drawable.imgestrellacompleta);
            return;
        }

        if (Math.abs(nota-pos) <= 0.5) {
            star.setImageResource(R.drawable.imgestrellamedia);
            return;
        }

        star.setImageResource(R.drawable.imgestrellavacia);


    }

}
