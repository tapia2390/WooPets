package woopets.com.woopets.cliente.carrito;

        import android.app.Activity;
        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Bitmap;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.TextView;

        import java.util.ArrayList;

        import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
        import woopets.com.woopets.Modelo.Item;
        import woopets.com.woopets.Modelo.cliente.ItemCarrito;
        import woopets.com.woopets.Modelo.cliente.Publicacion;
        import woopets.com.woopets.Oferente.ModelOferente.Utility;
        import woopets.com.woopets.R;
        import woopets.com.woopets.cliente.ModelCliente;
        import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;


/**
 * Created by andres on 10/31/17.
 */

public class FavoritosAdapter  extends BaseAdapter {


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private OnImageFireChange listener;
    private final LayoutInflater mInflater;


    public FavoritosAdapter(Context c, OnImageFireChange listener) {
        mContext = c;
        this.listener = listener;
        mInflater = LayoutInflater.from(c);

    }

    @Override
    public int getCount() {
        return modelc.favoritos.size();
    }

    @Override
    public Object getItem(int i) {
        return modelc.favoritos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {


        FavoritosAdapter.ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_favorito,parent, false);
            holder = new FavoritosAdapter.ViewHolder();
            holder.valor = (TextView) convertView.findViewById(R.id.valor);
            holder.foto = (ImageView) convertView.findViewById(R.id.foto);
            holder.nombre = (TextView) convertView.findViewById(R.id.nombre);
            holder.layout = (LinearLayout) convertView.findViewById(R.id.linear);
            convertView.setTag(holder);

        }else{
            holder = (FavoritosAdapter.ViewHolder) convertView.getTag();
        }

        final String idPublicacion = modelc.favoritos.get(i);

        ImageView foto = holder.foto;
        TextView valor = holder.valor;
        TextView nombre = holder.nombre;
        LinearLayout layout = holder.layout;



        final Publicacion publi = modelc.getPublicacionById(idPublicacion);
        publi.fotosBaja.get(0).setListener(listener);



        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, DetallePublicacion.class);
                i.putExtra("IDPUBLICACION",publi.id);
                mContext.startActivity(i);
                ((Activity)mContext).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });


        if (publi == null) {
            return  convertView;
        }


        nombre.setText(publi.nombre);
        valor.setText(Utility.convertToMoney(publi.precio));


        Bitmap fotoBitmap = publi.fotosBaja.get(0).getFoto("PUBLICACION");

        if (fotoBitmap != null) {
            foto.setImageBitmap(fotoBitmap);
        }

        return convertView;

    }


    private static class ViewHolder {

        public TextView valor;
        public ImageView foto;
        public TextView nombre;
        public LinearLayout layout;



    }
}
