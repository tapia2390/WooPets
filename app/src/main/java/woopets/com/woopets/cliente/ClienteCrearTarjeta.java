package woopets.com.woopets.cliente;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.FirebaseFunctionsException;
import com.google.firebase.functions.HttpsCallableResult;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.TPaga;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.CmdIp;
import woopets.com.woopets.cliente.Comandos.CmdIp.OnIpListener;
import woopets.com.woopets.cliente.Comandos.CmdPayU;
import woopets.com.woopets.cliente.Comandos.CmdPayU.OnCheckTarjetas;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas;
import woopets.com.woopets.cliente.Comandos.ComandoTPaga;
import woopets.com.woopets.payU.PendientePayU;
import woopets.com.woopets.tpaga.Model.CardTarjeta;
import woopets.com.woopets.tpaga.Model.CustomerCard;
import woopets.com.woopets.tpaga.Model.CustomerCardResponse;
import woopets.com.woopets.tpaga.Model.TarjetaResponse;
import woopets.com.woopets.tpaga.io.MyApiAdapter;
import woopets.com.woopets.tpaga.io.MyApiAdapterCardTarjeta;

public class ClienteCrearTarjeta extends Activity {


    ModelCliente modelc = ModelCliente.getInstance();

    private FirebaseFunctions mFunctions;

    private ProgressDialog progressDialog;


    EditText nombre, direccion,ciudad, estado, cedula, telefono, correo,numero, meso, ano, cuotas, codigo;
    //MiniTarjeta mini;
    TextView info;

    Button aceptar;

    String origen = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_cliente_metodo_pago);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        origen = getIntent().getStringExtra("ORIGEN") ;

        progressDialog = new ProgressDialog(this);

        mFunctions = FirebaseFunctions.getInstance();

        ScrollView scroll = (ScrollView) findViewById(R.id.scroll);
        nombre = (EditText) findViewById(R.id.nombre);
        direccion = (EditText) findViewById(R.id.direccion);
        ciudad = (EditText) findViewById(R.id.ciudad);
        estado = (EditText) findViewById(R.id.estado);

        correo = (EditText) findViewById(R.id.correo);
        telefono = (EditText) findViewById(R.id.telefono);
        cedula = (EditText) findViewById(R.id.cedula);

        numero = (EditText) findViewById(R.id.numero);
        meso = (EditText) findViewById(R.id.mes);
        ano = (EditText) findViewById(R.id.ano);
        codigo = (EditText) findViewById(R.id.codigo);
        cuotas = (EditText) findViewById(R.id.cuotas);
        info = findViewById(R.id.info);
        aceptar = findViewById(R.id.button11);


        ImageView imgTarjeta = (ImageView) findViewById(R.id.imgTarjeta);
        ImageView imgUbicacion = (ImageView) findViewById(R.id.imgUbicacion);

        MiniTarjeta mini = modelc.getTarjetaActiva();


        if (modelc.direcciones.size() > 0) {
            imgUbicacion.setImageResource(R.drawable.imgubicacionok);
        }else{
            imgUbicacion.setImageResource(R.drawable.imgubicacionseleccionnok);
        }


        if (mini == null) {  ///Pilas esto debe ser lo ultimo que se haga
            imgTarjeta.setImageResource(R.drawable.imgtarjetanok);
            Cliente cli = modelc.cliente;
            Direccion dir = modelc.getDireccionActiva();
            nombre.setText(cli.nombre + " " + cli.apellido);
            if ( dir == null ) {
                return;
            }

            direccion.setText(dir.direccion);
            ciudad.setText(dir.ciudad);
            estado.setText(dir.estado);
            correo.setText(cli.correo);
            telefono.setText(cli.celular);

            return;
        }
        else {
            imgTarjeta.setImageResource(R.drawable.imgtarjetaok);

            info.setText("Puedes editar el número de cuotas de tu tarjeta actual o registrar una tarjeta nueva");
            aceptar.setText("Editar cuotas / Registrar tarjeta");
        }


        // Aca llega solo si tiene tarjeta anteriormente
        //nombre.setText(mini.nombre);
        direccion.setText(mini.direccion);
        ciudad.setText(mini.ciudad);
        estado.setText(mini.departamento);
        telefono.setText(mini.telefono);
        correo.setText(mini.correo);
        cedula.setText(mini.cedula);

        numero.setText(mini.lastFour);
        meso.setText("**");
        ano.setText("****");
        codigo.setText("****");
        cuotas.setText(mini.cuotas + "");
        nombre.setText("**** **********");


    }


    @Override
    public void onBackPressed()
    {
        if (origen == null) {
            finish();
        }
        else {
            Intent i = new Intent(getApplicationContext(), HomeCliente.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
        }

    }


    public void didTapFinalizar(View v) {

        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }catch (Exception x){

        }


        if (!huboCambios()){

            if (cuotas.getText().toString().equals("") || cuotas.getText().toString().equals("0")){
                cuotas.setError("Número de cuotas no válidas");
                return;
            }
            MiniTarjeta mini = modelc.getTarjetaActiva();
            mini.cuotas = Integer.valueOf(cuotas.getText().toString());

            new ComandoTPaga(null).updateCuotas(mini.id,mini.cuotas);
            finish();
            return;
        }

        if (!sinErrores()){
            return;
        }

        //mini.ciudad = ciudad.getText().toString();
        //mini.correo = correo.getText().toString();
        //mini.direccion = direccion.getText().toString();

        MiniTarjeta newmini = new MiniTarjeta();
        newmini.lastFour = numero.getText().toString().substring(numero.getText().toString().length() - 4);
        newmini.activo = true;
        newmini.franquicia = Utility.getTipoCreditCard(numero.getText().toString());
        //newmini.token = response.body().getId();
        newmini.cuotas = Integer.valueOf(cuotas.getText().toString());
        newmini.referenceCode = ComandoPreguntas.getLlaveFirebase();
        newmini.id = newmini.referenceCode;
        newmini.ciudad = ciudad.getText().toString();
        newmini.departamento = estado.getText().toString();
        newmini.correo = correo.getText().toString();
        newmini.nombre = nombre.getText().toString();
        newmini.cedula = cedula.getText().toString();
        newmini.numero = numero.getText().toString();
        newmini.securityCode = codigo.getText().toString();
        newmini.direccion = direccion.getText().toString();
        newmini.telefono = telefono.getText().toString();
        newmini.expirationDate = ano.getText().toString() + "/" + meso.getText().toString();
        //newmini.ip = getIp();


        tokenizarCreditCard(modelc.cliente.getMapaToPagoInicial(newmini), newmini);

    }



    public boolean sinErrores() {


        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);     // current year
        int mMes = c.get(Calendar.MONTH)+1;   // current month
        int mes= 5;
        int year = 2020;
        int yearSuma = mYear+10;
        int yearSuma2 = 2020;
        int max_cuotas = 0;



        try {

            year = Integer.parseInt(ano.getText().toString());
            yearSuma = mYear+10;
            yearSuma2 = Integer.parseInt(ano.getText().toString());
            mes = Integer.parseInt(meso.getText().toString());
            max_cuotas  = Integer.parseInt(cuotas.getText().toString());


        }catch (Exception z){

        }


        if (!Utility.isSoloLetrasPuntosEspacios(nombre.getText().toString())){
            nombre.setError("El nombre contiene caracteres incorrectos");
            nombre.requestFocus();
            return false;
        }


        if (cedula.getText().toString().length() < 5) {
            cedula.setError("Debes incluir tu cedula");
            cedula.requestFocus();
            return false;
        }
        if (!TextUtils.isDigitsOnly(cedula.getText())) {
            cedula.setError("La cedula no puede contener letras");
            cedula.requestFocus();
            return false;
        }

        String tel = telefono.getText().toString();

        if (!TextUtils.isDigitsOnly(telefono.getText())) {
            telefono.setError("El teléfono no puede contener letras");
            telefono.requestFocus();
            return false;
        }

        if (telefono.getText().toString().length() == 7 || telefono.getText().toString().length() == 10) {

        } else {
            telefono.setError("El télefono esta muy largo o muy corto");
            telefono.requestFocus();
            return false;

        }


         if(nombre.getText().toString().equals("") || nombre.getText().toString().length()<4){
            nombre.setError("Nombre es obligatorio");
            return  false;
        }
        if(Utility.LunCheck((numero.getText().toString())) == false ) {
            numero.setError("Número de la tarjeta es incorrecto");
            return  false;
        }
        if(numero.getText().toString().equals("") ){
            numero.setError("Número de la tarjeta de crédito incorrecto");
            return  false;
        }
        if(numero.getText().toString().length() < 13 ||  numero.getText().toString().length() > 19){
            numero.setError("Número de la tarjeta de crédito incorrecto");
            return  false;
        }


        if( Utility.getTipoCreditCard(numero.getText().toString()).equals("")) {
            numero.setError("Solo se aceptan VISA, MASTER, AMERICAN y DINERS");
            return  false;
        }

        if(meso.getText().toString().equals("")){
            meso.setError("Mes obligatorio");
            return  false;
        }

         if (!Utility.isNumeric(ano.getText().toString())){
             ano.setError("El año debe ser un número");
             return  false;
         }

         if (!Utility.isNumeric(meso.getText().toString())){
             meso.setError("El año debe ser un número");
             return  false;
         }
         if (mes <1||mes >12){
            meso.setError("Mes erroneo");
            return  false;
        }
        if (ano.getText().toString().equals("") || ano.getText().toString().length()<4){
            ano.setError("Año no válido");
            return  false;
        }
        if (year < mYear  ){
            ano.setError("Año debe ser mayor");
            return  false;
        }

        if (year == mYear &&  mes < mMes ){
            meso.setError("Mes no válido o Tarjeta vencida");
            return  false;
        }

        Date vencimiento = new GregorianCalendar(yearSuma2, mes, 15).getTime();
        int numMeses =  Utility.calcularMesesEntresFechas(new Date(), vencimiento);
        if (numMeses > 120){
            ano.setError("Fecha de vencimiento de la tarjeta esta no es correcto");
            return  false;

         }
        if (!Utility.isNumeric(codigo.getText().toString())){
             codigo.setError("Código de seguridad no válido");
             return  false;
         }
        if (codigo.getText().toString().equals("") || codigo.getText().toString().length()<3 || codigo.getText().toString().length()>4){
            codigo.setError("Código de seguridad no válido");
            return  false;
        }

        if (!Utility.isLongitudCvcOk(numero.getText().toString(), codigo.getText().toString())){
            codigo.setError("Código de seguridad no válido");
            return  false;
        }

        if (cuotas.getText().toString().equals("") || cuotas.getText().toString().equals("0")){
            cuotas.setError("Número de cuotas no válidas");
            return  false;
        }

        if(max_cuotas>36 || max_cuotas <= 0){
            cuotas.setError("Número de cuotas no válidas");
            return  false;

        }

        if (direccion.getText().toString().length() < 4) {
            direccion.setError("Debes incluir la dirección de facturación");
            direccion.requestFocus();
            return false;
        }

        if (ciudad.getText().toString().length() < 4) {
            ciudad.setError("Debes incluir la ciudad de facturación");
            ciudad.requestFocus();
            return false;
        }

        if (estado.getText().toString().length() < 4) {
            estado.setError("Debes incluir el departamento");
            estado.requestFocus();
            return false;
        }

        if (!Utility.isEmailValid(correo.getText().toString())) {
            correo.setError("Debes incluir un correo valido");
            correo.requestFocus();
            return false;
        }


        return true;
    }




    public boolean huboCambios(){


        MiniTarjeta mini = modelc.getTarjetaActiva();
        if (nombre.getText().toString().equals("**** **********")  && (numero.getText().toString().equals(mini.lastFour)
                && meso.getText().toString().equals("**") && ano.getText().toString().equals("****"))
                && codigo.getText().toString().equals("****")){

            return false;
        }


        return true;

    }



    public void tokenizarCreditCard(final Map data, final MiniTarjeta nueva){

        progressDialog.setMessage("Validando la información por favor espere...");
        progressDialog.show();


        CmdIp.getIp(this, new OnIpListener() {
            @Override
            public void gotIp(String ip) {

                nueva.ip = ip;
                ((Map)data.get("tarjeta")).put("ipAddress",ip);
                hacerPagoTemporalConTarjeta(data, nueva);
            }

            @Override
            public void fallo() {

                showAlertSinInternet("Fallo en la transacción", "No pudo iniciar la transacción. Intente más tarde");

            }
        });


    }



    private Task<Map> reembolsarPago(Map data) {



        return mFunctions
                .getHttpsCallable("reembolsarPago")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        Map result = (Map) task.getResult().getData();
                        return result;
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     return;
                                                 }



                                                 Map result = (Map)task.getResult().get("transactionResponse");

                                                 if (result == null){
                                                     Toast.makeText(getApplicationContext(),"Fallo al hacer el reverso, pruebas?",Toast.LENGTH_LONG).show();

                                                 }else {

                                                     Toast.makeText(getApplicationContext(), "Reembolso: " + result.get("state").toString(), Toast.LENGTH_LONG).show();
                                                 }

                                                 progressDialog.dismiss();

                                             }
                                         }
                );
    }




    private Task<String> hacerPagoTemporalConTarjeta(final Map data, final MiniTarjeta nueva) {


        return mFunctions
                .getHttpsCallable("tarjetaAprobada")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        CmdPayU.checkTarjetasPayU(nueva.referenceCode,"clientes", modelc.cliente.id, new OnCheckTarjetas() {
                            @Override
                            public void gotError(String error) {
                                progressDialog.dismiss();
                                showAlertSinInternet("Fallo en la transacción", "Problema con el sistema de pagos: " + error +   " \nPor favor intente más tarde" );
                                return;

                            }

                            @Override
                            public void gotDeclined(String error) {
                                try {
                                    modelc.apagarTarjetas();
                                    nueva.estado = "DECLINED";
                                    nueva.activo = false;
                                    new ComandoTPaga(null).setRegistroCredidCard(nueva);
                                    progressDialog.dismiss();
                                    showAlertSinInternet("Fallo en la transacción", "La tarjeta fue rechazada: " + error +  "\nIntenta con una nueva tarjeta." );
                                }catch (Exception e){

                                }
                            }

                            @Override
                            public void gotPending(MiniTarjeta leida) {
                                MiniTarjeta actual = modelc.getTarjetaActiva();    //1.
                                if (actual != null ) {
                                    borrarToken(actual.token);                         //2.
                                }                      //2.
                                modelc.apagarTarjetas();                           //3.
                                modelc.tarjetas.add(leida);                        //4. Pilas el orden es impotante

                                progressDialog.dismiss();
                                nueva.estado = "PENDING";
                                showAlertTarjetaPendiente("Validación Tarjeta", "La tarjeta inscrita será validada por personal de payU y tomará de 1 a 4 horas dicho proceso. Te llegará una notificación informandote si tu tarjeta podrá ser usada para tus compras.");

                            }

                            @Override
                            public void gotApproved(MiniTarjeta leida) {
                                MiniTarjeta actual = modelc.getTarjetaActiva();    //1.
                                if (actual != null ) {
                                    borrarToken(actual.token);                         //2.
                                }
                                modelc.apagarTarjetas();                           //3.
                                modelc.tarjetas.add(leida);                        //4. Pilas el orden es impotante
                                progressDialog.dismiss();


                                if (origen != null){
                                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();

                                } else {
                                    finish();;
                                }


                            }
                        });

                        try {
                            String result = (String) task.getResult().getData();

                            return result;

                        }catch (Exception e){
                            return  null;
                        }
                    }
                });
    }


    private Task<Map> hacerPagoTemporalConTarjetaViejo(Map data, final MiniTarjeta mini) {     //Siempre se tokeniza y siempre se reversa



        return mFunctions
                .getHttpsCallable("pagoConTarjetaN")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        try {
                            Map result = (Map) task.getResult().getData();
                            return result;

                        }catch (Exception e){
                            return  null;
                        }
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }
                                                     progressDialog.dismiss();
                                                     showAlertSinInternet("Fallo en la transacción", "Problema con el sistema de pagos, por favor intente mas tarde. COD:A1");

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     return;
                                                 }


                                                 try {
                                                     Map result = (Map) task.getResult().get("transactionResponse");
                                                     String idOrdenPayU = result.get("orderId").toString();
                                                     String transactionId = result.get("transactionId").toString();
                                                     String status = result.get("state").toString();
                                                     String responseCode = result.get("responseCode").toString();

                                                     if (status.equals("DECLINED")){
                                                         progressDialog.dismiss();
                                                         showAlertSinInternet("Fallo en la transacción", "La tarjeta fue rechazada.  COD:" + responseCode );
                                                         return;

                                                     }

                                                     if (status.equals("PENDING")){
                                                         progressDialog.dismiss();

                                                         modelc.apagarTarjetas();
                                                         modelc.tarjetas.add(mini);
                                                         mini.estado = status;


                                                         new ComandoTPaga(null).setRegistroCredidCard(mini);

                                                         PendientePayU pend = new PendientePayU();

                                                         pend.id = mini.id;
                                                         pend.state = "PENDING";
                                                         pend.orderId = idOrdenPayU;
                                                         pend.tipo = "Cliente-Inscripcion";
                                                         pend.tokenDevice = modelc.cliente.tokenDevice;
                                                         pend.transactionId  = transactionId;
                                                         pend.uid = modelc.cliente.id;

                                                         CmdPayU.registrarPendiente(pend);

                                                         progressDialog.dismiss();

                                                         showAlertTarjetaPendiente("Validación Tarjeta", "La tarjeta inscrita será validada por personal de payU y tomará de 1 a 4 horas dicho proceso. Te llegará una notificación informandote si tu tarjeta podrá ser usada para tus compras.");

                                                         return;

                                                     }


                                                     Toast.makeText(getApplicationContext(),"Pago " + result.get("state").toString(),Toast.LENGTH_LONG).show();


                                                     Map<String, Object> data = new HashMap<>();
                                                     data.put("idOrdenPayU", idOrdenPayU);
                                                     data.put("transactionId", transactionId);

                                                     reembolsarPago(data);  // Con exito o fracaso continua la ejecucion, solo dejar un log

                                                     tokenizar(modelc.cliente.getMapaToTokenizar(mini),mini);

                                                 }
                                                 catch (Exception e ){
                                                     progressDialog.dismiss();
                                                     Log.e("ERROR:", e.getMessage());
                                                     showAlertSinInternet("Fallo en la transacción", "Problema con el sistema de pagos, por favor intente mas tarde. COD:A3");
                                                     return;

                                                 }


                                             }
                                         }
                );
    }




    private Task<Map> tokenizar(Map data, final MiniTarjeta mini) {


        return mFunctions
                .getHttpsCallable("tokenizando")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        Map result = (Map) task.getResult().getData();
                        return result;
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     return;
                                                 }

                                                 Map result = (Map)task.getResult().get("creditCardToken");
                                                 Toast.makeText(getApplicationContext(),result.get("creditCardTokenId").toString(),Toast.LENGTH_LONG).show();

                                                 mini.token = result.get("creditCardTokenId").toString();
                                                 mini.lastFour = result.get("maskedNumber").toString();
                                                 mini.estado = result.get("state").toString();;


                                                 modelc.apagarTarjetas();
                                                 modelc.tarjetas.add(mini);
                                                 new ComandoTPaga(null).setRegistroCredidCard(mini);

                                                 progressDialog.dismiss();


                                                 if (origen != null){
                                                     Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                                                     i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                     startActivity(i);
                                                     finish();

                                                 } else {
                                                     finish();;
                                                 }



                                             }
                                         }
                );
    }

    private Task<String> borrarToken(String token) {


        Map<String, Object> data = new HashMap<>();
        data.put("token", token);

        return mFunctions
                .getHttpsCallable("borrarToken")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {

                        try {
                            String result = (String) task.getResult().getData();
                            return result;
                        }
                        catch (Exception e) {
                            return null;
                        }
                    }
                });
    }




    /*public String getIp(){

        String ipWifi = getWifiIPAddress();

        if (!ipWifi.equals("0.0.0.0") && !ipWifi.equals("")){
            return  ipWifi;
        }

        return getMobileIPAddress();

    }*/

    public String getWifiIPAddress() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        return  Formatter.formatIpAddress(ip);
    }


    public static String getMobileIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return  addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            return "0.0.0.0";
        }
        return "0.0.0.0";
    }





    public void showAlertSinInternet(String titulo, String mensaje){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ClienteCrearTarjeta.this);

        // set title
        alertDialogBuilder.setTitle(""+titulo);

        // set dialog message
        alertDialogBuilder
                .setMessage(""+mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public void showAlertTarjetaPendiente(String titulo, String mensaje){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ClienteCrearTarjeta.this);

        // set title
        alertDialogBuilder.setTitle(""+titulo);

        // set dialog message
        alertDialogBuilder
                .setMessage(""+mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();

                        if (origen != null){
                            Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();

                        } else {
                            finish();;
                        }



                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }



}
