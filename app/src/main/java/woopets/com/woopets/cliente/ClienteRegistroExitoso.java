package woopets.com.woopets.cliente;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCompras;
import woopets.com.woopets.cliente.Comandos.ComandoCompras.OnComandoComprasListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTips;

public class ClienteRegistroExitoso extends Activity {

    ModelCliente modelc = ModelCliente.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_registro_exitoso);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (modelc.tipoLogeo == "faceb") {
            ImageView ingreso = (ImageView) findViewById(R.id.imagenIngreso);
            ingreso.setImageResource(R.drawable.imgfbok);
        }

    }


    public void didTapContinuarRegistro(View v){
        Intent i = new Intent(getApplicationContext(), ClienteRegistroBasicos.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        finish();
    }

    public void  didTapDespues(View v){

        preCargar();


    }


    private void preCargar() {

        String tipo = "";
        Mascota mascota = modelc.getMascotaActiva();
        if (mascota != null ) {
            tipo = mascota.tipo;
        }


        new ComandoCompras(new OnComandoComprasListener() {
            @Override
            public void cargoNuevaCompra() {

            }

            @Override
            public void cargoModificacionCompra() {

            }
        }).getComprasAbiertas(modelc.cliente.id);

        new ComandoCompras(new OnComandoComprasListener() {
            @Override
            public void cargoNuevaCompra() {

            }

            @Override
            public void cargoModificacionCompra() {

            }
        }).getComprasCerradas(modelc.cliente.id);



        new ComandoCalificacion(null).getMisCalificaciones(modelc.cliente.id);
        new ComandoTips().getTips();
        new ComandoTips().getPolitica();
        new ComandoAyuda(null).getInicales();
        CmdNoti.getInstance().getNotificaciones();

        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {
                //Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                //startActivity(i);
                //finish();

            }

            @Override
            public void cargoPublicacion() {
                ///Pilas aca no puede ir nada// o jamas se va a llamar
            }
        }).getLast6Destacados(tipo);



        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {
                ///Pilas aca no puede ir nada// o jamas se va a llamar
            }

            @Override
            public void cargoPublicacion() {
                Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                startActivity(i);
                finish();
            }
        }).getTodasPublicaciones(tipo);




    }




}
