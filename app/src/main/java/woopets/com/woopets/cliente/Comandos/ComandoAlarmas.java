package woopets.com.woopets.cliente.Comandos;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Alarma;
import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/25/17.
 */

public class ComandoAlarmas {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");





    public ComandoAlarmas(){


    }

    public void eliminarAlarma(String idCliente, String idMascota, String idAlarma){
        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/mascotas/"+idMascota+"/alertas/"+idAlarma);
        ref.removeValue();
    }

    public void setEstadoAlarma(String idCliente, String idMascota, String idAlarma, boolean estado){
        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/mascotas/"+idMascota+"/alertas/"+idAlarma+"/activada");
        ref.setValue(estado);
    }


    public String getLLave(){

        return  referencia.push().getKey();
    }

    public void crearAlarma(String idCliente, String idMascota, Alarma alarma){
        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/mascotas/"+idMascota+"/alertas/"+alarma.id);
        ref.setValue(alarma);

    }

    public void updateAlarma(String idCliente, String idMascota, Alarma alarma){
        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/mascotas/"+idMascota+"/alertas/"+alarma.id);

        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("activada", alarma.activada);
        mapa.put("fechaFin",  alarma.fechaFin);
        mapa.put("fechaInicio",  alarma.fechaInicio);
        mapa.put("frecuencia",  alarma.frecuencia);
        mapa.put("hora",  alarma.hora);
        mapa.put("id",  alarma.id);
        mapa.put("idAlarmaManager",  alarma.idAlarmaManager);
        mapa.put("nombre",  alarma.nombre);
        mapa.put("tipoRecordatorio",alarma.tipoRecordatorio);

        ref.updateChildren(mapa);

    }





}





