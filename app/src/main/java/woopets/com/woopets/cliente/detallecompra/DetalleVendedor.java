package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.Oferente;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;

public class DetalleVendedor extends Activity {

    ModelCliente modelc = ModelCliente.getInstance();

    TextView diasUno, diasDos, horaUno, horaDos, jornadaUno, jornadaDos , razonSocial;

    LinearLayout layHoraUno, layHoraDos;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_vendedor);


        diasUno = (TextView) findViewById(R.id.diasUno);
        diasDos = (TextView) findViewById(R.id.diasDos);
        horaUno = (TextView) findViewById(R.id.hora1);
        horaDos = (TextView) findViewById(R.id.hora2);
        jornadaUno = (TextView) findViewById(R.id.jornadaUno);
        jornadaDos = (TextView) findViewById(R.id.jornadaDos);
        razonSocial = (TextView) findViewById(R.id.razonsocial);

        layHoraUno = (LinearLayout) findViewById(R.id.layHorarioUno);
        layHoraDos = (LinearLayout) findViewById(R.id.layHorarioDos);

        String idPublicacion = getIntent().getStringExtra("IDPUBLICACION");
        Publicacion publi = modelc.getPublicacionById(idPublicacion);
        Oferente oferente = publi.oferente;


        razonSocial.setText(oferente.razonSocial);


        if (oferente.horarios.size() > 0 ) {
            Horario hUno = oferente.horarios.get(0);
            diasUno.setText(hUno.dias);
            horaUno.setText(hUno.horaInicio + " : " + hUno.horaCierre );
            if (hUno.sinJornadaContinua) {
                jornadaUno.setText("Cerramos entre 12 y 2:00 PM");
            }

        }
        if (oferente.horarios.size() > 1) {
            Horario hDos = oferente.horarios.get(1);
            diasDos.setText(hDos.dias);
            horaDos.setText(hDos.horaInicio + " : " + hDos.horaCierre );
            if (hDos.sinJornadaContinua) {
                jornadaDos.setText("Cerramos entre 12 y 2:00 PM");
            }
        }

        if (oferente.horarios.size() == 1) {
            layHoraDos.setVisibility(View.GONE);
        }


    }
}
