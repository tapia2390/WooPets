package woopets.com.woopets.cliente.Comandos;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by andres on 8/10/18.
 */

public class CmdIp {


    public interface OnIpListener {
        void gotIp(String ip);
        void fallo();

    }


    public static void getIp(Context context, final OnIpListener listener){

        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="https://us-central1-mypetsdesarrollo.cloudfunctions.net/ip";


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        String parts[] = response.split(",");
                        listener.gotIp(parts[1]);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.fallo();

            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);



    }

}
