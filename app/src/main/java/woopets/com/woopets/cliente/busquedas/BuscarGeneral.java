package woopets.com.woopets.cliente.busquedas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;


public class BuscarGeneral extends Activity implements OnImageFireChange {


    private ListView listaView;
    BuscarGeneralAdapter adapter;
    ModelCliente modelc = ModelCliente.getInstance();

    ArrayList<Publicacion> publicaciones;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_general);

        modelc.filtro.clear();
        listaView = findViewById(R.id.lista);

        if (getIntent().hasExtra("CATEGORIA")){
            final String categoria = getIntent().getStringExtra("CATEGORIA");

            Mascota mascota = modelc.getMascotaActiva();

            if (mascota == null) {
                    publicaciones = modelc.getPublicacionesXCategoria(categoria);
            } else {
                    publicaciones = modelc.getPublicacionesXCategoriaPeroConMascota(categoria, mascota);
            }

            adapter = new BuscarGeneralAdapter(getBaseContext(),this, publicaciones);
        }else {

            Mascota mascota = modelc.getMascotaActiva();

            if (mascota == null) {
                publicaciones = modelc.getPublicacionesXCategoria("");
            } else {
                publicaciones = modelc.getPublicacionesXCategoriaPeroConMascota("", mascota);
            }
            adapter = new BuscarGeneralAdapter(getBaseContext(),this, publicaciones);
        }

        listaView.setAdapter(adapter);

        final SearchView search = findViewById(R.id.search);
        search.setFocusable(false);


        search.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);

                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                return false;
            }
        });



        listaView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                Publicacion publi;


                if (modelc.filtro.size() > 0){
                    publi = modelc.filtro.get(pos);
                } else {
                        publi = publicaciones.get(pos);
                }

                Intent i = new Intent(getBaseContext(), DetallePublicacion.class);
                i.putExtra("IDPUBLICACION",publi.id);
                startActivity(i);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

            }
        });



    }

    @Override
    public void cargoImagen(String tipo) {
        adapter.notifyDataSetChanged();
    }
}
