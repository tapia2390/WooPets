package woopets.com.woopets.cliente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Notificacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 11/14/17.
 */

public class CmdNoti {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    private ArrayList<OnCmdNotificacionesListener> listeners = new ArrayList<>();

    private static final CmdNoti ourInstance = new CmdNoti();

    public static CmdNoti getInstance() {
        return ourInstance;

    }


    public interface OnCmdNotificacionesListener {

        void cargoNotificacion();

    }


    public void registrarListener (OnCmdNotificacionesListener listener) {
        listeners.add(listener);
    }


    public void llegoNotificacion(){
        for (OnCmdNotificacionesListener listener:listeners){
            listener.cargoNotificacion();
        }

    }

    private CmdNoti() {


    }


    public void getNotificaciones(){

        DatabaseReference ref = database.getReference("mensajes");

        Query query = ref.orderByChild("idCliente").equalTo(modelc.cliente.id);

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {

                modelc.addNotificacion(readNotificacion(snap));
                modelc.sortMisNotificaciones();
                llegoNotificacion();
            }

            @Override
            public void onChildChanged(DataSnapshot snap, String s) {
                modelc.addNotificacion(readNotificacion(snap));
                modelc.sortMisNotificaciones();
                llegoNotificacion();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        query.addChildEventListener(listener);

        modelc.hijosListener.put(query,listener);

    }




    public void actualizarTokenDevice(String tokenDevice){
        if ((tokenDevice == null) || tokenDevice.equals("") ){
            return;
        }

        DatabaseReference ref = database.getReference("clientes/"+modelc.cliente.id + "/tokenDevice");
        ref.setValue(tokenDevice);

    }


    private Notificacion readNotificacion(DataSnapshot snap) {

        Notificacion noti = snap.getValue(Notificacion.class);
        noti.id = snap.getKey();
        return noti;
    }



    public void marcarComoVista(String idMensaje){

        DatabaseReference ref = database.getReference("mensajes/"+idMensaje + "/visto");
        ref.setValue(true);

    }

    public void eliminarMensaje(String idMensaje){


        DatabaseReference ref = database.getReference("mensajes/"+idMensaje);
        ref.removeValue();

    }



}
