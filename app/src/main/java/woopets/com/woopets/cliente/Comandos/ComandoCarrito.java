
package woopets.com.woopets.cliente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Alarma;
import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.TPaga;
import woopets.com.woopets.Modelo.cliente.Ubicacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.ModelCliente;


/**
 * Created by andres on 10/1/17.
 */

public class ComandoCarrito {

    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");



    public interface OnComandoCarritoChangeListener {

        void cargoCarrito();

    }


    public ComandoCarrito(){


    }



    public void getCarritos(String uid, final OnComandoCarritoChangeListener mListener) {


        DatabaseReference ref = database.getReference();

        ref = database.getReference("clientes/" + uid + "/carrito");
        ChildEventListener listener = new ChildEventListener(){
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {
                ItemCarrito item = readCarrito(snap);
                modelc.addCarrito(item);
                agregarPublicacion(item.idPublicacion);
                mListener.cargoCarrito();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot snap) {
                mListener.cargoCarrito();

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        ref.addChildEventListener(listener);
        modelc.hijosListenerRef.put(ref,listener);


    }





    public void agregarPublicacion(String idPublicacion){
        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {

            }

            @Override
            public void cargoPublicacion() {

            }
        }).getPublicacion(idPublicacion);
    }




    public ItemCarrito readCarrito(DataSnapshot snap){

        ItemCarrito item = snap.getValue(ItemCarrito.class);
        item.id = snap.getKey();
        return item;

    }


    public  void updateMetodoPago(String metodo){

        Cliente cli = modelc.cliente;
        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/metodoDePago");
        ref.setValue(metodo);


    }


    public void crearMascota(Mascota newMascota) {

        Cliente cli = modelc.cliente;

        if (newMascota.nombre.equals("")){
            return;
        }

        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/mascotas/"+newMascota.id);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Map<String, Object> mascota = new HashMap<String, Object>();

        mascota.put("activa", true);
        mascota.put("fechaNacimiento", dateFormat.format(newMascota.fechaNacimiento));
        mascota.put("foto","FotoTuMascota");
        mascota.put("genero",newMascota.genero);
        mascota.put("nombre",newMascota.nombre);
        mascota.put("genero",newMascota.genero);
        mascota.put("raza", newMascota.raza);
        mascota.put("tipo", newMascota.tipo);

        ref.setValue(mascota);

    }

    public void updateMascota(Mascota actualMascota) {

        Cliente cli = modelc.cliente;

        if (actualMascota.nombre.equals("")){
            return;
        }

        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/mascotas/"+actualMascota.id);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Map<String, Object> mascota = new HashMap<String, Object>();

        mascota.put("activa", true);
        mascota.put("fechaNacimiento", dateFormat.format(actualMascota.fechaNacimiento));
        mascota.put("foto","FotoTuMascota");
        mascota.put("genero",actualMascota.genero);
        mascota.put("nombre",actualMascota.nombre);
        mascota.put("genero",actualMascota.genero);
        mascota.put("raza", actualMascota.raza);
        mascota.put("tipo", actualMascota.tipo);

        ref.updateChildren(mascota);

    }


    public void activarMascota(String idMascota, String idCliente){

        for (Mascota masc:modelc.mascotas){
            masc.activa = false;
            if (masc.id.equals(idMascota)){
                masc.activa = true;
            }

            DatabaseReference ref = database.getReference("clientes/"+idCliente+"/mascotas/"+masc.id+"/activa");
            ref.setValue(false);

        }

        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/mascotas/"+idMascota+"/activa");
        ref.setValue(true);

    }

    public void activarDireccion(String idDireccion, String idCliente){

        for (Direccion dir:modelc.direcciones){
            dir.porDefecto = false;
            if (dir.id.equals(idDireccion)){
                dir.porDefecto = true;
            }

            DatabaseReference ref = database.getReference("clientes/"+idCliente+"/direcciones/"+dir.id+"/porDefecto");
            ref.setValue(false);

        }

        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/direcciones/"+idDireccion+"/porDefecto");
        ref.setValue(true);

    }





    public void eliminarMascota(String idMascota, String idCliente) {

        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/mascotas/"+idMascota);
        ref.removeValue();
    }


    public void eliminarDireccion(String idDireccion, String idCliente) {

        DatabaseReference ref = database.getReference("clientes/"+idCliente+"/direcciones/"+idDireccion);
        ref.removeValue();
    }




    public String getLLave(){

        return  referencia.push().getKey();
    }

    public void adicionarAlCarrito(ItemCarrito item) {

        Cliente cli = modelc.cliente;


        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/carrito/"+item.id);

        Map<String, Object> mapa = new HashMap<String, Object>();

        mapa.put("cantidadCompra", item.cantidadCompra);
        mapa.put("idPublicacion", item.idPublicacion);
        mapa.put("servicio", false);

        ref.setValue(mapa);

    }

    public void eliminarDelCarrito(String idCarrito) {

        Cliente cli = modelc.cliente;


        DatabaseReference ref = database.getReference("clientes/" + cli.id + "/carrito/" + idCarrito);


        ref.removeValue();

    }

    public void adicionarAFavoritos(String idPublicacion) {

        Cliente cli = modelc.cliente;

        DatabaseReference ref = database.getReference("clientes/"+cli.id+"/favoritos/"+idPublicacion);

        ref.setValue(true);

    }


    public void eliminarDeFavoritos(String idPublicacion) {

        Cliente cli = modelc.cliente;

        DatabaseReference ref = database.getReference("clientes/" + cli.id + "/favoritos/" + idPublicacion);

        ref.removeValue();

    }




    private void getLlavesTPaga(){

        DatabaseReference ref = database.getReference();

        ref = database.getReference("params");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelc.tpaga.endPoint = (String) snap.child("tpendpoint").getValue();
                modelc.tpaga.publicKey = (String) snap.child("tppublica").getValue();
                modelc.tpaga.privateKey = (String) snap.child("tpprivada").getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }



}
