package woopets.com.woopets.cliente.menu;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.ArrayList;
import woopets.com.woopets.Modelo.ImageFire;
import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;

/**
 * Created by andres on 10/24/17.
 */

public class TipsUsoAdapter  extends PagerAdapter {


    private ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private OnImageFireChange listener;


    private ArrayList<ImageFire> destacados;
    ImageView fotoDestacado;

    public TipsUsoAdapter(Context context, OnImageFireChange listener) {
        mContext = context;
        this.listener = listener;

        destacados = modelc.tips;


    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {


        final int pos = position;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        FrameLayout layout = (FrameLayout) inflater.inflate(R.layout.layout_tip_page, container, false);
        fotoDestacado = (ImageView) layout.findViewById(R.id.foto);
        Bitmap foto = destacados.get(position).getFoto("TIP");
        destacados.get(position).setListener(listener);

        if (foto != null) {
            fotoDestacado.setImageBitmap(foto);
        }

        container.addView(layout);

        return layout;

    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return destacados.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}




