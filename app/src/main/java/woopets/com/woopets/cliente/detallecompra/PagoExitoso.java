package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.EstadosCompra;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;

public class PagoExitoso extends Activity {



    TextView lbServicio, nombre, cantidad;
    LinearLayout layerProducto, layerCompraCerrada, layerCalificacion;
    FrameLayout layerServicio;
    ImageView star1, star2, star3,star4,star5, favorito;

    TextView    fechaServicio, direccionDomicilio, direccionEnvio, telefono, valor, fechaCompraCerrada, fechaCalificaion, comentario;
    TextView pol1, pol2;

    ModelCliente modelc = ModelCliente.getInstance();




    Compra compra;
    Publicacion publi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pago_exitoso);

        lbServicio = (TextView) findViewById(R.id.lbServicio);
        nombre = (TextView) findViewById(R.id.nombre);
        cantidad = (TextView) findViewById(R.id.cantidad);
        fechaServicio = (TextView) findViewById(R.id.fechaServicio);
        direccionDomicilio = (TextView) findViewById(R.id.direccionDomicilio);
        direccionEnvio = (TextView) findViewById(R.id.direccionEnvio);
        telefono = (TextView) findViewById(R.id.telefono);
        valor = (TextView) findViewById(R.id.valor);
        fechaCompraCerrada = (TextView) findViewById(R.id.fechaCompraCerrada);
        fechaCalificaion = (TextView) findViewById(R.id.fechaCalificacion);
        comentario = (TextView) findViewById(R.id.comentario);
        layerServicio = (FrameLayout) findViewById(R.id.layerServicio);
        layerProducto = (LinearLayout) findViewById(R.id.layerProduto);
        layerCompraCerrada =  (LinearLayout) findViewById(R.id.layerCompraCerrada);
        layerCalificacion =  (LinearLayout) findViewById(R.id.layerCalificacion);
        TextView titulo = (TextView) findViewById(R.id.titulo);
        pol1 = (TextView) findViewById(R.id.pol1);
        pol2 = (TextView) findViewById(R.id.pol2);



        star1 = (ImageView) findViewById(R.id.estre1);
        star2 = (ImageView) findViewById(R.id.estre2);
        star3 = (ImageView) findViewById(R.id.estre3);
        star4 = (ImageView) findViewById(R.id.estre4);
        star5 = (ImageView) findViewById(R.id.estre5);


        layerServicio.setVisibility(View.GONE);
        layerProducto.setVisibility(View.GONE);
        layerCompraCerrada.setVisibility(View.GONE);
        layerCalificacion.setVisibility(View.GONE);


        String idCompra = getIntent().getStringExtra("IDCOMPRA");


        compra = modelc.getCompraById(idCompra);
        publi = modelc.getPublicacionById(compra.idPublicacion);


        nombre.setText(publi.nombre);
        cantidad.setText(compra.cantidad+"");
        telefono.setText(publi.oferente.telefono);
        valor.setText(Utility.convertToMoney(compra.valor ) );

        if (publi.servicio) {
            lbServicio.setText("El servicio:");
            layerServicio.setVisibility(View.VISIBLE);
            fechaServicio.setText(compra.fechaServicio);
            direccionDomicilio.setText(compra.direccion);
            titulo.setText("Compra Agendada");


        } else { // Si es un producto
            lbServicio.setText("El producto:");
            layerProducto.setVisibility(View.VISIBLE);
            telefono.setText(modelc.cliente.celular);
            direccionEnvio.setText(compra.direccion);

            if (compra.estado.equals((EstadosCompra.PENDIENTE.toString()))){
                pol1.setVisibility(View.VISIBLE);
                pol2.setVisibility(View.VISIBLE);
            }
        }

        if (compra.estado.equals((EstadosCompra.CERRADA.toString()))){
            layerCompraCerrada.setVisibility(View.VISIBLE);
            fechaCompraCerrada.setText(compra.fechaCierre);
        }

        Calificacion cali = modelc.getCalificacionToCompra(compra.id);
        if ( cali != null){
            layerCalificacion.setVisibility(View.VISIBLE);
            comentario.setText(cali.comentario);
            fechaCalificaion.setText(cali.fecha);
            pintarEstrellas(cali.calificacion);

        }


    }


    private void pintarEstrellas(double calificacion){
        pintarApagarPrenderEstrella(star1,1,calificacion);
        pintarApagarPrenderEstrella(star2,2,calificacion);
        pintarApagarPrenderEstrella(star3,3,calificacion);
        pintarApagarPrenderEstrella(star4,4,calificacion);
        pintarApagarPrenderEstrella(star5,5,calificacion);
    }



    public void didTapAceptar(View v){

        String origen = "";
        if (getIntent().hasExtra("ORIGEN")) {
            origen = getIntent().getStringExtra("ORIGEN");
        }

        if (origen.equals("ENCOMPRA")){
            Intent intent = new Intent(this, HomeCliente.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();


        }
        else{
            finish();
        }


    }

    @Override
    public void onBackPressed() {
        String origen = "";
        if (getIntent().hasExtra("ORIGEN")) {
            origen = getIntent().getStringExtra("ORIGEN");
        }

        if (origen.equals("ENCOMPRA")){
            Intent intent = new Intent(this, HomeCliente.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();


        }
        else{
            finish();
        }

    }



    private void pintarApagarPrenderEstrella(ImageView star, double pos, double nota){

        if (nota >= pos) {
            star.setImageResource(R.drawable.imgestrellacompleta);
            return;
        }

        if (Math.abs(nota-pos) <= 0.5) {
            star.setImageResource(R.drawable.imgestrellamedia);
            return;
        }

        star.setImageResource(R.drawable.imgestrellavacia);


    }

}
