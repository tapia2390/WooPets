package woopets.com.woopets.cliente.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.TPaga;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ClienteCrearTarjeta;
import woopets.com.woopets.cliente.ClienteRegistro;
import woopets.com.woopets.cliente.ClienteRegistroBasicos;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.CmdNoti.OnCmdNotificacionesListener;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.menu.Ayuda;
import woopets.com.woopets.cliente.ClienteTpaga;
import woopets.com.woopets.cliente.menu.PoliticaUso;
import woopets.com.woopets.cliente.menu.TipsUso;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by andres on 10/19/17.
 */

public class MenuFragmento extends Fragment  implements OnClickListener{


    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser user = mAuth.getCurrentUser();
    ModelCliente modelc = ModelCliente.getInstance();

    TextView numeroNoti, version;

    FrameLayout layerNoti;

    ImageView fb,twitter,instagram;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.tab_menu, container, false);

        LinearLayout miPerfil = (LinearLayout) v.findViewById(R.id.miPerfil);
        LinearLayout metodo = (LinearLayout) v.findViewById(R.id.metodo);
        LinearLayout tips = (LinearLayout) v.findViewById(R.id.tips);
        LinearLayout notificaciones = (LinearLayout) v.findViewById(R.id.notificaciones);
        LinearLayout aviso = (LinearLayout) v.findViewById(R.id.aviso);
        LinearLayout ayuda = (LinearLayout) v.findViewById(R.id.ayuda);
        LinearLayout cerrar = (LinearLayout) v.findViewById(R.id.cerrar);
        version = v.findViewById(R.id.version);


        fb = (ImageView) v.findViewById(R.id.fb);
        twitter = (ImageView) v.findViewById(R.id.twitter);
        instagram = (ImageView) v.findViewById(R.id.instagram);


        layerNoti = (FrameLayout) v.findViewById(R.id.layerrojo);
        TextView lbperfil = (TextView) v.findViewById(R.id.lbPerfil);
        Space spacioMedio = (Space) v.findViewById(R.id.espacioMedio);

        numeroNoti = (TextView) v.findViewById(R.id.numeroNoti);

        layerNoti.setVisibility(View.GONE);

        version.setText(Utility.getVersionParaUsuario(getActivity()));



        miPerfil.setOnClickListener(this);
        metodo.setOnClickListener(this);
        tips.setOnClickListener(this);
        notificaciones.setOnClickListener(this);
        aviso.setOnClickListener(this);
        ayuda.setOnClickListener(this);
        cerrar.setOnClickListener(this);


        pintarNotificaciones();


        CmdNoti.getInstance().registrarListener(new OnCmdNotificacionesListener() {
            @Override
            public void cargoNotificacion() {
                pintarNotificaciones();
            }
        });


        if (modelc.tipoLogeo.equals("anonimo")){
            lbperfil.setText("Registrate");
            metodo.setVisibility(View.GONE);
            //cerrar.setVisibility(View.GONE);
            spacioMedio.setVisibility(View.GONE);
        }

        fb.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/WoopetsApp"));
                startActivity(browserIntent);

            }
        });


        twitter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/WoopetsApp"));
                startActivity(browserIntent);

            }
        });

        instagram.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/woopetsapp"));
                startActivity(browserIntent);

            }
        });


        return  v;

    }



    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.miPerfil: didTapMiPerfil(view);
                break;
            case R.id.metodo: didTapMetodo(view);
                break;
            case R.id.notificaciones: didTapNotificaciones(view);
                break;
            case R.id.tips: didTapTips(view);
                break;
            case R.id.ayuda: didTapAyuda(view);
                break;
            case R.id.aviso: didTapAviso(view);
                break;
            case R.id.cerrar : didTapCerrarSesion(view);
                break;

        }

    }


    public void didTapMiPerfil(View view){

        if (modelc.tipoLogeo.equals("anonimo")){
            Intent i = new Intent(getApplicationContext(),ClienteRegistro.class);
            startActivity(i);
            getActivity().overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

        }else {
            Intent i = new Intent(getApplicationContext(),ClienteRegistroBasicos.class);
            startActivity(i);
            getActivity().overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        }


    }


    public void didTapNotificaciones(View view){

        Intent i = new Intent(getApplicationContext(),Notificaciones.class);
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);


    }

    public void didTapMetodo(View view){


        MiniTarjeta mini = modelc.getTarjetaActiva();

        if (mini != null && mini.estado.equals("PENDING") ){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Tarjeta pendiente");
            builder.setMessage("La tarjeta inscrita está en proceso de validación por parte de PayU. Espera una notificación o valida más tarde su estado.");

            // add the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int j) {

                    return;

                }
            });



            // create and show the alert dialog
            AlertDialog dialog = builder.create();
            dialog.show();
            return;

        }

        Intent i = new Intent(getApplicationContext(),ClienteCrearTarjeta.class);
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }

    public void didTapTips(View view){

        Intent i = new Intent(getApplicationContext(),TipsUso.class);
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }

    public void didTapAyuda(View view){


        if (modelc.tipoLogeo.equals("anonimo")){
            Intent i = new Intent(getApplicationContext(),ClienteRegistro.class);
            startActivity(i);
            getActivity().overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

        }else {
            Intent i = new Intent(getApplicationContext(),Ayuda.class);
            startActivity(i);
            getActivity().overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        }


    }


    public void didTapAviso(View view){

        Intent i = new Intent(getApplicationContext(),PoliticaUso.class);
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }



    public void didTapCerrarSesion(View view) {

        if (modelc.tipoLogeo.equals("anonimo")){
            showAlertDialogParaLogoutAnonimo();

            return;
        }

        hacerLogout();


    }




    public void hacerLogout(){
        mAuth.signOut();
        LoginManager.getInstance().logOut();

        modelc.pararListeners();

        modelc.cliente = new Cliente();
        modelc.mascotas.clear();
        modelc.direcciones.clear();
        modelc.compraActual.reset();
        modelc.tarjetas.clear();
        modelc.carritos.clear();
        modelc.favoritos.clear();
        modelc.tpaga = new TPaga();
        modelc.chats.clear();
        modelc.solicitudes.clear();
        modelc.preguntas.clear();
        modelc.misCompras.clear();
        modelc.misCalificaciones.clear();
        modelc.notificaciones.clear();


        Intent i = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        getActivity().finish();
    }

    public void showAlertDialogParaLogoutAnonimo() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Atención");
        builder.setMessage("Al salir perderás la información que ha sido guardada. ¿Deseas registrarte?");

        // add the buttons
        builder.setPositiveButton("Registrarme", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int j) {

                ///// H
                Intent i = new Intent(getApplicationContext(), ClienteRegistro.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

            }
        });

        builder.setNeutralButton("No, cerrar sesión", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                hacerLogout();
            }
        });
        builder.setNegativeButton("Cancelar", null);

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void pintarNotificaciones(){

        int i = modelc.notificacionesSinVer();
        if (i > 0) {
            layerNoti.setVisibility(View.VISIBLE);
            numeroNoti.setText( i + "");
        }
        else {
            layerNoti.setVisibility(View.GONE);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        pintarNotificaciones();

    }
}
