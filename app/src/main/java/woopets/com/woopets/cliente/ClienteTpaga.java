package woopets.com.woopets.cliente;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.TPaga;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.ClienteCrearTarjeta;
import woopets.com.woopets.cliente.Comandos.ComandoCliente;
import woopets.com.woopets.cliente.Comandos.ComandoTPaga;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.tpaga.Model.CustomerCliente;
import woopets.com.woopets.tpaga.Model.CustomerResponse;
import woopets.com.woopets.tpaga.io.MyApiAdapter;



public class ClienteTpaga extends Activity {

    EditText nombre, apellido, telefono, correo;
    TextView tarjeta;
    CheckBox checkBoxTarjeta;
    CheckBox checkBoxEfectivo;

    ImageView imgTarjeta;
    ImageView imgUbicacion;

    ModelCliente modelc = ModelCliente.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_tpaga);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        LinearLayout ckTarjeta =  (LinearLayout) findViewById(R.id.ckTarjeta);
        final LinearLayout ckEfectivo =  (LinearLayout) findViewById(R.id.ckEfectivo);

        nombre = (EditText) findViewById(R.id.nombre);
        apellido = (EditText) findViewById(R.id.apellido);
        telefono = (EditText) findViewById(R.id.telefono);
        correo = (EditText) findViewById(R.id.correo);
        tarjeta = (TextView) findViewById(R.id.btnTarjeta);

        imgTarjeta = (ImageView) findViewById(R.id.imgTarjeta);
        imgUbicacion = (ImageView) findViewById(R.id.imgUbicacion);

        checkBoxTarjeta = (CheckBox) findViewById(R.id.checkBoxTarjeta);
        checkBoxEfectivo = (CheckBox) findViewById(R.id.checkBoxEfectivo);

        ScrollView scroll = (ScrollView) findViewById(R.id.scroll);

        ckTarjeta.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxTarjeta.isChecked()){
                    apagar();
                }else {
                    activar();
                }

            }
        });

        ckEfectivo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                apagar();
            }
        });

        checkBoxTarjeta.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxTarjeta.isChecked()){
                    activar();
                }else {
                    apagar();
                }
            }
        });

        checkBoxEfectivo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                apagar();
            }
        });


        if (modelc.cliente.metodoPago.equals("Tarjeta")){
            activar();
        } else{
            apagar();
        }



        tarjeta.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!sinErrores()){
                    return;
                }

                if (!huboCambios()){
                    Intent i = new Intent(getApplicationContext(), ClienteCrearTarjeta.class);
                    startActivity(i);
                    return;
                }

                crearClienteEnTPaga();

            }
        });



        scroll.getViewTreeObserver().addOnScrollChangedListener(new OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {

                try {
                    InputMethodManager inputManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }catch (Exception x){

                }

            }
        });

        fillCampos();

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }



    @Override
    protected void onResume() {
        super.onResume();

        fillCampos();

    }


    public void fillCampos(){

        TPaga tpaga = modelc.tpaga;

        if (tpaga.nombres.equals("")) {
            nombre.setText(modelc.cliente.nombre);
        }else {
            nombre.setText(tpaga.nombres);
        }

        if (tpaga.apellidos.equals("")) {
            apellido.setText(modelc.cliente.apellido);
        }else {
            apellido.setText(tpaga.apellidos);
        }

        if (tpaga.telefono.equals("")) {
            telefono.setText(modelc.cliente.celular);
        }else {
            telefono.setText(tpaga.telefono);
        }

        if (tpaga.correo.equals("")) {
            correo.setText(modelc.cliente.correo);
        }else {
            correo.setText(tpaga.correo);
        }

        MiniTarjeta mini = modelc.getTarjetaActiva();

        if (modelc.direcciones.size() > 0) {
            imgUbicacion.setImageResource(R.drawable.imgubicacionok);
        }else{
            imgUbicacion.setImageResource(R.drawable.imgubicacionseleccionnok);
        }


        if (mini == null ) {
            imgTarjeta.setImageResource(R.drawable.imgtarjetanok);
        }
        else {
            imgTarjeta.setImageResource(R.drawable.imgtarjetaok);
            tarjeta.setText("Tarjeta de crédito terminada en " + mini.lastFour);
        }

    }

    public void crearClienteEnTPaga(){

        String firstName = nombre.getText().toString();
        String lastName = apellido.getText().toString();
        String email = correo.getText().toString();
        String phone = telefono.getText().toString();
        String merchantCustomerId ="1213234";


        Call<CustomerResponse> callCliente = MyApiAdapter.getApiService().addCustomerPost(CustomerCliente.create(firstName,lastName,email,phone,merchantCustomerId));
        // metodo que deja en cola la peticion
        callCliente.enqueue(new Callback<CustomerResponse>() {

            /**
             * Invoked for a received HTTP response.
             * <p>
             * Note: An HTTP response may still indicate an application-level failure such as a 404 or 500.
             * Call {@link Response#isSuccessful()} to determine if the response indicates success.
             *
             * @param call
             * @param response
             */
            @Override
            public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {

                if (response.isSuccessful()) {
                    response.body();
                    //Log.v("TagInfo","Body "+response.body()+"\n"+response.body().toString()+"\n"+response.body().getId());
                    //System.out.print(""+response.body().getId());

                    //Toast.makeText(getApplicationContext(),"ok " +response.body().getId(),Toast.LENGTH_SHORT).show();
                    modelc.tpaga.idClienteTpaga = response.body().getId();
                    callCrearDatosTPagaFirebase();
                    Intent i = new Intent(getApplicationContext(), ClienteCrearTarjeta.class);
                    startActivity(i);
                    return;
                }
                if (response.code() >= 500) {
                    Toast.makeText(getApplicationContext(),"Error conectando con el servidor",Toast.LENGTH_SHORT).show();
                    Log.v("TagInfo","Body "+"Error conectando con el servidor"+response.errorBody());
                }
                if (response.code() == 422) {
                    Toast.makeText(getApplicationContext(),"Valide su información o correo invalido",Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(getApplicationContext(),"Error con la información",Toast.LENGTH_SHORT).show();
                    Log.v("TagInfo","Body "+"Error"+response.errorBody().toString());
                }
            }



            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected
             * exception occurred creating the request or processing the response.
             *
             * @param call
             * @param t
             */
            @Override
            public void onFailure(Call<CustomerResponse> call, Throwable t) {
                Log.v("TagInfo","Body "+"Error"+t.getLocalizedMessage());
                Toast.makeText(getApplicationContext(),"Error conectando con el servidor",Toast.LENGTH_SHORT).show();

            }
        });
    }




    public boolean sinErrores() {
        if (nombre.getText().toString().length() < 3) {
            nombre.setError("Debes incluir tu nombre");
            nombre.requestFocus();
            return false;
        }


        if (apellido.getText().toString().length() < 3) {
            apellido.setError("Debes incluir tu apellido");
            apellido.requestFocus();
            return false;
        }


        String tel = telefono.getText().toString();

        if (!TextUtils.isDigitsOnly(telefono.getText())) {
            telefono.setError("El teléfono no puede contener letras");
            telefono.requestFocus();
            return false;
        }

        if (telefono.getText().toString().length() == 7 || telefono.getText().toString().length() == 10) {

        } else {
            telefono.setError("El télefono esta muy largo o muy corto");
            telefono.requestFocus();
            return false;

        }

        if (!Utility.isEmailValid(correo.getText().toString())){
            correo.setError("El correo no es valido");
            correo.requestFocus();
        }


        return true;
    }



    public void activar(){

       try {
           InputMethodManager inputManager = (InputMethodManager)
                   getSystemService(Context.INPUT_METHOD_SERVICE);

           inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                   InputMethodManager.HIDE_NOT_ALWAYS);
       }catch (NullPointerException n){

       }

        nombre.setEnabled(true);
        apellido.setEnabled(true);
        telefono.setEnabled(true);
        correo.setEnabled(true);
        tarjeta.setEnabled(true);
        checkBoxEfectivo.setChecked(false);
        checkBoxTarjeta.setChecked(true);

        nombre.setAlpha(1.0f);
        apellido.setAlpha(1.0f);
        telefono.setAlpha(1.0f);
        correo.setAlpha(1.0f);
        tarjeta.setAlpha(1.0f);

        modelc.cliente.metodoPago = "Tarjeta";
        new ComandoCliente(null).updateMetodoPago("Tarjeta");
        fillCampos();


    }


    public void apagar(){

        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }catch (NullPointerException n){

        }
        nombre.setEnabled(false);
        apellido.setEnabled(false);
        telefono.setEnabled(false);
        correo.setEnabled(false);
        tarjeta.setEnabled(false);
        checkBoxEfectivo.setChecked(true);
        checkBoxTarjeta.setChecked(false);
        nombre.setError(null);
        apellido.setError(null);
        telefono.setError(null);
        correo.setError(null);
        tarjeta.setError(null);


        nombre.setAlpha(0.6f);
        apellido.setAlpha(0.6f);
        telefono.setAlpha(0.6f);
        correo.setAlpha(0.6f);
        tarjeta.setAlpha(0.6f);

        modelc.cliente.metodoPago = "Sin definir";
        new ComandoCliente(null).updateMetodoPago("Sin definir");

        fillCampos();

    }


    public boolean huboCambios(){

        TPaga tpaga = modelc.tpaga;

        if (!tpaga.nombres.equals(nombre.getText().toString()) || !tpaga.apellidos.equals(apellido.getText().toString()) ||
                !tpaga.telefono.equals(telefono.getText().toString()) || !tpaga.correo.equals(correo.getText().toString())
                ){

            return true;

        }

        return false;

    }



    private void callCrearDatosTPagaFirebase(){

        modelc.tpaga.nombres = nombre.getText().toString();
        modelc.tpaga.apellidos = apellido.getText().toString();
        modelc.tpaga.telefono = telefono.getText().toString();
        modelc.tpaga.correo = correo.getText().toString();

        new ComandoTPaga(null).actualizarDatosTPaga(modelc.tpaga);

    }


    public  void  didTapFinalizar(View v){

        MiniTarjeta mini = modelc.getTarjetaActiva();

        if (mini == null) {
            modelc.cliente.metodoPago = "Sin definir";
            new ComandoCliente(null).updateMetodoPago("Sin definir");
        }




        if (modelc.compraActual.idPublicacion.equals("")) {
            Intent intent = new Intent(getApplicationContext(), HomeCliente.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }

        finish();

    }

}
