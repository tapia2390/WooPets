package woopets.com.woopets.cliente;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import woopets.com.woopets.BuildConfig;
import woopets.com.woopets.Comandos.ComandoCategorias;
import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.TapsHome;
import woopets.com.woopets.Oferente.VenderEnMypet;
import woopets.com.woopets.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

import java.util.regex.Pattern;

import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCliente;
import woopets.com.woopets.cliente.Comandos.ComandoCliente.OnComandoClienteChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoOferentes;
import woopets.com.woopets.cliente.Comandos.ComandoOferentes.OnComandoOferentesChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoRegistroCliente;
import woopets.com.woopets.cliente.Comandos.ComandoRegistroCliente.OnComandoRegistroClienteChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTips;

public class Cliente_login extends Activity implements OnComandoRegistroClienteChangeListener, OnComandoClienteChangeListener {

    EditText email,password;
    Button iniciarsession;
    final Context context = this;

    ProgressBar progressBar4;

    //bd
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressDialog progressDialog;
    private ProgressDialog mProgressDialog;
    ModelCliente modelc = ModelCliente.getInstance();
    private static final String TAG ="AndroidBash";
    private CallbackManager callbackManager;
    String correo1,password1;
    ComandoRegistroCliente comandoRegistrarte;
    ComandoCliente comandoCliente;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_cliente_login);

        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        iniciarsession = (Button) findViewById(R.id.iniciarsessionoferente);
        progressBar4 =(ProgressBar)findViewById(R.id.login_progress);
        progressDialog = new ProgressDialog(this);

        comandoRegistrarte = new ComandoRegistroCliente(this);
        comandoCliente = new ComandoCliente(this);


        if(estaConectado()){
            //Toast.makeText(getApplication(),"Conectado a internet", Toast.LENGTH_SHORT).show();

                mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    final FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {

                        modelc.cliente.id = user.getUid();

                        new ComandoCategorias(new ComandoCategorias.OnComandoCategoriasChangeListener() {
                            @Override
                            public void cargoCategorias() {


                            }

                            @Override
                            public void cargoTipoUsuario(String tipo) {
                                if (tipo.equals("OFERENTE")){
                                    Toast.makeText(getApplicationContext(), "Esta cuenta ya existe como Vendedor",
                                            Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                    iniciarsession.setEnabled(true);
                                    mAuth.signOut();
                                    return;
                                }
                                else if (tipo.equals("CLIENTE")){
                                    ingresarComoCliente(user);
                                }
                                else {    //Si no existe
                                    if (modelc.tipoLogeo.equals("faceb")){
                                        createNewUserFB(user);
                                        return;

                                    }else {
                                        Intent i = new Intent(getApplicationContext(), ClienteRegistroBasicos.class);
                                        startActivity(i);
                                        finish();
                                    }

                                }

                            }

                            @Override
                            public void actualizoSistema() {

                            }
                        }).getTipoUsuario(user.getUid());
                    } else {
                        // Users is signed out
                        Log.d(TAG, "onAuthStateChanged:signed_out"+user+"no logueado");
                        // Toast.makeText(getApplication(),"...."+user+"no logueado", Toast.LENGTH_SHORT).show();
                    }
                }
            };
        }else{
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            showAlertSinInternet();
            return;
        }


        //FaceBook
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.button_facebook_login);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                modelc.tipoLogeo="faceb";
                Log.d(TAG, "facebook:onSuccess:" + loginResult);


                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                Log.v("facebook", "facebook"+error.getMessage());
                Toast.makeText(getApplicationContext(), "Revise su acceso a internet.",
                        Toast.LENGTH_LONG).show();

            }
        });

        email.clearFocus();
        password.clearFocus();



    }



    public void ingresarComoCliente(final FirebaseUser user ){

        modelc.cliente.id = user.getUid();


        if (user.getProviders().size() > 0) {

            String provider = user.getProviders().get(0);
            if (provider.equals("password")) {
                modelc.tipoLogeo = "normal";
            }else{
                modelc.tipoLogeo = "faceb";
            }

            if(modelc.tipoLogeo.equals("faceb")){
                comandoRegistrarte.enviarRegistro3(user.getDisplayName(), user.getUid(), user.getEmail());

            }

            if(modelc.tipoLogeo.equals("normal")){
                comandoRegistrarte.enviarRegistro2(user.getUid());
            }


        }

        if (user.isAnonymous()){
            comandoRegistrarte.enviarRegistro2(user.getUid());
        }




        new ComandoCliente(new OnComandoClienteChangeListener() {
            @Override
            public void cargoCliente() {
                if ( modelc.cliente.tieneDatosBasicos() ) {

                    new ComandoOferentes(new OnComandoOferentesChangeListener() {
                        @Override
                        public void cargoMiniOferentes() {
                            preCargar();
                        }
                    }).getMiniOferentes();

                } else {
                    Intent i = new Intent(getApplicationContext(),ClienteRegistroExitoso.class);
                    startActivity(i);
                    finish();
                }

            }

            @Override
            public void clienteNoExiste() {

            }
        }).getCliente(user.getUid());


    }

    private void handleFacebookAccessToken(final AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        showProgressDialog();


        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            progressDialog.dismiss();
                            hideProgressDialog();
                            LoginManager.getInstance().logOut();
                            String error= "Problema con la configuración. ";

                            if (task.getException() instanceof FirebaseException){
                                error = ((FirebaseException) task.getException()).toString();
                                Log.e("ERROR CON FACE-FIRE", error);
                                //progressDialog.dismiss();
                                //return;
                            }

                            try {
                                error = ((FirebaseAuthException) task.getException()).getErrorCode();
                            }
                            catch (ClassCastException c){
                                c.printStackTrace();
                            }
                            if (error.equals("ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL")){
                                Toast.makeText(getApplicationContext(), "Una cuenta con ese correo ya existe.",
                                        Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(getApplicationContext(), error,
                                        Toast.LENGTH_SHORT).show();
                            }

                        }else{
                            String uid=task.getResult().getUser().getUid();
                            String name=task.getResult().getUser().getDisplayName();
                            String email=task.getResult().getUser().getEmail();

                            Log.v("datos usuario","datos usuario"+uid+name+email);

                            FirebaseUser user = task.getResult().getUser();

                            //createNewUserFB(user);

                        }


                    }
                });
    }

    private void createNewUserFB(FirebaseUser user) {

        String versionName = BuildConfig.VERSION_NAME;
        String userId = user.getUid();

        comandoRegistrarte.registroInicialClienteFB(user.getEmail(), user.getDisplayName(), userId, versionName );
        //Toast.makeText(getApplicationContext(), "Se envio el registro ", Toast.LENGTH_LONG).show();
        email.setText("");
        password.setText("");

        Intent i  = new Intent(getApplicationContext(), ClienteRegistroExitoso.class);
        startActivity(i);
        finish();

    }




    private void preCargar() {

        String tipo = "";
        Mascota mascota = modelc.getMascotaActiva();
        if (mascota != null ) {
            tipo = mascota.tipo;
        }

        Direccion direccion = modelc.getDireccionActiva();

        new ComandoCalificacion(null).getMisCalificaciones(modelc.cliente.id);
        new ComandoTips().getTips();
        new ComandoTips().getPolitica();
        new ComandoAyuda(null).getInicales();
        CmdNoti.getInstance().getNotificaciones();



        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {
                //Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                //startActivity(i);
                //finish();
            }

            @Override
            public void cargoPublicacion() {
                ///Pilas aca no puede ir nada// o jamas se va a llamar
            }
        }).getLast6Destacados(tipo);

        if (direccion != null && direccion.ubicacion != null) {

            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {

                }

                @Override
                public void cargoPublicacion() {
                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                    startActivity(i);
                    finish();

                }
            }).getOferentesCercanos(direccion.ubicacion);

        } else {
            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {
                    ///Pilas aca no puede ir nada// o jamas se va a llamar
                }

                @Override
                public void cargoPublicacion() {
                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                    startActivity(i);
                    finish();
                }
            }).getTodasPublicaciones(tipo);


            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {
                    ///Pilas aca no puede ir nada// o jamas se va a llamar
                }

                @Override
                public void cargoPublicacion() {
                    Intent i = new Intent(getApplicationContext(), HomeCliente.class);
                    startActivity(i);
                    finish();
                }
            }).getTodasPublicaciones(tipo);
        }


    }





    public void ingresarComoOferente(FirebaseUser user ){



    }


    //atras hadware
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            /*Intent i = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);*/
            finish();
        }
        return true;
    }



    public void loguinCliente(View v){

        if(estaConectado()){
            if(email.getText().toString().equals("")) {
                email.setError("Correo obligatorio");
                showAlertMsm(getString(R.string.ingresoFallido),getString(R.string.ingresoFallidoDescrip));
            }
            else if (!validarEmail(email.getText().toString())){
                email.setError("Email no válido");
            }
            else if(password.getText().toString().equals("")) {
                password.setError("Contraseña obligatoria");
                showAlertMsm(getString(R.string.ingresoFallido),getString(R.string.ingresoFallidoDescrip));
            }
            else if(password.getText().length()<3) {
                password.setError("Contraseña incorrecta");
            }
            else{

                logueo();
            }
        }else{
            showAlertSinInternet();
        }
    }


    public void olvidopaswword(View v){
        if(email.getText().toString().equals("")) {
            email.setError("Correo obligatorio");
            showAlertMsm(getString(R.string.recuperarpasswortitle),getString(R.string.recuperarpassworddescripcion));
        }
        else if (!validarEmail(email.getText().toString())){
            email.setError("Email no válido");
        }else {

            Log.v("ok", "ok");
            progressBar4.setVisibility(View.VISIBLE);
            String emailId = email.getText().toString();
            mAuth.sendPasswordResetEmail(emailId)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(woopets.com.woopets.cliente.Cliente_login.this, "Las instrucciones han sido enviados a su correo electrónico", Toast.LENGTH_SHORT).show();
                                progressBar4.setVisibility(View.GONE);
                            } else {
                                Toast.makeText(woopets.com.woopets.cliente.Cliente_login.this, "¡Lo siento! Inténtalo de nuevo.", Toast.LENGTH_SHORT).show();
                                progressBar4.setVisibility(View.GONE);
                            }

                        }
                    });
        }

    }

    //validacion conexion internet
    protected Boolean estaConectado(){
        if(conectadoWifi()){
            Log.v("wifi","Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        }else{
            if(conectadoRedMovil()){
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            }else{
                showAlertSinInternet();
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }



    protected Boolean conectadoWifi(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }




    public void showAlertSinInternet(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle(""+getString(R.string.sinInternet));

        // set dialog message
        alertDialogBuilder
                .setMessage(""+getString(R.string.sinInternetDescripcion))
                .setCancelable(false)
                .setPositiveButton("Reintentar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        Intent inte  = new Intent(getBaseContext(), woopets.com.woopets.Oferente.LoguinOferente.class);
                        startActivity(inte);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void showAlertMsm(String titulo, String descripcion){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle(""+titulo);

        // set dialog message
        alertDialogBuilder
                .setMessage(""+descripcion)
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    //validar email
    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public void ventaMypet(View v){
        Intent i = new Intent(getApplicationContext(),ClienteRegistro.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
        finish();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,
                resultCode, data);
    }



    @Override
    public void onStart() {
        super.onStart();
        if(mAuth==null || mAuthListener ==null){
            return;
        }else{
            mAuth.addAuthStateListener(mAuthListener);
        }


    }

    @Override
    public void onStop() {
        super.onStop();


        if(mAuth==null || mAuthListener ==null){
            return;
        }else{
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }



    //logueo
    public void logueo(){
        correo1 = email.getText().toString().toString();
        password1= password.getText().toString().toString();
        // Toast.makeText(this, "Se guarda el registro", Toast.LENGTH_LONG).show();
        progressDialog.setMessage("Validando la información, por favor espere...");
        progressDialog.show();
        iniciarsession.setEnabled(false);
        mAuth.signInWithEmailAndPassword(email.getText().toString(),password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:"+task.isSuccessful());

                        FirebaseAuthException ex = (FirebaseAuthException) task.getException();
                        if (ex==null){
                            return;
                        }

                        String error = ex.getErrorCode();

                        if (error.equals("ERROR_INVALID_EMAIL")){
                            Log.d(TAG, "signInWithEmail:onComplete:"+"ERROR CON EL CORREO");
                            Toast.makeText(getApplication(),""+"Correo no valido", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                        if (error.equals("ERROR_USER_NOT_FOUND")){
                            Log.d(TAG, "signInWithEmail:onComplete:" + "USUARIO NUEVO");
                            Toast.makeText(getApplication(),""+"USUARIO NO REGISTRADO", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                        if (error.equals("ERROR_WRONG_PASSWORD")){
                            Log.d(TAG, "signInWithEmail:onComplete:" + "CONTRASEÑA INCORRECTA");
                            Toast.makeText(getApplication(),""+"CONTRASEÑA INCORRECTA", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                        if (!task.isSuccessful()){
                            Log.d(TAG, "signInWithEmail:onComplete:" + task.getException());
                            Toast.makeText(getApplication(),""+"FALLO EN LA AUTENTICACION", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }

                        iniciarsession.setEnabled(true);

                    }
                });
    }

    @Override
    public void cargoCliente() {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        Intent i = new Intent(getApplicationContext(),HomeCliente.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
        finish();


    }

    @Override
    public void clienteNoExiste() {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        Intent i = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
        finish();

    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void informacionRegistroCliente() {

    }

    @Override
    public void errorInformacionRegistroCliente() {

    }
}

