package woopets.com.woopets.cliente.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.carrito.CarritoAdapter;
import woopets.com.woopets.cliente.carrito.FavoritosAdapter;

/**
 * Created by andres on 10/19/17.
 */

public class FavoritosFragmento extends Fragment implements OnClickListener, OnImageFireChange {


    TextView carrito, misCompras, favoritos;

    private ListView listaView;
    FavoritosAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View vi = inflater.inflate(R.layout.fragment_favoritos_fragmento, container, false);

        listaView = (ListView) vi.findViewById(R.id.lista);
        adapter = new FavoritosAdapter(getContext(),this);
        listaView.setEmptyView(vi.findViewById(R.id.vacio));
        listaView.setAdapter(adapter);

        carrito = (TextView) vi.findViewById(R.id.carrito);
        misCompras = (TextView) vi.findViewById(R.id.misCompras);
        favoritos = (TextView) vi.findViewById(R.id.favoritos);

        misCompras.setOnClickListener(this);
        carrito.setOnClickListener(this);
        favoritos.setOnClickListener(this);

        return vi;
    }


    @Override
    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.misCompras:
                irCompras();
                break;
            case R.id.carrito:
                irCarrito();
                break;
            case R.id.favoritos:
                irFavoritos();
                break;

        }

    }



    public void irCompras() {
        replaceFragment(new MisComprasFragmento());
    }

    public void irCarrito() {
        replaceFragment(CarritoFragmento.newInstance("CARRITO"));
    }

    public void irFavoritos() {
        replaceFragment(new FavoritosFragmento());
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);

        transaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }


    @Override
    public void cargoImagen(String tipo) {
        adapter.notifyDataSetChanged();

    }
}

