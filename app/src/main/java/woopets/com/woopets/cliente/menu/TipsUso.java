package woopets.com.woopets.cliente.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.home.PublicacionPageAdapter;

public class TipsUso extends Activity implements OnImageFireChange {


    private TipsUsoAdapter pageAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips_uso);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

        pageAdapter = new TipsUsoAdapter(getBaseContext(),this);
        pager.setAdapter(pageAdapter);
        TabLayout tabpuntos = (TabLayout) findViewById(R.id.tab_puntos);
        tabpuntos.setupWithViewPager(pager, true);


    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

    @Override
    public void cargoImagen(String tipo) {
        if (tipo.equals("TIP")) {
            pageAdapter.notifyDataSetChanged();
        }
    }
}
