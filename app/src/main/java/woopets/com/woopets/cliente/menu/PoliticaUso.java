package woopets.com.woopets.cliente.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.method.ScrollingMovementMethod;
import android.webkit.WebView;
import android.widget.TextView;

import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.ModelCliente;

public class PoliticaUso extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_politica_uso);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


        String htmlText = "<html><body style=\"text-align:justify\"> %s </body></Html>";
        WebView webView = (WebView) findViewById(R.id.web);
        ModelCliente modelc = ModelCliente.getInstance();
        webView.loadDataWithBaseURL(null, modelc.politica, "text/html", "utf-8", null);



    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

}
