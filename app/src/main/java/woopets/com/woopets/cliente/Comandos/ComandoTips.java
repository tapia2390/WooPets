package woopets.com.woopets.cliente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.regex.Pattern;

import woopets.com.woopets.Modelo.ImageFire;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 11/15/17.
 */

public class ComandoTips {

    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    public ComandoTips(){


    }

    public void getTips() {

        DatabaseReference ref = database.getReference("tipsDeUso");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                for (DataSnapshot tip : snap.getChildren()){

                    ImageFire img = new ImageFire();
                    String[] parts = tip.getValue().toString().split(Pattern.quote("."));
                    img.nombre = parts[0];
                    img.extension = ".jpg";
                    img.folderStorage = "tipsDeUsoAndroid";
                    modelc.tips.add(img);

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void getPolitica(){

        DatabaseReference ref = database.getReference("TerminosYCondiciones");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                for (DataSnapshot tip : snap.getChildren()){

                   String titulo = (String) tip.child("titulo").getValue();
                    String texto = (String) tip.child("texto").getValue();
                    modelc.politica = texto;

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }








}
