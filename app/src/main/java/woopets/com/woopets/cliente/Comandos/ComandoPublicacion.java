package woopets.com.woopets.cliente.Comandos;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.Transaction.Handler;
import com.google.firebase.database.Transaction.Result;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.Distancia;
import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.Oferente;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Modelo.cliente.Ubicacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/22/17.
 */

public class ComandoPublicacion {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    public interface OnComandoPublicacionChangeListener {

        void cargoDestacado();
        void cargoPublicacion();

    }


    private ComandoPublicacion.OnComandoPublicacionChangeListener mListener;

    public ComandoPublicacion(ComandoPublicacion.OnComandoPublicacionChangeListener mListener){

        this.mListener = mListener;
    }



    public void getPublicacion(String idPublicacion){

        if (idPublicacion.equals("")){
            return;
        }

        DatabaseReference ref = database.getReference("productos/"+idPublicacion);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                Publicacion publi = readPublicacion(snap);
                modelc.addPublicacion(publi);
                getOferenteParaPublicacion(publi, -1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private Publicacion readPublicacion(DataSnapshot snap) {

        Publicacion publi = new Publicacion(snap.getKey());

        publi.activo = (boolean) snap.child("activo").getValue();
        publi.categoria = (String) snap.child("categoria").getValue();
        publi.descripcion = (String) snap.child("descripcion").getValue();
        publi.destacado = (boolean) snap.child("destacado").getValue();
        publi.fechaCreacion = (String) snap.child("fechaCreacion").getValue();
        publi.idOferente = (String) snap.child("idOferente").getValue();
        publi.nombre = (String) snap.child("nombre").getValue();
        publi.precio = (String) snap.child("precio").getValue();
        publi.servicio = (boolean) snap.child("servicio").getValue();
        publi.subCategoria = (String) snap.child("subCategoria").getValue();
        publi.target = (String) snap.child("target").getValue();

        if (publi.servicio) {
            publi.duracion = (long) snap.child("duracion").getValue();
            publi.duracionMedida = (String) snap.child("duracionMedida").getValue();
            publi.servicioEnDomicilio = (boolean) snap.child("servicioEnDomicilio").getValue();
        }
        else {

            publi.stock  = (long) snap.child("stock").getValue();
        }

        if (snap.hasChild("timestamp")) {
            publi.timestamp = (long) snap.child("timestamp").getValue();
        }

        int numFotos = 1;
        for (DataSnapshot ignored :snap.child("fotos").getChildren()) {
            publi.addImageFireParaFoto(numFotos);
            numFotos++;
        }

        for (DataSnapshot horaSnap :snap.child("horario").getChildren()) {
            Horario horario = new Horario();
            horario.dias = (String)horaSnap.child("dias").getValue();
            horario.horaCierre = (String)horaSnap.child("horaCierre").getValue();
            horario.horaInicio = (String)horaSnap.child("horaInicio").getValue();
            horario.nombreArbol = horaSnap.getKey();
            horario.sinJornadaContinua = (boolean)horaSnap.child("sinJornadaContinua").getValue();
            publi.horarios.add(horario);
        }

        return publi;


    }



    public void getLast6Destacados(String target){

        DatabaseReference ref = database.getReference("productos");

        if (target.equals("")) {
            Query query = ref.orderByChild("destacado").limitToLast(6).equalTo(true);
            ChildEventListener listener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot snap, String s) {
                    Publicacion publi = readPublicacion(snap);
                    if (publi.activo) {
                        modelc.addDestacado(publi);
                        getOferenteParaPublicacion(publi, -1);
                        mListener.cargoDestacado();
                    }

                }

                @Override
                public void onChildChanged(DataSnapshot snap, String s) {
                    Publicacion publi = readPublicacion(snap);
                    if (publi.activo) {
                        modelc.addDestacado(publi);
                        getOferenteParaPublicacion(publi, -1);
                        mListener.cargoDestacado();
                    }

                }

                @Override
                public void onChildRemoved(DataSnapshot snap) {
                    Publicacion publi = readPublicacion(snap);
                    publi.borrada = true;
                    modelc.addDestacado(publi);
                    getOferenteParaPublicacion(publi, -1);
                    mListener.cargoDestacado();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };

            query.addChildEventListener(listener);
            modelc.hijosListener.put(query,listener);

        } else {

            Query query = ref.orderByChild("destacado").equalTo(true);
            ChildEventListener listener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot snap, String s) {

                    Publicacion publi = readPublicacion(snap);
                    if (publi.activo) {
                        modelc.addDestacado(publi);
                        getOferenteParaPublicacion(publi, -1);
                        mListener.cargoDestacado();
                    }

                }

                @Override
                public void onChildChanged(DataSnapshot snap, String s) {
                    Publicacion publi = readPublicacion(snap);
                    if (publi.activo) {
                        modelc.addDestacado(publi);
                        getOferenteParaPublicacion(publi, -1);
                        mListener.cargoDestacado();
                    }

                }

                @Override
                public void onChildRemoved(DataSnapshot snap) {
                    //modelc.deleteDestacado(readPublicacion(snap));
                    //mListener.cargoDestacado();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            query.addChildEventListener(listener);
            modelc.hijosListener.put(query,listener);

        }

    }



    public void getTodasPublicaciones(String target){

        DatabaseReference ref = database.getReference("productos");

        if (target.equals("")) {
            Query query = ref.orderByKey().limitToLast(100);
            ChildEventListener listener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot snap, String s) {
                    Publicacion publi = readPublicacion(snap);
                    if (publi.activo) {
                        //modelc.addDestacado(publi);
                        modelc.addPublicacion(publi);
                        getOferenteParaPublicacion(publi, -1);
                        mListener.cargoPublicacion();;
                    }

                }

                @Override
                public void onChildChanged(DataSnapshot snap, String s) {
                    Publicacion publi = readPublicacion(snap);
                    if (publi.activo) {
                        //modelc.addDestacado(publi);
                        modelc.addPublicacion(publi);
                        getOferenteParaPublicacion(publi, -1);
                        mListener.cargoPublicacion();;
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot snap) {
                    Publicacion publi = readPublicacion(snap);
                    publi.borrada = true;
                    modelc.addPublicacion(publi);
                    getOferenteParaPublicacion(publi, -1);
                    mListener.cargoPublicacion();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            query.addChildEventListener(listener);
            modelc.hijosListener.put(query,listener);


        } else {

            Query query = ref.orderByKey().limitToLast(400);
            ChildEventListener listener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot snap, String s) {

                    Publicacion publi = readPublicacion(snap);
                    if (publi.activo) {
                        modelc.addDestacado(publi);
                        getOferenteParaPublicacion(publi, -1);
                        mListener.cargoPublicacion();
                    }

                }

                @Override
                public void onChildChanged(DataSnapshot snap, String s) {
                    Publicacion publi = readPublicacion(snap);
                    if (publi.activo) {
                        modelc.addDestacado(publi);
                        getOferenteParaPublicacion(publi, -1);
                        mListener.cargoPublicacion();
                    }

                }

                @Override
                public void onChildRemoved(DataSnapshot snap) {
                    Publicacion publi = readPublicacion(snap);
                    publi.borrada = true;
                    modelc.addPublicacion(publi);
                    getOferenteParaPublicacion(publi, -1);
                    mListener.cargoPublicacion();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            query.addChildEventListener(listener);
            modelc.hijosListener.put(query,listener);
        }

    }



    public Oferente readOferente(DataSnapshot snap, float distancia) {

        Oferente ofer = new Oferente();

        ofer.id = snap.getKey();
        ofer.direccion = (String) snap.child("direccion").getValue();
        ofer.razonSocial = (String) snap.child("razonSocial").getValue();
        ofer.celular = (String) snap.child("celular").getValue();
        ofer.telefono = (String) snap.child("telefono").getValue();
        ofer.correo = (String) snap.child("correo").getValue();
        ofer.distancia = distancia;


        ofer.web = (String) snap.child("web").getValue();
        if (ofer.web == null ) {
            ofer.web = "Sin registro";
        }


        for (DataSnapshot horaSnap :snap.child("horario").getChildren()) {
            Horario horario = new Horario();
            horario.dias = (String)horaSnap.child("dias").getValue();
            horario.horaCierre = (String)horaSnap.child("horaCierre").getValue();
            horario.horaInicio = (String)horaSnap.child("horaInicio").getValue();
            horario.nombreArbol = horaSnap.getKey();
            horario.sinJornadaContinua = (boolean)horaSnap.child("sinJornadaContinua").getValue();
            ofer.horarios.add(horario);
        }

        return  ofer;

    }




    public void getOferenteParaPublicacion(final Publicacion publi, final float distancia) {

        DatabaseReference ref = database.getReference("oferentes"+ "/" + publi.idOferente);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                publi.oferente = readOferente(snap, distancia);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }




    public void getPublicacionesOferente(final Distancia ofe) {

        DatabaseReference ref = database.getReference("productos");

            Query query = ref.orderByChild("idOferente").equalTo(ofe.idOferente);

            ChildEventListener listener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot snap, String s) {
                    Publicacion publi = readPublicacion(snap);

                    if (publi.activo) {
                        publi.distancia = ofe.metros;
                        modelc.addPublicacion(publi);
                        getOferenteParaPublicacion(publi, ofe.metros);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot snap, String s) {
                    Publicacion publi = readPublicacion(snap);
                    if (publi.activo) {
                        publi.distancia = ofe.metros;
                        modelc.addPublicacion(publi);
                        getOferenteParaPublicacion(publi, ofe.metros);
                    }


                }

                @Override
                public void onChildRemoved(DataSnapshot snap) {
                    Publicacion publi = readPublicacion(snap);
                    publi.borrada = true;
                    publi.distancia = ofe.metros;
                    modelc.addPublicacion(publi);
                    getOferenteParaPublicacion(publi, ofe.metros);


                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            query.addChildEventListener(listener);
            modelc.hijosListener.put(query, listener);


    }



    public void getOferentesCercanos(final Ubicacion loc){

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("ubicacionOferentes");
        GeoFire geoFire = new GeoFire(ref);
        GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(loc.latitud, loc.longitud), 500);

        modelc.publicaciones.clear();

        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {

            final ArrayList<Distancia> oferentes = new ArrayList<>();


            @Override
            public void onKeyEntered(String key, GeoLocation location) {


                float metros = getDistancia(loc,location);

                Distancia newOferente = new Distancia(key,metros);
                if (oferentes.contains(newOferente)){
                    return;
                }
                oferentes.add(newOferente);
            }

            @Override
            public void onKeyExited(String key) {


            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {

                for  (Distancia idOferente :oferentes) {

                    getPublicacionesOferente(idOferente);
                }

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        while (modelc.publicaciones.size() < 2){
                            Log.i("SIZE", modelc.publicaciones.size()+"");


                        }
                        mListener.cargoPublicacion();
                    }
                });


            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });

    }





    public void getOferentesCercanos(final Ubicacion loc, final String idPublicacionAviso){

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("ubicacionOferentes");
        GeoFire geoFire = new GeoFire(ref);
        GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(loc.latitud, loc.longitud), 500);

        modelc.publicaciones.clear();

        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {

            final ArrayList<Distancia> oferentes = new ArrayList<>();

            @Override
            public void onKeyEntered(String key, GeoLocation location) {


                float metros = getDistancia(loc,location);

                Distancia newOferente = new Distancia(key,metros);
                if (oferentes.contains(newOferente)){
                    return;
                }
                oferentes.add(newOferente);
            }

            @Override
            public void onKeyExited(String key) {


            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {

                for  (Distancia idOferente :oferentes) {

                    getPublicacionesOferente(idOferente);
                }

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        while (modelc.publicaciones.size() < 10){
                            Log.i("SIZE", modelc.publicaciones.size()+"");


                        }
                        Publicacion publi = modelc.getPublicacionById(idPublicacionAviso);
                        while (publi == null){
                            publi = modelc.getPublicacionById(idPublicacionAviso);
                        }
                        mListener.cargoPublicacion();
                    }
                });


            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });

    }

    public float getDistancia(Ubicacion ubi, GeoLocation geo){

        Location l1 = new Location("");
        l1.setLatitude(ubi.latitud);
        l1.setLongitude(ubi.longitud);

        Location l2 = new Location("");
        l2.setLatitude(geo.latitude);
        l2.setLongitude(geo.longitude);

        return l1.distanceTo(l2);

    }
   


    public static ComandoPublicacion.OnComandoPublicacionChangeListener dummy = new ComandoPublicacion.OnComandoPublicacionChangeListener() {

        @Override
        public void cargoDestacado()
        {}

        @Override
        public void cargoPublicacion() {

        }


    };




    public static void sumarClic(String idPublicacion){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("productos/"+idPublicacion+"/clics");


        ref.runTransaction(new Handler() {
            @Override
            public Result doTransaction(MutableData mutableData) {
                Object o = mutableData.getValue();

                if (o == null){
                    return Transaction.success(mutableData);
                }
                int numero = Integer.valueOf(o.toString());


                mutableData.setValue(numero + 1);

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean commited, DataSnapshot snap) {
                if (commited){
                    //mListener.adicionoInventario();
                   //Vamos a asumir que es exitoo
                }
            }
        });



    }


}

    

