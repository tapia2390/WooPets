package woopets.com.woopets.cliente.Comandos;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.Horario;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/25/17.
 */

public class ComandoCalificacion {


    Modelo modelo = Modelo.getInstance();
    ModelCliente modelc = ModelCliente.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    public interface OnComandoCalificacionChangeListener {

        void cargoCalificaciones();

    }


    private ComandoCalificacion.OnComandoCalificacionChangeListener mListener;

    public ComandoCalificacion(ComandoCalificacion.OnComandoCalificacionChangeListener mListener){

        this.mListener = mListener;
    }


    private Calificacion readCalificacion(DataSnapshot snap) {

        Calificacion cali = snap.getValue(Calificacion.class);
        cali.idCalificacion = snap.getKey();
        return cali;

    }


    public void getCalificaciones(final String idPublicacion){

        DatabaseReference ref = database.getReference("calificaciones");

        Query query = ref.orderByChild("idPublicacion").equalTo(idPublicacion);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                Publicacion publi = modelc.getPublicacionById(idPublicacion);

                if (publi == null) {
                    return;
                }
                publi.calificaciones = new ArrayList<Calificacion>();


                for (DataSnapshot snapCali : snap.getChildren()) {
                    Calificacion cali = readCalificacion(snapCali);
                    publi.calificaciones.add(cali);
                }
                if (mListener == null){
                    return;
                }

                mListener.cargoCalificaciones();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void getMisCalificaciones(final String idCliente){

        DatabaseReference ref = database.getReference("calificaciones");

        Query query = ref.orderByChild("idCliente").equalTo(idCliente);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                modelc.misCalificaciones.clear();

                for (DataSnapshot snapCali : snap.getChildren()) {
                    Calificacion cali = readCalificacion(snapCali);
                    modelc.misCalificaciones.add(cali);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void calificarMiCompra(Calificacion cali) {

        DatabaseReference ref = database.getReference("calificaciones/");
        ref = ref.child(ref.push().getKey());

        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("idCliente", cali.idCliente);
        mapa.put("idOferente",  cali.idOferente);
        mapa.put("idPublicacion", cali.idPublicacion);
        mapa.put("idCompra", cali.idCompra);
        mapa.put("calificacion",cali.calificacion);
        mapa.put("comentario",cali.comentario);
        mapa.put("fecha", Utility.getFechaHora());
        mapa.put("timestamp", ServerValue.TIMESTAMP);

        ref.setValue(mapa);

    }








    public static ComandoCalificacion.OnComandoCalificacionChangeListener dummy = new ComandoCalificacion.OnComandoCalificacionChangeListener() {


        @Override
        public void cargoCalificaciones() {

        }
    };



}




