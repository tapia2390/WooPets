package woopets.com.woopets.cliente.home;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/20/17.
 */

public class GrillaProductosAdapter  extends BaseAdapter{


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private OnImageFireChange listener;
    private ArrayList<Publicacion> publicaciones;
    private final LayoutInflater mInflater;


    public GrillaProductosAdapter(Context c, OnImageFireChange listener) {
        mContext = c;
        this.listener = listener;
        mInflater = LayoutInflater.from(c);

        publicaciones = new ArrayList<>(modelc.publicaciones.subList(0,Math.min(80,modelc.publicaciones.size())));
    }


    @Override
    public int getCount() {
        return publicaciones.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }



    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View v = convertView;
        Publicacion publi = publicaciones.get(i);
        publi.fotosBaja.get(0).setListener(listener);


        if (v == null) {
            v = mInflater.inflate(R.layout.item_imagen_publicacion, viewGroup, false);
            v.setTag(R.id.foto, v.findViewById(R.id.foto));
            v.setTag(R.id.text, v.findViewById(R.id.text));
            v.setTag(R.id.valor, v.findViewById(R.id.valor));

        }

        ImageView picture = (ImageView) v.getTag(R.id.foto);
        TextView name =  (TextView) v.getTag(R.id.text);
        TextView valor =  (TextView) v.getTag(R.id.valor);


        name.setText(publi.nombre);
        valor.setText(Utility.convertToMoney(publi.precio));


        if (publi.fotosBaja.size() == 0){
            return v;
        }

        Bitmap fotoBitmap = publi.fotosBaja.get(0).getFoto("PUBLICACION");


        if (fotoBitmap != null) {
            picture.setImageBitmap(fotoBitmap);
        }else {
            picture.setImageResource(R.drawable.btnexotico);
        }

        return v;

    }



}
