package woopets.com.woopets.cliente.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.geofire.GeoLocation;

import java.util.ArrayList;
import java.util.Collections;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Modelo.cliente.Ubicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;

import static android.support.constraint.R.id.parent;

/**
 * Created by andres on 10/26/17.
 */

public class FiltroCategoriaAdapter extends BaseAdapter {

    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private OnImageFireChange listener;
    private ArrayList<Publicacion> publicaciones;
    private final LayoutInflater mInflater;

    //Solamente cuando es anonimo deberia filtrar por tipo de resto muestra el resto de categorias
    public FiltroCategoriaAdapter(Context c, String categoria, OnImageFireChange listener, boolean reverzarFiltroMascota) {
        mContext = c;
        this.listener = listener;
        mInflater = LayoutInflater.from(c);


        Mascota mascota = modelc.getMascotaActiva();

        if (mascota == null) {

            if (reverzarFiltroMascota) {
                publicaciones = modelc.getPublicacionesXCategoria("");
            }
            else {
                publicaciones = modelc.getPublicacionesXCategoria(categoria);
            }


        } else {
            if (reverzarFiltroMascota) {
                publicaciones = modelc.getPublicacionesParaFiltroMasEnEstaCategoria(categoria, mascota);
            }
            else {
                publicaciones = modelc.getPublicacionesXCategoriaPeroConMascota(categoria, mascota);
            }
        }
        Collections.sort(publicaciones);

    }


    @Override
    public int getCount() {

        return publicaciones.size() / 2 ;

    }

    @Override
    public Object getItem(int i) {
        return publicaciones.get(i * 2);
    }

    @Override
    public long getItemId(int i) {
        return i * 2;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_imagen_publicacion_doble,parent, false);
            holder = new ViewHolder();
            holder.layer1 = (LinearLayout) convertView.findViewById(R.id.layer1);
            holder.layer2 = (LinearLayout) convertView.findViewById(R.id.layer2);
            holder.layerDis1 = (LinearLayout) convertView.findViewById(R.id.layoutDistancia);
            holder.layerDis2 = (LinearLayout) convertView.findViewById(R.id.layoutDistancia2);

            holder.name1 = (TextView) convertView.findViewById(R.id.text1);
            holder.name2 = (TextView) convertView.findViewById(R.id.text2);
            holder.valor1 = (TextView) convertView.findViewById(R.id.valor1);
            holder.valor2= (TextView) convertView.findViewById(R.id.valor2);
            holder.picture1 = (ImageView) convertView.findViewById(R.id.foto1);
            holder.picture2 = (ImageView) convertView.findViewById(R.id.foto2);
            holder.dis1 = (TextView) convertView.findViewById(R.id.distancia1);
            holder.dis2 = (TextView) convertView.findViewById(R.id.distancia2);


            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        final Publicacion publi1 = publicaciones.get(i * 2);
        publi1.fotosBaja.get(0).setListener(listener);
        LinearLayout layout1 = holder.layer1;
        LinearLayout layoutDis1 = holder.layerDis1;
        final ImageView picture1 = holder.picture1;
        TextView name1 =  holder.name1;
        TextView valor1 =  holder.valor1;
        TextView dis1 =  holder.dis1;

        name1.setText(publi1.nombre);
        valor1.setText(Utility.convertToMoney(publi1.precio));

        if (publi1.oferente.distancia ==  -1){
            layoutDis1.setVisibility(View.GONE);

        }
        else{
            layoutDis1.setVisibility(View.VISIBLE);
        }

        dis1.setText(Utility.convertToDistancia(publi1.distancia));


        layout1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, DetallePublicacion.class);
                i.putExtra("IDPUBLICACION",publi1.id);
                mContext.startActivity(i);
                ((Activity)mContext).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                //((Activity)mContext).finish();  No quitar si se quita se sale de la app
            }
        });

        Bitmap fotoBitmap = publi1.fotosBaja.get(0).getFoto("PUBLICACION");


        if (fotoBitmap != null) {
            picture1.setImageBitmap(fotoBitmap);
        }else {
            picture1.setImageResource(R.drawable.btnexotico);
        }


        //Segundo cajon
        final Publicacion publi2 = publicaciones.get((i * 2) + 1);
        publi2.fotosBaja.get(0).setListener(listener);
        LinearLayout layout2 = holder.layer2;
        LinearLayout layoutDis2 = holder.layerDis2;
        final ImageView picture2 = holder.picture2;
        TextView name2 =  holder.name2;
        TextView valor2 =  holder.valor2;
        TextView dis2 =  holder.dis2;


        name2.setText(publi2.nombre);
        valor2.setText(Utility.convertToMoney(publi2.precio));


        if (publi2.oferente.distancia ==  -1){
            layoutDis2.setVisibility(View.GONE);

        }
        else {

            layoutDis2.setVisibility(View.VISIBLE);
        }

        dis2.setText(Utility.convertToDistancia(publi2.distancia));


        layout2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, DetallePublicacion.class);
                i.putExtra("IDPUBLICACION",publi2.id);
                mContext.startActivity(i);
                ((Activity)mContext).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                //((Activity)mContext).finish();  No quitar si se quita se sale de la app
            }
        });



        Bitmap fotoBitmap2 = publi2.fotosBaja.get(0).getFoto("PUBLICACION");


        if (fotoBitmap2 != null) {
            picture2.setImageBitmap(fotoBitmap2);
        }else {
            picture2.setImageResource(R.drawable.btnexotico);
        }




        return convertView;
    }



    private static class ViewHolder {
        public LinearLayout layer1;
        public LinearLayout layer2;

        public LinearLayout layerDis1;
        public LinearLayout layerDis2;

        public TextView name1;
        public TextView name2;
        public TextView valor1;
        public TextView valor2;
        public TextView dis1;
        public TextView dis2;
        public ImageView picture1;
        public ImageView picture2;

    }


    @Override
    public void notifyDataSetChanged() {

        Collections.sort(publicaciones);

        super.notifyDataSetChanged();
    }
}
