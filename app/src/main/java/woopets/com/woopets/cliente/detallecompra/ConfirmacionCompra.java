package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.ModelCliente.OnModelClienteListener;

public class ConfirmacionCompra extends Activity {

    ImageView foto;
    TextView nombre, valor, fecha, cantidad, subtotal, total, pol1, pol2;
    LinearLayout botones;
    ModelCliente modelc = ModelCliente.getInstance();


    int cantidadSeleccionada = modelc.compraActual.cantidad;

    Space espacioServicio;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmacion_compra);

        nombre = (TextView) findViewById(R.id.nombre);
        valor = (TextView) findViewById(R.id.valor);
        fecha = (TextView) findViewById(R.id.fecha);
        cantidad = (TextView) findViewById(R.id.cantidad);
        subtotal = (TextView) findViewById(R.id.subtotal);
        total = (TextView) findViewById(R.id.total);
        foto = (ImageView) findViewById(R.id.foto);
        espacioServicio = (Space) findViewById(R.id.espacioServicio);
        botones = (LinearLayout) findViewById(R.id.botones);
        pol1 = (TextView) findViewById(R.id.pol1);
        pol2 = (TextView) findViewById(R.id.pol2);



        if (savedInstanceState != null &&  modelc.publicaciones.size() == 0) {

            nombre.setText("");
            valor.setText(Utility.convertToMoney(0));
            subtotal.setText(Utility.convertToMoney(0));
            total.setText(Utility.convertToMoney(0));

            final String idPublicacion = savedInstanceState.getString("IDPUBLICACION");
            cantidad.setText(savedInstanceState.getString("CANTIDAD"));
            modelc.compraActual.cantidad  =  Integer.parseInt(cantidad.getText().toString());
            modelc.compraActual.idPublicacion = idPublicacion;
            cantidadSeleccionada = Integer.parseInt(cantidad.getText().toString());


            modelc.reiniciarCarga(idPublicacion, new OnModelClienteListener() {
                @Override
                public void terminoPrecarga() {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pintarVista(idPublicacion);

                        }
                    });

                }
            });
            return;

        }


        pintarVista(modelc.compraActual.idPublicacion);

    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString("IDPUBLICACION", modelc.compraActual.idPublicacion);
        outState.putString("CANTIDAD", cantidad.getText().toString());

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }



    
    private void pintarVista(String idPublicacion){

        final Publicacion publi = modelc.getPublicacionById(modelc.compraActual.idPublicacion);

        int valInt = Integer.valueOf(publi.precio);
        int subtotalInt = valInt * cantidadSeleccionada;

        nombre.setText(publi.nombre);
        valor.setText(Utility.convertToMoney(publi.precio) + " c/u");
        fecha.setText(modelc.compraActual.fecha + " " + modelc.compraActual.hora);
        cantidad.setText(cantidadSeleccionada + "");
        String subTotalCad = Utility.convertToMoney(valInt * cantidadSeleccionada);
        subtotal.setText(subTotalCad);
        total.setText(subTotalCad);
        foto.setImageBitmap(publi.fotosBaja.get(0).getFoto("PUBLI"));
        publi.fotosBaja.get(0).setListener(new OnImageFireChange() {
            @Override
            public void cargoImagen(String tipo) {
                foto.setImageBitmap(publi.fotosBaja.get(0).getFoto("PUBLI"));
            }
        });

        if (!publi.servicio) {
            espacioServicio.setVisibility(View.GONE);
            fecha.setVisibility(View.GONE);

        }
        else {
            botones.setVisibility(View.GONE);
            pol1.setVisibility(View.GONE);
            pol2.setVisibility(View.GONE);
        }

    }


    public void didTapMenosUno(View v) {
        cantidadSeleccionada--;
        if (cantidadSeleccionada < 1) {
            cantidadSeleccionada = 1;
        }

        Publicacion publi = modelc.getPublicacionById(modelc.compraActual.idPublicacion);
        cantidad.setText(cantidadSeleccionada+"");
        int valInt = Integer.valueOf(publi.precio);
        String subTotalCad = Utility.convertToMoney(valInt * cantidadSeleccionada);
        subtotal.setText(subTotalCad);
        total.setText(subTotalCad);



    }

    public void didTapMasUno(View v) {
        Publicacion publi = modelc.getPublicacionById(modelc.compraActual.idPublicacion);
        cantidadSeleccionada++;
        cantidad.setText(cantidadSeleccionada+"");
        int valInt = Integer.valueOf(publi.precio);
        String subTotalCad = Utility.convertToMoney(valInt * cantidadSeleccionada);
        subtotal.setText(subTotalCad);
        total.setText(subTotalCad);

    }


    public void didTapAceptar(View v) {

        Publicacion publi = modelc.getPublicacionById(modelc.compraActual.idPublicacion);

        if (!publi.servicio &&  publi.stock <  cantidadSeleccionada) {

             showAlerta("Alerta","No hay suficiente inventario para tu pedido" );
             return;
        }



        modelc.compraActual.cantidad = cantidadSeleccionada;
        Intent i = new Intent(getApplicationContext(), ConfirmacionDireccion.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }


    public void showAlerta(String titulo, String mensaje){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ConfirmacionCompra.this);

        // set title
        alertDialogBuilder.setTitle(""+titulo);

        // set dialog message
        alertDialogBuilder
                .setMessage(""+mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }



}
