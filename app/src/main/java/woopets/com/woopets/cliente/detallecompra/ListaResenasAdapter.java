package woopets.com.woopets.cliente.detallecompra;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.Item;
import woopets.com.woopets.Modelo.cliente.Calificacion;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Chat;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoChat;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;


/**
 * Created by andres on 10/31/17.
 */

public class ListaResenasAdapter  extends BaseAdapter {


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private final LayoutInflater mInflater;

    ArrayList<Calificacion> calificaciones ;



    public ListaResenasAdapter(Context c, ArrayList<Calificacion> calificaciones) {
        mContext = c;
        mInflater = LayoutInflater.from(c);

        this.calificaciones = calificaciones;

    }


    @Override
    public int getCount() {
        return calificaciones.size();
    }

    @Override
    public Object getItem(int i) {
        return calificaciones.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {


        ViewHolder holder;

        Calificacion cali = calificaciones.get(i);


        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.item_resena,parent, false);

            holder = new ViewHolder();
            holder.hora = (TextView) convertView.findViewById(R.id.hora);
            holder.comentario = (TextView) convertView.findViewById(R.id.comentario);
            holder.star1 = (ImageView) convertView.findViewById(R.id.estre1);
            holder.star2 = (ImageView) convertView.findViewById(R.id.estre2);
            holder.star3 = (ImageView) convertView.findViewById(R.id.estre3);
            holder.star4 = (ImageView) convertView.findViewById(R.id.estre4);
            holder.star5 = (ImageView) convertView.findViewById(R.id.estre5);

            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }


        TextView hora = holder.hora;
        TextView mensaje = holder.comentario;

        hora.setText(cali.fecha);
        mensaje.setText(cali.comentario);

        pintarEstrellas(holder.star1,holder.star2, holder.star3,holder.star4,holder.star5, cali.calificacion);



        return convertView;

    }


    private void pintarEstrellas(ImageView s1, ImageView s2, ImageView s3, ImageView s4, ImageView s5, double calificacion){
        pintarApagarPrenderEstrella(s1,1,calificacion);
        pintarApagarPrenderEstrella(s2,2,calificacion);
        pintarApagarPrenderEstrella(s3,3,calificacion);
        pintarApagarPrenderEstrella(s4,4,calificacion);
        pintarApagarPrenderEstrella(s5,5,calificacion);
    }


    private void pintarApagarPrenderEstrella(ImageView star, double pos, double nota){

        if (nota >= pos) {
            star.setImageResource(R.drawable.imgestrellacompleta);
            return;
        }

        if (Math.abs(nota-pos) <= 0.5) {
            star.setImageResource(R.drawable.imgestrellamedia);
            return;
        }

        star.setImageResource(R.drawable.imgestrellavacia);


    }



    private static class ViewHolder {

        public TextView hora;
        public ImageView star1, star2, star3,star4,star5;
        public TextView comentario;



    }
}
