package woopets.com.woopets.cliente.home;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnScrollChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridLayout.LayoutParams;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by andres on 10/19/17.
 */

public class HomeFragmento extends Fragment implements OnImageFireChange, OnClickListener {


    ModelCliente modelc = ModelCliente.getInstance();
    private DestacadosPageAdapter pageAdapter;
    private GrillaProductosAdapter grillaAdapter;
    private GridView gridview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View vi =  inflater.inflate(R.layout.tab_home, container, false);

        final ScrollView scroll = (ScrollView) vi.findViewById(R.id.scroll);



        ///////////////////////////INICIO    GRILLLA


        gridview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                Publicacion publi = modelc.publicaciones.get(position);
                Intent i = new Intent(getContext(), DetallePublicacion.class);
                i.putExtra("IDPUBLICACION",publi.id);
                getContext().startActivity(i);
                ((Activity)getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                ((Activity)getContext()).finish();
            }
        });




        scroll.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                vi.findViewById(R.id.grilla).getParent()
                        .requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });



        gridview.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {


                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });



        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        ViewGroup.LayoutParams params = gridview.getLayoutParams();
        params.height = (int)(size.y * 0.7);

        gridview.setLayoutParams(params);
        //----Header 1




        ///////////////////////////FIN    GRILLLA

        /////////////////////////// INICIO DESTACADOS

        ViewPager pager = (ViewPager) vi.findViewById(R.id.pager);

        if (modelc.destacados.size() > 0) {
            pageAdapter = new DestacadosPageAdapter(getContext(),this);

            pager.setAdapter(pageAdapter);

            TabLayout tabpuntos = (TabLayout) vi.findViewById(R.id.tab_puntos);
            tabpuntos.setupWithViewPager(pager, true);

        } else {
            pager.setVisibility(View.GONE);
        }




        //////////Barra de categorias
        ((LinearLayout)vi.findViewById(R.id.optAccesorios)).setOnClickListener(this);
        ((LinearLayout)vi.findViewById(R.id.optAmiguitos)).setOnClickListener(this);
        ((LinearLayout)vi.findViewById(R.id.optGuarderia)).setOnClickListener(this);
        ((LinearLayout)vi.findViewById(R.id.optMedicamentos)).setOnClickListener(this);
        ((LinearLayout)vi.findViewById(R.id.optNutricion)).setOnClickListener(this);
        ((LinearLayout)vi.findViewById(R.id.optPaseador)).setOnClickListener(this);
        ((LinearLayout)vi.findViewById(R.id.optSalud)).setOnClickListener(this);
        ((LinearLayout)vi.findViewById(R.id.optLindos)).setOnClickListener(this);
        ///////////////////

        gridview = (GridView) vi.findViewById(R.id.grilla);
        grillaAdapter = new GrillaProductosAdapter(getContext(),this);
        gridview.setAdapter(grillaAdapter);



        return vi;
    }




    @Override
    public void cargoImagen(String tipo) {
        if (tipo.equals("DESTACADO")) {
            pageAdapter.notifyDataSetChanged();
        }
        if (tipo.equals("PUBLICACION")) {

            grillaAdapter.notifyDataSetChanged();
        }

    }


    @Override
    public void onClick(View view) {



        switch (view.getId()) {

            case R.id.optNutricion: irCategorias("Nutrición");
                break;
            case R.id.optAccesorios: irCategorias("Accesorios");
                break;
            case R.id.optGuarderia: irCategorias("Guardería 5 patas");
                break;
            case R.id.optAmiguitos: irCategorias("Amiguitos en el cielo");
                break;
            case R.id.optMedicamentos: irCategorias("Medicamentos");
                break;
            case R.id.optPaseador: irCategorias("Paseador");
                break;
            case R.id.optLindos: irCategorias("Lind@ y Limpi@");
                break;
            case R.id.optSalud: irCategorias("Vamos al médico");
                break;

        }

    }


    public void irCategorias(String categoria){

        Intent i = new Intent(getApplicationContext(),FiltroCategoria.class);
        i.putExtra("CATEGORIA", categoria);
        startActivity(i);
        ((Activity)getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
        ((Activity)getContext()).finish();
    }
}
