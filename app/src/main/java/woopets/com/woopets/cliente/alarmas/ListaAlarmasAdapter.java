package woopets.com.woopets.cliente.alarmas;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.SystemClock;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Alarma;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoAlarmas;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.ConfirmacionCompra;


/**
 * Created by andres on 10/15/17.
 */

public class ListaAlarmasAdapter extends BaseAdapter {


    private Context mContext;
    private LayoutInflater mInflater;
    ModelCliente modelc = ModelCliente.getInstance();
    Mascota mascota;


    public ListaAlarmasAdapter(Context context, Mascota mascota){

        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mascota = mascota;
    }


    @Override
    public int getCount() {
        return mascota.alarmas.size();
    }

    @Override
    public Object getItem(int i) {
        return mascota.alarmas.get(i);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.item_alarma, parent, false);
        TextView nombre =  (TextView) rowView.findViewById(R.id.nombre);
        TextView tipo = (TextView) rowView.findViewById(R.id.tipo);
        TextView fecha = (TextView) rowView.findViewById(R.id.fecha);

        LinearLayout layoutGeneral = (LinearLayout) rowView.findViewById(R.id.layoutGeneral);
        FrameLayout btnBorrar = (FrameLayout) rowView.findViewById(R.id.borrar);
        FrameLayout frameCheck = (FrameLayout) rowView.findViewById(R.id.fondoCheck);

        final CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.check);


        final Alarma alarma = mascota.alarmas.get(i);


        nombre.setText(alarma.nombre);
        tipo.setText(alarma.tipoRecordatorio);
        fecha.setText(alarma.fechaInicio + " " + alarma.hora);

        if (alarma.activada){
            checkBox.setChecked(true);

        }else {
            checkBox.setChecked(false);
        }



        btnBorrar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mascota.eliminarAlarma(modelc.cliente.id, alarma.id);
                ListaAlarmasAdapter.this.notifyDataSetChanged();

            }
        });

        frameCheck.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                alarma.activada = !alarma.activada;
                checkBox.setChecked(alarma.activada);
                /*if (alarma.activada){
                    alarma.activada = false;
                    checkBox.setChecked(false);


                }
                else {
                    alarma.activada = true;
                    checkBox.setChecked(true);

                }*/
                new ComandoAlarmas().setEstadoAlarma(modelc.cliente.id, mascota.id,alarma.id,checkBox.isChecked());


            }
        });


        checkBox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                alarma.activada = !alarma.activada;
                new ComandoAlarmas().setEstadoAlarma(modelc.cliente.id, mascota.id,alarma.id,checkBox.isChecked());

                if (alarma.activada){
                    startAlarma(alarma);
                }
                else {
                   stopAlarma();

                }
            }
        });

        layoutGeneral.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, CrearAlarma.class);

                i.putExtra("IDMASCOTA",mascota.id);
                i.putExtra("IDALARMA",alarma.id);
                mContext.startActivity(i);

            }
        });

        return rowView;
    }


    public void startAlarma(Alarma alarma){

        NotificationScheduler.setRecordatorio(mContext, AlarmaNotificationReciver.class, 7, alarma.getInicio() , alarma.idAlarmaManager);

    }


    public void stopAlarma(){

        AlarmManager manager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, AlarmaNotificationReciver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);

        manager.cancel(pendingIntent);

    }

}

