package woopets.com.woopets.cliente.home;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.CmdNoti.OnCmdNotificacionesListener;
import woopets.com.woopets.cliente.ListaMacotasAdapter;

public class Notificaciones extends Activity {


    private ListView mListView;
    private NotificacionesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificaciones);


        mAdapter = new NotificacionesAdapter(this);

        mListView = (ListView) findViewById(R.id.lista);

        mListView.setEmptyView(findViewById(R.id.vacio));
        mListView.setAdapter(mAdapter);

        CmdNoti.getInstance().registrarListener(new OnCmdNotificacionesListener() {
            @Override
            public void cargoNotificacion() {
                mAdapter.notifyDataSetChanged();
            }
        });


    }
}
