package woopets.com.woopets.cliente.carrito;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.Item;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Chat;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoChat;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;


/**
 * Created by andres on 10/31/17.
 */

public class ChatAdapter  extends BaseAdapter {


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private final LayoutInflater mInflater;

    ArrayList<Chat> chats ;
    TextView hora, mensaje;


    public ChatAdapter(Context c, ArrayList<Chat> chats) {
        mContext = c;
        mInflater = LayoutInflater.from(c);

        this.chats = chats;

    }
    

    @Override
    public int getCount() {
        return chats.size();
    }

    @Override
    public Object getItem(int i) {
        return chats.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {


        //ViewHolder holder;

        Chat chat = chats.get(i);

        new ComandoChat(null).marcarComoLeido(chat.id);


        /*if (convertView == null) {
            if (chat.isMsgDelCliente(modelc.cliente.id)){
                convertView = mInflater.inflate(R.layout.item_lista_chat,parent, false);
            } else{
                convertView = mInflater.inflate(R.layout.item_lista_chat_izq,parent, false);
            }

            holder = new ViewHolder();
            holder.hora = (TextView) convertView.findViewById(R.id.hora);
            holder.mensaje = (TextView) convertView.findViewById(R.id.mensaje);
            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }*/


        if (chat.isMsgDelCliente(modelc.cliente.id)){
            convertView = mInflater.inflate(R.layout.item_lista_chat,parent, false);
        } else{
            convertView = mInflater.inflate(R.layout.item_lista_chat_izq,parent, false);
        }

        TextView hora = (TextView) convertView.findViewById(R.id.hora);
        TextView mensaje = (TextView) convertView.findViewById(R.id.mensaje);

        hora.setText(chat.fechaMensaje);
        mensaje.setText(chat.mensaje);

        return convertView;

    }


   /* private static class ViewHolder {

        public TextView hora;
        public TextView mensaje;


    }*/
}
