package woopets.com.woopets.cliente.detallecompra;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ClienteRegistroBasicos;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * Created by andres on 10/31/17.
 */
public class  CambiarDireccionAdapter extends BaseAdapter {


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private final LayoutInflater mInflater;
    private ProgressDialog progressDialog;



    public CambiarDireccionAdapter(Context c, Activity act) {
        mContext = c;
        mInflater = LayoutInflater.from(c);
        progressDialog = new ProgressDialog(act);

    }


    @Override
    public int getCount() {
        return modelc.direcciones.size();
    }

    @Override
    public Object getItem(int i) {
        return modelc.direcciones.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {


        ViewHolder holder;

        final Direccion dir = modelc.direcciones.get(i);


        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.item_cambiar_direccion,parent, false);

            holder = new ViewHolder();
            holder.nombre = (TextView) convertView.findViewById(R.id.nombre);
            holder.direccion = (TextView) convertView.findViewById(R.id.direccion);
            holder.btnEliminar = (FrameLayout) convertView.findViewById(R.id.borrar);
            holder.layerSeleccion = (FrameLayout) convertView.findViewById(R.id.layerSeleccion);
            holder.check = (CheckBox) convertView.findViewById(R.id.checkbox);

            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }


        TextView nombre = holder.nombre;
        TextView direccion = holder.direccion;
        final CheckBox check = holder.check;
        FrameLayout layerSeleccion = holder.layerSeleccion;
        FrameLayout btnEliminar = holder.btnEliminar;

        nombre.setText(dir.nombre);
        direccion.setText(dir.direccion);


        if (dir.porDefecto) {
            check.setChecked(true);

        }else{
            check.setChecked(false);
        }


        layerSeleccion.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                activarDireccion(check, dir);

            }
        });

        btnEliminar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (modelc.direcciones.size()==1) {
                    return;
                }
                modelc.eliminarDireccion(dir.id);
                if (dir.porDefecto){
                    modelc.activarCualquierDireccion();
                    modelc.compraActual.direccion = modelc.getDireccionActiva().direccion;
                    modelc.compraActual.departamento = modelc.getDireccionActiva().estado;

                }
                CambiarDireccionAdapter.this.notifyDataSetChanged();


            }
        });

        check.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                activarDireccion(check, dir);

            }
        });

        direccion.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), ClienteRegistroBasicos.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(i);
                //mContext.overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

            }
        });



        return convertView;

    }


    private static class ViewHolder {

        public TextView nombre;
        public TextView direccion;
        public FrameLayout layerSeleccion;
        public CheckBox check;
        public FrameLayout btnEliminar;


    }

    private void activarDireccion(CheckBox check, Direccion dir){

       boolean noRegarcar = check.isChecked();

        check.setChecked(true);
        modelc.activarDireccion(dir.id);
        modelc.compraActual.direccion = modelc.getDireccionActiva().direccion;
        modelc.compraActual.departamento = modelc.getDireccionActiva().estado;

        CambiarDireccionAdapter.this.notifyDataSetChanged();


        progressDialog.setMessage("Buscando productos y servicios cercanos...");
        progressDialog.show();

        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {

            }

            @Override
            public void cargoPublicacion() {
                progressDialog.dismiss();


            }
        }).getOferentesCercanos(modelc.getDireccionActiva().ubicacion);

    }
}


