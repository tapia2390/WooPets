package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.FirebaseFunctionsException;
import com.google.firebase.functions.HttpsCallableResult;


import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.InfoCompraActual;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.PagoTarjetaCliente;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ClienteCrearTarjeta;
import woopets.com.woopets.cliente.Comandos.CmdIp;
import woopets.com.woopets.cliente.Comandos.CmdIp.OnIpListener;
import woopets.com.woopets.cliente.Comandos.CmdPayU;
import woopets.com.woopets.cliente.Comandos.CmdPayU.OnCheckPagos;
import woopets.com.woopets.cliente.Comandos.CmdPayU.OnCheckTarjetas;
import woopets.com.woopets.cliente.Comandos.ComadoPago;
import woopets.com.woopets.cliente.Comandos.ComadoPago.OnComadoPagoListener;
import woopets.com.woopets.cliente.Comandos.ComandoInventario;
import woopets.com.woopets.cliente.Comandos.ComandoInventario.OnComandoInventarioListener;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.ModelCliente.OnModelClienteListener;
import woopets.com.woopets.payU.PagoPayU;


public class ConfirmacionDireccion extends Activity {


    TextView nombre, direccionLabel, direccion, cambiarDir, cambiarMet, telefono, metodo, total;
    ModelCliente modelc = ModelCliente.getInstance();
    Publicacion publi;
    Cliente cliente = modelc.cliente;
    private ProgressDialog progressDialog;
    private FirebaseFunctions mFunctions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmacion_direccion);



        progressDialog = new ProgressDialog(this);

        mFunctions = FirebaseFunctions.getInstance();

        nombre = (TextView) findViewById(R.id.nombre);
        direccionLabel = (TextView) findViewById(R.id.dirLabel);
        direccion = (TextView) findViewById(R.id.direccion);
        cambiarDir = (TextView) findViewById(R.id.cambiarDir);
        cambiarMet = (TextView) findViewById(R.id.cambiarMet);
        telefono = (TextView) findViewById(R.id.telefono);
        metodo = (TextView) findViewById(R.id.metodo);
        total = (TextView) findViewById(R.id.total);

        nombre.setText("");
        direccion.setText("");
        telefono.setText("");
        metodo.setText("");
        total.setText("");



        if (savedInstanceState != null ) {


            final String idPublicacion = savedInstanceState.getString("IDPUBLICACION");
            modelc.compraActual.cantidad  =  savedInstanceState.getInt("CANTIDAD");
            modelc.compraActual.idPublicacion = idPublicacion;

            Log.i("SIG","CONF DIRECCION  =" + idPublicacion);


            modelc.reiniciarCarga(idPublicacion, new OnModelClienteListener() {
                @Override
                public void terminoPrecarga() {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pintarVista(idPublicacion);

                        }
                    });
                }
            });
            return;

        }

        publi = modelc.getPublicacionById(modelc.compraActual.idPublicacion);
        cliente = modelc.cliente;
        pintarVista(modelc.compraActual.idPublicacion);


    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString("IDPUBLICACION", modelc.compraActual.idPublicacion);
        outState.putInt("CANTIDAD", modelc.compraActual.cantidad);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }



    private void pintarVista(String idPublicacion) {

        publi = modelc.getPublicacionById(idPublicacion);

        Log.i("SIG","Pintar vista  num=" + publi.nombre );

        cliente = modelc.cliente;

        nombre.setText(cliente.nombre + " " + cliente.apellido);
        telefono.setText(cliente.celular);
        //metodo.setText(modelc.cliente.metodoPago);
        total.setText(Utility.convertToMoney(Integer.valueOf(publi.precio) * modelc.compraActual.cantidad));

        pintarDireccion();

    }




    public void didTapCambiarDireccion(View v){
        Intent i = new Intent(getApplicationContext(), CambiarDireccion.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }


    public void didTapCambiarMetodo(View v){

        Intent i = new Intent(getApplicationContext(), ClienteCrearTarjeta.class);

        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }


    public void didTapAceptar(View v) {


        if (!estaConectado()) {
            String titulo = "Sin Internet";
            String mensaje = "Sin Conexión a Internet";
            showAlertSinInternet(titulo, mensaje);
            return;
        }


        if (modelc.getTarjetaActiva() == null || !modelc.getTarjetaActiva().activo){
            showAlertSinInternet("Info del pago", "Falta adicionar los datos de la tarjeta");
            return;
        }

        if (modelc.getTarjetaActiva().estado.equals("PENDING")) {
            showAlertSinInternet("Validación Tarjeta", "La tarjeta inscrita será validada por personal de payU y tomará de 1 a 4 horas dicho proceso. Te llegará una notificación informandote si tu tarjeta podrá ser usada para tus compras.");
            return;
        }





        InfoCompraActual info = modelc.compraActual;
        Publicacion publi = modelc.getPublicacionById(info.idPublicacion);

        if (!publi.activo || publi.borrada == true){
            showAlertSinInternet("Error", "Este producto o servicio ha dejado de estar activo");
            return;
        }


        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(ConfirmacionDireccion.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(ConfirmacionDireccion.this);
        }

        String msg = "";

        if (publi.servicio){
            msg = "Con esto confirmas el agendamiento del servicio, el cobro será realizado una vez el establecimiento confirme la prestación del mismo";
        }
        else {
            String val = Utility.convertToMoney(Integer.valueOf(publi.precio) * modelc.compraActual.cantidad);
            msg = "A punto de cargar a tu tarjeta una compra por valor de " + val + ". ¿Estas seguro?";

        }

        builder.setTitle("Confirma tu información")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        procesarPago();
                        //porcesarConMercadoPago();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }




    protected void procesarPago(){

        String idCompra = new ComandoPreguntas(null).getLlave();
        InfoCompraActual info = modelc.compraActual;
        Publicacion publi = modelc.getPublicacionById(info.idPublicacion);


        //Creamos la compra
        final Compra compra = new Compra();
        compra.id = idCompra;
        compra.servicio = publi.servicio;
        compra.cantidad = info.cantidad;
        compra.idPublicacion = publi.id;
        compra.idOferente = publi.idOferente;
        compra.estado = "Pendiente";
        compra.valor = info.getValor();
        compra.metodoPago = modelc.cliente.metodoPago;
        compra.direccion = info.direccion;


        if (publi.servicio) {
            compra.fechaServicio = info.fecha + " " + info.hora;

        }

        ////Ahora si a procesar el pago

        progressDialog.setMessage("Validando la información, por favor espere...");
        progressDialog.show();



       /* if (modelc.cliente.metodoPago.equals("Efectivo")){
            generarOrden(compra, false);
            return;
        }*/


        ///Aca ya es en tarjeta
        if (publi.servicio) {
            compra.fechaServicio = info.fecha + " " + info.hora;
            generarOrden(compra, true);
            return;
        }


        reservarEnInventario(compra);

    }



    protected void reservarEnInventario(final Compra compra){

        new ComandoInventario(new OnComandoInventarioListener() {
            @Override
            public void reservoInventario() {

                /// Aca ya es un producto y con tarjeta.
                //procesarTarjeta(compra);
                procesarTarjetaPayU(compra);

            }

            @Override
            public void insuficienteInventario() {
                showAlertSinInternet("Alerta", "El producto esta agotado, no hay suficiente inventario");
                progressDialog.dismiss();
                return;
            }

            @Override
            public void adicionoInventario() {

            }
        }).reservarCantidad(compra.idPublicacion, compra.cantidad);


    }


    protected void adicionarEnInventario(final Compra compra){

        new ComandoInventario(new OnComandoInventarioListener() {
            @Override
            public void reservoInventario() {


            }

            @Override
            public void insuficienteInventario() {

            }

            @Override
            public void adicionoInventario() {

            }
        }).adicionarCantidad(compra.idPublicacion, compra.cantidad);


    }



    protected void procesarTarjetaPayU(final Compra compra){


        CmdIp.getIp(this, new OnIpListener() {
            @Override
            public void gotIp(String ip) {
                final MiniTarjeta mini = modelc.getTarjetaActiva();

                mini.referenceCode = compra.id;
                mini.ip = ip;
                hacerPagoFinalConTarjeta(compra, modelc.cliente.getMapaToPagoToken(compra, mini), mini);
            }

            @Override
            public void fallo() {

                showAlertSinInternet("Fallo en la transacción", "No pudo iniciar la transacción. Intente más tarde");

            }
        });

    }




    private Task<String> hacerPagoFinalConTarjeta(final Compra compra, Map data, final MiniTarjeta mini) {     //Usamos el token para hacer el pago.



        return mFunctions
                .getHttpsCallable("pagoConTokenAndroid")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        CmdPayU.checkPagos(compra.id, new OnCheckPagos() {
                            @Override
                            public void pagoAprobado(PagoPayU pagoPayU) {
                                progressDialog.dismiss();

                                PagoTarjetaCliente pago = new PagoTarjetaCliente();
                                pago.Descripcion = "Compra Producto";
                                pago.authorizationCode = pagoPayU.authorizationCode;
                                pago.idCompra = compra.id;
                                pago.idTarjeta = mini.id;
                                pago.metodoPago = "Tarjeta";
                                pago.idPago = pagoPayU.orderId;
                                pago.paymentTransaction = pagoPayU.transactionId;
                                modelc.pagoActual = pago;
                                registrarPago(compra, pago);

                            }


                            @Override
                            public void pagoReachazado(String razon) {
                                adicionarEnInventario(compra);
                                showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago, revisa los datos de tu tarjeta: " + razon);
                                progressDialog.dismiss();

                            }


                            @Override
                            public void pagoPendiente(PagoPayU pagoPayU) {
                                //adicionarEnInventario(compra);
                                showAlertSinInternet("Transacción pendiente",  "La transacción esta siendo evaluada por personal de PauY, esto puede tardar entre una a cuatro horas");
                                progressDialog.dismiss();
                            }


                            @Override
                            public void pagoConError(String razon) {
                                adicionarEnInventario(compra);
                                showAlertSinInternet("Fallo en la transacción", "Fallo en la sistema de pagos, intenta más tarde: " + razon);
                                progressDialog.dismiss();
                            }
                        });
                        String result = (String) task.getResult().getData();
                        return result;
                    }
                });
    }


    private Task<Map> hacerPagoFinalConTarjetaViejo(final Compra compra, Map data, final MiniTarjeta mini) {     //Usamos el token para hacer el pago.


        return mFunctions
                .getHttpsCallable("pagoConToken")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        Map result = (Map) task.getResult().getData();
                        return result;
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     adicionarEnInventario(compra);
                                                     progressDialog.dismiss();
                                                     showAlertSinInternet("Fallo en la transacción", "No se pudo completar la transacción, intenta más tarde. Código de error: P2");
                                                     return;

                                                 }


                                                 try {

                                                     Map result = (Map) task.getResult().get("transactionResponse");

                                                     String idOrdenPayU = result.get("orderId").toString();
                                                     String transactionId = result.get("transactionId").toString();
                                                     String authorizationCode = result.get("authorizationCode").toString();
                                                     String responseCode = result.get("responseCode").toString();

                                                     String status = result.get("state").toString();

                                                     if (status.equals("DECLINED")) {
                                                         adicionarEnInventario(compra);
                                                         showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago, revisa los datos de tu tarjeta. Código de error: " + responseCode);
                                                         progressDialog.dismiss();
                                                         return;

                                                     }

                                                     Toast.makeText(getApplicationContext(), "Pago " + result.get("state").toString(), Toast.LENGTH_LONG).show();

                                                     progressDialog.dismiss();

                                                     PagoTarjetaCliente pago = new PagoTarjetaCliente();
                                                     pago.Descripcion = "Compra Producto";
                                                     pago.authorizationCode = authorizationCode;
                                                     pago.idCompra = compra.id;
                                                     pago.idTarjeta = mini.id;
                                                     pago.metodoPago = "Tarjeta";
                                                     pago.idPago = idOrdenPayU;
                                                     pago.paymentTransaction = transactionId;
                                                     modelc.pagoActual = pago;
                                                     registrarPago(compra, pago);

                                                 }
                                                 catch (Exception e){
                                                     adicionarEnInventario(compra);
                                                     progressDialog.dismiss();
                                                     showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago, intenta más tarde. Código de error: P1");
                                                     return;

                                                 }

                                             }
                                         }
                );
    }


  /*
    protected void procesarTarjeta(final Compra compra){


        final MiniTarjeta mini = modelc.getTarjetaActiva();


        //int iacAmount= (int)(Double.parseDouble(""+compra.valor)*0.08);



        final Call<TPPAgoResponse> tppAgoResponseCall = MyApiAdapter.getApiService().generarPago(TPPAgo.create("WooPets App",0,mini.token,compra.valor,0,mini.cuotas,compra.id,"COP"));
        tppAgoResponseCall.enqueue(new retrofit2.Callback<TPPAgoResponse>() {
            @Override
            public void onResponse(Call<TPPAgoResponse> call, Response<TPPAgoResponse> response) {

                if (response.code() == 201) {
                    TPPAgoResponse result = response.body();
                    Gson gson = new GsonBuilder().setPrettyPrinting().create();

                    if(response.body().getTransactionInfo().getStatus().equals("authorized")){
                        PagoTarjetaCliente pago = new PagoTarjetaCliente();
                        pago.Descripcion = "Compra Producto";
                        pago.authorizationCode = response.body().getTransactionInfo().getAuthorizationCode();
                        pago.idCompra = compra.id;
                        pago.idTarjeta = mini.id;
                        pago.metodoPago = "Tarjeta";
                        pago.idPago =  response.body().getId();
                        pago.paymentTransaction = response.body().getPaymentTransaction();
                        modelc.pagoActual = pago;
                        registrarPago(compra);

                        return;

                    }else{
                        adicionarEnInventario(compra);
                        Toast.makeText(getApplicationContext(),"No se pudo realizar la transacción.",Toast.LENGTH_SHORT).show();
                    }
                }
                else if (response.code() >= 500) {
                    adicionarEnInventario(compra);
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Error conectando con el servidor",Toast.LENGTH_SHORT).show();
                    Log.v("TagInfo","Body "+"Error conectando con el servidor"+response.errorBody());
                    return;
                }
                else if (response.code() == 422) {
                    adicionarEnInventario(compra);
                    progressDialog.dismiss();

                    Toast.makeText(getApplicationContext(),"Valide su información o correo invalido",Toast.LENGTH_SHORT).show();

                }else if (response.code() == 402) {
                    String resbodi = "";
                    JSONObject json;
                    String msmgson  ="";

                    progressDialog.dismiss();
                    response.raw();
                    String r1 = response.raw().message();
                    response.errorBody();
                    try {
                        resbodi = response.errorBody().string();
                        JSONObject jsonObj = null;
                        try {
                            jsonObj = new JSONObject(resbodi);
                            msmgson  = jsonObj.get("errorMessage").toString();
                            Log.v("msmgson","msmgson"+msmgson);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    adicionarEnInventario(compra);
                    Toast.makeText(getApplicationContext(),msmgson+". Puedes intentar con otra tarjeta o pagar en efectivo", Toast.LENGTH_SHORT).show();

                    return;
                }

                else{
                    adicionarEnInventario(compra);
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"No se pudo realizar la transacción.",Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            @Override
            public void onFailure(Call<TPPAgoResponse> call, Throwable t) {

                adicionarEnInventario(compra);
                progressDialog.dismiss();

                Log.v("status2","status2"+t.getMessage()+"..."+t.getCause());
                System.out.print(""+t.getMessage()+"..."+t.getCause());

                Toast.makeText(getApplicationContext(),"La transacción no es posible en este momento. Intentelo más tarde.",Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(),""+t.getLocalizedMessage()+" "+t.getMessage(),Toast.LENGTH_SHORT).show();

                return;
            }
        });


    }
*/

    protected void registrarPago(final Compra compra, PagoTarjetaCliente pago){

        new ComadoPago(new OnComadoPagoListener() {
            @Override
            public void didPagoExitoso() {


            }

            @Override
            public void didPagoFallido() {

            }

            @Override
            public void didRegistroExitoso() {
                generarOrden(compra, true);
            }

            @Override
            public void didRegistroFallido() {
                progressDialog.dismiss();
                showAlertSinInternet("Problema con la trasacción", "Se descontó el dinero de su tarjeta de crédito, pero no se pudo registrar el pedido. Comuniquese con WooPets");

            }
        }).registrarPago(compra.id, pago);

    }



    protected void generarOrden(final Compra compra, boolean conTarjeta){

        new ComadoPago(new OnComadoPagoListener() {
            @Override
            public void didPagoExitoso() {
                progressDialog.dismiss();
                modelc.addCompra(compra);
                Intent i = new Intent(getApplicationContext(), PagoExitoso.class);
                i.putExtra("IDCOMPRA", compra.id);
                i.putExtra("ORIGEN", "ENCOMPRA");
                startActivity(i);
                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

                ComandoInventario.checkInventario(compra, publi);
                finish();
            }

            @Override
            public void didPagoFallido() {

            }

            @Override
            public void didRegistroExitoso() {

            }

            @Override
            public void didRegistroFallido() {

            }

        }).crearOrden(compra, conTarjeta);


    }



    //conexion internet
    //validacion conexion internet
    protected Boolean estaConectado(){
        if(conectadoWifi()){
            Log.v("wifi","Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        }else{
            if(conectadoRedMovil()){
                Log.v("Datos", "Tu dispositivo tiene Conexion Movil.");
                return true;
            }else{
                String titulo ="Sin Internet";
                String mensaje ="Sin Conexión a Internet";
                showAlertSinInternet(titulo,mensaje);
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    protected Boolean conectadoWifi(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }



    public void showAlertSinInternet(String titulo, String mensaje){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ConfirmacionDireccion.this);

        // set title
        alertDialogBuilder.setTitle(""+titulo);

        // set dialog message
        alertDialogBuilder
                .setMessage(""+mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public void pintarDireccion(){

        if (publi == null){
            return;
        }

        final MiniTarjeta mini = modelc.getTarjetaActiva();

        if (mini != null ){
            metodo.setText(mini.lastFour );
        }
        else{
            metodo.setText("Sin definir");
        }

        if (publi.servicio) {
            if (publi.servicioEnDomicilio) {
                direccionLabel.setText("Dirección del servicio (domicilio)");
                if (modelc.compraActual.direccion.equals("")) {
                    direccion.setText(modelc.getDireccionActiva().direccion);
                    modelc.compraActual.direccion = modelc.getDireccionActiva().direccion;
                    modelc.compraActual.departamento = modelc.getDireccionActiva().estado;

                }
                else {
                    direccion.setText(modelc.compraActual.direccion);
                }
            }else {
                direccionLabel.setText("Dirección del servicio (tienda)");
                direccion.setText(publi.oferente.direccion);
                modelc.compraActual.direccion = publi.oferente.direccion;
                modelc.compraActual.departamento = "Cundinamarca";  //TODO hacer que publi.oferente tambien tenga la direccion

                cambiarDir.setVisibility(View.GONE);
            }
        } else{
            direccionLabel.setText("Dirección de entrega");

            if (modelc.compraActual.direccion.equals("")) {
                if (modelc.getDireccionActiva() == null){
                    return;
                }
                direccion.setText(modelc.getDireccionActiva().direccion);
                modelc.compraActual.direccion = modelc.getDireccionActiva().direccion;
                modelc.compraActual.departamento = modelc.getDireccionActiva().estado;

            }
            else {
                direccion.setText(modelc.compraActual.direccion);
            }

        }

    }

    @Override
    public void onResume() {
        super.onResume();


        pintarDireccion();

    }

   // public String getIp(){




        /*String ipWifi = getWifiIPAddress();

        if (!ipWifi.equals("0.0.0.0") && !ipWifi.equals("")){
            return  ipWifi;
        }

        return getMobileIPAddress();*/

    //}

    public String getWifiIPAddress() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        return  Formatter.formatIpAddress(ip);
    }


    public static String getMobileIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return  addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            return "0.0.0.0";
        }
        return "0.0.0.0";
    }



}
