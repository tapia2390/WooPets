package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;

import com.github.chrisbanes.photoview.PhotoView;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;

public class ZoomProducto extends Activity {


    ModelCliente modelc = ModelCliente.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_producto);


        String idPublicacion = getIntent().getStringExtra("IDPUBLICACION");
        final int pos = getIntent().getIntExtra("POS",0);

        final Publicacion publi =  modelc.getPublicacionById(idPublicacion);

        final PhotoView photoView = (PhotoView) findViewById(R.id.foto);
        photoView.setImageBitmap(publi.fotosMedia.get(pos).getFoto("PUBLICACION"));


        publi.fotosAlta.get(pos).setListener(new OnImageFireChange() {
            @Override
            public void cargoImagen(String tipo) {

                Bitmap bmap =  publi.fotosAlta.get(pos).getFoto("PUBLICACION");
                if (bmap != null){
                    Matrix m = photoView.getImageMatrix();
                    float escala = photoView.getScale();

                    photoView.setImageBitmap(bmap);
                    photoView.setImageMatrix(m);
                    photoView.setScale(escala);


                }
            }
        });

        Bitmap bmap =  publi.fotosAlta.get(pos).getFoto("PUBLICACION");
        if (bmap != null){
            photoView.setImageBitmap(bmap);
        }





    }
}
