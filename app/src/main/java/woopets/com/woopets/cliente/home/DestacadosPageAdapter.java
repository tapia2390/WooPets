package woopets.com.woopets.cliente.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.HomeCliente;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.ProponerMascota;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;

/**
 * Created by andres on 10/21/17.
 */

public class DestacadosPageAdapter  extends PagerAdapter{


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private OnImageFireChange listener;


    private ArrayList<Publicacion> destacados;
    ImageView fotoDestacado;
    TextView nombre, valor;


    public DestacadosPageAdapter(Context context, OnImageFireChange listener) {
        mContext = context;
        this.listener = listener;

        destacados = new ArrayList<>(modelc.destacados.subList(0,Math.min(5,modelc.destacados.size())));

    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {


        final int pos = position;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.layout_destacado_page_con_franja, container, false);
        fotoDestacado = (ImageView) layout.findViewById(R.id.foto);
        nombre = (TextView) layout.findViewById(R.id.nombre);
        valor = (TextView) layout.findViewById(R.id.valor);


        final Publicacion destacado = destacados.get(position);
        nombre.setText(destacado.nombre);
        valor.setText(Utility.convertToMoney(destacado.precio));
        Bitmap foto = destacado.imagenDestacado.getFoto("DESTACADO");
        destacado.imagenDestacado.setListener(listener);

        if (foto != null) {
            fotoDestacado.setImageBitmap(foto);
        }



        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ComandoPublicacion.sumarClic(destacado.id);
                Intent i = new Intent(mContext, DetallePublicacion.class);
                i.putExtra("IDPUBLICACION",destacado.id);
                mContext.startActivity(i);
                ((Activity)mContext).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                //((Activity)mContext).finish();
            }
        });


        container.addView(layout);

        return layout;


    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return destacados.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}
