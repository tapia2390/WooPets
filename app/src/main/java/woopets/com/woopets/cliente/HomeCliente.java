package woopets.com.woopets.cliente;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.google.firebase.auth.FirebaseAuth;

import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.Comandos.CmdNoti;
import woopets.com.woopets.cliente.Comandos.CmdNoti.OnCmdNotificacionesListener;
import woopets.com.woopets.cliente.Comandos.ComandoAyuda;
import woopets.com.woopets.cliente.Comandos.ComandoCalificacion;
import woopets.com.woopets.cliente.Comandos.ComandoCarrito;
import woopets.com.woopets.cliente.Comandos.ComandoCarrito.OnComandoCarritoChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoCompras;
import woopets.com.woopets.cliente.Comandos.ComandoCompras.OnComandoComprasListener;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion;
import woopets.com.woopets.cliente.Comandos.ComandoPublicacion.OnComandoPublicacionChangeListener;
import woopets.com.woopets.cliente.Comandos.ComandoTips;
import woopets.com.woopets.cliente.home.CarritoFragmento;
import woopets.com.woopets.cliente.home.HomeFragmento;
import woopets.com.woopets.cliente.home.MenuFragmento;
import woopets.com.woopets.cliente.home.NewHomeFragmento;

public class HomeCliente extends AppCompatActivity implements OnTabSelectedListener {


    ModelCliente modelc = ModelCliente.getInstance();

    TextView numero, numeroNotificacionesSinVer;
    FrameLayout frameRojoMenu;
    View vCarrito;
    View vMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


        setContentView(R.layout.activity_home_cliente);

        String tabInicial = "INICIO";

        if (getIntent().getExtras().containsKey("TAB")){
            tabInicial = getIntent().getStringExtra("TAB");
        }

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        boolean yaOmitioMascota = pref.getBoolean("ya_omitio_mascota",false);


        if (modelc.mascotas.size() == 0 && !yaOmitioMascota  && !FirebaseAuth.getInstance().getCurrentUser().isAnonymous()){
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                Intent i = new Intent(getApplicationContext(), ProponerMascota.class);
                startActivity(i);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
                finish();
            } else {
                if (!extras.containsKey("OMITIR_MASCOTA")){
                    Intent i = new Intent(getApplicationContext(), ProponerMascota.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
                    finish();
                    return;
                }

                if (extras.getString("OMITIR_MASCOTA").equals("NO")) {
                    Intent i = new Intent(getApplicationContext(), ProponerMascota.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
                    finish();

                }

            }
        }





        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        //setSupportActionBar(toolbar);


        //Initializing the tablayout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        tabLayout.addOnTabSelectedListener(this);

        tabLayout.getTabAt(0).setIcon(R.drawable.btncarrito);
        tabLayout.getTabAt(0).setText("Carrito");
        tabLayout.getTabAt(1).setIcon(R.drawable.btninicio);
        tabLayout.getTabAt(1).setText("Inicio");
        tabLayout.getTabAt(2).setIcon(R.drawable.btnmenu);
        tabLayout.getTabAt(2).setText("Menú");
        tabLayout.getTabAt(2).getIcon().setColorFilter(Color.GRAY, Mode.SRC_IN);

        vCarrito = (FrameLayout) View.inflate(getApplicationContext(), R.layout.tab_home_item, null);
        View vInicio = (FrameLayout) View.inflate(getApplicationContext(), R.layout.tab_home_item, null);
        vMenu = (FrameLayout) View.inflate(getApplicationContext(), R.layout.tab_home_item, null);



        //1. carrito
        ImageView iconoCarrito = vCarrito.findViewById(R.id.icono);
        iconoCarrito.setImageResource(R.drawable.btncarrito);
        TextView textoCarrito = vCarrito.findViewById(R.id.titulo);
        textoCarrito.setText("Carrito");

        if (modelc.carritos.size() == 0 ){
            vCarrito.findViewById(R.id.layerrojo).setVisibility(View.GONE);
        }else {
            numero = vCarrito.findViewById(R.id.numeroNoti);
            numero.setText("" + modelc.carritos.size());
        }

        vCarrito.findViewById(R.id.layerrojo);
        tabLayout.getTabAt(0).setCustomView(vCarrito);


        //2. inicio
        ImageView iconoInicio = vInicio.findViewById(R.id.icono);
        iconoInicio.setImageResource(R.drawable.btninicio);
        TextView textoInicio = vInicio.findViewById(R.id.titulo);
        textoInicio.setText("Inicio");
        vInicio.findViewById(R.id.layerrojo).setVisibility(View.GONE);
        tabLayout.getTabAt(1).setCustomView(vInicio);


        //3. menu
        ImageView iconoMenu = vMenu.findViewById(R.id.icono);
        iconoMenu.setImageResource(R.drawable.btnmenu);
        TextView textoMenu = vMenu.findViewById(R.id.titulo);
        frameRojoMenu =  vMenu.findViewById(R.id.layerrojo);
        textoMenu.setText("Menú");
        textoMenu.setTextColor(Color.GRAY);
        int numNotificaciones = modelc.notificacionesSinVer();
        if (numNotificaciones == 0) {
            frameRojoMenu.setVisibility(View.GONE);
        } else {
            frameRojoMenu.setVisibility(View.VISIBLE);
            if (numeroNotificacionesSinVer == null){
                numeroNotificacionesSinVer = vMenu.findViewById(R.id.numeroNoti);
            }
            numeroNotificacionesSinVer.setText("" + numNotificaciones);
        }
        tabLayout.getTabAt(2).setCustomView(vMenu);



        tabLayout.setOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                tab.getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                TextView titulo = tab.getCustomView().findViewById(R.id.titulo);
                titulo.setTextColor(Color.WHITE);


            }

            @Override
            public void onTabUnselected(Tab tab) {
                tab.getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                TextView titulo = tab.getCustomView().findViewById(R.id.titulo);
                titulo.setTextColor(Color.GRAY);

            }

            @Override
            public void onTabReselected(Tab tab) {

            }
        });


        if (tabInicial.equals("INICIO")) {
            replaceFragment(new NewHomeFragmento());
            tabLayout.getTabAt(1).select();
        }
        else {
            replaceFragment(CarritoFragmento.newInstance("COMPRAS"));
            tabLayout.getTabAt(0).select();
        }

        CmdNoti.getInstance().registrarListener(new OnCmdNotificacionesListener() {
            @Override
            public void cargoNotificacion() {

               // pintarNotificaciones();

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("UI thread", "I am the UI thread");
                        pintarNotificaciones();

                    }
                });

            }
        });


        new ComandoCarrito().getCarritos(modelc.cliente.id, new OnComandoCarritoChangeListener() {
            @Override
            public void cargoCarrito() {
                if (modelc.carritos.size() == 0 ){
                    vCarrito.findViewById(R.id.layerrojo).setVisibility(View.GONE);
                }else {
                    numero = vCarrito.findViewById(R.id.numeroNoti);
                    numero.setText("" + modelc.carritos.size());
                }

            }
        });



    }



    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }



    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }



    @Override
    public void onTabSelected(Tab tab) {
        if (tab.getPosition() == 0) {
            replaceFragment(CarritoFragmento.newInstance("CARRITO"));
        } else if (tab.getPosition() == 1) {
            replaceFragment(new NewHomeFragmento());
        } else {
            replaceFragment(new MenuFragmento());
        }

    }



    @Override
    public void onTabUnselected(Tab tab) {

    }

    @Override
    public void onTabReselected(Tab tab) {

    }


    /* @Override
    public void onBackPressed() {

    }*/



    @Override
    protected void onRestart() {
        super.onRestart();
        //View vCarrito = (FrameLayout) View.inflate(getApplicationContext(), R.layout.tab_home_item, null);
        //numero = vCarrito.findViewById(R.id.numeroNoti);

        if (numero != null) {
            numero.setText("" + modelc.carritos.size());
        }
        else {
            View vCarrito = (FrameLayout) View.inflate(getApplicationContext(), R.layout.tab_home_item, null);
            numero = vCarrito.findViewById(R.id.numeroNoti);
            numero.setText("" + modelc.carritos.size());

        }

        if (modelc.publicaciones.size() == 0 ) {
            preCargar();
        }

        pintarNotificaciones();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (modelc.publicaciones.size() == 0 ) {
            preCargar();
        }
        pintarNotificaciones();
    }


    private void pintarNotificaciones(){


        if (vMenu == null){
            vMenu = View.inflate(getApplicationContext(), R.layout.tab_home_item, null);

        }
        int numNotificaciones = modelc.notificacionesSinVer();
        Log.d("UI thread", "NOTIFICACIONES: " + numNotificaciones);

        if (numNotificaciones == 0) {
            frameRojoMenu.setVisibility(View.GONE);
        } else {
            //si no exit
            frameRojoMenu.setVisibility(View.VISIBLE);
            if (numeroNotificacionesSinVer == null){
                numeroNotificacionesSinVer = vMenu.findViewById(R.id.numeroNoti);
            }
            numeroNotificacionesSinVer.setText("" + numNotificaciones);
        }

    }


    private void preCargar() {

        String tipo = "";
        Mascota mascota = modelc.getMascotaActiva();
        if (mascota != null) {
            tipo = mascota.tipo;
        }


        new ComandoCalificacion(null).getMisCalificaciones(modelc.cliente.id);
        new ComandoTips().getTips();
        new ComandoTips().getPolitica();
        new ComandoAyuda(null).getInicales();
        CmdNoti.getInstance().getNotificaciones();


        Direccion direccion = modelc.getDireccionActiva();


        new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
            @Override
            public void cargoDestacado() {

            }

            @Override
            public void cargoPublicacion() {
                ///Pilas aca no puede ir nada// o jamas se va a llamar
            }
        }).getLast6Destacados(tipo);

        if (direccion != null && direccion.ubicacion != null) {

            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {

                }

                @Override
                public void cargoPublicacion() {

                }
            }).getOferentesCercanos(direccion.ubicacion);

        } else {
            new ComandoPublicacion(new OnComandoPublicacionChangeListener() {
                @Override
                public void cargoDestacado() {
                    ///Pilas aca no puede ir nada// o jamas se va a llamar
                }

                @Override
                public void cargoPublicacion() {

                }
            }).getTodasPublicaciones(tipo);


            new ComandoCompras(new OnComandoComprasListener() {
                @Override
                public void cargoNuevaCompra() {

                }

                @Override
                public void cargoModificacionCompra() {

                }
            }).getComprasAbiertas(modelc.cliente.id);

            new ComandoCompras(new OnComandoComprasListener() {
                @Override
                public void cargoNuevaCompra() {

                }

                @Override
                public void cargoModificacionCompra() {

                }
            }).getComprasCerradas(modelc.cliente.id);




        }
    }
}
