package woopets.com.woopets.cliente.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.cliente.Mascota;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;

/**
 * Created by Andres on 3/27/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyView> {

    ModelCliente modelc = ModelCliente.getInstance();
    private ArrayList<Publicacion> publicaciones;
    private Context context;


    public class MyView extends RecyclerView.ViewHolder {

        public ImageView picture;
        public TextView name;
        public TextView valor;
        public LinearLayout principal;


        public MyView(View view) {
            super(view);

            picture = (ImageView) view.findViewById(R.id.foto);
            name = (TextView) view.findViewById(R.id.text);
            valor = (TextView) view.findViewById(R.id.valor);
            principal = (LinearLayout) view.findViewById(R.id.principal);


        }
    }


    public RecyclerViewAdapter(Context context, String categoria) {
        this.context = context;

        Mascota mascota = modelc.getMascotaActiva();

        if (mascota == null){
            return;
        }else  {
            publicaciones = modelc.getPublicacionesXCategoriaPeroConMascota(categoria, mascota);
        }



    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_imagen_categoria, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        final Publicacion publi = publicaciones.get(position);
        publi.fotosBaja.get(0).setListener(new OnImageFireChange() {
            @Override
            public void cargoImagen(String tipo) {
                if (tipo.equals("PUBLICACION")) {
                    Publicacion publi = publicaciones.get(position);
                    Bitmap fotoBitmap = publi.fotosBaja.get(0).getFoto("PUBLICACION");


                    if (fotoBitmap != null) {
                        holder.picture.setImageBitmap(fotoBitmap);
                    } else {
                        holder.picture.setImageResource(R.drawable.btnexotico);
                    }
                }

            }
        });


        holder.name.setText(publi.nombre);
        holder.valor.setText(Utility.convertToMoney(publi.precio));

        if (publi.fotosBaja.size() == 0) {
            return;
        }

        Bitmap fotoBitmap = publi.fotosBaja.get(0).getFoto("PUBLICACION");


        if (fotoBitmap != null) {
            holder.picture.setImageBitmap(fotoBitmap);
        } else {
            holder.picture.setImageResource(R.drawable.btnexotico);
        }


        holder.principal.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                Intent i = new Intent(context, DetallePublicacion.class);
                i.putExtra("IDPUBLICACION",publi.id);
                v.getContext().startActivity(i);
                ((Activity)v.getContext()).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                ((Activity)v.getContext()).finish();
            }

        });


    }

    @Override
    public int getItemCount() {

        Mascota mascota = modelc.getMascotaActiva();

        if (mascota == null){
            return 0;
        }

        return publicaciones.size();
    }

}

