
package woopets.com.woopets.cliente.carrito;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.ImageFire.OnImageFireChange;
import woopets.com.woopets.Modelo.Item;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.EstadosCompra;
import woopets.com.woopets.Modelo.cliente.ItemCarrito;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoChat;
import woopets.com.woopets.cliente.Comandos.ComandoChat.OnComandoChatListener;
import woopets.com.woopets.cliente.Comandos.ComandoCompras;
import woopets.com.woopets.cliente.ListaMacotasAdapter;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.DetallePublicacion;
import woopets.com.woopets.cliente.detallecompra.PagoExitoso;


/**
 * Created by andres on 10/31/17.
 */

public class MisComprasAdapter  extends BaseAdapter {


    ModelCliente modelc = ModelCliente.getInstance();
    private Context mContext;
    private OnImageFireChange listener;
    private final LayoutInflater mInflater;

    ImageView star1, star2, star3,star4,star5;



    public MisComprasAdapter(Context c, OnImageFireChange listener) {
        mContext = c;
        this.listener = listener;
        mInflater = LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return modelc.misCompras.size();
    }

    @Override
    public Object getItem(int i) {
        return modelc.misCompras.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {


        MisComprasAdapter.ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_miscompras,parent, false);
            holder = new MisComprasAdapter.ViewHolder();
            holder.estado = (TextView) convertView.findViewById(R.id.estado);
            holder.foto = (ImageView) convertView.findViewById(R.id.foto);
            holder.nombre = (TextView) convertView.findViewById(R.id.nombre);
            holder.reprogramada = (TextView) convertView.findViewById(R.id.lbReprogramado);

            holder.layout = (LinearLayout) convertView.findViewById(R.id.linear);
            holder.layerConfirmar = (LinearLayout) convertView.findViewById(R.id.layerConfirmar);
            holder.layerCalificar = (LinearLayout) convertView.findViewById(R.id.layerCalificar);
            holder.layerEstrellas = (LinearLayout) convertView.findViewById(R.id.layerEstrellas);
            holder.fechaServicio = (TextView) convertView.findViewById(R.id.fechaServicio);
            holder.btnConfirmar = (TextView) convertView.findViewById(R.id.btnConfirmar);
            holder.btnRechazar = (TextView) convertView.findViewById(R.id.btnRechazar);
            holder.btnCalificar = (TextView) convertView.findViewById(R.id.btnCalificar);
            holder.btnChat = (ImageView) convertView.findViewById(R.id.btnChat);

            holder.star1 = (ImageView) convertView.findViewById(R.id.estre1);
            holder.star2 = (ImageView) convertView.findViewById(R.id.estre2);
            holder.star3 = (ImageView) convertView.findViewById(R.id.estre3);
            holder.star4 = (ImageView) convertView.findViewById(R.id.estre4);
            holder.star5 = (ImageView) convertView.findViewById(R.id.estre5);

            holder.layerSinLeer = (FrameLayout) convertView.findViewById(R.id.layerrojo);
            holder.sinLeer = (TextView) convertView.findViewById(R.id.sinleer);
            holder.frameChat = (FrameLayout) convertView.findViewById(R.id.frameChat);




            convertView.setTag(holder);

        }else{
            holder = (MisComprasAdapter.ViewHolder) convertView.getTag();
        }

        final Compra compra = modelc.misCompras.get(i);
        final Publicacion publi = modelc.getPublicacionById(compra.idPublicacion);


        ImageView foto = holder.foto;
        TextView estado = holder.estado;
        TextView nombre = holder.nombre;
        TextView lbReprogramada = holder.reprogramada;

        TextView fechaServicio = holder.fechaServicio;
        LinearLayout layout = holder.layout;
        LinearLayout layerConfirmar = holder.layerConfirmar;
        LinearLayout layerCalificar = holder.layerCalificar;
        LinearLayout layerEstrellas = holder.layerEstrellas;
        TextView btnConfirmar = holder.btnConfirmar;
        TextView btnRechazar = holder.btnRechazar;
        TextView btnCalificar = holder.btnCalificar;
        ImageView btnChat = holder.btnChat;
        final FrameLayout layerSinLeer = holder.layerSinLeer;
        final TextView sinLeer = holder.sinLeer;
        final FrameLayout frameChat = holder.frameChat;


        star1 = holder.star1;
        star2 = holder.star2;
        star3 = holder.star3;
        star4 = holder.star4;
        star5 = holder.star5;


        publi.fotosBaja.get(0).setListener(listener);

        layerCalificar.setVisibility(View.GONE);
        layerConfirmar.setVisibility(View.GONE);
        layerEstrellas.setVisibility(View.GONE);
        fechaServicio.setVisibility(View.GONE);
        frameChat.setVisibility(View.GONE);



        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, PagoExitoso.class);
                i.putExtra("IDCOMPRA",compra.id);
                mContext.startActivity(i);
                ((Activity)mContext).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });



        nombre.setText(publi.nombre);
        fechaServicio.setText(compra.fechaServicio);


        if (!compra.reprogramada) {
            lbReprogramada.setVisibility(View.GONE);

        }
        else {
            lbReprogramada.setVisibility(View.VISIBLE);
        }

        if (publi.servicio){
            fechaServicio.setVisibility(View.VISIBLE);
            frameChat.setVisibility(View.VISIBLE);
        }

        if (compra.estado.equals(EstadosCompra.PENDIENTE.toString())){
            estado.setText("Compra en proceso");
        }

        if (compra.estado.equals(EstadosCompra.CONFIRMADA.toString())){
            estado.setText("Compra en proceso");
        }

        if (compra.estado.equals(EstadosCompra.ENTREGADA.toString())){
            layerConfirmar.setVisibility(View.VISIBLE);

            if (publi.servicio){
                estado.setText("¿Tu servicio ha sido efectivo?");
            }
            else {
                estado.setText("¿Tu producto ha llegado?");
            }
        }


        if (compra.estado.equals(EstadosCompra.CERRADA.toString())) {
            int cali = modelc.getMiCalificacionToCompra(compra.id);
            if (cali == -1) {
                layerCalificar.setVisibility(View.VISIBLE);
                if (publi.servicio) {
                    estado.setText("Servicio recibido");
                } else {
                    estado.setText("Producto recibido");
                }

            } else {
                layerEstrellas.setVisibility(View.VISIBLE);
                pintarEstrellas(cali);
                estado.setText("Compra finalizada");
            }

        }


        Bitmap fotoBitmap = publi.fotosBaja.get(0).getFoto("PUBLICACION");

        if (fotoBitmap != null) {
            foto.setImageBitmap(fotoBitmap);
        }

        btnConfirmar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                confirmarEntrega(compra);

            }
        });

        btnRechazar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                rechazarEntrega(compra);

            }
        });

        btnCalificar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mContext, CalificarCompra.class);
                i.putExtra("IDCOMPRA", compra.id);
                mContext.startActivity(i);
                ((Activity)mContext).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

            }
        });


        btnChat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, ChatCliente.class);
                i.putExtra("IDCOMPRA", compra.id);
                mContext.startActivity(i);
                ((Activity)mContext).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

            }
        });

        //layerSinLeer.setVisibility(View.GONE);

        new ComandoChat(new OnComandoChatListener() {
            @Override
            public void cargoChat(String idCompra) {

                int numero = modelc.sinLeer(idCompra);
                if (numero > 0) {
                    layerSinLeer.setVisibility(View.VISIBLE);
                    sinLeer.setText(numero + "");
                }

                if (numero == 0) {
                    layerSinLeer.setVisibility(View.GONE);
                    sinLeer.setText("");
                }

            }
        }).getChatsByIdCompra(compra.id);


        return convertView;

    }



    private void confirmarEntrega(final Compra compra){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mContext);

        // set title
        alertDialogBuilder.setTitle("Confirmación!");

        // set dialog message
        alertDialogBuilder
                .setMessage("¿Confirmas dar por cerrada la compra?")
                .setCancelable(false)
                .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        compra.estado =  EstadosCompra.CERRADA.toString();
                        new ComandoCompras(null).moverCompraACerradas(compra.id);
                        Intent i = new Intent(mContext, CalificarCompra.class);
                        i.putExtra("IDCOMPRA", compra.id);
                        mContext.startActivity(i);
                        ((Activity)mContext).overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    private void rechazarEntrega(final Compra compra){




        final String[] items = {"El producto/servicio no ha llegado","El producto no concuerda","El producto está en mal estadado","Cancelar"};

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mContext);
        builder.setTitle("Tipo de mascota");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                String problem =  items[item];
                if (problem.equals("Cancelar")){
                    return;
                }
                compra.estado =  EstadosCompra.PENDIENTE.toString();
                new ComandoCompras(null).recharzarCompra(compra.id, "Jamas llegó");

                MisComprasAdapter.this.notifyDataSetChanged();


            }
        });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();

    }


    private void pintarEstrellas(double calificacion){
        pintarApagarPrenderEstrella(star1,1,calificacion);
        pintarApagarPrenderEstrella(star2,2,calificacion);
        pintarApagarPrenderEstrella(star3,3,calificacion);
        pintarApagarPrenderEstrella(star4,4,calificacion);
        pintarApagarPrenderEstrella(star5,5,calificacion);
    }

    private void pintarApagarPrenderEstrella(ImageView star, double pos, double nota){

        if (nota >= pos) {
            star.setImageResource(R.drawable.imgestrellacompleta);
            return;
        }

        if (Math.abs(nota-pos) <= 0.5) {
            star.setImageResource(R.drawable.imgestrellamedia);
            return;
        }

        star.setImageResource(R.drawable.imgestrellavacia);

    }



    private static class ViewHolder {

        public TextView estado;
        public ImageView foto;
        public TextView nombre;
        public TextView reprogramada;

        public TextView fechaServicio;
        public LinearLayout layout;
        public LinearLayout layerConfirmar;
        public LinearLayout layerCalificar;
        public LinearLayout layerEstrellas;

        public TextView btnConfirmar;
        public TextView btnRechazar;

        public TextView btnCalificar;
        public ImageView btnChat;
        public TextView sinLeer;
        public FrameLayout layerSinLeer;
        public FrameLayout frameChat;


        ImageView star1, star2, star3, star4, star5;



    }
}

