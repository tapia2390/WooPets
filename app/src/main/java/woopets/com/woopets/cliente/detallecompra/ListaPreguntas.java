package woopets.com.woopets.cliente.detallecompra;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas.OnComandoPreguntasListener;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.carrito.FavoritosAdapter;

public class ListaPreguntas extends Activity {


    ModelCliente modelc = ModelCliente.getInstance();
    private ListView listaView;
    ListaPreguntasAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_preguntas);


        String idPublicacion = getIntent().getStringExtra("IDPUBLICACION");

        ArrayList<Pregunta> preguntas = modelc.getByIdPublicacionOrdenadas(idPublicacion);



        listaView = (ListView) findViewById(R.id.lista);
        adapter = new ListaPreguntasAdapter(getBaseContext(),preguntas);
        listaView.setAdapter(adapter);
        LinearLayout ver = (LinearLayout) findViewById(R.id.ver);
        TextView nombre = (TextView) findViewById(R.id.nombre);

        TextView titulo = (TextView) findViewById(R.id.titulo);

        final Publicacion publi =  modelc.getPublicacionById(idPublicacion);

        nombre.setText(publi.nombre);


        if (!publi.servicio) {
            titulo.setText("Preguntas sobre el producto");
        }


        new ComandoPreguntas(new OnComandoPreguntasListener() {
            @Override
            public void cargoPregunta(String idPublicacion) {
                adapter.preguntas = modelc.getByIdPublicacionOrdenadas(idPublicacion);
                adapter.notifyDataSetChanged();

            }
        }).getPreguntasByIdPublicacion(idPublicacion);


        ver.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), DetallePublicacion.class);
                i.putExtra("IDPUBLICACION", publi.id);
                startActivity(i);

            }
        });



    }


    public void didTapHacerPregunta(View v){

        String idPublicacion = getIntent().getStringExtra("IDPUBLICACION");
        Intent i = new Intent(getApplicationContext(), HacerPregunta.class);
        i.putExtra("IDPUBLICACION", idPublicacion);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);


    }

    @Override
    public void onResume() {
        super.onResume();
        String idPublicacion = getIntent().getStringExtra("IDPUBLICACION");
        adapter.preguntas = modelc.getByIdPublicacionOrdenadas(idPublicacion);
        adapter.notifyDataSetChanged();



    }


}
