package woopets.com.woopets.cliente;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import woopets.com.woopets.R;

public class ProponerMascota extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proponer_mascota);
    }



    public void didTapOmitir(View v) {


        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Editor editor = pref.edit();

        editor.putBoolean("ya_omitio_mascota",true);
        editor.commit();


        Intent i = new Intent(getApplicationContext(), HomeCliente.class);
        i.putExtra("OMITIR_MASCOTA","SI");
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
        finish();

    }

    public void didTapCrearMascota(View v) {

        Intent i = new Intent(getApplicationContext(), CrearMascota.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
        finish();

    }

}
