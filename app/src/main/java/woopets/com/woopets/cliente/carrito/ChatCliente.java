package woopets.com.woopets.cliente.carrito;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.cliente.Chat;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.EstadosCompra;
import woopets.com.woopets.Modelo.cliente.Pregunta;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ClienteRegistro;
import woopets.com.woopets.cliente.Comandos.ComandoChat;
import woopets.com.woopets.cliente.Comandos.ComandoChat.OnComandoChatListener;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas.OnComandoPreguntasListener;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.cliente.detallecompra.HacerPregunta;
import woopets.com.woopets.cliente.detallecompra.ListaPreguntasAdapter;

public class ChatCliente extends Activity {


    ModelCliente modelc = ModelCliente.getInstance();
    private ListView listaView;
    ChatAdapter adapter;
    FrameLayout vacio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_cliente);

        final String idCompra = getIntent().getStringExtra("IDCOMPRA");

        ArrayList<Chat> chats = modelc.getChatsByIdCompra(idCompra);

        vacio = (FrameLayout) findViewById(R.id.vacio);
        listaView = (ListView) findViewById(R.id.lista);
        adapter = new ChatAdapter(getBaseContext(),chats);
        listaView.setEmptyView(findViewById(R.id.vacio));
        listaView.setAdapter(adapter);

        if (modelc.getChatsByIdCompra(idCompra).size() > 0 ) {
            vacio.setVisibility(View.GONE);
        }

        new ComandoChat(new OnComandoChatListener() {
            @Override
            public void cargoChat(String idOrden) {

                adapter.chats = modelc.getChatsByIdCompra(idCompra);
                if (modelc.getChatsByIdCompra(idCompra).size() > 0 ) {
                    vacio.setVisibility(View.GONE);
                }
                adapter.notifyDataSetChanged();

            }
        }).getChatsByIdCompra(idCompra);


    }


    public void didTapHacerPregunta(View v){



        final String idCompra = getIntent().getStringExtra("IDCOMPRA");
        Compra compra = modelc.getCompraById(idCompra);
        if (compra.estado.equals(EstadosCompra.CERRADA.toString())){
            showAlert("Lo sentimos, tu venta ya ha sido cerrada y ya no es posible enviar mensajes");
            return;
        }
        Intent i = new Intent(getApplicationContext(), HacerChat.class);
        i.putExtra("IDCOMPRA", idCompra);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }

    @Override
    public void onResume() {
        super.onResume();
        String idCompra = getIntent().getStringExtra("IDCOMPRA");
        adapter.chats = modelc.getChatsByIdCompra(idCompra);
        adapter.notifyDataSetChanged();



    }

    public void showAlert(String mensaje){

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                ChatCliente.this);

        // set title
        alertDialogBuilder.setTitle("Alerta");

        // set dialog message
        alertDialogBuilder
                .setMessage(mensaje)
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        return;
                    }
                });

        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

}
