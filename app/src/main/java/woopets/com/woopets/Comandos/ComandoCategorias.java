package woopets.com.woopets.Comandos;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import woopets.com.woopets.Modelo.Categorias;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Modelo.SubCategorias;

/**
 * Created by tacto on 29/08/17.
 */

public class ComandoCategorias {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    public interface OnComandoCategoriasChangeListener {

        void cargoCategorias();
        void cargoTipoUsuario(String tipo);
        void  actualizoSistema();
    }

    //interface del listener de la actividad interesada
    private ComandoCategorias.OnComandoCategoriasChangeListener mListener;

    public ComandoCategorias(ComandoCategorias.OnComandoCategoriasChangeListener mListener) {

        this.mListener = mListener;
    }


    public void getCategorias() {

        DatabaseReference ref = database.getReference("listados/categorias/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelo.listaCategorias.clear();
                for (DataSnapshot listcCategoria : snap.getChildren()) {

                    boolean servicio = (boolean) listcCategoria.child("servicio").getValue();
                    Categorias categorias = new Categorias();
                    categorias.setIdNombre(listcCategoria.getKey());
                    categorias.setImagen(listcCategoria.child("imagen").getValue().toString());
                    categorias.setNombre(listcCategoria.child("nombre").getValue().toString());

                   // int posicion  = (int)(listcCategoria.child("posicion").getValue());
                    categorias.setPosicion(((Long) listcCategoria.child("posicion").getValue()).intValue());

                    categorias.setServicio(servicio);

                    //nuevo arbol
                    DataSnapshot snapSubCategorias;
                    snapSubCategorias = (DataSnapshot) listcCategoria.child("subcategoria/");
                    for (DataSnapshot subcategoria : snapSubCategorias.getChildren()) {

                        SubCategorias subCategorias = new SubCategorias();

                        boolean estado = (boolean) subcategoria.getValue();
                        if (estado) {
                            subCategorias.setIdNombre(subcategoria.getKey());
                            subCategorias.setEstado(estado);
                            categorias.subcategorias.add(subCategorias);
                        }

                    }
                    modelo.listaCategorias.add(categorias);


                }
                mListener.cargoCategorias();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("Error", "No cargo el inicio");
            }
        });

    }



    public void getTipoUsuario(String id) {


        DatabaseReference ref = database.getReference("clientes/"+id);
        final DatabaseReference ref2 = database.getReference("oferentes/"+id);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

               if (snap.getValue() == null ){
                   ref2.addListenerForSingleValueEvent(new ValueEventListener() {
                       @Override
                       public void onDataChange(DataSnapshot snap2) {
                           if (snap2.getValue() == null) {
                               mListener.cargoTipoUsuario("NOEXISTE");
                           }
                           else {
                               mListener.cargoTipoUsuario("OFERENTE");
                           }

                       }

                       @Override
                       public void onCancelled(DatabaseError databaseError) {

                       }
                   });

               }else{
                   mListener.cargoTipoUsuario("CLIENTE");
               }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("Error al buscar cliente", databaseError.getMessage() );

            }
        });


    }



    //asignamos los datos a Sistema
    public void getSytem(){

        DatabaseReference ref = database.getReference("system/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {//addListenerForSingleValueEvent no queda escuchando peticiones
            @Override
            public void onDataChange(DataSnapshot snap) {


                Long androideBuildlong = (Long)snap.child("androideBuild").getValue();
                modelo.sistema.setBuild(androideBuildlong.intValue());

                modelo.sistema.setLink(snap.child("androideLink").getValue().toString());
                boolean double1 = (boolean)snap.child("androideUpdateMandatorio").getValue();
                modelo.sistema.setUpdateMandatorio(double1);

                Long  entero2 = (Long) snap.child("androideVersion").getValue();
                modelo.sistema.setVersion(entero2);


                mListener.actualizoSistema();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoCategoriasChangeListener sDummyCallbacks = new OnComandoCategoriasChangeListener() {
        @Override
        public void cargoCategorias() {
        }

        @Override
        public void cargoTipoUsuario(String tipo) {

        }

        @Override
        public void actualizoSistema() {

        }



    };
}
