package woopets.com.woopets.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import woopets.com.woopets.Modelo.Categorias;
import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.NotificaionesOferente;
import woopets.com.woopets.R;

/**
 * Created by tacto on 31/08/17.
 */

public class CategoriasAdapter extends BaseAdapter {

    private  Modelo modelo = Modelo.getInstance();
    //Atributos del adaptador
    Context mContext;
    AdapterCallback mListener =  sDummyCallbacks;
    private List<Categorias> categoria;


    public CategoriasAdapter(Context mContext, AdapterCallback mListener){

        this.mContext = mContext;
        this.mListener = mListener;

        //ordenadon un array por la posicion del objrto
        Collections.sort(modelo.listaCategorias, new Comparator<Categorias>() {
            @Override
            public int compare(Categorias o1, Categorias o2) {
                return new Long(o1.getPosicion()).compareTo(new Long(o2.getPosicion()));

            }
        });
        this.categoria = modelo.listaCategorias;

    }

    public int getCount() {
        return categoria.size();
    }

    public Object getItem(int position) {
        return categoria.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        View retval = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewitem, null);
        ImageView accesorios_circulo = (ImageView) retval.findViewById(R.id.accesorios_circulo);
        ImageView idcategoria = (ImageView) retval.findViewById(R.id.idcategoria);
        TextView text_categoria = (TextView) retval.findViewById(R.id.text_categoria);

        String nombreimg = modelo.eliminarTildes(categoria.get(position).getImagen().toLowerCase().replaceAll("\\s", ""));
        int resID = mContext.getResources().getIdentifier(nombreimg, "drawable", mContext.getPackageName()); // This will return an integer value stating the id for the image you want.
        accesorios_circulo.setBackgroundResource(resID);
        text_categoria.setText(categoria.get(position).getNombre());



        if (modelo.selectPosition != -1){
            if (modelo.selectPositionAnterior == position){
                if (modelo.selectImge == true) {
                    //your drawable code
                    if(modelo.selectPosition == position ){
                        idcategoria.setVisibility(View.VISIBLE);
                    }
                    //your other stuff : changing color etc

                } else {
                    idcategoria.setVisibility(View.INVISIBLE);
                }
            }

    }


        return retval;
    }




    //create this method in Adapter class
    public void setSelected(int pos, boolean estado) {

            modelo.selectPosition = pos;//change selected item position
            modelo.selectImge = estado;
            notifyDataSetChanged();  //refresh views


    }


    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
 */
    public interface AdapterCallback
    {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback()
    {


    };

}
