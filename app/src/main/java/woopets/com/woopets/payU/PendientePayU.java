package woopets.com.woopets.payU;

/**
 * Created by andres on 8/13/18.
 */

public class PendientePayU {

    public String tipo = "";
    public String id = "";     //reference code id de la compra o de la transaccion o de la operacion siempre que sea unico
    public int valor = 0;
    public String state = "PENDING";     //DECLINED     //APPROVED
    public String orderId = "";
    public String transactionId = "";
    public String tokenDevice = "";
    public String uid = "";

}
