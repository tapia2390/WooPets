package woopets.com.woopets.Oferente.AdapterOferente;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.Oferente.VentasCerradasFecha_Cantidad;
import woopets.com.woopets.R;

/**
 * Created by tacto on 23/08/17.
 */

public class MisVentasAbiertasCerradasInactivasAdapter extends BaseAdapter {

    //Atributos del adaptador
    final Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<CompraProductoAbiertaCerrada> productosVentasAbiertasCerradas;
    private Modelo sing = Modelo.getInstance();
    String imegenPortada = "";


    public MisVentasAbiertasCerradasInactivasAdapter(Context mContext, AdapterCallback mListener) {



        if (sing.comprasProductosAbiertasCerradases2.size() > 0) {
            sing.comprasProductos.clear();

            HashMap<String, CompraProductoAbiertaCerrada> map = new HashMap<String, CompraProductoAbiertaCerrada>();

            for (CompraProductoAbiertaCerrada compraProducto : sing.comprasProductosAbiertasCerradases2) {
                int i = 0;
                map.put(compraProducto.getPedidos().get(i).getIdPublicacion(), compraProducto);
                i++;
            }

            Set<Map.Entry<String, CompraProductoAbiertaCerrada>> set = map.entrySet();
            for (Map.Entry<String, CompraProductoAbiertaCerrada> entry : set) {
                sing.comprasProductos.add(entry.getValue());
            }
        }




        this.mContext = mContext;
        this.mListener = mListener;
        this.productosVentasAbiertasCerradas = sing.comprasProductos;

    }

    @Override
    public int getCount() {
        return productosVentasAbiertasCerradas.size();
    }

    @Override
    public Object getItem(int position) {
        return productosVentasAbiertasCerradas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ImageView list_img;
        final Button list_img_cantidad;
        final ImageView list_img_favorito;
        final TextView list_valor_producto;
        final TextView list_nombre_producto;
        String foto;
        final RelativeLayout contenedor;

        final CompraProductoAbiertaCerrada comprasProductosAbiertasCerradas = (CompraProductoAbiertaCerrada) getItem(position);

        int p = position;
        // Inflate la vista de la productosOferentes
        RelativeLayout itemLayout = (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.adapter_list_publicaciones, parent, false);


        list_img = (ImageView) itemLayout.findViewById(R.id.list_img);
        list_img_cantidad = (Button) itemLayout.findViewById(R.id.list_img_cantidad);
        list_img_favorito = (ImageView) itemLayout.findViewById(R.id.list_img_favorito);
        list_valor_producto = (TextView) itemLayout.findViewById(R.id.list_valor_producto);
        list_nombre_producto = (TextView) itemLayout.findViewById(R.id.list_nombre_producto);
        contenedor = (RelativeLayout) itemLayout.findViewById(R.id.contenedor);


        //cointar repetidos

        Set<CompraProductoAbiertaCerrada> quipu = new HashSet<CompraProductoAbiertaCerrada>(sing.comprasProductosAbiertasCerradases2);
        for (CompraProductoAbiertaCerrada key : quipu) {
            System.out.println(key + " : " + Collections.frequency(sing.comprasProductosAbiertasCerradases2, key));
        }
        //fin repetidos


        //   list_img.setImageDrawable(mContext.getResources().getDrawable(R.drawable.btnfoto));
        int precio =  comprasProductosAbiertasCerradas.getValor() / comprasProductosAbiertasCerradas.getPedidos().get(0).getCantidad();


        list_valor_producto.setText("" + Utility.convertToMoney(precio));


        list_nombre_producto.setText("" + comprasProductosAbiertasCerradas.getPedidos().get(0).getProductosOferentes().get(0).getNombre());

        int i = 0;
        int acumulador = 0;
        int acumulador2 = 0;
        while (i < sing.comprasProductosAbiertasCerradases2.size()) {
            if (sing.comprasProductosAbiertasCerradases2.get(i).getPedidos().get(0).getProductosOferentes().get(0).getNombre().equals(list_nombre_producto.getText().toString())) {
                acumulador = acumulador + sing.comprasProductosAbiertasCerradases2.get(i).getPedidos().get(0).getCantidad();
                acumulador2++;
            }
            i++;
        }


        list_img_cantidad.setText("" + acumulador2);

        foto = comprasProductosAbiertasCerradas.getPedidos().get(0).getProductosOferentes().get(0).getFotos().get(0);


        if (comprasProductosAbiertasCerradas.getPedidos().get(0).getProductosOferentes().get(0).getDestacado() == false && foto.equals("Destacdo")) {
            foto = comprasProductosAbiertasCerradas.getPedidos().get(0).getProductosOferentes().get(0).getFotos().get(1);

        }
        //descargar imagenes firebase

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference().child("productos/" + comprasProductosAbiertasCerradas.getPedidos().get(0).getProductosOferentes().get(0).getId() + "/" + foto);

        //get download file url
        storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Log.i("Main", "File uri: " + uri.toString());

                /*
                *  Picasso.with(mContext)
                        .load(uri.toString())
                        .error(R.drawable.btnfoto)
                        .into(list_img);
                        */
            }
        });


        //download the file
        try {
            final File localFile = File.createTempFile("images", "jpg");
            final String finalFoto = foto;
            storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    list_img.setImageBitmap(bitmap);


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    int resID2 = mContext.getResources().getIdentifier(finalFoto, "drawable", mContext.getPackageName()); // This will return an integer value stating the id for the image you want.

                    if (resID2 == 0) {
                        // Toast.makeText(mContext, "Error al cargar la imagen", Toast.LENGTH_SHORT).show();
                        list_img.setImageDrawable(mContext.getResources().getDrawable(R.drawable.btnfoto));

                    } else {
                        list_img.setBackgroundResource(resID2);
                    }


                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Main", "IOE Exception");
        }


        list_img_cantidad.setBackgroundResource(R.drawable.circulo_rojo_style);

        if (comprasProductosAbiertasCerradas.getPedidos().get(0).getProductosOferentes().get(0).getDestacado() == true) {
            list_img_favorito.setBackgroundResource(R.drawable.destacado_ok);
        }

        contenedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sing.abiertaCerrada = "abiertas";
                sing.posicionServicio = position;
                sing.uidProducto = comprasProductosAbiertasCerradas.getPedidos().get(0).getIdPublicacion();
                sing.idCliente = comprasProductosAbiertasCerradas.getIdCliente();
                //Toast.makeText(mContext,"-"+comprasProductosAbiertasCerradas.getPedidos().get(0).getIdPublicacion()+"-"+comprasProductosAbiertasCerradas.getPedidos().get(position).getId()+"-",Toast.LENGTH_LONG).show();
                Intent i = new Intent(mContext, VentasCerradasFecha_Cantidad.class);
                mContext.startActivity(i);

            }
        });


        return itemLayout;
    }


    public Drawable CargarImagen(String foto) {

        try {
            String nombre = foto;
            nombre = sing.eliminarTildes(nombre.toLowerCase()).replaceAll("\\s", "");
            Resources res = mContext.getResources();


            String mDrawableName = nombre;
            int resID = res.getIdentifier(mDrawableName, "drawable", mContext.getPackageName());

            if (resID != 0) {
                return ContextCompat.getDrawable(mContext, resID);
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public Drawable CargarImagenCirculo() {
        String nombre = "ingreseleccionado";
        Resources res = mContext.getResources();
        int resID = res.getIdentifier(nombre, "drawable", mContext.getPackageName());


        return ContextCompat.getDrawable(mContext, resID);

    }


    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
  */
    public interface AdapterCallback {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback() {


    };

}
