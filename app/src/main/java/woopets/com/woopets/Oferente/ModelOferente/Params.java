package woopets.com.woopets.Oferente.ModelOferente;

/**
 * Created by tacto on 4/12/17.
 */

public class Params {

    int impresionesDestacado;
    String tpendpoint;
    String tpprivada;
    String tppublica;
    int valorDestacado;

    public int getImpresionesDestacado() {
        return impresionesDestacado;
    }

    public void setImpresionesDestacado(int impresionesDestacado) {
        this.impresionesDestacado = impresionesDestacado;
    }

    public String getTpendpoint() {
        return tpendpoint;
    }

    public void setTpendpoint(String tpendpoint) {
        this.tpendpoint = tpendpoint;
    }

    public String getTpprivada() {
        return tpprivada;
    }

    public void setTpprivada(String tpprivada) {
        this.tpprivada = tpprivada;
    }

    public String getTppublica() {
        return tppublica;
    }

    public void setTppublica(String tppublica) {
        this.tppublica = tppublica;
    }

    public int getValorDestacado() {
        return valorDestacado;
    }

    public void setValorDestacado(int valorDestacado) {
        this.valorDestacado = valorDestacado;
    }
}
