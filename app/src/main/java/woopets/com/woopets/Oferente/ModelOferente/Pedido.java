package woopets.com.woopets.Oferente.ModelOferente;

import java.util.ArrayList;

/**
 * Created by tacto on 1/11/17.
 */

public class Pedido {
    String id = "";
    int cantidad = 0;
    String direccion = "";
    String fechaPago = "";
    boolean reprogramada = false;
    String  estado = "";
    String  fechaDelServicio = "";
    String  idPublicacion = "";
    boolean  servicio;
    String motivo = "";
    String justificacion = "";

    ArrayList<ProductosOferente> productosOferentes = new ArrayList<ProductosOferente>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFechaDelServicio() {
        return fechaDelServicio;
    }

    public void setFechaDelServicio(String fechaDelServicio) {
        this.fechaDelServicio = fechaDelServicio;
    }

    public String getIdPublicacion() {
        return idPublicacion;
    }

    public void setIdPublicacion(String idPublicacion) {
        this.idPublicacion = idPublicacion;
    }

    public boolean getServicio() {
        return servicio;
    }

    public void setServicio(boolean servicio) {
        this.servicio = servicio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public boolean isReprogramada() {
        return reprogramada;
    }

    public void setReprogramada(boolean reprogramada) {
        this.reprogramada = reprogramada;
    }

    public boolean isServicio() {
        return servicio;
    }

    public ArrayList<ProductosOferente> getProductosOferentes() {
        return productosOferentes;
    }

    public void setProductosOferentes(ArrayList<ProductosOferente> productosOferentes) {
        this.productosOferentes = productosOferentes;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }
}
