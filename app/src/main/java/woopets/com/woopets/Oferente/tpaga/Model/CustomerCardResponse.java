package woopets.com.woopets.Oferente.tpaga.Model;

/**
 * Created by tacto on 9/08/17.
 */

public class CustomerCardResponse {

    String id;
    String lastFour;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type;

    public String getId() {
        return id;
    }

    public void setId(String token) {
        this.id = id;
    }

    public String getLastFours() {
        return lastFour;
    }

    public void setLastFours(String lastFour) {
        lastFour = lastFour;
    }
}
