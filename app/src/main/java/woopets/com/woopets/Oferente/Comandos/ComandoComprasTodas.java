package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import woopets.com.woopets.Modelo.cliente.Oferente;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.ModelOferente.Cliente;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Direccion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Pedido;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;

/**
 * Created by tacto on 23/08/17.
 */

public class ComandoComprasTodas {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo = Modelo.getInstance();

    public interface OnComandoComprasAbiertasCerradasoChangeListener {

        void comprasTodas();

    }

    //interface del listener de la actividad interesada
    private OnComandoComprasAbiertasCerradasoChangeListener mListener;

    public ComandoComprasTodas(OnComandoComprasAbiertasCerradasoChangeListener mListener) {

        this.mListener = mListener;
    }


    //abiertas
    public void comprasAbiertas(final String abiertaCerrada) {

        modelo.comprasProductosAbiertas.clear();

        DatabaseReference ref;
        Query query;
        ref = database.getReference("compras/abiertas/");//ruta path
        query = ref.orderByChild("idOferente").equalTo(modelo.uid);


        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snProductosAbiertasCerradas, String s) {


                if (!snProductosAbiertasCerradas.exists()) {
                    mListener.comprasTodas();
                } else {
                    CompraProductoAbiertaCerrada comprasProductosAbiertasCerradas = readPublicacion(snProductosAbiertasCerradas);
                    modelo.addCompraAbierta(comprasProductosAbiertasCerradas);
                    mListener.comprasTodas();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                CompraProductoAbiertaCerrada comprasProductosAbiertasCerradas = readPublicacion(dataSnapshot);
                modelo.addCompraAbierta(comprasProductosAbiertasCerradas);
                mListener.comprasTodas();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                modelo.borrarCompra("abiertas", dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        query.addChildEventListener(listener);
        modelo.hijosListener.put(query, listener);


    }

    private CompraProductoAbiertaCerrada readPublicacion(DataSnapshot snProductosAbiertasCerradas) {
        CompraProductoAbiertaCerrada comprasProductosAbiertasCerradas = new CompraProductoAbiertaCerrada(snProductosAbiertasCerradas.getKey());

        ArrayList<Pedido> listPedidos = new ArrayList<Pedido>();

        comprasProductosAbiertasCerradas.setAbiertaCerrada("abiertas");
        comprasProductosAbiertasCerradas.setKey(snProductosAbiertasCerradas.getKey());
        comprasProductosAbiertasCerradas.setFecha(snProductosAbiertasCerradas.child("fecha").getValue().toString());

        comprasProductosAbiertasCerradas.setIdCliente(snProductosAbiertasCerradas.child("idCliente").getValue().toString());

        comprasProductosAbiertasCerradas.setIdOferente(snProductosAbiertasCerradas.child("idOferente").getValue().toString());

        if (snProductosAbiertasCerradas.child("metodoPago").getValue().toString() != null) {
            comprasProductosAbiertasCerradas.setMetodoDePago(snProductosAbiertasCerradas.child("metodoPago").getValue().toString());
        }
        comprasProductosAbiertasCerradas.setTimestamp(((Long) snProductosAbiertasCerradas.child("timestamp").getValue()).intValue());
        comprasProductosAbiertasCerradas.setValor(((Long) snProductosAbiertasCerradas.child("valor").getValue()).intValue());

        String idCliente = snProductosAbiertasCerradas.child("idCliente").getValue().toString();
        //comprasProductosAbiertasCerradas.setCliente(getDatosCliente(idCliente));

        //nuevo arbol
        DataSnapshot snapPedido;
        snapPedido = (DataSnapshot) snProductosAbiertasCerradas.child("pedido");
        for (DataSnapshot dspedido : snapPedido.getChildren()) {
            Pedido pedido = new Pedido();
            pedido.setId(dspedido.getKey());
            pedido.setCantidad(((Long) dspedido.child("cantidad").getValue()).intValue());
            pedido.setEstado(dspedido.child("estado").getValue().toString());

            if (snProductosAbiertasCerradas.child("fechaServicio").getValue() != null) {
                pedido.setFechaDelServicio(dspedido.child("fechaServicio").getValue().toString());
            }

            if (dspedido.child("motivo").getValue() != null) {
                pedido.setMotivo(dspedido.child("motivo").getValue().toString());
            }

            if (dspedido.child("justificacion").getValue() != null) {
                pedido.setJustificacion(dspedido.child("justificacion").getValue().toString());
            }

            pedido.setIdPublicacion(dspedido.child("idPublicacion").getValue().toString());

            if (dspedido.child("direccion").getValue() != null) {
                pedido.setDireccion(dspedido.child("direccion").getValue().toString());
            }
            pedido.setProductosOferentes(getDatosProductosOferente(dspedido.child("idPublicacion").getValue().toString()));

            boolean servicio = (boolean) dspedido.child("servicio").getValue();
            pedido.setServicio(servicio);


            if (dspedido.child("fechaServicio").getValue() != null) {
                pedido.setFechaDelServicio(dspedido.child("fechaServicio").getValue().toString());
            }


            if (dspedido.child("estado").getValue() != null) {
                pedido.setEstado(dspedido.child("estado").getValue().toString());
            }

            if (dspedido.child("fechaPago").getValue() != null) {
                pedido.setFechaPago(dspedido.child("fechaPago").getValue().toString());
            }

            if (dspedido.child("reprogramada").getValue() != null) {
                boolean reprogramada = (boolean) dspedido.child("reprogramada").getValue();
                pedido.setReprogramada(reprogramada);

            }

            listPedidos.add(pedido);

        }
        comprasProductosAbiertasCerradas.setPedidos(listPedidos);

        getClenteParaCompra(comprasProductosAbiertasCerradas);
        return comprasProductosAbiertasCerradas;
    }


    // cerradas

    public void comprasCerradas(final String abiertaCerrada) {

        modelo.comprasProductosCerradas.clear();

        DatabaseReference ref;
        Query query;
        ref = database.getReference("compras/cerradas/");//ruta path
        query = ref.orderByChild("idOferente").equalTo(modelo.uid);


        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snProductosAbiertasCerradas, String s) {


                if (!snProductosAbiertasCerradas.exists()) {
                    mListener.comprasTodas();
                } else {
                    CompraProductoAbiertaCerrada comprasProductosAbiertasCerradas = readPublicacionCerrada(snProductosAbiertasCerradas);
                    modelo.addCompraCerrada(comprasProductosAbiertasCerradas);
                    mListener.comprasTodas();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                CompraProductoAbiertaCerrada comprasProductosAbiertasCerradas = readPublicacionCerrada(dataSnapshot);
                modelo.addCompraCerrada(comprasProductosAbiertasCerradas);
                mListener.comprasTodas();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                modelo.borrarCompra("cerradas", dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        query.addChildEventListener(listener);
        modelo.hijosListener.put(query, listener);


    }


    private CompraProductoAbiertaCerrada readPublicacionCerrada(DataSnapshot snProductosAbiertasCerradas) {

        CompraProductoAbiertaCerrada comprasProductosAbiertasCerradas = new CompraProductoAbiertaCerrada(snProductosAbiertasCerradas.getKey());
        ArrayList<Pedido> listPedidos = new ArrayList<Pedido>();

        comprasProductosAbiertasCerradas.setAbiertaCerrada("cerradas");
        comprasProductosAbiertasCerradas.setKey(snProductosAbiertasCerradas.getKey());
        comprasProductosAbiertasCerradas.setFecha(snProductosAbiertasCerradas.child("fecha").getValue().toString());

        comprasProductosAbiertasCerradas.setIdCliente(snProductosAbiertasCerradas.child("idCliente").getValue().toString());

        comprasProductosAbiertasCerradas.setIdOferente(snProductosAbiertasCerradas.child("idOferente").getValue().toString());

        if (snProductosAbiertasCerradas.child("metodoPago").getValue().toString() != null) {
            comprasProductosAbiertasCerradas.setMetodoDePago(snProductosAbiertasCerradas.child("metodoPago").getValue().toString());
        }
        comprasProductosAbiertasCerradas.setTimestamp(((Long) snProductosAbiertasCerradas.child("timestamp").getValue()).intValue());
        comprasProductosAbiertasCerradas.setValor(((Long) snProductosAbiertasCerradas.child("valor").getValue()).intValue());

        String idCliente = snProductosAbiertasCerradas.child("idCliente").getValue().toString();
        //comprasProductosAbiertasCerradas.setCliente(getDatosCliente(idCliente));

        //nuevo arbol
        DataSnapshot snapPedido;
        snapPedido = (DataSnapshot) snProductosAbiertasCerradas.child("pedido");
        for (DataSnapshot dspedido : snapPedido.getChildren()) {
            Pedido pedido = new Pedido();
            pedido.setId(dspedido.getKey());
            pedido.setCantidad(((Long) dspedido.child("cantidad").getValue()).intValue());
            pedido.setEstado(dspedido.child("estado").getValue().toString());

            if (snProductosAbiertasCerradas.child("fechaServicio").getValue() != null) {
                pedido.setFechaDelServicio(dspedido.child("fechaServicio").getValue().toString());
            }

            if (dspedido.child("motivo").getValue() != null) {
                pedido.setMotivo(dspedido.child("motivo").getValue().toString());
            }

            if (dspedido.child("justificacion").getValue() != null) {
                pedido.setJustificacion(dspedido.child("justificacion").getValue().toString());
            }

            pedido.setIdPublicacion(dspedido.child("idPublicacion").getValue().toString());

            if (dspedido.child("direccion").getValue() != null) {
                pedido.setDireccion(dspedido.child("direccion").getValue().toString());
            }
            pedido.setProductosOferentes(getDatosProductosOferente(dspedido.child("idPublicacion").getValue().toString()));

            boolean servicio = (boolean) dspedido.child("servicio").getValue();
            pedido.setServicio(servicio);


            if (dspedido.child("fechaServicio").getValue() != null) {
                pedido.setFechaDelServicio(dspedido.child("fechaServicio").getValue().toString());
            }


            if (dspedido.child("estado").getValue() != null) {
                pedido.setEstado(dspedido.child("estado").getValue().toString());
            }

            if (dspedido.child("fechaPago").getValue() != null) {
                pedido.setFechaPago(dspedido.child("fechaPago").getValue().toString());
            }

            if (dspedido.child("reprogramada").getValue() != null) {
                boolean reprogramada = (boolean) dspedido.child("reprogramada").getValue();
                pedido.setReprogramada(reprogramada);

            }

            listPedidos.add(pedido);

        }
        comprasProductosAbiertasCerradas.setPedidos(listPedidos);

        getClenteParaCompra(comprasProductosAbiertasCerradas);

        return comprasProductosAbiertasCerradas;
    }


    public void getClenteParaCompra(final CompraProductoAbiertaCerrada compra) {


        DatabaseReference ref = database.getReference("clientes" + "/" + compra.getIdCliente());
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                compra.setCliente(readCliente(snap));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public ArrayList<Cliente> readCliente(DataSnapshot snap) {

        final Cliente cliente = new Cliente();
        final ArrayList<Cliente> ltCliente = new ArrayList<Cliente>();
        final ArrayList<Direccion> listDirecion = new ArrayList<Direccion>();

        cliente.setId(snap.getKey());
        cliente.setNombre(snap.child("nombre").getValue().toString());

        cliente.setApellido(snap.child("apellido").getValue().toString());

        cliente.setCelular(snap.child("celular").getValue().toString());

        //nuevo arbol
        DataSnapshot snapDirecciones;
        snapDirecciones = (DataSnapshot) snap.child("direcciones");
        for (DataSnapshot dsdir : snapDirecciones.getChildren()) {

            Direccion direccion = new Direccion();
            direccion.setId(dsdir.getKey());
            direccion.setDireccion(dsdir.child("direccion").getValue().toString());
            direccion.setNombre(dsdir.child("nombre").getValue().toString());
            listDirecion.add(direccion);
        }

        cliente.setListDirecion(listDirecion);
        cliente.setCorreo(snap.child("correo").getValue().toString());
        cliente.setTokens(snap.child("tokenDevice").getValue().toString());
        ltCliente.add(cliente);

        return ltCliente;
    }


    public ArrayList<Cliente> getDatosCliente(String idCliente) {

        final Cliente cliente = new Cliente();
        final ArrayList<Cliente> ltCliente = new ArrayList<Cliente>();
        final ArrayList<Direccion> listDirecion = new ArrayList<Direccion>();

        DatabaseReference ref = database.getReference("clientes/" + idCliente + "/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                cliente.setId(snap.getKey());
                cliente.setNombre(snap.child("nombre").getValue().toString());

                cliente.setApellido(snap.child("apellido").getValue().toString());

                cliente.setCelular(snap.child("celular").getValue().toString());

                //nuevo arbol
                DataSnapshot snapDirecciones;
                snapDirecciones = (DataSnapshot) snap.child("direcciones");
                for (DataSnapshot dsdir : snapDirecciones.getChildren()) {

                    Direccion direccion = new Direccion();
                    direccion.setId(dsdir.getKey());
                    direccion.setDireccion(dsdir.child("direccion").getValue().toString());
                    direccion.setNombre(dsdir.child("nombre").getValue().toString());
                    listDirecion.add(direccion);
                }

                cliente.setListDirecion(listDirecion);

                cliente.setCorreo(snap.child("correo").getValue().toString());

                cliente.setTokens(snap.child("tokenDevice").getValue().toString());


                ltCliente.add(cliente);
                mListener.comprasTodas();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return ltCliente;
    }


    public ArrayList<ProductosOferente> getDatosProductosOferente(String idProductosOferente) {

        final ProductosOferente productosOferente = new ProductosOferente();
        final ArrayList<ProductosOferente> ltProductosOferente = new ArrayList<ProductosOferente>();

        DatabaseReference ref = database.getReference("productos/" + idProductosOferente + "/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {


                productosOferente.setId(snap.getKey());


                boolean activo = (boolean) snap.child("activo").getValue();

                productosOferente.setActivo(activo);
                productosOferente.setCategoria(snap.child("categoria").getValue().toString());


                productosOferente.setDescripcion(snap.child("descripcion").getValue().toString());

                if (snap.child("destacado").getValue() != null) {
                    boolean destacado = (boolean) snap.child("destacado").getValue();
                    productosOferente.setDestacado(destacado);
                }

                if (snap.child("fechaCreacion").getValue() != null) {
                    productosOferente.setFechaCreacion(snap.child("fechaCreacion").getValue().toString());
                }

                boolean servicio = (boolean) snap.child("servicio").getValue();

                if (servicio == true) {
                    //nuevo arbol
                    DataSnapshot snaphorario;
                    snaphorario = (DataSnapshot) snap.child("horario");

                    //nuevo subarbol
                    DataSnapshot snapFinDeSemana;
                    snapFinDeSemana = (DataSnapshot) snaphorario.child("FinDeSemana");

                    if (snapFinDeSemana.child("dias").getValue() != null) {

                        productosOferente.setHorarioDiasFinDeSemana(snapFinDeSemana.child("dias").getValue().toString());
                        productosOferente.setHorarioCierreDiasFinDeSemana(snapFinDeSemana.child("horaCierre").getValue().toString());
                        productosOferente.setHorarioInicioDiasFinDeSemana(snapFinDeSemana.child("horaInicio").getValue().toString());

                    }


                    if (snapFinDeSemana.child("sinJornadaContinua").getValue() != null) {
                        boolean sinJornadaContinuaFinSemana = (boolean) snapFinDeSemana.child("sinJornadaContinua").getValue();
                        productosOferente.setSinJornadaContinuaFinDeSemana(sinJornadaContinuaFinSemana);

                    }


                    //fin subarbol
                    //nuevo subarbol
                    DataSnapshot snapSemana;
                    snapSemana = (DataSnapshot) snaphorario.child("Semana");
                    if (snapSemana.child("dias").getValue() != null) {
                        productosOferente.setHorarioDiasDeLaSemana(snapSemana.child("dias").getValue().toString());
                        productosOferente.setHorarioCierreDiasDeLaSemana(snapSemana.child("horaCierre").getValue().toString());
                        productosOferente.setHorarioInicioDiasDeLaSemana(snapSemana.child("horaInicio").getValue().toString());

                    }

                    if (snapSemana.child("sinJornadaContinua").getValue() != null) {
                        boolean sinJornadaContinuaSemana = (boolean) snapSemana.child("sinJornadaContinua").getValue();
                        productosOferente.setSinJornadaContinuaSemana(sinJornadaContinuaSemana);
                    }

                    //fin subarbol
                    //fin arbol

                    productosOferente.setDuracion(((Long) snap.child("duracion").getValue()).intValue());
                    productosOferente.setDuracionMedida(snap.child("duracionMedida").getValue().toString());

                    boolean servicioDomicilio = (boolean) snap.child("servicioEnDomicilio").getValue();
                    productosOferente.setServicioEnDomicilio(servicioDomicilio);


                }

                if (servicio == false) {
                    if (snap.child("subcategoria").getValue() != null) {
                        productosOferente.setSubcategoria(snap.child("subcategoria").getValue().toString());

                    }
                    if (snap.hasChild("stock")) {
                        productosOferente.setCantidad(((Long) snap.child("stock").getValue()).intValue());

                    }

                }
                productosOferente.setServicio(servicio);

                //nuevo arbol
                DataSnapshot snapFotos;
                snapFotos = (DataSnapshot) snap.child("fotos/");
                ArrayList<String> fotosProductosOferentes = new ArrayList<String>();
                for (DataSnapshot foto : snapFotos.getChildren()) {
                    fotosProductosOferentes.add(foto.getValue().toString());
                }

                productosOferente.setFotos(fotosProductosOferentes);

                productosOferente.setIdOferente(snap.child("idOferente").getValue().toString());
                productosOferente.setTitulo(snap.child("nombre").getValue().toString());
                productosOferente.setPrecio(snap.child("precio").getValue().toString());
                productosOferente.setTarget(snap.child("target").getValue().toString());
                if (snap.child("timestamp").getValue() != null) {
                    productosOferente.setTimestamp(((Long) snap.child("timestamp").getValue()).intValue());
                }

                ltProductosOferente.add(productosOferente);
                mListener.comprasTodas();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return ltProductosOferente;
    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoComprasAbiertasCerradasoChangeListener sDummyCallbacks = new OnComandoComprasAbiertasCerradasoChangeListener() {


        @Override
        public void comprasTodas() {
        }


    };
}
