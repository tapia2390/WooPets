package woopets.com.woopets.Oferente.Fragment;

/**
 * Created by tacto on 3/07/17.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.MyPublicacionesCategorias;
import woopets.com.woopets.Oferente.PreguntasFrecuentes;
import woopets.com.woopets.Oferente.TapsHome;
import woopets.com.woopets.R;

//import woopets.com.woopets.R;

public class MyPublicaciones extends Fragment {


    Modelo modelo = Modelo.getInstance();
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //View view = inflater.inflate(R.layout.mypublicaciones,container,false);

        //View view = inflater.inflate(R.layout.mypublicaciones,container,false);


        if(modelo.vistaPublicacion == 1){
            Intent i = new Intent(getActivity(), MyPublicacionesCategorias.class);
            startActivity(i);
            getActivity().finish();
        }if(modelo.vistaPublicacion == 2){
            Intent i = new Intent(getActivity(), TapsHome.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            getActivity().finish();

        }

        return null;

    }

}
