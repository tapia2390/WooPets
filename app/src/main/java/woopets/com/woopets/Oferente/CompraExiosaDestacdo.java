package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class CompraExiosaDestacdo extends Activity {

    TextView valor_destacado;
    TextView txt_impresiones;
    Modelo modelo = Modelo.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_compra_exiosa_destacdo);

        valor_destacado = (TextView)findViewById(R.id.valor_destacado);
        txt_impresiones = (TextView)findViewById(R.id.txt_impresiones);

        valor_destacado.setText(""+Utility.convertToMoney(modelo.params.getValorDestacado()));
        txt_impresiones.setText("Por "+ modelo.params.getImpresionesDestacado() +" impresiones (Visualizaciones totales de los usuarios) al mes.");


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }
    }




    @Override
    public void onBackPressed() {
        btn_ok();
        super.onBackPressed();
    }

    public void btn_ok(){
        Intent intent = new Intent(getApplicationContext(), InformacionProductoServicioOferente.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void finalizar_ok(View v){
        btn_ok();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

}
