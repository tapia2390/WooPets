package woopets.com.woopets.Oferente.Comandos;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 12/09/17.
 */

public class ComandoGeoFire {

    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnGeoFireChangeListener {

        void cargoGeoFire();
        void errorGeoFire();

    }


    //interface del listener de la actividad interesada
    private ComandoGeoFire.OnGeoFireChangeListener mListener;

    public ComandoGeoFire(ComandoGeoFire.OnGeoFireChangeListener mListener){

        this.mListener = mListener;

    }


    public void setGeoFire(){

        DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference("oferentes/"+modelo.uid+"/ubicacion/");


        if(ref1 == null){
            if(!modelo.uid.equals("")){
                Map<String, Object> datos_gps = new HashMap<String, Object>();
                datos_gps.put("lat",modelo.latitud);
                datos_gps.put("lon",  modelo.longitud);
                ref1.updateChildren(datos_gps);
            }

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("ubicacionOferentes/");
            GeoFire geoFire = new GeoFire(ref);
            geoFire.setLocation(modelo.uid, new GeoLocation(modelo.latitud, modelo.longitud), new GeoFire.CompletionListener() {
                @Override
                public void onComplete(String key, DatabaseError error) {
                    if (error != null) {
                        System.err.println("Se ha producido un error al guardar la ubicación: " + error);
                        mListener.errorGeoFire();
                    } else {
                        System.out.println("Ubicación guardada en el servidor correctamente!");
                        mListener.cargoGeoFire();
                    }
                }
            });

        }




    }



    /**
     * Para evitar nullpointerExeptions
     */
    private static ComandoGeoFire.OnGeoFireChangeListener sDummyCallbacks = new ComandoGeoFire.OnGeoFireChangeListener()
    {
        @Override
        public void cargoGeoFire()
        {}

        @Override
        public void errorGeoFire()
        {}

    };
}
