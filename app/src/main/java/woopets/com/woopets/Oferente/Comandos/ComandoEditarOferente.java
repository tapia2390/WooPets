package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Oferente.ModelOferente.DatosTpaga;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Tarjetas;

/**
 * Created by tacto on 29/06/17.
 */

public class ComandoEditarOferente {

    //traer ui
    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    public interface OnComandoRegistroOferenteChangeListener {

        void informacionRegistroOferente();

        void errorInformacionRegistroOferente();

        void cargoDatosOferente();

    }

    //interface del listener de la actividad interesada
    private OnComandoRegistroOferenteChangeListener mListener;

    public ComandoEditarOferente(OnComandoRegistroOferenteChangeListener mListener) {

        this.mListener = mListener;
    }


    public void editarOferente(final String celularEstablecimiento, final String celularOferente, final String documentoOferente,
                               final String nombreOferente, final String tipoDocumentoOferente, final String correoEstablecimineto,
                               final String direcionEstablecimineto, final String finDeSemanadias,
                               final String finDESemanaHoraCierre, final String finDesemanHorarioInicio, final String semanadias, final String semanaHoraCierre,
                               final String semanaHorarioInicio, final String nitEstablecimiento, final String paginawebEstablecimiento, final String razonSocialEstablecimiento,
                                final double latitud, final double longitud,
                               boolean sinJornadaContinuaSemana, boolean booleansinJornadaContinuaFinSemana, final String telefono, final String telefono2) {


        DatabaseReference key = referencia.push();

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("h:mm a");
        System.out.println("Hora: " + hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: " + dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: " + hourdateFormat.format(date));

        int setHora = 0;
        String string = "" + hourFormat.format(date);
        String[] parts = string.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];

        if (part2.equals("a.") || part2.equals("a.m.") || part2.equals("a. m.") || part2.equals("A.")) {
            part2 = "AM";
        }
        if (part2.equals("p.") || part2.equals("p.m.") || part2.equals("p. m.") || part2.equals("P.")) {
            part2 = "PM";
        }


        DatabaseReference ref = database.getReference("oferentes/" + modelo.uid + "/");//ruta path

        Map<String, Object> enviarRegistro = new HashMap<String, Object>();
        enviarRegistro.put("celular", celularEstablecimiento);

        Map<String, Object> contactoPrincipal = new HashMap<String, Object>();
        DatabaseReference key2 = referencia.push();
        contactoPrincipal.put("celular", celularOferente);
        contactoPrincipal.put("correo", modelo.datosOferente.getCorreoElectronicoContacto());
        contactoPrincipal.put("telefono", telefono);
        contactoPrincipal.put("documento", documentoOferente);
        contactoPrincipal.put("nombre", nombreOferente);
        contactoPrincipal.put("tipoDocumento", tipoDocumentoOferente);
        enviarRegistro.put("contactoPrincipal", contactoPrincipal);

        enviarRegistro.put("correo", correoEstablecimineto);
        enviarRegistro.put("direccion", direcionEstablecimineto);
        enviarRegistro.put("ciudad", modelo.datosOferente.ciudad);
        enviarRegistro.put("departamento", modelo.datosOferente.departamento);

        enviarRegistro.put("fechaUltimoLogeo", dateFormat.format(date));
        enviarRegistro.put("horaUltimoLogeo", part1 + " " + part2);
        enviarRegistro.put("telefono", telefono2);

        Map<String, Object> horario = new HashMap<String, Object>();
        if (!finDeSemanadias.equals("")) {

            Map<String, Object> findesemana = new HashMap<String, Object>();
            findesemana.put("dias", finDeSemanadias);
            findesemana.put("horaCierre", finDESemanaHoraCierre);
            findesemana.put("horaInicio", finDesemanHorarioInicio);
            findesemana.put("sinJornadaContinua", booleansinJornadaContinuaFinSemana);

            horario.put("FinDeSemana", findesemana);

            enviarRegistro.put("horario", horario);

        }

        if (!semanadias.equals("")) {
            Map<String, Object> semana = new HashMap<String, Object>();
            semana.put("dias", semanadias);
            semana.put("horaCierre", semanaHoraCierre);
            semana.put("horaInicio", semanaHorarioInicio);
            semana.put("sinJornadaContinua", sinJornadaContinuaSemana);

            horario.put("Semana", semana);

            enviarRegistro.put("horario", horario);


        }

        enviarRegistro.put("nit", nitEstablecimiento);
        if (!paginawebEstablecimiento.equals("")) {
            enviarRegistro.put("paginaWeb", paginawebEstablecimiento);
        }

        enviarRegistro.put("razonSocial", razonSocialEstablecimiento);
        enviarRegistro.put("systemDevice", "ANDROID");


        Map<String, Object> ubicacion = new HashMap<String, Object>();
        ubicacion.put("lat", latitud);
        ubicacion.put("lon", longitud);
        enviarRegistro.put("ubicacion", ubicacion);


        ref.updateChildren(enviarRegistro, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    mListener.informacionRegistroOferente();
            }
        });

    }


    public void getDatosOferente() {
        DatabaseReference ref = database.getReference("oferentes/" + modelo.uid);//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                if (snap.exists()) {


                    if (snap.child("aprobacionMyPet").getValue() != null) {
                        modelo.datosOferente.setAprobacionMyPet(snap.child("aprobacionMyPet").getValue().toString());
                    }

                    if (snap.hasChild("celular")) {
                        modelo.datosOferente.setCelular(snap.child("celular").getValue().toString());
                    }

                    //nuevo arbol
                    DataSnapshot snapcontactoPrincipal;
                    snapcontactoPrincipal = (DataSnapshot) snap.child("contactoPrincipal");

                    modelo.datosOferente.setCelularContacto(snapcontactoPrincipal.child("celular").getValue().toString());
                    modelo.datosOferente.setCorreoElectronicoContacto(snapcontactoPrincipal.child("correo").getValue().toString());
                    modelo.datosOferente.setNumeroDeDocumentoCntacto(snapcontactoPrincipal.child("documento").getValue().toString());
                    modelo.datosOferente.setNombreContacto(snapcontactoPrincipal.child("nombre").getValue().toString());
                    modelo.datosOferente.setTelefonoFijoContacto(snapcontactoPrincipal.child("telefono").getValue().toString());
                    modelo.datosOferente.setTipodeDocumentoContacto(snapcontactoPrincipal.child("tipoDocumento").getValue().toString());
                    //fin

                    DataSnapshot snapdatosTpaga;
                    snapdatosTpaga = (DataSnapshot) snap.child("datosTpaga");
                    if (snapdatosTpaga.exists()) {
                        DatosTpaga datosTpaga = new DatosTpaga();
                        datosTpaga.setApellidos(snapdatosTpaga.child("apellidos").getValue().toString());
                        datosTpaga.setCorreo(snapdatosTpaga.child("correo").getValue().toString());
                        datosTpaga.setIdClienteTpaga(snapdatosTpaga.child("idClienteTpaga").getValue().toString());
                        modelo.idClienteTpaga = snapdatosTpaga.child("idClienteTpaga").getValue().toString();
                        datosTpaga.setNombres(snapdatosTpaga.child("nombres").getValue().toString());
                        datosTpaga.setTelefono(snapdatosTpaga.child("telefono").getValue().toString());
                        modelo.datosOferente.setDatosTpaga(datosTpaga);
                        //fin

                    }


                    if (snap.child("tarjetas").getValue() != null) {
                        DataSnapshot snapTar = snap.child("tarjetas");
                        for (DataSnapshot tar : snapTar.getChildren()) {
                            MiniTarjeta mini = tar.getValue(MiniTarjeta.class);
                            mini.id = tar.getKey();
                            modelo.tarjetas.add(mini);
                        }
                    }



                    if (snap.hasChild("horario")) {

                        //nuevo arbol
                        DataSnapshot snaphorario;
                        snaphorario = (DataSnapshot) snap.child("horario");

                        //nuevo subarbol
                        DataSnapshot snapFinDeSemana;
                        snapFinDeSemana = (DataSnapshot) snaphorario.child("FinDeSemana");


                        if (snapFinDeSemana.child("dias").getValue() != null) {
                            modelo.datosOferente.setHorarioDiasFinDeSemana(snapFinDeSemana.child("dias").getValue().toString());
                            modelo.datosOferente.setHorarioCierreDiasFinDeSemana(snapFinDeSemana.child("horaCierre").getValue().toString());
                            modelo.datosOferente.setHorarioInicioDiasFinDeSemana(snapFinDeSemana.child("horaInicio").getValue().toString());


                            modelo.horariofindesemanaIni = snapFinDeSemana.child("horaInicio").getValue().toString();
                            modelo.horariofindesemanaFin = snapFinDeSemana.child("horaInicio").getValue().toString();

                            if (snapFinDeSemana.child("sinJornadaContinua").getValue() != null) {
                                boolean sinJornadaContinuaFinSemana = (boolean) snapFinDeSemana.child("sinJornadaContinua").getValue();
                                modelo.datosOferente.setSinJornadaContinuaFinDeSemana(sinJornadaContinuaFinSemana);
                                modelo.sinJornadaContinuaFinDeSemana = sinJornadaContinuaFinSemana;

                            }
                        }


                        //fin subarbol
                        //nuevo subarbol
                        DataSnapshot snapSemana;
                        snapSemana = (DataSnapshot) snaphorario.child("Semana");


                        if (snapSemana.child("dias").getValue() != null) {
                            modelo.datosOferente.setHorarioDiasDeLaSemana(snapSemana.child("dias").getValue().toString());
                            modelo.datosOferente.setHorarioCierreDiasDeLaSemana(snapSemana.child("horaCierre").getValue().toString());
                            modelo.datosOferente.setHorarioInicioDiasDeLaSemana(snapSemana.child("horaInicio").getValue().toString());

                            modelo.horariodesemanaIni = snapSemana.child("horaInicio").getValue().toString();
                            modelo.horariodesemanaFin = snapSemana.child("horaCierre").getValue().toString();


                            if (snapSemana.child("sinJornadaContinua").getValue() != null) {
                                boolean sinJornadaContinuaSemana = (boolean) snapSemana.child("sinJornadaContinua").getValue();
                                modelo.datosOferente.setSinJornadaContinuaSemana(sinJornadaContinuaSemana);
                                modelo.sinJornadaContinuaSemana = sinJornadaContinuaSemana;

                            }
                        }
                        //fin subarbol
                        //fin arbol
                    }

                    //nuevo arbol
                    DataSnapshot snapubicacion;
                    snapubicacion = (DataSnapshot) snap.child("ubicacion");

                    try {
                        double lat = (double) snapubicacion.child("lat").getValue();
                        modelo.datosOferente.setLatitud(lat);

                    } catch (Exception e) {
                        modelo.datosOferente.setLatitud(0.0);
                    }


                    try {
                        double lon = (double) snapubicacion.child("lon").getValue();
                        modelo.datosOferente.setLongitud(lon);
                    } catch (Exception e) {

                        modelo.datosOferente.setLongitud(0.0);
                    }


                    //fin

                    modelo.datosOferente.setCorreoElectronico(snap.child("correo").getValue().toString());
                    modelo.datosOferente.setDireccion(snap.child("direccion").getValue().toString());

                    if (snap.hasChild("ciudad")){
                        modelo.datosOferente.ciudad = snap.child("ciudad").getValue().toString();
                        modelo.datosOferente.departamento = snap.child("departamento").getValue().toString();
                    } else {
                        modelo.datosOferente.ciudad = "Bogotá";
                        modelo.datosOferente.departamento = "Cundinamarca";
                    }


                    modelo.datosOferente.setNit(snap.child("nit").getValue().toString());
                    if (snap.child("paginaWeb").getValue() != null) {
                        modelo.datosOferente.setPaginaWe(snap.child("paginaWeb").getValue().toString());
                    } else {
                        modelo.datosOferente.setPaginaWe("");

                    }
                    modelo.datosOferente.setRazonSocial(snap.child("razonSocial").getValue().toString());
                    modelo.datosOferente.setTelefonoFijo(snap.child("telefono").getValue().toString());

                }
                mListener.cargoDatosOferente();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoRegistroOferenteChangeListener sDummyCallbacks = new OnComandoRegistroOferenteChangeListener() {
        @Override
        public void informacionRegistroOferente() {
        }

        @Override
        public void errorInformacionRegistroOferente() {
        }

        @Override
        public void cargoDatosOferente() {
        }

    };
}
