package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import woopets.com.woopets.BuildConfig;
import woopets.com.woopets.Oferente.Comandos.ComandoEditarEliminarEstado;
import woopets.com.woopets.Oferente.Comandos.ComandoRegistroProductoOferente;
import woopets.com.woopets.Oferente.ModelOferente.ImageUtils;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;
import woopets.com.woopets.Oferente.ModelOferente.Utility;


import woopets.com.woopets.Oferente.ModelOferente.uploadFirebase.RootActivity;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.views.SquareImageView;

import static woopets.com.woopets.R.id.info;
import static woopets.com.woopets.R.id.perro;

public class MyPublicacionesFoto extends Activity implements ComandoRegistroProductoOferente.OnComandoRegistroProductoOferenteChangeListener, ComandoEditarEliminarEstado.OnComandoEditarEliminarEstadoChangeListener {

    ComandoEditarEliminarEstado comandoEditarEliminarEstado;

    SquareImageView camara1, camara2, camara3, camara4, camara5, camara6;
    Button txt_camara1, txt_camara2, txt_camara3, txt_camara4, txt_camara5, txt_camara6;
    RelativeLayout relativeLayout_camara2, relativeLayout_camara3, relativeLayout_camara4, relativeLayout_camara5, relativeLayout_camara6;

    ComandoRegistroProductoOferente comandoRegistroProductoOferente;
    ArrayList<String> foto = new ArrayList<String>();
    private ProgressDialog progressDialog;
    final Context context = this;
    String keyFirebase = "";
    Modelo modelo = Modelo.getInstance();

    boolean img_cam1 = false;
    boolean img_cam2 = false;
    boolean img_cam3 = false;
    boolean img_cam4 = false;
    boolean img_cam5 = false;
    boolean img_cam6 = false;

    String userChoosenTask = "";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    static final int REQUEST_TAKE_PHOTO = 3;

    Uri photoURI = null;
    String ruta1 = null;

    Uri uriSavedImage1 = null;
    Uri uriSavedImage1Baja = null;
    Uri uriSavedImage1Alta = null;

    Uri uriSavedImage2 = null;
    Uri uriSavedImage2Baja = null;
    Uri uriSavedImage2Alta = null;

    Uri uriSavedImage3 = null;
    Uri uriSavedImage3Baja = null;
    Uri uriSavedImage3Alta = null;

    Uri uriSavedImage4 = null;
    Uri uriSavedImage4Baja = null;
    Uri uriSavedImage4Alta = null;

    Uri uriSavedImage5 = null;
    Uri uriSavedImage5Baja = null;
    Uri uriSavedImage5Alta = null;

    Uri uriSavedImage6 = null;
    Uri uriSavedImage6Baja = null;
    Uri uriSavedImage6Alta = null;


    int camaraSelecionada = 1;

    //variables par subir imagen a firebase
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference referencia = database.getReference();


    String mCurrentPhotoPath = "";

    int btnCamara1 = 0;
    int btnCamara2 = 0;
    int btnCamara3 = 0;
    int btnCamara4 = 0;
    int btnCamara5 = 0;
    int btnCamara6 = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_my_publicaciones_foto);

        camara1 = (SquareImageView) findViewById(R.id.camara1);
        camara2 = (SquareImageView) findViewById(R.id.camara2);
        camara3 = (SquareImageView) findViewById(R.id.camara3);
        camara4 = (SquareImageView) findViewById(R.id.camara4);
        camara5 = (SquareImageView) findViewById(R.id.camara5);
        camara6 = (SquareImageView) findViewById(R.id.camara6);

        txt_camara1 = (Button) findViewById(R.id.txt_camara1);
        txt_camara2 = (Button) findViewById(R.id.txt_camara2);
        txt_camara3 = (Button) findViewById(R.id.txt_camara3);
        txt_camara4 = (Button) findViewById(R.id.txt_camara4);
        txt_camara5 = (Button) findViewById(R.id.txt_camara5);
        txt_camara6 = (Button) findViewById(R.id.txt_camara6);

        relativeLayout_camara2 = (RelativeLayout) findViewById(R.id.relativeLayout_camara2);
        relativeLayout_camara3 = (RelativeLayout) findViewById(R.id.relativeLayout_camara3);
        relativeLayout_camara4 = (RelativeLayout) findViewById(R.id.relativeLayout_camara4);
        relativeLayout_camara5 = (RelativeLayout) findViewById(R.id.relativeLayout_camara5);
        relativeLayout_camara6 = (RelativeLayout) findViewById(R.id.relativeLayout_camara6);


        camara2.setClickable(false);
        camara3.setClickable(false);
        camara4.setClickable(false);
        camara5.setClickable(false);
        camara6.setClickable(false);

        txt_camara2.setClickable(false);
        txt_camara3.setClickable(false);
        txt_camara4.setClickable(false);
        txt_camara5.setClickable(false);
        txt_camara6.setClickable(false);


        comandoEditarEliminarEstado = new ComandoEditarEliminarEstado(this);

        progressDialog = new ProgressDialog(this);

//inicio permiso camara
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

            }
        }


        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.CAMERA)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.CAMERA},
                        2);
            }
        }


//fin permiso camara

        comandoRegistroProductoOferente = new ComandoRegistroProductoOferente(this);

        DatabaseReference key = referencia.push();
        keyFirebase = key.getKey();

        editarFoto();

    }








    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            Log.v("cerrar","cerrar");
            atras();
        }
        return true;
    }

    private void atras() {
        if(btnCamara1== 1 || btnCamara2== 1 || btnCamara3== 1 || btnCamara4== 1 || btnCamara5== 1 || btnCamara6== 1){
            comfirmAlertAtras();
        }else{
            finish();
        }

    }

    public void comfirmAlertAtras() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // Setting Alert Dialog Title
        alertDialogBuilder.setTitle("Cancelar");
        // Icon Of Alert Dialog
        //alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // Setting Alert Dialog Message
        alertDialogBuilder.setMessage("¿Quieres cancelar el proceso?");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Log.v("ok", "ok");
                if (modelo.publicacion_estado == 1) {
                    modelo.publicacion_estado = 0;

                    finish();

                } else {

                    finish();
                }
            }
        });


        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getApplicationContext(),"You clicked over No",Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void camara1(View v) {
        camaraSelecionada = 1;
        btnCamara1 = 1;
        selectImage();
    }

    public void camara2(View v) {
        camaraSelecionada = 2;
        btnCamara2 = 1;
        selectImage();
    }

    public void camara3(View v) {
        camaraSelecionada = 3;
        btnCamara3 = 1;
        selectImage();
    }

    public void camara4(View v) {
        camaraSelecionada = 4;
        btnCamara4 = 1;
        selectImage();
    }

    public void camara5(View v) {
        camaraSelecionada = 5;
        btnCamara5 = 1;
        selectImage();
    }

    public void camara6(View v) {
        camaraSelecionada = 6;
        btnCamara6 = 1;
        selectImage();
    }

    public void refresh() {
        float alpa = (float) 1.0;

        if (img_cam1 && img_cam2 && img_cam3 == false && img_cam4 == false && img_cam5 == false && img_cam6 == false) {
            camara2.setClickable(true);
            txt_camara2.setClickable(true);
            relativeLayout_camara2.setAlpha(alpa);

        }
        if (img_cam1 && img_cam2 && img_cam3 && img_cam4 == false && img_cam5 == false && img_cam6 == false) {
            camara3.setClickable(true);
            txt_camara3.setClickable(true);
            relativeLayout_camara3.setAlpha(alpa);

        }

        if (img_cam1 && img_cam2 && img_cam3 && img_cam4 && img_cam5 == false && img_cam6 == false) {
            camara4.setClickable(true);
            txt_camara4.setClickable(true);
            relativeLayout_camara4.setAlpha(alpa);
            img_cam4 = true;
        }

        if (img_cam1 && img_cam2 && img_cam3 && img_cam4 && img_cam5 && img_cam6 == false) {
            camara5.setClickable(true);
            txt_camara5.setClickable(true);
            relativeLayout_camara5.setAlpha(alpa);

        }

        if (img_cam1 && img_cam2 && img_cam3 && img_cam4 && img_cam5 && img_cam6) {
            camara6.setClickable(true);
            txt_camara6.setClickable(true);
            relativeLayout_camara6.setAlpha(alpa);

        }
    }


    //camara
    private void selectImage() {

        final CharSequence[] items = {"Tomar foto", "Selecionar imagen", "Cancelar"};

        AlertDialog.Builder builder = new AlertDialog.Builder(MyPublicacionesFoto.this);
        builder.setTitle("Seleccionar foto");
        AlertDialog.Builder cancelar = builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                boolean result = Utility.checkPermission(MyPublicacionesFoto.this);
                if (items[item].equals("Tomar foto")) {
                    userChoosenTask = "Take Photo";
                    if (result) {
                        cameraIntent();
                    }


                } else if (items[item].equals("Selecionar imagen")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {
                        galleryIntent();
                    }

                } else if (items[item].equals("Cancelar")) {
                    dialog.dismiss();
                }
            }


        });
        builder.show();
    }


    //permiso camara v6
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    /// manejo de camara
    private void cameraIntent() {
        //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //startActivityForResult(intent, REQUEST_CAMERA);

        dispatchTakePictureIntent();
    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent, SELECT_FILE);

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix //
                ".jpg",         // suffix //
                storageDir      // directory //
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                ruta1 = photoFile.getAbsolutePath();
                photoURI = FileProvider.getUriForFile(this,
                        //"com.tactoapps.android.fileprovider",
                        getString(R.string.authorities),
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_TAKE_PHOTO) {
                onCaptureImageResult();

            }
        }
    }


    //galeria
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        if (data != null) {

            if (camaraSelecionada == 1) {
                img_cam1 = true;


                String url1 = getRealPathFromURI(data.getData());
                setPic(camara1, url1);

                Uri uriSaved = data.getData();


                String newUrl = compressImage(uriSaved, "Media");
                File f = new File(newUrl);
                uriSavedImage1 = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Baja");
                f = new File(newUrl);
                uriSavedImage1Baja = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Alta");
                f = new File(newUrl);
                uriSavedImage1Alta = Uri.fromFile(f);


                if (txt_camara1.getText().toString().equals("Tomar foto")) {
                    foto.add("Foto1");
                } else {
                    foto.set(0, "Foto1");
                }

                txt_camara1.setText("Cambiar foto");
                img_cam2 = true;
                refresh();
            } else if (camaraSelecionada == 2) {
                String url1 = getRealPathFromURI(data.getData());
                setPic(camara2, url1);

                Uri uriSaved = data.getData();


                String newUrl = compressImage(uriSaved, "Media");
                File f = new File(newUrl);
                uriSavedImage2 = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Baja");
                f = new File(newUrl);
                uriSavedImage2Baja = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Alta");
                f = new File(newUrl);
                uriSavedImage2Alta = Uri.fromFile(f);

                if (txt_camara2.getText().toString().equals("Tomar foto")) {
                    foto.add("Foto2");
                } else {
                    foto.set(1, "Foto2");
                }
                txt_camara2.setText("Cambiar foto");
                img_cam3 = true;
                refresh();
            } else if (camaraSelecionada == 3) {

                String url1 = getRealPathFromURI(data.getData());
                setPic(camara3, url1);


                Uri uriSaved = data.getData();


                String newUrl = compressImage(uriSaved, "Media");
                File f = new File(newUrl);
                uriSavedImage3 = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Baja");
                f = new File(newUrl);
                uriSavedImage3Baja = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Alta");
                f = new File(newUrl);
                uriSavedImage3Alta = Uri.fromFile(f);

                if (txt_camara3.getText().toString().equals("Tomar foto")) {
                    foto.add("Foto3");
                } else {
                    foto.set(2, "Foto3");
                }
                txt_camara3.setText("Cambiar foto");
                img_cam4 = true;
                refresh();
            } else if (camaraSelecionada == 4) {

                String url1 = getRealPathFromURI(data.getData());
                setPic(camara4, url1);

                Uri uriSaved = data.getData();

                String newUrl = compressImage(uriSaved, "Media");
                File f = new File(newUrl);
                uriSavedImage4 = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Baja");
                f = new File(newUrl);
                uriSavedImage4Baja = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Alta");
                f = new File(newUrl);
                uriSavedImage4Alta = Uri.fromFile(f);


                if (txt_camara4.getText().toString().equals("Tomar foto")) {
                    foto.add("Foto4");
                } else {
                    foto.set(3, "Foto4");
                }
                txt_camara4.setText("Cambiar foto");
                img_cam5 = true;
                refresh();

            } else if (camaraSelecionada == 5) {

                String url1 = getRealPathFromURI(data.getData());
                setPic(camara5, url1);

                Uri uriSaved = data.getData();


                String newUrl = compressImage(uriSaved, "Media");
                File f = new File(newUrl);
                uriSavedImage5 = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Baja");
                f = new File(newUrl);
                uriSavedImage5Baja = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Alta");
                f = new File(newUrl);
                uriSavedImage5Alta = Uri.fromFile(f);

                if (txt_camara5.getText().toString().equals("Tomar foto")) {
                    foto.add("Foto5");
                } else {
                    foto.set(4, "Foto5");
                }
                txt_camara5.setText("Cambiar foto");
                img_cam6 = true;
                refresh();
            } else if (camaraSelecionada == 6) {

                String url1 = getRealPathFromURI(data.getData());
                setPic(camara6, url1);

                Uri uriSaved = data.getData();


                String newUrl = compressImage(uriSaved, "Media");
                File f = new File(newUrl);
                uriSavedImage6 = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Baja");
                f = new File(newUrl);
                uriSavedImage6Baja = Uri.fromFile(f);

                newUrl = compressImage(uriSaved, "Alta");
                f = new File(newUrl);
                uriSavedImage6Alta = Uri.fromFile(f);

                if (txt_camara6.getText().toString().equals("Tomar foto")) {
                    foto.add("Foto6");
                } else {
                    foto.set(5, "Foto6");
                }
                txt_camara6.setText("Cambiar foto");

            }

        }


    }


    //camara
    private void onCaptureImageResult() {
        //variabale imageview para verificar el tap

        if (camaraSelecionada == 1) {
            setPic(camara1, ruta1);
            galleryAddPic(ruta1);
            String newUrl = compressImageFromCamera(mCurrentPhotoPath, "Media");
            File f = new File(newUrl);
            img_cam1 = true;
            uriSavedImage1 = Uri.fromFile(f);

            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Baja");
            f = new File(newUrl);
            uriSavedImage1Baja = Uri.fromFile(f);

            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Alta");
            f = new File(newUrl);
            uriSavedImage1Alta = Uri.fromFile(f);


            if (txt_camara1.getText().toString().equals("Tomar foto")) {
                foto.add("Foto1");
            } else {
                foto.set(0, "Foto1");
            }
            txt_camara1.setText("Cambiar foto");
            img_cam2 = true;
            refresh();
        } else if (camaraSelecionada == 2) {
            setPic(camara2, ruta1);
            galleryAddPic(ruta1);
            String newUrl = compressImageFromCamera(mCurrentPhotoPath, "Media");
            File f = new File(newUrl);
            img_cam2 = true;
            uriSavedImage2 = Uri.fromFile(f);

            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Baja");
            f = new File(newUrl);
            uriSavedImage2Baja = Uri.fromFile(f);

            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Alta");
            f = new File(newUrl);
            uriSavedImage2Alta = Uri.fromFile(f);

            if (txt_camara2.getText().toString().equals("Tomar foto")) {
                foto.add("Foto2");
            } else {
                foto.set(1, "Foto2");
            }
            txt_camara2.setText("Cambiar foto");
            img_cam3 = true;
            refresh();

        } else if (camaraSelecionada == 3) {
            setPic(camara3, ruta1);
            galleryAddPic(ruta1);
            String newUrl = compressImageFromCamera(mCurrentPhotoPath, "Media");
            File f = new File(newUrl);
            img_cam3 = true;
            uriSavedImage3 = Uri.fromFile(f);

            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Baja");
            f = new File(newUrl);
            uriSavedImage3Baja = Uri.fromFile(f);

            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Alta");
            f = new File(newUrl);
            uriSavedImage3Alta = Uri.fromFile(f);

            if (txt_camara3.getText().toString().equals("Tomar foto")) {
                foto.add("Foto3");
            } else {
                foto.set(2, "Foto3");
            }
            txt_camara3.setText("Cambiar foto");
            img_cam4 = true;
            refresh();
        } else if (camaraSelecionada == 4) {
            setPic(camara4, ruta1);
            galleryAddPic(ruta1);
            String newUrl = compressImageFromCamera(mCurrentPhotoPath, "Media");
            File f = new File(newUrl);
            img_cam2 = true;
            uriSavedImage4 = Uri.fromFile(f);

            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Baja");
            f = new File(newUrl);
            uriSavedImage4Baja = Uri.fromFile(f);

            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Alta");
            f = new File(newUrl);
            uriSavedImage4Alta = Uri.fromFile(f);

            if (txt_camara4.getText().toString().equals("Tomar foto")) {
                foto.add("Foto4");
            } else {
                foto.set(3, "Foto4");
            }
            txt_camara4.setText("Cambiar foto");
            img_cam5 = true;
            refresh();
        } else if (camaraSelecionada == 5) {
            setPic(camara5, ruta1);
            galleryAddPic(ruta1);
            String newUrl = compressImageFromCamera(mCurrentPhotoPath, "Media");
            File f = new File(newUrl);
            img_cam5 = true;
            uriSavedImage5 = Uri.fromFile(f);

            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Baja");
            f = new File(newUrl);
            uriSavedImage5Baja = Uri.fromFile(f);

            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Alta");
            f = new File(newUrl);
            uriSavedImage5Alta = Uri.fromFile(f);

            if (txt_camara5.getText().toString().equals("Tomar foto")) {
                foto.add("Foto5");
            } else {
                foto.set(4, "Foto5");
            }
            txt_camara5.setText("Cambiar foto");
            img_cam6 = true;
            refresh();
        } else if (camaraSelecionada == 6) {
            setPic(camara6, ruta1);
            galleryAddPic(ruta1);
            String newUrl = compressImageFromCamera(mCurrentPhotoPath, "Media");
            File f = new File(newUrl);
            img_cam2 = true;
            uriSavedImage6 = Uri.fromFile(f);


            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Baja");
            f = new File(newUrl);
            uriSavedImage6Baja = Uri.fromFile(f);


            newUrl = compressImageFromCamera(mCurrentPhotoPath, "Alta");
            f = new File(newUrl);
            uriSavedImage6Alta = Uri.fromFile(f);

            if (txt_camara6.getText().toString().equals("Tomar foto")) {
                foto.add("Foto6");
            } else {
                foto.set(5, "Foto6");
            }
            txt_camara6.setText("Cambiar foto");
            img_cam6 = true;
            refresh();
        }


    }


    public void showAlertFoto() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.informacionincompleta));

        // set dialog message
        alertDialogBuilder
                .setMessage("Por favor, Seleccione o tome una foto")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void finalizar(View v) {


        if (img_cam1 == false) {
            showAlertFoto();
        } else {

            modelo.productosOferente.setFotos(foto);
            progressDialog.setMessage("Validando la información, por favor espere...");
            progressDialog.show();

            if (modelo.publicacion_estado == 1) {
                if (uriSavedImage1 == null && uriSavedImage2 == null && uriSavedImage3 == null && uriSavedImage4 == null && uriSavedImage5 == null && uriSavedImage6 == null) {
                    atras();
                } else {
                    comandoEditarEliminarEstado.setActualizarFoto(modelo.pro_uidProducto);
                }

            } else {
                comandoRegistroProductoOferente.setRegistroProducto(keyFirebase);
            }

        }


    }

    @Override
    public void errorSetProductoOferente() {
        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(), "Error con la conexión", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setProductoOferente() {

        uploadImage();

    }

    private void uploadImage() {

        String nameFoto = "";
        int contador = 0;

        if (foto.size() > 0) {

            for (int i = 0; i <= foto.size() - 1; i++) {

                contador = contador + 1;
                nameFoto = "Foto" + contador + "".concat(".png");


                FirebaseStorage storage = FirebaseStorage.getInstance();
                final StorageReference srMedia, srBaja, srAlta;
                //StorageReference storageReference = storage.getReferenceFromUrl("gs://mypetpruebas-6e090.appspot.com").child("productos/"+keyFirebase+"/"+nameFoto);
                if (modelo.publicacion_estado == 1) {
                    nameFoto = "Foto" + contador + "".concat(".png");
                    srBaja = storage.getReference().child("productos/" + modelo.pro_uidProducto + "/" + nameFoto);
                    nameFoto = "Foto" + contador + "_medio".concat(".png");
                    srMedia = storage.getReference().child("productos/" + modelo.pro_uidProducto + "/" + nameFoto);
                    nameFoto = "Foto" + contador + "_alto".concat(".png");
                    srAlta = storage.getReference().child("productos/" + modelo.pro_uidProducto + "/" + nameFoto);


                } else {
                    nameFoto = "Foto" + contador + "".concat(".png");
                    srBaja = storage.getReference().child("productos/" + keyFirebase + "/" + nameFoto);
                    nameFoto = "Foto" + contador + "_medio".concat(".png");
                    srMedia = storage.getReference().child("productos/" + keyFirebase + "/" + nameFoto);
                    nameFoto = "Foto" + contador + "_alto".concat(".png");
                    srAlta = storage.getReference().child("productos/" + keyFirebase + "/" + nameFoto);
                }
                if (contador == 1 && btnCamara1 == 1 && contador <= foto.size()) {

                    pasarImagenSimple(uriSavedImage1Baja, srBaja);
                    pasarImagenSimple(uriSavedImage1Alta, srAlta);
                    pasarImagen(uriSavedImage1, camara1, srMedia, contador);

                } else if (contador == 2 && btnCamara2 == 1 && contador <= foto.size()) {
                    pasarImagenSimple(uriSavedImage2Baja, srBaja);
                    pasarImagenSimple(uriSavedImage2Alta, srAlta);
                    pasarImagen(uriSavedImage2, camara1, srMedia, contador);

                } else if (contador == 3 && btnCamara3 == 1 && contador <= foto.size()) {
                    pasarImagenSimple(uriSavedImage3Baja, srBaja);
                    pasarImagenSimple(uriSavedImage3Alta, srAlta);
                    pasarImagen(uriSavedImage3, camara1, srMedia, contador);

                } else if (contador == 4 && btnCamara4 == 1 && contador <= foto.size()) {
                    pasarImagenSimple(uriSavedImage4Baja, srBaja);
                    pasarImagenSimple(uriSavedImage4Alta, srAlta);
                    pasarImagen(uriSavedImage4, camara1, srMedia, contador);

                } else if (contador == 5 && btnCamara5 == 1 && contador <= foto.size()) {
                    pasarImagenSimple(uriSavedImage5Baja, srBaja);
                    pasarImagenSimple(uriSavedImage5Alta, srAlta);
                    pasarImagen(uriSavedImage5, camara1, srMedia, contador);

                } else if (contador == 6 && btnCamara6 == 1 && contador <= foto.size()) {
                    pasarImagenSimple(uriSavedImage6Baja, srBaja);
                    pasarImagenSimple(uriSavedImage6Alta, srAlta);
                    pasarImagen(uriSavedImage6, camara6, srMedia, contador);
                }

            }
        } else {
            Toast.makeText(getApplicationContext(), "No ha seleccionado ninguna foto", Toast.LENGTH_LONG).show();
        }

    }


    public void pasarImagenSimple(final Uri uriSavedImage, StorageReference storage) {

        if (uriSavedImage != null) {
            storage.putFile(uriSavedImage)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            Log.i("SUBIO FOTO", uriSavedImage.toString());

                            //Toast.makeText(MyPublicacionesFoto.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(MyPublicacionesFoto.this, "Error al cargar la imagen ", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }


    public void showAlertActualizacion() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.publicupdate));

        // set dialog message
        alertDialogBuilder
                .setMessage("")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onBackPressed();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void pasarImagen(Uri uriSavedImage, ImageView imagen, StorageReference storage, final int k) {

        if (uriSavedImage != null) {
            storage.putFile(uriSavedImage)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                            for (int j = 0; j <= foto.size(); j++) {
                                if (j == k) {

                                    if (modelo.publicacion_estado == 1) {
                                        modelo.publicacion_estado = 0;
                                        progressDialog.dismiss();
                                        progressDialog.cancel();
                                        showAlertActualizacion();

                                    } else {

                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                        // Setting Alert Dialog Title
                                        alertDialogBuilder.setTitle("¡Publicación exitosa!");
                                        // Icon Of Alert Dialog
                                        // alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
                                        // Setting Alert Dialog Message
                                        alertDialogBuilder.setMessage("Has finalizado tu publicación, puedes verla o volver al inicio. ");
                                        alertDialogBuilder.setCancelable(false);

                                        alertDialogBuilder.setPositiveButton("PUBLICACIÓN", new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                Log.v("ok", "ok");

                                                modelo.pro_uidProducto = keyFirebase;
                                                modelo.pro_abiertascerrada = "abiertas";
                                                modelo.activa_inactiva = true;
                                                modelo.pro_oferente_vista = 1;
                                                modelo.limiarDatos();

                                                progressDialog.dismiss();
                                                progressDialog.cancel();

                                                Intent i = new Intent(getApplicationContext(), InformacionProductoServicioOferente.class);
                                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(i);
                                                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                                                finish();



                                            }
                                        });

                                        alertDialogBuilder.setNegativeButton("INICIO", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                //Toast.makeText(getApplicationContext(),"You clicked over No",Toast.LENGTH_SHORT).show();
                                                modelo.limiarDatos();
                                                Intent intent = new Intent(getApplicationContext(), TapsHome.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                                                startActivity(intent);

                                                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                                                progressDialog.dismiss();
                                                finish();

                                            }
                                        });

                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        if (!MyPublicacionesFoto.this.isFinishing()){
                                            alertDialog.show();
                                        }
                                        //alertDialog.show();


                                    }
                                }
                            }


                            //Toast.makeText(MyPublicacionesFoto.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(MyPublicacionesFoto.this, "Error al cargar la imagen ", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }


    @Override
    public void cargoEstado() {

    }

    @Override
    public void cargoEliminarPublicacion() {

    }

    @Override
    public void cargoAbiertas() {

    }

    @Override
    public void cargoCerradas() {

    }

    @Override
    public void cargoTipo() {

    }

    @Override
    public void cargoArticulo() {

    }

    @Override
    public void cargoFoto() {


        uploadImage();

    }


    private void editarFoto() {



        if (modelo.publicacion_estado == 1) {

            final float alpa = (float) 1.0;
            for (int i = 0; i < modelo.productosOferentesBoolean.get(0).getFotos().size(); i++) {


                progressDialog.setMessage("Cargando imagenes, por favor espere...");
                progressDialog.show();

                final String fotos = modelo.productosOferentesBoolean.get(0).getFotos().get(i);

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReference().child("productos/" + modelo.productosOferentesBoolean.get(0).getId() + "/" + fotos);

                //get download file url
                storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Log.i("Main", "File uri: " + uri.toString());
                    }
                });

                //download the file
                try {
                    final File localFile = File.createTempFile("images", "jpg");
                    storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());

                            if (fotos.equals("Foto1.png")) {
                                camara1.setImageBitmap(bitmap);
                                String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, null, null);
                                uriSavedImage1 = Uri.parse(path);
                                txt_camara1.setText("Cambiar foto");
                                foto.add("Foto1");
                                img_cam1 = true;
                                img_cam2 = true;
                                refresh();
                            }

                            if (fotos.equals("Foto2.png")) {
                                camara2.setImageBitmap(bitmap);
                                String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, null, null);
                                uriSavedImage2 = Uri.parse(path);
                                txt_camara2.setText("Cambiar foto");
                                foto.add("Foto2");
                                img_cam2 = true;
                                img_cam3 = true;

                                camara2.setClickable(true);
                                txt_camara2.setClickable(true);
                                relativeLayout_camara2.setAlpha(alpa);
                                refresh();
                            }

                            if (fotos.equals("Foto3.png")) {
                                camara3.setImageBitmap(bitmap);
                                String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, null, null);
                                uriSavedImage3 = Uri.parse(path);
                                txt_camara3.setText("Cambiar foto");
                                foto.add("Foto3");
                                img_cam3 = true;
                                img_cam4 = true;

                                camara3.setClickable(true);
                                txt_camara3.setClickable(true);
                                relativeLayout_camara3.setAlpha(alpa);
                                refresh();
                            }

                            if (fotos.equals("Foto4.png")) {
                                camara4.setImageBitmap(bitmap);
                                txt_camara4.setText("Cambiar foto");
                                String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, null, null);
                                uriSavedImage4 = Uri.parse(path);
                                foto.add("Foto4");
                                img_cam4 = true;
                                img_cam5 = true;

                                camara4.setClickable(true);
                                txt_camara4.setClickable(true);
                                relativeLayout_camara4.setAlpha(alpa);
                                refresh();
                            }

                            if (fotos.equals("Foto5.png")) {
                                camara5.setImageBitmap(bitmap);
                                String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, null, null);
                                uriSavedImage5 = Uri.parse(path);
                                txt_camara5.setText("Cambiar foto");
                                foto.add("Foto5");
                                img_cam5 = true;
                                img_cam6 = true;

                                camara5.setClickable(true);
                                txt_camara5.setClickable(true);
                                relativeLayout_camara5.setAlpha(alpa);
                                refresh();
                            }

                            if (fotos.equals("Foto6.png")) {
                                camara6.setImageBitmap(bitmap);
                                String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, null, null);
                                uriSavedImage6 = Uri.parse(path);
                                foto.add("Foto6");
                                txt_camara6.setText("Cambiar foto");
                                img_cam6 = true;

                                camara6.setClickable(true);
                                txt_camara6.setClickable(true);
                                relativeLayout_camara6.setAlpha(alpa);
                                refresh();
                            }

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();


                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("Main", "IOE Exception");
                }


                //fin descargar imagen

            }

            modelo.productosOferente.setFotos(foto);


            //hiloi de 3 segundos

            Thread timer= new Thread(){
                public void run(){
                    try{
                        sleep(5000);

                    }
                    catch(InterruptedException e){
                        e.printStackTrace();
                    }
                    finally{

                        progressDialog.dismiss();
                        progressDialog.cancel();
                        return;
                    }
                }
            };
            timer.start();

        }

    }



    public String compressImage(Uri imageUri, String nivel) {

        String filePath = getRealPathFromURI(imageUri);

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);


        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;


        if (nivel.equals("Baja")) {
            maxHeight = 408.0f;
            maxWidth = 306.0f;
        }

        if (nivel.equals("Media")) {
            maxHeight = 816.0f;
            maxWidth = 612.0f;
        }

        if (nivel.equals("Alta")) {
            maxHeight = 1224.0f;
            maxWidth = 918.0f;
        }


        float imgRatio = (float)actualWidth / (float)actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }


//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 1, 1,
                    scaledBitmap.getWidth()-1, scaledBitmap.getHeight()-1, matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }


    public String compressImageFromCamera(String imageUri, String nivel) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;


        if (nivel.equals("Baja")) {
            maxHeight = 408.0f;
            maxWidth = 306.0f;
        }

        if (nivel.equals("Media")) {
            maxHeight = 816.0f;
            maxWidth = 612.0f;
        }

        if (nivel.equals("Alta")) {
            maxHeight = 1224.0f;
            maxWidth = 918.0f;
        }


        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }


    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");

        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }


    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }


    public String getRealPathFromURI(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    private void setPic(SquareImageView camara, String ruta) {
        // Get the dimensions of the View
        int targetW = camara.getWidth();
        int targetH = camara.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(ruta, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = 1;

        if (targetW != 0 && targetH != 0) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(ruta, bmOptions);
        camara.setScaleType(ScaleType.CENTER_CROP);

        camara.setImageBitmap(bitmap);

    }


    private void galleryAddPic(String ruta) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(ruta);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
        progressDialog.cancel();

        this.finish();
    }

    public void onFinish() {

    }
}


