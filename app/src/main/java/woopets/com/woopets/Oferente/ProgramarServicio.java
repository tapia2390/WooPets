package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import woopets.com.woopets.Oferente.Comandos.ComandoComfirmarReprogrmarChat;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class ProgramarServicio extends Activity implements ComandoComfirmarReprogrmarChat.OnComandoComfirmarReprogrmarChatoChangeListener {

    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int seconds;

    TextView txt_duracion_servicio;
    Button btn_fecha;
    Button btn_hora;
    DatePicker datePicker;
    TimePicker timePicker;
    String am_pm = "";
    Modelo modelo = Modelo.getInstance();
    String idCompra = "";

    ComandoComfirmarReprogrmarChat comandoComfirmarReprogrmarChat;
    CompraProductoAbiertaCerrada compra;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_programar_servicio);

        txt_duracion_servicio = (TextView)findViewById(R.id.txt_duracion_servicio);
        btn_fecha = (Button) findViewById(R.id.btn_fecha);
        btn_hora = (Button) findViewById(R.id.btn_hora);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }



        if (getIntent().hasExtra("IDCOMPRA")) {
            idCompra = getIntent().getStringExtra("IDCOMPRA");
            compra = modelo.getCompraByIdCompra(idCompra);
        } else {
            return;   //Esto es para que alguna vista esta llamando esta vista y no pasa el parametro del IDCOMPRA el error sea evidente y asi detectarlo mas facil
        }

        String fecha = compra.getPedidos().get(0).getFechaDelServicio();

        String[] parts = fecha.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];
        String part3 = parts[2];
        Toast.makeText(getApplicationContext(), "fecha "+fecha, Toast.LENGTH_SHORT).show();

        txt_duracion_servicio.setText("Recuerda la duracion de este servicio es de: "+compra.getPedidos().get(0).getProductosOferentes().get(0).getDuracion() + " " + compra.getPedidos().get(0).getProductosOferentes().get(0).getDuracionMedida());
        comandoComfirmarReprogrmarChat = new ComandoComfirmarReprogrmarChat(this);

        // Get current calendar date and time.
        Calendar currCalendar = Calendar.getInstance();

        // Set the timezone which you want to display time.
        currCalendar.setTimeZone(TimeZone.getTimeZone("America/Bogota"));

        year = currCalendar.get(Calendar.YEAR);
        currCalendar.get(Calendar.AM_PM);
        month = currCalendar.get(Calendar.MONTH);
        day = currCalendar.get(Calendar.DAY_OF_MONTH);
        hour = currCalendar.get(Calendar.HOUR);
        minute = currCalendar.get(Calendar.MINUTE);
        seconds = currCalendar.get(Calendar.SECOND);

        if (currCalendar.get(Calendar.AM_PM) == Calendar.AM)
            am_pm = "AM";
        else if (currCalendar.get(Calendar.AM_PM) == Calendar.PM)
            am_pm = "PM";

         //showUserSelectDateTime();
        btn_fecha.setText(""+part1);
        btn_hora.setText(""+part2+" "+part3);


        // Get date picker object.

         datePicker = (DatePicker)findViewById(R.id.datePickerExample);
        datePicker.init(year, month, day, null);
        datePicker.init(year , month  , day , new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int day) {
                ProgramarServicio.this.year = year;
                ProgramarServicio.this.month = month;
                ProgramarServicio.this.day = day;

                showUserSelectDateTime();
            }
        });

        // Get time picker object.
         timePicker = (TimePicker)findViewById(R.id.pickertime);
        timePicker.setIs24HourView(false);
        //timePicker.setCurrentHour(hour);
        //timePicker.setCurrentMinute(minute);

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int minute) {
                timePicker.setIs24HourView(false);



                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, hour);
                //datetime.set(Calendar.MINUTE, minute);

                if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                    am_pm = "AM";
                else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                    am_pm = "PM";

                String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ?"12":Integer.toString( datetime.get(Calendar.HOUR) );


                ProgramarServicio.this.hour = Integer.parseInt(strHrsToShow);
                ProgramarServicio.this.minute =  minute;


                showUserSelectDateTime();
            }
        });
    }




    @Override
    public void onBackPressed() {
        cancelar();
        super.onBackPressed();
    }

    private void cancelar() {

        finish();
    }

    public void btn_cancelar(View v){
        cancelar();
    }


    /* Show user selected date time in bottom text vew area. */
    private void showUserSelectDateTime()
    {
        // Get TextView object which is used to show user pick date and time.

        StringBuffer strBuffer = new StringBuffer();
        StringBuffer strBuffer2 = new StringBuffer();


        strBuffer.append(this.day);
        strBuffer.append("/");
        strBuffer.append(this.month+1);
        strBuffer.append("/");
        strBuffer.append(this.year);

        strBuffer2.append(this.hour);
        strBuffer2.append(":");
        if(this.minute < 10){
            strBuffer2.append(0);
            strBuffer2.append(this.minute);
        }else{
            strBuffer2.append(this.minute);
        }


        //btn_fecha.setText(strBuffer.toString());
        btn_fecha.setGravity(Gravity.CENTER);
        btn_fecha.setTextSize(20);

        //btn_hora.setText(strBuffer.toString());
        btn_hora.setGravity(Gravity.CENTER);
        btn_hora.setTextSize(20);


        String dateInString = strBuffer.toString();
        String dateInString2 = strBuffer2.toString()+" "+am_pm;



        String date = dateInString;
        if (date.charAt(1) == '/') date = "0" + date;
        if (date.charAt(4) == '/') date = date.substring(0,3) + "0" + date.substring(3);

        btn_fecha.setText(""+date);

        if (dateInString2.charAt(1) == ':') dateInString2 = "" + dateInString2;
        if (dateInString2.charAt(3) > '9') dateInString2 = dateInString2.substring(0,3) + "0" + dateInString2.substring(3);

        btn_hora.setText(""+dateInString2);


    }

    public void fecha(View v){
        btn_fecha.setBackgroundResource(R.drawable.verde_oscuro_post_border_style);
        btn_hora.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        btn_fecha.setTextColor(Color.WHITE);
        btn_hora.setTextColor(Color.BLACK);
        timePicker.setVisibility(View.GONE);
        datePicker.setVisibility(View.VISIBLE);


    }

    public void hora(View v){
        btn_fecha.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        btn_hora.setBackgroundResource(R.drawable.verde_oscuro_post_border_style);
        btn_fecha.setTextColor(Color.BLACK);
        btn_hora.setTextColor(Color.WHITE);
        timePicker.setVisibility(View.VISIBLE);
        datePicker.setVisibility(View.GONE);

    }


    public void programar_aceptar(View v) {

        comandoComfirmarReprogrmarChat.reprogramar(idCompra, btn_fecha.getText().toString()+" "+btn_hora.getText().toString());
    }

    @Override
    public void cmConfirmar() {

    }

    @Override
    public void cmReprogramar() {



        compra.getPedidos().get(0).setFechaDelServicio(btn_fecha.getText().toString()+" "+btn_hora.getText().toString());
        compra.getPedidos().get(0).setEstado("Reprogramada");
        comandoComfirmarReprogrmarChat.reprogramarNotification(compra.getPedidos().get(0).getProductosOferentes().get(0).getNombre(),compra.getPedidos().get(0).getFechaDelServicio());

        cancelar();
    }

    @Override
    public void cmChat() {

    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }
}
