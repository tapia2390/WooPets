package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;


import android.widget.TimePicker;

import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;
import woopets.com.woopets.R;

public class HorarioAtencionDomicilio extends Activity implements TimePickerDialog.OnTimeSetListener, android.app.TimePickerDialog.OnTimeSetListener {


    final Context context = this;
    public ArrayList<String> diasDeLaSemana = new ArrayList<String>();
    public ArrayList<String> finDeSemanaFestivos = new ArrayList<String>();

    ProductosOferente productosOferente = new ProductosOferente();
    boolean l = false;
    boolean m = false;
    boolean x = false;
    boolean j = false;
    boolean v = false;
    boolean s = false;
    boolean d = false;
    boolean f = false;

    Button lunes, martes, miercoles, jueves, viernes, sabado, domingo, festivo, horarioaceptar;
    EditText txt_desde, txt_desde1, txt_hasta, txt_hasta1;
    CheckedTextView check_cierre_fin_semana, check_cierre_semana;

    Modelo modelo = Modelo.getInstance();

    Date date;
    DateFormat hourFormat;

    int setHora = 0;
    int cancelar  = 0;
    String activitiHorario = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_horario_atencion_domicilio);


        Bundle bundle2 = getIntent().getExtras();
        activitiHorario = bundle2.getString("horario");

        lunes = (Button) findViewById(R.id.lunes);
        martes = (Button) findViewById(R.id.martes);
        miercoles = (Button) findViewById(R.id.miercoles);
        jueves = (Button) findViewById(R.id.jueves);
        viernes = (Button) findViewById(R.id.viernes);
        sabado = (Button) findViewById(R.id.sabado);
        domingo = (Button) findViewById(R.id.domingo);
        festivo = (Button) findViewById(R.id.festivo);
        horarioaceptar = (Button) findViewById(R.id.horarioaceptar);
        txt_desde = (EditText) findViewById(R.id.txt_desde);
        txt_desde1 = (EditText) findViewById(R.id.txt_desde1);
        txt_hasta = (EditText) findViewById(R.id.txt_hasta);
        txt_hasta1 = (EditText) findViewById(R.id.txt_hasta1);

    }


    @Override
    protected void onStart() {

        super.onStart();
        date = new Date();
        hourFormat = new SimpleDateFormat("hh:mm aa");
        System.out.println("Hora: " + hourFormat.format(date));


        check_cierre_semana = (CheckedTextView) findViewById(R.id.check_cierre_semana);
        check_cierre_fin_semana = (CheckedTextView) findViewById(R.id.check_cierre_fin_semana);


        if (modelo.sinJornadaContinuaSemana == true) {
            check_cierre_semana.setChecked(true);
        } else {
            check_cierre_semana.setChecked(false);
        }

        if (modelo.sinJornadaContinuaFinDeSemana == true) {
            check_cierre_fin_semana.setChecked(true);
        } else {
            check_cierre_fin_semana.setChecked(false);
        }

        check_cierre_semana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (check_cierre_semana.isChecked() == false) {
                    modelo.sinJornadaContinuaSemana = true;
                    check_cierre_semana.setChecked(true);
                } else {
                    modelo.sinJornadaContinuaSemana = false;
                    check_cierre_semana.setChecked(false);
                }
            }
        });


        check_cierre_fin_semana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (check_cierre_fin_semana.isChecked() == false) {
                    check_cierre_fin_semana.setChecked(true);
                    modelo.sinJornadaContinuaFinDeSemana = true;
                } else {
                    modelo.sinJornadaContinuaFinDeSemana = false;
                    check_cierre_fin_semana.setChecked(false);
                }
            }
        });



        if (activitiHorario.equals("horarioEditarOferente") ) {
            modelo.diasDeLaSemana.clear();
            modelo.finDeSemanaFestivos.clear();
            String ds = modelo.datosOferente.getHorarioDiasDeLaSemana();
            String dsf = modelo.datosOferente.getHorarioDiasFinDeSemana();
            String diasS[] = ds.split(",");
            String diasF[] = dsf.split(",");

            List<String> arraydias = new ArrayList<String>();
            arraydias = Arrays.asList(diasS);
            List<String> arraydiasFin = new ArrayList<String>();
            arraydiasFin = Arrays.asList(diasF);

            //10 7 4 9
            modelo.diasDeLaSemana.addAll(arraydias);
            modelo.finDeSemanaFestivos.addAll(arraydiasFin);

            modelo.horariodesemanaIni = modelo.datosOferente.getHorarioInicioDiasDeLaSemana();
            modelo.horariodesemanaFin = modelo.datosOferente.getHorarioCierreDiasDeLaSemana();
            modelo.sinJornadaContinuaSemana = modelo.datosOferente.getSinJornadaContinuaSemana();

            modelo.horariofindesemanaIni = modelo.datosOferente.getHorarioInicioDiasFinDeSemana();
            modelo.horariofindesemanaFin = modelo.datosOferente.getHorarioCierreDiasFinDeSemana();
            modelo.sinJornadaContinuaFinDeSemana = modelo.datosOferente.getSinJornadaContinuaFinDeSemana();

        }

        //9 5 12 6
        if(activitiHorario.equals("horarioMyPublicacionesArticulo")){

            txt_desde.setText(modelo.horariodesemanaIni);
            txt_hasta.setText(modelo.horariodesemanaFin);
            check_cierre_semana.setChecked(modelo.sinJornadaContinuaSemana);

            txt_desde1.setText(modelo.horariofindesemanaIni);
            txt_hasta1.setText(modelo.horariofindesemanaIni);
            check_cierre_fin_semana.setChecked(modelo.sinJornadaContinuaFinDeSemana);

        }

        if (activitiHorario.equals("horarioRegistroOferente") ) {
            modelo.sinJornadaContinuaSemana = modelo.datosOferente.getSinJornadaContinuaSemana();
            modelo.sinJornadaContinuaFinDeSemana = modelo.datosOferente.getSinJornadaContinuaFinDeSemana();
        }

        if (modelo.diasDeLaSemana.size() > 0) {

            Log.v("m", "m" + modelo.diasDeLaSemana.get(0));
            for (int i = 0; i < modelo.diasDeLaSemana.size(); i++) {

                if (modelo.diasDeLaSemana.get(i).equals("Lunes,") || modelo.diasDeLaSemana.get(i).equals("Lunes")) {
                    l = true;
                    lunes.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    lunes.setTextColor(getResources().getColor(R.color.colorBlanco));
                    diasDeLaSemana.add("1");
                }

                if (modelo.diasDeLaSemana.get(i).equals("Martes,") || modelo.diasDeLaSemana.get(i).equals("Martes")) {
                    m = true;
                    martes.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    martes.setTextColor(getResources().getColor(R.color.colorBlanco));
                    diasDeLaSemana.add("2");
                }

                if (modelo.diasDeLaSemana.get(i).equals("Miercoles,") || modelo.diasDeLaSemana.get(i).equals("Miercoles") || modelo.diasDeLaSemana.get(i).equals("Miércoles,") || modelo.diasDeLaSemana.get(i).equals("Miércoles")) {
                    x = true;
                    miercoles.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    miercoles.setTextColor(getResources().getColor(R.color.colorBlanco));
                    diasDeLaSemana.add("3");
                }

                if (modelo.diasDeLaSemana.get(i).equals("Jueves,") || modelo.diasDeLaSemana.get(i).equals("Jueves")) {
                    j = true;
                    jueves.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    jueves.setTextColor(getResources().getColor(R.color.colorBlanco));
                    diasDeLaSemana.add("4");
                }

                if (modelo.diasDeLaSemana.get(i).equals("Viernes,") || modelo.diasDeLaSemana.get(i).equals("Viernes")) {
                    v = true;
                    viernes.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    viernes.setTextColor(getResources().getColor(R.color.colorBlanco));
                    diasDeLaSemana.add("5");
                }
            }


            txt_desde.setText(modelo.horariodesemanaIni);
            txt_hasta.setText(modelo.horariodesemanaFin);
            check_cierre_semana.setChecked(modelo.sinJornadaContinuaSemana);

        }

        if (modelo.finDeSemanaFestivos.size() > 0) {

            Log.v("m", "m" + modelo.finDeSemanaFestivos.get(0));
            for (int i = 0; i < modelo.finDeSemanaFestivos.size(); i++) {

                if (modelo.finDeSemanaFestivos.get(i).equals("Sabado,") || modelo.finDeSemanaFestivos.get(i).equals("Sabado") || modelo.finDeSemanaFestivos.get(i).equals("Sábado,") || modelo.finDeSemanaFestivos.get(i).equals("Sábado")) {
                    s = true;
                    sabado.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    sabado.setTextColor(getResources().getColor(R.color.colorBlanco));
                    finDeSemanaFestivos.add("6");
                }

                if (modelo.finDeSemanaFestivos.get(i).equals("Domingo,") || modelo.finDeSemanaFestivos.get(i).equals("Domingo")) {
                    d = true;
                    domingo.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    domingo.setTextColor(getResources().getColor(R.color.colorBlanco));
                    finDeSemanaFestivos.add("7");
                }

                if (modelo.finDeSemanaFestivos.get(i).equals("Festivo,") || modelo.finDeSemanaFestivos.get(i).equals("Festivo")) {
                    f = true;
                    festivo.setBackgroundResource(R.drawable.verde_oscuro_post_border_style);
                    festivo.setTextColor(getResources().getColor(R.color.colorBlanco));
                    finDeSemanaFestivos.add("8");
                }
            }


            txt_desde1.setText(modelo.horariofindesemanaIni);
            txt_hasta1.setText(modelo.horariofindesemanaFin);
            check_cierre_fin_semana.setChecked(modelo.sinJornadaContinuaFinDeSemana);
        }



    }

    @Override
    public void onBackPressed() {
        btnAtras();
        super.onBackPressed();
    }

    private void btnAtras() {
        this.finish();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void diasSemana(View view) {

        //Toast.makeText(getApplicationContext(),"id:"+view.getId(),Toast.LENGTH_SHORT).show();
        switch (view.getId()) {
            case R.id.lunes:
                //Toast.makeText(getApplicationContext(),"Dia: Lunes",Toast.LENGTH_SHORT).show();
                if (l == false) {
                    l = true;
                    lunes.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    lunes.setTextColor(getResources().getColor(R.color.colorBlanco));
                    diasDeLaSemana.add("1");
                } else {
                    l = false;
                    lunes.setBackgroundResource(R.drawable.circulo_gris_claro_post_border_style);
                    lunes.setTextColor(getResources().getColor(R.color.colorNegro));
                    for (int i = 0; i < diasDeLaSemana.size(); i++) {
                        if (diasDeLaSemana.get(i).equals("1")) {
                            diasDeLaSemana.remove(i);
                        }
                    }
                }
                break;
            case R.id.martes:
                //Toast.makeText(getApplicationContext(),"Dia: martes",Toast.LENGTH_SHORT).show();
                if (m == false) {
                    m = true;
                    martes.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    martes.setTextColor(getResources().getColor(R.color.colorBlanco));
                    diasDeLaSemana.add("2");
                } else {
                    m = false;
                    martes.setBackgroundResource(R.drawable.circulo_gris_claro_post_border_style);
                    martes.setTextColor(getResources().getColor(R.color.colorNegro));
                    for (int i = 0; i < diasDeLaSemana.size(); i++) {
                        if (diasDeLaSemana.get(i).equals("2")) {
                            diasDeLaSemana.remove(i);
                        }
                    }
                }
                break;
            case R.id.miercoles:
                //Toast.makeText(getApplicationContext(),"Dia: Miercoles",Toast.LENGTH_SHORT).show();

                if (x == false) {
                    x = true;
                    miercoles.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    miercoles.setTextColor(getResources().getColor(R.color.colorBlanco));
                    diasDeLaSemana.add("3");
                } else {
                    x = false;
                    miercoles.setBackgroundResource(R.drawable.circulo_gris_claro_post_border_style);
                    miercoles.setTextColor(getResources().getColor(R.color.colorNegro));
                    for (int i = 0; i < diasDeLaSemana.size(); i++) {
                        if (diasDeLaSemana.get(i).equals("3")) {
                            diasDeLaSemana.remove(i);
                        }
                    }
                }
                break;
            case R.id.jueves:
                //Toast.makeText(getApplicationContext(),"Dia: Jueves",Toast.LENGTH_SHORT).show();
                if (j == false) {
                    j = true;
                    jueves.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    jueves.setTextColor(getResources().getColor(R.color.colorBlanco));
                    diasDeLaSemana.add("4");
                } else {
                    j = false;
                    jueves.setBackgroundResource(R.drawable.circulo_gris_claro_post_border_style);
                    jueves.setTextColor(getResources().getColor(R.color.colorNegro));
                    for (int i = 0; i < diasDeLaSemana.size(); i++) {
                        if (diasDeLaSemana.get(i).equals("4")) {
                            diasDeLaSemana.remove(i);
                        }
                    }
                }

                break;
            case R.id.viernes:
                //Toast.makeText(getApplicationContext(),"Dia: Viernes",Toast.LENGTH_SHORT).show();
                if (v == false) {
                    v = true;
                    viernes.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    viernes.setTextColor(getResources().getColor(R.color.colorBlanco));
                    diasDeLaSemana.add("5");
                } else {
                    v = false;
                    viernes.setBackgroundResource(R.drawable.circulo_gris_claro_post_border_style);
                    viernes.setTextColor(getResources().getColor(R.color.colorNegro));
                    for (int i = 0; i < diasDeLaSemana.size(); i++) {
                        if (diasDeLaSemana.get(i).equals("5")) {
                            diasDeLaSemana.remove(i);
                        }
                    }
                }
                break;
            case R.id.sabado:
                //Toast.makeText(getApplicationContext(),"Dia: sabado",Toast.LENGTH_SHORT).show();
                if (s == false) {
                    s = true;
                    sabado.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    sabado.setTextColor(getResources().getColor(R.color.colorBlanco));
                    finDeSemanaFestivos.add("6");
                } else {
                    s = false;
                    sabado.setBackgroundResource(R.drawable.circulo_gris_claro_post_border_style);
                    sabado.setTextColor(getResources().getColor(R.color.colorNegro));
                    for (int i = 0; i < finDeSemanaFestivos.size(); i++) {
                        if (finDeSemanaFestivos.get(i).equals("6")) {
                            finDeSemanaFestivos.remove(i);
                        }
                    }
                }
                break;
            case R.id.domingo:
                //Toast.makeText(getApplicationContext(),"Dia: domingo",Toast.LENGTH_SHORT).show();
                if (d == false) {
                    d = true;
                    domingo.setBackgroundResource(R.drawable.circulo_verde_oscuro_post_border_style);
                    domingo.setTextColor(getResources().getColor(R.color.colorBlanco));
                    finDeSemanaFestivos.add("7");
                } else {
                    d = false;
                    domingo.setBackgroundResource(R.drawable.circulo_gris_claro_post_border_style);
                    domingo.setTextColor(getResources().getColor(R.color.colorNegro));
                    for (int i = 0; i < finDeSemanaFestivos.size(); i++) {
                        if (finDeSemanaFestivos.get(i).equals("7")) {
                            finDeSemanaFestivos.remove(i);
                        }
                    }
                }
                break;
            case R.id.festivo:
                //Toast.makeText(getApplicationContext(),"Dia: festivo",Toast.LENGTH_SHORT).show();
                if (f == false) {
                    f = true;
                    festivo.setBackgroundResource(R.drawable.verde_oscuro_post_border_style);
                    festivo.setTextColor(getResources().getColor(R.color.colorBlanco));
                    finDeSemanaFestivos.add("8");
                } else {
                    f = false;
                    festivo.setBackgroundResource(R.drawable.gris_claro_post_border_style);
                    festivo.setTextColor(getResources().getColor(R.color.colorNegro));
                    for (int i = 0; i < finDeSemanaFestivos.size(); i++) {
                        if (finDeSemanaFestivos.get(i).equals("8")) {
                            finDeSemanaFestivos.remove(i);
                        }
                    }
                }
                break;
        }
    }


    public void horarioaceptar(View v){
        horarioaceptar2();
    }

    public void horarioaceptar2() {

        if (diasDeLaSemana.size() <= 0 && finDeSemanaFestivos.size() <= 0) {
            showAlertHorario();
            return;

        }
        if (diasDeLaSemana.size() > 0) {
            if (txt_desde.getText().toString().equals("") || txt_hasta.getText().toString().equals("")) {
                showAlertHorario();
                return;
            }
        }

        if (finDeSemanaFestivos.size() > 0) {
            if (txt_desde1.getText().toString().equals("") || txt_hasta1.getText().toString().equals("")) {
                showAlertHorario();
                return;
            }

        }
        eviarDatos();

    }


    public void showAlertHorario() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.informacionincompleta));

        // set dialog message
        alertDialogBuilder
                .setMessage("" + getString(R.string.informacionincompletaHorarioOferene))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        return;
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void hora(View view) {

        switch (view.getId()) {
            case R.id.txt_hasta:
                setHora = 1;
                customTimePickerDialog();
                break;

            case R.id.txt_desde:
                setHora = 2;
                customTimePickerDialog();
                break;

            case R.id.txt_hasta1:
                setHora = 3;
                customTimePickerDialog();
                break;

            case R.id.txt_desde1:
                setHora = 4;
                customTimePickerDialog();
                break;
        }
    }


    //__methode will be call when we click on "Custom Date Picker Dialog" and will be show the custom date selection dilog.
    public void customTimePickerDialog() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog dpd = TimePickerDialog.newInstance(this, now.get(Calendar.HOUR), now.get(Calendar.MINUTE), false);
        dpd.setAccentColor(getResources().getColor(R.color.colorVerdeOscuro));
        dpd.show(getFragmentManager(), "Timepickerdialog");
    }

    //___this is the listener callback method will be call on time selection by default date picker.
    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
        //Toast.makeText(this, "Selected by default time picker : " + hourOfDay + ":" + minute, Toast.LENGTH_SHORT).show();
    }

    //___this is the listener callback method will be call on time selection by custom date picker.
    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        Calendar datetime = Calendar.getInstance();
        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        datetime.set(Calendar.MINUTE, minute);
        date = datetime.getTime();
        Log.v("hourOfDay", "hourOfDay" + hourOfDay + "hra ..." + hourFormat.format(date));
        //Toast.makeText(this, "Selected by custom time picker : " + hourOfDay + ":" + minute, Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(),""+hourFormat.format(date), Toast.LENGTH_LONG).show();


        String string = "" + hourFormat.format(date);
        String[] parts = string.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];

        if (part2.equals("a.") || part2.equals("a.m.") || part2.equals("a. m.") || part2.equals("A.")) {
            part2 = "AM";
        }
        if (part2.equals("p.") || part2.equals("p.m.") || part2.equals("p. m.") || part2.equals("P.")) {
            part2 = "PM";
        }

        if (setHora == 1) {
            txt_hasta.setText("" + part1 + " " + part2);
            modelo.horariodesemanaFin  = part1 + " " + part2;
        }
        if (setHora == 2) {
            txt_desde.setText("" + "" + part1 + " " + part2);
            modelo.horariodesemanaIni = part1 + " " + part2;
        }
        if (setHora == 3) {
            txt_hasta1.setText("" + "" + part1 + " " + part2);
            modelo.horariofindesemanaFin  = part1 + " " + part2;
        }
        if (setHora == 4) {
            txt_desde1.setText("" + "" + part1 + " " + part2);
            modelo.horariofindesemanaIni = part1 + " " + part2;
        }
    }

    private void eviarDatos() {
        modelo.diasDeLaSemana.clear();
        modelo.finDeSemanaFestivos.clear();
        if (diasDeLaSemana.size() > 0) {

            Collections.sort(diasDeLaSemana);
            for (int i = 0; i < diasDeLaSemana.size(); i++) {

                if (diasDeLaSemana.get(i).equals("1")) {
                    modelo.diasDeLaSemana.add("Lunes");
                }
                if (diasDeLaSemana.get(i).equals("2")) {
                    modelo.diasDeLaSemana.add("Martes");
                }
                if (diasDeLaSemana.get(i).equals("3")) {
                    modelo.diasDeLaSemana.add("Miércoles");
                }
                if (diasDeLaSemana.get(i).equals("4")) {
                    modelo.diasDeLaSemana.add("Jueves");
                }
                if (diasDeLaSemana.get(i).equals("5")) {
                    modelo.diasDeLaSemana.add("Viernes");
                }
            }

        }
        if (finDeSemanaFestivos.size() > 0) {

            Collections.sort(finDeSemanaFestivos);
            for (int i = 0; i < finDeSemanaFestivos.size(); i++) {

                if (finDeSemanaFestivos.get(i).equals("6")) {
                    modelo.finDeSemanaFestivos.add("Sábado");
                }
                if (finDeSemanaFestivos.get(i).equals("7")) {
                    modelo.finDeSemanaFestivos.add("Domingo");
                }
                if (finDeSemanaFestivos.get(i).equals("8")) {
                    modelo.finDeSemanaFestivos.add("Festivo");
                }
            }

        }


        modelo.datosOferente.setHorarioInicioDiasDeLaSemana(modelo.horariodesemanaIni);
        modelo.datosOferente.setHorarioCierreDiasDeLaSemana(modelo.horariodesemanaFin);
        modelo.datosOferente.setSinJornadaContinuaSemana(modelo.sinJornadaContinuaSemana);

        modelo.datosOferente.setHorarioInicioDiasFinDeSemana(modelo.horariofindesemanaIni);
        modelo.datosOferente.setHorarioCierreDiasFinDeSemana(modelo.horariofindesemanaFin);
        modelo.datosOferente.setSinJornadaContinuaFinDeSemana(modelo.sinJornadaContinuaFinDeSemana);


        btnAtras();
    }

}
