package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 1/12/17.
 */

public class ComandoDatosCelular {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    public interface OnComandoDatosCelularChangeListener {

        void cargoDatosCelular();
        void usuarioNoregistardo();
    }

    //interface del listener de la actividad interesada
    private ComandoDatosCelular.OnComandoDatosCelularChangeListener mListener;

    public ComandoDatosCelular(ComandoDatosCelular.OnComandoDatosCelularChangeListener mListener){

        this.mListener = mListener;
    }


    public void setDatosCelular(final String tokenDevice){


        DatabaseReference ref = database.getReference("oferentes/" + modelo.uid);//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                if (!snap.exists()) {
                    mListener.usuarioNoregistardo();
                }
                else{
                    Date date = new Date();
                    //Caso 1: obtener la hora y salida por pantalla con formato:
                    DateFormat hourFormat = new SimpleDateFormat("h:mm aa");
                    System.out.println("Hora: "+hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    System.out.println("Fecha: "+dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
                    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                    System.out.println("Hora y fecha: "+hourdateFormat.format(date));


                    final DatabaseReference ref = database.getReference("oferentes/"+modelo.uid+"/");

                    if(!modelo.uid.equals("")){

                        String string = ""+hourFormat.format(date);
                        String[] parts = string.split(" ");
                        String part1 = parts[0];
                        String part2 = parts[1];

                        if(part2.equals("a.") || part2.equals("a.m")  || part2.equals("a. m.") || part2.equals("A.")){
                            part2 = "AM";
                        }
                        if(part2.equals("p.") || part2.equals("p.m")|| part2.equals("p. m.") || part2.equals("P.")){
                            part2 = "PM";
                        }

                        Map<String, Object> dotosLogueo = new HashMap<String, Object>();
                        dotosLogueo.put("fechaUltimoLogeo",""+dateFormat.format(date));
                        dotosLogueo.put("horaUltimoLogeo", ""+part1+" "+part2);
                        dotosLogueo.put("systemDevice", "Android");
                        dotosLogueo.put("tokenDevice", ""+tokenDevice);
                        dotosLogueo.put("version", ""+modelo.versionName);
                        ref.updateChildren(dotosLogueo);

                    }

                    mListener.cargoDatosCelular();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoDatosCelularChangeListener sDummyCallbacks = new OnComandoDatosCelularChangeListener()
    {
        @Override
        public void cargoDatosCelular()
        {}

        @Override
        public void usuarioNoregistardo()
        {}



    };
}
