package woopets.com.woopets.Oferente.ModelOferente;

/**
 * Created by tacto on 10/12/17.
 */

public class PreguntasProducto {

    String fechaProducto ="";
    String pregunta ="";
    String  respuesta ="";

    public String getFechaProducto() {
        return fechaProducto;
    }

    public void setFechaProducto(String fechaProducto) {
        this.fechaProducto = fechaProducto;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
