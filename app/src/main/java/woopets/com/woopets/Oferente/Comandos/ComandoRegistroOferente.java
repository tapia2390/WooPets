package woopets.com.woopets.Oferente.Comandos;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 29/06/17.
 */

public class ComandoRegistroOferente {

    //traer ui
    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    public interface OnComandoRegistroOferenteChangeListener {

        void informacionRegistroOferente();
        void errorInformacionRegistroOferente();

    }

    //interface del listener de la actividad interesada
    private OnComandoRegistroOferenteChangeListener mListener;

    public ComandoRegistroOferente(OnComandoRegistroOferenteChangeListener mListener){

        this.mListener = mListener;
    }


    public void registroOferente(final String celularEstablecimiento, final String celularOferente, final String correoOferente, final String documentoOferente,
                                 final String nombreOferente, final String telefonoOferente, final String tipoDocumentoOferente, final String correoEstablecimineto,
                                 final String direcionEstablecimineto,  final String finDeSemanadias,
                                 final String finDESemanaHoraCierre, final String finDesemanHorarioInicio, String semanadias, final String semanaHoraCierre,
                                 final String semanaHorarioInicio, final String nitEstablecimiento, final String paginawebEstablecimiento,final String razonSocialEstablecimiento,
                                 final String telefonoEstablecimiento, final double latitud, final double longitud ,final String vercionAndoid, String idUsu,
                                 boolean  sinJornadaContinuaSemana, boolean booleansinJornadaContinuaFinSemana) {

        DatabaseReference key = referencia.push();

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("h:mm a");
        System.out.println("Hora: "+hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: "+dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: "+hourdateFormat.format(date));

        int setHora = 0;
        String string = ""+hourFormat.format(date);
        String[] parts = string.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];

        if(part2.equals("a.") || part2.equals("a.m.") || part2.equals("a. m.")|| part2.equals("A.")){
            part2 = "AM";
        }
        if(part2.equals("p.") ||  part2.equals("p.m.") ||  part2.equals("p. m.") || part2.equals("P.")){
            part2 = "PM";
        }


        DatabaseReference ref = database.getReference("oferentes/"+idUsu+"/");//ruta path

        Map<String, Object> enviarRegistro = new HashMap<String, Object>();
        enviarRegistro.put("aprobacionMyPet", "Pendiente");
        enviarRegistro.put("celular", celularEstablecimiento);

            Map<String, Object> contactoPrincipal = new HashMap<String, Object>();
            DatabaseReference key2 = referencia.push();
            contactoPrincipal.put("celular",celularOferente);
            contactoPrincipal.put("correo",correoOferente);
            contactoPrincipal.put("documento",documentoOferente);
            contactoPrincipal.put("nombre",nombreOferente);
            contactoPrincipal.put("telefono",telefonoOferente);
            contactoPrincipal.put("tipoDocumento",tipoDocumentoOferente);
        enviarRegistro.put("contactoPrincipal",contactoPrincipal);


        enviarRegistro.put("correo", correoEstablecimineto);
        enviarRegistro.put("direccion", direcionEstablecimineto);
        enviarRegistro.put("fechaUltimoLogeo", dateFormat.format(date));
        enviarRegistro.put("horaUltimoLogeo", part1+" "+part2);

        Map<String,Object> horario = new HashMap<String,Object>();
        if(!finDeSemanadias.equals("")){

            Map<String,Object> findesemana = new HashMap<String,Object>();
            findesemana.put("dias",finDeSemanadias);
            findesemana.put("horaCierre",finDESemanaHoraCierre);
            findesemana.put("horaInicio",finDesemanHorarioInicio);
            findesemana.put("sinJornadaContinua", booleansinJornadaContinuaFinSemana);

            horario.put("FinDeSemana",findesemana);

            enviarRegistro.put("horario",horario);

        }

        if(semanadias.equals("dias")){
            semanadias = "";
        }
        if(!semanadias.equals("") && semanadias.length() > 0){
            Map<String,Object> semana = new HashMap<String,Object>();
            semana.put("dias",semanadias);
            semana.put("horaCierre",semanaHoraCierre);
            semana.put("horaInicio",semanaHorarioInicio);
            semana.put("sinJornadaContinua",sinJornadaContinuaSemana);

            horario.put("Semana",semana);

            enviarRegistro.put("horario",horario);



        }

        enviarRegistro.put("nit", nitEstablecimiento);
        if(!paginawebEstablecimiento.equals("")){
            enviarRegistro.put("paginaWeb", paginawebEstablecimiento);
        }

        enviarRegistro.put("razonSocial", razonSocialEstablecimiento);
        enviarRegistro.put("systemDevice", "ANDROID" );
        enviarRegistro.put("telefono", telefonoEstablecimiento);


        Map<String, Object> ubicacion = new HashMap<String, Object>();
        ubicacion.put("lat", latitud);
        ubicacion.put("lon", longitud);
        enviarRegistro.put("ubicacion",ubicacion);

        enviarRegistro.put("version", vercionAndoid);
        setGeoFire(idUsu);

        ref.setValue(enviarRegistro, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {

                    mListener.informacionRegistroOferente();
                } else {
                    mListener.errorInformacionRegistroOferente();
                }
            }
        });

    }



    public void setGeoFire(String idUsu){


        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("ubicacionOferentes/");
        GeoFire geoFire = new GeoFire(ref);
        geoFire.setLocation(idUsu, new GeoLocation(modelo.latitud, modelo.longitud), new GeoFire.CompletionListener() {
            @Override
            public void onComplete(String key, DatabaseError error) {
                if (error != null) {
                    System.err.println("Se ha producido un error al guardar la ubicación: " + error);

                } else {
                    System.out.println("Ubicación guardada en el servidor correctamente!");

                }
            }
        });



    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoRegistroOferenteChangeListener sDummyCallbacks = new OnComandoRegistroOferenteChangeListener()
    {
        @Override
        public void informacionRegistroOferente()
        {}

        @Override
        public void errorInformacionRegistroOferente()
        {}

    };
}
