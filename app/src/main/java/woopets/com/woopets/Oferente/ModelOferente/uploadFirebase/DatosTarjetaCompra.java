package woopets.com.woopets.Oferente.ModelOferente.uploadFirebase;

/**
 * Created by tacto on 31/01/18.
 */

public class DatosTarjetaCompra {
    int cuotasTarjeta = 0;
    String idTarjeta = "";
    String tokenTarjeta = "";
    public String correo = "";
    public String cedula = "";
    public String direccion = "";
    public String ciudad = "";
    public String departamento = "";
    public String nombre = "";
    public String franquicia = "";
    public String telefono = "";




    public int getCuotasTarjeta() {
        return cuotasTarjeta;
    }

    public void setCuotasTarjeta(int cuotasTarjeta) {
        this.cuotasTarjeta = cuotasTarjeta;
    }

    public String getIdTarjeta() {
        return idTarjeta;
    }

    public void setIdTarjeta(String idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public String getTokenTarjeta() {
        return tokenTarjeta;
    }

    public void setTokenTarjeta(String tokenTarjeta) {
        this.tokenTarjeta = tokenTarjeta;
    }
}
