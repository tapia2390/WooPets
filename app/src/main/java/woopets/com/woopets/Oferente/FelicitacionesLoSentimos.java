package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class FelicitacionesLoSentimos extends Activity {

    TextView titulomypet;
    TextView descricionventa;
    String exitosoNegado="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_felicitaciones_lo_sentimos);

        Bundle bundle2 = getIntent().getExtras();
        exitosoNegado = bundle2.getString("exitosoNegado");


        titulomypet = (TextView)findViewById(R.id.titulomypet);
        descricionventa = (TextView)findViewById(R.id.descricionventa);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

        if(exitosoNegado.equals("1")){
            titulomypet.setText("¡Felicitaciones!");
            descricionventa.setText("Tu información ha sido validad con éxito, desde este momento tus" +
                    " publicaciones serán visibles a los usuarios WooPets, si no tiene ninguna, te invitamos " +
                    "a crear tu primera publicación desde la app.  ");

        }else{
            titulomypet.setText("¡Lo sentimos!");
            descricionventa.setText("Tu información presenta algunos inconvenientes, los campos: \n" +
                    "Xxx, Xxx \n " +
                    "No han sido confirmados con éxito, te pedimos el favor de verificar la información " +
                    "para poder aprobar tu registro. Podrás editar los datos através de la opción " +
                    "&#34;Editar Perfil&#34;, alli encntraras los campos en blanco para corregir, te recordamos " +
                    "que las publicaciones no serán visibles a los usuarios Woopets hasta que seas aprobado. ");
        }
    }



    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void btn_ok(View v){
        aprovadoRechazado();
    }

    public void aprovadoRechazado(){
        finish();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }
}
