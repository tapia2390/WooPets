package woopets.com.woopets.Oferente.AdapterOferente;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import woopets.com.woopets.Oferente.DetallesVenasAC;
import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.NotificaionesOferente;
import woopets.com.woopets.Oferente.RespuestaPreguntasPublicaciones;
import woopets.com.woopets.R;

/**
 * Created by tacto on 23/08/17.
 */

public class PreguntasMisPublicacionesClienteAdapter extends BaseAdapter {

    //Atributos del adaptador
    final Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<ClassPreguntas_A_MisPublicaciones> preguntasMisPublicacionesCliente;
    private Modelo sing = Modelo.getInstance();
    String imegenPortada = "";


    public PreguntasMisPublicacionesClienteAdapter(Context mContext, AdapterCallback mListener) {

        sing.adicionarListaPreguntas(sing.idPreguntasClientePublicaciones, sing.filtro);
        this.mContext = mContext;
        this.mListener = mListener;


        //ordenar
        Collections.sort(sing.preguntas_publicaiones_detalle, new Comparator<ClassPreguntas_A_MisPublicaciones>() {
            @Override
            public int compare(ClassPreguntas_A_MisPublicaciones o1, ClassPreguntas_A_MisPublicaciones o2) {
                return new Long(o2.getTimestamp()).compareTo(new Long(o1.getTimestamp()));
            }
        });


        Set<ClassPreguntas_A_MisPublicaciones> quipu = new HashSet<ClassPreguntas_A_MisPublicaciones>(sing.preguntas_publicaiones_detalle);
        for (ClassPreguntas_A_MisPublicaciones key : quipu) {
            System.out.println(key + " : " + Collections.frequency(sing.listaNotifications, key));
        }



        this.preguntasMisPublicacionesCliente = sing.preguntas_publicaiones_detalle;


    }

    @Override
    public int getCount() {
        return preguntasMisPublicacionesCliente.size();
    }

    @Override
    public Object getItem(int position) {
        return preguntasMisPublicacionesCliente.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        final ImageView foto_ciente;
        final TextView id_pregunta;
        final TextView txt_nombre_cliente;
        final TextView fecha_pregunta;
        final TextView txt_pregunta;
        final TextView txt_respuesta;
        final TextView fecha_respuesta;
        final Button responder;
        final RelativeLayout contenedor_lista_clientes;
        LinearLayout layout_respusta;


        final ClassPreguntas_A_MisPublicaciones preguntas_A_MisPublicaciones = (ClassPreguntas_A_MisPublicaciones) getItem(position);

        // Inflate la vista de la productosOferentes
        RelativeLayout itemLayout = (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.adapter_preguntas_publicaciones_cliente, parent, false);

        foto_ciente = (ImageView) itemLayout.findViewById(R.id.foto_ciente);
        id_pregunta = (TextView) itemLayout.findViewById(R.id.id_pregunta);
        txt_nombre_cliente = (TextView) itemLayout.findViewById(R.id.txt_nombre_cliente);
        fecha_pregunta = (TextView) itemLayout.findViewById(R.id.fecha_pregunta);
        txt_pregunta = (TextView) itemLayout.findViewById(R.id.txt_pregunta);
        txt_respuesta = (TextView) itemLayout.findViewById(R.id.txt_respuesta);
        fecha_respuesta = (TextView) itemLayout.findViewById(R.id.fecha_respuesta);
        responder = (Button) itemLayout.findViewById(R.id.responder);
        layout_respusta = (LinearLayout) itemLayout.findViewById(R.id.layout_respusta);






        if (preguntas_A_MisPublicaciones.getFechaRespuesta() != null) {
            responder.setVisibility(View.GONE);
        }

        if (preguntas_A_MisPublicaciones.getFechaRespuesta() == null) {
            layout_respusta.setVisibility(View.GONE);
        }


        id_pregunta.setText("" + preguntas_A_MisPublicaciones.getId());

        if (preguntas_A_MisPublicaciones.getListaClienete().size() > 0) {
            txt_nombre_cliente.setText("" + preguntas_A_MisPublicaciones.getListaClienete().get(0).getNombre() + " " + preguntas_A_MisPublicaciones.getListaClienete().get(0).getApellido());

        } else {
            txt_nombre_cliente.setText("");
        }

        fecha_pregunta.setText("" + preguntas_A_MisPublicaciones.getFechaPregunta());
        txt_pregunta.setText("" + preguntas_A_MisPublicaciones.getPregunta());


        if (preguntas_A_MisPublicaciones.getFechaRespuesta() != null) {
            fecha_respuesta.setText("" + preguntas_A_MisPublicaciones.getFechaRespuesta());
            txt_respuesta.setText("" + preguntas_A_MisPublicaciones.getRespuesta());
        }

        responder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sing.posicionRespuesta = position;
                Intent i = new Intent(mContext, RespuestaPreguntasPublicaciones.class);
                i.putExtra("idPregunta", preguntas_A_MisPublicaciones.getId());
                mContext.startActivity(i);
                //Toast.makeText(mContext, "id"+preguntas_A_MisPublicaciones.getId(),Toast.LENGTH_LONG).show();
            }
        });



        return itemLayout;
    }


    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
  */
    public interface AdapterCallback {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback() {


    };

}
