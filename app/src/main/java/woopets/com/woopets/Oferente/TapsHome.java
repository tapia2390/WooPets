package woopets.com.woopets.Oferente;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import woopets.com.woopets.Comandos.ComandoCategorias;
import woopets.com.woopets.Oferente.AdapterOferente.NotificacionesAdapter;
import woopets.com.woopets.Oferente.Comandos.ComandoGetListaProducto;
import woopets.com.woopets.Oferente.Comandos.ComandoNotificaciones;
import woopets.com.woopets.Oferente.Comandos.ComandoPreguntaPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.AdapterOferente.SectionsPageAdapter;
import woopets.com.woopets.Oferente.Fragment.MyGuarderia;
import woopets.com.woopets.Oferente.Fragment.MyNotificacion;
import woopets.com.woopets.Oferente.Fragment.MyPublicaciones;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class TapsHome extends AppCompatActivity implements ComandoGetListaProducto.OnComandoGetListaProductoChangeListener, ComandoNotificaciones.OnComandoNotificacionesChangeListener, ComandoCategorias.OnComandoCategoriasChangeListener, ComandoPreguntaPublicaciones.OnComandoPreguntaPublicacionesChangeListener{

    private static final String TAG = "MainActivity";
    ComandoGetListaProducto comandoGetListaProducto;
    Context context;
    TextView numero;
    View vCarrito;
    View vpublicar;
    View vGuarderia;
    String uid;

    ComandoNotificaciones comandoNotificaciones;
    ComandoCategorias comandoCategorias;
    ComandoPreguntaPublicaciones comandoPreguntaPublicaciones;


    private SectionsPageAdapter mSectionsPageAdapter;

    private ViewPager mViewPager;
    TabLayout tabLayout;

    final int[] ICONS = new int[]{
            R.drawable.btnperfilblanco,
            R.drawable.btnpublicarblanco,
            R.drawable.btnnotificacionblanco,
    };

    public LocationManager locationManager;
    double latitude, longitud;
    Modelo modelo = Modelo.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);



        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


        setContentView(R.layout.activity_taps_home);
        context = getApplicationContext();

        modelo.vistaPublicacion = 0;

        vCarrito = (FrameLayout) View.inflate(getApplicationContext(), R.layout.tab_home_item, null);
        vpublicar = (FrameLayout) View.inflate(getApplicationContext(), R.layout.tab_home_item, null);
        vGuarderia = (FrameLayout) View.inflate(getApplicationContext(), R.layout.tab_home_item, null);


    }



    //Prefences
    private void cargarSharedPreferenceUID() {

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("preferenciasUID", Context.MODE_PRIVATE);

         uid = sharedPreferences.getString("uid", "");

    }


    public void guardarSharedPreferenceUID() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("preferenciasUID", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("uid", modelo.uid);


        editor.commit();
    }



    public void setupTabIcons() {
        tabLayout.getTabAt(1).setIcon(ICONS[2]);
        tabLayout.getTabAt(1).setText("Mi tienda");
        tabLayout.getTabAt(1).setText("Publicar");
        tabLayout.getTabAt(1).setIcon(ICONS[1]);
        tabLayout.getTabAt(0).setText("Notificaciones");
        tabLayout.getTabAt(0).setIcon(ICONS[0]);


        ImageView iconoCarrito = vCarrito.findViewById(R.id.icono);
        iconoCarrito.setImageResource(R.drawable.btnnotificacionblanco);
        TextView textoCarrito = vCarrito.findViewById(R.id.titulo);
        textoCarrito.setTextSize((float) 10);
        textoCarrito.setText("Notificaciones");

        ImageView publicar = vpublicar.findViewById(R.id.icono);
        publicar.setImageResource(R.drawable.btnpublicarblanco);
        TextView textoPublicar = vpublicar.findViewById(R.id.titulo);
        textoPublicar.setTextSize((float) 10);
        textoPublicar.setText("Publicar");
        vpublicar.findViewById(R.id.layerrojo).setVisibility(View.GONE);

        ImageView guarderia = vGuarderia.findViewById(R.id.icono);
        guarderia.setImageResource(R.drawable.btnperfilblanco);
        TextView textoguarderia = vGuarderia.findViewById(R.id.titulo);
        textoguarderia.setTextSize((float) 10);
        textoguarderia.setText("Mi Tienda");
        vGuarderia.findViewById(R.id.layerrojo).setVisibility(View.GONE);


        int i = 0;
        int contador = 0;

        while (i < modelo.listaNotifications.size()) {
            if (modelo.listaNotifications.get(i).getVisto() == false) {
                contador = contador + 1;
            }
            i++;

        }

        if (contador == 0) {
            vCarrito.findViewById(R.id.layerrojo).setVisibility(View.GONE);
        } else {
            vCarrito.findViewById(R.id.layerrojo).setVisibility(View.VISIBLE);
            numero = vCarrito.findViewById(R.id.numeroNoti);
            numero.setText("" + contador);

        }

        vCarrito.findViewById(R.id.layerrojo);
        tabLayout.getTabAt(2).setCustomView(vCarrito);
        tabLayout.getTabAt(1).setCustomView(vpublicar);
        tabLayout.getTabAt(0).setCustomView(vGuarderia);


    }


    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());

        adapter.addFragment(new MyGuarderia(), "");
        adapter.addFragment(new MyPublicaciones(), "");
        adapter.addFragment(new MyNotificacion(), "");
        viewPager.setAdapter(adapter);

    }




    @Override
    public void onBackPressed() {

        modelo.vistaPublicacion =0;
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
        //this.finish();

        super.onBackPressed();
    }

    // tener guardado la latud y longitud al cargar la vista
    private void locationStart2() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        TapsHome.Localizacion local = new TapsHome.Localizacion();
        local.setMainActivity(this);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) local);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) local);

        if (gpsEnabled) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));


            try {
                latitude = Double.parseDouble("" + location.getLatitude());
                longitud = Double.parseDouble("" + location.getLongitude());
            } catch (Exception e) {
                Log.v("exeption", "exeption");
            }

        }/*else {
            showAlerGpsDesactivado();
        }*/

    }


    @Override
    public void getlistProductoOferente() {
        modelo.listaProductosOferentes.size();
    }

    @Override
    public void preguntasMisPublicaciones() {

    }

    @Override
    public void preguntasMisPublicacionesNotificacion() {

    }


    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener {
        TapsHome mainActivity;

        public TapsHome getMainActivity() {
            return mainActivity;
        }

        public void setMainActivity(TapsHome mainActivity) {
            this.mainActivity = mainActivity;
        }


        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion

            //loc.getLatitude();
            //loc.getLongitude();

            String Text = "Mi ubicacion actual es: " + "\n Lat = "
                    + loc.getLatitude() + "\n Long = " + loc.getLongitude();

            //  Toast.makeText(getApplicationContext(),""+Text,Toast.LENGTH_SHORT).show();


//metodo que  conbierte latitu y longitud en direccion

            if (loc != null) {
                // setCurrentLocation(loc);

                modelo.latitud = loc.getLatitude();
                modelo.longitud = loc.getLongitude();
            }

        }

        private void setCurrentLocation(Location loc) {
            String Text = "Mi ubicacion actual es--: " + "\n Lat = "
                    + loc.getLatitude() + "\n Long = --" + loc.getLongitude();

            //Toast.makeText(getApplicationContext(),""+Text,Toast.LENGTH_SHORT).show();


        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado
            Toast.makeText(getApplicationContext(), "GPS Desactivado", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado

            Toast.makeText(getApplicationContext(), "GPS Activo", Toast.LENGTH_SHORT).show();


        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }


    }
    // fin clas Location

    @Override
    public void cargoCategorias() {
       // Toast.makeText(getApplicationContext(), "1...", Toast.LENGTH_SHORT).show();

        Log.v("tamanolistaN", "tamanolistaN" + modelo.listaNotifications.size());
        tabLayout.setupWithViewPager(mViewPager);

        setupTabIcons();


    }

    @Override
    public void cargoTipoUsuario(String tipo) {

    }

    @Override
    public void cargoCambio() {

    }

    @Override
    public void actualizoSistema() {

    }

    @Override
    public void onStart() {
        super.onStart();
       // Toast.makeText(getApplicationContext(), "onstart tap"+modelo.listaNotifications.size(),Toast.LENGTH_SHORT).show();

        cargarSharedPreferenceUID();
        if (modelo.uid.equals("")) {
            modelo.uid = uid;
        }

        mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        //variables del horario  oferente
        modelo.horariodesemanaIni = "";
        modelo.horariodesemanaFin = "";
        modelo.horariofindesemanaIni = "";
        modelo.horariofindesemanaFin = "";
        modelo.latitud = 0.0;
        ;
        modelo.longitud = 0.0;
        modelo.sinJornadaContinuaSemana = false;
        modelo.sinJornadaContinuaFinDeSemana = false;

        //variables del horario al registrar un articulo Oferente
        modelo.diasDeLaSemanaArticulo.clear();
        modelo.finDeSemanaFestivosArticulo.clear();
        modelo.horariodesemanaIniArticulo = "";
        modelo.horariodesemanaFinArticulo = "";
        modelo.horariofindesemanaIniArticulo = "";
        modelo.horariofindesemanaFinArticulo = "";
        modelo.latitudArticulo = 0.0;
        ;
        modelo.longitudArticulo = 0.0;
        ;
        modelo.sinJornadaContinuaSemanaArticulo = false;
        modelo.sinJornadaContinuaFinDeSemanaArticulo = false;

        modelo.diasDeLaSemana.clear();
        modelo.finDeSemanaFestivos.clear();

        setupTabIcons();

        comandoGetListaProducto = new ComandoGetListaProducto(this);
        comandoGetListaProducto.getListaProducto();

        comandoNotificaciones = new ComandoNotificaciones(this);
        comandoNotificaciones.getNotificocaiones();

        comandoCategorias = new ComandoCategorias(this);
        comandoCategorias.getCategorias();

        comandoPreguntaPublicaciones = new ComandoPreguntaPublicaciones(this);
        comandoPreguntaPublicaciones.getPreguntasPublicaciones2();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                //Toast.makeText(getApplicationContext(),"onPageScrolled position"+position,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageSelected(int position) {
                //Toast.makeText(getApplicationContext(),"onPageSelected position"+position,Toast.LENGTH_SHORT).show();

                if (position == 1) {
                    modelo.vistaPublicacion =1;
                    if(modelo.vistaPublicacion == 1){
                        Intent i = new Intent(getApplicationContext(), MyPublicacionesCategorias.class);
                        startActivity(i);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //   Toast.makeText(getApplicationContext(),"state position"+state,Toast.LENGTH_SHORT).show();

            }
        });


        int i = 0;
        int contador = 0;

        while (i < modelo.listaNotifications.size()) {
            if (modelo.listaNotifications.get(i).getVisto() == false) {
                contador = contador + 1;
            }
            i++;

        }

        if (contador == 0) {
            vCarrito.findViewById(R.id.layerrojo).setVisibility(View.GONE);
        } else {
            numero = vCarrito.findViewById(R.id.numeroNoti);
            numero.setText("" + contador);
        }

        vCarrito.findViewById(R.id.layerrojo);
        tabLayout.getTabAt(2).setCustomView(vCarrito);

        tabLayout.getTabAt(1).setCustomView(vpublicar);
        tabLayout.getTabAt(0).setCustomView(vGuarderia);


    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }



}
