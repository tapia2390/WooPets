package woopets.com.woopets.Oferente.Comandos;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tactomotion on 6/01/17.
 */
public class ComandoOrdenTarjeta {

    //traer ui
    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnComandoOrdenTarjetaChangeListener {

        void destacadoEnviadoTarjeta();
        void errorEnviadoTarjeta();

    }

    //interface del listener de la actividad interesada
    private OnComandoOrdenTarjetaChangeListener mListener;

    public ComandoOrdenTarjeta(OnComandoOrdenTarjetaChangeListener mListener){

        this.mListener = mListener;

    }

    public void setCrerDestacado(final String celular, final String direccion, final String comentario, final String useuario,final  String authorizationCode, final String idPago, final  String idTarjeta, final  String paymentTransaction) {

        DatabaseReference key = referencia.push();

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("h:mm a");
        System.out.println("Hora: "+hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: "+dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: "+hourdateFormat.format(date));

        int setHora = 0;
        String string = ""+hourFormat.format(date);
        String[] parts = string.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];

        if(part2.equals("a.") || part2.equals("a.m.") || part2.equals("a. m.")|| part2.equals("A.")){
            part2 = "AM";
        }
        if(part2.equals("p.") ||  part2.equals("p.m.") ||  part2.equals("p. m.") || part2.equals("P.")){
            part2 = "PM";
        }


        DatabaseReference ref = database.getReference("pagosOferente/"+key.getKey()+"/");//ruta path

        Map<String, Object> enviarRegistro = new HashMap<String, Object>();

        enviarRegistro.put("fechaPago", dateFormat.format(date)+" "+part1+" "+part2);
        enviarRegistro.put("Dispositivo", "Android");
        enviarRegistro.put("Descripcion", "Destacado");
        enviarRegistro.put("authorizationCode", ""+authorizationCode);
        enviarRegistro.put("idPago", ""+idPago);
        enviarRegistro.put("idTarjeta", ""+idTarjeta);
        enviarRegistro.put("metodoPago", "Tarjeta");
        enviarRegistro.put("paymentTransaction", ""+paymentTransaction);
        enviarRegistro.put("idPublicacion", ""+ modelo.uidProducto);
        enviarRegistro.put("timestamp", ServerValue.TIMESTAMP);


        DatabaseReference ref2 = database.getReference("imagenes/"+modelo.productosOferentes.get(0).getId()+"/destacado/");   //ruta path
        ref2.setValue(1);


        ref.setValue(enviarRegistro, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {

                    mListener.errorEnviadoTarjeta();
                } else {
                    mListener.destacadoEnviadoTarjeta();
                }
            }
        });

    }




    public void setCoboro(final  String authorizationCode, final String idPago, final  String idTarjeta, final  String paymentTransaction,  final  String  idCompra) {

        DatabaseReference key = referencia.push();

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("hh:mm a");
        System.out.println("Hora: "+hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: "+dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: "+hourdateFormat.format(date));



        int setHora = 0;
        String string = ""+hourFormat.format(date);
        String[] parts = string.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];

        if(part2.equals("a.") || part2.equals("a.m.") || part2.equals("a. m.")|| part2.equals("A.")){
            part2 = "AM";
        }
        if(part2.equals("p.") ||  part2.equals("p.m.") ||  part2.equals("p. m.") || part2.equals("P.")){
            part2 = "PM";
        }

        DatabaseReference ref = database.getReference("pagosCliente/"+key.getKey()+"/");//ruta path

        Map<String, Object> enviarRegistro = new HashMap<String, Object>();

        enviarRegistro.put("fechaPago", dateFormat.format(date)+" "+part1+" "+part2);
        enviarRegistro.put("Dispositivo", "Android");
        enviarRegistro.put("Descripcion", "Compra Servicio" );
        enviarRegistro.put("authorizationCode", ""+authorizationCode);
        enviarRegistro.put("idPago", ""+idPago);
        enviarRegistro.put("idTarjeta", ""+idTarjeta);
        enviarRegistro.put("metodoPago", "Tarjeta");
        enviarRegistro.put("paymentTransaction", ""+paymentTransaction);
        enviarRegistro.put("idCompra", ""+idCompra);
        enviarRegistro.put("timestamp", ServerValue.TIMESTAMP);


        ref.setValue(enviarRegistro, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {

                    mListener.errorEnviadoTarjeta();
                } else {
                    mListener.destacadoEnviadoTarjeta();
                }
            }
        });

    }

    /**
         * Para evitar nullpointerExeptions
         */
        private static OnComandoOrdenTarjetaChangeListener sDummyCallbacks = new OnComandoOrdenTarjetaChangeListener()
        {
            @Override
            public void destacadoEnviadoTarjeta()
            {}

            @Override
            public void errorEnviadoTarjeta()
            {}



        };

}
