package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Tarjetas;

/**
 * Created by tacto on 10/08/17.
 */

public class ComandoTpaga {

    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnTpagaChangeListener {


        void cargoCrediCard();
        void  cargoIdClienteTpaga();
        void cargoActualizarActualizarEstadoCard();
        void cargoCrediCardFalse();
        void cargoCrediCardFalse2();
        void cargoActualizarActualizarEstadoCard2();
        void  cargoCrediCard2();
    }


    //interface del listener de la actividad interesada
    private OnTpagaChangeListener mListener;

    public ComandoTpaga(OnTpagaChangeListener mListener){

        this.mListener = mListener;

    }

    //metodo  que trae las tarjetas registradas

   /*
    public void getCrediCard(){
        DatabaseReference ref = database.getReference("oferentes/"+modelo.uid+"/tarjetas/");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                modelo.tarjeta.clear();
                if(snap.getValue() != null) {
                    for (DataSnapshot tarjeta : snap.getChildren()) {
                        Tarjetas tarjetas  = new Tarjetas();
                        tarjetas.setKeyId(tarjeta.getKey());
                        boolean activo =  (boolean)tarjeta.child("activo").getValue();
                        tarjetas.setActivo(activo);
                        tarjetas.setCuotas(((Long) tarjeta.child("cuotas").getValue()).intValue());
                        tarjetas.setFranquicia(tarjeta.child("franquicia").getValue().toString());
                        tarjetas.setLastFour(tarjeta.child("lastFour").getValue().toString());
                        tarjetas.setToken(tarjeta.child("token").getValue().toString());

                        modelo.tarjeta.add(tarjetas);
                    }
                    mListener.cargoCrediCard();
                }else{
                    mListener.cargoCrediCard();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }*/


    public void getIdClienteTpaga(){
        DatabaseReference ref = database.getReference("oferentes/"+modelo.uid+"/idClienteTpaga/");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelo.idClienteTpaga ="";
                if(snap.getValue() != null) {
                    modelo.idClienteTpaga = snap.getValue().toString();
                    mListener.cargoIdClienteTpaga();
                }else {
                    mListener.cargoIdClienteTpaga();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

/*
    public void updateCArdAllfalse(){

        DatabaseReference ref = database.getReference("oferentes/"+modelo.uid+"/tarjetas/");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelo.tarjeta.clear();
                for (DataSnapshot tarjeta : snap.getChildren()) {
                    Tarjetas tarjetas  = new Tarjetas();
                    tarjetas.setKeyId(tarjeta.getKey());
                    DatabaseReference ref2 = database.getReference("oferentes/"+modelo.uid+"/tarjetas/"+tarjeta.getKey()+"/activo/");
                    ref2.setValue(false);
                    boolean activo =  (boolean)tarjeta.child("activo").getValue();
                    tarjetas.setActivo(false);
                    tarjetas.setCuotas(((Long) tarjeta.child("cuotas").getValue()).intValue());
                    tarjetas.setFranquicia(tarjeta.child("franquicia").getValue().toString());
                    tarjetas.setLastFour(tarjeta.child("lastFour").getValue().toString());
                    tarjetas.setToken(tarjeta.child("token").getValue().toString());

                    modelo.tarjeta.add(tarjetas);
                }

                mListener.cargoCrediCardFalse();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }




    public void updateCArdTrue(final String keyCard){

        final DatabaseReference ref3 = database.getReference("oferentes/"+modelo.uid+"/tarjetas/"+keyCard+"/activo/");
        ref3.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                ref3.setValue(true);
                mListener.cargoActualizarActualizarEstadoCard();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


*/

/*

    public void updateCArdAllfalse2(){

        DatabaseReference ref = database.getReference("oferentes/"+modelo.uid+"/tarjetas/");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelo.tarjeta.clear();
                for (DataSnapshot tarjeta : snap.getChildren()) {
                    Tarjetas tarjetas  = new Tarjetas();
                    tarjetas.setKeyId(tarjeta.getKey());
                    DatabaseReference ref2 = database.getReference("clientes/"+modelo.uid+"/tarjetas/"+tarjeta.getKey()+"/activo/");
                    ref2.setValue(false);
                    boolean activo =  (boolean)tarjeta.child("activo").getValue();
                    tarjetas.setActivo(false);
                    tarjetas.setCuotas(((Long) tarjeta.child("cuotas").getValue()).intValue());
                    tarjetas.setFranquicia(tarjeta.child("franquicia").getValue().toString());
                    tarjetas.setLastFour(tarjeta.child("lastFour").getValue().toString());
                    tarjetas.setToken(tarjeta.child("token").getValue().toString());

                    modelo.tarjeta.add(tarjetas);
                }

                mListener.cargoCrediCardFalse2();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }



    public void updateCArdTrue2(final String keyCard){

        final DatabaseReference ref3 = database.getReference("oferentes/"+modelo.uid+"/tarjetas/"+keyCard+"/activo/");
        ref3.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                ref3.setValue(true);
                mListener.cargoActualizarActualizarEstadoCard2();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void getCrediCard2(){
        DatabaseReference ref = database.getReference("oferentes/"+modelo.uid+"/tarjetas/");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                modelo.tarjeta.clear();
                if(snap.getValue() != null) {

                    for (DataSnapshot tarjeta : snap.getChildren()) {
                        Tarjetas tarjetas  = new Tarjetas();
                        tarjetas.setKeyId(tarjeta.getKey());
                        boolean activo =  (boolean)tarjeta.child("activo").getValue();
                        tarjetas.setActivo(activo);
                        tarjetas.setCuotas(((Long) tarjeta.child("cuotas").getValue()).intValue());
                        tarjetas.setFranquicia(tarjeta.child("franquicia").getValue().toString());
                        tarjetas.setLastFour(tarjeta.child("lastFour").getValue().toString());
                        tarjetas.setToken(tarjeta.child("token").getValue().toString());

                        modelo.tarjeta.add(tarjetas);
                    }
                    mListener.cargoCrediCard2();
                }else{
                    mListener.cargoCrediCard2();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnTpagaChangeListener sDummyCallbacks = new OnTpagaChangeListener()
    {


        public void cargoCrediCard()
        {}

        public void cargoCrediCard2()
        {}

        public void cargoIdClienteTpaga()
        {}


        public void cargoActualizarActualizarEstadoCard()
        {}
        public void cargoCrediCardFalse()
        {}

        public void cargoCrediCardFalse2()
        {}

        public void cargoActualizarActualizarEstadoCard2()
        {}
    };
}
