package woopets.com.woopets.Oferente.Comandos;


import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.Chat;
import woopets.com.woopets.Modelo.cliente.Mensaje;
import woopets.com.woopets.Oferente.ModelOferente.ChatOferente;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.cliente.ModelCliente;

/**
 * Created by andres on 10/25/17.
 */

public class ComandoChatOferente {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    public interface OnComandoChatListener {

        void getChasts();

        void setChasts();

    }


    private ComandoChatOferente.OnComandoChatListener mListener;

    public ComandoChatOferente(OnComandoChatListener mListener) {

        this.mListener = mListener;
    }

    public void ponerChat(final String msmChat) {

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("h:mm a");
        System.out.println("Hora: " + hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: " + dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: " + hourdateFormat.format(date));


        String string = "" + hourFormat.format(date);
        String[] parts = string.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];

        if (part2.equals("a.") || part2.equals("a.m.") || part2.equals("a. m.") || part2.equals("A.")) {
            part2 = "AM";
        }
        if (part2.equals("p.") || part2.equals("p.m.") || part2.equals("p. m.") || part2.equals("P.")) {
            part2 = "PM";
        }

        DatabaseReference key = referencia.push();
        DatabaseReference ref = database.getReference("chats/" + key.getKey() + "/");

        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("emisor", modelo.uid);
        mapa.put("fechaMensaje", "" + dateFormat.format(date) + " " + part1 + " " + part2);
        mapa.put("idCompra", modelo.idCompara);
        mapa.put("mensaje", msmChat);
        mapa.put("receptor", modelo.idCliente);
        mapa.put("timestamp", ServerValue.TIMESTAMP);
        mapa.put("visto", false);
        ref.updateChildren(mapa);
        mListener.setChasts();


    }


    public void enviarPushNotification(final String msg) {

        DatabaseReference key = referencia.push();
        DatabaseReference ref = database.getReference("mensajes/" + key.getKey() + "/");

        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("idCliente", modelo.idCliente);
        mapa.put("idCompra", modelo.idCompara);
        mapa.put("infoAdicional", msg);
        mapa.put("mensaje", "Tienes un mensaje en el chat de tu compra");
        mapa.put("timestamp", ServerValue.TIMESTAMP);
        mapa.put("tipo", "chat");
        mapa.put("titulo", "Nuevo mensaje");
        String token = modelo.token;
        if (token == null || token.equals("")) {
            token = "1";
        }

        Map<String, Object> tokens = new HashMap<String, Object>();
        tokens.put(token, true);
        mapa.put("tokens", tokens);

        mapa.put("visto", false);
        ref.updateChildren(mapa);


    }


    public void getChatOferente(final String idPublicacion, final String idClienete) {

        modelo.chatOferentes.clear();

        DatabaseReference ref = database.getReference("chats/");//ruta path
        Query query = ref.orderByChild("idCompra").equalTo(idPublicacion);

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {


                if (!snap.exists()) {
                    mListener.getChasts();
                } else {

                    String emisor = snap.child("emisor").getValue().toString();


                    if(idClienete.equals("")){
                        ChatOferente chat = new ChatOferente();
                        chat.setId(snap.getKey());
                        chat.setEmisor(snap.child("emisor").getValue().toString());
                        chat.setFechaMensaje(snap.child("fechaMensaje").getValue().toString());
                        chat.setIdCompra(snap.child("idCompra").getValue().toString());
                        chat.setMensaje(snap.child("mensaje").getValue().toString());
                        chat.setReceptor(snap.child("receptor").getValue().toString());
                        chat.setTimestamp(((Long) snap.child("timestamp").getValue()).intValue());
                        boolean servicio = (boolean) snap.child("visto").getValue();
                        chat.setVisto(servicio);

                        modelo.addChat(chat);
                    }

                    if (emisor.equals(idClienete) || emisor.equals(modelo.uid)) {

                        ChatOferente chat = new ChatOferente();
                        chat.setId(snap.getKey());
                        chat.setEmisor(snap.child("emisor").getValue().toString());
                        chat.setFechaMensaje(snap.child("fechaMensaje").getValue().toString());
                        chat.setIdCompra(snap.child("idCompra").getValue().toString());
                        chat.setMensaje(snap.child("mensaje").getValue().toString());
                        chat.setReceptor(snap.child("receptor").getValue().toString());
                        chat.setTimestamp(((Long) snap.child("timestamp").getValue()).intValue());
                        boolean servicio = (boolean) snap.child("visto").getValue();
                        chat.setVisto(servicio);

                        modelo.addChat(chat);
                    }
                    

                }

                mListener.getChasts();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
        };

        query.addChildEventListener(listener);
        modelo.hijosListener.put(query, listener);

    }



    public void getChatOferente2(final String idPublicacion, final String idClienete) {

        modelo.chatOferentes.clear();

        DatabaseReference ref = database.getReference("chats/");//ruta path
        Query query = ref.orderByChild("idCompra").equalTo(idPublicacion);

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {

                if (!snap.exists()) {
                    mListener.getChasts();
                } else {

                    String emisor = snap.child("emisor").getValue().toString();


                    if(idClienete.equals("")){
                        ChatOferente chat = new ChatOferente();
                        chat.setId(snap.getKey());
                        chat.setEmisor(snap.child("emisor").getValue().toString());
                        chat.setFechaMensaje(snap.child("fechaMensaje").getValue().toString());
                        chat.setIdCompra(snap.child("idCompra").getValue().toString());
                        chat.setMensaje(snap.child("mensaje").getValue().toString());
                        chat.setReceptor(snap.child("receptor").getValue().toString());
                        chat.setTimestamp(((Long) snap.child("timestamp").getValue()).intValue());
                        boolean servicio = (boolean) snap.child("visto").getValue();
                        chat.setVisto(servicio);

                        if(servicio == false && !emisor.equals(modelo.uid) ){
                            chatVisto(snap.getKey());
                        }

                        modelo.addChat(chat);
                    }

                    if (emisor.equals(idClienete) || emisor.equals(modelo.uid)) {

                        ChatOferente chat = new ChatOferente();
                        chat.setId(snap.getKey());
                        chat.setEmisor(snap.child("emisor").getValue().toString());
                        chat.setFechaMensaje(snap.child("fechaMensaje").getValue().toString());
                        chat.setIdCompra(snap.child("idCompra").getValue().toString());
                        chat.setMensaje(snap.child("mensaje").getValue().toString());
                        chat.setReceptor(snap.child("receptor").getValue().toString());
                        chat.setTimestamp(((Long) snap.child("timestamp").getValue()).intValue());
                        boolean servicio = (boolean) snap.child("visto").getValue();
                        chat.setVisto(servicio);

                        if(servicio == false && !emisor.equals(modelo.uid) ){
                            chatVisto(snap.getKey());
                        }
                        modelo.addChat(chat);
                    }


                }

                mListener.getChasts();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
        };

        query.addChildEventListener(listener);
        modelo.hijosListener.put(query, listener);

    }



    public void chatVisto(String id){

        final DatabaseReference ref = database.getReference("chats/"+id+"/");
        Map<String, Object> dotosLeidos = new HashMap<String, Object>();
        dotosLeidos.put("visto",true);
        ref.updateChildren(dotosLeidos);
    }

    public static ComandoChatOferente.OnComandoChatListener dummy = new ComandoChatOferente.OnComandoChatListener() {


        @Override
        public void getChasts() {
        }

        @Override
        public void setChasts() {
        }

    };


}







