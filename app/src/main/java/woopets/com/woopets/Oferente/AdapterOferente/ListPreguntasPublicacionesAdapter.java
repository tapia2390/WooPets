package woopets.com.woopets.Oferente.AdapterOferente;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.PreguntasPublicaciones;
import woopets.com.woopets.Oferente.PreguntasFrecuentes;
import woopets.com.woopets.R;

/**
 * Created by tactomotion on 26/12/16.
 */
public class ListPreguntasPublicacionesAdapter extends BaseAdapter {

    //Atributos del adaptador
    private final Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<PreguntasPublicaciones> preguntasPublicaciones;
    private Modelo sing = Modelo.getInstance();

    int posicionadapter;

    TextView contadoradapter;


    public ListPreguntasPublicacionesAdapter(Context mContext, AdapterCallback mListener) {

        this.mContext = mContext;
        this.mListener = mListener;




        if (sing.preguntasPublicaciones.size() > 0) {

            HashMap<String, PreguntasPublicaciones> map = new HashMap<String, PreguntasPublicaciones>();

            for (PreguntasPublicaciones compraProducto : sing.preguntasPublicaciones) {
                int i = 0;
                map.put(compraProducto.getTexto(), compraProducto);
                i++;
            }

            sing.preguntasPublicaciones.clear();

            Set<Map.Entry<String, PreguntasPublicaciones>> set = map.entrySet();
            for (Map.Entry<String, PreguntasPublicaciones> entry : set) {
                sing.preguntasPublicaciones.add(entry.getValue());
            }
        }




        this.preguntasPublicaciones = sing.preguntasPublicaciones;

    }

    @Override
    public int getCount() {
        return preguntasPublicaciones.size();
    }

    @Override
    public Object getItem(int position) {
        return preguntasPublicaciones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // Declare Variables
        final TextView txtPregunta;
        final Button btn_flecha;


        final PreguntasPublicaciones listpreguntasPublicaciones = (PreguntasPublicaciones) getItem(position);


        // Inflate la vista de la Orden
        RelativeLayout itemLayout =  (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.adapter_preguntas_publicaciones, parent, false);

        // Toma los elementos a utilizar
        txtPregunta = (TextView) itemLayout.findViewById(R.id.txtPregunta);
        btn_flecha = (Button) itemLayout.findViewById(R.id.btn_flecha);
        txtPregunta.setText(sing.preguntasPublicaciones.get(position).getTexto());


        return itemLayout;

    }


    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
   */
    public interface AdapterCallback {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback() {


    };
}
