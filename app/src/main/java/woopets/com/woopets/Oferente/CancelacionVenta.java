package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import woopets.com.woopets.Oferente.Comandos.ComandoCancelarVenta;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class CancelacionVenta extends Activity implements ComandoCancelarVenta.OnComandoCancelarVentaoChangeListener{

    //int posicion = 0;
    String idCompra;
    Modelo modelo = Modelo.getInstance();
    Button bnt_op1;
    Button bnt_op2;
    Button bnt_op3;
    Button bnt_op4;
    EditText edittext_texto;
    String texto = "";
    ComandoCancelarVenta comandoCancelarVenta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cancelacion_venta);

        Bundle bundle2 = getIntent().getExtras();
        idCompra = bundle2.getString("IDCOMPRA");

        bnt_op1 = (Button)findViewById(R.id.bnt_op1);
        bnt_op2 = (Button)findViewById(R.id.bnt_op2);
        bnt_op3 = (Button)findViewById(R.id.bnt_op3);
        bnt_op4 = (Button)findViewById(R.id.bnt_op4);
        edittext_texto = (EditText) findViewById(R.id.edittext_texto);
        comandoCancelarVenta = new ComandoCancelarVenta(this);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

    }



    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void btn1(View v){
        bnt_op1.setBackgroundResource(R.drawable.verde_oscuro_post_border_style);
        bnt_op2.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        bnt_op3.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        bnt_op4.setBackgroundResource(R.drawable.gris_claro_post_border_style);

        bnt_op1.setTextColor(Color.WHITE);
        bnt_op2.setTextColor(Color.BLACK);
        bnt_op3.setTextColor(Color.BLACK);
        bnt_op4.setTextColor(Color.BLACK);

        texto = bnt_op1.getText().toString();

    }

    public void btn2(View v){
        bnt_op1.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        bnt_op2.setBackgroundResource(R.drawable.verde_oscuro_post_border_style);
        bnt_op3.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        bnt_op4.setBackgroundResource(R.drawable.gris_claro_post_border_style);

        bnt_op1.setTextColor(Color.BLACK);
        bnt_op2.setTextColor(Color.WHITE);
        bnt_op3.setTextColor(Color.BLACK);
        bnt_op4.setTextColor(Color.BLACK);


        texto = bnt_op2.getText().toString();
    }

    public void btn3(View v){
        bnt_op1.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        bnt_op2.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        bnt_op3.setBackgroundResource(R.drawable.verde_oscuro_post_border_style);
        bnt_op4.setBackgroundResource(R.drawable.gris_claro_post_border_style);

        bnt_op1.setTextColor(Color.BLACK);
        bnt_op2.setTextColor(Color.BLACK);
        bnt_op3.setTextColor(Color.WHITE);
        bnt_op4.setTextColor(Color.BLACK);


        texto = bnt_op3.getText().toString();

    }

    public void btn4(View v){
        bnt_op1.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        bnt_op2.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        bnt_op3.setBackgroundResource(R.drawable.gris_claro_post_border_style);
        bnt_op4.setBackgroundResource(R.drawable.verde_oscuro_post_border_style);

        bnt_op1.setTextColor(Color.BLACK);
        bnt_op2.setTextColor(Color.BLACK);
        bnt_op3.setTextColor(Color.BLACK);
        bnt_op4.setTextColor(Color.WHITE);

        edittext_texto.setVisibility(View.VISIBLE);
        texto = edittext_texto.getText().toString();

    }


    public void enviar(View v){

        if(edittext_texto.getText().toString().equals("") || texto.equals("")){
            edittext_texto.setError("Campo obligatorio");
        }else {
            comandoCancelarVenta.setCancelarVenta(idCompra);
            Toast.makeText(getApplicationContext(),"Enviado...",Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void ventaCancelada() {
        Toast.makeText(getApplicationContext(),"Enviado...",Toast.LENGTH_LONG).show();
        comandoCancelarVenta.actualizarEstadoVenta(idCompra, texto, edittext_texto.getText().toString());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }


}
