package woopets.com.woopets.Oferente.AdapterOferente;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.List;

import woopets.com.woopets.Oferente.InformacionProductoServicioOferente;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.Oferente.MyPublicacionesFoto;
import woopets.com.woopets.R;

/**
 * Created by tacto on 23/08/17.
 */

public class MisPublicacioneAdapterInactivas extends BaseAdapter {

    //Atributos del adaptador
    final Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<ProductosOferente> productosOferente;
    private Modelo sing = Modelo.getInstance();
    String imegenPortada = "";


    public MisPublicacioneAdapterInactivas(Context mContext, AdapterCallback mListener){

        this.mContext = mContext;
        this.mListener = mListener;
        this.productosOferente = sing.productosOferentes2;

    }

    @Override
    public int getCount() {
        return productosOferente.size();
    }

    @Override
    public Object getItem(int position) {
        return productosOferente.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ImageView list_img;
        final Button list_img_cantidad;
        final ImageView list_img_favorito;
        final TextView list_valor_producto;
        final TextView list_nombre_producto;
        final  String foto;
        final  RelativeLayout contenedor;
        final ImageView imageView4;


        final ProductosOferente productosOferentes =  (ProductosOferente) getItem(position);

        // Inflate la vista de la productosOferentes
        RelativeLayout itemLayout =  (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.adapter_list_publicaciones2, parent, false);


        list_img = (ImageView) itemLayout.findViewById(R.id.list_img);
        list_img_cantidad = (Button) itemLayout.findViewById(R.id.list_img_cantidad);
        list_img_favorito = (ImageView) itemLayout.findViewById(R.id.list_img_favorito);
        list_valor_producto = (TextView) itemLayout.findViewById(R.id.list_valor_producto);
        list_nombre_producto = (TextView) itemLayout.findViewById(R.id.list_nombre_producto);
        contenedor = (RelativeLayout) itemLayout.findViewById(R.id.contenedor);
        imageView4 = (ImageView) itemLayout.findViewById(R.id.imageView4);



            if(productosOferentes.getDestacado() == true){
                list_img_favorito.setBackgroundResource(R.drawable.destacado_ok);
            }
            foto = productosOferentes.getFotos().get(0);

            //descargar imagenes firebase

            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference().child("productos/"+productosOferentes.getId()+"/"+foto);

            //get download file url
            storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Log.i("Main", "File uri: " + uri.toString());
                }
            });


            //download the file
            try {
                final File localFile = File.createTempFile("images", "jpg");
                storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                        list_img.setImageBitmap(bitmap);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        int resID2 = mContext.getResources().getIdentifier(foto, "drawable",  mContext.getPackageName()); // This will return an integer value stating the id for the image you want.

                        if(resID2 == 0){
                            // Toast.makeText(mContext, "Error al cargar la imagen", Toast.LENGTH_SHORT).show();
                            list_img.setBackgroundResource(R.drawable.btnfoto);

                        }else{
                            list_img.setBackgroundResource(resID2);
                        }


                    }
                });
            } catch (IOException e ) {
                e.printStackTrace();
                Log.e("Main", "IOE Exception");
            }



            list_img.setImageDrawable(CargarImagen(foto));


            list_valor_producto.setText(""+ Utility.convertToMoney(productosOferentes.getPrecio()));
            list_nombre_producto.setText(productosOferentes.getNombre());

            //gs://mypetpruebas-6e090.appspot.com/ productos/id/

            contenedor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    sing.pro_uidProducto = productosOferentes.getId();
                    sing.pro_posicionServicio = position;
                    sing.pro_abiertascerrada = "cerradas";

                    //Toast.makeText(mContext, "id"+productosOferentes.getId(), Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(mContext,InformacionProductoServicioOferente.class);
                    mContext.startActivity(i);
                }
            });





        return itemLayout;
    }


    public Drawable CargarImagen(String foto){

        try{
            String nombre = foto;
            nombre = sing.eliminarTildes(nombre.toLowerCase()).replaceAll("\\s","");
            Resources res = mContext.getResources();


            String mDrawableName = nombre;
            int resID = res.getIdentifier(mDrawableName , "drawable", mContext.getPackageName());

            if(resID !=0){
                return ContextCompat.getDrawable(mContext, resID);
            }
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public Drawable CargarImagenCirculo(){
        String nombre = "ingreseleccionado";
        Resources res = mContext.getResources();
        int resID = res.getIdentifier(nombre , "drawable", mContext.getPackageName());


        return ContextCompat.getDrawable(mContext, resID);

    }






    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
  */
    public interface AdapterCallback
    {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback()
    {


    };

}
