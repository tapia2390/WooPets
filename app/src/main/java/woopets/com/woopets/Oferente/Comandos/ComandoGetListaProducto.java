package woopets.com.woopets.Oferente.Comandos;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;

/**
 * Created by tacto on 23/08/17.
 */

public class ComandoGetListaProducto {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo = Modelo.getInstance();

    public interface OnComandoGetListaProductoChangeListener {

        void getlistProductoOferente();
    }

    //interface del listener de la actividad interesada
    private OnComandoGetListaProductoChangeListener mListener;

    public ComandoGetListaProducto(OnComandoGetListaProductoChangeListener mListener) {

        this.mListener = mListener;
    }


    public void getListaProducto() {
        modelo.listaProductosOferentes.clear();
        DatabaseReference ref = database.getReference("productos/");//ruta path
        Query query = ref.orderByChild("idOferente").equalTo(modelo.uid);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                ProductosOferente productosOferente = new ProductosOferente();

                if (snap.exists()) {
                    for (DataSnapshot snListaProductos : snap.getChildren()) {

                        productosOferente.setId(snListaProductos.getKey());
                        boolean activo = (boolean) snListaProductos.child("activo").getValue();

                        productosOferente.setActivo(activo);
                        productosOferente.setCategoria(snListaProductos.child("categoria").getValue().toString());


                        productosOferente.setDescripcion(snListaProductos.child("descripcion").getValue().toString());

                        if (snListaProductos.child("destacado").getValue() != null) {
                            boolean destacado = (boolean) snListaProductos.child("destacado").getValue();
                            productosOferente.setDestacado(destacado);
                        }

                        if(snListaProductos.child("fechaCreacion").getValue() != null){
                            productosOferente.setFechaCreacion(snListaProductos.child("fechaCreacion").getValue().toString());
                        }

                        boolean servicio = (boolean) snListaProductos.child("servicio").getValue();

                        if (servicio == true) {
                            //nuevo arbol
                            DataSnapshot snaphorario;
                            snaphorario = (DataSnapshot) snListaProductos.child("horario");

                            //nuevo subarbol
                            DataSnapshot snapFinDeSemana;
                            snapFinDeSemana = (DataSnapshot) snaphorario.child("FinDeSemana");

                            if (snapFinDeSemana.child("dias").getValue() != null) {

                                productosOferente.setHorarioDiasFinDeSemana(snapFinDeSemana.child("dias").getValue().toString());
                                productosOferente.setHorarioCierreDiasFinDeSemana(snapFinDeSemana.child("horaCierre").getValue().toString());
                                productosOferente.setHorarioInicioDiasFinDeSemana(snapFinDeSemana.child("horaInicio").getValue().toString());

                            }


                            if (snapFinDeSemana.child("sinJornadaContinua").getValue() != null) {
                                boolean sinJornadaContinuaFinSemana = (boolean) snapFinDeSemana.child("sinJornadaContinua").getValue();
                                productosOferente.setSinJornadaContinuaFinDeSemana(sinJornadaContinuaFinSemana);

                            }


                            //fin subarbol
                            //nuevo subarbol
                            DataSnapshot snapSemana;
                            snapSemana = (DataSnapshot) snaphorario.child("Semana");
                            if (snapSemana.child("dias").getValue() != null) {
                                productosOferente.setHorarioDiasDeLaSemana(snapSemana.child("dias").getValue().toString());
                                productosOferente.setHorarioCierreDiasDeLaSemana(snapSemana.child("horaCierre").getValue().toString());
                                productosOferente.setHorarioInicioDiasDeLaSemana(snapSemana.child("horaInicio").getValue().toString());

                            }

                            if (snapSemana.child("sinJornadaContinua").getValue() != null) {
                                boolean sinJornadaContinuaSemana = (boolean) snapSemana.child("sinJornadaContinua").getValue();
                                productosOferente.setSinJornadaContinuaSemana(sinJornadaContinuaSemana);
                            }

                            //fin subarbol
                            //fin arbol

                            productosOferente.setDuracion(((Long) snListaProductos.child("duracion").getValue()).intValue());
                            productosOferente.setDuracionMedida(snListaProductos.child("duracionMedida").getValue().toString());

                            boolean servicioDomicilio = (boolean) snListaProductos.child("servicioEnDomicilio").getValue();
                            productosOferente.setServicioEnDomicilio(servicioDomicilio);


                        }

                        if (servicio == false) {
                            if (snListaProductos.child("subcategoria").getValue() != null) {
                                productosOferente.setSubcategoria(snListaProductos.child("subcategoria").getValue().toString());

                            }
                            if(snListaProductos.hasChild("stock")){
                                productosOferente.setCantidad(((Long) snListaProductos.child("stock").getValue()).intValue());

                            }

                        }
                        productosOferente.setServicio(servicio);

                        //nuevo arbol
                        DataSnapshot snapFotos;
                        snapFotos = (DataSnapshot) snListaProductos.child("fotos/");
                        ArrayList<String> fotosProductosOferentes = new ArrayList<String>();
                        for (DataSnapshot foto : snapFotos.getChildren()) {
                            fotosProductosOferentes.add(foto.getValue().toString());
                        }

                        productosOferente.setFotos(fotosProductosOferentes);

                        productosOferente.setIdOferente(snListaProductos.child("idOferente").getValue().toString());
                        productosOferente.setTitulo(snListaProductos.child("nombre").getValue().toString());
                        productosOferente.setPrecio(snListaProductos.child("precio").getValue().toString());
                        productosOferente.setTarget(snListaProductos.child("target").getValue().toString());
                        if(snListaProductos.child("timestamp").getValue() != null){
                            productosOferente.setTimestamp(((Long)snListaProductos.child("timestamp").getValue()).intValue());
                        }


                        modelo.listaProductosOferentes.add(productosOferente);
                    }
                }

                mListener.getlistProductoOferente();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoGetListaProductoChangeListener sDummyCallbacks = new OnComandoGetListaProductoChangeListener() {


        @Override
        public void getlistProductoOferente() {
        }

    };
}
