package woopets.com.woopets.Oferente.Fragment;

/**
 * Created by tacto on 3/07/17.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import woopets.com.woopets.Oferente.AdapterOferente.NotificacionesAdapter;
import woopets.com.woopets.Oferente.Comandos.ComandoNotificaciones;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.TapsHome;
import woopets.com.woopets.R;

public class MyNotificacion extends Fragment implements NotificacionesAdapter.AdapterCallback, ComandoNotificaciones.OnComandoNotificacionesChangeListener {


    private static final String TAG = "Tab1Fragment";
    NotificacionesAdapter mAdapter;
    ComandoNotificaciones comandoNotificaciones;


    private Modelo modelo = Modelo.getInstance();
    private ListView lista_pregunta_publicaciones;
    private RelativeLayout relativeLayout_noti;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mynotificaciones, container, false);
        //Toast.makeText(getActivity(), "TESTING BUTTON CLICK 2",Toast.LENGTH_SHORT).show();

        lista_pregunta_publicaciones = (ListView) view.findViewById(R.id.lista_pregunta_publicaciones);
        relativeLayout_noti = (RelativeLayout) view.findViewById(R.id.relativeLayout_noti);

        comandoNotificaciones = new ComandoNotificaciones(this);
        if (modelo.listaNotifications.size() > 0) {
            relativeLayout_noti.setVisibility(View.GONE);
            mAdapter = new NotificacionesAdapter(MyNotificacion.this,getActivity(), this);
            lista_pregunta_publicaciones.setAdapter(mAdapter);
            Log.v("rol", "rol" + " " + modelo.listaNotifications.size());
        }

        return view;
    }


    //atras hadware
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            Log.v("cancel", "cancel");

        }
        return true;
    }

    @Override
    public void cargoCategorias() {
      //  Toast.makeText(getActivity(),"frag",Toast.LENGTH_SHORT).show();


        refres();
    }

    public void refres() {

        Log.v("tamanolistaN", "tamanolistaN" + modelo.listaNotifications.size());
        if (modelo.listaNotifications.size() > 0) {
            relativeLayout_noti.setVisibility(View.GONE);
            mAdapter = new NotificacionesAdapter(MyNotificacion.this, getActivity(), this);
            lista_pregunta_publicaciones.setAdapter(mAdapter);
            Log.v("rol", "rol" + " " + modelo.listaNotifications.size());

        }



    }


    public  void refres2(){

        modelo.listaNotifications.clear();
        comandoNotificaciones.getNotificocaiones();
       // mAdapter.notifyDataSetChanged();


    }


    @Override
    public void cargoTipoUsuario(String tipo) {

    }

    @Override
    public void cargoCambio() {

    }


    @Override
    public void onStart() {
        super.onStart();
        modelo.vista = 0;
        comandoNotificaciones.getNotificocaiones();
    }
}
