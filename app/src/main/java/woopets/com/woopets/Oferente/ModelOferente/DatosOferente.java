package woopets.com.woopets.Oferente.ModelOferente;

/**
 * Created by tacto on 22/08/17.
 */

public class DatosOferente {

    String razonSocial = "";
    String nit = "";
    String direccion = "";
    public String ciudad = "";
    public String departamento = "";

    String telefonoFijo = "";
    String celular = "";
    String correoElectronico = "";
    String correoElectronicoContacto = "";
    String paginaWe = "";
    String horarioDiasDeLaSemana = "";
    String horarioCierreDiasDeLaSemana = "";
    String horarioInicioDiasDeLaSemana = "";
    String horarioDiasFinDeSemana = "";
    String horarioCierreDiasFinDeSemana = "";
    String horarioInicioDiasFinDeSemana = "";
    String nombreContacto = "";
    String tipodeDocumentoContacto = "";
    String numeroDeDocumentoCntacto = "";
    String telefonoFijoContacto = "";
    String celularContacto = "";
    double latitud = 0;
    double longitud = 0;
    boolean sinJornadaContinuaSemana = false;
    boolean sinJornadaContinuaFinDeSemana = false;
    String aprobacionMyPet = "";

    DatosTpaga datosTpaga = new DatosTpaga();
    Tarjetas tarjetas = new Tarjetas();


    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getPaginaWe() {
        return paginaWe;
    }

    public void setPaginaWe(String paginaWe) {
        this.paginaWe = paginaWe;
    }


    public String getHorarioDiasDeLaSemana() {
        return horarioDiasDeLaSemana;
    }

    public void setHorarioDiasDeLaSemana(String horarioDiasDeLaSemana) {
        this.horarioDiasDeLaSemana = horarioDiasDeLaSemana;
    }

    public String getHorarioDiasFinDeSemana() {
        return horarioDiasFinDeSemana;
    }

    public void setHorarioDiasFinDeSemana(String horarioDiasFinDeSemana) {
        this.horarioDiasFinDeSemana = horarioDiasFinDeSemana;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getTipodeDocumentoContacto() {
        return tipodeDocumentoContacto;
    }

    public void setTipodeDocumentoContacto(String tipodeDocumentoContacto) {
        this.tipodeDocumentoContacto = tipodeDocumentoContacto;
    }

    public String getNumeroDeDocumentoCntacto() {
        return numeroDeDocumentoCntacto;
    }

    public void setNumeroDeDocumentoCntacto(String numeroDeDocumentoCntacto) {
        this.numeroDeDocumentoCntacto = numeroDeDocumentoCntacto;
    }

    public String getTelefonoFijoContacto() {
        return telefonoFijoContacto;
    }

    public void setTelefonoFijoContacto(String telefonoFijoContacto) {
        this.telefonoFijoContacto = telefonoFijoContacto;
    }

    public String getCelularContacto() {
        return celularContacto;
    }

    public void setCelularContacto(String celularContacto) {
        this.celularContacto = celularContacto;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getHorarioCierreDiasDeLaSemana() {
        return horarioCierreDiasDeLaSemana;
    }

    public void setHorarioCierreDiasDeLaSemana(String horarioCierreDiasDeLaSemana) {
        this.horarioCierreDiasDeLaSemana = horarioCierreDiasDeLaSemana;
    }

    public String getHorarioInicioDiasDeLaSemana() {
        return horarioInicioDiasDeLaSemana;
    }

    public void setHorarioInicioDiasDeLaSemana(String horarioInicioDiasDeLaSemana) {
        this.horarioInicioDiasDeLaSemana = horarioInicioDiasDeLaSemana;
    }

    public String getHorarioCierreDiasFinDeSemana() {
        return horarioCierreDiasFinDeSemana;
    }

    public void setHorarioCierreDiasFinDeSemana(String horarioCierreDiasFinDeSemana) {
        this.horarioCierreDiasFinDeSemana = horarioCierreDiasFinDeSemana;
    }

    public String getHorarioInicioDiasFinDeSemana() {
        return horarioInicioDiasFinDeSemana;
    }

    public void setHorarioInicioDiasFinDeSemana(String horarioInicioDiasFinDeSemana) {
        this.horarioInicioDiasFinDeSemana = horarioInicioDiasFinDeSemana;
    }

    public boolean getSinJornadaContinuaSemana() {
        return sinJornadaContinuaSemana;
    }

    public void setSinJornadaContinuaSemana(boolean sinJornadaContinuaSemana) {
        this.sinJornadaContinuaSemana = sinJornadaContinuaSemana;
    }

    public boolean getSinJornadaContinuaFinDeSemana() {
        return sinJornadaContinuaFinDeSemana;
    }

    public void setSinJornadaContinuaFinDeSemana(boolean sinJornadaContinuaFinDeSemana) {
        this.sinJornadaContinuaFinDeSemana = sinJornadaContinuaFinDeSemana;
    }

    public String getAprobacionMyPet() {
        return aprobacionMyPet;
    }

    public void setAprobacionMyPet(String aprobacionMyPet) {
        this.aprobacionMyPet = aprobacionMyPet;
    }

    public DatosTpaga getDatosTpaga() {
        return datosTpaga;
    }

    public void setDatosTpaga(DatosTpaga datosTpaga) {
        this.datosTpaga = datosTpaga;
    }

    public Tarjetas getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(Tarjetas tarjetas) {
        this.tarjetas = tarjetas;
    }

    public String getCorreoElectronicoContacto() {
        return correoElectronicoContacto;
    }

    public void setCorreoElectronicoContacto(String correoElectronicoContacto) {
        this.correoElectronicoContacto = correoElectronicoContacto;
    }
}
