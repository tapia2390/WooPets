package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 24/06/17.
 */

public class ComandoSistemaOferente {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    public interface OnComandoSistemaOferenteChangeListener {

        void cargoCategorias();
    }

    //interface del listener de la actividad interesada
    private ComandoSistemaOferente.OnComandoSistemaOferenteChangeListener mListener;

    public ComandoSistemaOferente(ComandoSistemaOferente.OnComandoSistemaOferenteChangeListener mListener){

        this.mListener = mListener;
    }


    public void getSistemaOferente(){



    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoSistemaOferenteChangeListener sDummyCallbacks = new OnComandoSistemaOferenteChangeListener()
    {
        @Override
        public void cargoCategorias()
        {}


    };
}
