package woopets.com.woopets.Oferente.AdapterOferente;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import woopets.com.woopets.Oferente.EditarPerfilOferente;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.RegistroOferente;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.ModelCliente;


/**
 * Created by andres on 10/15/17.
 */

public class ListaAddressAdapterOferente extends BaseAdapter {


    private Context mContext;
    private LayoutInflater mInflater;
    Modelo modelo = Modelo.getInstance();
    ModelCliente modeloc = ModelCliente.getInstance();

    List<Address> direcciones;
    private int posDir = 0;
    String activiti2 = "";


    public ListaAddressAdapterOferente(Context context, List<Address> direcciones, int posDir, String activiti) {

        this.direcciones = direcciones;
        this.posDir = posDir;
        mContext = context;
        this.activiti2 = activiti;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return direcciones.size();
    }

    @Override
    public Object getItem(int i) {
        return direcciones.get(i);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }


    @Override
    public View getView(final int i, View view, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.item_geo_direccion, parent, false);
        final TextView dir = (TextView) rowView.findViewById(R.id.direccion);

        dir.setText(direcciones.get(i).getAddressLine(0));

        dir.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sharedPreferences = mContext.getSharedPreferences("preferencias usuario", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("txt_direecion", dir.getText().toString());
                editor.commit();

                modelo.datosOferente.setDireccion("" + dir.getText().toString());
                String ciudad =  adressToCiudad(direcciones.get(i));
                modelo.datosOferente.ciudad = ciudad;
                modelo.datosOferente.departamento = modeloc.getDepartamento(ciudad);;


                ((Activity) mContext).finish();


            }
        });

        return rowView;
    }

    public String adressToCiudad(Address ladir){
        return  ladir.getLocality();
    }


}

