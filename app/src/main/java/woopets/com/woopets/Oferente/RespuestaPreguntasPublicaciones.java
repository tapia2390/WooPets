package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import woopets.com.woopets.Oferente.Comandos.ComandoRespuestaPreguntaPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class RespuestaPreguntasPublicaciones extends Activity implements ComandoRespuestaPreguntaPublicaciones.OnComandoRespuestaPreguntaPublicacionesChangeListener{

    ComandoRespuestaPreguntaPublicaciones comandoRespuestaPreguntaPublicaciones;
    Modelo modelo = Modelo.getInstance();
    EditText text_respuesta;
    String idPregunta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_respuesta_preguntas_publicaciones);

        text_respuesta = (EditText)findViewById(R.id.text_respuesta);

        Bundle bundle2 = getIntent().getExtras();
        idPregunta = bundle2.getString("idPregunta");

        comandoRespuestaPreguntaPublicaciones =new ComandoRespuestaPreguntaPublicaciones(this);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

    }




    @Override
    public void onBackPressed() {
        atras();
        super.onBackPressed();
    }

    private void atras() {
        
        finish();
    }

    public void btn_atras(View v){
        atras();

    }

    public void btn_ok(View v){

        if(text_respuesta.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Campo obligatorio",Toast.LENGTH_LONG).show();
            text_respuesta.setError("Campo obligatorio");
        }else{

            comandoRespuestaPreguntaPublicaciones.setRespuestaNotificocaiones(idPregunta, text_respuesta.getText().toString());
            comandoRespuestaPreguntaPublicaciones.respuestaNotificacion(idPregunta,text_respuesta.getText().toString());
            modelo.filtro = 1;
            finish();
        }

    }

    @Override
    public void cargoRespuesta() {

    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }
}
