package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;

import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class CoashMarckDestacados extends Activity {

    String uidProducto;
    String abiertascerrada;
    int posicionServicio =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_coash_marck_destacados);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

        Bundle bundle2 = getIntent().getExtras();
        uidProducto = bundle2.getString("idProducto");
        posicionServicio = bundle2.getInt("posicionServicio");
        abiertascerrada = bundle2.getString("abiertascerrada");



    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }


}
