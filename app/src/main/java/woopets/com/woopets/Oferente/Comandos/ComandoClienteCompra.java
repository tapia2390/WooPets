package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import woopets.com.woopets.Oferente.ModelOferente.Cliente;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Pedido;

/**
 * Created by tacto on 23/08/17.
 */

public class ComandoClienteCompra {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo  = Modelo.getInstance();

    public interface OnComandoComprasAbiertasCerradasoChangeListener {

        void clienteCompraAbiertaCerrada();
    }

    //interface del listener de la actividad interesada
    private OnComandoComprasAbiertasCerradasoChangeListener mListener;

    public ComandoClienteCompra(OnComandoComprasAbiertasCerradasoChangeListener mListener){

        this.mListener = mListener;
    }



    public void comprasAbiertasCerradas_(final String idPublicacion, final String abiertaCerrada){


        modelo.comprasProductosAbiertasCerradases.clear();
        modelo.comprasProductosAbiertasCerradases2.clear();

        DatabaseReference ref;
        Query query;
        if(abiertaCerrada.equals("abiertas")){
             ref = database.getReference("compras/abiertas/");//ruta path
             query = ref.orderByChild("idPublicacion").equalTo(idPublicacion);
        }else{
            ref = database.getReference("compras/cerradas/");//ruta path
            query = ref.orderByChild("idPublicacion").equalTo(idPublicacion);
        }

        ChildEventListener listener = new ChildEventListener()  {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {


                if(!snap.exists()){
                    mListener.clienteCompraAbiertaCerrada();
                }else{
                    CompraProductoAbiertaCerrada comprasProductosAbiertasCerradas = new CompraProductoAbiertaCerrada();
                    ArrayList<Pedido> listPedidos = new ArrayList<Pedido>();

                    for (DataSnapshot snProductosAbiertasCerradas : snap.getChildren()) {

                        comprasProductosAbiertasCerradas.setAbiertaCerrada(abiertaCerrada);
                        comprasProductosAbiertasCerradas.setKey(snProductosAbiertasCerradas.getKey());
                        comprasProductosAbiertasCerradas.setFecha(snProductosAbiertasCerradas.child("fecha").getValue().toString());

                        comprasProductosAbiertasCerradas.setIdCliente(snProductosAbiertasCerradas.child("idCliente").getValue().toString());

                        listadoCliente(snProductosAbiertasCerradas.child("idCliente").getValue().toString());

                        comprasProductosAbiertasCerradas.setIdOferente(snProductosAbiertasCerradas.child("idOferente").getValue().toString());
                        comprasProductosAbiertasCerradas.setTimestamp(((Long) snProductosAbiertasCerradas.child("timestamp").getValue()).intValue());
                        comprasProductosAbiertasCerradas.setValor(((Long) snProductosAbiertasCerradas.child("valor").getValue()).intValue());

                        //nuevo arbol
                        DataSnapshot snapPedido;
                        snapPedido = (DataSnapshot) snProductosAbiertasCerradas.child("pedido");
                        for (DataSnapshot dspedido : snapPedido.getChildren()){
                            Pedido pedido = new Pedido();
                            pedido.setId(dspedido.getKey());
                            pedido.setCantidad(((Long) dspedido.child("cantidad").getValue()).intValue());
                            pedido.setEstado(dspedido.child("estado").getValue().toString());
                            pedido.setFechaDelServicio(dspedido.child("fechaServicio").getValue().toString());
                            pedido.setIdPublicacion(dspedido.child("idPublicacion").getValue().toString());
                            boolean servicio = (boolean) dspedido.child("servicio").getValue();
                            pedido.setServicio(servicio);
                            listPedidos.add(pedido);


                        }
                        comprasProductosAbiertasCerradas.setPedidos(listPedidos);
                    }

                    if(abiertaCerrada.equals("abiertas")){
                        modelo.comprasProductosAbiertasCerradases.add(comprasProductosAbiertasCerradas);
                    }else{
                        modelo.comprasProductosAbiertasCerradases2.add(comprasProductosAbiertasCerradas);
                    }

                    mListener.clienteCompraAbiertaCerrada();
                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        query.addChildEventListener(listener);
        modelo.hijosListener.put(query,listener);


    }


    public Cliente listadoCliente (String idCliente){
        DatabaseReference ref = database.getReference("cliente/");//ruta path
        Query query  = ref.orderByChild("idPublicacion").equalTo(idCliente);
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return null;
    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoComprasAbiertasCerradasoChangeListener sDummyCallbacks = new OnComandoComprasAbiertasCerradasoChangeListener()
    {


        @Override
        public void clienteCompraAbiertaCerrada()
        {}

    };
}
