package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import woopets.com.woopets.Oferente.Comandos.ComandoEditarEliminarEstado;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;
import woopets.com.woopets.R;

public class MyPublicacionesTipo extends Activity implements ComandoEditarEliminarEstado.OnComandoEditarEliminarEstadoChangeListener {


    Modelo modelo = Modelo.getInstance();
    ImageView perro, gato, ave, pez, roedor, exotico_otro;
    boolean img_perro = false;
    boolean img_gato = false;
    boolean img_ave = false;
    boolean img_pez = false;
    boolean img_roedor = false;
    boolean img_exotico_otro = false;
    String text_target = "";
    final Context context = this;
    ComandoEditarEliminarEstado comandoEditarEliminarEstado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_my_publicaciones_tipo);

        perro = (ImageView) findViewById(R.id.perro);
        gato = (ImageView) findViewById(R.id.gato);
        ave = (ImageView) findViewById(R.id.ave);
        pez = (ImageView) findViewById(R.id.pez);
        roedor = (ImageView) findViewById(R.id.roedor);
        exotico_otro = (ImageView) findViewById(R.id.exotico_otro);
        comandoEditarEliminarEstado = new ComandoEditarEliminarEstado(this);

        editarTipo();
        preferneciaTipo();

    }


    public void continuar(View v) {


        if (img_perro || img_gato || img_ave || img_pez || img_roedor || img_exotico_otro) {
            text_target = TextUtils.join(", ", modelo.target);
            modelo.productosOferente.setTarget(text_target);
            if (modelo.publicacion_estado == 1) {
                if (text_target.equals("")) {
                    showAlertDatosObligatorios();
                }
                comandoEditarEliminarEstado.setActualizarTipo(modelo.pro_uidProducto, text_target);
            } else {
                Intent i = new Intent(getApplicationContext(), MyPublicacionesArticulo.class);
                startActivity(i);
                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                //finish();
            }


        } else {
            showAlertDatosObligatorios();
        }

    }


    @Override
    public void onBackPressed() {
        modelo.target.clear();
        finish();

        super.onBackPressed();
    }

    public void perro(View v) {
        if (img_perro == false) {
            img_perro = true;
            modelo.target.add("Perro");
            perro.setVisibility(View.VISIBLE);
        } else {
            modelo.target.remove("Perro");
            img_perro = false;
            perro.setVisibility(View.INVISIBLE);
        }

    }

    public void gato(View v) {
        if (img_gato == false) {
            img_gato = true;
            modelo.target.add("Gato");
            gato.setVisibility(View.VISIBLE);
        } else {
            img_gato = false;
            modelo.target.remove("Gato");
            gato.setVisibility(View.INVISIBLE);
        }
    }

    public void ave(View v) {
        if (img_ave == false) {
            img_ave = true;
            modelo.target.add("Ave");
            ave.setVisibility(View.VISIBLE);
        } else {
            img_ave = false;
            modelo.target.remove("Ave");
            ave.setVisibility(View.INVISIBLE);
        }
    }

    public void pez(View v) {

        if (img_pez == false) {
            img_pez = true;
            modelo.target.add("Pez");
            pez.setVisibility(View.VISIBLE);
        } else {
            img_pez = false;
            modelo.target.remove("Pez");
            pez.setVisibility(View.INVISIBLE);
        }
    }

    public void roedor(View v) {

        if (img_roedor == false) {
            img_roedor = true;
            modelo.target.add("Roedor");
            roedor.setVisibility(View.VISIBLE);
        } else {
            img_roedor = false;
            modelo.target.remove("Roedor");
            roedor.setVisibility(View.INVISIBLE);
        }
    }

    public void exotico_otro(View v) {
        if (img_exotico_otro == false) {
            img_exotico_otro = true;
            modelo.target.add("Exótico (otros)");
            exotico_otro.setVisibility(View.VISIBLE);
        } else {
            img_exotico_otro = false;
            modelo.target.remove("Exótico (otros)");
            exotico_otro.setVisibility(View.INVISIBLE);
        }
    }


    public void showAlertDatosObligatorios() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("¡Advertencia!");
        alertDialogBuilder.setMessage("Por favor, seleccione una opción.");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Log.v("ok", "ok");
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void cargoEstado() {

    }

    @Override
    public void cargoEliminarPublicacion() {

    }

    @Override
    public void cargoAbiertas() {

    }

    @Override
    public void cargoCerradas() {

    }

    @Override
    public void cargoTipo() {

        modelo.productosOferentes.get(0).setTarget(text_target);
        modelo.publicacion_estado = 0;
        //Intent i = new Intent(getApplicationContext(), InformacionProductoServicioOferente.class);
        //startActivity(i);

        showAlertActualizacion();

    }

    public void showAlertActualizacion() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.publicupdate));

        // set dialog message
        alertDialogBuilder
                .setMessage("")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onBackPressed();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    public void cargoArticulo() {

    }

    @Override
    public void cargoFoto() {

    }


    private void editarTipo() {

        if (modelo.publicacion_estado == 1) {
            String listasinEspacios = modelo.productosOferentesBoolean.get(0).getTarget().replaceAll("\\s", "");
            List<String> myList = new ArrayList<String>(Arrays.asList(listasinEspacios.split(",")));
            for (int i = 0; i < myList.size(); i++) {

                if (myList.get(i).equals("Perro")) {
                    img_perro = true;
                    modelo.target.add("Perro");
                    perro.setVisibility(View.VISIBLE);
                }

                if (myList.get(i).equals("Gato")) {
                    img_gato = true;
                    modelo.target.add("Gato");
                    gato.setVisibility(View.VISIBLE);
                }


                if (myList.get(i).equals("Ave")) {
                    img_ave = true;
                    modelo.target.add("Ave");
                    ave.setVisibility(View.VISIBLE);
                }

                if (myList.get(i).equals("Pez")) {
                    img_pez = true;
                    modelo.target.add("Pez");
                    pez.setVisibility(View.VISIBLE);
                }

                if (myList.get(i).equals("Roedor")) {
                    img_roedor = true;
                    modelo.target.add("Roedor");
                    roedor.setVisibility(View.VISIBLE);
                }

                if (myList.get(i).equals("Exótico(otros)")) {
                    img_exotico_otro = true;
                    modelo.target.add("Exótico (otros)");
                    exotico_otro.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    public void preferneciaTipo() {
        if (modelo.target.size() > 0) {
            for (int i = 0; i < modelo.target.size(); i++) {

                if (modelo.target.get(i).equals("Perro")) {
                    img_perro = true;
                    perro.setVisibility(View.VISIBLE);
                }

                if (modelo.target.get(i).equals("Gato")) {
                    img_gato = true;
                    gato.setVisibility(View.VISIBLE);
                }


                if (modelo.target.get(i).equals("Ave")) {
                    img_ave = true;
                    ave.setVisibility(View.VISIBLE);
                }

                if (modelo.target.get(i).equals("Pez")) {
                    img_pez = true;
                    pez.setVisibility(View.VISIBLE);
                }

                if (modelo.target.get(i).equals("Roedor")) {
                    img_roedor = true;
                    roedor.setVisibility(View.VISIBLE);
                }

                if (modelo.target.get(i).equals("Exótico (otros)")) {
                    img_exotico_otro = true;
                    exotico_otro.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
