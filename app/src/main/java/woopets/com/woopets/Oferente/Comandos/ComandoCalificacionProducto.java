package woopets.com.woopets.Oferente.Comandos;


import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Oferente.ModelOferente.Calificacion;
import woopets.com.woopets.Oferente.ModelOferente.ChatOferente;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;

/**
 * Created by andres on 10/25/17.
 */

public class ComandoCalificacionProducto {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    public interface OnComandoCalificacionProductoListener {

        void getCalificaciones();

    }


    private ComandoCalificacionProducto.OnComandoCalificacionProductoListener mListener;

    public ComandoCalificacionProducto(OnComandoCalificacionProductoListener mListener) {

        this.mListener = mListener;
    }


    public void getCalificacionOferente(final String idPublicacion, final String idClienete) {

        modelo.calificaiones.clear();

        DatabaseReference ref = database.getReference("calificaciones/");//ruta path
        Query query = ref.orderByChild("idCompra").equalTo(idPublicacion);

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {


                if (!snap.exists()) {
                    mListener.getCalificaciones();
                } else {

                    if (idClienete.equals(idClienete)) {
                        Calificacion calificacion = new Calificacion();
                        calificacion.setId(snap.getKey());
                        calificacion.setCalificacion(((Long) snap.child("calificacion").getValue()).intValue());
                        calificacion.setComentario(snap.child("comentario").getValue().toString());
                        calificacion.setFecha(snap.child("fecha").getValue().toString());
                        calificacion.setIdCliente(snap.child("idCliente").getValue().toString());
                        calificacion.setIdCompra(snap.child("idCompra").getValue().toString());
                        calificacion.setIdOferente(snap.child("idOferente").getValue().toString());
                        calificacion.setIdPublicacion(snap.child("idPublicacion").getValue().toString());
                        calificacion.setTimestamp(((Long) snap.child("timestamp").getValue()).intValue());

                        modelo.calificaiones.add(calificacion);
                    }

                }

                mListener.getCalificaciones();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
        };

        query.addChildEventListener(listener);
        modelo.hijosListener.put(query, listener);

    }

    public static ComandoCalificacionProducto.OnComandoCalificacionProductoListener dummy = new ComandoCalificacionProducto.OnComandoCalificacionProductoListener() {


        @Override
        public void getCalificaciones() {
        }



    };


}







