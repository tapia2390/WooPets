package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import woopets.com.woopets.Oferente.ModelOferente.Cliente;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Direccion;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Pedido;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;
import woopets.com.woopets.Oferente.ModelOferente.uploadFirebase.DatosTarjetaCompra;

/**
 * Created by tacto on 23/08/17.
 */

public class ComandoComprasAbiertasCerradas {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo = Modelo.getInstance();

    public interface OnComandoComprasAbiertasCerradasoChangeListener {

        void comprasAbiertasCerradasoOferente();

    }

    //interface del listener de la actividad interesada
    private OnComandoComprasAbiertasCerradasoChangeListener mListener;

    public ComandoComprasAbiertasCerradas(OnComandoComprasAbiertasCerradasoChangeListener mListener) {

        this.mListener = mListener;
    }

    public void comprasAbiertasCerradas_(final String uid, final String abiertaCerrada) {

        modelo.comprasProductosAbiertasCerradases2.clear();

        DatabaseReference ref;
        Query query;
        if (abiertaCerrada.equals("abiertas")) {
            ref = database.getReference("compras/abiertas/");//ruta path
            query = ref.orderByChild("idOferente").equalTo(modelo.uid);
        } else {
            ref = database.getReference("compras/cerradas/");//ruta path
            query = ref.orderByChild("idOferente").equalTo(modelo.uid);
        }

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snProductosAbiertasCerradas, String s) {


                if (!snProductosAbiertasCerradas.exists()) {
                    mListener.comprasAbiertasCerradasoOferente();
                } else {
                    CompraProductoAbiertaCerrada comprasProductosAbiertasCerradas = new CompraProductoAbiertaCerrada();
                    ArrayList<Pedido> listPedidos = new ArrayList<Pedido>();
                    listPedidos.clear();

                    ArrayList<DatosTarjetaCompra> listTarjeta = new ArrayList<DatosTarjetaCompra>();
                    listTarjeta.clear();


                    comprasProductosAbiertasCerradas.setAbiertaCerrada(abiertaCerrada);
                    comprasProductosAbiertasCerradas.setKey(snProductosAbiertasCerradas.getKey());
                    comprasProductosAbiertasCerradas.setFecha(snProductosAbiertasCerradas.child("fecha").getValue().toString());



                    //nuevo arbol tarjeta
                    if(snProductosAbiertasCerradas.child("datosTarjeta").exists()){
                        //nuevo arbol tarjeta
                        DataSnapshot snapTarjeta;
                        snapTarjeta = (DataSnapshot) snProductosAbiertasCerradas.child("datosTarjeta");

                        DatosTarjetaCompra tarjeta = new DatosTarjetaCompra();
                        tarjeta.setCuotasTarjeta(((Long) snapTarjeta.child("cuotasTarjeta").getValue()).intValue());
                        tarjeta.setIdTarjeta(snapTarjeta.child("idTarjeta").getValue().toString());
                        tarjeta.setTokenTarjeta(snapTarjeta.child("tokenTarjeta").getValue().toString());
                        tarjeta.cedula = snapTarjeta.child("cedula").getValue().toString();
                        tarjeta.ciudad = snapTarjeta.child("ciudad").getValue().toString();
                        tarjeta.departamento = snapTarjeta.child("departamento").getValue().toString();
                        tarjeta.correo = snapTarjeta.child("correo").getValue().toString();
                        tarjeta.direccion = snapTarjeta.child("direccion").getValue().toString();
                        tarjeta.franquicia = snapTarjeta.child("franquicia").getValue().toString();
                        tarjeta.nombre = snapTarjeta.child("nombre").getValue().toString();
                        tarjeta.telefono = snapTarjeta.child("telefono").getValue().toString();

                        listTarjeta.add(tarjeta);
                        comprasProductosAbiertasCerradas.setTarjeta(listTarjeta);

                    }

                    comprasProductosAbiertasCerradas.setIdCliente(snProductosAbiertasCerradas.child("idCliente").getValue().toString());

                    comprasProductosAbiertasCerradas.setIdOferente(snProductosAbiertasCerradas.child("idOferente").getValue().toString());

                    if (snProductosAbiertasCerradas.child("metodoPago").getValue().toString() != null) {
                        comprasProductosAbiertasCerradas.setMetodoDePago(snProductosAbiertasCerradas.child("metodoPago").getValue().toString());
                    }
                    comprasProductosAbiertasCerradas.setTimestamp(((Long) snProductosAbiertasCerradas.child("timestamp").getValue()).intValue());
                    comprasProductosAbiertasCerradas.setValor(((Long) snProductosAbiertasCerradas.child("valor").getValue()).intValue());

                    String idCliente = snProductosAbiertasCerradas.child("idCliente").getValue().toString();
                    comprasProductosAbiertasCerradas.setCliente(getDatosCliente(idCliente));

                    //nuevo arbol
                    DataSnapshot snapPedido;
                    snapPedido = (DataSnapshot) snProductosAbiertasCerradas.child("pedido");
                    for (DataSnapshot dspedido : snapPedido.getChildren()) {
                        Pedido pedido = new Pedido();
                        pedido.setId(dspedido.getKey());
                        pedido.setCantidad(((Long) dspedido.child("cantidad").getValue()).intValue());
                        pedido.setEstado(dspedido.child("estado").getValue().toString());

                        if (snProductosAbiertasCerradas.child("fechaServicio").getValue() != null) {
                            pedido.setFechaDelServicio(dspedido.child("fechaServicio").getValue().toString());
                        }

                        if (dspedido.child("motivo").getValue() != null) {
                            pedido.setMotivo(dspedido.child("motivo").getValue().toString());
                        }

                        if (dspedido.child("justificacion").getValue() != null) {
                            pedido.setJustificacion(dspedido.child("justificacion").getValue().toString());
                        }

                        pedido.setIdPublicacion(dspedido.child("idPublicacion").getValue().toString());

                        if (dspedido.child("direccion").getValue() != null) {
                            pedido.setDireccion(dspedido.child("direccion").getValue().toString());
                        }

                        String idPublicacion = dspedido.child("idPublicacion").getValue().toString();


                        if (abiertaCerrada.equals("abiertas")) {
                            pedido.setProductosOferentes(modelo.getProductoByIdProducto(idPublicacion, "abiertas"));

                        } else {

                            pedido.setProductosOferentes(modelo.getProductoByIdProducto(idPublicacion, "cerradas"));
                        }

                        boolean servicio = (boolean) dspedido.child("servicio").getValue();
                        pedido.setServicio(servicio);


                        if (dspedido.child("fechaServicio").getValue() != null) {
                            pedido.setFechaDelServicio(dspedido.child("fechaServicio").getValue().toString());
                        }


                        if (dspedido.child("estado").getValue() != null) {
                            pedido.setEstado(dspedido.child("estado").getValue().toString());
                        }

                        if (dspedido.child("fechaPago").getValue() != null) {
                            pedido.setFechaPago(dspedido.child("fechaPago").getValue().toString());
                        }

                        if (dspedido.child("reprogramada").getValue() != null) {
                            boolean reprogramada = (boolean) dspedido.child("reprogramada").getValue();
                            pedido.setReprogramada(reprogramada);

                        }

                        listPedidos.add(pedido);

                    }
                    comprasProductosAbiertasCerradas.setPedidos(listPedidos);

                    modelo.addCompraAbiertaCerrada(comprasProductosAbiertasCerradas);


                    mListener.comprasAbiertasCerradasoOferente();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        query.addChildEventListener(listener);
        modelo.hijosListener.put(query, listener);


    }


    public ArrayList<Cliente> getDatosCliente(String idCliente) {

        final Cliente cliente = new Cliente();
        final ArrayList<Cliente> ltCliente = new ArrayList<Cliente>();
        final ArrayList<Direccion> listDirecion = new ArrayList<Direccion>();

        DatabaseReference ref = database.getReference("clientes/" + idCliente + "/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                cliente.setId(snap.getKey());
                cliente.setNombre(snap.child("nombre").getValue().toString());

                cliente.setApellido(snap.child("apellido").getValue().toString());

                cliente.setCelular(snap.child("celular").getValue().toString());

                //nuevo arbol
                DataSnapshot snapDirecciones;
                snapDirecciones = (DataSnapshot) snap.child("direcciones");
                for (DataSnapshot dsdir : snapDirecciones.getChildren()) {

                    Direccion direccion = new Direccion();
                    direccion.setId(dsdir.getKey());
                    direccion.setDireccion(dsdir.child("direccion").getValue().toString());
                    direccion.setNombre(dsdir.child("nombre").getValue().toString());
                    listDirecion.add(direccion);
                }

                cliente.setListDirecion(listDirecion);

                cliente.setCorreo(snap.child("correo").getValue().toString());

                cliente.setTokens(snap.child("tokenDevice").getValue().toString());


                ltCliente.add(cliente);
                mListener.comprasAbiertasCerradasoOferente();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return ltCliente;
    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoComprasAbiertasCerradasoChangeListener sDummyCallbacks = new OnComandoComprasAbiertasCerradasoChangeListener() {


        @Override
        public void comprasAbiertasCerradasoOferente() {
        }


    };
}
