package woopets.com.woopets.Oferente.ModelOferente;

import java.util.ArrayList;

/**
 * Created by tacto on 24/11/17.
 */

public class ClassPreguntas_A_MisPublicaciones {

    String id;
    String fechaPregunta;
    String fechaRespuesta;
    String idCliente;
    String idOferente;
    String idPublicacion;
    String pregunta;
    String respuesta;
    Long timestamp;
    public ArrayList<Cliente> listaClienete = new ArrayList<Cliente>();
    public ArrayList<ProductosOferente> listaProductosOferentes = new ArrayList<ProductosOferente>();


    public ClassPreguntas_A_MisPublicaciones(String key) {
        this.id = key;
    }

    public ClassPreguntas_A_MisPublicaciones() {

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFechaPregunta() {
        return fechaPregunta;
    }

    public void setFechaPregunta(String fechaPregunta) {
        this.fechaPregunta = fechaPregunta;
    }

    public String getFechaRespuesta() {
        return fechaRespuesta;
    }

    public void setFechaRespuesta(String fechaRespuesta) {
        this.fechaRespuesta = fechaRespuesta;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdOferente() {
        return idOferente;
    }

    public void setIdOferente(String idOferente) {
        this.idOferente = idOferente;
    }

    public String getIdPublicacion() {
        return idPublicacion;
    }

    public void setIdPublicacion(String idPublicacion) {
        this.idPublicacion = idPublicacion;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public ArrayList<Cliente> getListaClienete() {
        return listaClienete;
    }

    public void setListaClienete(ArrayList<Cliente> listaClienete) {
        this.listaClienete = listaClienete;
    }

    public ArrayList<ProductosOferente> getListaProductosOferentes() {
        return listaProductosOferentes;
    }

    public void setListaProductosOferentes(ArrayList<ProductosOferente> listaProductosOferentes) {
        this.listaProductosOferentes = listaProductosOferentes;
    }
}
