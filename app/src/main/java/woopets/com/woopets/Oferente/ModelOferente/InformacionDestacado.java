package woopets.com.woopets.Oferente.ModelOferente;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;

import woopets.com.woopets.Oferente.TapsHome;
import woopets.com.woopets.R;

public class InformacionDestacado extends Activity {

    Modelo modelo = Modelo.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_informacion_destacado);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            Log.v("cerrar","cerrar");
          /*  Intent i = new Intent(getApplicationContext(), TapsHome.class);
            startActivity(i);
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);*/
            finish();
        }
        return true;
    }

}
