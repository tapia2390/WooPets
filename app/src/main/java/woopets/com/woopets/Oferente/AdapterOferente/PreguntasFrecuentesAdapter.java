package woopets.com.woopets.Oferente.AdapterOferente;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntasFrecuentes;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.PreguntasPublicaciones;
import woopets.com.woopets.R;

/**
 * Created by tactomotion on 26/12/16.
 */
public class PreguntasFrecuentesAdapter extends BaseAdapter {

    //Atributos del adaptador
    private final Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<ClassPreguntasFrecuentes> classPreguntasFrecuentes;
    private Modelo sing = Modelo.getInstance();

    public PreguntasFrecuentesAdapter(Context mContext, AdapterCallback mListener) {

        this.mContext = mContext;
        this.mListener = mListener;
        this.classPreguntasFrecuentes = sing.classpreguntasFrecuentes;

    }

    @Override
    public int getCount() {
        return classPreguntasFrecuentes.size();
    }

    @Override
    public Object getItem(int position) {
        return classPreguntasFrecuentes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // Declare Variables
        final TextView txtPregunta;
        final TextView txtRespuesta;


        final ClassPreguntasFrecuentes listpreguntasfrecuentes = (ClassPreguntasFrecuentes) getItem(position);


        // Inflate la vista de la Orden
        RelativeLayout itemLayout =  (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.adapterpreguntasfrecuentes, parent, false);

        // Toma los elementos a utilizar
        txtPregunta = (TextView) itemLayout.findViewById(R.id.txtTitulo);
        txtRespuesta = (TextView) itemLayout.findViewById(R.id.txtrespuesta);

        txtPregunta.setText("P: "+listpreguntasfrecuentes.getPregunta());
        txtRespuesta.setText("R: "+listpreguntasfrecuentes.getRespuesta());


        return itemLayout;

    }


    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
   */
    public interface AdapterCallback {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback() {


    };
}
