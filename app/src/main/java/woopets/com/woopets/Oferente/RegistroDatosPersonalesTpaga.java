package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.InfoCompraActual;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.PagoTarjetaCliente;
import woopets.com.woopets.Modelo.cliente.Publicacion;
import woopets.com.woopets.Oferente.Comandos.ComandoOrdenTarjeta;
import woopets.com.woopets.Oferente.Comandos.ComandoRegitroTpaga;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.Oferente.ModelOferente.uploadFirebase.DatosTarjetaCompra;
import woopets.com.woopets.Oferente.tpaga.Model.CustomerCliente;
import woopets.com.woopets.Oferente.tpaga.Model.CustomerResponse;
import woopets.com.woopets.Oferente.tpaga.Model.TPPAgo;
import woopets.com.woopets.Oferente.tpaga.Model.TPPAgoResponse;
import woopets.com.woopets.Oferente.tpaga.io.MyApiAdapter;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.Comandos.CmdIp;
import woopets.com.woopets.cliente.Comandos.CmdIp.OnIpListener;
import woopets.com.woopets.cliente.Comandos.CmdPayU;
import woopets.com.woopets.cliente.Comandos.CmdPayU.OnCheckPagos;
import woopets.com.woopets.cliente.Comandos.ComadoPago;
import woopets.com.woopets.cliente.Comandos.ComadoPago.OnComadoPagoListener;
import woopets.com.woopets.cliente.Comandos.ComandoInventario;
import woopets.com.woopets.cliente.Comandos.ComandoInventario.OnComandoInventarioListener;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas;
import woopets.com.woopets.cliente.detallecompra.ConfirmacionDireccion;
import woopets.com.woopets.payU.PagoPayU;

import android.util.Patterns;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.FirebaseFunctionsException;
import com.google.firebase.functions.HttpsCallableResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class RegistroDatosPersonalesTpaga extends Activity implements ComandoRegitroTpaga.OnRegitroTpagaChangeListener, ComandoOrdenTarjeta.OnComandoOrdenTarjetaChangeListener  {


    EditText nombre_tp, correo_tp, celular_tp;
    private ProgressDialog progressDialog;
    ComandoRegitroTpaga comandoRegitroTpaga;
    String idCliente = "";
    Modelo modelo = Modelo.getInstance();
    TextView total_pagar_destacado, txt_tarjeta_credito;
    RelativeLayout relative_tarjeta;
    final Context context = this;

    private FirebaseFunctions mFunctions;


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference referencia = database.getReference();
    ComandoOrdenTarjeta comandoOrdenTarjeta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registro_datos_personales_tpaga);


        mFunctions = FirebaseFunctions.getInstance();

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


        nombre_tp = (EditText) findViewById(R.id.nombre_tp);
        correo_tp = (EditText) findViewById(R.id.correo_tp);
        celular_tp = (EditText) findViewById(R.id.celular_tp);
        total_pagar_destacado = (TextView) findViewById(R.id.total_pagar_destacado);
        relative_tarjeta = (RelativeLayout) findViewById(R.id.relative_tarjeta);
        progressDialog = new ProgressDialog(this);
        txt_tarjeta_credito = (TextView) findViewById(R.id.txt_tarjeta_credito);

        comandoRegitroTpaga = new ComandoRegitroTpaga(this);
        comandoOrdenTarjeta = new ComandoOrdenTarjeta(this);


        nombre_tp.setText(modelo.datosOferente.getNombreContacto());
        correo_tp.setText(modelo.datosOferente.getCorreoElectronicoContacto());
        celular_tp.setText(modelo.datosOferente.getCelularContacto());


        MiniTarjeta mini = modelo.getTarjetaActiva();


        if (mini != null ){
            txt_tarjeta_credito.setText(mini.lastFour );
        }
        else{
            txt_tarjeta_credito.setText("Sin definir");
        }

        total_pagar_destacado.setText("$" + modelo.params.getValorDestacado());

        relative_tarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), RegistroDatosTarjetaTpaga.class);
                startActivity(i);
            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();

        MiniTarjeta mini = modelo.getTarjetaActiva();


        if (mini != null ){
            txt_tarjeta_credito.setText(mini.lastFour );
        }
        else{
            txt_tarjeta_credito.setText("Sin definir");
        }

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void siguiente_tp() {

        if (nombre_tp.getText().toString().equals("")) {
            nombre_tp.setError("Nombre obligatorio");
        }  else if (correo_tp.getText().toString().equals("")) {
            correo_tp.setError("Correo obligatorio");
        } else if (!validarEmail(correo_tp.getText().toString())) {
            correo_tp.setError("Correo no válido");
        } else if (celular_tp.getText().toString().equals("") || celular_tp.getText().length() < 10 || celular_tp.getText().length() > 10) {
            celular_tp.setError("Número de celular erróneo");
        } else {

            if (!estaConectado()) {
                String titulo = "Sin Internet";
                String mensaje = "Sin Conexión a Internet";
                showAlertSinInternet(titulo, mensaje);
                return;
            }


            if (modelo.getTarjetaActiva() == null){
                showAlertSinInternet("Info del pago", "Falta adicionar los datos de la tarjeta");
                return;
            }


            android.app.AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new android.app.AlertDialog.Builder(RegistroDatosPersonalesTpaga.this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new android.app.AlertDialog.Builder(RegistroDatosPersonalesTpaga.this);
            }

            String val = Utility.convertToMoney( modelo.params.getValorDestacado());
            String  msg = "A punto de cargar a tu tarjeta una compra por valor de " + val + ". ¿Estas seguro?";



            builder.setTitle("Confirma tu información")
                    .setMessage(msg)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            procesarPago();
                            //porcesarConMercadoPago();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();


        }

    }


    //validar email
    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }




    public void didTapAceptar(View v) {

        siguiente_tp();


    }





    protected void procesarPago(){

        String idCompra = new ComandoPreguntas(null).getLlave();


        //Creamos la compra
        final Compra compra = new Compra();
        compra.id = idCompra;
        compra.servicio = true;
        compra.cantidad = 1;
        compra.idPublicacion = modelo.productosOferentesBoolean.get(0).getId();
        compra.idOferente = modelo.uid;
        compra.valor = modelo.params.getValorDestacado();
        compra.direccion = "Desconocida";



        ////Ahora si a procesar el pago

        progressDialog.setMessage("Validando la información, por favor espere...");
        progressDialog.show();

        procesarTarjetaPayU(compra);

    }

    protected void procesarTarjetaPayU(final Compra compra){




        CmdIp.getIp(this, new OnIpListener() {
            @Override
            public void gotIp(String ip) {

                final MiniTarjeta mini = modelo.getTarjetaActiva();

                mini.referenceCode = ComandoPreguntas.getLlaveFirebase();
                mini.ip = ip;

                hacerPagoFinalConTarjeta(compra, modelo.getMapaToPagoToken(compra, mini), mini);

            }

            @Override
            public void fallo() {

                showAlertSinInternet("Fallo en la transacción", "No pudo iniciar la transacción. Intente más tarde");

            }
        });






    }


    private Task<Map> hacerPagoFinalConTarjeta(final Compra compra, Map data, final MiniTarjeta mini) {     //Usamos el token para hacer el pago.


        return mFunctions
                .getHttpsCallable("pagoDestacadoTokenAndroid")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {

                        CmdPayU.checkPagos(compra.id, new OnCheckPagos() {
                            @Override
                            public void pagoAprobado(PagoPayU pagoPayU) {
                                progressDialog.dismiss();

                                PagoTarjetaCliente pago = new PagoTarjetaCliente();
                                pago.Descripcion = "Compra Destacado";
                                pago.authorizationCode = pagoPayU.authorizationCode;
                                pago.idCompra = compra.id;
                                pago.idTarjeta = mini.id;
                                pago.metodoPago = "Tarjeta";
                                pago.idPago = pagoPayU.orderId;
                                pago.paymentTransaction = pagoPayU.transactionId;
                                registrarPago(compra, pago);

                            }

                            @Override
                            public void pagoReachazado(String razon) {
                                showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago, solicita a tu cliente otro medio de pago: " + razon);
                                progressDialog.dismiss();


                            }

                            @Override
                            public void pagoPendiente(PagoPayU pagoPayU) {
                                //adicionarEnInventario(compra);
                                showAlertSinInternet("Transacción pendiente",  "La transacción esta siendo evaluada por personal de PauY, esto puede tardar entre una a cuatro horas");
                                progressDialog.dismiss();


                            }

                            @Override
                            public void pagoConError(String razon) {
                                showAlertSinInternet("Fallo en la transacción", "Fallo en la sistema de pagos, intenta más tarde: " + razon);
                                progressDialog.dismiss();
                            }
                        });

                        Map result = (Map) task.getResult().getData();
                        return result;
                    }
                });
    }

    private Task<Map> hacerPagoFinalConTarjetaViejo(final Compra compra, Map data, final MiniTarjeta mini) {     //Usamos el token para hacer el pago.


        return mFunctions
                .getHttpsCallable("pagoDestacadoTokenAndroid")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {

                        Map result = (Map) task.getResult().getData();
                        return result;
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     progressDialog.dismiss();
                                                     showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago, intenta más tarde. Código de error: P2");
                                                     return;

                                                 }


                                                 try {

                                                     Map result = (Map) task.getResult().get("transactionResponse");

                                                     String idOrdenPayU = result.get("orderId").toString();
                                                     String transactionId = result.get("transactionId").toString();
                                                     String authorizationCode = result.get("authorizationCode").toString();
                                                     String responseCode = result.get("responseCode").toString();

                                                     String status = result.get("state").toString();

                                                     if (status.equals("DECLINED")) {
                                                         showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago, revisa los datos de tu tarjeta. Código de error: " + responseCode);
                                                         progressDialog.dismiss();
                                                         return;

                                                     }

                                                     Toast.makeText(getApplicationContext(), "Pago " + result.get("state").toString(), Toast.LENGTH_LONG).show();

                                                     progressDialog.dismiss();

                                                     PagoTarjetaCliente pago = new PagoTarjetaCliente();
                                                     pago.Descripcion = "Compra Destacado";
                                                     pago.authorizationCode = authorizationCode;
                                                     pago.idCompra = compra.id;
                                                     pago.idTarjeta = mini.id;
                                                     pago.metodoPago = "Tarjeta";
                                                     pago.idPago = idOrdenPayU;
                                                     pago.paymentTransaction = transactionId;
                                                     //registrarPago(c);
                                                 }
                                                 catch (Exception e){
                                                     progressDialog.dismiss();
                                                     showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago, intenta más tarde. Código de error: P1");
                                                     return;

                                                 }

                                             }
                                         }
                );
    }





    protected void registrarPago(final Compra comp, PagoTarjetaCliente pago){

        new ComadoPago(new OnComadoPagoListener() {
            @Override
            public void didPagoExitoso() {


            }

            @Override
            public void didPagoFallido() {

            }

            @Override
            public void didRegistroExitoso() {
                modelo.productosOferentesBoolean.get(0).setDestacado(true);
                comandoRegitroTpaga.updateEstado();
                Intent i = new Intent(getApplicationContext(), CompraExiosaDestacdo.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                finish();
            }

            @Override
            public void didRegistroFallido() {
                progressDialog.dismiss();
                showAlertSinInternet("Problema con la transacción", "Se descontó el dinero de su tarjeta de crédito, pero no se pudo registrar el pedido. Comuniquese con WooPets");

            }
        }).registrarPagoOferente( pago, comp);

    }




   /* public String getIp(){

        String ipWifi = getWifiIPAddress();

        if (!ipWifi.equals("0.0.0.0") && !ipWifi.equals("")){
            return  ipWifi;
        }

        return getMobileIPAddress();

    }*/

    public String getWifiIPAddress() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        return  Formatter.formatIpAddress(ip);
    }


    public static String getMobileIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return  addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            return "0.0.0.0";
        }
        return "0.0.0.0";
    }



    @Override
    public void registroCustomer() {


    }

    @Override
    public void registroCredidCard() {

    }

    @Override
    public void registroDatosTp() {
        progressDialog.dismiss();
        Intent i = new Intent(getApplicationContext(), RegistroDatosTarjetaTpaga.class);
        i.putExtra("idCliente", idCliente);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        finish();
    }


    //
    public void vermas_tpaga(View v) {
        Uri uri = Uri.parse(modelo.urlTpaga);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }





    public void showAlertRegistro() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("Advertencia");

        // set dialog message
        alertDialogBuilder
                .setMessage("Debes inscribir primero tu tarjeta.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void showAlertCompra(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // Setting Alert Dialog Title
        alertDialogBuilder.setTitle("Confirma tu información");
        // Icon Of Alert Dialog
        // alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // Setting Alert Dialog Message
        alertDialogBuilder.setMessage("A punto de cargar a tu tarjeta una compra por valor de $"+modelo.params.getValorDestacado()+". estas seguro? ");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Si, Aceptar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Log.v("ok", "ok");

                enviar_info_tpaga();

            }
        });

        alertDialogBuilder.setNegativeButton("No, Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getApplicationContext(),"You clicked over No",Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // generacion del pago con tarjeta

    public void enviar_info_tpaga() {


        String idTarjeta = modelo.datosOferente.getTarjetas().getToken();
        int num_cuotas = modelo.datosOferente.getTarjetas().getCuotas();

        final String idTarjeta2 = idTarjeta;
        final DatabaseReference key = referencia.push();
        final String keyFireTpaga = key.getKey();

        double iacAmount = Double.parseDouble("" + modelo.params.getValorDestacado()) * 0.08;
        int iacAmount_porcentaje = (int) iacAmount;
        int total_pago_tarjeta = modelo.params.getValorDestacado();

        final Call<TPPAgoResponse> tppAgoResponseCall = MyApiAdapter.getApiService().generarPago(TPPAgo.create("WooPets", iacAmount_porcentaje, idTarjeta, total_pago_tarjeta, 0, num_cuotas, "" + keyFireTpaga, "COP"));
        tppAgoResponseCall.enqueue(new Callback<TPPAgoResponse>() {
            @Override
            public void onResponse(Call<TPPAgoResponse> call, Response<TPPAgoResponse> response) {
                // response.body(); me devuelve null en el rawResponse en la opcion de mensaje me devulve pago requerido
                System.out.print("Code" + response.code());

                if (response.code() == 201) {
                    TPPAgoResponse result = response.body();
                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                    //Toast.makeText(getApplicationContext(),"¿¿ "+gson.toJson(result)+"  -_-"+response,Toast.LENGTH_LONG).show();

                    if (response.body().getTransactionInfo().getStatus().equals("authorized")) {
                        showalertEnviar2(response.body().getTransactionInfo().getAuthorizationCode(), "" + response.body().getId(), response.body().getPaymentTransaction(), idTarjeta2);

                        System.out.print("" + response.body().getTransactionInfo().getStatus());
                        Log.v("Status", "Status" + response.body().getTransactionInfo().getStatus());
                        return;

                    } else {
                        Toast.makeText(getApplicationContext(), "No se pudo realizar la transacción.", Toast.LENGTH_SHORT).show();
                    }
                } else if (response.code() >= 500) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Error conectando con el servidor", Toast.LENGTH_SHORT).show();
                    Log.v("TagInfo", "Body " + "Error conectando con el servidor" + response.errorBody());
                    return;
                } else if (response.code() == 422) {
                    progressDialog.dismiss();

                    Toast.makeText(getApplicationContext(), "Valide su información o correo invalido", Toast.LENGTH_SHORT).show();

                } else if (response.code() == 402) {
                    String resbodi = "";
                    JSONObject json;
                    String msmgson = "";

                    progressDialog.dismiss();
                    response.raw();
                    String r1 = response.raw().message();
                    response.errorBody();
                    try {
                        resbodi = response.errorBody().string();
                        JSONObject jsonObj = null;
                        try {
                            jsonObj = new JSONObject(resbodi);
                            msmgson = jsonObj.get("errorMessage").toString();
                            Log.v("msmgson", "msmgson" + msmgson);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    Toast.makeText(getApplicationContext(), msmgson + ". Puedes intentar con otra tarjeta o pagar en efectivo", Toast.LENGTH_SHORT).show();

                    return;
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "No se pudo realizar la transacción.", Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            @Override
            public void onFailure(Call<TPPAgoResponse> call, Throwable t) {


                progressDialog.dismiss();

                Log.v("status2", "status2" + t.getMessage() + "..." + t.getCause());
                System.out.print("" + t.getMessage() + "..." + t.getCause());

                Toast.makeText(getApplicationContext(), "La transacción no es posible en este momento. Intentelo más tarde.", Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(),""+t.getLocalizedMessage()+" "+t.getMessage(),Toast.LENGTH_SHORT).show();

                return;
            }
        });

    }

    public void showalertEnviar2(final String authorizationCode, final String idPago, final String paymentTransaction, final String idTarjeta) {


        if (estaConectado()) {

            validacion_pago(authorizationCode, idPago, paymentTransaction, idTarjeta);

            /*   modelo.usuario.setCelular(tel);
            modelo.usuario.setDireccion(dir);
            modelo.orden.limpiarPrompagandas();
            comandoUsuarios.setActualizarConductor(modelo.usuario.getCelular(), modelo.usuario.getDireccion(), usuarioId);
            comandoOrdenTarjeta.setCrearOrden(tel, dir, coment, usuarioId, authorizationCode,idPago,idTarjeta,paymentTransaction);
*/

        } else {
            progressDialog.dismiss();
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            String titulo = "Sin Internet";
            String mensaje = "Sin Conexión a Internet";

            showAlertSinInternet(titulo, mensaje);
            return;
        }


    }

    //tarjeta


    public void validacion_pago(final String authorizationCode, final String idPago, final String paymentTransaction, final String idTarjeta) {

        if (estaConectado()) {

            comandoOrdenTarjeta.setCrerDestacado(modelo.datosOferente.getCelular(), modelo.datosOferente.getDireccion(), "Destacado", modelo.uid, authorizationCode, idPago, paymentTransaction, idTarjeta);

        } else {
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            String titulo = "Sin Internet";
            String mensaje = "Sin Conexión a Internet";

            showAlertSinInternet(titulo, mensaje);
            return;
        }
    }

    //fin tarjeta


    //conexion internet
    //validacion conexion internet
    protected Boolean estaConectado() {
        if (conectadoWifi()) {
            Log.v("wifi", "Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        } else {
            if (conectadoRedMovil()) {
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            } else {
                String titulo = "Sin Internet";
                String mensaje = "Sin Conexión a Internet";
                showAlertSinInternet(titulo, mensaje);
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    protected Boolean conectadoWifi() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }


    public void showAlertSinInternet(String titulo, String mensaje) {

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + titulo);

        // set dialog message
        alertDialogBuilder
                .setMessage("" + mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    @Override
    public void destacadoEnviadoTarjeta() {

        comandoRegitroTpaga.updateEstado();
        Intent i = new Intent(getApplicationContext(), CompraExiosaDestacdo.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        finish();
    }

    @Override
    public void errorEnviadoTarjeta() {
        Toast.makeText(getApplicationContext(), "Error al generar el pago, intente de nuevo", Toast.LENGTH_LONG).show();
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

}
