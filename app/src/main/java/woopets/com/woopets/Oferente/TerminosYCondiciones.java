package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import woopets.com.woopets.Oferente.Comandos.ComandoTerminosYCondiciones;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class TerminosYCondiciones extends Activity implements ComandoTerminosYCondiciones.OnComandoTerminosYCondicionesChangeListener {

    ComandoTerminosYCondiciones comandoTerminosYCondiciones;
    TextView terminos_condiciones_texto;
    Button terminos_condiciones;
    Modelo modelo = Modelo.getInstance();
    String uidProducto;
    String abiertascerrada;
    int posicionServicio = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_terminos_ycondiciones);

        comandoTerminosYCondiciones = new ComandoTerminosYCondiciones(this);
        comandoTerminosYCondiciones.getTerminos_y_Condiciones();

        terminos_condiciones_texto = (TextView) findViewById(R.id.terminos_condiciones_texto);
        terminos_condiciones = (Button) findViewById(R.id.terminos_condiciones);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

    }

    @Override
    public void terminos_y_Condiciones() {

        terminos_condiciones.setText(""+modelo.classTerminosYCondiciones.getTitulo());
        terminos_condiciones_texto.setText(""+modelo.classTerminosYCondiciones.getTexto());
    }




    @Override
    public void onBackPressed() {

        if(modelo.vista == 1){


            Bundle bundle2 = getIntent().getExtras();
            uidProducto = bundle2.getString("idProducto");
            posicionServicio = bundle2.getInt("posicionServicio");
            abiertascerrada = bundle2.getString("abiertascerrada");
            modelo.vista=0;

        }

        finish();
        super.onBackPressed();
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }
}
