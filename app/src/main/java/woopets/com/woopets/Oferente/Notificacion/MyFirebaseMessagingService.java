package woopets.com.woopets.Oferente.Notificacion;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;;

import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

/**
 * Created by tacto on 16/05/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    int notificationID = 1;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

        Log.d(TAG,"Notification Message Body2"+remoteMessage.getFrom());
        Log.d(TAG,"Notification Message Body3"+remoteMessage.getTo());
        Log.d(TAG,"Notification Message Body4"+remoteMessage.getData());
        Log.d(TAG,"Notification Message Body5"+remoteMessage.getMessageId());
        Log.d(TAG,"Notification Message Body6"+remoteMessage.getMessageType());
        Log.d(TAG,"Notification Message Body7"+remoteMessage.getCollapseKey());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getTitle());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getColor());

        displayNotification(remoteMessage.getNotification().getBody());
        //showNotification(remoteMessage.getNotification().getBody());


    }

    protected void displayNotification(String body){
        Intent i = new Intent(this, Splash.class);
        i.putExtra("notificationID", notificationID);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, 0);
        NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

       // CharSequence ticker ="Nueva entrada en SekthDroid";
        CharSequence contentTitle = "WooPets";
        CharSequence contentText = ""+body;
        Notification noti = new NotificationCompat.Builder(this)
                .setContentIntent(pendingIntent)
               // .setTicker(ticker)
                .setAutoCancel(true)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setSmallIcon(R.mipmap.ic_launcher)
               // .addAction(R.mipmap.ic_launcher, ticker, pendingIntent)
                .setVibrate(new long[] {100, 250, 100, 500})
                .build();
        nm.notify(notificationID, noti);
    }




    //otra nofificacion
    /*private void showNotification(String sms){
        Intent i = new Intent(this, Splash.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("FCM test")
                .setContentText(sms)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0,builder.build());

    }*/
}