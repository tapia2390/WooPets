package woopets.com.woopets.Oferente.ModelOferente;

/**
 * Created by tacto on 6/12/17.
 */

public class DatosTpaga {

    String apellidos = "";
    String correo = "";
    String idClienteTpaga = "";
    String nombres = "";
    String telefono = "";

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getIdClienteTpaga() {
        return idClienteTpaga;
    }

    public void setIdClienteTpaga(String idClienteTpaga) {
        this.idClienteTpaga = idClienteTpaga;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
