package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import woopets.com.woopets.Oferente.AdapterOferente.PreguntasFrecuentesAdapter;
import woopets.com.woopets.Oferente.Comandos.ComandoPreguntasFrecuentes;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class PreguntasFrecuentes extends Activity  implements PreguntasFrecuentesAdapter.AdapterCallback, ComandoPreguntasFrecuentes.OnComandoPreguntasFrecuentesChangeListener{

    ComandoPreguntasFrecuentes comandoPreguntasFrecuentes;
    PreguntasFrecuentesAdapter mAdapter;
    ListView list_preguntas_frecuentes;
    TextView text_sin_datos;
    Modelo modelo = Modelo.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_my_preguntas_frecuentes);

        comandoPreguntasFrecuentes = new ComandoPreguntasFrecuentes(this);
        comandoPreguntasFrecuentes.getListPreguntasfrecuentes();


        list_preguntas_frecuentes = (ListView) findViewById(R.id.list_preguntas_frecuentes);
        text_sin_datos = (TextView) findViewById(R.id.text_sin_datos);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void displayLista(){
       mAdapter = new PreguntasFrecuentesAdapter(this,this);
        list_preguntas_frecuentes.setAdapter(mAdapter);
    }
    @Override
    public void getPreguntasFrecuentes() {

        if(modelo.classpreguntasFrecuentes.size() > 0){
            text_sin_datos.setVisibility(View.GONE);
            displayLista();
        }

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }
}
