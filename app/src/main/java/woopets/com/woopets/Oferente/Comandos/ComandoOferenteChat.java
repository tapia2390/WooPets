package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 28/12/17.
 */

public class ComandoOferenteChat {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo  = Modelo.getInstance();

    public interface OnComandoOferenteChatoChangeListener {

        void cmChat();
        
    }


    //interface del listener de la actividad interesada
    private OnComandoOferenteChatoChangeListener mListener;

    public ComandoOferenteChat(OnComandoOferenteChatoChangeListener mListener){

        this.mListener = mListener;
    }

    public void getDatosChat(){

    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoOferenteChatoChangeListener sDummyCallbacks = new OnComandoOferenteChatoChangeListener()
    {


        @Override
        public void cmChat()
        {}

        

    };
}
