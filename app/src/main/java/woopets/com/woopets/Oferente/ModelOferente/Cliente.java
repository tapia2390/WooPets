package woopets.com.woopets.Oferente.ModelOferente;

import java.util.ArrayList;

/**
 * Created by tacto on 8/11/17.
 */

public class Cliente {

    String id = "";
    String nombre = "";
    String apellido = "";
    String celular = "";
    String correo = "";
    String tokens = "";
    ArrayList<Direccion> listDirecion = new ArrayList<Direccion>();




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTokens() {
        return tokens;
    }

    public void setTokens(String tokens) {
        this.tokens = tokens;
    }

    public ArrayList<Direccion> getListDirecion() {
        return listDirecion;
    }

    public void setListDirecion(ArrayList<Direccion> listDirecion) {
        this.listDirecion = listDirecion;
    }
}
