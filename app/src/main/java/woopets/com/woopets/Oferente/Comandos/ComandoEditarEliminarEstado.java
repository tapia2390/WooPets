package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Pedido;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;

/**
 * Created by tacto on 13/12/17.
 */

public class ComandoEditarEliminarEstado {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    public interface OnComandoEditarEliminarEstadoChangeListener {

        void cargoEstado();

        void cargoEliminarPublicacion();

        void cargoAbiertas();

        void cargoCerradas();

        void cargoTipo();

        void cargoArticulo();

        void cargoFoto();
    }

    //interface del listener de la actividad interesada
    private ComandoEditarEliminarEstado.OnComandoEditarEliminarEstadoChangeListener mListener;

    public ComandoEditarEliminarEstado(ComandoEditarEliminarEstado.OnComandoEditarEliminarEstadoChangeListener mListener) {

        this.mListener = mListener;
    }

    public void setEstadoPublicacion(boolean estado, String idProducto) {

        final DatabaseReference ref = database.getReference("productos/" + idProducto + "/activo/");//ruta path
        ref.setValue(estado);
        modelo.productosOferentesBoolean.get(0).setActivo(estado);
        mListener.cargoEstado();
    }


    public void validarComprasAbiertasCerradas(final String abiertaCerrada, final String idPublicacion) {
        DatabaseReference ref;
        Query query;
        if (abiertaCerrada.equals("abiertas")) {
            ref = database.getReference("compras/abiertas/");//ruta path
            query = ref.orderByChild("idOferente").equalTo(modelo.uid);
        } else {
            ref = database.getReference("compras/cerradas/");//ruta path
            query = ref.orderByChild("idOferente").equalTo(modelo.uid);
        }

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {
                DataSnapshot snapPedido;
                snapPedido = (DataSnapshot) snap.child("pedido");
                for (DataSnapshot dspedido : snapPedido.getChildren()) {

                    Pedido pedido = new Pedido();

                    pedido.setId(dspedido.child("idPublicacion").getValue().toString());
                    if (abiertaCerrada.equals("abiertas")) {
                        modelo.pedidoAbiertas.add(pedido);
                        mListener.cargoAbiertas();
                    } else {
                        modelo.pedidoCerardas.add(pedido);
                        mListener.cargoCerradas();
                    }

                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        query.addChildEventListener(listener);
        modelo.hijosListener.put(query, listener);
    }

    public void setEliminar(String idProducto) {
        final DatabaseReference ref = database.getReference("productos/" + idProducto + "/");//ruta path
        ref.removeValue();


        FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference storageReference = storage.getReference().child("productos/" + idProducto + "/");
        storageReference.delete();

        mListener.cargoEliminarPublicacion();
    }

    public void setActualizar(String idProducto) {

    }

    public void setActualizarTipo( final String idProducto , final String text_target) {
;
        FirebaseStorage storage = FirebaseStorage.getInstance();
        final DatabaseReference ref = database.getReference("productos/" + idProducto + "/target/");//ruta path
        ref.setValue(text_target);
        mListener.cargoTipo();
    }

    public void setActualizarArticulo(String idProducto) {


        final DatabaseReference ref = database.getReference("productos/"+idProducto+"/");//ruta path
        Map<String, Object> enviarRegistroProducto = new HashMap<String, Object>();


        enviarRegistroProducto.put("nombre", modelo.productosOferente.getTitulo());
        enviarRegistroProducto.put("descripcion", modelo.productosOferente.getDescripcion());
        enviarRegistroProducto.put("precio", modelo.productosOferente.getPrecio());

        //si es un producto es false
        if(modelo.servicio_producto == false){
            enviarRegistroProducto.put("stock", modelo.productosOferente.getCantidad());
        }

        //si es un servicio es true
        Map<String,Object> horario = new HashMap<String,Object>();
        if(modelo.diasDeLaSemana.size()>0){

            Map<String,Object> semana = new HashMap<String,Object>();
            semana.put("dias",modelo.productosOferente.getHorarioDiasDeLaSemana());
            semana.put("horaCierre",modelo.productosOferente.getHorarioCierreDiasDeLaSemana());
            semana.put("horaInicio",modelo.productosOferente.getHorarioInicioDiasDeLaSemana());
            semana.put("sinJornadaContinua", modelo.productosOferente.getSinJornadaContinua());
            horario.put("Semana",semana);
            enviarRegistroProducto.put("horario",horario);

        }

        if(modelo.finDeSemanaFestivos.size()>0){

            Map<String,Object> findesemana = new HashMap<String,Object>();
            findesemana.put("dias", modelo.productosOferente.getHorarioDiasFinDeSemana());

            findesemana.put("horaCierre",modelo.productosOferente.getHorarioCierreDiasFinDeSemana());
            findesemana.put("horaInicio",modelo.productosOferente.getHorarioInicioDiasFinDeSemana());
            findesemana.put("sinJornadaContinua",modelo.productosOferente.getFinsinJornadaContinua());
            horario.put("FinDeSemana",findesemana);
            enviarRegistroProducto.put("horario",horario);

        }

        if(modelo.servicio_producto == true){
            enviarRegistroProducto.put("duracion",modelo.productosOferente.getDuracion());
            enviarRegistroProducto.put("servicioEnDomicilio",modelo.servicioEnDomicilio);
            enviarRegistroProducto.put("duracionMedida",modelo.productosOferente.getDuracionMedida());
        }

        //enviarRegistroProducto.put("servicio",modelo.productosOferente.getServicio());

        ref.updateChildren(enviarRegistroProducto, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {

                    mListener.cargoArticulo();
                } else {
                    mListener.cargoArticulo();
                }
            }
        });

    }

    public void setActualizarFoto(String idProducto) {

        /*FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference storageReference = storage.getReference().child("productos/" + idProducto);
        storageReference.delete();

*/

        final DatabaseReference ref = database.getReference("productos/"+idProducto+"/");//ruta path
        Map<String, Object> enviarRegistroProducto = new HashMap<String, Object>();


        Collections.sort(modelo.productosOferente.getFotos());

        //arbol de fotos
        Map<String,Object> fotos = new HashMap<String,Object>();

        for(int i = 0; i < modelo.productosOferente.getFotos().size(); i++){

            int contador = i+1;

            fotos.put("Foto"+contador,"Foto"+contador+".png");
        }

        enviarRegistroProducto.put("fotos",fotos);


        ref.updateChildren(enviarRegistroProducto, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {

                    mListener.cargoFoto();
                } else {
                    mListener.cargoFoto();
                }
            }
        });
    }



    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoEditarEliminarEstadoChangeListener sDummyCallbacks = new OnComandoEditarEliminarEstadoChangeListener() {
        @Override
        public void cargoEstado() {

        }

        @Override
        public void cargoEliminarPublicacion() {

        }

        @Override
        public void cargoAbiertas() {

        }

        @Override
        public void cargoCerradas() {

        }

        @Override
        public void cargoTipo() {

        }

        @Override
        public void cargoArticulo() {

        }

        @Override
        public void cargoFoto() {

        }

    };
}
