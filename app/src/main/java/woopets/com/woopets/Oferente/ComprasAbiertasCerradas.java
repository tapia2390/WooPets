package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import woopets.com.woopets.Oferente.AdapterOferente.MisPublicacioneAdapter;
import woopets.com.woopets.Oferente.AdapterOferente.MisPublicacioneAdapterInactivas;
import woopets.com.woopets.Oferente.AdapterOferente.MisVentasAbiertasCerradasAdapter;
import woopets.com.woopets.Oferente.AdapterOferente.MisVentasAbiertasCerradasInactivasAdapter;
import woopets.com.woopets.Oferente.Comandos.ComandoComprasAbiertasCerradas;
import woopets.com.woopets.Oferente.Comandos.ComandoGetProducto;
import woopets.com.woopets.Oferente.Comandos.ComandoRegistroProducto;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;


public class ComprasAbiertasCerradas extends Activity implements MisVentasAbiertasCerradasAdapter.AdapterCallback, MisVentasAbiertasCerradasInactivasAdapter.AdapterCallback, ComandoComprasAbiertasCerradas.OnComandoComprasAbiertasCerradasoChangeListener, ComandoRegistroProducto.OnComandoRegistroProductChangeListener {

    LinearLayout layout_publicaciones, layout_list_publicaciones_activos, layout_list_publicaciones_inactivos;
    ListView lista_mis_publicaciones, lista_mis_publicaciones_inactivas;
    Button button7;
    Button button8;


    private MisVentasAbiertasCerradasInactivasAdapter mAdapter2;
    private MisVentasAbiertasCerradasAdapter mAdapter;
    Modelo modelo = Modelo.getInstance();

    ComandoComprasAbiertasCerradas comandoComprasAbiertasCerradas;
    ComandoRegistroProducto comandoGetProducto;
    boolean activo = true;
    boolean inactivo = false;
    String abiertaCerrada = modelo.abiertas_cerrada;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_compras_abiertas_cerradas);


        layout_publicaciones = (LinearLayout) findViewById(R.id.layout_publicaciones);
        layout_list_publicaciones_activos = (LinearLayout) findViewById(R.id.layout_list_publicaciones_activos);
        layout_list_publicaciones_inactivos = (LinearLayout) findViewById(R.id.layout_list_publicaciones_inactivos);
        lista_mis_publicaciones = (ListView) findViewById(R.id.lista_mis_publicaciones);
        lista_mis_publicaciones_inactivas = (ListView) findViewById(R.id.lista_mis_publicaciones_inactivas);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


    }


    @Override
    protected void onStart() {
        super.onStart();
        lista_mis_publicaciones.setAdapter(null);
        comandoComprasAbiertasCerradas = new ComandoComprasAbiertasCerradas(this);

        //comandoGetProducto = new ComandoRegistroProducto(this);
        //comandoGetProducto.getProducto();

        modelo.comprasProductos.clear();


        if (modelo.abiertas_cerrada.equals("abiertas")) {
            button7.setBackgroundResource(R.drawable.btnactivoazul);
            button8.setBackgroundResource(R.drawable.btnactivogris);
            //Toast.makeText(getApplicationContext(), "abiertas", Toast.LENGTH_SHORT).show();
            activo = true;
            inactivo = false;
            abiertaCerrada = "abiertas";
            modelo.abiertas_cerrada = "abiertas";
            comandoComprasAbiertasCerradas.comprasAbiertasCerradas_(modelo.uid, abiertaCerrada);

        } else {
            //button8.setBackgroundResource(R.drawable.btnactivoazul);
            button8.setBackgroundResource(R.drawable.btnactivoazul);
            button7.setBackgroundResource(R.drawable.btnactivogris);
            // Toast.makeText(getApplicationContext(), "cerradas", Toast.LENGTH_SHORT).show();
            activo = false;
            inactivo = true;
            abiertaCerrada = "cerradas";
            modelo.abiertas_cerrada = "cerradas";
            comandoComprasAbiertasCerradas.comprasAbiertasCerradas_(modelo.uid, abiertaCerrada);
        }


    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void displayListaActivas() {



        modelo.comprasProductos.clear();
        if (modelo.comprasProductosAbiertasCerradases2.get(modelo.comprasProductosAbiertasCerradases2.size() - 1).getPedidos().get(0).getProductosOferentes().size() > 0) {
            activo = true;
            inactivo = false;
            abiertaCerrada = "abiertas";
            modelo.abiertas_cerrada = "abiertas";

            mAdapter = new MisVentasAbiertasCerradasAdapter(this, this);
            lista_mis_publicaciones.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
        else {
            lista_mis_publicaciones.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

        }

    }


    private void displayListaInactivas() {
        modelo.comprasProductos.clear();


        if (modelo.comprasProductosAbiertasCerradases2.get(modelo.comprasProductosAbiertasCerradases2.size() - 1).getPedidos().get(0).getProductosOferentes().size() > 0) {

            activo = false;
            inactivo = true;
            abiertaCerrada = "cerradas";
            modelo.abiertas_cerrada = "cerradas";

            mAdapter2 = new MisVentasAbiertasCerradasInactivasAdapter(this, this);
            lista_mis_publicaciones_inactivas.setAdapter(mAdapter2);
            mAdapter2.notifyDataSetChanged();
        }else{
            lista_mis_publicaciones.setAdapter(mAdapter2);
            mAdapter2.notifyDataSetChanged();

        }
    }

    public void activas(View v) {

        if(mAdapter != null){
            mAdapter.notifyDataSetChanged();
        }

            modelo.comprasProductos.clear();
            button7.setBackgroundResource(R.drawable.btnactivoazul);
            button8.setBackgroundResource(R.drawable.btnactivogris);
            //Toast.makeText(getApplicationContext(), "abiertas", Toast.LENGTH_SHORT).show();
            activo = true;
            inactivo = false;
            abiertaCerrada = "abiertas";
            modelo.abiertas_cerrada = "abiertas";
            comandoComprasAbiertasCerradas.comprasAbiertasCerradas_(modelo.uid, abiertaCerrada);

    }

    public void inactivas(View v) {

        if(mAdapter2 != null){
            mAdapter2.notifyDataSetChanged();
        }
            modelo.comprasProductos.clear();
            lista_mis_publicaciones.setAdapter(null);
            button8.setBackgroundResource(R.drawable.btnactivoazul);
            button7.setBackgroundResource(R.drawable.btnactivogris);
            // Toast.makeText(getApplicationContext(), "cerradas", Toast.LENGTH_SHORT).show();
            activo = false;
            inactivo = true;
            abiertaCerrada = "cerradas";
            modelo.abiertas_cerrada = "cerradas";
            comandoComprasAbiertasCerradas.comprasAbiertasCerradas_(modelo.uid, abiertaCerrada);



    }


    @Override
    public void comprasAbiertasCerradasoOferente() {


        if (activo == true) {

            if(modelo.comprasProductosAbiertasCerradases2.size() == 0){
                displayListaActivas();
            }

            if (modelo.comprasProductosAbiertasCerradases2.size() > 0 && modelo.comprasProductosAbiertasCerradases2.get(modelo.comprasProductosAbiertasCerradases2.size() - 1).getCliente().size() > 0 && modelo.comprasProductosAbiertasCerradases2.size() > 0 && modelo.comprasProductosAbiertasCerradases2.get(modelo.comprasProductosAbiertasCerradases2.size() - 1).getPedidos().get(0).getProductosOferentes().size() > 0) {
                layout_list_publicaciones_activos.setVisibility(View.VISIBLE);
                layout_list_publicaciones_inactivos.setVisibility(View.GONE);
                layout_publicaciones.setVisibility(View.GONE);
                displayListaActivas();
            } else {
                layout_list_publicaciones_inactivos.setVisibility(View.GONE);
                layout_list_publicaciones_activos.setVisibility(View.GONE);
                layout_publicaciones.setVisibility(View.VISIBLE);

            }

        } else if (inactivo == true) {


            if(modelo.comprasProductosAbiertasCerradases2.size() == 0){
                displayListaInactivas();
            }

            if (modelo.comprasProductosAbiertasCerradases2.size() > 0 && modelo.comprasProductosAbiertasCerradases2.get(modelo.comprasProductosAbiertasCerradases2.size() - 1).getCliente().size() > 0 && modelo.comprasProductosAbiertasCerradases2.size() > 0 && modelo.comprasProductosAbiertasCerradases2.get(modelo.comprasProductosAbiertasCerradases2.size() - 1).getPedidos().get(0).getProductosOferentes().size() > 0) {
                layout_list_publicaciones_inactivos.setVisibility(View.VISIBLE);
                layout_list_publicaciones_activos.setVisibility(View.GONE);
                layout_publicaciones.setVisibility(View.GONE);
                displayListaInactivas();
            } else {
                layout_list_publicaciones_inactivos.setVisibility(View.GONE);
                layout_list_publicaciones_activos.setVisibility(View.GONE);
                layout_publicaciones.setVisibility(View.VISIBLE);
                displayListaInactivas();

            }

        }


    }

    @Override
    public void registroProductoOferente() {

    }

    @Override
    public void getProductoOferente() {

        // Toast.makeText(getApplicationContext(), "abiertas", Toast.LENGTH_SHORT).show();
        activo = true;
        inactivo = false;
        comandoComprasAbiertasCerradas.comprasAbiertasCerradas_(modelo.uid, abiertaCerrada);

        //comandoComprasAbiertasCerradas.comprasAbiertasCerradas_(modelo.uid,abiertaCerrada);

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

}
