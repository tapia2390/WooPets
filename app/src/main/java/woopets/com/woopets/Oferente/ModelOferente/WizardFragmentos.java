package woopets.com.woopets.Oferente.ModelOferente;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import woopets.com.woopets.R;


public class WizardFragmentos extends Fragment {


    int wizard_page_position;


    public WizardFragmentos(int position) {

        this.wizard_page_position = position;
    }


     @Override
     public void onDestroy() {
         super.onDestroy();

     }

     @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layout_id = R.layout.page1;
        switch (wizard_page_position) {
            case 0:
                layout_id = R.layout.page1;
                break;


        }

        return inflater.inflate(layout_id, container, false);
    }


     public boolean onKeyDown(int keyCode, KeyEvent event) {
         // TODO Auto-generated method stub
         if (keyCode == event.KEYCODE_BACK) {
             Log.v("cerrar","cerrar");
         }
         return false;
     }

}
