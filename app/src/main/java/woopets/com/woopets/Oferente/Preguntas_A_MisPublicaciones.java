package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import woopets.com.woopets.Oferente.AdapterOferente.PreguntasPublicacionesAdapter;
import woopets.com.woopets.Oferente.Comandos.ComandoPreguntaPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class Preguntas_A_MisPublicaciones extends Activity implements PreguntasPublicacionesAdapter.AdapterCallback, ComandoPreguntaPublicaciones.OnComandoPreguntaPublicacionesChangeListener{

    TextView text_preguntas_publicaciones;
    ListView lista_pregunta_publicaciones;
    ComandoPreguntaPublicaciones comandoPreguntaPublicaciones;
    Modelo modelo = Modelo.getInstance();
    private PreguntasPublicacionesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_preguntas__a__mis_publicaciones);


        text_preguntas_publicaciones = (TextView) findViewById(R.id.text_preguntas_publicaciones);
        lista_pregunta_publicaciones = (ListView) findViewById(R.id.lista_pregunta_publicaciones);
        comandoPreguntaPublicaciones  = new ComandoPreguntaPublicaciones(this);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }



    }


    @Override
    protected void onStart() {
        comandoPreguntaPublicaciones.getPreguntasPublicaciones();
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void displayLista(){

        mAdapter  = new PreguntasPublicacionesAdapter(this,this);
        lista_pregunta_publicaciones.setAdapter(mAdapter);
    }


    @Override
    public void preguntasMisPublicaciones() {

        if(modelo.classPreguntas_a_misPublicaciones.size() > 0){
            if(modelo.classPreguntas_a_misPublicaciones.get(modelo.classPreguntas_a_misPublicaciones.size()-1).listaProductosOferentes.size() > 0){
                text_preguntas_publicaciones.setVisibility(View.GONE);
                displayLista();
            }
        }

    }

    @Override
    public void preguntasMisPublicacionesNotificacion() {

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

}
