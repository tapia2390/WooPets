package woopets.com.woopets.Oferente.ModelOferente;

import java.util.ArrayList;

/**
 * Created by tacto on 23/08/17.
 */

public class ProductosOferente {

    String id = "";
    boolean activo = false;
    String titulo = "";
    String categoria = "";
    int cantidad = 0;
    String subcategoria = "";
    String descripcion = "";
    boolean destacado = false;
    String horarioDiasDeLaSemana = "";
    String horarioInicioDiasDeLaSemana = "";
    String horarioCierreDiasDeLaSemana = "";
    boolean sinJornadaContinua = false;
    String horarioDiasFinDeSemana = "";
    String horarioInicioDiasFinDeSemana = "";
    String horarioCierreDiasFinDeSemana = "";
    boolean finsinJornadaContinua = false;
    String idOferente = "";
    String nombre = "";
    String precio = "";
    boolean servicio = false;
    String target = "";
    boolean servicioEnDomicilio;
    int duracion = 0;
    String duracionMedida = "";
    String fechaCreacion = "";
    long timestamp;
    boolean sinJornadaContinuaSemana;
    boolean sinJornadaContinuaFinDeSemana;

    ArrayList<String> fotos = new ArrayList<String>();

    public ProductosOferente(String key) {

        this.id = key;
    }

    public ProductosOferente() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }


    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        this.subcategoria = subcategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean getDestacado() {
        return destacado;
    }

    public void setDestacado(boolean destacado) {
        this.destacado = destacado;
    }

    public String getHorarioDiasDeLaSemana() {
        return horarioDiasDeLaSemana;
    }

    public void setHorarioDiasDeLaSemana(String horarioDiasDeLaSemana) {
        this.horarioDiasDeLaSemana = horarioDiasDeLaSemana;
    }

    public String getHorarioInicioDiasDeLaSemana() {
        return horarioInicioDiasDeLaSemana;
    }

    public void setHorarioInicioDiasDeLaSemana(String horarioInicioDiasDeLaSemana) {
        this.horarioInicioDiasDeLaSemana = horarioInicioDiasDeLaSemana;
    }

    public String getHorarioCierreDiasDeLaSemana() {
        return horarioCierreDiasDeLaSemana;
    }

    public void setHorarioCierreDiasDeLaSemana(String horarioCierreDiasDeLaSemana) {
        this.horarioCierreDiasDeLaSemana = horarioCierreDiasDeLaSemana;
    }

    public String getHorarioDiasFinDeSemana() {
        return horarioDiasFinDeSemana;
    }

    public void setHorarioDiasFinDeSemana(String horarioDiasFinDeSemana) {
        this.horarioDiasFinDeSemana = horarioDiasFinDeSemana;
    }

    public String getHorarioInicioDiasFinDeSemana() {
        return horarioInicioDiasFinDeSemana;
    }

    public void setHorarioInicioDiasFinDeSemana(String horarioInicioDiasFinDeSemana) {
        this.horarioInicioDiasFinDeSemana = horarioInicioDiasFinDeSemana;
    }

    public String getHorarioCierreDiasFinDeSemana() {
        return horarioCierreDiasFinDeSemana;
    }

    public void setHorarioCierreDiasFinDeSemana(String horarioCierreDiasFinDeSemana) {
        this.horarioCierreDiasFinDeSemana = horarioCierreDiasFinDeSemana;
    }

    public String getIdOferente() {
        return idOferente;
    }

    public void setIdOferente(String idOferente) {
        this.idOferente = idOferente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public boolean getServicio() {
        return servicio;
    }

    public void setServicio(boolean servicio) {
        this.servicio = servicio;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }


    public boolean getServicioEnDomicilio() {
        return servicioEnDomicilio;
    }

    public void setServicioEnDomicilio(boolean servicioEnDomicilio) {
        this.servicioEnDomicilio = servicioEnDomicilio;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getDuracionMedida() {
        return duracionMedida;
    }

    public void setDuracionMedida(String duracionMedida) {
        this.duracionMedida = duracionMedida;
    }

    public boolean getSinJornadaContinua() {
        return sinJornadaContinua;
    }

    public void setSinJornadaContinua(boolean sinJornadaContinua) {
        this.sinJornadaContinua = sinJornadaContinua;
    }

    public boolean getFinsinJornadaContinua() {
        return finsinJornadaContinua;
    }

    public void setFinsinJornadaContinua(boolean finsinJornadaContinua) {
        this.finsinJornadaContinua = finsinJornadaContinua;
    }

    public ArrayList<String> getFotos() {
        return fotos;
    }

    public void setFotos(ArrayList<String> fotos) {
        this.fotos = fotos;
    }

    public boolean getSinJornadaContinuaSemana() {
        return sinJornadaContinuaSemana;
    }

    public void setSinJornadaContinuaSemana(boolean sinJornadaContinuaSemana) {
        this.sinJornadaContinuaSemana = sinJornadaContinuaSemana;
    }

    public boolean getSinJornadaContinuaFinDeSemana() {
        return sinJornadaContinuaFinDeSemana;
    }

    public void setSinJornadaContinuaFinDeSemana(boolean sinJornadaContinuaFinDeSemana) {
        this.sinJornadaContinuaFinDeSemana = sinJornadaContinuaFinDeSemana;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
