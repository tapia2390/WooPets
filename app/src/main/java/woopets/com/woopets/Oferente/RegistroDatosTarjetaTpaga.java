package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.FirebaseFunctionsException;
import com.google.firebase.functions.HttpsCallableResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import woopets.com.woopets.Modelo.cliente.Cliente;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Oferente.Comandos.ComandoOrdenTarjeta;
import woopets.com.woopets.Oferente.Comandos.ComandoRegitroTpaga;
import woopets.com.woopets.Oferente.Comandos.ComandoTpaga;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.Oferente.tpaga.Model.CardTarjeta;
import woopets.com.woopets.Oferente.tpaga.Model.CustomerCard;
import woopets.com.woopets.Oferente.tpaga.Model.CustomerCardResponse;
import woopets.com.woopets.Oferente.tpaga.Model.Luhn;
import woopets.com.woopets.Oferente.tpaga.Model.TPPAgo;
import woopets.com.woopets.Oferente.tpaga.Model.TPPAgoResponse;
import woopets.com.woopets.Oferente.tpaga.Model.TarjetaResponse;
import woopets.com.woopets.Oferente.tpaga.io.MyApiAdapter;
import woopets.com.woopets.Oferente.tpaga.io.MyApiAdapterCardTarjeta;
import woopets.com.woopets.R;
import woopets.com.woopets.cliente.Comandos.CmdIp;
import woopets.com.woopets.cliente.Comandos.CmdIp.OnIpListener;
import woopets.com.woopets.cliente.Comandos.CmdPayU;
import woopets.com.woopets.cliente.Comandos.CmdPayU.OnCheckTarjetas;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas;
import woopets.com.woopets.cliente.Comandos.ComandoTPaga;
import woopets.com.woopets.cliente.HomeCliente;

public class RegistroDatosTarjetaTpaga extends Activity  {



    EditText nombre, direccion,ciudad,estado, cedula, telefono, correo,numero, meso, ano, cuotas, codigo;

    Button btn_finalizar;
    ComandoRegitroTpaga comandoRegitroTpaga;
    private ProgressDialog progressDialog;
    Modelo modelo = Modelo.getInstance();
    ComandoTpaga comandoTpaga;
    ComandoOrdenTarjeta comandoOrdenTarjeta;
    final Context context = this;
    String pagoSharedPreference = "";

    private FirebaseFunctions mFunctions;

    FirebaseDatabase database = FirebaseDatabase.getInstance();

    MiniTarjeta mini;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registro_datos_tarjeta_tpaga);


        numero = (EditText) findViewById(R.id.numero);
        nombre = (EditText) findViewById(R.id.nombre);
        meso = (EditText) findViewById(R.id.mes);
        ano = (EditText) findViewById(R.id.ano);
        codigo = (EditText) findViewById(R.id.codigo);
        cuotas = (EditText) findViewById(R.id.cuotas);
        direccion = (EditText) findViewById(R.id.direccion);
        ciudad = (EditText) findViewById(R.id.ciudad);
        estado = (EditText) findViewById(R.id.estado);
        cedula = (EditText) findViewById(R.id.cedula);
        telefono = (EditText) findViewById(R.id.telefono);
        correo = (EditText) findViewById(R.id.correo);


        btn_finalizar = (Button) findViewById(R.id.button11);
        progressDialog = new ProgressDialog(this);


        mFunctions = FirebaseFunctions.getInstance();


        mini = modelo.getTarjetaActiva();

        if (mini == null) {  ///Pilas esto debe ser lo ultimo que se haga


            nombre.setText(modelo.datosOferente.getNombreContacto());


            direccion.setText(modelo.datosOferente.getDireccion());
            ciudad.setText(modelo.datosOferente.getDireccion());
            estado.setText(modelo.datosOferente.getDireccion());
            correo.setText(modelo.datosOferente.getCorreoElectronicoContacto());
            telefono.setText(modelo.datosOferente.getCelularContacto());
            cedula.setText(modelo.datosOferente.getNumeroDeDocumentoCntacto());

            return;
        }



        // Aca llega solo si tiene tarjeta anteriormente
        //nombre.setText(mini.nombre);
        direccion.setText(mini.direccion);
        ciudad.setText(mini.ciudad);
        estado.setText(mini.departamento);
        telefono.setText(mini.telefono);
        correo.setText(mini.correo);
        cedula.setText(mini.cedula);

        numero.setText(mini.lastFour);
        meso.setText("**");
        ano.setText("****");
        codigo.setText("****");
        cuotas.setText(mini.cuotas + "");
        nombre.setText("**** **********");


    }



    @Override
    public void onBackPressed() {
        atras2();
        super.onBackPressed();
    }

    public void atras2() {
        finish();
    }



    public boolean huboCambios(){


        if (nombre.getText().toString().equals("**** **********")  && (numero.getText().toString().equals(mini.lastFour)
                && meso.getText().toString().equals("**") && ano.getText().toString().equals("****"))
                && codigo.getText().toString().equals("****")){
            return false;
        }


        return true;

    }




    public void didTapFinalizar(View v) {


        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }catch (Exception x){

        }


        if (!huboCambios()){

            if (cuotas.getText().toString().equals("") || cuotas.getText().toString().equals("0")){
                cuotas.setError("Número de cuotas no válidas");
                return;
            }

            mini.cuotas = Integer.valueOf(cuotas.getText().toString());

            new ComandoTPaga(null).updateCuotasOferente(mini.id, mini.cuotas);
            finish();

            return;
        }



            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);     // current year
            int mMes = c.get(Calendar.MONTH)+1;   // current month
            int mes= 5;
            int year = 2020;
            int yearSuma = mYear+10;
            int yearSuma2 = 2020;
            int max_cuotas = 0;



            try {

                year = Integer.parseInt(ano.getText().toString());
                yearSuma = mYear+10;
                yearSuma2 = Integer.parseInt(ano.getText().toString());
                mes = Integer.parseInt(meso.getText().toString());
                max_cuotas  = Integer.parseInt(cuotas.getText().toString());


            }catch (Exception z){

            }


            if (meso.getText().toString().equals("") || meso.getText().toString().length() < 2) {
                meso.setError("Mes obligatorio");
                return;
            }

            if (ano.getText().toString().equals("") || ano.getText().toString().length() < 4) {
                ano.setError("Año no válido");
                return;
            }
            if (cuotas.getText().toString().equals("") || cuotas.getText().toString().equals("0")) {
                cuotas.setError("Número de cuotas no válidas");
                return;
            }
            if (cedula.getText().toString().length() < 5) {
                cedula.setError("Debes incluir tu cédula");
                cedula.requestFocus();
                return;
            }
            if (!TextUtils.isDigitsOnly(cedula.getText())) {
                cedula.setError("La cédula no puede contener letras");
                cedula.requestFocus();
                return ;
            }


            if (!TextUtils.isDigitsOnly(telefono.getText())) {
                telefono.setError("El teléfono no puede contener letras");
                telefono.requestFocus();
                return;
            }

            if (telefono.getText().toString().length() == 7 || telefono.getText().toString().length() == 10) {

            } else {
                telefono.setError("El télefono esta muy largo o muy corto");
                telefono.requestFocus();
                return;

            }


        if (!Utility.isSoloLetrasPuntosEspacios(nombre.getText().toString())){
            nombre.setError("El nombre contiene caracteres incorrectos");
            nombre.requestFocus();
            return;
        }

            if(nombre.getText().toString().equals("") || nombre.getText().toString().length()<4){
                nombre.setError("Nombre es obligatorio");
                return;
            }
            if(Utility.LunCheck((numero.getText().toString())) == false ) {
                numero.setError("Número de la tarjeta es incorrecto");
                return;
            }

            if(numero.getText().toString().equals("") ){
                numero.setError("Número de la tarjeta de crédito incorrecto");
                return;
            }
            if(numero.getText().toString().length() < 13 ||  numero.getText().toString().length() > 19){
                numero.setError("Número de la tarjeta de crédito incorrecto");
                return;
            }


            if( Utility.getTipoCreditCard(numero.getText().toString()).equals("")) {
                numero.setError("Solo se aceptan VISA, MASTER, AMERICAN y DINERS");
                return;
            }

            if(meso.getText().toString().equals("")){
                meso.setError("Mes obligatorio");
                return  ;
            }

            if (!Utility.isNumeric(ano.getText().toString())){
                ano.setError("El año debe ser un número");
                return  ;
            }

            if (!Utility.isNumeric(meso.getText().toString())){
                meso.setError("El año debe ser un número");
                return ;
            }


            Date vencimiento = new GregorianCalendar(yearSuma2, mes, 15).getTime();
            int numMeses =  Utility.calcularMesesEntresFechas(new Date(), vencimiento);
            if (numMeses > 120){
                ano.setError("Fecha de vencimiento de la tarjeta esta no es correcto");
                return ;

            }



            if (!Utility.isNumeric(codigo.getText().toString())){
                codigo.setError("Código de seguridad no válido");
                return ;
            }
            if (codigo.getText().toString().equals("") || codigo.getText().toString().length()<3 || codigo.getText().toString().length()>4){
                codigo.setError("Código de seguridad no válido");
                return ;
            }

            if (!Utility.isLongitudCvcOk(numero.getText().toString(), codigo.getText().toString())){
                codigo.setError("Código de seguridad no válido");
                return  ;
            }

            if (cuotas.getText().toString().equals("") || cuotas.getText().toString().equals("0")){
                cuotas.setError("Número de cuotas no válidas");
                return  ;
            }

            if(max_cuotas>36 || max_cuotas <= 0){
                cuotas.setError("Número de cuotas no válidas");
                return ;

            }

            if (direccion.getText().toString().length() < 4) {
                direccion.setError("Debes incluir la dirección de facturación");
                direccion.requestFocus();
                return ;
            }

            if (ciudad.getText().toString().length() < 4) {
                ciudad.setError("Debes incluir la ciudad de facturación");
                ciudad.requestFocus();
                return ;
            }

            if (estado.getText().toString().length() < 4) {
                estado.setError("Debes incluir el departamento");
                estado.requestFocus();
                return;
            }

            if (!Utility.isEmailValid(correo.getText().toString())) {
                correo.setError("Debes incluir un correo valido");
                correo.requestFocus();
                return ;
            }


            if (!Luhn.Check(numero.getText().toString())) {
                numero.setError("Número de la tarjeta es incorrecto");
            } else if (numero.getText().toString().equals("")) {
                numero.setError("Número de la tarjeta de crédito incorrecto");
            } else if (numero.getText().toString().length() < 13 || numero.getText().toString().length() > 19) {
                numero.setError("Número de la tarjeta de crédito incorrecto");
            } else if (nombre.getText().toString().equals("") || nombre.getText().toString().length() < 4) {
                nombre.setError("nombre  obligatorio");
            } else if (meso.getText().toString().equals("") || meso.getText().toString().length() < 2) {
                meso.setError("Mes obligatorio");
            } else if (mes < 1 || mes > 12) {
                meso.setError("Mes erroneo");
            } else if (ano.getText().toString().equals("") || ano.getText().toString().length() < 4) {
                ano.setError("Año no válido");
            } else if (year < mYear || yearSuma2 > yearSuma) {
                ano.setError("Año no válido");
            } else if (year == mYear && mes < mMes) {
                meso.setError("Mes no válido o Tarjeta vencida");
            } else if (codigo.getText().toString().equals("") || codigo.getText().toString().length() < 3 || codigo.getText().toString().length() > 4) {
                codigo.setError("Codigo de seguridad no válido");
            } else if (cuotas.getText().toString().equals("") || cuotas.getText().toString().equals("0")) {
                cuotas.setError("Número de cuotas no válidas");
            } else if (max_cuotas > 36 || max_cuotas <= 0) {
                cuotas.setError("Número de cuotas no válidas");
            } else {
                info_ok();
            }

    }


    public void info_ok() {


        progressDialog.setMessage("Validando la información por favor espere...");
        progressDialog.show();

        //mini.ciudad = ciudad.getText().toString();
        //mini.correo = correo.getText().toString();
        //mini.direccion = direccion.getText().toString();

        MiniTarjeta newmini = new MiniTarjeta();
        newmini.lastFour = numero.getText().toString().substring(numero.getText().toString().length() - 4);
        newmini.activo = true;
        newmini.franquicia = Utility.getTipoCreditCard(numero.getText().toString());
        //newmini.token = response.body().getId();
        newmini.cuotas = Integer.valueOf(cuotas.getText().toString());
        newmini.referenceCode = ComandoPreguntas.getLlaveFirebase();
        newmini.id = ComandoPreguntas.getLlaveFirebase();
        newmini.ciudad = ciudad.getText().toString();
        newmini.departamento = estado.getText().toString();
        newmini.correo = correo.getText().toString();
        newmini.nombre = nombre.getText().toString();
        newmini.cedula = cedula.getText().toString();
        newmini.numero = numero.getText().toString();
        newmini.securityCode = codigo.getText().toString();
        newmini.direccion = direccion.getText().toString();
        newmini.telefono = telefono.getText().toString();
        newmini.expirationDate = ano.getText().toString() + "/" + meso.getText().toString();
        //newmini.ip = getIp();

        tokenizarCreditCard(modelo.getMapaToPagoInicial(newmini), newmini);


    }


    public void tokenizarCreditCard(final Map data, final MiniTarjeta mini){

        progressDialog.setMessage("Validando la información por favor espera...");
        progressDialog.show();



        CmdIp.getIp(this, new OnIpListener() {
            @Override
            public void gotIp(String ip) {

                mini.ip = ip;
                ((Map)data.get("tarjeta")).put("ipAddress",ip);
                hacerPagoTemporalConTarjeta(data, mini);

            }

            @Override
            public void fallo() {

                showAlertSinInternet("Fallo en la transacción", "No pudo iniciar la transacción. Intente más tarde");

            }
        });




    }


    private Task<String> hacerPagoTemporalConTarjeta(final Map data, final MiniTarjeta nueva) {


        return mFunctions
                .getHttpsCallable("tarjetaAprobadaO")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        CmdPayU.checkTarjetasPayU(nueva.referenceCode, "oferentes", modelo.uid, new OnCheckTarjetas() {
                            @Override
                            public void gotError(String error) {
                                progressDialog.dismiss();
                                showAlertSinInternet("Fallo en la transacción", "Problema con el sistema de pagos: " + error +   " \nPor favor intente más tarde" );
                                return;

                            }

                            @Override
                            public void gotDeclined(String error) {
                                try {
                                    modelo.apagarTarjetas();
                                    nueva.estado = "DECLINED";
                                    nueva.activo = false;
                                    new ComandoTPaga(null).setRegistroCredidCardOferente(nueva);
                                    progressDialog.dismiss();
                                    showAlertSinInternet("Fallo en la transacción", "La tarjeta fue rechazada: " + error +  "\nIntenta con una nueva tarjeta." );
                                }catch (Exception e){

                                }
                            }

                            @Override
                            public void gotPending(MiniTarjeta leida) {
                                MiniTarjeta actual = modelo.getTarjetaActiva();    //1.
                                if (actual != null ) {
                                    borrarToken(actual.token);                         //2.
                                }                      //2.
                                modelo.apagarTarjetas();                              //3.
                                modelo.tarjetas.add(leida);                           //4. Pilas el orden es impotante

                                progressDialog.dismiss();
                                nueva.estado = "PENDING";
                                showAlertSinInternet("Validación Tarjeta", "La tarjeta inscrita será validada por personal de payU y tomará de 1 a 4 horas dicho proceso. Te llegará una notificación informandote si tu tarjeta podrá ser usada para tus compras.");

                            }

                            @Override
                            public void gotApproved(MiniTarjeta leida) {
                                MiniTarjeta actual = modelo.getTarjetaActiva();    //1.
                                if (actual != null ) {
                                    borrarToken(actual.token);                     //2.
                                }
                                modelo.apagarTarjetas();                           //3.
                                modelo.tarjetas.add(leida);                        //4. Pilas el orden es impotante
                                progressDialog.dismiss();


                                finish();


                            }
                        });

                        try {
                            String result = (String) task.getResult().getData();

                            return result;

                        }catch (Exception e){
                            return  null;
                        }
                    }
                });
    }



    private Task<String> borrarToken(String token) {


        Map<String, Object> data = new HashMap<>();
        data.put("token", token);

        return mFunctions
                .getHttpsCallable("borrarToken")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {

                        try {
                            String result = (String) task.getResult().getData();
                            return result;
                        }
                        catch (Exception e) {
                            return null;
                        }
                    }
                });
    }



    private Task<Map> hacerPagoTemporalConTarjetaViejo(Map data, final MiniTarjeta mini) {     //Siempre se tokeniza y siempre se reversa


        return mFunctions
                .getHttpsCallable("pagoConTarjetaN")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        try {
                            Map result = (Map) task.getResult().getData();
                            return result;

                        }catch (Exception e){
                            return  null;
                        }
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }
                                                     progressDialog.dismiss();
                                                     showAlertSinInternet("Fallo en la transacción", "Problema con el sistema de pagos, por favor intente mas tarde. COD:A1");

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     return;
                                                 }


                                                 try {
                                                     Map result = (Map) task.getResult().get("transactionResponse");
                                                     String idOrdenPayU = result.get("orderId").toString();
                                                     String transactionId = result.get("transactionId").toString();
                                                     String status = result.get("state").toString();
                                                     String responseCode = result.get("responseCode").toString();
                                                     if (status.equals("DECLINED")){
                                                         progressDialog.dismiss();
                                                         showAlertSinInternet("Fallo en la transacción", "La tarjeta fue rechazada.  COD:" + responseCode );
                                                         //return;

                                                     }


                                                     Toast.makeText(getApplicationContext(),"Pago " + result.get("state").toString(),Toast.LENGTH_LONG).show();


                                                     Map<String, Object> data = new HashMap<>();
                                                     data.put("idOrdenPayU", idOrdenPayU);
                                                     data.put("transactionId", transactionId);

                                                     reembolsarPago(data);        // Con exito o fracaso continua la ejecucion, solo dejar un log

                                                     tokenizar(modelo.getMapaToTokenizar(mini),mini);

                                                 }
                                                 catch (Exception e ){
                                                     progressDialog.dismiss();
                                                     showAlertSinInternet("Fallo en la transacción", "Problema con el sistema de pagos, por favor intenta más tarde. COD:A3");
                                                     return;

                                                 }


                                             }
                                         }
                );
    }






    private Task<Map> tokenizar(Map data, final MiniTarjeta mini) {


        return mFunctions
                .getHttpsCallable("tokenizando")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        Map result = (Map) task.getResult().getData();
                        return result;
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     return;
                                                 }

                                                 Map result = (Map)task.getResult().get("creditCardToken");
                                                 Toast.makeText(getApplicationContext(),result.get("creditCardTokenId").toString(),Toast.LENGTH_LONG).show();

                                                 mini.token = result.get("creditCardTokenId").toString();
                                                 mini.lastFour = result.get("maskedNumber").toString();

                                                 modelo.apagarTarjetas();
                                                 modelo.tarjetas.add(mini);
                                                 new ComandoTPaga(null).setRegistroCredidCardOferente(mini);

                                                 progressDialog.dismiss();

                                                 finish();


                                             }
                                         }
                );
    }



    private Task<Map> reembolsarPago(Map data) {



        return mFunctions
                .getHttpsCallable("reembolsarPago")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        Map result = (Map) task.getResult().getData();
                        return result;
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     return;
                                                 }



                                                 Map result = (Map)task.getResult().get("transactionResponse");

                                                 if (result == null){
                                                     Toast.makeText(getApplicationContext(),"Fallo al hacer el reverso, pruebas?",Toast.LENGTH_LONG).show();

                                                 }else {

                                                     Toast.makeText(getApplicationContext(), "Reembolso: " + result.get("state").toString(), Toast.LENGTH_LONG).show();
                                                 }

                                                 progressDialog.dismiss();

                                             }
                                         }
                );
    }





    

    public void showAlertAceptar() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("¡Tarjeta inscrita!");

        // set dialog message
        alertDialogBuilder
                .setMessage("Tu método de pago ha sido actualizado")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        pagoSharedPreference = "Tarjeta";
                        //vista
                        atras2();

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }




    public void showalertEnviar2(final String authorizationCode, final String idPago, final String paymentTransaction, final String idTarjeta) {


        if (estaConectado()) {

            validacion_pago(authorizationCode, idPago, paymentTransaction, idTarjeta);

            /*   modelo.usuario.setCelular(tel);
            modelo.usuario.setDireccion(dir);
            modelo.orden.limpiarPrompagandas();
            comandoUsuarios.setActualizarConductor(modelo.usuario.getCelular(), modelo.usuario.getDireccion(), usuarioId);
            comandoOrdenTarjeta.setCrearOrden(tel, dir, coment, usuarioId, authorizationCode,idPago,idTarjeta,paymentTransaction);
*/

        } else {
            progressDialog.dismiss();
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            String titulo = "Sin Internet";
            String mensaje = "Sin Conexión a Internet";

            showAlertSinInternet(titulo, mensaje);
            return;
        }


    }


    //tarjeta


    public void validacion_pago(final String authorizationCode, final String idPago, final String paymentTransaction, final String idTarjeta) {

        if (estaConectado()) {

            comandoOrdenTarjeta.setCrerDestacado(modelo.datosOferente.getCelular(), modelo.datosOferente.getDireccion(), "Destacado", modelo.uid, authorizationCode, idPago, paymentTransaction, idTarjeta);

        } else {
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            String titulo = "Sin Internet";
            String mensaje = "Sin Conexión a Internet";

            showAlertSinInternet(titulo, mensaje);
            return;
        }
    }

    //fin tarjeta


    //conexion internet
    //validacion conexion internet
    protected Boolean estaConectado() {
        if (conectadoWifi()) {
            Log.v("wifi", "Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        } else {
            if (conectadoRedMovil()) {
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            } else {
                String titulo = "Sin Internet";
                String mensaje = "Sin Conexión a Internet";
                showAlertSinInternet(titulo, mensaje);
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }


    protected Boolean conectadoWifi() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }


    public void showAlertSinInternet(String titulo, String mensaje) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + titulo);

        // set dialog message
        alertDialogBuilder
                .setMessage("" + mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }




    /*public String getIp(){

        String ipWifi = getWifiIPAddress();

        if (!ipWifi.equals("0.0.0.0") && !ipWifi.equals("")){
            return  ipWifi;
        }

        return getMobileIPAddress();

    }*/

    public String getWifiIPAddress() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        return  Formatter.formatIpAddress(ip);
    }


    public static String getMobileIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return  addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            return "0.0.0.0";
        }
        return "0.0.0.0";
    }


}
