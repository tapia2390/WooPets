package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import woopets.com.woopets.Oferente.Comandos.ComandoEditarEliminarEstado;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;

public class MyPublicacionesArticulo extends Activity implements ComandoEditarEliminarEstado.OnComandoEditarEliminarEstadoChangeListener {

    Modelo modelo = Modelo.getInstance();
    final Context context = this;
    EditText txt_titulo, txt_descripcion_art, txt_precio, txt_cantidad;
    LinearLayout duracion_servicio;
    TextView txt_duracion_servicio, txt_minutos_servicios;
    EditText txt_hora_servicios;

    TextView textodiassemana, textofindiassemana, textodiasfinsemana, textofindiafinssemana;
    TextView cierre_etre_fin_semana, cierre_etre_semana;
    TextView txt_horariodomicilio;
    LinearLayout horariolayout, finhorariolayout;
    String diasDeLaSemana = "";
    String findiasDeLaSemana = "";
    TextView txt_horio_Servicio;
    LinearLayout layout_horio_Servicio, linearLayout_cantidad;
    SwitchCompat swich_servicio_domcilio, swich_servicio_en_tienda;

    ComandoEditarEliminarEstado comandoEditarEliminarEstado;
    int cont = 0;
    String precionSinconvercion = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_my_publicaciones_articulo);

        txt_titulo = (EditText) findViewById(R.id.txt_titulo);
        txt_descripcion_art = (EditText) findViewById(R.id.txt_descripcion_art);
        txt_precio = (EditText) findViewById(R.id.txt_precio);
        txt_cantidad = (EditText) findViewById(R.id.txt_cantidad);
        cierre_etre_semana = (TextView) findViewById(R.id.cierre_etre_semana);
        cierre_etre_fin_semana = (TextView) findViewById(R.id.cierre_etre_fin_semana);

        txt_horariodomicilio = (TextView) findViewById(R.id.txt_horariodomicilio);
        horariolayout = (LinearLayout) findViewById(R.id.horariolayout);
        finhorariolayout = (LinearLayout) findViewById(R.id.finhorariolayout);
        textodiassemana = (TextView) findViewById(R.id.textodiassemana);
        textofindiassemana = (TextView) findViewById(R.id.textofindiassemana);
        textodiasfinsemana = (TextView) findViewById(R.id.textodiasfinsemana);
        textofindiafinssemana = (TextView) findViewById(R.id.textofindiafinssemana);
        duracion_servicio = (LinearLayout) findViewById(R.id.duracion_servicio);
        txt_duracion_servicio = (TextView) findViewById(R.id.txt_duracion_servicio);
        txt_minutos_servicios = (TextView) findViewById(R.id.txt_minutos_servicios);
        txt_hora_servicios = (EditText) findViewById(R.id.txt_hora_servicios);
        txt_horio_Servicio = (TextView) findViewById(R.id.txt_horio_Servicio);
        layout_horio_Servicio = (LinearLayout) findViewById(R.id.layout_horio_Servicio);
        linearLayout_cantidad = (LinearLayout) findViewById(R.id.linearLayout_cantidad);
        swich_servicio_domcilio = (SwitchCompat) findViewById(R.id.swich_servicio_domcilio);
        swich_servicio_en_tienda = (SwitchCompat) findViewById(R.id.swich_servicio_en_tienda);


        /* txt_titulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarTipoSharedPreferenceOferente();
            }
        });*/
    }


    public void continuar(View v) {


        if (txt_titulo.getText().toString().equals("")) {
            txt_titulo.setError("Escriba un título");
            showAlertAceptar();
        } else if (txt_descripcion_art.getText().toString().equals("")) {
            txt_descripcion_art.setError("Escriba una descripción");
            showAlertAceptar();
        } else if (txt_descripcion_art.getText().length() < 25) {
            txt_descripcion_art.setError("mínimo 25 caracteres");
            showAlertAceptar();
        } else if (txt_descripcion_art.getText().toString().length() < 25) {
            txt_descripcion_art.setError("mínimo 25 caracteres");
            showAlertAceptar();
        } else if (txt_descripcion_art.getText().length() < 25) {
            txt_descripcion_art.setError("mínimo 25 caracteres");
            showAlertAceptar();
        } else if (txt_precio.getText().toString().equals("")) {
            txt_precio.setError("Ingrese un precio");
            showAlertAceptar();
        } else if (modelo.servicio_producto == false && txt_cantidad.getText().toString().equals("")) {
            txt_cantidad.setError("Ingrese una cantidad");
            showAlertAceptar();

        } else {


            modelo.productosOferente.setActivo(true);
            modelo.productosOferente.setTitulo(txt_titulo.getText().toString());
            modelo.productosOferente.setDescripcion(txt_descripcion_art.getText().toString());
            modelo.productosOferente.setPrecio(precionSinconvercion.replace(".", "").replaceAll("\\s", "").trim());


            if (modelo.servicio_producto == false) {
                modelo.productosOferente.setCantidad(Integer.parseInt(txt_cantidad.getText().toString()));
            }


            if (modelo.servicio_producto == true) {


                if (modelo.diasDeLaSemana.size() <= 0 && modelo.finDeSemanaFestivos.size() <= 0 || txt_hora_servicios.getText().toString().equals("") || txt_hora_servicios.getText().toString().equals("0")) {
                    showAlertAceptar();
                    return;
                }

                if (modelo.diasDeLaSemana.size() > 0) {
                    modelo.productosOferente.setHorarioDiasDeLaSemana(textodiassemana.getText().toString());
                    modelo.productosOferente.setHorarioCierreDiasDeLaSemana(modelo.horariodesemanaFin);
                    modelo.productosOferente.setHorarioInicioDiasDeLaSemana(modelo.horariodesemanaIni);
                    modelo.productosOferente.setSinJornadaContinua(modelo.sinJornadaContinuaSemana);

                }

                if (modelo.finDeSemanaFestivos.size() > 0) {
                    modelo.productosOferente.setHorarioDiasFinDeSemana(textodiasfinsemana.getText().toString());
                    modelo.productosOferente.setHorarioCierreDiasFinDeSemana(modelo.horariofindesemanaFin);
                    modelo.productosOferente.setHorarioInicioDiasFinDeSemana(modelo.horariofindesemanaIni);
                    modelo.productosOferente.setFinsinJornadaContinua(modelo.sinJornadaContinuaFinDeSemana);

                }

                modelo.productosOferente.setServicio(modelo.servicioEnDomicilio);
                modelo.productosOferente.setDuracion(Integer.parseInt(txt_hora_servicios.getText().toString()));
                modelo.productosOferente.setDuracionMedida(txt_minutos_servicios.getText().toString());
            }

            if (modelo.publicacion_estado == 1) {
                modelo.publicacion_estado = 0;

                comandoEditarEliminarEstado.setActualizarArticulo(modelo.pro_uidProducto);


            } else {

                guardarTipoSharedPreferenceOferente();
                Log.v("Publicacion foto", "Publicacion foto");
                Intent i = new Intent(getApplicationContext(), MyPublicacionesFoto.class);
                startActivity(i);
                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            }
        }

    }


    @Override
    public void onBackPressed() {

        borrarTipoSharedPreferenceOferente();
        if (modelo.publicacion_estado == 1) {
            modelo.publicacion_estado = 0;
        }

        finish();

        super.onBackPressed();
    }

    public void showAlertAceptar() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.informacionincompleta));

        // set dialog message
        alertDialogBuilder
                .setMessage("" + getString(R.string.informacionincompletaDes))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public void relative(View v) {
        modelo.horarioMyPublicacionesArticulo = "horarioMyPublicacionesArticulo";
        guardarTipoSharedPreferenceOferente();
        //Toast.makeText(getApplicationContext(),"relative", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(getApplicationContext(), HorarioAtencionDomicilio.class);
        i.putExtra("horario", "horarioMyPublicacionesArticulo");
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
    }


    public void duracionservicio(View v) {
        showBottomSheetDialog();
    }

    private void showBottomSheetDialog() {
        final CharSequence[] items = {"Minutos", "Horas", "Días"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("seleccione una opción");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                txt_minutos_servicios.setText(items[item]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void cargoEstado() {

    }

    @Override
    public void cargoEliminarPublicacion() {

    }

    @Override
    public void cargoAbiertas() {

    }

    @Override
    public void cargoCerradas() {

    }

    @Override
    public void cargoTipo() {

    }

    @Override
    public void cargoArticulo() {

        //Intent i = new Intent(getApplicationContext(), InformacionProductoServicioOferente.class);
        //startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
        showAlertActualizacion();

    }


    public void showAlertActualizacion() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.publicupdate));

        // set dialog message
        alertDialogBuilder
                .setMessage("")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent(getApplicationContext(), InformacionProductoServicioOferente.class);
                        onBackPressed();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    public void cargoFoto() {

    }


    //sharedpreferences
    public void cargarTipoSharedPreferenceOferente() {
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias articulos", Context.MODE_PRIVATE);
        // SharedPreferences.Editor editor =sharedPreferences.edit();

        txt_titulo.setText(sharedPreferences.getString("txt_titulo", ""));
        precionSinconvercion = sharedPreferences.getString("precionSinconvercion", "");
        txt_descripcion_art.setText(sharedPreferences.getString("txt_descripcion_art", ""));
        txt_precio.setText(sharedPreferences.getString("txt_precio", ""));
        textodiassemana.setText(sharedPreferences.getString("textodiassemana", ""));
        textofindiassemana.setText(sharedPreferences.getString("textofindiassemana", ""));
        cierre_etre_semana.setText(sharedPreferences.getString("cierre_etre_semana", ""));
        textodiasfinsemana.setText(sharedPreferences.getString("textodiasfinsemana", ""));
        textofindiafinssemana.setText(sharedPreferences.getString("textofindiafinssemana", ""));
        cierre_etre_fin_semana.setText(sharedPreferences.getString("cierre_etre_fin_semana", ""));
        txt_hora_servicios.setText(sharedPreferences.getString("txt_hora_servicios", ""));
        txt_minutos_servicios.setText(sharedPreferences.getString("txt_minutos_servicios", ""));
        txt_cantidad.setText(sharedPreferences.getString("txt_cantidad", ""));

    }


    public void guardarTipoSharedPreferenceOferente() {
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias articulos", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();


        editor.putString("txt_titulo", txt_titulo.getText().toString());
        editor.putString("txt_descripcion_art", txt_descripcion_art.getText().toString());
        editor.putString("txt_precio", txt_precio.getText().toString());
        editor.putString("precionSinconvercion", precionSinconvercion);
        editor.putString("textodiassemana", textodiassemana.getText().toString());
        editor.putString("textofindiassemana", textofindiassemana.getText().toString());
        editor.putString("cierre_etre_semana", cierre_etre_semana.getText().toString());
        editor.putString("textodiasfinsemana", textodiasfinsemana.getText().toString());
        editor.putString("textofindiafinssemana", textofindiafinssemana.getText().toString());
        editor.putString("cierre_etre_fin_semana", cierre_etre_fin_semana.getText().toString());
        editor.putString("txt_hora_servicios", txt_hora_servicios.getText().toString());
        editor.putString("txt_minutos_servicios", txt_minutos_servicios.getText().toString());
        editor.putString("txt_cantidad", txt_cantidad.getText().toString());
        txt_minutos_servicios.setText("Minutos");

        editor.commit();
    }


    public void borrarTipoSharedPreferenceOferente() {
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias articulos", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();


        editor.putString("precionSinconvercion", "");
        editor.putString("txt_titulo", "");
        editor.putString("txt_descripcion_art", "");
        editor.putString("txt_precio", "");
        editor.putString("textodiassemana", "");
        editor.putString("textofindiassemana", "");
        editor.putString("cierre_etre_semana", "");
        editor.putString("textodiasfinsemana", "");
        editor.putString("textofindiafinssemana", "");
        editor.putString("cierre_etre_fin_semana", "");
        editor.putString("txt_hora_servicios", "");
        editor.putString("txt_minutos_servicios", "");
        editor.putString("txt_cantidad", "");

        editor.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //borrarTipoSharedPreferenceOferente();
    }

    @Override
    protected void onPause() {
        super.onPause();
        guardarTipoSharedPreferenceOferente();
    }

    private void editatArticulo() {
        if (modelo.publicacion_estado == 1) {

            txt_titulo.setText("" + modelo.productosOferentesBoolean.get(0).getTitulo());
            txt_descripcion_art.setText("" + modelo.productosOferentesBoolean.get(0).getDescripcion());
            txt_precio.setText("" + Utility.convertToMoney(modelo.productosOferentesBoolean.get(0).getPrecio()).replace("$", ""));
            precionSinconvercion = modelo.productosOferentesBoolean.get(0).getPrecio().replace(".", "").replaceAll("\\s", "").trim();
            ;
            txt_precio.setSelection(txt_precio.getText().length());


            if (modelo.productosOferentesBoolean.get(0).getServicio() == false) {
                txt_cantidad.setText("" + modelo.productosOferentesBoolean.get(0).getCantidad());
            }

            if (modelo.productosOferentesBoolean.get(0).getServicio() == true) {
                txt_hora_servicios.setText("" + modelo.productosOferentesBoolean.get(0).getDuracion());
                txt_minutos_servicios.setText("" + modelo.productosOferentesBoolean.get(0).getDuracionMedida());

                if (modelo.horarioMyPublicacionesArticulo.equals("horarioMyPublicacionesArticulo")) {


                    if (swich_servicio_domcilio.isChecked() == true) {
                        swich_servicio_domcilio.setChecked(true);
                        swich_servicio_en_tienda.setChecked(false);
                        modelo.servicioEnDomicilio = true;
                    } else {
                        swich_servicio_domcilio.setChecked(false);
                        swich_servicio_en_tienda.setChecked(true);
                        modelo.servicioEnDomicilio = false;
                    }


                    if (swich_servicio_en_tienda.isChecked() == true) {
                        swich_servicio_domcilio.setChecked(false);
                        swich_servicio_en_tienda.setChecked(true);
                        modelo.servicioEnDomicilio = false;
                    } else {
                        swich_servicio_domcilio.setChecked(true);
                        swich_servicio_en_tienda.setChecked(false);
                        modelo.servicioEnDomicilio = true;
                    }


                    if (modelo.diasDeLaSemana.size() > 0) {
                        horariolayout.setVisibility(View.VISIBLE);
                        /*for (int i = 0; i < modelo.diasDeLaSemana.size(); i++) {
                            diasDeLaSemana += modelo.diasDeLaSemana.get(i)+",";
                        }*/

                        if (modelo.diasDeLaSemana.size() == 1) {
                            textodiassemana.setText(diasDeLaSemana.substring(0, diasDeLaSemana.length() - 1));
                        }

                        if (modelo.diasDeLaSemana.size() > 1) {
                            textodiassemana.setText(diasDeLaSemana.substring(0, diasDeLaSemana.length() - 1));
                        }


                        //textodiassemana.setText(diasDeLaSemana);
                        textofindiassemana.setText(modelo.horariodesemanaIni + " - " + modelo.horariodesemanaFin);

                        if (modelo.sinJornadaContinuaSemana == false) {
                            cierre_etre_semana.setText(getString(R.string.jornadacontinua));
                        } else {
                            cierre_etre_semana.setText(getString(R.string.cerramosentre));
                        }
                    }

                    //
                    if (modelo.finDeSemanaFestivos.size() > 0) {

                        finhorariolayout.setVisibility(View.VISIBLE);
                      /*  for (int i = 0; i < modelo.finDeSemanaFestivos.size(); i++) {
                            findiasDeLaSemana += modelo.finDeSemanaFestivos.get(i)+",";
                        }*/

                        if (modelo.finDeSemanaFestivos.size() == 1) {
                            textodiasfinsemana.setText(findiasDeLaSemana.substring(0, findiasDeLaSemana.length() - 1));
                        }

                        if (modelo.finDeSemanaFestivos.size() > 1) {
                            textodiasfinsemana.setText(findiasDeLaSemana.substring(0, findiasDeLaSemana.length() - 1));
                        }
                        textofindiafinssemana.setText(modelo.horariofindesemanaIni + " - " + modelo.horariofindesemanaFin);

                        if (modelo.sinJornadaContinuaFinDeSemana == false) {
                            cierre_etre_fin_semana.setText(getString(R.string.jornadacontinua));
                        } else {
                            cierre_etre_fin_semana.setText(getString(R.string.cerramosentre));
                        }
                    }


                } else {


                    if (modelo.productosOferentesBoolean.get(0).getServicioEnDomicilio() == true) {

                        swich_servicio_domcilio.setChecked(true);
                        swich_servicio_en_tienda.setChecked(false);
                        modelo.servicioEnDomicilio = true;
                    } else {
                        swich_servicio_domcilio.setChecked(false);
                        swich_servicio_en_tienda.setChecked(true);
                        modelo.servicioEnDomicilio = false;
                    }


                    if (modelo.productosOferentesBoolean.get(0).getServicioEnDomicilio() == false) {
                        swich_servicio_domcilio.setChecked(false);
                        swich_servicio_en_tienda.setChecked(true);
                        modelo.servicioEnDomicilio = false;
                    } else {
                        swich_servicio_domcilio.setChecked(true);
                        swich_servicio_en_tienda.setChecked(false);
                        modelo.servicioEnDomicilio = true;

                    }


                    modelo.horariodesemanaIni = modelo.productosOferentesBoolean.get(0).getHorarioInicioDiasDeLaSemana();
                    modelo.horariodesemanaFin = modelo.productosOferentesBoolean.get(0).getHorarioCierreDiasDeLaSemana();
                    modelo.sinJornadaContinuaSemana = modelo.productosOferentesBoolean.get(0).getSinJornadaContinuaSemana();

                    textodiassemana.setText("" + modelo.productosOferentesBoolean.get(0).getHorarioDiasDeLaSemana());
                    textofindiassemana.setText("" + modelo.productosOferentesBoolean.get(0).getHorarioInicioDiasDeLaSemana() + "-" + modelo.productosOferentesBoolean.get(0).getHorarioCierreDiasDeLaSemana());


                    modelo.horariofindesemanaIni = modelo.productosOferentesBoolean.get(0).getHorarioInicioDiasFinDeSemana();
                    modelo.horariofindesemanaFin = modelo.productosOferentesBoolean.get(0).getHorarioCierreDiasFinDeSemana();
                    modelo.sinJornadaContinuaFinDeSemana = modelo.productosOferentesBoolean.get(0).getSinJornadaContinuaFinDeSemana();


                    textodiasfinsemana.setText("" + modelo.productosOferentesBoolean.get(0).getHorarioDiasFinDeSemana());
                    textofindiafinssemana.setText("" + modelo.productosOferentesBoolean.get(0).getHorarioInicioDiasFinDeSemana() + "-" + modelo.productosOferentesBoolean.get(0).getHorarioCierreDiasFinDeSemana());
                }
            }

        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        cargandoDatos();
    }

    public void cargandoDatos() {


        diasDeLaSemana = "";
        findiasDeLaSemana = "";
        cargarTipoSharedPreferenceOferente();


        comandoEditarEliminarEstado = new ComandoEditarEliminarEstado(this);

        //SwitchCompat
        swich_servicio_domcilio.setChecked(modelo.servicioEnDomicilio);
        if (swich_servicio_domcilio.isChecked() == true) {
            swich_servicio_domcilio.setChecked(true);
            swich_servicio_en_tienda.setChecked(false);
            modelo.servicioEnDomicilio = true;
        } else {
            swich_servicio_domcilio.setChecked(false);
            swich_servicio_en_tienda.setChecked(true);
            modelo.servicioEnDomicilio = false;
        }


        swich_servicio_domcilio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (swich_servicio_domcilio.isChecked() == true) {
                    swich_servicio_domcilio.setChecked(true);
                    swich_servicio_en_tienda.setChecked(false);
                    modelo.servicioEnDomicilio = true;
                } else {
                    swich_servicio_domcilio.setChecked(false);
                    swich_servicio_en_tienda.setChecked(true);
                    modelo.servicioEnDomicilio = false;
                }
            }
        });

        swich_servicio_en_tienda.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (swich_servicio_en_tienda.isChecked() == true) {
                    swich_servicio_domcilio.setChecked(false);
                    swich_servicio_en_tienda.setChecked(true);
                    modelo.servicioEnDomicilio = false;
                } else {
                    swich_servicio_domcilio.setChecked(true);
                    swich_servicio_en_tienda.setChecked(false);
                    modelo.servicioEnDomicilio = true;
                }
            }
        });

        //fin SwitchCompat


        if (modelo.servicio_producto == true) {
            duracion_servicio.setVisibility(View.VISIBLE);
            txt_duracion_servicio.setVisibility(View.VISIBLE);
            linearLayout_cantidad.setVisibility(View.GONE);

            txt_horio_Servicio.setVisibility(View.VISIBLE);
            layout_horio_Servicio.setVisibility(View.VISIBLE);

            if (modelo.diasDeLaSemana.size() > 0) {
                horariolayout.setVisibility(View.VISIBLE);
                for (int i = 0; i < modelo.diasDeLaSemana.size(); i++) {
                    diasDeLaSemana += modelo.diasDeLaSemana.get(i) + ",";
                }

                String dias = diasDeLaSemana.substring(0, diasDeLaSemana.length() - 1);
                textodiassemana.setText(dias);
                textofindiassemana.setText(modelo.horariodesemanaIni + " - " + modelo.horariodesemanaFin);

                if (modelo.sinJornadaContinuaSemana == false) {
                    cierre_etre_semana.setText(getString(R.string.jornadacontinua));
                } else {
                    cierre_etre_semana.setText(getString(R.string.cerramosentre));
                }
            }

            if (modelo.finDeSemanaFestivos.size() > 0) {

                finhorariolayout.setVisibility(View.VISIBLE);
                for (int i = 0; i < modelo.finDeSemanaFestivos.size(); i++) {
                    findiasDeLaSemana += modelo.finDeSemanaFestivos.get(i) + ",";
                }

                String diasf = findiasDeLaSemana.substring(0, findiasDeLaSemana.length() - 1);
                textodiasfinsemana.setText(diasf);
                textofindiafinssemana.setText(modelo.horariofindesemanaIni + " - " + modelo.horariofindesemanaFin);

                if (modelo.sinJornadaContinuaFinDeSemana == false) {
                    cierre_etre_fin_semana.setText(getString(R.string.jornadacontinua));
                } else {
                    cierre_etre_fin_semana.setText(getString(R.string.cerramosentre));
                }
            }

        } else {
            duracion_servicio.setVisibility(View.GONE);
            txt_duracion_servicio.setVisibility(View.GONE);
            txt_horio_Servicio.setVisibility(View.GONE);
            layout_horio_Servicio.setVisibility(View.GONE);
        }


        if (modelo.publicacion_estado == 1) {
            modelo.servicio_producto = modelo.productosOferentes.get(0).getServicio();
            editatArticulo();
        }

        txt_precio.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (i != 0) {
                    //Toast.makeText(getApplicationContext(), " on " + txt_precio.getText().toString(), Toast.LENGTH_SHORT).show();
                    String precio = txt_precio.getText().toString();
                    String temp = "";

                    int precio2 = 0;


                    try {
                        precio = txt_precio.getText().toString().replace(" ", "");
                        Locale colombia = new Locale("es", "CO");
                        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance(colombia);
                        defaultFormat.setMaximumFractionDigits(0);
                        precio = precio.replace(".", "");
                        precio2 = Integer.parseInt(precio.replace(" ", "").replaceAll("\\s", "").trim());
                        temp = defaultFormat.format(precio2);
                        temp = temp.replace(",", ".");
                        temp = temp.replace("$", "");

                        txt_precio.setText(temp);
                        txt_precio.setSelection(txt_precio.getText().length());
                        precionSinconvercion = temp.replace(".", "").replaceAll("\\s", "").trim();


                    } catch (RuntimeException r) {
                        Log.i("MONEY", r.getLocalizedMessage());
                        Log.i("MONEY", r.getMessage());

                    }
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }


}
