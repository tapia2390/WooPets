package woopets.com.woopets.Oferente.AdapterOferente;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import woopets.com.woopets.Oferente.InformacionProductoServicioOferente;
import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.PreguntasPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.Oferente.Preguntas_A_MisPublicacionesCliente;
import woopets.com.woopets.R;

/**
 * Created by tactomotion on 26/12/16.
 */
public class PreguntasPublicacionesAdapter extends BaseAdapter {

    //Atributos del adaptador
    private final Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<ClassPreguntas_A_MisPublicaciones> classPreguntas_a_misPublicaciones;
    private Modelo sing = Modelo.getInstance();

    int posicionadapter;

    TextView contadoradapter;


    public PreguntasPublicacionesAdapter(Context mContext, AdapterCallback mListener) {

        sing.adicionarListaPreguntasSinResponder();

        sing.comprasProductos2.clear();
        if (sing.preguntas_publicaiones.size() > 0) {


            HashMap<String, ClassPreguntas_A_MisPublicaciones> map = new HashMap<String, ClassPreguntas_A_MisPublicaciones>();

            for (ClassPreguntas_A_MisPublicaciones compraProducto : sing.preguntas_publicaiones) {
                int i = 0;
                map.put(compraProducto.getIdPublicacion(), compraProducto);
                i++;
            }
            Set<Map.Entry<String, ClassPreguntas_A_MisPublicaciones>> set = map.entrySet();
            for (Map.Entry<String, ClassPreguntas_A_MisPublicaciones> entry : set) {
                sing.comprasProductos2.add(entry.getValue());
            }
        }

        if (sing.preguntas_publicaiones.size() > 0) {
            Collections.sort(sing.preguntas_publicaiones, new Comparator<ClassPreguntas_A_MisPublicaciones>() {
                @Override
                public int compare(ClassPreguntas_A_MisPublicaciones o1, ClassPreguntas_A_MisPublicaciones o2) {
                    return new Long(o1.getTimestamp()).compareTo(new Long(o2.getTimestamp()));
                }
            });

        }

        this.mContext = mContext;
        this.mListener = mListener;
        this.classPreguntas_a_misPublicaciones = sing.comprasProductos2;

    }

    @Override
    public int getCount() {
        return classPreguntas_a_misPublicaciones.size();
    }

    @Override
    public Object getItem(int position) {
        return classPreguntas_a_misPublicaciones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // Declare Variables
        final TextView  txtTitulo;
        final TextView txtPregunta;
        final Button btn_flecha;
        final LinearLayout layout_adapter;
        final ImageView list_img;
        final  Button list_img_cantidad;
        final ImageView list_img_favorito;
        String foto = "";


        final ClassPreguntas_A_MisPublicaciones listClassPreguntas_a_misPublicaciones = (ClassPreguntas_A_MisPublicaciones) getItem(position);


        // Inflate la vista de la Orden
        RelativeLayout itemLayout =  (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.adapter_preguntas_publicaciones, parent, false);

        // Toma los elementos a utilizar
        txtPregunta = (TextView) itemLayout.findViewById(R.id.txtPregunta);
        btn_flecha = (Button) itemLayout.findViewById(R.id.btn_flecha);
        layout_adapter = (LinearLayout) itemLayout.findViewById(R.id.layout_adapter);
        list_img = (ImageView) itemLayout.findViewById(R.id.list_img);
        list_img_favorito = (ImageView) itemLayout.findViewById(R.id.list_img_favorito);
        list_img_cantidad = (Button) itemLayout.findViewById(R.id.list_img_cantidad);
        txtTitulo = (TextView) itemLayout.findViewById(R.id.txtTitulo);

        if(listClassPreguntas_a_misPublicaciones.listaProductosOferentes.size() > 0){

            txtTitulo.setText(""+ Utility.convertToMoney(listClassPreguntas_a_misPublicaciones.listaProductosOferentes.get(0).getPrecio()));
            txtPregunta.setText(listClassPreguntas_a_misPublicaciones.listaProductosOferentes.get(0).getTitulo());

            foto = listClassPreguntas_a_misPublicaciones.listaProductosOferentes.get(0).getFotos().get(0).toString();
            //Toast.makeText(mContext,""+foto+position, Toast.LENGTH_LONG).show();



            if(listClassPreguntas_a_misPublicaciones.listaProductosOferentes.get(0).getDestacado()== false && foto.equals("Destacdo") )  {
                foto =listClassPreguntas_a_misPublicaciones.listaProductosOferentes.get(0).getFotos().get(1);

            }

            int i = 0;
            int acumulador2 = 0;
            while (i < sing.preguntas_publicaiones.size()) {
                if (sing.preguntas_publicaiones.get(i).getIdPublicacion().equals(listClassPreguntas_a_misPublicaciones.getIdPublicacion())) {
                    acumulador2  ++;
                }
                i++;
            }


            list_img_cantidad.setText(""+acumulador2);


            //descargar imagenes firebase

            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference().child("productos/"+listClassPreguntas_a_misPublicaciones.listaProductosOferentes.get(0).getId()+"/"+foto);

            //get download file url
            storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Log.i("Main", "File uri: " + uri.toString());

                /*
                *  Picasso.with(mContext)
                        .load(uri.toString())
                        .error(R.drawable.btnfoto)
                        .into(list_img);
                        */
                }
            });


            //download the file
            try {
                final File localFile = File.createTempFile("images", "jpg");
                final String finalFoto = foto;
                storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                        list_img.setImageBitmap(bitmap);


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        int resID2 = mContext.getResources().getIdentifier(finalFoto, "drawable",  mContext.getPackageName()); // This will return an integer value stating the id for the image you want.

                        if(resID2 == 0){
                            // Toast.makeText(mContext, "Error al cargar la imagen", Toast.LENGTH_SHORT).show();
                            list_img.setImageDrawable(mContext.getResources().getDrawable(R.drawable.btnfoto));

                        }else{
                            list_img.setBackgroundResource(resID2);
                        }


                    }
                });
            } catch (IOException e ) {
                e.printStackTrace();
                Log.e("Main", "IOE Exception");
            }



            if(listClassPreguntas_a_misPublicaciones.listaProductosOferentes.get(0).getDestacado()== true){
                list_img_favorito.setBackgroundResource(R.drawable.destacado_ok);
            }
        }


        layout_adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext,"posicion"+position+" id:"+listClassPreguntas_a_misPublicaciones.getId(),Toast.LENGTH_LONG).show();

                sing.idPreguntasClientePublicacionesPosicion  =position;

                sing.idPreguntasClientePublicaciones = listClassPreguntas_a_misPublicaciones.getIdPublicacion();
                sing.filtro = 0;
                Intent i = new Intent(mContext,Preguntas_A_MisPublicacionesCliente.class);
                mContext.startActivity(i);
            }
        });



        return itemLayout;

    }


    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
   */
    public interface AdapterCallback {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback() {


    };
}