package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import woopets.com.woopets.Adapter.CategoriasAdapter;
import woopets.com.woopets.Adapter.HorizontalListView;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;
import woopets.com.woopets.R;

public class MyPublicacionesCategorias extends Activity implements CategoriasAdapter.AdapterCallback {

    private static final String TAG = "Tab1Fragment";
    private Button ayuda_popup;
    private EditText txt_categoria;
    private Button continuar;
    Modelo modelo = Modelo.getInstance();
    private CategoriasAdapter mAdapter;
    HorizontalListView listview;
    int positionSelect = 0;
    String nombreCategoria = "";

    LinearLayout layout_select_opciones;
    RelativeLayout tipo_subcategoria;
    ArrayList<String> items2;
    String subcategoria = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_my_publicaciones_categorias);


        listview = (HorizontalListView) findViewById(R.id.listview);
        layout_select_opciones = (LinearLayout) findViewById(R.id.layout_select_opciones);
        tipo_subcategoria = (RelativeLayout) findViewById(R.id.tipo_subcategoria);
        modelo.selectImge = false;


        //popup
        ayuda_popup = (Button) findViewById(R.id.ayuda_popup);
        ayuda_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater layoutInflater
                        = (LayoutInflater) getBaseContext()
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = layoutInflater.inflate(R.layout.popup, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);

                Button btnDismiss = (Button) popupView.findViewById(R.id.dismiss);
                btnDismiss.setOnClickListener(new Button.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        popupWindow.dismiss();
                    }
                });
                popupWindow.showAsDropDown(ayuda_popup, 0, -295);

            }
        });


        //combobox
        tipo_subcategoria = (RelativeLayout) findViewById(R.id.tipo_subcategoria);
        txt_categoria = (EditText) findViewById(R.id.txt_categoria);
        tipo_subcategoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showBottomSheetDialogSelect();
            }
        });

        //btn Continuar
        continuar = (Button) findViewById(R.id.continuar);
        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (modelo.selectImge == false || modelo.listaCategorias.get(modelo.selectPosition).subcategorias.size() > 0 && txt_categoria.getText().toString().equals("")) {
                    showAlertDatosObligatorios();
                } else {

                    modelo.productosOferente.setCategoria(modelo.listaCategorias.get(modelo.selectPositionAnterior).getNombre());
                    modelo.productosOferente.setSubcategoria(txt_categoria.getText().toString());
                    Intent i = new Intent(getApplicationContext(), MyPublicacionesTipo.class);
                    overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                    startActivity(i);

                }
            }
        });


        displayCategorias();

        if (!modelo.productosOferente.getCategoria().equals("")) {

            mAdapter.setSelected(modelo.selectPositionAnterior, true);
            if (modelo.listaCategorias.size() > 0) {
                modelo.servicio_producto = modelo.listaCategorias.get(modelo.selectPositionAnterior).getServicio();
                mAdapter.setSelected(modelo.selectPositionAnterior, true);

                if (modelo.listaCategorias.get(modelo.selectPositionAnterior).subcategorias.size() > 0) {
                    layout_select_opciones.setVisibility(View.VISIBLE);
                    // Toast.makeText(getApplicationContext(),""+modelo.listaCategorias.get(modelo.selectPositionAnterior).subcategorias.get(0).getIdNombre(),Toast.LENGTH_LONG).show();
                    txt_categoria.setText(modelo.productosOferente.getSubcategoria());
                    for (int i2 = 0; i2 < modelo.listaCategorias.get(modelo.selectPositionAnterior).subcategorias.size(); i2++) {
                        subcategoria += modelo.listaCategorias.get(modelo.selectPositionAnterior).subcategorias.get(i2).getIdNombre() + ",";
                    }
                    Log.v("ok", "ok" + subcategoria);
                }
            }
        }

        listview.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                txt_categoria.setText("");
                modelo.selectPosition = i;
                String datos = "position: " + i + " " + "id: " + l;
                //Toast.makeText(getApplicationContext(), ""+datos, Toast.LENGTH_LONG).show();

                modelo.servicio_producto = modelo.listaCategorias.get(i).getServicio();
                mAdapter.setSelected(i, true);
                modelo.selectPositionAnterior = i;
                if (modelo.listaCategorias.get(modelo.selectPositionAnterior).subcategorias.size() > 0) {
                    layout_select_opciones.setVisibility(View.VISIBLE);
                    // Toast.makeText(getApplicationContext(),""+modelo.listaCategorias.get(modelo.selectPositionAnterior).subcategorias.get(0).getIdNombre(),Toast.LENGTH_LONG).show();

                    subcategoria = "";
                    for (int i2 = 0; i2 < modelo.listaCategorias.get(modelo.selectPositionAnterior).subcategorias.size(); i2++) {
                        subcategoria += modelo.listaCategorias.get(modelo.selectPositionAnterior).subcategorias.get(i2).getIdNombre() + ",";
                    }
                    Log.v("ok", "ok" + subcategoria);

                } else {
                    layout_select_opciones.setVisibility(View.INVISIBLE);
                    subcategoria = "";
                    txt_categoria.setText("");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        borrarDatosHorario();

    }

    private void borrarDatosHorario() {
        modelo.diasDeLaSemanaArticulo.clear();
        modelo.finDeSemanaFestivosArticulo.clear();
        modelo.horariodesemanaIniArticulo = "";
        modelo.horariodesemanaFinArticulo = "";
        modelo.horariofindesemanaIniArticulo = "";
        modelo.horariofindesemanaFinArticulo = "";
        modelo.latitudArticulo = 0;
        modelo.longitudArticulo = 0;
        modelo.sinJornadaContinuaSemanaArticulo = false;
        modelo.sinJornadaContinuaFinDeSemanaArticulo = false;

        //variables del horario al registrar un  Oferente
        modelo.diasDeLaSemana.clear();
        modelo.finDeSemanaFestivos.clear();

        //variables del horario  oferente
        modelo.horariodesemanaIni = "";
        modelo.horariodesemanaFin = "";
        modelo.horariofindesemanaIni = "";
        modelo.horariofindesemanaFin = "";
        modelo.sinJornadaContinuaSemana = false;
        modelo.sinJornadaContinuaFinDeSemana = false;

    }


    private void displayCategorias() {

        mAdapter = new CategoriasAdapter(this, this);
        listview.setAdapter(mAdapter);
    }


    public void showBottomSheetDialogSelect() {


        final CharSequence[] items = subcategoria.split(",");

        // final CharSequence[] items = new CharSequence[]{"Snacks", "Alimentos"};


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Subcategoría");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                txt_categoria.setText("" + items[item]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public void showAlertCategoria() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("¡Advertencia!");
        alertDialogBuilder.setMessage("Sólo puedes seleccionar una categoría");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Log.v("ok", "ok");
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void showAlertDatosObligatorios() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("¡Advertencia!");
        alertDialogBuilder.setMessage("Todos los campos son obligatorios");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Log.v("ok", "ok");
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        Log.v("cancel", "cancel");
        if (modelo.vista == 1) {
            modelo.vista = 2;


        } else {
            modelo.vistaPublicacion =1;
            modelo.selectImge = false;
           Intent i = new Intent(getApplicationContext(), TapsHome.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
           i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            modelo.productosOferente.setCategoria("");
            modelo.productosOferente.setSubcategoria("");
            borrarDatosHorario();
            modelo.target.clear();

        }
        finish();
        super.onBackPressed();

    }
}


