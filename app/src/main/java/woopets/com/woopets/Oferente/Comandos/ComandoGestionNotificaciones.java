package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 18/12/17.
 */

public class ComandoGestionNotificaciones {

    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    public interface OnComandoGestionNotificacionesChangeListener {

        void cargoEliminar();
        void  cargoActualizacionNotificaion();
    }

    //interface del listener de la actividad interesada
    private ComandoGestionNotificaciones.OnComandoGestionNotificacionesChangeListener mListener;

    public ComandoGestionNotificaciones(ComandoGestionNotificaciones.OnComandoGestionNotificacionesChangeListener mListener){

        this.mListener = mListener;
    }


    public void eliminarNotificaciones(final String id){
        final DatabaseReference ref = database.getReference("mensajes/"+id+"/" );//ruta path
        ref.removeValue();
        mListener.cargoEliminar();
    }

    public void actulizarVistoNotificaciones(final String id){
        final DatabaseReference ref = database.getReference("mensajes/"+id+"/visto/" );//ruta path
        ref.setValue(true);
        mListener.cargoActualizacionNotificaion();
    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoGestionNotificacionesChangeListener sDummyCallbacks = new OnComandoGestionNotificacionesChangeListener()
    {
        @Override
        public void cargoEliminar()
        {}

        @Override
        public void cargoActualizacionNotificaion()
        {}


    };
}
