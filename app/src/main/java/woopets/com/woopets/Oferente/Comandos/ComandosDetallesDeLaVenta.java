package woopets.com.woopets.Oferente.Comandos;

import android.annotation.SuppressLint;
import android.util.Log;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 11/12/17.
 */

public class ComandosDetallesDeLaVenta {

    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnDetallesDeLaVentaChangeListener {


        void finProductoServicio();
    }


    //interface del listener de la actividad interesada
    private ComandosDetallesDeLaVenta.OnDetallesDeLaVentaChangeListener mListener;

    public ComandosDetallesDeLaVenta(ComandosDetallesDeLaVenta.OnDetallesDeLaVentaChangeListener mListener) {

        this.mListener = mListener;

    }

    public void setProductoServicioEntregado(String idCompra) {


        modelo = Modelo.getInstance();


        //colocar metodo update fecha y hora de llegada
        DatabaseReference ref = database.getReference("compras/abiertas/" + idCompra);//ruta path
        DatabaseReference refCerradas = database.getReference("compras/cerradas/" + idCompra);//ruta path
        moverRegistro(ref, refCerradas);

        //mListener.cargoFinalizacionOrden();

    }


    public void moverRegistro(final DatabaseReference fromPath, final DatabaseReference toPath) {
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                modelo = Modelo.getInstance();
                toPath.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            Log.d("error", "Error al mover registros historico");
                        } else {
                            fromPath.removeValue();

                            mListener.finProductoServicio();

                        }
                    }
                });
            }


            @SuppressLint("LongLogTag")
            public void onCancelled(DatabaseError databaseError) {

                Log.v("moveFirebaseRecord() failed. firebaseError", "error intentando copiar datos cerradas");
            }
        });
    }


    public void respuestaNotificacion(final boolean servicio_producto, final String idCliente, final String idPublicacion,  final String infoAdicional, final String  infotokens) {

        String key = referencia.push().getKey();

        DatabaseReference ref = database.getReference("mensajes/" + referencia.push().getKey());

        if (!modelo.uid.equals("")) {

            Map<String, Object> dotosNotificacion = new HashMap<String, Object>();
            dotosNotificacion.put("idCliente", "" + idCliente);
            dotosNotificacion.put("idCompra", "" + idPublicacion);
            dotosNotificacion.put("infoAdicional", "" + infoAdicional);

            if(servicio_producto== true){
                dotosNotificacion.put("mensaje", "El servicio " +infoAdicional + " ha sido efectivo. No olvides calificarnos y enviar tus comentaios para mejorar cada día" + "");
            }
            else{
                dotosNotificacion.put("mensaje", "El producto " + infoAdicional + " ha sido entregado. No olvides calificarnos y enviar tus comentaios para mejorar cada día" + "");
            }
            dotosNotificacion.put("timestamp", ServerValue.TIMESTAMP);
            dotosNotificacion.put("tipo", "compraFinalizada");
            dotosNotificacion.put("titulo", "Compra finalizada");


            String token = infotokens;

            if (token == null || token.equals("")) {
                token = "1";
            }

            Map<String, Object> tokens = new HashMap<String, Object>();
            tokens.put(token, true);
            dotosNotificacion.put("tokens", tokens);

            dotosNotificacion.put("visto", false);

            ref.setValue(dotosNotificacion);
            System.out.print("" + key);

        }


    }

    public void actualizarEstadoVenta(String idCompra) {


        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("h:mm aa");
        System.out.println("Hora: " + hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: " + dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: " + hourdateFormat.format(date));


        String string = "" + hourFormat.format(date);
        String[] parts = string.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];

        if (part2.equals("a.") || part2.equals("a.m") ||  part2.equals("a.m.") || part2.equals("a. m.") || part2.equals("A.")) {
            part2 = "AM";
        }

        if (part2.equals("p.") || part2.equals("p.m") || part2.equals("p.m.") || part2.equals("p. m.") || part2.equals("P.")) {
            part2 = "PM";
        }

        final DatabaseReference ref = database.getReference("compras/cerradas/" + idCompra + "/pedido/1/estado/");//ruta path
        ref.setValue("Cerrada");

        String fecha = dateFormat.format(date) + " " + part1 + " " + part2;

        final DatabaseReference ref4 = database.getReference("compras/cerradas/" + idCompra + "/fechaCierre/");//ruta path
        ref4.setValue(fecha);

        final DatabaseReference ref2 = database.getReference("compras/cerradas/" + idCompra + "/pedido/1/fechaPago/");//ruta path
        ref2.setValue(fecha);

        //al cerrar la compra eliminar rama datosTarjeta
        final DatabaseReference ref3 = database.getReference("compras/cerradas/" + idCompra + "/datosTarjeta/");//ruta path
        ref3.removeValue();

    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static ComandosDetallesDeLaVenta.OnDetallesDeLaVentaChangeListener sDummyCallbacks = new ComandosDetallesDeLaVenta.OnDetallesDeLaVentaChangeListener() {


        public void finProductoServicio() {
        }

    };
}
