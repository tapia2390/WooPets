package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;

import woopets.com.woopets.Oferente.AdapterOferente.PreguntasMisPublicacionesClienteAdapter;
import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class Preguntas_A_MisPublicacionesCliente extends Activity implements PreguntasMisPublicacionesClienteAdapter.AdapterCallback{

    Modelo modelo  = Modelo.getInstance();
    ListView list_preguntas_publicaiones;
    Button btn_titulo;
    private PreguntasMisPublicacionesClienteAdapter mAdapter;
    String idCompra = "";
    String idCliente = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_preguntas__a__mis_publicaciones_cliente);

        list_preguntas_publicaiones =(ListView)findViewById(R.id.list_preguntas_publicaiones);
        btn_titulo =(Button) findViewById(R.id.btn_titulo);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


    }


    @Override
    public void onBackPressed() {
         if(modelo.vista == 1 || modelo.vista == 2){
            modelo.vista = 0;}
            finish();

        super.onBackPressed();
    }

    private void displayListaActivas(){

        mAdapter = new PreguntasMisPublicacionesClienteAdapter(this,this);
        list_preguntas_publicaiones.setAdapter(mAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(modelo.vista == 1) {

            btn_titulo.setText(""+modelo.classPreguntas_a_misPublicaciones.get(modelo.idPreguntasClientePublicacionesPosicion).getListaProductosOferentes().get(0).getTitulo());

            if (getIntent().hasExtra("IDCOMPRA")) {
                idCompra = getIntent().getStringExtra("IDCOMPRA");
            } else {
                return;   //Esto es para que alguna vista esta llamando esta vista y no pasa el parametro del IDCOMPRA el error sea evidente y asi detectarlo mas facil
            }

            if (getIntent().hasExtra("IDCLIENTE")) {
                idCliente = getIntent().getStringExtra("IDCLIENTE");
            } else {
                return;   //Esto es para que alguna vista esta llamando esta vista y no pasa el parametro del IDCOMPRA el error sea evidente y asi detectarlo mas facil
            }

            modelo.adicionarListaPreguntasAmisPublcaciones2(modelo.idPreguntasClientePublicaciones,idCliente);
        }

        else if(modelo.vista == 2) {
            modelo.adicionarListaPreguntas(modelo.idPreguntasClientePublicaciones, modelo.filtro);

            btn_titulo.setText(""+modelo.nombreProducto);


        }
        else{

            ClassPreguntas_A_MisPublicaciones preguntaPublicacion = modelo.getPreguntaById(modelo.idPreguntasClientePublicaciones);

            btn_titulo.setText(""+preguntaPublicacion.getListaProductosOferentes().get(0).getTitulo());


        }

        displayListaActivas();
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }
}
