package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import woopets.com.woopets.Oferente.AdapterOferente.ChatAdapter;
import woopets.com.woopets.Oferente.AdapterOferente.MisVentasAbiertasCerradasAdapter;
import woopets.com.woopets.Oferente.Comandos.ComandoChatOferente;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class OferenteChat extends Activity implements ComandoChatOferente.OnComandoChatListener,ChatAdapter.AdapterCallback{

    ImageButton chatSendButton;
    ListView messagesContainer;
    ChatAdapter mAdapter;
    LinearLayout chatControls;

    TextView messageedit;
    ComandoChatOferente comandoChatOferente;
    Modelo modelo = Modelo.getInstance();
    String idCompra = "";
    CompraProductoAbiertaCerrada compra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_oferente_chat);

        chatSendButton = (ImageButton) findViewById(R.id.chatSendButton);
        messagesContainer = (ListView) findViewById(R.id.messagesContainer);
        chatControls = (LinearLayout) findViewById(R.id.chatControls);
        messageedit = (TextView) findViewById(R.id.messageedit);

        comandoChatOferente = new ComandoChatOferente(this);
        comandoChatOferente.getChatOferente2(modelo.idCompara, modelo.idCliente);



        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

        if (getIntent().hasExtra("IDCOMPRA")) {
            idCompra = getIntent().getStringExtra("IDCOMPRA");
            compra = modelo.getCompraByIdCompra(idCompra);

        } else {
            return;   //Esto es para que alguna vista esta llamando esta vista y no pasa el parametro del IDCOMPRA el error sea evidente y asi detectarlo mas facil
        }


        modelo.chatVistos = 0;
        //Toast.makeText(getApplicationContext(),""+modelo.abiertaCerrada,Toast.LENGTH_LONG).show();
        if(compra.getAbiertaCerrada().equals("cerradas") || compra.getPedidos().get(0).getEstado().equals("Cerrada")){
            chatControls.setVisibility(View.GONE);
        }


        messageedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), RespuestaChat.class);
                i.putExtra("IDCOMPRA",idCompra);
                startActivity(i);
                //finish();
            }
        });
    }


    public void  setMsm(View v){
        Intent i = new Intent(getApplicationContext(), RespuestaChat.class);
        i.putExtra("IDCOMPRA",idCompra);
        startActivity(i);
        //finish();
    }



    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    @Override
    public void getChasts() {

        if(modelo.chatOferentes.size() > 0){
            displayLista();
        }
    }

    @Override
    public void setChasts() {

    }


    private void displayLista() {

        mAdapter = new ChatAdapter(this, this);
        messagesContainer.setAdapter(mAdapter);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }
}
