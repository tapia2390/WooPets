package woopets.com.woopets.Oferente.AdapterOferente;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import woopets.com.woopets.Oferente.Comandos.ComandoGestionNotificaciones;
import woopets.com.woopets.Oferente.Comandos.ComandoNotificaciones;
import woopets.com.woopets.Oferente.Comandos.ComandoPreguntaPublicacionesNotificacion;
import woopets.com.woopets.Oferente.DetallesVenasAC;
import woopets.com.woopets.Oferente.FelicitacionesLoSentimos;
import woopets.com.woopets.Oferente.Fragment.MyNotificacion;
import woopets.com.woopets.Oferente.ModelOferente.ChatOferente;
import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntasFrecuentes;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.NotificaionesOferente;
import woopets.com.woopets.Oferente.OferenteChat;
import woopets.com.woopets.Oferente.Preguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.Preguntas_A_MisPublicacionesCliente;
import woopets.com.woopets.Oferente.VenderEnMypet;
import woopets.com.woopets.Oferente.VentasCerradasFecha_Cantidad;
import woopets.com.woopets.R;

/**
 * Created by tactomotion on 26/12/16.
 */
public class NotificacionesAdapter extends BaseAdapter implements ComandoGestionNotificaciones.OnComandoGestionNotificacionesChangeListener, ComandoNotificaciones.OnComandoNotificacionesChangeListener, ComandoPreguntaPublicacionesNotificacion.OnComandoPreguntaPublicacionesNotificacionhangeListener {

    //Atributos del adaptador
    private final Context mContext;
    MyNotificacion mContext2;
    AdapterCallback mListener = sDummyCallbacks;
    private List<NotificaionesOferente> notificacion;
    private Modelo sing = Modelo.getInstance();
    private ProgressDialog progressDialog;
    ComandoGestionNotificaciones comadogetionarNotificaciones;
    ComandoNotificaciones comandoNotificaciones;
    ComandoPreguntaPublicacionesNotificacion comandoPreguntaPublicacionesNotificacion;
    ArrayList<NotificaionesOferente> listaNotifications2 = new java.util.ArrayList<NotificaionesOferente>();
    public NotificacionesAdapter(MyNotificacion noti,Context mContext, AdapterCallback mListener) {


       /* Set<NotificaionesOferente> quipu = new HashSet<NotificaionesOferente>(sing.listaNotifications);
        for (NotificaionesOferente key : quipu) {
            System.out.println(key + " : " + Collections.frequency(sing.listaNotifications, key));
        }*/


        //ordenar
       /* Collections.sort(sing.listaNotifications, new Comparator<NotificaionesOferente>() {
            @Override
            public int compare(NotificaionesOferente o1, NotificaionesOferente o2) {
                return new Long(o2.getTimestamp()).compareTo(new Long(o1.getTimestamp()));
            }
        });*/


        Collections.sort(sing.listaNotifications, new Comparator<NotificaionesOferente>() {
                    @Override
                    public int compare(final NotificaionesOferente object1, final NotificaionesOferente object2) {
                        return new Long(object1.getTimestamp()).compareTo(new Long(object2.getTimestamp()));
                    }
                }
        );

        for (int i = sing.listaNotifications.size() - 1; i >= 0; i--) {
            System.out.print("notific: " + sing.listaNotifications.get(i).getTitulo());
            listaNotifications2.add(sing.listaNotifications.get(i));

        }
        sing.listaNotifications.clear();
        sing.listaNotifications = listaNotifications2;

        sing.filtro = 0;
        this.mContext = mContext;
        this.mListener = mListener;
        this.notificacion = sing.listaNotifications;
        this.mContext2  = noti;

    }

    @Override
    public int getCount() {
        return notificacion.size();
    }

    @Override
    public Object getItem(int position) {
        return notificacion.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // Declare Variables
        final LinearLayout layout_adapter;
        final ImageView imageView4;
        final ImageView imageView5;
        final TextView txtTitulo;
        final TextView txtrespuesta;
        final Button btn_color;


        final NotificaionesOferente listaNotificicaion = (NotificaionesOferente) getItem(position);


        // Inflate la vista de la Orden
        LinearLayout itemLayout = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.adapter_notificaciones, parent, false);


        progressDialog = new ProgressDialog(mContext);
        // Toma los elementos a utilizar
        layout_adapter = (LinearLayout) itemLayout.findViewById(R.id.layout_adapter);
        imageView4 = (ImageView) itemLayout.findViewById(R.id.imageView4);
        imageView5 = (ImageView) itemLayout.findViewById(R.id.imageView5);
        txtTitulo = (TextView) itemLayout.findViewById(R.id.txtTitulo);
        txtrespuesta = (TextView) itemLayout.findViewById(R.id.txtrespuesta);
        btn_color = (Button) itemLayout.findViewById(R.id.btn_color);

        comadogetionarNotificaciones = new ComandoGestionNotificaciones(this);
        comandoNotificaciones = new ComandoNotificaciones(this);

        comandoPreguntaPublicacionesNotificacion = new ComandoPreguntaPublicacionesNotificacion(this);


        if (listaNotificicaion.getTipo().equals("pregunta-respuesta")) {
            imageView5.setBackgroundResource(R.drawable.btnpreguntaspublicaciones);
            txtTitulo.setText("Nueva pregunta en:");
            txtrespuesta.setText("" + listaNotificicaion.getInfoAdicional());
        }

        if (listaNotificicaion.getTipo().equals("chat")) {
            imageView5.setBackgroundResource(R.drawable.imgchat);
            txtTitulo.setText("Nuevo mensaje en Chat:");
            txtrespuesta.setText("Nuevo mensaje");
        }

        if (listaNotificicaion.getTipo().equals("registro")) {
            imageView5.setBackgroundResource(R.drawable.registro_notificacion);

            if (listaNotificicaion.getTitulo().equals("¡Felicitaciones!")) {
                txtTitulo.setText("¡Felicitaciones!");
                txtrespuesta.setText("Tu registro ha sido aprobado");
            } else {
                txtTitulo.setText("¡Lo sentimos!");
                txtrespuesta.setText("Tu registro no ha sido aprobado");
            }


        }

        if (listaNotificicaion.getTipo().equals("inventario")) {
            imageView5.setBackgroundResource(R.drawable.inventario_notificacion);
            txtrespuesta.setText("" + listaNotificicaion.getInfoAdicional());
            txtTitulo.setText("" + listaNotificicaion.getTitulo());
        }


        if (listaNotificicaion.getTipo().equals("ventaRealizada")) {
            imageView5.setBackgroundResource(R.drawable.imgventa);
            txtTitulo.setText("Venta realizada:");
            txtrespuesta.setText("" + listaNotificicaion.getInfoAdicional());
        }

        if (listaNotificicaion.getTipo().equals("ventaCalificada")) {
            imageView5.setBackgroundResource(R.drawable.imgventa);
            txtrespuesta.setText("" + listaNotificicaion.getInfoAdicional());
            txtTitulo.setText("Venta calificada:");
        }


        if (listaNotificicaion.getVisto() == true) {
            btn_color.setVisibility(View.GONE);
        }

        layout_adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext,"id: "+listaNotificicaion.getId(),Toast.LENGTH_LONG).show();


                if (listaNotificicaion.getTipo().equals("pregunta-respuesta")) {
                    sing.vista = 2;
                    sing.idPreguntasClientePublicaciones = listaNotificicaion.getIdPublicacion();
                    comadogetionarNotificaciones.actulizarVistoNotificaciones(listaNotificicaion.getId());

                    sing.nombreProducto = listaNotificicaion.getInfoAdicional();
                    //Toast.makeText(mContext, "Cargando información...", Toast.LENGTH_LONG).show();
                    sing.idPreguntasClientePublicaciones = listaNotificicaion.getIdPublicacion();
                    sing.filtro = 1;

                    Intent i = new Intent(mContext, Preguntas_A_MisPublicacionesCliente.class);
                    mContext.startActivity(i);
                    //comandoPreguntaPublicacionesNotificacion.getPreguntasPublicaciones(listaNotificicaion.getIdPublicacion());

                }

                if (listaNotificicaion.getTipo().equals("registro")) {

                    String aprobado_rechazado = "0";

                    if (listaNotificicaion.getTitulo().equals("¡Felicitaciones!")) {
                        aprobado_rechazado = "1";
                    } else {
                        aprobado_rechazado = "0";
                    }

                    comadogetionarNotificaciones.actulizarVistoNotificaciones(listaNotificicaion.getId());
                    Intent i = new Intent(mContext, FelicitacionesLoSentimos.class);
                    i.putExtra("exitosoNegado", aprobado_rechazado);
                    mContext.startActivity(i);
                }

                if (listaNotificicaion.getTipo().equals("ventaRealizada")) {
                    comadogetionarNotificaciones.actulizarVistoNotificaciones(listaNotificicaion.getId());
                    Intent i = new Intent(mContext, DetallesVenasAC.class);
                    i.putExtra("IDCOMPRA", listaNotificicaion.getIdCompra());
                    mContext.startActivity(i);


                }

                if (listaNotificicaion.getTipo().equals("ventaCalificada")) {
                    comadogetionarNotificaciones.actulizarVistoNotificaciones(listaNotificicaion.getId());
                    Intent i = new Intent(mContext, DetallesVenasAC.class);
                    i.putExtra("IDCOMPRA", listaNotificicaion.getIdCompra());
                    mContext.startActivity(i);

                }


                if (listaNotificicaion.getTipo().equals("chat")) {
                    comadogetionarNotificaciones.actulizarVistoNotificaciones(listaNotificicaion.getId());
                    sing.idCompara = listaNotificicaion.getId();
                    sing.idCliente = listaNotificicaion.getIdCliente();
                    Intent i = new Intent(mContext, DetallesVenasAC.class);
                    i.putExtra("IDCOMPRA", listaNotificicaion.getIdCompra());
                    mContext.startActivity(i);

                }


            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(mContext);
                // Setting Alert Dialog Title
                alertDialogBuilder.setTitle("Eliminar Notificación");
                // Icon Of Alert Dialog
                //alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
                // Setting Alert Dialog Message
                alertDialogBuilder.setMessage("¿Estas seguro (a) de eliminar la notificación selecionada?");
                alertDialogBuilder.setCancelable(false);

                alertDialogBuilder.setPositiveButton("Si, continuar", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.v("ok", "ok");

                        progressDialog.setMessage("Eliminando mensaje Por favor, espere!...");
                        progressDialog.show();
                        comadogetionarNotificaciones.eliminarNotificaciones(listaNotificicaion.getId());

                        mContext2.refres2();

                    }
                });

                alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(getApplicationContext(),"You clicked over No",Toast.LENGTH_SHORT).show();
                    }
                });

                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });


        return itemLayout;

    }



    static void burbuja(int arreglo[])

    {

        for(int i = 0; i < arreglo.length - 1; i++)

        {

            for(int j = 0; j < arreglo.length - 1; j++)

            {

                if (arreglo[j] < arreglo[j + 1])

                {

                    int tmp = arreglo[j+1];

                    arreglo[j+1] = arreglo[j];

                    arreglo[j] = tmp;

                }

            }

        }

        for(int i = 0;i < arreglo.length; i++)

        {

            System.out.print(arreglo[i]+"\n");

        }

    }


    @Override
    public void cargoEliminar() {
        //notifyDataSetChanged();
        progressDialog.dismiss();
        comandoNotificaciones.getNotificocaiones();
    }

    @Override
    public void cargoActualizacionNotificaion() {

        Log.v("DatosUpdate", "OK");

    }

    @Override
    public void cargoCategorias() {
        //Toast.makeText(mContext,"adapter",Toast.LENGTH_SHORT).show();

        // MyNotificacion mn = new MyNotificacion();
        // mn.refres();
    }

    @Override
    public void cargoTipoUsuario(String tipo) {

    }

    @Override
    public void cargoCambio() {

    }

    @Override
    public void preguntasMisPublicaciones() {


    }

    @Override
    public void preguntasMisPublicacionesNotificacion() {


    }


    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
   */
    public interface AdapterCallback {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback() {


    };
}
