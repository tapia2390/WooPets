package woopets.com.woopets.Oferente.tpaga.Model;

/**
 * Created by tacto on 9/08/17.
 */

public class CustomerCard {


    String token;

    public static  CustomerCard create(String token){
        CustomerCard customerCard = new CustomerCard();
        customerCard.token = token;
        return  customerCard;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
