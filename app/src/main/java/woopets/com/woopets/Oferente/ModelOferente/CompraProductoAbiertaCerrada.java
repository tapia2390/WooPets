package woopets.com.woopets.Oferente.ModelOferente;

import java.util.ArrayList;

import woopets.com.woopets.Oferente.ModelOferente.uploadFirebase.DatosTarjetaCompra;

/**
 * Created by tacto on 1/11/17.
 */


public class CompraProductoAbiertaCerrada {

    String abiertaCerrada = "";
    String key = "";
    String fecha = "";
    String idCliente = "";
    String metodoPago  = "";
    String idOferente = "";
    String metodoDePago = "";
    int timestamp = 0;
    int valor = 0;


    ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
    ArrayList<Cliente> cliente = new ArrayList<Cliente>();
    ArrayList<DatosTarjetaCompra> tarjeta = new ArrayList<DatosTarjetaCompra>();


    public CompraProductoAbiertaCerrada() {

    }
    public CompraProductoAbiertaCerrada(String key) {
        this.key = key;
    }


    public String getAbiertaCerrada() {
        return abiertaCerrada;
    }

    public void setAbiertaCerrada(String abiertaCerrada) {
        this.abiertaCerrada = abiertaCerrada;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdOferente() {
        return idOferente;
    }

    public void setIdOferente(String idOferente) {
        this.idOferente = idOferente;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public ArrayList<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(ArrayList<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public ArrayList<Cliente> getCliente() {
        return cliente;
    }

    public void setCliente(ArrayList<Cliente> cliente) {
        this.cliente = cliente;
    }

    public String getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(String metodoPago) {
        this.metodoPago = metodoPago;
    }

    public String getMetodoDePago() {
        return metodoDePago;
    }

    public void setMetodoDePago(String metodoDePago) {
        this.metodoDePago = metodoDePago;
    }

    public ArrayList<DatosTarjetaCompra> getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(ArrayList<DatosTarjetaCompra> tarjeta) {
        this.tarjeta = tarjeta;
    }
}
