package woopets.com.woopets.Oferente;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.LinearLayoutManager;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import woopets.com.woopets.BuildConfig;
import woopets.com.woopets.Oferente.Comandos.ComandoGeoFire;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.Comandos.ComandoRegistroOferente;
import woopets.com.woopets.R;

public class RegistroOferente extends Activity implements ComandoRegistroOferente.OnComandoRegistroOferenteChangeListener, ComandoGeoFire.OnGeoFireChangeListener {


    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser user = mAuth.getCurrentUser();
    Modelo modelo = Modelo.getInstance();
    final Context context = this;
    BottomSheetBehavior behavior;
    private BottomSheetDialog mBottomSheetDialog;

    EditText txt_razon_social, txt_nit, txt_direecion, txt_telefonofijo, txt_telefonofijo_ext, txt_celular, txt_correoelectronico, txt_paginaweb;
    EditText txt_nombre_completo, txt_numdocumento, txt_celular_cont_ext, txt_telefonofijo_cont, txt_celular_cont, txt_correoelectronico_cont, txt_passwor_usu_cont;
    TextView txt_horariodomicilio;
    EditText txt_tipodocumento;
    ImageView imgdireecion;
    LinearLayout horariolayout, finhorariolayout;
    private static final String TAG = "AndroidBash";

    TextView textodiassemana, textofindiassemana, textodiasfinsemana, textofindiafinssemana;

    TextView cierre_etre_fin_semana, cierre_etre_semana;

    private ProgressDialog progressDialog;
    private final int REQUEST_PERMISSION = 1000;
    double latitude, longitud;


    String diasDeLaSemana = "";
    String findiasDeLaSemana = "";
    ComandoRegistroOferente comandoRegistroOferente;
    public LocationManager locationManager;

    ComandoGeoFire comandoGeoFire;
    int estadoActividad =0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registro_oferente);

        comandoRegistroOferente = new ComandoRegistroOferente(this);
        comandoGeoFire = new ComandoGeoFire(this);

        txt_razon_social = (EditText) findViewById(R.id.txt_razon_social);
        txt_nit = (EditText) findViewById(R.id.txt_nit);
        txt_direecion = (EditText) findViewById(R.id.txt_direecion);
        txt_telefonofijo = (EditText) findViewById(R.id.txt_telefonofijo);
        txt_celular = (EditText) findViewById(R.id.txt_celular);
        txt_correoelectronico = (EditText) findViewById(R.id.txt_correoelectronico);
        txt_paginaweb = (EditText) findViewById(R.id.txt_paginaweb);
        txt_nombre_completo = (EditText) findViewById(R.id.txt_nombre_completo);
        txt_numdocumento = (EditText) findViewById(R.id.txt_numdocumento);
        txt_telefonofijo_cont = (EditText) findViewById(R.id.txt_telefonofijo_cont);
        txt_celular_cont = (EditText) findViewById(R.id.txt_celular_cont);
        txt_correoelectronico_cont = (EditText) findViewById(R.id.txt_correoelectronico_cont);
        txt_passwor_usu_cont = (EditText) findViewById(R.id.txt_passwor_usu_cont);
        txt_tipodocumento = (EditText) findViewById(R.id.txt_tipodocumento);
        txt_horariodomicilio = (TextView) findViewById(R.id.txt_horariodomicilio);
        horariolayout = (LinearLayout) findViewById(R.id.horariolayout);
        finhorariolayout = (LinearLayout) findViewById(R.id.finhorariolayout);
        txt_telefonofijo_ext = (EditText) findViewById(R.id.txt_telefonofijo_ext);
        txt_celular_cont_ext = (EditText) findViewById(R.id.txt_celular_cont_ext);

        textodiassemana = (TextView) findViewById(R.id.textodiassemana);
        textofindiassemana = (TextView) findViewById(R.id.textofindiassemana);
        textodiasfinsemana = (TextView) findViewById(R.id.textodiasfinsemana);
        textofindiafinssemana = (TextView) findViewById(R.id.textofindiafinssemana);

        imgdireecion = (ImageView) findViewById(R.id.imgdireecion);

        cierre_etre_fin_semana = (TextView) findViewById(R.id.cierre_etre_fin_semana);
        cierre_etre_semana = (TextView) findViewById(R.id.cierre_etre_semana);


    }


    private void showBottomSheetDialog() {
        final CharSequence[] items = {"Cédula de ciudadanía", "Cédula de extranjería", "NIT para personas jurídicas"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tipo de documento");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                txt_tipodocumento.setText(items[item]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void tipodocumento(View v) {
        //Toast.makeText(getApplicationContext(), ":) ",Toast.LENGTH_SHORT).show();
        showBottomSheetDialog();
    }
    //fin sheet


    //atras hadware
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            comfirmAlertCancelar();
        }
        return true;
    }



    public void cancelar(View v) {
        comfirmAlertCancelar();
    }


    public void aceptar(View v) {

        if (estaConectado()) {

            if (txt_razon_social.getText().toString().equals("") || txt_nit.getText().toString().equals("") || txt_direecion.getText().toString().equals("") || txt_telefonofijo.getText().toString().equals("")
                    || txt_celular.getText().toString().equals("") || txt_correoelectronico.getText().toString().equals("") || txt_nombre_completo.getText().toString().equals("")
                    || txt_numdocumento.getText().toString().equals("") || txt_telefonofijo_cont.getText().toString().equals("") || txt_celular_cont.getText().toString().equals("") || txt_correoelectronico_cont.getText().toString().equals("")
                    || txt_tipodocumento.getText().toString().equals("") || txt_passwor_usu_cont.getText().toString().equals("")) {

                showAlertAceptar();
            }

            if (txt_direecion.getText().toString().length() > 0) {
                txt_direecion.setEnabled(true);

            }
            if (modelo.diasDeLaSemana.size() <= 0 && modelo.finDeSemanaFestivos.size() <= 0) {
                showAlertAceptar();

            } else if (!validarEmail(txt_correoelectronico.getText().toString())) {
                txt_correoelectronico.setError("Email no válido");
                showAlertAceptar();
            } else if (!validarEmail(txt_correoelectronico_cont.getText().toString())) {
                txt_correoelectronico_cont.setError("Email no válido");
                showAlertAceptar();
            } else if (txt_telefonofijo.getText().length() <= 6) {
                txt_telefonofijo.setError("Teléfono no valido");
                showAlertAceptar();
            } else if (txt_celular.getText().length() <= 9) {
                txt_celular.setError("Celular no valido");
                showAlertAceptar();
            } else if (txt_celular_cont.getText().length() <= 9) {
                txt_celular_cont.setError("Celular no valido");
                showAlertAceptar();
            } else if (txt_telefonofijo_cont.getText().length() <= 6) {
                txt_telefonofijo_cont.setError("Teléfono no valido");
                showAlertAceptar();
            } else if (txt_passwor_usu_cont.getText().length() < 7) {
                txt_passwor_usu_cont.setError("Contraseña muy corta");
                showAlertAceptar();
            } else if (modelo.latitud == null || modelo.longitud == null || modelo.latitud == 0 || modelo.longitud == 0 || modelo.latitud == 0.0 || modelo.longitud == 0.0) {
                showAlerGpsDesactivado();
            } else {
                Log.v("ok", "ok");
                progressDialog.setMessage("Validando la información, por favor, espere...");
                progressDialog.show();
                registro();

            }
        } else {
            showAlertSinInternet();
        }
    }

    public void comfirmAlertCancelar() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // Setting Alert Dialog Title
        alertDialogBuilder.setTitle("" + getString(R.string.confirmar));
        // Icon Of Alert Dialog
        //alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // Setting Alert Dialog Message
        alertDialogBuilder.setMessage("" + getString(R.string.abandonarvista));
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Log.v("ok", "ok");

                modelo.sinJornadaContinuaSemana = false;
                modelo.sinJornadaContinuaFinDeSemana = false;


                modelo.diasDeLaSemanaArticulo.clear();
                modelo.finDeSemanaFestivosArticulo.clear();
                modelo.horariodesemanaIniArticulo = "";
                modelo.horariodesemanaFinArticulo = "";
                modelo.horariofindesemanaIniArticulo = "";
                modelo.horariofindesemanaFinArticulo = "";
                modelo.latitudArticulo = 0;
                modelo.longitudArticulo = 0;
                modelo.sinJornadaContinuaSemanaArticulo = false;
                modelo.sinJornadaContinuaFinDeSemanaArticulo = false;

                //variables del horario al registrar un  Oferente
                modelo.diasDeLaSemana.clear();
                modelo.finDeSemanaFestivos.clear();

                //variables del horario  oferente
                modelo.horariodesemanaIni = "";
                modelo.horariodesemanaFin = "";
                modelo.horariofindesemanaIni = "";
                modelo.horariofindesemanaFin = "";
                modelo.latitud = 0.0;
                modelo.longitud = 0.0;
                modelo.sinJornadaContinuaSemana = false;
                modelo.sinJornadaContinuaFinDeSemana = false;


                borrarSharedPreferenceOferente();
                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                finish();
            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getApplicationContext(),"You clicked over No",Toast.LENGTH_SHORT).show();
            }
        });
       /* alertDialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"You clicked on Cancel",Toast.LENGTH_SHORT).show();
            }
        });*/

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void showAlertAceptar() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.informacionincompleta));

        // set dialog message
        alertDialogBuilder
                .setMessage("" + getString(R.string.informacionincompletaDes))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public void relative(View v) {
        //Toast.makeText(getApplicationContext(),"relative", Toast.LENGTH_SHORT).show();
        guardarSharedPreferenceOferente();
        Intent i = new Intent(getApplicationContext(), HorarioAtencionDomicilio.class);
        i.putExtra("horario", " ");
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
    }

    public void showAlertRegistro() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.recibido_solicitus));

        // set dialog message
        alertDialogBuilder
                .setMessage("" + getString(R.string.recibido_solicitusd_descripcion))
                .setCancelable(false)
                .setPositiveButton("OK,ENTENDIDO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent inte = new Intent(getBaseContext(), TapsHome.class);
                        startActivity(inte);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    //validar email
    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    //sharedpreferences
    public void cargarSharedPreferenceOferente() {
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias usuario", Context.MODE_PRIVATE);
        // SharedPreferences.Editor editor =sharedPreferences.edit();

        txt_razon_social.setText(sharedPreferences.getString("txt_razon_social", ""));
        txt_nit.setText(sharedPreferences.getString("txt_nit", ""));
        txt_direecion.setText(sharedPreferences.getString("txt_direecion", ""));
        txt_telefonofijo.setText(sharedPreferences.getString("txt_telefonofijo", ""));
        txt_celular.setText(sharedPreferences.getString("txt_celular", ""));
        txt_correoelectronico.setText(sharedPreferences.getString("txt_correoelectronico", ""));
        txt_paginaweb.setText(sharedPreferences.getString("txt_paginaweb", ""));
        txt_nombre_completo.setText(sharedPreferences.getString("txt_nombre_completo", ""));
        txt_numdocumento.setText(sharedPreferences.getString("txt_numdocumento", ""));
        txt_telefonofijo_cont.setText(sharedPreferences.getString("txt_telefonofijo_cont", ""));
        txt_celular_cont.setText(sharedPreferences.getString("txt_celular_cont", ""));
        txt_correoelectronico_cont.setText(sharedPreferences.getString("txt_correoelectronico_cont", ""));
        txt_tipodocumento.setText(sharedPreferences.getString("txt_tipodocumento", ""));
        txt_passwor_usu_cont.setText(sharedPreferences.getString("txt_passwor_usu_cont", ""));

    }

    public void guardarSharedPreferenceOferente() {
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias usuario", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();


        editor.putString("txt_razon_social", txt_razon_social.getText().toString());
        editor.putString("txt_nit", txt_nit.getText().toString());
        editor.putString("txt_direecion", txt_direecion.getText().toString());
        editor.putString("txt_telefonofijo", txt_telefonofijo.getText().toString());
        editor.putString("txt_celular", txt_celular.getText().toString());
        editor.putString("txt_correoelectronico", txt_correoelectronico.getText().toString());
        editor.putString("txt_paginaweb", txt_paginaweb.getText().toString());
        editor.putString("txt_nombre_completo", txt_nombre_completo.getText().toString());
        editor.putString("txt_numdocumento", txt_numdocumento.getText().toString());
        editor.putString("txt_telefonofijo_cont", txt_telefonofijo_cont.getText().toString());
        editor.putString("txt_celular_cont", txt_celular_cont.getText().toString());
        editor.putString("txt_correoelectronico_cont", txt_correoelectronico_cont.getText().toString());
        editor.putString("txt_tipodocumento", txt_tipodocumento.getText().toString());
        editor.putString("txt_passwor_usu_cont", txt_passwor_usu_cont.getText().toString());

        editor.commit();
    }


    public void borrarSharedPreferenceOferente() {
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias usuario", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();


        editor.putString("txt_razon_social", "");
        editor.putString("txt_nit", "");
        editor.putString("txt_direecion", "");
        editor.putString("txt_telefonofijo", "");
        editor.putString("txt_celular", "");
        editor.putString("txt_correoelectronico", "");
        editor.putString("txt_paginaweb", "");
        editor.putString("txt_nombre_completo", "");
        editor.putString("txt_numdocumento", "");
        editor.putString("txt_telefonofijo_cont", "");
        editor.putString("txt_celular_cont", "");
        editor.putString("txt_correoelectronico_cont", "");
        editor.putString("txt_tipodocumento", "");
        editor.putString("txt_passwor_usu_cont", "");

        editor.commit();
    }


    //validacion conexion internet
    protected Boolean estaConectado() {
        if (conectadoWifi()) {
            Log.v("wifi", "Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        } else {
            if (conectadoRedMovil()) {
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            } else {
                showAlertSinInternet();
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    protected Boolean conectadoWifi() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }


    public void showAlertSinInternet() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.sinInternet));

        // set dialog message
        alertDialogBuilder
                .setMessage("" + getString(R.string.sinInternetDescripcion))
                .setCancelable(false)
                .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    //GpS
    public void gps(View v) {


        //Verificamos si el GPS esta prendido o no:
        guardarSharedPreferenceOferente();

        if (Build.VERSION.SDK_INT >= 23) {
            estadoActividad =1;
            checkPermission();
        } else {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            } else {
                if (estaConectado()) {
                    estadoActividad =1;
                    locationStart();
                } else {
                    showAlertSinInternet();
                }
            }
        }
    }


    //gps inicio
    //gps+

    private void locationStart() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Localizacion Local = new Localizacion();
        Local.setMainActivity(this);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);


        if (gpsEnabled) {
            LocationManager locationManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));


            try {
                latitude = Double.parseDouble("" + location.getLatitude());
                longitud = Double.parseDouble("" + location.getLongitude());
                modelo.latitud = latitude;
                modelo.longitud = longitud;
            } catch (Exception e) {
                Log.v("exeption", "exeption");
            }



            if(estadoActividad == 1){
                guardarSharedPreferenceOferente();
                imgdireecion.setClickable(false);
                imgdireecion.setEnabled(false);
                Intent i = new Intent(getApplicationContext(), MapsActivityOferente.class);
                i.putExtra("actividad", "RegistroPerfilOferente");
                startActivity(i);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }


        } else {
            showAlerGpsDesactivado();
        }

    }


    public void setLocation(Location loc) {
        //Obtener la direcci—n de la calle a partir de la latitud y la longitud
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
                String address = "";
                if (list.size() > 0) {
                    // Address address = list.get(0);
                    for (int i = 0; i < list.get(0).getMaxAddressLineIndex(); i++) {
                        address += list.get(0).getAddressLine(i) + "\n";
                    }
                }

                //Toast.makeText(getApplicationContext(),"Mi dirección es ...: \n \n"+""+list.get(0).getThoroughfare()+list.get(0).getFeatureName() + address, Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                System.out.print("" + e.getLocalizedMessage() + " " + e.getMessage());
                Log.v("ErrorDir", "ErrorDir" + "" + e.getLocalizedMessage() + " " + e.getMessage());
            }
        }
    }

    @Override
    public void informacionRegistroOferente() {


    }

    @Override
    public void errorInformacionRegistroOferente() {

        progressDialog.dismiss();
        // Toast.makeText(getApplicationContext(),"Error al enviar la información por favor valide su conexión.",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void cargoGeoFire() {
        logueo();
    }

    @Override
    public void errorGeoFire() {

    }

    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener {
        RegistroOferente mainActivity;

        public RegistroOferente getMainActivity() {
            return mainActivity;
        }

        public void setMainActivity(RegistroOferente mainActivity) {
            this.mainActivity = mainActivity;
        }


        @Override
        public void onLocationChanged(Location loc) {
            // Este mŽtodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la detecci—n de un cambio de ubicacion
            loc.getLatitude();
            loc.getLongitude();

            modelo.latitud = loc.getLatitude();
            modelo.longitud = loc.getLongitude();
            String Text = "Mi ubicaci—n actual es: " + "\n Lat = "
                    + loc.getLatitude() + "\n Long = " + loc.getLongitude();


            //  this.mainActivity.setLocation(loc);
        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este mŽtodo se ejecuta cuando el GPS es desactivado
            //"GPS Desactivado"
        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este mŽtodo se ejecuta cuando el GPS es activado
            //"GPS Activado"
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }


    }


    public void showAlerGpsDesactivado() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // Setting Alert Dialog Title
        alertDialogBuilder.setTitle("GPS deshabilitado");
        // Icon Of Alert Dialog
        alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // Setting Alert Dialog Message
        alertDialogBuilder.setMessage("Active la función de localización para determinar su ubicación ");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Ajustes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(settingsIntent);
            }
        });

        alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //
            }
        });
        /*alertDialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"You clicked on Cancel",Toast.LENGTH_SHORT).show();
            }
        });*/

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    //gps fin


    @Override
    public void onStart() {
        super.onStart();

        cargaRegistro();

        imgdireecion.setClickable(true);
        imgdireecion.setEnabled(true);

        if (mAuth == null || mAuthListener == null) {
            return;
        } else {
            mAuth.addAuthStateListener(mAuthListener);
        }


    }

    private void cargaRegistro() {

         diasDeLaSemana = "";
         findiasDeLaSemana = "";


        locationStart2();
        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);


        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        if (modelo.diasDeLaSemana.size() > 0) {
            horariolayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < modelo.diasDeLaSemana.size(); i++) {
                diasDeLaSemana += modelo.diasDeLaSemana.get(i) + ",";
            }

            String dias = diasDeLaSemana.substring(0, diasDeLaSemana.length() - 1);
            // DIAS DE LA SEMANA
            textodiassemana.setText(dias);
            //HORARIO INICIO Y FIN
            textofindiassemana.setText(modelo.horariodesemanaIni + " - " + modelo.horariodesemanaFin);
            //SERVICIO CONTINUO
            if (modelo.sinJornadaContinuaSemana == true) {
                cierre_etre_semana.setText("Cerramos entre 12 y 2 pm");
            }


        }
        if (modelo.finDeSemanaFestivos.size() > 0) {

            finhorariolayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < modelo.finDeSemanaFestivos.size(); i++) {
                findiasDeLaSemana += modelo.finDeSemanaFestivos.get(i) + ",";
            }

            String diasf = findiasDeLaSemana.substring(0, findiasDeLaSemana.length() - 1);
            //DIAS  FIN DE SEMANA
            textodiasfinsemana.setText(diasf);
            // HORARIO FINDE SEMANA INICIO Y FIN
            textofindiafinssemana.setText(modelo.horariofindesemanaIni + " - " + modelo.horariofindesemanaFin);

            //JORNADA CINTINUA FIN DE SEMANA
            if (modelo.datosOferente.getSinJornadaContinuaFinDeSemana() == true) {
                cierre_etre_fin_semana.setText("Cerramos entre 12 y 2 pm");
            }

        }

        cargarSharedPreferenceOferente();
        progressDialog = new ProgressDialog(this);

        gpsDatos();
        ////


        if (estaConectado()) {
            //Toast.makeText(getApplication(),"Conectado a internet", Toast.LENGTH_SHORT).show();

            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    user = firebaseAuth.getCurrentUser();
                    if (user != null) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                context);

                        // set title
                        alertDialogBuilder.setTitle("" + getString(R.string.recibido_solicitus));

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("" + getString(R.string.recibido_solicitusd_descripcion))
                                .setCancelable(false)
                                .setPositiveButton("OK,ENTENDIDO", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {


                                        modelo.uid = user.getUid();
                                        comandoGeoFire.setGeoFire();
                                        // Users is signed in
                                        // Toast.makeText(getApplication(),"...."+user+"logueado OK", Toast.LENGTH_SHORT).show();
                                        String email = user.getEmail();
                                        String dispalayName = user.getDisplayName();

                                        if (email == null) {
                                            try {
                                                email = user.getProviderData().get(0).getEmail();
                                            } catch (Exception e) {
                                                email = "";
                                            }
                                            if (email == null) {
                                                email = "";
                                            }

                                        }

                                        progressDialog.dismiss();

                                        borrarSharedPreferenceOferente();

                                        Intent inte = new Intent(getBaseContext(), TapsHome.class);
                                        startActivity(inte);

                                        finish();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    } else {
                        // Users is signed out
                        Log.d(TAG, "onAuthStateChanged:signed_out" + user + "no logueado");
                        // Toast.makeText(getApplication(),"...."+user+"no logueado", Toast.LENGTH_SHORT).show();
                    }
                }
            };
        } else {
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            showAlertSinInternet();
            return;
        }

        if (txt_direecion.getText().toString().length() > 0) {
            txt_direecion.setEnabled(true);
            imgdireecion.setBackgroundResource(R.drawable.btngeolocalizarok);
        } else {
            txt_direecion.setEnabled(true);
        }

    }

    @Override
    public void onStop() {
        super.onStop();


        if (mAuth == null || mAuthListener == null) {
            return;
        } else {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    //logueo
    public void logueo() {

        // Toast.makeText(this, "Se guarda el registro", Toast.LENGTH_LONG).show();
        progressDialog.setMessage("Validando la información, por favor espere...");
        progressDialog.show();

        mAuth.signInWithEmailAndPassword(txt_correoelectronico_cont.getText().toString(), txt_passwor_usu_cont.getText().toString()).
                addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        FirebaseAuthException ex = (FirebaseAuthException) task.getException();
                        if (ex == null) {
                            return;
                        }

                        String error = ex.getErrorCode();

                        if (error.equals("ERROR_INVALID_EMAIL")) {
                            Log.d(TAG, "signInWithEmail:onComplete:" + "ERROR CON EL CORREO");
                            Toast.makeText(getApplication(), "" + "Correo no valido", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();

                        }
                        if (error.equals("ERROR_USER_NOT_FOUND")) {
                            Log.d(TAG, "signInWithEmail:onComplete:" + "USUARIO NUEVO");
                            Toast.makeText(getApplication(), "" + "USUARIO NO REGISTRADO", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();

                        }
                        if (error.equals("ERROR_WRONG_PASSWORD")) {
                            Log.d(TAG, "signInWithEmail:onComplete:" + "CONTRASEÑA INCORRECTA");
                            Toast.makeText(getApplication(), "" + "CONTRASEÑA INCORRECTA", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();

                        }
                        if (!task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:onComplete:" + task.getException());
                            Toast.makeText(getApplication(), "" + "FALLO EN LA AUTENTICACION", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }


                    }
                });
    }


    ///registro

    public void registro() {
        mAuth.createUserWithEmailAndPassword(txt_correoelectronico_cont.getText().toString(), txt_passwor_usu_cont.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        FirebaseAuthException ex = (FirebaseAuthException) task.getException();
                        if (ex != null) {
                            Log.v("error re", "registro error" + ex.getLocalizedMessage());

                            //Toast.makeText(getApplicationContext(), "Ocurrio un error al registrarse, intente nuevamente.", Toast.LENGTH_LONG).show();
                            System.out.print("" + ex.getLocalizedMessage());

                            String error = ex.getErrorCode();

                            progressDialog.dismiss();
                            Log.v("log re", "error re" + error);
                            if (error.equals("ERROR_EMAIL_ALREADY_IN_USE")) {
                                Toast.makeText(getApplicationContext(), " Correo ya  existente.", Toast.LENGTH_LONG).show();
                            }//ERROR_WEAK_PASSWORD

                            if (error.equals("ERROR_WEAK_PASSWORD")) {
                                Toast.makeText(getApplicationContext(), " Contraseña muy debil.", Toast.LENGTH_LONG).show();
                            }


                            return;
                        }


                        if (task.isSuccessful()) {
                            //display some message here
                            //display some message here
                            //Toast.makeText(getApplicationContext(), "Registrado exitosamente", Toast.LENGTH_LONG).show();
                            createNewUser(task.getResult().getUser());


                        } else {
                            //display some message here
                            Toast.makeText(getApplicationContext(), "error de registro, intentelo de nuevo", Toast.LENGTH_LONG).show();
                        }


                    }
                });
    }


    private void createNewUser(FirebaseUser userFromRegistration) {
        String email = userFromRegistration.getEmail();
        String userId = userFromRegistration.getUid();

        ingresar2(userId);
        modelo.uid = userId;
    }


    public void ingresar2(String userUid) {

        int versionCodeAndroid = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;

        comandoRegistroOferente.registroOferente(txt_celular.getText().toString(), txt_celular_cont.getText().toString(), txt_correoelectronico_cont.getText().toString(), txt_numdocumento.getText().toString(),
                txt_nombre_completo.getText().toString(), txt_telefonofijo_cont.getText().toString(), txt_tipodocumento.getText().toString(), txt_correoelectronico.getText().toString(),
                txt_direecion.getText().toString(), textodiasfinsemana.getText().toString(), modelo.horariofindesemanaFin, modelo.horariofindesemanaIni,
                textodiassemana.getText().toString(), modelo.horariodesemanaFin, modelo.horariodesemanaIni, txt_nit.getText().toString(),
                txt_paginaweb.getText().toString(), txt_razon_social.getText().toString(), txt_telefonofijo.getText().toString(), modelo.latitud,
                modelo.longitud, "" + versionName, userUid, modelo.sinJornadaContinuaSemana, modelo.sinJornadaContinuaFinDeSemana);

    }


    //permisos v6
    public void checkPermission() {
        //ya permitida
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (estaConectado()) {
                locationStart();
            } else {
                showAlertSinInternet();
            }
        }
        //Si usted ha sido rechazado
        else {
            requestLocationPermission();
        }
    }

    // Pedir permiso
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION);

        } else {
            //Toast toast = Toast.makeText(this, "No es permitido y no se puede ejecutar la aplicación", Toast.LENGTH_SHORT);
            //toast.show();

            showAlerGpsDesactivado();
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, REQUEST_PERMISSION);

        }
    }

    private void gpsDatos() {
        locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (estaConectado()) {
                locationStart2();
            } else {
                showAlertSinInternet();
            }
        }
        //Si usted ha sido rechazado
        else {
            requestLocationPermission();
        }
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (estaConectado()) {
            locationStart2();
        } else {
            showAlertSinInternet();
        }
    }


    // tener guardado la latud y longitud al cargar la vista
    private void locationStart2() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Localizacion local = new Localizacion();
        local.setMainActivity(this);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) local);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) local);

        if (gpsEnabled) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));


            try {
                latitude = Double.parseDouble("" + location.getLatitude());
                longitud = Double.parseDouble("" + location.getLongitude());
            } catch (Exception e) {
                Log.v("exeption", "exeption");
            }

        }/*else {
            showAlerGpsDesactivado();
        }*/

    }


    // Los resultados de recepción de
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_PERMISSION) {
            //uso está permitido
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();

            } else {
                // La correspondencia, cuando todavía se niega
                //  Toast toast = Toast.makeText(this, "No se puede estar haciendo más", Toast.LENGTH_SHORT);
                // toast.show();
                showAlerGpsDesactivado();
            }
        }
    }

    /**
     * Stop using GPS listener
     * Calling this method will stop using GPS in your app
     */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        borrarSharedPreferenceOferente();
    }
}
