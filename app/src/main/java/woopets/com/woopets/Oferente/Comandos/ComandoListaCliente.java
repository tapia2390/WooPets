package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import woopets.com.woopets.Oferente.ModelOferente.Cliente;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 9/11/17.
 */

public class ComandoListaCliente {


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo  = Modelo.getInstance();

    public interface OnComandoListaClienteChangeListener {

        void cargoListaCliente();
    }

    //interface del listener de la actividad interesada
    private OnComandoListaClienteChangeListener mListener;

    public ComandoListaCliente(OnComandoListaClienteChangeListener mListener){

        this.mListener = mListener;
    }


    public void getListaCliente(final String abiertasCerrada, final String idCliente){
        DatabaseReference ref = database.getReference("clientes");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                Cliente cliente = new Cliente();

                ArrayList<Cliente> listaCliente = new ArrayList<Cliente>();
                if(abiertasCerrada.equals("abiertas")){

                    for(int i = 0; i < modelo.comprasProductosAbiertasCerradases.size(); i++){

                        cliente.setId(snap.getKey());
                        cliente.setNombre(snap.child("nombre").getValue().toString());
                        cliente.setApellido(snap.child("apellido").getValue().toString());
                        cliente.setCelular(snap.child("celular").getValue().toString());
                        cliente.setCorreo(snap.child("correo").getValue().toString());
                        cliente.setTokens(snap.child("tokenDevice").getValue().toString());
                        listaCliente.add(cliente);
                        modelo.comprasProductosAbiertasCerradases.get(i).setCliente(listaCliente);
                    }

                }else {

                    for(int i = 0; i < modelo.comprasProductosAbiertasCerradases2.size(); i++){

                        cliente.setId(snap.getKey());
                        cliente.setNombre(snap.child("nombre").getValue().toString());
                        cliente.setApellido(snap.child("apellido").getValue().toString());
                        cliente.setCelular(snap.child("celular").getValue().toString());
                        cliente.setCorreo(snap.child("correo").getValue().toString());
                        cliente.setTokens(snap.child("tokenDevice").getValue().toString());
                        listaCliente.add(cliente);
                        modelo.comprasProductosAbiertasCerradases2.get(i).setCliente(listaCliente);
                    }

                }

                mListener.cargoListaCliente();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoListaClienteChangeListener sDummyCallbacks = new OnComandoListaClienteChangeListener()
    {


        @Override
        public void cargoListaCliente()
        {}

    };
}
