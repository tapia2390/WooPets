package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

import java.util.regex.Pattern;

import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class LoguinOferente extends Activity {

    EditText email, password;
    Button iniciarsessionoferente;
    final Context context = this;

    ProgressBar progressBar4;

    //bd
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser user = mAuth.getCurrentUser();
    private ProgressDialog progressDialog;
    Modelo modelo = Modelo.getInstance();
    private static final String TAG = "AndroidBash";
    String correo1, password1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_loguin_oferente);

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        iniciarsessionoferente = (Button) findViewById(R.id.iniciarsessionoferente);
        progressBar4 = (ProgressBar) findViewById(R.id.login_progress);
        progressDialog = new ProgressDialog(this);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


        //validar verciones
        //interfaces que nos indica que ya cargo la informacion y valida las verciones


        if (estaConectado()) {
            //Toast.makeText(getApplication(),"Conectado a internet", Toast.LENGTH_SHORT).show();

            Log.v("actualizoSistema", "actualizoSistema" + "metodo");

            if (modelo.existeActualizacion(this)) {

                Log.v("actualizoSistema", "actualizoSistema" + "if");
                String ms = "Disfruta de la última versión de WooPets, por favor descárgala.";

                if (modelo.sistema.getUpdateMandatorio()) {
                    HacerDialogo(ms, false);

                    return;
                } else {
                    HacerDialogo(ms, true);
                    return;
                }

            } else {

                mAuthListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        if (user != null) {
                            modelo.uid = user.getUid();
                            // Users is signed in
                            // Toast.makeText(getApplication(),"...."+user+"logueado OK", Toast.LENGTH_SHORT).show();
                            String email = user.getEmail();
                            String dispalayName = user.getDisplayName();

                            if (email == null) {
                                try {
                                    email = user.getProviderData().get(0).getEmail();
                                } catch (Exception e) {
                                    email = "";
                                }
                                if (email == null) {
                                    email = "";
                                }

                            }

                            modelo.sinJornadaContinuaSemana = false;
                            modelo.sinJornadaContinuaFinDeSemana = false;


                            modelo.diasDeLaSemanaArticulo.clear();
                            modelo.finDeSemanaFestivosArticulo.clear();
                            modelo.horariodesemanaIniArticulo = "";
                            modelo.horariodesemanaFinArticulo = "";
                            modelo.horariofindesemanaIniArticulo = "";
                            modelo.horariofindesemanaFinArticulo = "";
                            modelo.latitudArticulo = 0;
                            modelo.longitudArticulo = 0;
                            modelo.sinJornadaContinuaSemanaArticulo = false;
                            modelo.sinJornadaContinuaFinDeSemanaArticulo = false;

                            //variables del horario al registrar un  Oferente
                            modelo.diasDeLaSemana.clear();
                            modelo.finDeSemanaFestivos.clear();

                            //variables del horario  oferente
                            modelo.horariodesemanaIni = "";
                            modelo.horariodesemanaFin = "";
                            modelo.horariofindesemanaIni = "";
                            modelo.horariofindesemanaFin = "";
                            modelo.latitud = 0.0;
                            modelo.longitud = 0.0;
                            modelo.sinJornadaContinuaSemana = false;
                            modelo.sinJornadaContinuaFinDeSemana = false;

                            progressDialog.dismiss();
                            Intent i = new Intent(getApplicationContext(), TapsHome.class);
                            startActivity(i);
                            finish();
                        } else {
                            // Users is signed out
                            Log.d(TAG, "onAuthStateChanged:signed_out" + user + "no logueado");
                            // Toast.makeText(getApplication(),"...."+user+"no logueado", Toast.LENGTH_SHORT).show();
                        }
                    }
                };
            }
        } else {
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            showAlertSinInternet();
            return;
        }


    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void loguinOferente(View v) {

        if (estaConectado()) {
            if (email.getText().toString().equals("")) {
                email.setError("Correo obligatorio");
                showAlertMsm(getString(R.string.ingresoFallido), getString(R.string.ingresoFallidoDescrip));
            } else if (!validarEmail(email.getText().toString())) {
                email.setError("Email no válido");
            } else if (password.getText().toString().equals("")) {
                password.setError("Contraseña obligatoria");
                showAlertMsm(getString(R.string.ingresoFallido), getString(R.string.ingresoFallidoDescrip));
            } else if (password.getText().length() < 3) {
                password.setError("Contraseña incorrecta");
            } else {

                Log.v("ok", "ok");
                logueo();
            }
        } else {
            showAlertSinInternet();
        }
    }

    public void olvidopaswword(View v) {
        if (email.getText().toString().equals("")) {
            email.setError("Correo obligatorio");
            showAlertMsm(getString(R.string.recuperarpasswortitle), getString(R.string.recuperarpassworddescripcion));
        } else if (!validarEmail(email.getText().toString())) {
            email.setError("Email no válido");
        } else {

            Log.v("ok", "ok");
            progressBar4.setVisibility(View.VISIBLE);
            String emailId = email.getText().toString();
            mAuth.sendPasswordResetEmail(emailId)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(LoguinOferente.this, "Las instrucciones han sido enviados a su correo electrónico", Toast.LENGTH_SHORT).show();
                                progressBar4.setVisibility(View.GONE);
                            } else {
                                Toast.makeText(LoguinOferente.this, "¡Lo siento! Inténtalo de nuevo.", Toast.LENGTH_SHORT).show();
                                progressBar4.setVisibility(View.GONE);
                            }

                        }
                    });
        }

    }

    //validacion conexion internet
    protected Boolean estaConectado() {
        if (conectadoWifi()) {
            Log.v("wifi", "Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        } else {
            if (conectadoRedMovil()) {
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            } else {
                showAlertSinInternet();
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    protected Boolean conectadoWifi() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }


    public void showAlertSinInternet() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.sinInternet));

        // set dialog message
        alertDialogBuilder
                .setMessage("" + getString(R.string.sinInternetDescripcion))
                .setCancelable(false)
                .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent inte = new Intent(getBaseContext(), LoguinOferente.class);
                        startActivity(inte);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void showAlertMsm(String titulo, String descripcion) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + titulo);

        // set dialog message
        alertDialogBuilder
                .setMessage("" + descripcion)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    //validar email
    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public void ventaMypet(View v) {
        Intent i = new Intent(getApplicationContext(), VenderEnMypet.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);


    }


    @Override
    public void onStart() {
        super.onStart();
        if (mAuth == null || mAuthListener == null) {
            return;
        } else {
            mAuth.addAuthStateListener(mAuthListener);
        }


    }

    @Override
    public void onStop() {
        super.onStop();


        if (mAuth == null || mAuthListener == null) {
            return;
        } else {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    //logueo
    public void logueo() {
        correo1 = email.getText().toString().toString();
        password1 = password.getText().toString().toString();
        // Toast.makeText(this, "Se guarda el registro", Toast.LENGTH_LONG).show();
        progressDialog.setMessage("Validando la información, por favor espere...");
        progressDialog.show();
        iniciarsessionoferente.setEnabled(false);
        mAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        FirebaseAuthException ex = (FirebaseAuthException) task.getException();
                        if (ex == null) {
                            return;
                        }

                        String error = ex.getErrorCode();

                        if (error.equals("ERROR_INVALID_EMAIL")) {
                            Log.d(TAG, "signInWithEmail:onComplete:" + "ERROR CON EL CORREO");
                            progressDialog.dismiss();
                            Toast.makeText(getApplication(), "" + "Datos no validos", Toast.LENGTH_SHORT).show();

                        }
                        if (error.equals("ERROR_USER_NOT_FOUND")) {
                            Log.d(TAG, "signInWithEmail:onComplete:" + "USUARIO NUEVO");
                            progressDialog.dismiss();
                            Toast.makeText(getApplication(), "" + "USUARIO NO REGISTRADO", Toast.LENGTH_SHORT).show();
                            iniciarsessionoferente.setEnabled(true);
                        }
                        if (error.equals("ERROR_WRONG_PASSWORD")) {
                            Log.d(TAG, "signInWithEmail:onComplete:" + "CONTRASEÑA INCORRECTA");
                            progressDialog.dismiss();
                            Toast.makeText(getApplication(), "" + "Datos no validos", Toast.LENGTH_SHORT).show();

                            iniciarsessionoferente.setEnabled(true);
                        }
                        if (!task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:onComplete:" + task.getException());
                            progressDialog.dismiss();
                            Toast.makeText(getApplication(), "" + "Datos no validos", Toast.LENGTH_SHORT).show();
                            iniciarsessionoferente.setEnabled(true);
                        }

                    }
                });
    }


    //alesrt validacion verciones
    private void HacerDialogo(String msj, final boolean cancel) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // asignar el titulo
        alertDialogBuilder.setTitle("Nueva Actualización");

        // asignar el mensaje
        alertDialogBuilder
                .setMessage(msj)
                .setCancelable(cancel)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        irAAppStore();

                    }
                });

        // se crea el dialogo
        AlertDialog alertDialog = alertDialogBuilder.create();

        // sse muestra
        alertDialog.show();
    }


    private void irAAppStore() {

        String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            finish();
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            finish();
        }
    }



    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }
}
