package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import woopets.com.woopets.Oferente.Comandos.ComandoGeoFire;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.Comandos.ComandoEditarOferente;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class EditarPerfilOferente extends Activity implements ComandoEditarOferente.OnComandoRegistroOferenteChangeListener, ComandoGeoFire.OnGeoFireChangeListener {


    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser user = mAuth.getCurrentUser();
    Modelo modelo = Modelo.getInstance();
    final Context context = this;
    BottomSheetBehavior behavior;
    private BottomSheetDialog mBottomSheetDialog;
    public LocationManager locationManager;
    double latitude, longitud;

    EditText txt_razon_social, txt_nit, txt_direecion, txt_telefonofijo, txt_celular, txt_correoelectronico, txt_paginaweb;
    EditText txt_nombre_completo, txt_numdocumento, txt_telefonofijo_cont, txt_celular_cont;
    TextView txt_horariodomicilio;
    EditText txt_tipodocumento;
    LinearLayout horariolayout, finhorariolayout;
    private static final String TAG = "AndroidBash";

    TextView textodiassemana, textofindiassemana, textodiasfinsemana, textofindiafinssemana;
    TextView cierre_etre_fin_semana, cierre_etre_semana;
    private ProgressDialog progressDialog;


    String diasDeLaSemana = "";
    String findiasDeLaSemana = "";

    ComandoEditarOferente comandoEditarOferente;
    ComandoGeoFire comandoGeoFire;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_editar_perfil_oferente);

        txt_razon_social = (EditText) findViewById(R.id.txt_razon_social);
        txt_nit = (EditText) findViewById(R.id.txt_nit);
        txt_direecion = (EditText) findViewById(R.id.txt_direecion);
        txt_telefonofijo = (EditText) findViewById(R.id.txt_telefonofijo);
        txt_celular = (EditText) findViewById(R.id.txt_celular);
        txt_correoelectronico = (EditText) findViewById(R.id.txt_correoelectronico);
        txt_paginaweb = (EditText) findViewById(R.id.txt_paginaweb);
        txt_nombre_completo = (EditText) findViewById(R.id.txt_nombre_completo);
        txt_numdocumento = (EditText) findViewById(R.id.txt_numdocumento);
        txt_telefonofijo_cont = (EditText) findViewById(R.id.txt_telefonofijo_cont);
        txt_celular_cont = (EditText) findViewById(R.id.txt_celular_cont);
        txt_tipodocumento = (EditText) findViewById(R.id.txt_tipodocumento);
        txt_horariodomicilio = (TextView) findViewById(R.id.txt_horariodomicilio);
        horariolayout = (LinearLayout) findViewById(R.id.horariolayout);
        finhorariolayout = (LinearLayout) findViewById(R.id.finhorariolayout);

        textodiassemana = (TextView) findViewById(R.id.textodiassemana);
        textofindiassemana = (TextView) findViewById(R.id.textofindiassemana);
        textodiasfinsemana = (TextView) findViewById(R.id.textodiasfinsemana);
        textofindiafinssemana = (TextView) findViewById(R.id.textofindiafinssemana);

        cierre_etre_fin_semana = (TextView) findViewById(R.id.cierre_etre_fin_semana);
        cierre_etre_semana = (TextView) findViewById(R.id.cierre_etre_semana);

        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


    }


    @Override
    protected void onStart() {


        diasDeLaSemana = "";
        findiasDeLaSemana = "";
        locationStart2();


        comandoGeoFire = new ComandoGeoFire(this);

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        progressDialog = new ProgressDialog(this);

        ////

        if (estaConectado()) {
            //Toast.makeText(getApplication(),"Conectado a internet", Toast.LENGTH_SHORT).show();
            //coamndo
            comandoEditarOferente = new ComandoEditarOferente(this);
            //comandoEditarOferente.getDatosOferente();

            cargoDatosOferenteOk();

        } else {
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            showAlertSinInternet();
            return;
        }

        super.onStart();
    }


    //atras hadware
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            modelo.actividadHorario = "";
            comfirmAlertCancelar();
        }
        return true;
    }


    private void showBottomSheetDialog() {
        final CharSequence[] items = {"Cédula de ciudadanía", "Cédula de extranjería", "NIT para personas jurídicas"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tipo de documento");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                txt_tipodocumento.setText(items[item]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public void tipodocumento(View v) {
        //Toast.makeText(getApplicationContext(), ":) ",Toast.LENGTH_SHORT).show();
        showBottomSheetDialog();
    }
    //fin sheet


    public void cancelar(View v) {
        comfirmAlertCancelar();
    }


    public void aceptar(View v) {

        if (estaConectado()) {

            if (txt_razon_social.getText().toString().equals("") || txt_nit.getText().toString().equals("") || txt_direecion.getText().toString().equals("") || txt_telefonofijo.getText().toString().equals("")
                    || txt_celular.getText().toString().equals("") || txt_correoelectronico.getText().toString().equals("") || txt_nombre_completo.getText().toString().equals("")
                    || txt_numdocumento.getText().toString().equals("") || txt_telefonofijo_cont.getText().toString().equals("") || txt_celular_cont.getText().toString().equals("")
                    || txt_tipodocumento.getText().toString().equals("")) {

                showAlertAceptar();
                txt_direecion.setEnabled(false);
                txt_direecion.setCursorVisible(false);
                txt_direecion.setFocusable(false);
                txt_direecion.setFocusableInTouchMode(false);
            }

            if (txt_direecion.getText().toString().length() > 0) {
                txt_direecion.setEnabled(false);
                txt_direecion.setCursorVisible(true);
                txt_direecion.setFocusable(true);
                txt_direecion.setFocusableInTouchMode(true);

            }
            if (modelo.diasDeLaSemana.size() <= 0 && modelo.finDeSemanaFestivos.size() <= 0) {
                showAlertAceptar();

            } else if (!validarEmail(txt_correoelectronico.getText().toString())) {
                txt_correoelectronico.setError("Email no válido");
                showAlertAceptar();
            } else if (txt_telefonofijo.getText().length() <= 6) {
                txt_telefonofijo.setError("Teléfono no valido");
                showAlertAceptar();
            } else if (txt_celular.getText().length() <= 9) {
                txt_celular.setError("Celular no valido");
                showAlertAceptar();
            } else if (txt_celular_cont.getText().length() <= 9) {
                txt_celular_cont.setError("Celular no valido");
                showAlertAceptar();
            } else if (txt_telefonofijo_cont.getText().length() <= 6) {
                txt_telefonofijo_cont.setError("Teléfono no valido");
                showAlertAceptar();
            } else {
                Log.v("ok", "ok");
                progressDialog.setMessage("Validando la información, por favor espere!...");
                progressDialog.show();
                //metodo actualizar datos Comando
                comandoEditarOferente.editarOferente(txt_celular.getText().toString(), txt_celular_cont.getText().toString(), txt_numdocumento.getText().toString(),
                        txt_nombre_completo.getText().toString(), txt_tipodocumento.getText().toString(), txt_correoelectronico.getText().toString(),
                        txt_direecion.getText().toString(), textodiasfinsemana.getText().toString(), modelo.horariofindesemanaFin, modelo.horariofindesemanaIni,
                        textodiassemana.getText().toString(), modelo.horariodesemanaFin, modelo.horariodesemanaIni, txt_nit.getText().toString(),
                        txt_paginaweb.getText().toString(), txt_razon_social.getText().toString(), modelo.latitud,
                        modelo.longitud, modelo.sinJornadaContinuaSemana, modelo.sinJornadaContinuaFinDeSemana, txt_telefonofijo_cont.getText().toString(), txt_telefonofijo.getText().toString());
            }
        } else {
            showAlertSinInternet();
        }
    }

    public void comfirmAlertCancelar() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // Setting Alert Dialog Title
        alertDialogBuilder.setTitle("" + getString(R.string.confirmar));
        // Icon Of Alert Dialog
        //alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // Setting Alert Dialog Message
        alertDialogBuilder.setMessage("" + getString(R.string.abandonarvista));
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Log.v("ok", "ok");

                comandoEditarOferente.getDatosOferente();

                modelo.actividadHorario = "";
                finish();
            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getApplicationContext(),"You clicked over No",Toast.LENGTH_SHORT).show();
            }
        });
       /* alertDialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"You clicked on Cancel",Toast.LENGTH_SHORT).show();
            }
        });*/

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void showAlertAceptar() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.informacionincompleta));

        // set dialog message
        alertDialogBuilder
                .setMessage("" + getString(R.string.informacionincompletaDes))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public void relative(View v) {
        modelo.actividadHorario = "EditPerfilOferente";
        //Toast.makeText(getApplicationContext(),"relative", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(getApplicationContext(), HorarioAtencionDomicilio.class);
        i.putExtra("horario", "horarioEditarOferente");
        startActivity(i);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
    }

    public void showAlertActualizacion() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.actualizacionexitoso));

        // set dialog message
        alertDialogBuilder
                .setMessage("Tu actualización ha sido exitosa")
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        comandoEditarOferente.getDatosOferente();

                        modelo.actividadHorario = "";
                        finish();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    //validar email
    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }


    //validacion conexion internet
    protected Boolean estaConectado() {
        if (conectadoWifi()) {
            Log.v("wifi", "Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        } else {
            if (conectadoRedMovil()) {
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            } else {
                showAlertSinInternet();
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    protected Boolean conectadoWifi() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }


    public void showAlertSinInternet() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + getString(R.string.sinInternet));

        // set dialog message
        alertDialogBuilder
                .setMessage("" + getString(R.string.sinInternetDescripcion))
                .setCancelable(false)
                .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    //GpS
    public void gps(View v) {

        //Verificamos si el GPS esta prendido o no:

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {
            if (estaConectado()) {
                locationStart();
            } else {
                showAlertSinInternet();
            }
        }

    }


    //gps inicio
    //gps+

    private void locationStart() {
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        EditarPerfilOferente.Localizacion Local = new EditarPerfilOferente.Localizacion();
        Local.setMainActivity(this);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);

        if (gpsEnabled) {
            LocationManager locationManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));

            try {

                double latitude = location.getLatitude();
                double longitud = location.getLongitude();

                modelo.latitud = latitude;
                modelo.longitud = longitud;
                modelo.gpsActivo = true;

            } catch (Exception e) {

                modelo.latitud = 0.0;
                modelo.longitud = 0.0;
                modelo.gpsActivo = true;

            }


            Intent i = new Intent(getApplicationContext(), MapsActivityOferente.class);
            i.putExtra("actividad", "EditPerfilOferente");
            startActivity(i);
            overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);


        } else {
            showAlerGpsDesactivado();
        }

    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();
                return;
            }
        }
    }

    public void setLocation(Location loc) {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);

                    //  Toast.makeText(getApplicationContext(),""+"Mi direccion es: \n"
                    //      + DirCalle.getAddressLine(0),Toast.LENGTH_SHORT).show();


                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener {
        EditarPerfilOferente mainActivity;

        public EditarPerfilOferente getMainActivity() {
            return mainActivity;
        }

        public void setMainActivity(EditarPerfilOferente mainActivity) {
            this.mainActivity = mainActivity;
        }


        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion

            //loc.getLatitude();
            //loc.getLongitude();

            String Text = "Mi ubicacion actual es: " + "\n Lat = "
                    + loc.getLatitude() + "\n Long = " + loc.getLongitude();

            //  Toast.makeText(getApplicationContext(),""+Text,Toast.LENGTH_SHORT).show();


//metodo que  conbierte latitu y longitud en direccion
            this.mainActivity.setLocation(loc);

            if (loc != null) {
                // setCurrentLocation(loc);

                modelo.latitud = loc.getLatitude();
                modelo.longitud = loc.getLongitude();
            }

        }

        private void setCurrentLocation(Location loc) {
            String Text = "Mi ubicacion actual es--: " + "\n Lat = "
                    + loc.getLatitude() + "\n Long = --" + loc.getLongitude();

            //Toast.makeText(getApplicationContext(),""+Text,Toast.LENGTH_SHORT).show();


        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado
            Toast.makeText(getApplicationContext(), "GPS Desactivado", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado

            Toast.makeText(getApplicationContext(), "GPS Activo", Toast.LENGTH_SHORT).show();


        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }


    }


    public void showAlerGpsDesactivado() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // Setting Alert Dialog Title
        alertDialogBuilder.setTitle("GPS deshabilitado");
        // Icon Of Alert Dialog
        alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // Setting Alert Dialog Message
        alertDialogBuilder.setMessage("Active la función de localización para determinar su ubicación ");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Ajustes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(settingsIntent);
            }
        });

        alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //
            }
        });
        /*alertDialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"You clicked on Cancel",Toast.LENGTH_SHORT).show();
            }
        });*/

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    //gps fin


    @Override
    public void informacionRegistroOferente() {
        progressDialog.dismiss();
        showAlertActualizacion();
    }

    @Override
    public void errorInformacionRegistroOferente() {

    }

    @Override
    public void cargoDatosOferente() {

    }

    public void cargoDatosOferenteOk() {

        //Toast.makeText(getApplicationContext(), "gps2"+modelo.datosOferente.getLatitud()+" long_"+modelo.datosOferente.getLongitud(),Toast.LENGTH_LONG).show();

        if (modelo.datosOferente.getLatitud() == 0.0 || modelo.datosOferente.getLatitud() == 0 || modelo.datosOferente.getLongitud() == 0.0 || modelo.datosOferente.getLongitud() == 0) {
            if (estaConectado()) {
                comandoGeoFire.setGeoFire();
            } else {
                Toast.makeText(getApplicationContext(), "GPS desactivado", Toast.LENGTH_SHORT).show();

                //se colocaria un alert para indicar que no esta activo el gps
                Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(settingsIntent);
            }
        }


        if (modelo.diasDeLaSemana.size() > 0) {
            horariolayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < modelo.diasDeLaSemana.size(); i++) {
                diasDeLaSemana += modelo.diasDeLaSemana.get(i) + ",";
            }

            String dias = diasDeLaSemana.substring(0, diasDeLaSemana.length() - 1);

            //dos
            textodiassemana.setText(dias);
            textofindiassemana.setText(modelo.horariodesemanaIni + " - " + modelo.horariodesemanaFin);


        }
        if (modelo.finDeSemanaFestivos.size() > 0) {

            finhorariolayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < modelo.finDeSemanaFestivos.size(); i++) {
                findiasDeLaSemana += modelo.finDeSemanaFestivos.get(i) + ",";
            }

            String diasf = findiasDeLaSemana.substring(0, findiasDeLaSemana.length() - 1);

            //dos
            textodiasfinsemana.setText(diasf);
            textofindiafinssemana.setText(modelo.horariofindesemanaIni + " - " + modelo.horariofindesemanaFin);

        }


        txt_razon_social.setText(modelo.datosOferente.getRazonSocial());
        txt_nit.setText(modelo.datosOferente.getNit());
        txt_direecion.setText(modelo.datosOferente.getDireccion());
        txt_telefonofijo.setText(modelo.datosOferente.getTelefonoFijo());
        txt_celular.setText(modelo.datosOferente.getCelular());
        txt_correoelectronico.setText(modelo.datosOferente.getCorreoElectronico());
        txt_nombre_completo.setText(modelo.datosOferente.getNombreContacto());
        txt_numdocumento.setText(modelo.datosOferente.getNumeroDeDocumentoCntacto());
        txt_telefonofijo_cont.setText(modelo.datosOferente.getTelefonoFijoContacto());
        txt_celular_cont.setText(modelo.datosOferente.getCelularContacto());
        txt_tipodocumento.setText(modelo.datosOferente.getTipodeDocumentoContacto());
        txt_paginaweb.setText(modelo.datosOferente.getPaginaWe());


        if (modelo.sinJornadaContinuaSemana == true) {
            cierre_etre_semana.setVisibility(View.VISIBLE);
            cierre_etre_semana.setText("Cerramos entre 12 y 2 pm");
        }

        if (modelo.sinJornadaContinuaSemana == false) {
            cierre_etre_semana.setVisibility(View.VISIBLE);

        }


        if (modelo.sinJornadaContinuaFinDeSemana == true) {
            cierre_etre_fin_semana.setVisibility(View.VISIBLE);
            cierre_etre_fin_semana.setText("Cerramos entre 12 y 2 pm");
        }

        if (modelo.sinJornadaContinuaFinDeSemana == false) {
            cierre_etre_fin_semana.setVisibility(View.VISIBLE);
        }

        if (!modelo.actividadHorario.equals("EditPerfilOferente")) {


            if (!modelo.datosOferente.getHorarioDiasDeLaSemana().equals("") || modelo.datosOferente.getHorarioDiasDeLaSemana().toString().length() > 0) {
                horariolayout.setVisibility(View.VISIBLE);

                String str[] = modelo.datosOferente.getHorarioDiasDeLaSemana().split(",");
                List nl = new ArrayList();
                nl = Arrays.asList(str);

                modelo.diasDeLaSemana.addAll(nl);

                textodiassemana.setText(modelo.datosOferente.getHorarioDiasDeLaSemana());
                textofindiassemana.setText(modelo.horariodesemanaIni + " - " + modelo.horariodesemanaFin);

                modelo.horariodesemanaFin = modelo.datosOferente.getHorarioInicioDiasDeLaSemana();
                modelo.horariodesemanaIni = modelo.datosOferente.getHorarioCierreDiasDeLaSemana();
                modelo.sinJornadaContinuaSemana = modelo.datosOferente.getSinJornadaContinuaSemana();

                if (modelo.datosOferente.getSinJornadaContinuaSemana() == true) {
                    cierre_etre_semana.setVisibility(View.VISIBLE);
                    cierre_etre_semana.setText("Cerramos entre 12 y 2 pm");
                }

                if (modelo.datosOferente.getSinJornadaContinuaSemana() == false) {
                    cierre_etre_semana.setVisibility(View.VISIBLE);

                }
            }

            //fin de semana
            if (!modelo.datosOferente.getHorarioDiasFinDeSemana().equals("") || modelo.datosOferente.getHorarioDiasFinDeSemana().toString().length() > 0) {
                finhorariolayout.setVisibility(View.VISIBLE);

                String str2[] = modelo.datosOferente.getHorarioDiasDeLaSemana().split(",");
                List nl2 = new ArrayList();
                nl2 = Arrays.asList(str2);

                modelo.finDeSemanaFestivos.addAll(nl2);

                //uno
                textodiasfinsemana.setText(modelo.datosOferente.getHorarioDiasFinDeSemana());
                textofindiafinssemana.setText(modelo.datosOferente.getHorarioInicioDiasFinDeSemana() + " - " + modelo.datosOferente.getHorarioCierreDiasFinDeSemana());

                modelo.horariofindesemanaFin = modelo.datosOferente.getHorarioInicioDiasFinDeSemana();
                modelo.horariofindesemanaIni = modelo.datosOferente.getHorarioCierreDiasFinDeSemana();
                modelo.sinJornadaContinuaFinDeSemana = modelo.datosOferente.getSinJornadaContinuaFinDeSemana();

                if (modelo.datosOferente.getSinJornadaContinuaFinDeSemana() == true) {
                    cierre_etre_fin_semana.setVisibility(View.VISIBLE);
                    cierre_etre_fin_semana.setText("Cerramos entre 12 y 2 pm");
                }

                if (modelo.datosOferente.getSinJornadaContinuaFinDeSemana() == false) {
                    cierre_etre_fin_semana.setVisibility(View.VISIBLE);
                }

            }

        }


    }


    // tener guardado la latud y longitud al cargar la vista
    private void locationStart2() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        EditarPerfilOferente.Localizacion local = new EditarPerfilOferente.Localizacion();
        local.setMainActivity(this);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) local);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) local);

        if (gpsEnabled) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));


            try {
                latitude = Double.parseDouble("" + location.getLatitude());
                longitud = Double.parseDouble("" + location.getLongitude());

                modelo.latitud = latitude;
                modelo.longitud = longitud;

                //Toast.makeText(getApplicationContext(), "gps"+latitude+" long_"+longitud,Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.v("exeption", "exeption");
            }

        }/*else {
            showAlerGpsDesactivado();
        }*/

    }


    @Override
    public void cargoGeoFire() {

    }

    @Override
    public void errorGeoFire() {

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

}
