package woopets.com.woopets.Oferente.Comandos;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;

/**
 * Created by tacto on 21/09/17.
 */

public class ComandoRegistroProductoOferente {
    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    public interface OnComandoRegistroProductoOferenteChangeListener {

        void setProductoOferente();
        void errorSetProductoOferente();
    }

    private OnComandoRegistroProductoOferenteChangeListener mListener;

    public ComandoRegistroProductoOferente(OnComandoRegistroProductoOferenteChangeListener mListener){

        this.mListener = mListener;
    }


    public void setRegistroProducto(final String key){
       final DatabaseReference ref = database.getReference("productos/"+key+"/");//ruta path
        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("h:mm a");
        System.out.println("Hora: "+hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: "+dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: "+hourdateFormat.format(date));


        int setHora = 0;
        String string = ""+hourFormat.format(date);
        String[] parts = string.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];

        if(part2.equals("a.") || part2.equals("a.m.") || part2.equals("a. m.")|| part2.equals("A.")){
            part2 = "AM";
        }
        if(part2.equals("p.") ||  part2.equals("p.m.") ||  part2.equals("p. m.") || part2.equals("P.")){
            part2 = "PM";
        }


        Map<String, Object> enviarRegistroProducto = new HashMap<String, Object>();

        enviarRegistroProducto.put("categoria", modelo.productosOferente.getCategoria());

        enviarRegistroProducto.put("activo", modelo.productosOferente.getActivo());
        //validamos si la subcategoria esta vacioa o no
        if (!modelo.productosOferente.getSubcategoria().equals("")) {
            enviarRegistroProducto.put("subcategoria", modelo.productosOferente.getSubcategoria());
        }
        enviarRegistroProducto.put("target", modelo.productosOferente.getTarget());
        enviarRegistroProducto.put("nombre", modelo.productosOferente.getTitulo());
        enviarRegistroProducto.put("descripcion", modelo.productosOferente.getDescripcion());
        enviarRegistroProducto.put("precio", modelo.productosOferente.getPrecio());
        enviarRegistroProducto.put("idOferente", modelo.uid);
        enviarRegistroProducto.put("destacado",false);
        enviarRegistroProducto.put("fechaCreacion",""+dateFormat.format(date)+ " "+ part1+ " "+part2);
        enviarRegistroProducto.put("systemDevice", "ANDROID" );
        enviarRegistroProducto.put("version", modelo.versionName );


        //si es un producto es false
        if(modelo.servicio_producto == false){
            enviarRegistroProducto.put("stock", modelo.productosOferente.getCantidad());
        }

        //arbol de fotos
        Collections.sort(modelo.productosOferente.getFotos());

        Map<String,Object> fotos = new HashMap<String,Object>();

        for(int i = 0; i < modelo.productosOferente.getFotos().size(); i++){
           int contador = i+1;
            fotos.put("Foto"+contador,modelo.productosOferente.getFotos().get(i)+".png");
        }

        enviarRegistroProducto.put("fotos",fotos);


        if(modelo.servicio_producto == true){

            //si es un servicio es true
            Map<String,Object> horario = new HashMap<String,Object>();
            if(modelo.diasDeLaSemana.size()>0){

                Map<String,Object> semana = new HashMap<String,Object>();
                semana.put("dias",modelo.productosOferente.getHorarioDiasDeLaSemana());
                semana.put("horaCierre",modelo.productosOferente.getHorarioCierreDiasDeLaSemana());
                semana.put("horaInicio",modelo.productosOferente.getHorarioInicioDiasDeLaSemana());
                semana.put("sinJornadaContinua", modelo.productosOferente.getSinJornadaContinua());
                horario.put("Semana",semana);
                enviarRegistroProducto.put("horario",horario);

            }

            if(modelo.finDeSemanaFestivos.size()>0){

                Map<String,Object> findesemana = new HashMap<String,Object>();
                findesemana.put("dias", modelo.productosOferente.getHorarioDiasFinDeSemana());
                findesemana.put("horaCierre",modelo.productosOferente.getHorarioCierreDiasFinDeSemana());
                findesemana.put("horaInicio",modelo.productosOferente.getHorarioInicioDiasFinDeSemana());
                findesemana.put("sinJornadaContinua",modelo.productosOferente.getFinsinJornadaContinua());
                horario.put("FinDeSemana",findesemana);
                enviarRegistroProducto.put("horario",horario);

            }


            enviarRegistroProducto.put("duracion",modelo.productosOferente.getDuracion());
            enviarRegistroProducto.put("servicioEnDomicilio",modelo.servicioEnDomicilio);
            enviarRegistroProducto.put("duracionMedida",modelo.productosOferente.getDuracionMedida());
        }

        enviarRegistroProducto.put("servicio", modelo.servicio_producto);//modelo.productosOferente.getServicio());
        enviarRegistroProducto.put("timestamp", ServerValue.TIMESTAMP);

        ref.setValue(enviarRegistroProducto, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {

                    mListener.setProductoOferente();
                } else {
                    mListener.errorSetProductoOferente();
                }
            }
        });
    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoRegistroProductoOferenteChangeListener sDummyCallbacks = new OnComandoRegistroProductoOferenteChangeListener()
    {
        @Override
        public void setProductoOferente()
        {}

        @Override
        public void errorSetProductoOferente(){}
    };
}
