package woopets.com.woopets.Oferente.tpaga.Model;

import java.util.ArrayList;

/**
 * Created by tacto on 9/08/17.
 */

public class CustomerResponse {

    String id;
    String name;
    String lastName;
    String email;
    String phone;
    String  merchantCustomerId;


    public class Error {
        public ArrayList<Error> errors;

        public String object;
        public String field;
        public String rejected_value;
        public String message;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMerchantCustomerId() {
        return merchantCustomerId;
    }

    public void setMerchantCustomerId(String merchantCustomerId) {
        this.merchantCustomerId = merchantCustomerId;
    }




}
