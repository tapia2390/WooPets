package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.FirebaseFunctionsException;
import com.google.firebase.functions.HttpsCallableResult;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import woopets.com.woopets.Modelo.cliente.Compra;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Modelo.cliente.PagoTarjetaCliente;
import woopets.com.woopets.Oferente.Comandos.ComandoCalificacionProducto;
import woopets.com.woopets.Oferente.Comandos.ComandoChatOferente;
import woopets.com.woopets.Oferente.Comandos.ComandoComfirmarReprogrmarChat;
import woopets.com.woopets.Oferente.Comandos.ComandoOrdenTarjeta;
import woopets.com.woopets.Oferente.Comandos.ComandoPreguntaPublicaciones2;
import woopets.com.woopets.Oferente.Comandos.ComandoPreguntaPublicacionesVenta;
import woopets.com.woopets.Oferente.Comandos.ComandosDetallesDeLaVenta;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.Oferente.ModelOferente.uploadFirebase.DatosTarjetaCompra;
import woopets.com.woopets.Oferente.tpaga.Model.TPPAgo;
import woopets.com.woopets.Oferente.tpaga.Model.TPPAgoResponse;
import woopets.com.woopets.Oferente.tpaga.io.MyApiAdapter;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.Comandos.CmdIp;
import woopets.com.woopets.cliente.Comandos.CmdIp.OnIpListener;
import woopets.com.woopets.cliente.Comandos.CmdPayU;
import woopets.com.woopets.cliente.Comandos.CmdPayU.OnCheckPagos;
import woopets.com.woopets.cliente.Comandos.ComadoPago;
import woopets.com.woopets.cliente.Comandos.ComadoPago.OnComadoPagoListener;
import woopets.com.woopets.cliente.Comandos.ComandoPreguntas;
import woopets.com.woopets.cliente.ModelCliente;
import woopets.com.woopets.payU.PagoPayU;

public class DetallesVenasAC extends Activity implements ComandosDetallesDeLaVenta.OnDetallesDeLaVentaChangeListener, ComandoComfirmarReprogrmarChat.OnComandoComfirmarReprogrmarChatoChangeListener, ComandoPreguntaPublicaciones2.OnComandoPreguntaPublicacionesChangeListener, ComandoPreguntaPublicacionesVenta.OnComandoPreguntaPublicacionesVentaChangeListener, ComandoCalificacionProducto.OnComandoCalificacionProductoListener, ComandoOrdenTarjeta.OnComandoOrdenTarjetaChangeListener, ComandoChatOferente.OnComandoChatListener {

    //int posicion = 0;
    Modelo modelo = Modelo.getInstance();
    private FirebaseFunctions mFunctions;


    String fecha = "";
    String hora = "";
    String foto = "";
    int total = 0;

    Button btn_nombre;
    Button btn_fecha;
    TextView nombre_producto;
    TextView txt_dir;
    TextView txt_tel;
    TextView txt_hora;
    TextView txt_des;
    TextView txt_estado;
    TextView txt_valor_unitario;
    TextView txt_cant;
    TextView txt_total;
    ImageView img_foto;
    TextView cant_preguntas_publicaiones;
    TextView txt_duracion;
    LinearLayout layout_hora;
    LinearLayout linearLayout_duracion;
    TextView txt_di_mes;
    LinearLayout layut_preguntas_sobre_preguntas;
    LinearLayout layout_pendiente;
    LinearLayout layout_cancelado;
    TextView txt_entregado;
    TextView txt_motivo;
    TextView txt_justificacion;
    TextView pendiente_calificacion;
    TextView calificacion_comentario;
    LinearLayout linearLayoutServicio;
    LinearLayout layout_calificacion;
    LinearLayout idtextorecordarpoliticasdomicilio;
    Button btn_confirmar;
    Button btn_programar;
    Button btn_chat;
    Button btn_prodcuto_servicio;
    Button btn_contador_chat;
    RatingBar ratingBar;
    TextView txt_di_mes2;
    LinearLayout fecha_de_prestacion;
    ComandosDetallesDeLaVenta comandosDetallesDeLaVenta;
    ComandoComfirmarReprogrmarChat comandoComfirmarReprogrmarChat;
    ComandoPreguntaPublicaciones2 comandoPreguntaPublicaciones2;
    CompraProductoAbiertaCerrada compra;

    ComandoPreguntaPublicacionesVenta comandoPreguntaPublicacionesVenta;
    ComandoCalificacionProducto comandoCalificacionProducto;
    ComandoChatOferente comandoChatOferente;
    String idCompra = "";
    int cantPreguntas = 0;

    Bitmap bitmap = null;

    private ProgressDialog progressDialog;
    final Context context = this;

    //pago tarjeta
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference referencia = database.getReference();
    ComandoOrdenTarjeta comandoOrdenTarjeta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detalles_venas_ac);

        mFunctions = FirebaseFunctions.getInstance();

        progressDialog = new ProgressDialog(this);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }
      /*  if (savedInstanceState != null && modelo.uid.equals("")) {
            Log.i("SIG", "DETALLE VENTA  num=" + modelo.comprasProductosCerradas.size());

            idCompra = savedInstanceState.getString("IDCOMPRA");
            modelo.uid = savedInstanceState.getString("UID");
            modelo.repintardetalles(new Modelo.OnModeloListener() {
                @Override
                public void terminoPrecarga() {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            repintar();
                        }
                    });
                }
            });
            return;
        }*/
        repintar();

    }

    @Override
    protected void onStart() {
        super.onStart();
        repintar();
    }

    void repintar() {
        if (getIntent().hasExtra("IDCOMPRA")) {
            idCompra = getIntent().getStringExtra("IDCOMPRA");
            compra = modelo.getCompraByIdCompra(idCompra);

        } else {
            return;   //Esto es para que alguna vista esta llamando esta vista y no pasa el parametro del IDCOMPRA el error sea evidente y asi detectarlo mas facil
        }

        btn_nombre = (Button) findViewById(R.id.btn_nombre);
        txt_des = (TextView) findViewById(R.id.txt_des);
        btn_fecha = (Button) findViewById(R.id.btn_fecha);
        nombre_producto = (TextView) findViewById(R.id.nombre_producto);
        txt_dir = (TextView) findViewById(R.id.txt_dir);
        txt_tel = (TextView) findViewById(R.id.txt_tel);
        txt_hora = (TextView) findViewById(R.id.txt_hora);
        txt_estado = (TextView) findViewById(R.id.txt_estado);
        txt_valor_unitario = (TextView) findViewById(R.id.txt_valor_unitario);
        txt_cant = (TextView) findViewById(R.id.txt_cant);
        txt_total = (TextView) findViewById(R.id.txt_total);
        img_foto = (ImageView) findViewById(R.id.img_foto);
        txt_duracion = (TextView) findViewById(R.id.txt_duracion);
        layout_hora = (LinearLayout) findViewById(R.id.layout_hora);
        linearLayout_duracion = (LinearLayout) findViewById(R.id.linearLayout_duracion);
        txt_di_mes = (TextView) findViewById(R.id.txt_di_mes);
        layut_preguntas_sobre_preguntas = (LinearLayout) findViewById(R.id.layut_preguntas_sobre_preguntas);
        layout_pendiente = (LinearLayout) findViewById(R.id.layout_pendiente);
        layout_cancelado = (LinearLayout) findViewById(R.id.layout_cancelado);
        txt_entregado = (TextView) findViewById(R.id.txt_entregado);
        txt_motivo = (TextView) findViewById(R.id.txt_motivo);
        txt_justificacion = (TextView) findViewById(R.id.txt_justificacion);
        linearLayoutServicio = (LinearLayout) findViewById(R.id.linearLayoutServicio);
        layout_calificacion = (LinearLayout) findViewById(R.id.layout_calificacion);
        idtextorecordarpoliticasdomicilio = (LinearLayout) findViewById(R.id.idtextorecordarpoliticasdomicilio);
        btn_confirmar = (Button) findViewById(R.id.btn_confirmar);
        btn_programar = (Button) findViewById(R.id.btn_programar);
        btn_chat = (Button) findViewById(R.id.btn_chat);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        btn_prodcuto_servicio = (Button) findViewById(R.id.btn_prodcuto_servicio);
        btn_contador_chat = (Button) findViewById(R.id.btn_contador_chat);
        fecha_de_prestacion = (LinearLayout) findViewById(R.id.fecha_de_prestacion);
        pendiente_calificacion = (TextView) findViewById(R.id.pendiente_calificacion);
        calificacion_comentario = (TextView) findViewById(R.id.calificacion_comentario);
        txt_di_mes2 = (TextView) findViewById(R.id.txt_di_mes2);

        cant_preguntas_publicaiones = (TextView) findViewById(R.id.cant_preguntas_publicaiones);


        if (compra.getPedidos().get(0).getServicio() == true) {

            idtextorecordarpoliticasdomicilio.setVisibility(View.GONE);
            String string = compra.getPedidos().get(0).getFechaDelServicio();
            String[] parts = string.split(" ");
            String fecha = parts[0]; //
            String hora = parts[1] + " " + parts[2]; //


            String string_dia = fecha;
            txt_di_mes.setText("" + string_dia);
            txt_hora.setText("" + hora);

        } else {
            txt_di_mes.setVisibility(View.GONE);
            txt_hora.setVisibility(View.GONE);
        }

        comandoOrdenTarjeta = new ComandoOrdenTarjeta(this);

        comandosDetallesDeLaVenta = new ComandosDetallesDeLaVenta(this);
        comandoComfirmarReprogrmarChat = new ComandoComfirmarReprogrmarChat(this);

        comandoPreguntaPublicaciones2 = new ComandoPreguntaPublicaciones2(this);
        comandoPreguntaPublicaciones2.getPreguntasPublicaciones(compra.getPedidos().get(0).getIdPublicacion(), compra.getCliente().get(0).getId());


        comandoChatOferente = new ComandoChatOferente(this);
        comandoChatOferente.getChatOferente(modelo.idCompara, modelo.idCliente);

        //metodo que me trae las pregunsta  de la publicacion
        modelo.classPreguntas_a_misPublicaciones.clear();
        comandoPreguntaPublicacionesVenta = new ComandoPreguntaPublicacionesVenta(this);
        comandoPreguntaPublicacionesVenta.getPreguntasPublicacionesVenta(compra.getPedidos().get(0).getIdPublicacion());


        comandoCalificacionProducto = new ComandoCalificacionProducto(this);
        comandoCalificacionProducto.getCalificacionOferente(idCompra, compra.getCliente().get(0).getId());


        modelo.idCompara = compra.getKey();
        modelo.token = compra.getCliente().get(0).getTokens();
        modelo.idCliente = compra.getCliente().get(0).getId();

        btn_nombre.setText("" + compra.getCliente().get(0).getNombre());
        btn_fecha.setText("" + fecha);
      if(compra.getPedidos().get(0).getProductosOferentes().get(0).getTitulo().equals("")){
          nombre_producto.setText("" + compra.getPedidos().get(0).getProductosOferentes().get(0).getNombre());

      }
      else{
          nombre_producto.setText("" + compra.getPedidos().get(0).getProductosOferentes().get(0).getTitulo());

      }
        txt_des.setText("" + compra.getPedidos().get(0).getProductosOferentes().get(0).getDescripcion());
        //  txt_dir.setText("" + compra.getCliente().get(0).getListDirecion().get(0).getDireccion());
        txt_tel.setText("" + compra.getCliente().get(0).getCelular());


        //Toast.makeText(getApplicationContext(),""+compra.getPedidos().get(0).getProductosOferentes().get(0).getServicioEnDomicilio(),Toast.LENGTH_LONG).show();

        if (compra.getPedidos().get(0).getServicio() == false) {
            txt_dir.setText("" + compra.getCliente().get(0).getListDirecion().get(0).getDireccion());
        } else {
            if (compra.getPedidos().get(0).getProductosOferentes().get(0).getServicioEnDomicilio() == false) {
                txt_dir.setText("Servicio en el local");
            } else {
                txt_dir.setText("" + compra.getCliente().get(0).getListDirecion().get(0).getDireccion());
            }

        }


        if (compra.getPedidos().get(0).getServicio() == false) {
            btn_prodcuto_servicio.setText("Producto entregado");
            btn_prodcuto_servicio.setVisibility(View.VISIBLE);
        } else {
            btn_prodcuto_servicio.setText("Servicio entregado");
        }

        if (compra.getPedidos().get(0).getEstado().equals("Pendiente")) {
            layout_pendiente.setVisibility(View.VISIBLE);
            pendiente_calificacion.setVisibility(View.GONE);

        }

        if (compra.getPedidos().get(0).getEstado().equals("Reprogramada")) {
            layout_pendiente.setVisibility(View.VISIBLE);
            pendiente_calificacion.setVisibility(View.GONE);

            btn_programar.setVisibility(View.GONE);
            btn_confirmar.setVisibility(View.GONE);
            btn_prodcuto_servicio.setVisibility(View.VISIBLE);
        }

        if (compra.getPedidos().get(0).getEstado().equals("Cerrada")) {

            btn_prodcuto_servicio.setVisibility(View.GONE);
            btn_confirmar.setVisibility(View.GONE);
            btn_programar.setVisibility(View.GONE);
        }


        if (compra.getPedidos().get(0).getEstado().equals("Cancelado")) {
            btn_confirmar.setVisibility(View.GONE);
            btn_programar.setVisibility(View.GONE);
            pendiente_calificacion.setVisibility(View.GONE);
            layout_cancelado.setVisibility(View.VISIBLE);
            txt_motivo.setText("Cancelado por: " + compra.getPedidos().get(0).getMotivo());
            txt_justificacion.setText("Justificación: " + compra.getPedidos().get(0).getJustificacion());

        }


        if (compra.getPedidos().get(0).getEstado().equals("Entregado")) {
            txt_entregado.setVisibility(View.VISIBLE);
            btn_confirmar.setVisibility(View.GONE);
            btn_programar.setVisibility(View.GONE);
        }


        if (compra.getPedidos().get(0).getServicio() == true) {

            if (compra.getPedidos().get(0).getEstado().equals("Pendiente") || compra.getPedidos().get(0).getEstado().equals("Confirmada") || compra.getPedidos().get(0).getEstado().equals("Reprogramada")) {
                linearLayoutServicio.setVisibility(View.VISIBLE);
                fecha_de_prestacion.setVisibility(View.VISIBLE);
                pendiente_calificacion.setVisibility(View.GONE);
                if (compra.getPedidos().get(0).getEstado().equals("Confirmada") || compra.getPedidos().get(0).getEstado().equals("Reprogramada")) {

                    btn_confirmar.setVisibility(View.GONE);
                    btn_programar.setVisibility(View.GONE);
                    btn_prodcuto_servicio.setVisibility(View.VISIBLE);
                }


            }
            if (compra.getPedidos().get(0).getEstado().equals("Cerrada")) {
                linearLayoutServicio.setVisibility(View.VISIBLE);
                btn_confirmar.setVisibility(View.GONE);
                btn_programar.setVisibility(View.GONE);
            }
            if (compra.getPedidos().get(0).getEstado().equals("Confirmada")) {
                btn_prodcuto_servicio.setVisibility(View.VISIBLE);
            }

        }


        if (compra.getPedidos().get(0).getProductosOferentes().get(0).getServicio() == true) {
            layout_hora.setVisibility(View.VISIBLE);
            linearLayout_duracion.setVisibility(View.VISIBLE);

            txt_duracion.setText("" + compra.getPedidos().get(0).getProductosOferentes().get(0).getDuracion() + " " + compra.getPedidos().get(0).getProductosOferentes().get(0).getDuracionMedida());
        }

        txt_estado.setText("" + compra.getPedidos().get(0).getEstado());
        int precio = compra.getValor() / compra.getPedidos().get(0).getCantidad();
        txt_valor_unitario.setText("" + Utility.convertToMoney(precio));
        txt_cant.setText("" + compra.getPedidos().get(0).getCantidad());

        total = compra.getValor();
        txt_total.setText("" + Utility.convertToMoney(total));


        foto = compra.getPedidos().get(0).getProductosOferentes().get(0).getFotos().get(0);


        if (compra.getPedidos().get(0).getProductosOferentes().get(0).getDestacado() == false && foto.equals("Destacdo")) {
            foto = compra.getPedidos().get(0).getProductosOferentes().get(0).getFotos().get(1);

        }

        //descargar imagenes firebase

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference().child("productos/" + compra.getPedidos().get(0).getProductosOferentes().get(0).getId() + "/" + foto);

        //get download file url
        storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Log.i("Main", "File uri: " + uri.toString());
            }
        });


        //download the file
        try {
            final File localFile = File.createTempFile("images", "jpg");
            final String finalFoto = foto;
            storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    img_foto.setImageBitmap(bitmap);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    int resID2 = getResources().getIdentifier(finalFoto, "drawable", getPackageName()); // This will return an integer value stating the id for the image you want.

                    if (resID2 == 0) {
                        // Toast.makeText(mContext, "Error al cargar la imagen", Toast.LENGTH_SHORT).show();
                        img_foto.setBackgroundResource(R.drawable.btnfotomascota2);

                    } else {
                        img_foto.setBackgroundResource(resID2);
                    }


                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Main", "IOE Exception");
        }


        //preguntas
        layut_preguntas_sobre_preguntas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //modelo.classPreguntas_a_misPublicaciones.size()
                if (cantPreguntas > 0) {

                    //  Toast.makeText(getApplicationContext(), "id: " + modelo.listatadoClintesComprasProducto.get(posicion).getPedidos().get(0).getIdPublicacion(), Toast.LENGTH_LONG).show();
                    modelo.idPreguntasClientePublicaciones = compra.getPedidos().get(0).getIdPublicacion();
                    modelo.vista = 1;

                    Intent i = new Intent(getApplicationContext(), Preguntas_A_MisPublicacionesCliente.class);
                    i.putExtra("IDCOMPRA", idCompra);
                    i.putExtra("IDCLIENTE", compra.getIdCliente());
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(), "Sin preguntas ", Toast.LENGTH_LONG).show();

                }

            }
        });

    }


    String diaSemana(int dia, int mes, int ano) {
        String letraD = "";
        String letraM = "";
        /*Calendar c = Calendar.getInstance();
        c.set(ano, mes, dia, 0, 0, 0);
        nD=c.get(Calendar.DAY_OF_WEEK);*/
        TimeZone timezone = TimeZone.getTimeZone("America/Bogota");
        Calendar calendar = new GregorianCalendar(timezone);
        calendar.set(ano, mes, dia - 3);
        int nD = calendar.get(Calendar.DAY_OF_WEEK);
        Log.i("result", "diaSemana: " + nD + " dia:" + dia + " mes:" + mes + "año:" + ano);
        switch (nD) {
            case 1:
                letraD = "Domingo";
                break;
            case 2:
                letraD = "Lunes";
                break;
            case 3:
                letraD = "Martes";
                break;
            case 4:
                letraD = "Miércoles";
                break;
            case 5:
                letraD = "Jueves";
                break;
            case 6:
                letraD = "Viernes";
                break;
            case 7:
                letraD = "Sábado";
                break;
        }


        if (mes == 1) {
            letraM = "Ene";
        } else if (mes == 2) {
            letraM = "Feb";
        } else if (mes == 3) {
            letraM = "Mar";
        } else if (mes == 4) {
            letraM = "Abr";
        } else if (mes == 5) {
            letraM = "May";
        } else if (mes == 6) {
            letraM = "Jun";
        } else if (mes == 7) {
            letraM = "Jul";
        } else if (mes == 8) {
            letraM = "Ago";
        } else if (mes == 9) {
            letraM = "Sep";
        } else if (mes == 10) {
            letraM = "Oct";
        } else if (mes == 11) {
            letraM = "Nom";
        } else if (mes == 12) {
            letraM = "Dic";
        }

        return letraD + ", " + letraM;
    }


    ///click producto servicio
    public void producto_servicio(View v) {

        //generar cobro


        if (estaConectado()) {
            if (compra.getPedidos().get(0).getServicio() == true) {
                showAlertConfirmacion();
            } else {
                Toast.makeText(getApplicationContext(), "Entregado...", Toast.LENGTH_LONG).show();
                layout_pendiente.setVisibility(View.GONE);
                compra.setAbiertaCerrada("cerradas");
                comandosDetallesDeLaVenta.setProductoServicioEntregado(compra.getKey());



            }
        } else {
            showAlertSinInternet("" + getString(R.string.sinInternet), "" + getString(R.string.sinInternetDescripcion));
        }

    }


    ///click producto servicio
    public void producto_servicio_cancelacion(View v) {

        Intent i = new Intent(getApplicationContext(), CancelacionVenta.class);
        i.putExtra("IDCOMPRA", compra.getKey());
        startActivity(i);

    }


    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();
    }

    @Override
    public void finProductoServicio() {
        comandosDetallesDeLaVenta.respuestaNotificacion(compra.getPedidos().get(0).getServicio(), compra.getIdCliente(), compra.getKey(), compra.getPedidos().get(0).getProductosOferentes().get(0).getNombre(), compra.getCliente().get(0).getTokens());
        comandosDetallesDeLaVenta.actualizarEstadoVenta(compra.getKey());
        Toast.makeText(getApplicationContext(), "Entregado...", Toast.LENGTH_LONG).show();
        layout_pendiente.setVisibility(View.GONE);
        compra.getPedidos().get(0).setEstado("Cerrada");
        btn_prodcuto_servicio.setVisibility(View.GONE);
        txt_estado.setText("" + compra.getPedidos().get(0).getEstado());
        btn_confirmar.setVisibility(View.GONE);
        btn_programar.setVisibility(View.GONE);
        pendiente_calificacion.setVisibility(View.VISIBLE);
        progressDialog.dismiss();
        if (compra.getPedidos().get(0).getServicio() == true) {
            showAlertpagoExitoso();
        }


    }


    public void producto_confirmado(View v) {

        comandoComfirmarReprogrmarChat.confimar(compra.getKey());

    }

    public void producto_programar(View v) {
        Intent i = new Intent(getApplicationContext(), ProgramarServicio.class);
        i.putExtra("IDCOMPRA", idCompra);
        startActivity(i);

    }

    public void producto_chat(View v) {
        Intent i = new Intent(getApplicationContext(), OferenteChat.class);
        i.putExtra("IDCOMPRA", idCompra);
        startActivity(i);
    }

    @Override
    public void cmConfirmar() {
        compra.getPedidos().get(0).setEstado("Confirmada");
        btn_confirmar.setVisibility(View.GONE);
        btn_programar.setVisibility(View.GONE);
        btn_prodcuto_servicio.setVisibility(View.VISIBLE);
        comandoComfirmarReprogrmarChat.confirmadoNotification(compra.getPedidos().get(0).getProductosOferentes().get(0).getNombre());
    }

    @Override
    public void cmReprogramar() {

    }

    @Override
    public void cmChat() {

    }

    @Override
    public void preguntasMisPublicaciones2() {

        modelo.idPreguntasClientePublicacionesPosicion = 0;
        modelo.classPreguntas_a_misPublicacionesCliente.size();
        layut_preguntas_sobre_preguntas.setClickable(true);

        if (modelo.classPreguntas_a_misPublicacionesCliente.size() > 0) {
            layut_preguntas_sobre_preguntas.setClickable(true);
        }


    }

    @Override
    public void preguntasMisPublicacionesVEnta() {
        // int cantPreguntas = modelo.classPreguntas_a_misPublicaciones.size();
        // Toast.makeText(getApplicationContext(), "" + cantPreguntas, Toast.LENGTH_LONG).show();

        modelo.idPreguntasClientePublicaciones= compra.getPedidos().get(0).getProductosOferentes().get(0).getId();
        modelo.adicionarListaPreguntas(modelo.idPreguntasClientePublicaciones, modelo.filtro);
        modelo.filtro  = 0;
        cantPreguntas = 0;
        for (int i = 0; i < modelo.preguntas_publicaiones_detalle.size(); i++) {

            if (modelo.preguntas_publicaiones_detalle.get(i).getIdCliente().equals(compra.getIdCliente()) ) {
                cantPreguntas++;
            }
        }


        if (cantPreguntas == 0) {
            cant_preguntas_publicaiones.setText("Sin preguntas");
        }

        if (cantPreguntas == 1) {
            cant_preguntas_publicaiones.setText("" + cantPreguntas + " " + "pregunta");
        }

        if (cantPreguntas > 1) {
            cant_preguntas_publicaiones.setText("" + cantPreguntas + " " + "preguntas");
        }
    }

    @Override
    public void getCalificaciones() {
        if (modelo.calificaiones.size() > 0) {
            calificacion_comentario.setText(modelo.calificaiones.get(0).getComentario());
            calificacion_comentario.setVisibility(View.VISIBLE);
            layout_calificacion.setVisibility(View.VISIBLE);
            pendiente_calificacion.setVisibility(View.GONE);
            ratingBar.setRating(Float.parseFloat("" + modelo.calificaiones.get(0).getCalificacion()));

            String string = modelo.calificaiones.get(0).getFecha();

            if(string != null || !string.equals("")){
                txt_di_mes2.setText("" + modelo.calificaiones.get(0).getFecha());

            }else{
                txt_di_mes2.setText("Sin comentarios.");

            }

        }
    }



    public Map getMapaToPagoToken(Compra compra, DatosTarjetaCompra mini ){

        Map<String, Object> dirCobro = new HashMap<>();
        dirCobro.put("street1", mini.direccion);
        dirCobro.put("city", mini.ciudad);
        dirCobro.put("state", mini.departamento);
        dirCobro.put("phone", mini.telefono);

        Map<String, Object> tarjeta = new HashMap<>();
        tarjeta.put("creditCardTokenId", mini.getTokenTarjeta());
        tarjeta.put("INSTALLMENTS_NUMBER", mini.getCuotasTarjeta());
        tarjeta.put("paymentMethod", mini.franquicia);
        tarjeta.put("ipAddress", "0.0.0.0");     //Queda pendiente

        Map<String, Object> producto = new HashMap<>();
        producto.put("valor", compra.valor);


        Map<String, Object> data = new HashMap<>();
        data.put("referenceCode", compra.id);
        data.put("fullName", mini.nombre );
        data.put("emailAddress", mini.correo );
        data.put("contactPhone", mini.telefono );
        data.put("dniNumber", mini.cedula );         //cedula
        data.put("dirCobro", dirCobro );
        data.put("tarjeta", tarjeta );
        data.put("producto", producto );

        return  data;


    }


    // generacion del pago con tarjeta

    public void generarCobroPayU() {

        progressDialog.setMessage("Validando la información por favor espere...");
        progressDialog.show();

        String token = compra.getTarjeta().get(0).getTokenTarjeta();
        int cuotas   = compra.getTarjeta().get(0).getCuotasTarjeta();


         final Compra compraC = new Compra();
         compraC.id =  compra.getKey();
         compraC.valor = compra.getValor();

         final Map mapa = getMapaToPagoToken(compraC, compra.getTarjeta().get(0));



        CmdIp.getIp(this, new OnIpListener() {
            @Override
            public void gotIp(String ip) {

                //mini.referenceCode = ComandoPreguntas.getLlaveFirebase();
                //mini.ip = ip;
                mapa.put("ipAddress",ip);
                hacerPagoFinalConTarjeta(compraC, mapa, compra.getTarjeta().get(0));

            }

            @Override
            public void fallo() {

                showAlertSinInternet("Fallo en la transacción", "No pudo iniciar la transacción. Intente más tarde");

            }
        });


    }




    private Task<Map> hacerPagoFinalConTarjeta(final Compra compra, Map data, final DatosTarjetaCompra mini) {     //Usamos el token para hacer el pago.


        return mFunctions
                .getHttpsCallable("pagoServicioConTokenAndroid")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {

                        CmdPayU.checkPagos(compra.id, new OnCheckPagos() {
                            @Override
                            public void pagoAprobado(PagoPayU pagoPayU) {
                                progressDialog.dismiss();

                                PagoTarjetaCliente pago = new PagoTarjetaCliente();
                                pago.Descripcion = "Compra Servicio";
                                pago.authorizationCode = pagoPayU.authorizationCode;
                                pago.idCompra = compra.id;
                                pago.idTarjeta = mini.getIdTarjeta();
                                pago.metodoPago = "Tarjeta";
                                pago.idPago = pagoPayU.orderId;
                                pago.paymentTransaction = pagoPayU.transactionId;
                                registrarPago(compra, pago);

                            }

                            @Override
                            public void pagoReachazado(String razon) {
                                showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago, solicita a tu cliente otro medio de pago: " + razon);
                                progressDialog.dismiss();


                            }

                            @Override
                            public void pagoPendiente(PagoPayU pagoPayU) {
                                //adicionarEnInventario(compra);
                                showAlertSinInternet("Transaccion pendiente",  "La transacción esta siendo evaluada por personal de PauY, esto puede tardar entre una a cuatro horas");
                                progressDialog.dismiss();


                            }

                            @Override
                            public void pagoConError(String razon) {
                                showAlertSinInternet("Fallo en la transacción", "Fallo en la sistema de pagos, intenta más tarde: " + razon);
                                progressDialog.dismiss();
                            }
                        });
                        Map result = (Map) task.getResult().getData();
                        return result;
                    }
                });
    }






    private Task<Map> hacerPagoFinalConTarjetaViejo(final Compra compra, Map data, final DatosTarjetaCompra mini) {     //Usamos el token para hacer el pago.


        return mFunctions
                .getHttpsCallable("pagoServicioConToken")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, Map>() {
                    @Override
                    public Map then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        Map result = (Map) task.getResult().getData();
                        return result;
                    }
                }).addOnCompleteListener(new OnCompleteListener<Map>() {
                                             @Override
                                             public void onComplete(@NonNull Task<Map> task) {
                                                 if (!task.isSuccessful()) {
                                                     Exception e = task.getException();
                                                     if (e instanceof FirebaseFunctionsException) {
                                                         FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                         FirebaseFunctionsException.Code code = ffe.getCode();
                                                         Object details = ffe.getDetails();
                                                     }

                                                     Log.w("LOG", "addMessage:onFailure", e);

                                                     progressDialog.dismiss();
                                                     showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago, intenta más tarde. Código de error: P2");
                                                     return;

                                                 }


                                                 try {

                                                     Map result = (Map) task.getResult().get("transactionResponse");

                                                     String idOrdenPayU = result.get("orderId").toString();
                                                     String transactionId = result.get("transactionId").toString();
                                                     String authorizationCode = result.get("authorizationCode").toString();
                                                     String responseCode = result.get("responseCode").toString();

                                                     String status = result.get("state").toString();


                                                     if (status.equals("DECLINED")) {
                                                         showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago. Código de error: " + responseCode);
                                                         progressDialog.dismiss();
                                                         return;

                                                     }

                                                     Toast.makeText(getApplicationContext(), "Pago " + result.get("state").toString(), Toast.LENGTH_LONG).show();

                                                     progressDialog.dismiss();


                                                     showalertEnviar2(authorizationCode, idOrdenPayU, transactionId, mini.getIdTarjeta());


                                                 }
                                                 catch (Exception e){
                                                     progressDialog.dismiss();
                                                     showAlertSinInternet("Fallo en la transacción", "No se pudo completar el pago, intenta más tarde. Código de error: P1");
                                                     return;

                                                 }

                                             }
                                         }
                );
    }




    protected void registrarPago(final Compra comp, PagoTarjetaCliente pago){

        new ComadoPago(new OnComadoPagoListener() {
            @Override
            public void didPagoExitoso() {


            }

            @Override
            public void didPagoFallido() {

            }

            @Override
            public void didRegistroExitoso() {
                //Toast.makeText(getApplicationContext(), "Entregado...", Toast.LENGTH_LONG).show();
                layout_pendiente.setVisibility(View.GONE);
                compra.setAbiertaCerrada("cerradas");
                comandosDetallesDeLaVenta.setProductoServicioEntregado(compra.getKey());
            }

            @Override
            public void didRegistroFallido() {
                progressDialog.dismiss();
                showAlertSinInternet("Problema con la trasacción", "Se descontó el dinero de la tarjeta de crédito, pero no se pudo registrar el pedido. Comuniquese con WooPets");

            }
        }).registrarPago(comp.id, pago);

    }



    public void showalertEnviar2(final String authorizationCode, final String idPago, final String paymentTransaction, final String idTarjeta) {


        if (estaConectado()) {

            validacion_pago(authorizationCode, idPago, paymentTransaction, idTarjeta);


        } else {
            progressDialog.dismiss();
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            String titulo = "Sin Internet";
            String mensaje = "Sin Conexión a Internet";

            showAlertSinInternet(titulo, mensaje);
            return;
        }

    }



    public void validacion_pago(final String authorizationCode, final String idPago, final String paymentTransaction, final String idTarjeta) {

        if (estaConectado()) {

            comandoOrdenTarjeta.setCoboro(authorizationCode, idPago, idTarjeta, paymentTransaction, idCompra);

        } else {
            // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
            String titulo = "Sin Internet";
            String mensaje = "Sin Conexión a Internet";
            showAlertSinInternet(titulo, mensaje);
            return;
        }
    }



    @Override
    public void destacadoEnviadoTarjeta() {
        //Toast.makeText(getApplicationContext(), "Entregado...", Toast.LENGTH_LONG).show();
        layout_pendiente.setVisibility(View.GONE);
        compra.setAbiertaCerrada("cerradas");
        comandosDetallesDeLaVenta.setProductoServicioEntregado(compra.getKey());

    }

    @Override
    public void errorEnviadoTarjeta() {
        Toast.makeText(getApplicationContext(), "Error al generar el pago, intente de nuevo", Toast.LENGTH_LONG).show();
    }

    //conexion internet
    //validacion conexion internet
    protected Boolean estaConectado() {
        if (conectadoWifi()) {
            Log.v("wifi", "Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        } else {
            if (conectadoRedMovil()) {
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            } else {
                String titulo = "Sin Internet";
                String mensaje = "Sin Conexión a Internet";
                showAlertSinInternet(titulo, mensaje);
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }


    protected Boolean conectadoWifi() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }


    public void showAlertSinInternet(String titulo, String mensaje) {

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("" + titulo);

        // set dialog message
        alertDialogBuilder
                .setMessage("" + mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    // chat
    @Override
    public void getChasts() {

        if (modelo.chatOferentes.size() > 0) {
            int contador = 0;
            for (int i = 0; i < modelo.chatOferentes.size(); i++) {
                if (modelo.chatOferentes.get(i).getVisto() == false && !modelo.chatOferentes.get(i).getEmisor().equals(modelo.uid)) {
                    contador++;
                }
            }

            if (contador > 0) {
                btn_contador_chat.setVisibility(View.VISIBLE);
                btn_contador_chat.setText("" + contador);
            } else {
                btn_contador_chat.setVisibility(View.GONE);
            }
        } else {
            btn_contador_chat.setVisibility(View.GONE);
        }
    }

    @Override
    public void setChasts() {

    }


    public void showAlertpagoExitoso() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // Setting Alert Dialog Title
        alertDialogBuilder.setTitle("¡Pago exitoso!");
        // Icon Of Alert Dialog
        //alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // Setting Alert Dialog Message
        alertDialogBuilder.setMessage("El pago ha sido efectivo. Se ha cobrado el costo de la venta");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Log.v("ok", "ok");


            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void showAlertConfirmacion() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // Setting Alert Dialog Title
        alertDialogBuilder.setTitle("Confirma la información");
        // Icon Of Alert Dialog
        //alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // Setting Alert Dialog Message
        alertDialogBuilder.setMessage("A punto de cargar a la tarjeta de tu cliente una compra por valor de " + txt_total.getText().toString() + ". ¿Estas seguro?");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Log.v("ok", "ok");

                generarCobroPayU();
            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getApplicationContext(),"You clicked over No",Toast.LENGTH_SHORT).show();
            }
        });
       /* alertDialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"You clicked on Cancel",Toast.LENGTH_SHORT).show();
            }
        });*/

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void onSaveInstanceState(Bundle outState) {
        outState.putString("IDCOMPRA", idCompra);
        outState.putString("UID", modelo.uid);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }


    /*public String getIp(){

        String ipWifi = getWifiIPAddress();

        if (!ipWifi.equals("0.0.0.0") && !ipWifi.equals("")){
            return  ipWifi;
        }

        return getMobileIPAddress();

    }*/

    public String getWifiIPAddress() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        return  Formatter.formatIpAddress(ip);
    }


    public static String getMobileIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return  addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            return "0.0.0.0";
        }
        return "0.0.0.0";
    }




}

