package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import woopets.com.woopets.Oferente.AdapterOferente.MisPublicacioneAdapterInactivas;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.AdapterOferente.MisPublicacioneAdapter;
import woopets.com.woopets.Oferente.Comandos.ComandoRegistroProducto;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class MisPublicacionesOferente extends Activity implements MisPublicacioneAdapter.AdapterCallback,  MisPublicacioneAdapterInactivas.AdapterCallback, ComandoRegistroProducto.OnComandoRegistroProductChangeListener{


    LinearLayout layout_publicaciones,layout_list_publicaciones_activos,layout_list_publicaciones_inactivos;
    ListView lista_mis_publicaciones;
    ListView lista_mis_publicaciones1;
    TextView txt_activa_inactiva;
    private MisPublicacioneAdapter mAdapter;
    private MisPublicacioneAdapterInactivas mAdapterInactivas;
    Modelo  modelo  = Modelo.getInstance();

    ComandoRegistroProducto comandoRegistroProducto;
    boolean activo = true;
    boolean inactivo = false;

    Button button7;
    Button button8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mis_publicaciones_oferente);

        layout_publicaciones = (LinearLayout)findViewById(R.id.layout_publicaciones);
        layout_list_publicaciones_activos = (LinearLayout)findViewById(R.id.layout_list_publicaciones_activos);
        layout_list_publicaciones_inactivos = (LinearLayout)findViewById(R.id.layout_list_publicaciones_inactivos);
        lista_mis_publicaciones = (ListView) findViewById(R.id.lista_mis_publicaciones);
        lista_mis_publicaciones1 = (ListView) findViewById(R.id.lista_mis_publicaciones1);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);

        txt_activa_inactiva = (TextView) findViewById(R.id.txt_activa_inactiva);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

    }




    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), TapsHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        super.onBackPressed();

    }

    private void displayListaActivas(){
        modelo.activa_inactiva = true;
        lista_mis_publicaciones1.setVisibility(View.GONE);
         activo = true;
         inactivo = false;

         if(modelo.productosOferentes.size() > 0){
             mAdapter = new MisPublicacioneAdapter(this,this);
             lista_mis_publicaciones.setAdapter(mAdapter);
             Log.v("rol","rol"+" "+modelo.productosOferentes.size());

         }


    }

    private void displayListaInactivas(){

        if(modelo.productosOferentes2.size() > 0){
            modelo.activa_inactiva = false;
            lista_mis_publicaciones1.setVisibility(View.VISIBLE);
            lista_mis_publicaciones.setVisibility(View.GONE);
            activo = false;
            inactivo = true;
            mAdapterInactivas = new MisPublicacioneAdapterInactivas(this,this);
            lista_mis_publicaciones1.setAdapter(mAdapterInactivas);
            Log.v("rol","rol"+" "+modelo.productosOferentes2.size());

        }

    }


    public void activas(View v){
        button7.setBackgroundResource(R.drawable.btnactivoazul);
        button8.setBackgroundResource(R.drawable.btninactivogris);
        modelo.activa_inactiva = true;
        lista_mis_publicaciones1.setVisibility(View.GONE);
        lista_mis_publicaciones.setVisibility(View.VISIBLE);
       // Toast.makeText(getApplicationContext(), "activas", Toast.LENGTH_SHORT).show();
        activo = true;
        inactivo = false;
        comandoRegistroProducto.getProducto(true);
        txt_activa_inactiva.setText("No tienes publicaciones activas");


    }

    public void inactivas(View v){
        button7.setBackgroundResource(R.drawable.btnactivogris);
        button8.setBackgroundResource(R.drawable.btninactivoazul);
        modelo.activa_inactiva = false;
        lista_mis_publicaciones.setVisibility(View.GONE);
        lista_mis_publicaciones1.setVisibility(View.VISIBLE);
        //Toast.makeText(getApplicationContext(), "inactivas", Toast.LENGTH_SHORT).show();
        activo = false;
        inactivo = true;
        comandoRegistroProducto.getProducto(false);
        txt_activa_inactiva.setText("No tienes publicaciones inactivas");

    }

    @Override
    public void registroProductoOferente() {

    }

    @Override
    public void getProductoOferente() {


        if (activo == true ){
            if(modelo.productosOferentes.size() > 0){
                layout_list_publicaciones_activos.setVisibility(View.VISIBLE);
                layout_publicaciones.setVisibility(View.GONE);
                displayListaActivas();
            }else{
                layout_list_publicaciones_activos.setVisibility(View.GONE);
                layout_publicaciones.setVisibility(View.VISIBLE);

            }
        }
        else if(inactivo == true){
            if(modelo.productosOferentes2.size() > 0){
                layout_list_publicaciones_inactivos.setVisibility(View.VISIBLE);
                layout_list_publicaciones_activos.setVisibility(View.GONE);
                layout_publicaciones.setVisibility(View.GONE);
                displayListaInactivas();
            }else{
                layout_list_publicaciones_inactivos.setVisibility(View.GONE);
                layout_publicaciones.setVisibility(View.VISIBLE);

            }

        }else{
            if(modelo.productosOferentes.size() > 0){
                layout_list_publicaciones_activos.setVisibility(View.VISIBLE);
                layout_list_publicaciones_inactivos.setVisibility(View.GONE);
                layout_publicaciones.setVisibility(View.GONE);
                displayListaActivas();
            }else{
                layout_list_publicaciones_activos.setVisibility(View.GONE);
                layout_list_publicaciones_inactivos.setVisibility(View.GONE);
                layout_publicaciones.setVisibility(View.VISIBLE);

            }
        }

    }

    public void crear_publicacion(View v){
        modelo.vista =1;
        Intent i = new Intent(getApplicationContext(), MyPublicacionesCategorias.class);
        startActivity(i);
    }




    @Override
    protected void onStart() {
        super.onStart();
        comandoRegistroProducto = new ComandoRegistroProducto(this);
        comandoRegistroProducto.getProducto(true);
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }
}
