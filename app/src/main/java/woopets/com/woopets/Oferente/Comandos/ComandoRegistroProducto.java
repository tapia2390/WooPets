package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import woopets.com.woopets.Oferente.ModelOferente.Cliente;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;

/**
 * Created by tacto on 23/08/17.
 */

public class ComandoRegistroProducto {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo = Modelo.getInstance();

    public interface OnComandoRegistroProductChangeListener {

        void registroProductoOferente();

        void getProductoOferente();
    }

    //interface del listener de la actividad interesada
    private OnComandoRegistroProductChangeListener mListener;

    public ComandoRegistroProducto(OnComandoRegistroProductChangeListener mListener) {

        this.mListener = mListener;
    }


    public void getProducto(final boolean activo2) {

        modelo.productosOferentes.clear();
        modelo.productosOferentes2.clear();
        modelo.productosOferentesInactivas.clear();

        DatabaseReference ref = database.getReference("productos/");//ruta path
        Query query = ref.orderByChild("idOferente").equalTo(modelo.uid);
        ChildEventListener listener = new ChildEventListener() {


            @Override
            public void onChildAdded(DataSnapshot snap, String s) {


                boolean data2 = snap.exists();

                if (data2 == false) {
                    mListener.getProductoOferente();
                } else {
                    ProductosOferente productosOferente = new ProductosOferente();


                        productosOferente.setId(snap.getKey());

                        boolean activo = (boolean) snap.child("activo").getValue();
                        productosOferente.setActivo(activo);
                        productosOferente.setCategoria(snap.child("categoria").getValue().toString());
                        productosOferente.setDescripcion(snap.child("descripcion").getValue().toString());

                        if (snap.child("destacado").getValue() != null) {
                            boolean destacado = (boolean) snap.child("destacado").getValue();
                            productosOferente.setDestacado(destacado);
                        }

                        //nuevo arbol
                        DataSnapshot snapFotos;
                        snapFotos = (DataSnapshot) snap.child("fotos/");
                        ArrayList<String> fotosProductosOferentes = new ArrayList<String>();
                        for (DataSnapshot foto : snapFotos.getChildren()) {
                            fotosProductosOferentes.add(foto.getValue().toString());
                        }
                        productosOferente.setFotos(fotosProductosOferentes);
                        productosOferente.setIdOferente(snap.child("idOferente").getValue().toString());
                        productosOferente.setNombre(snap.child("nombre").getValue().toString());
                        productosOferente.setPrecio(snap.child("precio").getValue().toString());
                        boolean servicio = (boolean) snap.child("servicio").getValue();
                        productosOferente.setServicio(servicio);
                        productosOferente.setTarget(snap.child("target").getValue().toString());

                        if (activo == true) {
                            modelo.productosOferentes.add(productosOferente);
                        }
                        if (activo == false) {
                            modelo.productosOferentes2.add(productosOferente);
                        }


                    mListener.getProductoOferente();
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        query.addChildEventListener(listener);
        modelo.hijosListener.put(query, listener);
    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoRegistroProductChangeListener sDummyCallbacks = new OnComandoRegistroProductChangeListener() {
        @Override
        public void registroProductoOferente() {
        }

        @Override
        public void getProductoOferente() {
        }

    };
}
