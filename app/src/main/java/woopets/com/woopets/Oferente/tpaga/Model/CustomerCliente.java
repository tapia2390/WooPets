package woopets.com.woopets.Oferente.tpaga.Model;

/**
 * Created by tacto on 8/08/17.
 */

public class CustomerCliente {

    String id;
    String firstName;
    String lastName;
    String email;
    String phone;
    String  merchantCustomerId;

    public  CustomerCliente(){

    }

    public static CustomerCliente create(String firstName, String lastName, String email, String phone, String merchantCustomerId) {
        CustomerCliente customerCliente = new CustomerCliente();
        customerCliente.firstName = firstName;
        customerCliente.lastName = lastName;
        customerCliente.email = email;
        customerCliente.phone = phone;
        customerCliente.merchantCustomerId = merchantCustomerId;
        return customerCliente;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMerchantCustomerId() {
        return merchantCustomerId;
    }

    public void setMerchantCustomerId(String merchantCustomerId) {
        this.merchantCustomerId = merchantCustomerId;
    }



}
