package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class VenderEnMypet extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_vender_en_mypet);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void continuar(View v){
        comfirmAlert();
    }

    public void comfirmAlert(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // Setting Alert Dialog Title
        alertDialogBuilder.setTitle(""+getString(R.string.alertRecuerda));
        // Icon Of Alert Dialog
        //alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // Setting Alert Dialog Message
        alertDialogBuilder.setMessage(""+getString(R.string.alertRecuerdaDes));
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Si, Continuar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Log.v("ok","ok");
                Intent i = new Intent(getApplicationContext(),RegistroOferente.class);
                startActivity(i);
                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                //finish();
            }
        });

        alertDialogBuilder.setNegativeButton("No, Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getApplicationContext(),"You clicked over No",Toast.LENGTH_SHORT).show();
            }
        });
       /* alertDialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"You clicked on Cancel",Toast.LENGTH_SHORT).show();
            }
        });*/

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void terminos_conciones(View v){
        Intent i = new Intent(getApplicationContext(),AvisoDePrivacidad.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        //finish();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

}
