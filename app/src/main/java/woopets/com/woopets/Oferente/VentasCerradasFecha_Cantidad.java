package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;

import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

import woopets.com.woopets.Oferente.AdapterOferente.MisVentasAbiertasCerradasPorProductoAdapter;
import woopets.com.woopets.Oferente.Comandos.ComandoClienteCompra;
import woopets.com.woopets.Oferente.Comandos.ComandoListaCliente;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class VentasCerradasFecha_Cantidad extends Activity implements MisVentasAbiertasCerradasPorProductoAdapter.AdapterCallback {

    ListView lista_ventas_cerradas;

    ComandoListaCliente comandoListaCliente;
    Modelo modelo = Modelo.getInstance();
    MisVentasAbiertasCerradasPorProductoAdapter mAdapter2;
    TextView title_producto;
    TextView total_ventas;
    ImageView img_venta;
    Button btn2;
    Bitmap bitmap = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ventas_cerradas_fecha__cantidad);

        lista_ventas_cerradas = (ListView) findViewById(R.id.lista_ventas_cerradas);


        //Toast.makeText(getApplicationContext(),"-"+uidProducto+"-"+posicionServicio+"-"+abiertascerrada+"-"+idCliente,Toast.LENGTH_LONG).show();
        btn2 = (Button) findViewById(R.id.btn2);
        title_producto = (TextView) findViewById(R.id.title_producto);
        total_ventas = (TextView) findViewById(R.id.total_ventas);
        img_venta = (ImageView) findViewById(R.id.img_venta);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

    }

    @Override
    protected void onStart() {

        lista_ventas_cerradas.setAdapter(null);

        modelo.comprasProductos.get(modelo.posicionServicio).getPedidos().size();

        title_producto.setText("" + modelo.comprasProductos.get(modelo.posicionServicio).getPedidos().get(0).getProductosOferentes().get(0).getTitulo());

        int i = 0;
        int acumulador = 0;
        int acumulador2 = 0;
        while (i < modelo.comprasProductosAbiertasCerradases2.size()) {
            if (modelo.comprasProductosAbiertasCerradases2.get(i).getPedidos().get(0).getIdPublicacion().equals(modelo.comprasProductos.get(modelo.posicionServicio).getPedidos().get(0).getIdPublicacion()) && modelo.comprasProductosAbiertasCerradases2.get(i).getAbiertaCerrada().equals(modelo.abiertas_cerrada)) {
                acumulador = acumulador + modelo.comprasProductosAbiertasCerradases2.get(i).getPedidos().get(0).getCantidad();
                acumulador2  ++;
            }
            i++;
        }

        total_ventas.setText("Total ventas: " + acumulador2);
        modelo.adicionarListaClintesSegunLaCompra(modelo.uidProducto);


        String foto = modelo.comprasProductos.get(modelo.posicionServicio).getPedidos().get(0).getProductosOferentes().get(0).getFotos().get(0);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference().child("productos/" + modelo.comprasProductos.get(modelo.posicionServicio).getPedidos().get(0).getProductosOferentes().get(0).getId() + "/" + foto);

        //get download file url
        storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Log.i("Main", "File uri: " + uri.toString());
            }
        });


        //download the file
        try {
            final File localFile = File.createTempFile("images", "jpg");
            final String finalFoto = foto;
            storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    img_venta.setImageBitmap(bitmap);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    int resID2 = getResources().getIdentifier(finalFoto, "drawable", getPackageName()); // This will return an integer value stating the id for the image you want.

                    if (resID2 == 0) {
                        // Toast.makeText(mContext, "Error al cargar la imagen", Toast.LENGTH_SHORT).show();
                        img_venta.setBackgroundResource(R.drawable.btnfotomascota2);

                    } else {
                        img_venta.setBackgroundResource(resID2);
                    }


                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Main", "IOE Exception");
        }
        //fin descargar imagen

        btn2.setText("Ventas " + modelo.abiertas_cerrada);
        displaylista();
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void displaylista() {

        mAdapter2 = new MisVentasAbiertasCerradasPorProductoAdapter(this, this);
        lista_ventas_cerradas.setAdapter(mAdapter2);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }


}
