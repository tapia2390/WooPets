package woopets.com.woopets.Oferente.tpaga.Model;

/**
 * Created by tacto on 10/08/17.
 */

public class TPPAgoResponse {
    String id;
    String errorMessage;
    String orderId;
    String paymentTransaction;
    String idTarjeta;
    String idPago;
    String metodoPago;//Tarjeta
    String authorizationCode;//pendiente

    TransactionInfo transactionInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getErrorMessage() {

        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentTransaction() {
        return paymentTransaction;
    }

    public void setPaymentTransaction(String paymentTransaction) {
        this.paymentTransaction = paymentTransaction;
    }

    public TransactionInfo getTransactionInfo() {
        return transactionInfo;
    }

    public void setTransactionInfo(TransactionInfo transactionInfo) {
        this.transactionInfo = transactionInfo;
    }
}
