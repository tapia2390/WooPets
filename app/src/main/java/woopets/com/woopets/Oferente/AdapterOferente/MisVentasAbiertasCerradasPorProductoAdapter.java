package woopets.com.woopets.Oferente.AdapterOferente;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import woopets.com.woopets.Oferente.DetallesVenasAC;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.VentasCerradasFecha_Cantidad;
import woopets.com.woopets.R;

/**
 * Created by tacto on 23/08/17.
 */

public class MisVentasAbiertasCerradasPorProductoAdapter extends BaseAdapter {

    //Atributos del adaptador
    Context mContext = null;
    AdapterCallback mListener = sDummyCallbacks;
    private List<CompraProductoAbiertaCerrada> productosVentasAbiertasCerradas;
    private Modelo sing = Modelo.getInstance();
    String imegenPortada = "";



    public MisVentasAbiertasCerradasPorProductoAdapter(Context mContext, AdapterCallback mListener){

        sing.adicionarListaClintesSegunLaCompra2(sing.uidProducto);
        if (sing.listatadoClintesComprasProducto.size() > 0) {
            Collections.sort(sing.listatadoClintesComprasProducto, new Comparator<CompraProductoAbiertaCerrada>() {
                @Override
                public int compare(CompraProductoAbiertaCerrada o1, CompraProductoAbiertaCerrada o2) {
                    return new Long(o2.getTimestamp()).compareTo(new Long(o1.getTimestamp()));
                }
            });



        }



        this.mContext = mContext;
        this.mListener = mListener;
        this.productosVentasAbiertasCerradas = sing.listatadoClintesComprasProducto;


    }

    @Override
    public int getCount() {
        return productosVentasAbiertasCerradas.size();
    }

    @Override
    public Object getItem(int position) {
        return productosVentasAbiertasCerradas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        final TextView txt_cantidad;
        final TextView txt_name;
        final TextView txt_fecha;
        final Button btn_flecha;
        final RelativeLayout contenedor_lista_clientes;


        final CompraProductoAbiertaCerrada comprasProductosAbiertasCerradas =  (CompraProductoAbiertaCerrada) getItem(position);

        // Inflate la vista de la productosOferentes
        RelativeLayout itemLayout =  (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.adapter_venta_ac_segun_producto, parent, false);

        txt_cantidad = (TextView) itemLayout.findViewById(R.id.txt_cantidad);
        txt_name = (TextView) itemLayout.findViewById(R.id.txt_name);
        txt_fecha = (TextView) itemLayout.findViewById(R.id.txt_fecha);
        btn_flecha = (Button) itemLayout.findViewById(R.id.btn_flecha);

        contenedor_lista_clientes = (RelativeLayout) itemLayout.findViewById(R.id.contenedor_lista_clientes);

        txt_cantidad.setText(""+comprasProductosAbiertasCerradas.getPedidos().get(0).getCantidad());
        txt_name.setText(""+comprasProductosAbiertasCerradas.getCliente().get(0).getNombre()+" "+comprasProductosAbiertasCerradas.getCliente().get(0).getApellido());
        txt_fecha.setText(""+comprasProductosAbiertasCerradas.getFecha());

        contenedor_lista_clientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sing.posicionServicio2  = position;
                Intent i = new Intent(mContext, DetallesVenasAC.class);
                i.putExtra("IDCOMPRA",comprasProductosAbiertasCerradas.getKey());
                mContext.startActivity(i);

            }
        });


        return itemLayout;
    }









    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
  */
    public interface AdapterCallback
    {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback()
    {


    };

}
