package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 1/12/17.
 */

public class ComandoActualizacionProductoOferente {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo  = Modelo.getInstance();

    public interface OnComandoActualizacionProductoOferenteoChangeListener {

        void cmEstadoProducto();
        void cmEliminarProducto();
        void cmDetacarProducto();
    }


    //interface del listener de la actividad interesada
    private OnComandoActualizacionProductoOferenteoChangeListener mListener;

    public ComandoActualizacionProductoOferente(OnComandoActualizacionProductoOferenteoChangeListener mListener){

        this.mListener = mListener;
    }


    public void setEstadoProducto(String idProducto, boolean activo){

        final DatabaseReference ref = database.getReference("productos/"+idProducto+"/activo/" );//ruta path
        ref.setValue(activo);
        mListener.cmEstadoProducto();
    }

    public void deleteEstadoProducto(String idProducto){

        final DatabaseReference ref = database.getReference("productos/"+idProducto );//ruta path
        ref.removeValue();
        mListener.cmEliminarProducto();
    }


    public void destacadoproducto(String idProducto, boolean estado, String name_foto){

        final DatabaseReference ref = database.getReference("productos/"+idProducto+"/activo/" );//ruta path
        ref.setValue(estado);

        final DatabaseReference ref2 = database.getReference("productos/"+idProducto+"/fotos/Destacado/" );//ruta path
        ref2.setValue(name_foto);

        mListener.cmDetacarProducto();
    }



    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoActualizacionProductoOferenteoChangeListener sDummyCallbacks = new OnComandoActualizacionProductoOferenteoChangeListener()
    {


        @Override
        public void cmEstadoProducto()
        {}

        @Override
        public void cmEliminarProducto()
        {}

        @Override
        public  void cmDetacarProducto(){}

    };

}
