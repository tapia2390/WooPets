package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.Cliente;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;

/**
 * Created by tacto on 24/11/17.
 */

public class ComandoPreguntaPublicacionesVenta {


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo = Modelo.getInstance();



    public interface OnComandoPreguntaPublicacionesVentaChangeListener {

        void preguntasMisPublicacionesVEnta();
    }

    //interface del listener de la actividad interesada
    private OnComandoPreguntaPublicacionesVentaChangeListener mListener;

    public ComandoPreguntaPublicacionesVenta(OnComandoPreguntaPublicacionesVentaChangeListener mListener) {

        this.mListener = mListener;
    }


    public void getPreguntasPublicacionesVenta(final String idPublicacion) {
        modelo.classPreguntas_a_misPublicacionesDetalle.clear();

        DatabaseReference ref = database.getReference("preguntas/");//ruta path
        Query query = ref.orderByChild("idPublicacion").equalTo(idPublicacion);

        ChildEventListener listener = new ChildEventListener()  {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {

                ClassPreguntas_A_MisPublicaciones classPreguntas_a_misPublicaciones = new ClassPreguntas_A_MisPublicaciones();

                Long timestamp = (Long) snap.child("timestamp").getValue();

                classPreguntas_a_misPublicaciones.setId(snap.getKey());
                classPreguntas_a_misPublicaciones.setFechaPregunta(snap.child("fechaPregunta").getValue().toString());
                if(snap.hasChild("fechaRespuesta")){
                    classPreguntas_a_misPublicaciones.setFechaRespuesta(snap.child("fechaRespuesta").getValue().toString());
                }

                if(snap.hasChild("respuesta")){
                    classPreguntas_a_misPublicaciones.setRespuesta(snap.child("respuesta").getValue().toString());

                }
                classPreguntas_a_misPublicaciones.setIdOferente(snap.child("idOferente").getValue().toString());
                classPreguntas_a_misPublicaciones.setPregunta(snap.child("pregunta").getValue().toString());
                classPreguntas_a_misPublicaciones.setTimestamp(timestamp);

                //idCliente
                classPreguntas_a_misPublicaciones.setIdCliente(snap.child("idCliente").getValue().toString());
                classPreguntas_a_misPublicaciones.setListaClienete(getDatosCliente(snap.child("idCliente").getValue().toString()));

                //idPublicaciones
                classPreguntas_a_misPublicaciones.setIdPublicacion(snap.child("idPublicacion").getValue().toString());
                classPreguntas_a_misPublicaciones.setListaProductosOferentes(getDatosProducto(snap.child("idPublicacion").getValue().toString()));


                    modelo.addPreguntasPublicacones(classPreguntas_a_misPublicaciones);
                    if(modelo.classPreguntas_a_misPublicaciones.get(modelo.classPreguntas_a_misPublicaciones.size()-1).listaProductosOferentes.size() > 0){

                        mListener.preguntasMisPublicacionesVEnta();
                    }




                if (modelo.classPreguntas_a_misPublicaciones.size() > 0) {
                    Collections.sort(modelo.classPreguntas_a_misPublicaciones, new Comparator<ClassPreguntas_A_MisPublicaciones>() {
                        @Override
                        public int compare(ClassPreguntas_A_MisPublicaciones o1, ClassPreguntas_A_MisPublicaciones o2) {
                            return new Long(o2.getTimestamp()).compareTo(new Long(o1.getTimestamp()));
                        }
                    });


                }




            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        query.addChildEventListener(listener);
        modelo.hijosListener.put(query,listener);

    }


    public ArrayList<Cliente> getDatosCliente(String idCliente) {

        final Cliente cliente = new Cliente();
        final ArrayList<Cliente> ltCliente = new ArrayList<Cliente>();

        DatabaseReference ref = database.getReference("clientes/" + idCliente + "/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                    cliente.setId(snap.getKey());
                if(snap.hasChild("nombre")){
                    cliente.setNombre(snap.child("nombre").getValue().toString());
                }

                if(snap.hasChild("apellido")){
                    cliente.setApellido(snap.child("apellido").getValue().toString());
                }
                if(snap.hasChild("celular")){
                    cliente.setCelular(snap.child("celular").getValue().toString());
                }
                if(snap.hasChild("correo")){
                    cliente.setCorreo(snap.child("correo").getValue().toString());
                }

                if(snap.hasChild("tokenDevice")){
                    cliente.setTokens(snap.child("tokenDevice").getValue().toString());
                }

                ltCliente.add(cliente);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return ltCliente;
    }

    public ArrayList<ProductosOferente> getDatosProducto(String idProducto) {

        final ProductosOferente productosOferente = new ProductosOferente();
        final ArrayList<ProductosOferente> ltProductosOferente = new ArrayList<ProductosOferente>();


        DatabaseReference ref = database.getReference("productos/"+idProducto+"/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                productosOferente.setId(snap.getKey());
                boolean activo =  (boolean) snap.child("activo").getValue();

                productosOferente.setActivo(activo);
                productosOferente.setCategoria(snap.child("categoria").getValue().toString());


                productosOferente.setDescripcion(snap.child("descripcion").getValue().toString());

                if(snap.child("destacado").getValue() != null){
                    boolean destacado =  (boolean) snap.child("destacado").getValue();
                    productosOferente.setDestacado(destacado);
                }

                boolean servicio =  (boolean) snap.child("servicio").getValue();

                if(servicio == true){
                    //nuevo arbol
                    DataSnapshot snaphorario;
                    snaphorario = (DataSnapshot) snap.child("horario");

                    //nuevo subarbol
                    DataSnapshot snapFinDeSemana;
                    snapFinDeSemana = (DataSnapshot) snaphorario.child("FinDeSemana");

                    if(snapFinDeSemana.child("dias").getValue() != null){

                        productosOferente.setHorarioDiasFinDeSemana(snapFinDeSemana.child("dias").getValue().toString());
                        productosOferente.setHorarioCierreDiasFinDeSemana(snapFinDeSemana.child("horaCierre").getValue().toString());
                        productosOferente.setHorarioInicioDiasFinDeSemana(snapFinDeSemana.child("horaInicio").getValue().toString());

                    }


                    if(snapFinDeSemana.child("sinJornadaContinua").getValue() !=null){
                        boolean sinJornadaContinuaFinSemana = (boolean) snapFinDeSemana.child("sinJornadaContinua").getValue();
                        productosOferente.setSinJornadaContinuaFinDeSemana(sinJornadaContinuaFinSemana);

                    }


                    //fin subarbol
                    //nuevo subarbol
                    DataSnapshot snapSemana;
                    snapSemana = (DataSnapshot) snaphorario.child("Semana");
                    if(snapSemana.child("dias").getValue() != null ){
                        productosOferente.setHorarioDiasDeLaSemana(snapSemana.child("dias").getValue().toString());
                        productosOferente.setHorarioCierreDiasDeLaSemana(snapSemana.child("horaCierre").getValue().toString());
                        productosOferente.setHorarioInicioDiasDeLaSemana(snapSemana.child("horaInicio").getValue().toString());

                    }

                    if(snapSemana.child("sinJornadaContinua").getValue() !=null){
                        boolean sinJornadaContinuaSemana = (boolean) snapSemana.child("sinJornadaContinua").getValue();
                        productosOferente.setSinJornadaContinuaSemana(sinJornadaContinuaSemana);
                    }

                    //fin subarbol
                    //fin arbol

                    productosOferente.setDuracion(((Long) snap.child("duracion").getValue()).intValue());
                    productosOferente.setDuracionMedida(snap.child("duracionMedida").getValue().toString());

                    boolean servicioDomicilio = (boolean) snap.child("servicioEnDomicilio").getValue();
                    productosOferente.setServicioEnDomicilio(servicioDomicilio);


                }

                if(servicio == false){
                    if(snap.child("subcategoria").getValue() != null ){
                        productosOferente.setSubcategoria(snap.child("subcategoria").getValue().toString());

                    }
                    productosOferente.setCantidad(((Long) snap.child("stock").getValue()).intValue());

                }
                productosOferente.setServicio(servicio);

                //nuevo arbol
                DataSnapshot snapFotos;
                snapFotos = (DataSnapshot) snap.child("fotos/");
                ArrayList<String> fotosProductosOferentes = new ArrayList<String>();
                for (DataSnapshot foto : snapFotos.getChildren()) {
                    fotosProductosOferentes.add(foto.getValue().toString());
                }

                productosOferente.setFotos(fotosProductosOferentes);

                productosOferente.setIdOferente(snap.child("idOferente").getValue().toString());
                productosOferente.setTitulo(snap.child("nombre").getValue().toString());
                productosOferente.setPrecio(snap.child("precio").getValue().toString());
                productosOferente.setTarget(snap.child("target").getValue().toString());

                ltProductosOferente.add(productosOferente);
                mListener.preguntasMisPublicacionesVEnta();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return ltProductosOferente;
    }




    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoPreguntaPublicacionesVentaChangeListener sDummyCallbacks = new OnComandoPreguntaPublicacionesVentaChangeListener() {


        @Override
        public void preguntasMisPublicacionesVEnta() {
        }

    };
}
