package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.NotificaionesOferente;


/**
 * Created by tacto on 16/12/17.
 */

public class ComandoNotificaciones {

    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    public interface OnComandoNotificacionesChangeListener {

        void cargoCategorias();
        void cargoTipoUsuario(String tipo);
        void  cargoCambio();
    }

    //interface del listener de la actividad interesada
    private ComandoNotificaciones.OnComandoNotificacionesChangeListener mListener;

    public ComandoNotificaciones(ComandoNotificaciones.OnComandoNotificacionesChangeListener mListener) {

        this.mListener = mListener;
    }


    public void getNotificocaiones() {

        modelo.listaNotifications.clear();
        DatabaseReference ref = database.getReference("mensajes/");//ruta path
        Query query = ref.orderByChild("idOferente").equalTo(modelo.uid);

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {


                ArrayList<NotificaionesOferente> listaNotifications2 = new ArrayList<NotificaionesOferente>();


                if (snap.exists()) {
                    NotificaionesOferente notificaionesOferente = new NotificaionesOferente();

                    notificaionesOferente.setId(snap.getKey());
                    notificaionesOferente.setIdOferente(snap.child("idOferente").getValue().toString());


                    if (snap.hasChild("idPublicacion")) {
                        notificaionesOferente.setIdPublicacion(snap.child("idPublicacion").getValue().toString());
                    }
                    if (snap.hasChild("idCompra")) {
                        notificaionesOferente.setIdCompra(snap.child("idCompra").getValue().toString());
                    }

                    if (snap.hasChild("infoAdicional")) {
                        notificaionesOferente.setInfoAdicional(snap.child("infoAdicional").getValue().toString());
                    }

                    if (snap.hasChild("idCliente")) {
                        notificaionesOferente.setIdCliente(snap.child("idCliente").getValue().toString());
                    }

                    notificaionesOferente.setMensaje(snap.child("mensaje").getValue().toString());
                    notificaionesOferente.setTimestamp(((Long) snap.child("timestamp").getValue()).intValue());
                    notificaionesOferente.setTipo(snap.child("tipo").getValue().toString());
                    notificaionesOferente.setTitulo(snap.child("titulo").getValue().toString());

                    if (snap.child("visto").getValue() != null) {
                        boolean visto = (boolean) snap.child("visto").getValue();
                        notificaionesOferente.setVisto(visto);

                    }

                    //listaNotifications2.add(notificaionesOferente);

                    modelo.addNotificacion(notificaionesOferente);

                }


                mListener.cargoCategorias();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mListener.cargoCategorias();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        query.addChildEventListener(listener);
        modelo.hijosListener.put(query, listener);


    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoNotificacionesChangeListener sDummyCallbacks = new OnComandoNotificacionesChangeListener() {
        @Override
        public void cargoCategorias() {
        }

        @Override
        public void cargoTipoUsuario(String tipo) {

        }

        @Override
        public void cargoCambio() {

        }


    };
}
