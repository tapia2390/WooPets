package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntasFrecuentes;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.PreguntasFrecuentes;

/**
 * Created by tacto on 24/11/17.
 */

public class ComandoPreguntasFrecuentes {


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo  = Modelo.getInstance();

    public interface OnComandoPreguntasFrecuentesChangeListener {

        void getPreguntasFrecuentes();
    }

    //interface del listener de la actividad interesada
    private OnComandoPreguntasFrecuentesChangeListener mListener;

    public ComandoPreguntasFrecuentes(OnComandoPreguntasFrecuentesChangeListener mListener){

        this.mListener = mListener;
    }


    public void  getListPreguntasfrecuentes(){
        //preguntasFrecuentes
        modelo.classpreguntasFrecuentes.clear();
        DatabaseReference ref = database.getReference("preguntasFrecuentes/");//ruta path

        ChildEventListener listener = new ChildEventListener(){
            @Override
            public void onChildAdded(DataSnapshot snpreguntas, String s) {

                ClassPreguntasFrecuentes preguntasFrecuentes = new ClassPreguntasFrecuentes();
                if(snpreguntas.child("estado").getValue().equals("Activo") ){
                    Long timestamp =  (Long) snpreguntas.child("timestamp").getValue();

                    preguntasFrecuentes.setId(snpreguntas.getKey());
                    preguntasFrecuentes.setPregunta(snpreguntas.child("pregunta").getValue().toString());
                    preguntasFrecuentes.setRespuesta(snpreguntas.child("respuesta").getValue().toString());
                    preguntasFrecuentes.setEstado(snpreguntas.child("estado").getValue().toString());
                    preguntasFrecuentes.setTimestamp(timestamp);
                    modelo.classpreguntasFrecuentes.add(preguntasFrecuentes);
                }

                mListener.getPreguntasFrecuentes();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        ref.addChildEventListener(listener);
        modelo.hijosListenerRef.put(ref,listener);


    }



    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoPreguntasFrecuentesChangeListener sDummyCallbacks = new OnComandoPreguntasFrecuentesChangeListener()
    {


        @Override
        public void getPreguntasFrecuentes()
        {}

    };
}
