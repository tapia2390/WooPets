package woopets.com.woopets.Oferente.ModelOferente;

import android.app.Activity;
import android.app.Notification;
import android.content.pm.PackageInfo;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import woopets.com.woopets.Comandos.ComandoCategorias;
import woopets.com.woopets.Modelo.Categorias;
import woopets.com.woopets.Modelo.Sistema;
import woopets.com.woopets.Modelo.cliente.*;
import woopets.com.woopets.Modelo.cliente.Direccion;
import woopets.com.woopets.Oferente.Comandos.ComandoComprasAbiertasCerradas;
import woopets.com.woopets.Oferente.Comandos.ComandoComprasTodas;
import woopets.com.woopets.Oferente.Comandos.ComandoParams;
import woopets.com.woopets.Oferente.Comandos.ComandoPreguntaPublicaciones;
import woopets.com.woopets.cliente.Comandos.ComandoTPaga;
import woopets.com.woopets.cliente.ModelCliente;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


/**
 * Created by tacto on 21/06/17.
 */

public class Modelo {
    private static final Modelo ourInstance = new Modelo();

    public String abiertaCerrada = "";
    public int posicionServicio = 0;
    public int posicionServicio2 = 0;
    public String idCliente = "";

    //datos detallados de un producto editar,cancelar,eliminar
    public int publicacion_estado = 0;
    public String pro_uidProducto = "";
    public int pro_posicionServicio = 0;
    public String pro_abiertascerrada = "";
    public int pro_oferente_vista = 0;
    public String idPreguntasClientePublicaciones;
    public int idPreguntasClientePublicacionesPosicion = 0;
    ;
    public int vista = 0;
    public boolean gpsActivo = false;
    public boolean activa_inactiva = false;
    public String abiertas_cerrada = "abiertas";
    public int posicionRespuesta = 0;
    public String idCompara = "";
    public String token = "";
    public boolean servicioEnDomicilio = true;
    public String horarioMyPublicacionesArticulo = "";
    public String actividadHorario = "";
    public int chatVistos = 1;
    public int filtro = 0;
    public int vistaPublicacion = 0;
    public String nombreProducto = "" ;
    public int primercarga = 0;

    public static Modelo getInstance() {
        return ourInstance;
    }

    public HashMap<Query, ChildEventListener> hijosListener = new HashMap<>();
    public HashMap<Query, ValueEventListener> hijosListenerValue = new HashMap<>();
    public HashMap<DatabaseReference, ChildEventListener> hijosListenerRef = new HashMap<>();

    public Modelo() {
    }


    public void pararListeners() {
        for (Map.Entry<Query, ChildEventListener> entry : hijosListener.entrySet()) {
            Query ref = entry.getKey();
            ChildEventListener listener = entry.getValue();
            ref.removeEventListener(listener);

        }

        for (Map.Entry<Query, ValueEventListener> entry : hijosListenerValue.entrySet()) {
            Query ref = entry.getKey();
            ValueEventListener listener = entry.getValue();
            ref.removeEventListener(listener);

        }


        for (Map.Entry<DatabaseReference, ChildEventListener> entry : hijosListenerRef.entrySet()) {
            Query ref = entry.getKey();
            ChildEventListener listener = entry.getValue();
            ref.removeEventListener(listener);

        }

    }


    public String uid = "";
    public String idClienteTpaga = "";
    public String urlTpaga = "https://apps.co/comunidad/ver/1389/tpaga/";

    public DecimalFormat formatea = new DecimalFormat("#,###,###");
    //public NumberFormat formatea = new NumberFormat("###.###.##");

    public ArrayList<Categorias> listaCategorias = new ArrayList<Categorias>();

    //producto oferente
    public ProductosOferente productosOferente = new ProductosOferente();

    //variables del horario al registrar un  Oferente
    public ArrayList<String> diasDeLaSemana = new ArrayList<String>();
    public ArrayList<String> finDeSemanaFestivos = new ArrayList<String>();

    //variables del horario  oferente
    public String horariodesemanaIni = "";
    public String horariodesemanaFin = "";
    public String horariofindesemanaIni = "";
    public String horariofindesemanaFin = "";
    public Double latitud;
    public Double longitud;
    public boolean sinJornadaContinuaSemana = false;
    public boolean sinJornadaContinuaFinDeSemana = false;

    public boolean servicio_producto = false;
    //variables del horario al registrar un articulo Oferente
    public ArrayList<String> diasDeLaSemanaArticulo = new ArrayList<String>();
    public ArrayList<String> finDeSemanaFestivosArticulo = new ArrayList<String>();
    public String horariodesemanaIniArticulo = "";
    public String horariodesemanaFinArticulo = "";
    public String horariofindesemanaIniArticulo = "";
    public String horariofindesemanaFinArticulo = "";
    public double latitudArticulo;
    public double longitudArticulo;
    public boolean sinJornadaContinuaSemanaArticulo;
    public boolean sinJornadaContinuaFinDeSemanaArticulo;

    //variables para la opcion de selecionar una categorias
    public boolean selectImge = false;
    public int selectPosition = -1;
    public int selectPositionAnterior = 0;
    public int position = 0;
    public String versionName = "";


    //tpaga
    public String tpprivate = "";
    public String tppublic = "";
    public String tpurl = "";


    //datos activacion destacado
    public boolean dActivo = false;
    public String name_destacado = "";
    public String uidProducto = "";
    public int valodDestacado = 0;


    //arraylist Oferente
    public DatosOferente datosOferente = new DatosOferente();
    public Params params = new Params();
    public ClassTerminosYCondiciones classTerminosYCondiciones = new ClassTerminosYCondiciones();
    public ArrayList<ProductosOferente> productosOferentes = new ArrayList<ProductosOferente>();
    public ArrayList<ProductosOferente> productosOferentesBoolean = new ArrayList<ProductosOferente>();
    public ArrayList<ProductosOferente> productosOferentes2 = new ArrayList<ProductosOferente>();
    public ArrayList<ProductosOferente> listaProductosOferentes = new ArrayList<ProductosOferente>();
    public ArrayList<ProductosOferente> productosOferentesInactivas = new ArrayList<ProductosOferente>();
    public ArrayList<CompraProductoAbiertaCerrada> comprasProductosAbiertasCerradases = new ArrayList<CompraProductoAbiertaCerrada>();
    public ArrayList<CompraProductoAbiertaCerrada> comprasProductosAbiertasCerradases2 = new ArrayList<CompraProductoAbiertaCerrada>();
    public ArrayList<CompraProductoAbiertaCerrada> listatadoClintesComprasProducto = new ArrayList<CompraProductoAbiertaCerrada>();
    public ArrayList<PreguntasPublicaciones> preguntasPublicaciones = new ArrayList<PreguntasPublicaciones>();
    public ArrayList<ClassPreguntasFrecuentes> classpreguntasFrecuentes = new ArrayList<ClassPreguntasFrecuentes>();
    public ArrayList<ClassPreguntas_A_MisPublicaciones> classPreguntas_a_misPublicaciones = new ArrayList<ClassPreguntas_A_MisPublicaciones>();
    public ArrayList<ClassPreguntas_A_MisPublicaciones> classPreguntas_a_misPublicacionesCliente = new ArrayList<ClassPreguntas_A_MisPublicaciones>();
    public ArrayList<ClassPreguntas_A_MisPublicaciones> classPreguntas_a_misPublicacionesDetalle = new ArrayList<ClassPreguntas_A_MisPublicaciones>();
    public ArrayList<NotificaionesOferente> listaNotifications = new ArrayList<NotificaionesOferente>();
    public ArrayList<ChatOferente> chatOferentes = new ArrayList<ChatOferente>();
    public ArrayList<Calificacion> calificaiones = new ArrayList<Calificacion>();
    public ArrayList<String> target = new ArrayList<String>();
    public ArrayList<CompraProductoAbiertaCerrada> comprasProductos = new ArrayList<CompraProductoAbiertaCerrada>();
    public ArrayList<ClassPreguntas_A_MisPublicaciones> comprasProductos2 = new ArrayList<ClassPreguntas_A_MisPublicaciones>();
    public ArrayList<ClassPreguntas_A_MisPublicaciones> preguntas_publicaiones = new ArrayList<ClassPreguntas_A_MisPublicaciones>();
    public ArrayList<ClassPreguntas_A_MisPublicaciones> preguntas_publicaiones_detalle = new ArrayList<ClassPreguntas_A_MisPublicaciones>();

    //public ArrayList<Tarjetas> tarjeta = new ArrayList<Tarjetas>();
    public ArrayList<MiniTarjeta> tarjetas = new ArrayList<>();


    public ArrayList<Pedido> pedidoAbiertas = new ArrayList<Pedido>();
    public ArrayList<Pedido> pedidoCerardas = new ArrayList<Pedido>();


    public ArrayList<CompraProductoAbiertaCerrada> comprasProductosAbiertas = new ArrayList<CompraProductoAbiertaCerrada>();
    public ArrayList<CompraProductoAbiertaCerrada> comprasProductosCerradas = new ArrayList<CompraProductoAbiertaCerrada>();
    //arraylist usuario

    //para utilizar en la seleccion de fechas
    public String[] fechas = {"Hoy", "Mañana", "Pasado Mañana"};
    private SimpleDateFormat df = new SimpleDateFormat("EEEE, MMMM dd");
    public SimpleDateFormat dfsimple = new SimpleDateFormat("dd/MM/yyyy");


    public Sistema sistema = new Sistema();




    public Map getMapaToPagoInicial(MiniTarjeta mini) {


        Map<String, Object> dirEnvio = new HashMap<>();
        dirEnvio.put("street1", datosOferente.direccion);
        dirEnvio.put("city", datosOferente.direccion);
        dirEnvio.put("state", datosOferente.direccion);
        dirEnvio.put("phone", datosOferente.celular);


        Map<String, Object> dirCobro = new HashMap<>();
        dirCobro.put("street1", mini.direccion);
        dirCobro.put("city", mini.ciudad);
        dirCobro.put("state", mini.departamento);
        dirCobro.put("phone", mini.telefono);


        Map<String, Object> tarjeta = new HashMap<>();
        tarjeta.put("number", mini.numero);
        tarjeta.put("securityCode", mini.securityCode);
        tarjeta.put("expirationDate", mini.expirationDate);
        tarjeta.put("name", mini.nombre);
        tarjeta.put("INSTALLMENTS_NUMBER", mini.cuotas);
        tarjeta.put("paymentMethod", mini.franquicia);
        tarjeta.put("ipAddress", mini.ip);


        Map<String, Object> data = new HashMap<>();
        data.put("referenceCode", mini.referenceCode);
        data.put("fullName", mini.nombre);
        data.put("emailAddress", mini.correo);
        data.put("contactPhone", datosOferente.celular);
        data.put("dniNumber", mini.cedula);         //cedula
        data.put("dirEnvio", dirEnvio);
        data.put("dirCobro", dirCobro);
        data.put("tarjeta", tarjeta);


        return data;

    }


    public Map getMapaToTokenizar(MiniTarjeta mini){

        ModelCliente modelo = ModelCliente.getInstance();

        Map<String, Object> data = new HashMap<>();
        data.put("name", mini.nombre );
        data.put("identificationNumber", mini.cedula );
        data.put("paymentMethod", mini.franquicia );
        data.put("dniNumber", mini.cedula );         //cedula
        data.put("number", mini.numero );
        data.put("expirationDate", mini.expirationDate );

        return  data;


    }


    public Map getMapaToPagoToken(Compra compra, MiniTarjeta mini ){


        Map<String, Object> dirEnvio = new HashMap<>();
        dirEnvio.put("street1", datosOferente.direccion);
        dirEnvio.put("city", datosOferente.direccion);
        dirEnvio.put("state", datosOferente.direccion);
        dirEnvio.put("phone", datosOferente.celular);


        Map<String, Object> dirCobro = new HashMap<>();
        dirCobro.put("street1", mini.direccion);
        dirCobro.put("city", mini.ciudad);
        dirCobro.put("state", mini.departamento);
        dirCobro.put("phone", mini.telefono);

        Map<String, Object> tarjeta = new HashMap<>();
        tarjeta.put("creditCardTokenId", mini.token);
        tarjeta.put("INSTALLMENTS_NUMBER", mini.cuotas);
        tarjeta.put("paymentMethod", mini.franquicia);
        tarjeta.put("ipAddress", mini.ip);

        Map<String, Object> producto = new HashMap<>();
        producto.put("valor", compra.valor);
        producto.put("cantidad", compra.cantidad);


        Map<String, Object> data = new HashMap<>();
        data.put("referenceCode", compra.id);
        data.put("fullName", mini.nombre );
        data.put("emailAddress", mini.correo );
        data.put("contactPhone", datosOferente.celular );
        data.put("dniNumber", mini.cedula );         //cedula
        data.put("dirEnvio", dirEnvio );
        data.put("dirCobro", dirCobro );
        data.put("tarjeta", tarjeta );
        data.put("producto", producto );


        return  data;


    }



    public MiniTarjeta getTarjetaActiva(){
        for (MiniTarjeta mini:tarjetas){
            if (mini.activo) {
                return  mini;
            }
        }
        return null;
    }



    public void apagarTarjetas(){
        for (MiniTarjeta mini:tarjetas){
            new ComandoTPaga(null).desactivarTarjetaOferente(mini.id);
            mini.activo = false ;
        }

    }



    /**
     * Convierte la fecha de String a tipo Datime
     * Tiene en cuenta que el texto puede ser Hoy, Mañana, Pasado Mañana
     *
     * @param valor
     */
    public DateTime convertToFecha(String valor) {

        if (valor.equals(fechas[0])) {
            return DateTime.now();
        } else if (valor.equals(fechas[1])) {
            return DateTime.now().plusDays(1);
        } else if (valor.equals(fechas[2])) {
            return DateTime.now().plusDays(2);
        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern("EEEE, MMMM dd");
        DateTime dt = formatter.parseDateTime(valor);

        return dt;
    }

    /**
     * MEtodo que dada una fecha la convierte al formato hoy , mañana y pasado mañana o EEE MMMM dd
     *
     * @return
     */
    public String formatearFecha(Date f) {


        if (df.format(f).equals(df.format(new Date()))) {
            return fechas[0];
        }

        GregorianCalendar gc = new GregorianCalendar();
        gc.add(Calendar.DATE, 1);

        if (df.format(f).equals(df.format(gc.getTime()))) {
            return fechas[1];
        }

        gc.add(Calendar.DATE, 1);
        if (df.format(f).equals(df.format(gc.getTime()))) {
            return fechas[2];
        }

        return df.format(f);

    }

    /**
     * TOma los datos del sistema validacion de verciones
     *
     * @return
     */
    public Sistema tomarDatosSistema(Activity act) {
        Sistema sis = new Sistema();
        try {
            PackageInfo info = act.getPackageManager().getPackageInfo(act.getPackageName(), 0);

//            sis.setVersion(Integer.parseInt(info.versionName));
            sis.setBuild(Integer.parseInt("" + info.versionCode));


        } catch (Exception e) {
            sis.setBuild(10);
            sis.setVersion(10);
        }


        return sis;
    }

    // validacion de verciones
    public boolean existeActualizacion(Activity act) {


        Sistema miSis = tomarDatosSistema(act);

        if (miSis.getBuild() < sistema.getBuild()) {
            return true;
        } else {
            return false;
        }

        // return miSis.getBuild() < sistema.getBuild();

    }

    //eliminar tildes
    private static final String ORIGINAL
            = "ÁáÉéÍíÓóÚúÑñÜü";
    private static final String REPLACEMENT
            = "AaEeIiOoUuNnUu";

    public String eliminarTildes(String str) {
        if (str == null) {
            return null;
        }
        char[] array = str.toCharArray();
        for (int index = 0; index < array.length; index++) {
            int pos = ORIGINAL.indexOf(array[index]);
            if (pos > -1) {
                array[index] = REPLACEMENT.charAt(pos);
            }
        }
        return new String(array);
    }


    //
    public void adicionarListaClintesSegunLaCompra(String idProducto) {

        listatadoClintesComprasProducto.clear();
        for (int i = 0; i < comprasProductosAbiertasCerradases2.size(); i++) {
            if (comprasProductosAbiertasCerradases2.get(i).getPedidos().get(0).getIdPublicacion().equals(idProducto)) {
                listatadoClintesComprasProducto.add(comprasProductosAbiertasCerradases2.get(i));
            }
        }

    }


    public void adicionarListaClintesSegunLaCompra2(String idProducto) {
        ArrayList<CompraProductoAbiertaCerrada> listatadoClintesComprasProducto2 = new ArrayList<CompraProductoAbiertaCerrada>();
        
        for (int i = 0; i < listatadoClintesComprasProducto.size(); i++) {
            if (listatadoClintesComprasProducto.get(i).getPedidos().get(0).getIdPublicacion().equals(idProducto) &&  listatadoClintesComprasProducto.get(i).getAbiertaCerrada().equals(abiertas_cerrada)) {
                listatadoClintesComprasProducto2.add(listatadoClintesComprasProducto.get(i));
            }
        }


        listatadoClintesComprasProducto.clear();
        listatadoClintesComprasProducto =listatadoClintesComprasProducto2;
    }


    public void adicionarListaPreguntasAmisPublcaciones(String idPreguntasClientePublicaciones) {

        classPreguntas_a_misPublicacionesDetalle.clear();
        for (int i = 0; i < classPreguntas_a_misPublicaciones.size(); i++) {
            if (classPreguntas_a_misPublicaciones.get(i).getIdPublicacion().equals(idPreguntasClientePublicaciones)) {
                classPreguntas_a_misPublicacionesDetalle.add(classPreguntas_a_misPublicaciones.get(i));
            }
        }
    }


    public void adicionarListaPreguntasAmisPublcaciones2(String idPreguntasClientePublicaciones, String idCliente) {

        classPreguntas_a_misPublicacionesDetalle.clear();
        for (int i = 0; i < classPreguntas_a_misPublicaciones.size(); i++) {
            if (classPreguntas_a_misPublicaciones.get(i).getIdPublicacion().equals(idPreguntasClientePublicaciones) && classPreguntas_a_misPublicaciones.get(i).getIdCliente().equals(idCliente)) {
                classPreguntas_a_misPublicacionesDetalle.add(classPreguntas_a_misPublicaciones.get(i));
            }
        }
    }

    public void adicionarListaPreguntasAmisPublcacionesNotificacion(String idPreguntasClientePublicaciones) {

        classPreguntas_a_misPublicacionesDetalle.clear();
        for (int i = 0; i < classPreguntas_a_misPublicaciones.size(); i++) {
            if (classPreguntas_a_misPublicaciones.get(i).getIdPublicacion().equals(idPreguntasClientePublicaciones)) {
                classPreguntas_a_misPublicacionesDetalle.add(classPreguntas_a_misPublicaciones.get(i));
            }
        }
    }

    //evitar datos repetidos

    //compras
    public void addCompraAbiertaCerrada(CompraProductoAbiertaCerrada compra) {

        int index = getIndexDeCompra(compra);
        if (index >= 0) {
            comprasProductosAbiertasCerradases2.set(index, compra);
            return;
        }

        comprasProductosAbiertasCerradases2.add(compra);

    }


    private int getIndexDeCompra(CompraProductoAbiertaCerrada comp) {
        int index = 0;
        for (CompraProductoAbiertaCerrada compra : comprasProductosAbiertasCerradases2) {
            if (compra.getKey().equals(comp.getKey())) {
                return index;
            }
            index++;
        }
        return -1;
    }

    //chat

    public void addChat(ChatOferente chat) {

        int index = getIndexDeChat(chat);
        if (index >= 0) {
            chatOferentes.set(index, chat);
            return;
        }

        chatOferentes.add(chat);

    }


    private int getIndexDeChat(ChatOferente chat) {
        int index = 0;
        for (ChatOferente chats : chatOferentes) {
            if (chats.getId().equals(chat.getId())) {
                return index;
            }
            index++;
        }
        return -1;
    }

    //notificacaiones
    public void addNotificacion(NotificaionesOferente noti) {

        int index = getIndexDeNotificacion(noti);
        if (index >= 0) {
            listaNotifications.set(index, noti);
            return;
        }

        listaNotifications.add(noti);

    }


    private int getIndexDeNotificacion(NotificaionesOferente noti) {
        int index = 0;
        for (NotificaionesOferente notis : listaNotifications) {
            if (notis.getId().equals(noti.getId())) {
                return index;
            }
            index++;
        }
        return -1;
    }
    //fin

    //abiertas
    //borrar datos repetidos

    public void addCompraAbierta(CompraProductoAbiertaCerrada compra) {

        int index = getIndexDeCompraAbierta(compra);
        if (index >= 0) {
            comprasProductosAbiertas.set(index, compra);
            return;
        }
        comprasProductosAbiertas.add(compra);

    }


    private int getIndexDeCompraAbierta(CompraProductoAbiertaCerrada comp) {
        int index = 0;
        for (CompraProductoAbiertaCerrada compra : comprasProductosAbiertas) {
            if (compra.getKey().equals(comp.getKey())) {
                return index;
            }
            index++;
        }
        return -1;
    }





    //cerradas

    public void addCompraCerrada(CompraProductoAbiertaCerrada compra) {

        int index = getIndexDeCompraCerrada(compra);
        if (index >= 0) {
            comprasProductosCerradas.set(index, compra);
            return;
        }

        comprasProductosCerradas.add(compra);

    }


    private int getIndexDeCompraCerrada(CompraProductoAbiertaCerrada comp) {
        int index = 0;
        for (CompraProductoAbiertaCerrada compra : comprasProductosCerradas) {
            if (compra.getKey().equals(comp.getKey())) {
                return index;
            }
            index++;
        }
        return -1;
    }



    public void addPreguntasPublicacones(ClassPreguntas_A_MisPublicaciones pregunta) {

        int index = getIndexPreguntasPublicaciones(pregunta);
        if (index >= 0) {
            classPreguntas_a_misPublicaciones.set(index, pregunta);
            return;
        }


        classPreguntas_a_misPublicaciones.add(pregunta);

    }



    private int getIndexPreguntasPublicaciones(ClassPreguntas_A_MisPublicaciones preg) {
        int index = 0;
        for (ClassPreguntas_A_MisPublicaciones pregunta : classPreguntas_a_misPublicaciones) {
            if (pregunta.getId().equals(preg.getId())) {
                return index;
            }
            index++;
        }
        return -1;
    }


    public void limiarDatos() {
        productosOferente.setId("");
        productosOferente.setActivo(false);
        productosOferente.setTitulo("");
        productosOferente.setCategoria("");
        productosOferente.setCantidad(0);
        productosOferente.setSubcategoria("");
        productosOferente.setDescripcion("");
        productosOferente.setDestacado(false);
        productosOferente.setHorarioDiasDeLaSemana("");
        productosOferente.setHorarioInicioDiasDeLaSemana("");
        productosOferente.setHorarioCierreDiasDeLaSemana("");
        productosOferente.setSinJornadaContinua(false);
        productosOferente.setHorarioDiasFinDeSemana("");
        productosOferente.setHorarioInicioDiasFinDeSemana("");
        productosOferente.setHorarioCierreDiasFinDeSemana("");
        productosOferente.setFinsinJornadaContinua(false);
        productosOferente.setIdOferente("");
        productosOferente.setNombre("");
        productosOferente.setPrecio("");
        productosOferente.setServicio(false);
        productosOferente.setTarget("");
        productosOferente.setServicioEnDomicilio(false);
        productosOferente.setDuracion(0);
        productosOferente.setDuracionMedida("");
        productosOferente.setFechaCreacion("");
        productosOferente.setTimestamp(0);
        productosOferente.setSinJornadaContinuaSemana(false);
        productosOferente.setSinJornadaContinuaFinDeSemana(false);
        productosOferente.getFotos().clear();
        diasDeLaSemana.clear();
        sinJornadaContinuaSemana = false;
        sinJornadaContinuaFinDeSemana = false;

        horariodesemanaIni = "";
        horariodesemanaFin = "";

        finDeSemanaFestivos.clear();
        horariofindesemanaIni = "";
        horariofindesemanaFin = "";
        target.clear();
        servicio_producto = false;
    }


    public CompraProductoAbiertaCerrada getCompraByIdCompra(String idCompra) {

        for (CompraProductoAbiertaCerrada com : comprasProductosAbiertasCerradases2) {  //  Abiertas o Cerradas
            if (com.getKey().equals(idCompra)) {
                return com;
            }
        }

        for (CompraProductoAbiertaCerrada com : comprasProductosCerradas) {  // Cerradas
            if (com.getKey().equals(idCompra)) {
                return com;
            }
        }

        for (CompraProductoAbiertaCerrada com : comprasProductosAbiertas) {  // Abiertas
            if (com.getKey().equals(idCompra)) {
                return com;
            }
        }


        return null;
    }


    public void borrarCompra(String tipo, String idCompra) {
        if(tipo.equals("abiertas")){
            for(CompraProductoAbiertaCerrada com :comprasProductosAbiertas)
            {  // Cerradas
                if (com.getKey().equals(idCompra)) {
                    comprasProductosAbiertas.remove(com);
                    return;
                }
            }
        }else{
            for(CompraProductoAbiertaCerrada com :comprasProductosAbiertas)
            {  // Cerradas
                if (com.getKey().equals(idCompra)) {
                    comprasProductosCerradas.remove(com);
                    return;
                }
            }
        }

    }

    public void borrarPublicacion(String tipo, String idpublicacion) {
        if(tipo.equals("activas")){
            for(ProductosOferente com :productosOferentes)
            {  // Cerradas
                if (com.getId().equals(idpublicacion)) {
                    productosOferentes.remove(com);
                    return;
                }
            }
        }else{
            for(ProductosOferente com :productosOferentes2)
            {  // Cerradas
                if (com.getId().equals(idpublicacion)) {
                    productosOferentes2.remove(com);
                    return;
                }
            }
        }

    }


    public  ArrayList<ProductosOferente> getProductoByIdProducto(String idProductoa, String tipo) {

        final ArrayList<ProductosOferente> pOferente = new ArrayList<ProductosOferente>();
        pOferente.clear();

        if(tipo.equals("abiertas")){
            for (ProductosOferente com : productosOferentes) {  // Activas
                if (com.getId().equals(idProductoa)) {
                    pOferente.add(com);
                    return pOferente;
                }
            }
        }else{
            for (ProductosOferente com : productosOferentes2) {  // Inactivas
                if (com.getId().equals(idProductoa)) {
                    pOferente.add(com);
                    return pOferente;
                }
            }
        }
        return null;
    }



    public interface OnModeloListener {

        void terminoPrecarga();

    }


    // recargar informacion
    public boolean repintardetalles(final Modelo.OnModeloListener listener){

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


        if (user == null) {
            return false;
        }


        new ComandoComprasAbiertasCerradas(new ComandoComprasAbiertasCerradas.OnComandoComprasAbiertasCerradasoChangeListener() {
            @Override
            public void comprasAbiertasCerradasoOferente() {

            }
        }).comprasAbiertasCerradas_(uid, "abiertas");

        new ComandoPreguntaPublicaciones(new ComandoPreguntaPublicaciones.OnComandoPreguntaPublicacionesChangeListener() {
            @Override
            public void preguntasMisPublicaciones() {

            }

            @Override
            public void preguntasMisPublicacionesNotificacion() {

            }
        }).getPreguntasPublicaciones2();


        new ComandoComprasTodas(new ComandoComprasTodas.OnComandoComprasAbiertasCerradasoChangeListener() {
            @Override
            public void comprasTodas() {

            }
        }).comprasAbiertas("abiertas");


        new ComandoComprasTodas(new ComandoComprasTodas.OnComandoComprasAbiertasCerradasoChangeListener() {
            @Override
            public void comprasTodas() {

            }
        }).comprasCerradas("cerradas");

       /* ComandoComprasAbiertasCerradas comandoComprasAbiertasCerradas;
        ComandoPreguntaPublicaciones comandoPreguntaPublicaciones;
        ComandoComprasTodas comandoComprasTodas;


        comandoComprasAbiertasCerradas = new ComandoComprasAbiertasCerradas(null);
        comandoComprasAbiertasCerradas.comprasAbiertasCerradas_(uid, "abiertas");


        comandoPreguntaPublicaciones = new ComandoPreguntaPublicaciones(null);
        comandoPreguntaPublicaciones.getPreguntasPublicaciones2();

        comandoComprasTodas = new ComandoComprasTodas(null);
        comandoComprasTodas.comprasAbiertas("abiertas");
        comandoComprasTodas.comprasCerradas("cerradas");*/



        return true;

    }

    //filtrar preguntas sin responder
    public void adicionarListaPreguntasSinResponder() {

        preguntas_publicaiones.clear();
        for (int i = 0; i < classPreguntas_a_misPublicaciones.size(); i++) {
            if (classPreguntas_a_misPublicaciones.get(i).getFechaRespuesta() == null || classPreguntas_a_misPublicaciones.get(i).getFechaRespuesta().equals(null)) {
                preguntas_publicaiones.add(classPreguntas_a_misPublicaciones.get(i));
            }
        }

    }

    //todas las preguntas
    public void adicionarListaPreguntas(String idPuplicacion, int filtro) {

        preguntas_publicaiones_detalle.clear();
        for (int i = 0; i < classPreguntas_a_misPublicaciones.size(); i++) {

            if (classPreguntas_a_misPublicaciones.get(i).getIdPublicacion().equals(idPuplicacion) && filtro == 0 &&  classPreguntas_a_misPublicaciones.get(i).getFechaRespuesta() ==null) {
                preguntas_publicaiones_detalle.add(classPreguntas_a_misPublicaciones.get(i));
            }

            if (classPreguntas_a_misPublicaciones.get(i).getIdPublicacion().equals(idPuplicacion) && filtro == 1) {
                preguntas_publicaiones_detalle.add(classPreguntas_a_misPublicaciones.get(i));
            }
        }

    }


    public ClassPreguntas_A_MisPublicaciones getPreguntaById(String idCompra) { // es idPregunta

        for (ClassPreguntas_A_MisPublicaciones com : preguntas_publicaiones_detalle) {
            if (com.getId().equals(idCompra) || com.getIdPublicacion().equals(idCompra)) {
                return com;
            }
        }

        for (ClassPreguntas_A_MisPublicaciones com : classPreguntas_a_misPublicaciones) {
            if (com.getIdPublicacion().equals(idCompra)  ||  com.getId().equals(idCompra)) {
                return com;
            }
        }

        return null;
    }


}
