package woopets.com.woopets.Oferente.AdapterOferente;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import woopets.com.woopets.Oferente.ModelOferente.ChatOferente;
import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.RespuestaPreguntasPublicaciones;
import woopets.com.woopets.R;

/**
 * Created by tacto on 23/08/17.
 */

public class ChatAdapter extends BaseAdapter {

    //Atributos del adaptador
    final Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<ChatOferente> chatOferentes;
    private Modelo sing = Modelo.getInstance();
    String imegenPortada = "";



    public ChatAdapter(Context mContext, AdapterCallback mListener){

        //ordenar
        Collections.sort(sing.chatOferentes, new Comparator<ChatOferente>() {
            @Override
            public int compare(ChatOferente o1, ChatOferente o2) {
                return new Long(o1.getTimestamp()).compareTo(new Long(o2.getTimestamp()));
            }
        });


        Set<ChatOferente> quipu = new HashSet<ChatOferente>(sing.chatOferentes);
        for (ChatOferente key : quipu) {
            System.out.println(key + " : " + Collections.frequency(sing.chatOferentes, key));
        }

        this.mContext = mContext;
        this.mListener = mListener;
        this.chatOferentes = sing.chatOferentes;

    }

    @Override
    public int getCount() {
        return chatOferentes.size();
    }

    @Override
    public Object getItem(int position) {
        return chatOferentes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        final TextView id_pregunta;
        final TextView txt_nombre_cliente;
        final TextView fecha_pregunta;
        final TextView txt_pregunta;
        final TextView txt_respuesta;
        final  TextView fecha_respuesta;
        final Button responder;
        final RelativeLayout contenedor_lista_clientes;
        LinearLayout layout_respusta;
        LinearLayout oferente;


        final ChatOferente chatOferentes =  (ChatOferente) getItem(position);

        // Inflate la vista de la productosOferentes
        RelativeLayout itemLayout =  (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.adapter_chat, parent, false);


        fecha_pregunta = (TextView) itemLayout.findViewById(R.id.fecha_pregunta);
        txt_pregunta = (TextView) itemLayout.findViewById(R.id.txt_pregunta);
        fecha_respuesta = (TextView) itemLayout.findViewById(R.id.fecha_respuesta);
        txt_respuesta = (TextView) itemLayout.findViewById(R.id.txt_respuesta);
        id_pregunta = (TextView) itemLayout.findViewById(R.id.id_pregunta);

        layout_respusta = (LinearLayout) itemLayout.findViewById(R.id.layout_respusta);
        oferente = (LinearLayout) itemLayout.findViewById(R.id.oferente);


        if(chatOferentes.emisor.equals(sing.uid)){
            oferente.setVisibility(View.VISIBLE);
            fecha_pregunta.setText(chatOferentes.getFechaMensaje());
            txt_pregunta.setText(chatOferentes.getMensaje());
        }

        if(chatOferentes.emisor.equals(sing.idCliente)){
            layout_respusta.setVisibility(View.VISIBLE);
            fecha_respuesta.setText(chatOferentes.getFechaMensaje());
            txt_respuesta.setText(chatOferentes.getMensaje());
        }


        return itemLayout;
    }



    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
  */
    public interface AdapterCallback
    {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback()
    {


    };

}
