package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import woopets.com.woopets.Oferente.Comandos.ComandoChatOferente;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;

public class RespuestaChat extends Activity implements ComandoChatOferente.OnComandoChatListener {

    Modelo modelo = Modelo.getInstance();
    EditText text_respuesta;
    ComandoChatOferente comandoChatOferente;
    String idCompra = "";
    CompraProductoAbiertaCerrada compra;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_respuesta_chat);

        text_respuesta = (EditText)findViewById(R.id.text_respuesta);
        comandoChatOferente = new ComandoChatOferente(this);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

        if (getIntent().hasExtra("IDCOMPRA")) {
            idCompra = getIntent().getStringExtra("IDCOMPRA");
            compra = modelo.getCompraByIdCompra(idCompra);
        } else {
            return;   //Esto es para que alguna vista esta llamando esta vista y no pasa el parametro del IDCOMPRA el error sea evidente y asi detectarlo mas facil
        }

    }

    //atras hadware
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            atras();
        }
        return true;
    }

    private void atras() {

        finish();
    }

    public void btn_atras(View v){
        atras();

    }


    public void btn_ok(View v){

        if(text_respuesta.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Campo obligatorio",Toast.LENGTH_LONG).show();
            text_respuesta.setError("Campo obligatorio");
        }else{
            comandoChatOferente.ponerChat(text_respuesta.getText().toString());
        }

    }

    @Override
    public void getChasts() {

    }

    @Override
    public void setChasts() {
        comandoChatOferente.enviarPushNotification(compra.getPedidos().get(0).getProductosOferentes().get(0).getNombre());
        text_respuesta.setText("");
        showAlertChat();

    }


    public void showAlertChat() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("Mensaje" );

        // set dialog message
        alertDialogBuilder
                .setMessage("Mensaje enviado exitosamente")
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        atras();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

}
