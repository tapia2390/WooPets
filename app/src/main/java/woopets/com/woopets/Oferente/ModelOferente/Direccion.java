package woopets.com.woopets.Oferente.ModelOferente;

/**
 * Created by tacto on 23/01/18.
 */

public class Direccion {

    String id = "";
    String direccion = "";
    String nombre = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
