package woopets.com.woopets.Oferente.Comandos;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 11/12/17.
 */

public class ComandoCancelarVenta {


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo  = Modelo.getInstance();

    public interface OnComandoCancelarVentaoChangeListener {

        void ventaCancelada();
    }


    //interface del listener de la actividad interesada
    private ComandoCancelarVenta.OnComandoCancelarVentaoChangeListener mListener;

    public ComandoCancelarVenta(ComandoCancelarVenta.OnComandoCancelarVentaoChangeListener mListener){

        this.mListener = mListener;
    }


    public void setCancelarVenta(String idCompra){


            modelo = Modelo.getInstance();

            DatabaseReference ref = database.getReference("compras/abiertas/"+idCompra);//ruta path
            DatabaseReference refCerradas = database.getReference("compras/cerradas/"+idCompra);//ruta path
            moverRegistro(ref,refCerradas);

            //mListener.ventaCancelada();


        }


    public void moverRegistro(final DatabaseReference  fromPath, final DatabaseReference toPath){
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                modelo = Modelo.getInstance();
                toPath.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError !=null){
                            Log.d("error","Error al mover registros historico");
                        }else{
                            fromPath.removeValue();

                            mListener.ventaCancelada();

                        }
                    }
                });
            }

            @SuppressLint("LongLogTag")
            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.d("moveFirebaseRecord() failed. firebaseError", "error intentando copiar datos cerradas");
            }
        });
    }


    public void actualizarEstadoVenta(String idCompra, String motivo,String justificacion){

        final DatabaseReference ref = database.getReference("compras/cerradas/"+idCompra+"/pedido/1/" );//ruta path

        Map<String, Object> enviarDatos = new HashMap<String, Object>();
        enviarDatos.put("estado","Cancelado");
        enviarDatos.put("motivo", motivo);
        enviarDatos.put("justificacion", justificacion);
        ref.updateChildren(enviarDatos);
    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoCancelarVentaoChangeListener sDummyCallbacks = new OnComandoCancelarVentaoChangeListener()
    {


        @Override
        public void ventaCancelada()
        {}



    };
}
