package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Params;

/**
 * Created by tacto on 4/12/17.
 */

public class ComandoParams {


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo  = Modelo.getInstance();

    public interface OnComandoParamsChangeListener {

        void getParams();
    }

    //interface del listener de la actividad interesada
    private OnComandoParamsChangeListener mListener;

    public ComandoParams(OnComandoParamsChangeListener mListener){

        this.mListener = mListener;
    }





    public void getParams(){
        DatabaseReference ref = database.getReference("params/");//ruta path

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                Params params = new Params();
                modelo.tpurl = snap.child("tpendpoint").getValue().toString();
                modelo.tpprivate = snap.child("tpprivada").getValue().toString();
                modelo.tppublic = snap.child("tppublica").getValue().toString();

                modelo.params.setImpresionesDestacado(((Long) snap.child("impresionesDestacado").getValue()).intValue());

                modelo.params.setTpendpoint(snap.child("tpendpoint").getValue().toString());
                modelo.params.setTpprivada(snap.child("tpprivada").getValue().toString());
                modelo.params.setTppublica(snap.child("tppublica").getValue().toString());


                modelo.params.setValorDestacado(((Long) snap.child("valorDestacado").getValue()).intValue());



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoParamsChangeListener sDummyCallbacks = new OnComandoParamsChangeListener()
    {


        @Override
        public void getParams()
        {}

    };
}
