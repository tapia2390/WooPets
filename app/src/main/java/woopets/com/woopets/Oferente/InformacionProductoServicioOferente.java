package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import android.widget.LinearLayout.LayoutParams;

import java.io.File;

import woopets.com.woopets.Oferente.Comandos.ComandoEditarEliminarEstado;
import woopets.com.woopets.Oferente.Comandos.ComandoGetProducto;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.Oferente.ModelOferente.WizardFragmentos;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.views.SquareImageView;

public class InformacionProductoServicioOferente extends FragmentActivity implements ComandoGetProducto.OnComandoGetProductoChangeListener, ComandoEditarEliminarEstado.OnComandoEditarEliminarEstadoChangeListener {

    ComandoGetProducto comandoGetProducto;
    ComandoEditarEliminarEstado comandoEditarEliminarEstado;
    String uidProducto;
    String abiertascerrada;
    Modelo modelo = Modelo.getInstance();
    final Context context = this;

    TextView txt_categoria, txt_subcategoria, txt_tipo_de_animal, txt_titulo, txt_descripcion, txt_precio, txt_cantiad, txt_tiempo_de_entrega;
    LinearLayout layout_sub_categoria, layout_tiempo_espera, layout_cantidad, contenedor_horario;
    LinearLayout horariolayout, finhorariolayout;
    TextView cierre_etre_semana, cierre_etre_fin_semana;
    TextView textodiassemana, textofindiassemana;
    TextView textodiasfinsemana, textofindiafinssemana;
    Button btn_destacdo;
    TextView txt_tiempo_duracion;
    Button btn_estado;
    TextView txt_activo;
    TextView txt_dir_servicio_info;

    private ViewPager viewPager;
    RelativeLayout contenedor, layautensalada1;
    SquareImageView imegenesEnsalada;
    LinearLayout llDots;
    int indexEnsalada = 0;
    int cantidadVistas = 0;
    int index = 0;
    int posicionServicio = 0;
    private ProgressDialog progressDialog;
    boolean estado = false;
    LinearLayout layout_direccion_servicio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_informacion_producto_servicio_oferente);

        MultiDex.install(this);

        uidProducto = modelo.pro_uidProducto;
        posicionServicio = modelo.pro_posicionServicio;
        abiertascerrada = modelo.pro_abiertascerrada;
        // Toast.makeText(getApplicationContext(), "poisicion " + posicionServicio, Toast.LENGTH_LONG).show();

        txt_categoria = (TextView) findViewById(R.id.txt_categoria);
        txt_subcategoria = (TextView) findViewById(R.id.txt_subcategoria);
        txt_tipo_de_animal = (TextView) findViewById(R.id.txt_tipo_de_animal);
        txt_titulo = (TextView) findViewById(R.id.txt_titulo);
        txt_descripcion = (TextView) findViewById(R.id.txt_descripcion);
        txt_precio = (TextView) findViewById(R.id.txt_precio);
        txt_cantiad = (TextView) findViewById(R.id.txt_cantiad);
        txt_tiempo_de_entrega = (TextView) findViewById(R.id.txt_tiempo_de_entrega);
        layout_sub_categoria = (LinearLayout) findViewById(R.id.layout_sub_categoria);
        layout_tiempo_espera = (LinearLayout) findViewById(R.id.layout_tiempo_espera);
        layout_cantidad = (LinearLayout) findViewById(R.id.layout_cantidad);
        contenedor_horario = (LinearLayout) findViewById(R.id.contenedor_horario);
        horariolayout = (LinearLayout) findViewById(R.id.horariolayout);
        cierre_etre_semana = (TextView) findViewById(R.id.cierre_etre_semana);
        finhorariolayout = (LinearLayout) findViewById(R.id.finhorariolayout);
        cierre_etre_fin_semana = (TextView) findViewById(R.id.cierre_etre_fin_semana);
        textodiassemana = (TextView) findViewById(R.id.textodiassemana);
        textofindiassemana = (TextView) findViewById(R.id.textofindiassemana);
        textodiasfinsemana = (TextView) findViewById(R.id.textodiasfinsemana);
        textofindiafinssemana = (TextView) findViewById(R.id.textofindiafinssemana);
        btn_destacdo = (Button) findViewById(R.id.btn_destacdo);
        txt_tiempo_duracion = (TextView) findViewById(R.id.txt_tiempo_duracion);
        txt_activo = (TextView) findViewById(R.id.txt_activo);
        txt_dir_servicio_info = (TextView) findViewById(R.id.txt_dir_servicio_info);
        btn_estado = (Button) findViewById(R.id.btn_estado);
        imegenesEnsalada = (SquareImageView) findViewById(R.id.imegenesensalada);
        layout_direccion_servicio = (LinearLayout) findViewById(R.id.layout_direccion_servicio);

        llDots = (LinearLayout) findViewById(R.id.llDots);

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        progressDialog = new ProgressDialog(this);


        comandoEditarEliminarEstado = new ComandoEditarEliminarEstado(this);

        comandoGetProducto = new ComandoGetProducto(this);

        if (savedInstanceState != null) {
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


        indexEnsalada = modelo.position;


    }


    @Override
    public void getProductoOferente() {
        productServicio();


    }

    public void productServicio() {
        cantidadVistas = modelo.productosOferentesBoolean.get(0).getFotos().size();
        updateIndicators(indexEnsalada);
        llDots.removeAllViews();
        for (int i = 0; i < new ViewPagerAdapter(getSupportFragmentManager()).getCount(); i++) {
            ImageButton imgDot = new ImageButton(this);
            imgDot.setTag(i);
            imgDot.setImageResource(R.drawable.dot_selector);
            imgDot.setBackgroundResource(0);
            imgDot.setMaxHeight(40);
            imgDot.setMaxWidth(40);
            imgDot.setPadding(5, 5, 5, 5);
            LayoutParams params = new LayoutParams(25, 25);
            imgDot.setLayoutParams(params);

            if (i == indexEnsalada)
                imgDot.setSelected(true);
            llDots.addView(imgDot);
        }

        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        viewPager.setOnPageChangeListener(new WizardPageChangeListener());
        viewPager.setCurrentItem(indexEnsalada);

        //permisos de memoria
        boolean result = Utility.checkPermission(InformacionProductoServicioOferente.this);

        //
        if (modelo.productosOferentesBoolean.get(0).getDestacado() == true) {
            btn_destacdo.setHeight(35);
            btn_destacdo.setWidth(30);
            btn_destacdo.setBackgroundResource(R.drawable.imgdestacadoblanco);
        }

        if (modelo.productosOferentesBoolean.get(0).getActivo() == true) {
            btn_estado.setBackgroundResource(R.drawable.btnactivoazul);
            txt_activo.setText("Activo");
        } else {
            btn_estado.setBackgroundResource(R.drawable.btninactivogris);
            txt_activo.setText("Inactivo");
        }

        //Toast.makeText(getApplicationContext(), "servicio: " + modelo.productosOferentesBoolean.get(0).getServicio(), Toast.LENGTH_LONG).show();

        if (modelo.productosOferentesBoolean.get(0).getServicio() == true) {
            layout_cantidad.setVisibility(View.GONE);
            txt_tiempo_de_entrega.setText("" + modelo.productosOferentesBoolean.get(0).getDuracion() + " " + modelo.productosOferentesBoolean.get(0).getDuracionMedida());

            if (!modelo.productosOferentesBoolean.get(0).getHorarioDiasDeLaSemana().equals("")) {

                textodiassemana.setText(modelo.productosOferentesBoolean.get(0).getHorarioDiasDeLaSemana());
                if (!modelo.productosOferentesBoolean.get(0).getHorarioCierreDiasDeLaSemana().equals("")) {
                    horariolayout.setVisibility(View.VISIBLE);
                    textofindiassemana.setText(modelo.productosOferentesBoolean.get(0).getHorarioInicioDiasDeLaSemana() + " - " + modelo.productosOferentesBoolean.get(0).getHorarioCierreDiasDeLaSemana());
                }

                cierre_etre_semana.setVisibility(View.VISIBLE);

                if (modelo.sinJornadaContinuaSemana == false) {
                    cierre_etre_semana.setText(getString(R.string.jornadacontinua));
                } else {
                    cierre_etre_semana.setText(getString(R.string.cerramosentre));
                }


            }


            if (!modelo.productosOferentesBoolean.get(0).getHorarioDiasFinDeSemana().equals("")) {
                finhorariolayout.setVisibility(View.VISIBLE);

                textodiasfinsemana.setText(modelo.productosOferentesBoolean.get(0).getHorarioDiasFinDeSemana());
                if (!modelo.productosOferentesBoolean.get(0).getHorarioInicioDiasDeLaSemana().equals("")) {
                    textofindiafinssemana.setText(modelo.productosOferentesBoolean.get(0).getHorarioInicioDiasFinDeSemana() + " - " + modelo.productosOferentesBoolean.get(0).getHorarioCierreDiasFinDeSemana());
                }

                cierre_etre_fin_semana.setVisibility(View.VISIBLE);

                if (modelo.sinJornadaContinuaFinDeSemana == false) {
                    cierre_etre_fin_semana.setText(getString(R.string.jornadacontinua));
                } else {
                    cierre_etre_fin_semana.setText(getString(R.string.cerramosentre));
                }
            }

            txt_tiempo_duracion.setText("Duración  servicio");

        }
        if (modelo.productosOferentesBoolean.get(0).getServicio() == false) {
            txt_subcategoria.setText(modelo.productosOferentesBoolean.get(0).getSubcategoria());
            txt_cantiad.setText("" + modelo.productosOferentesBoolean.get(0).getCantidad());
            contenedor_horario.setVisibility(View.GONE);
            layout_tiempo_espera.setVisibility(View.GONE);
            horariolayout.setVisibility(View.GONE);
            finhorariolayout.setVisibility(View.GONE);
            txt_tiempo_duracion.setText("Tiempo de entrega " + "(" + modelo.productosOferentesBoolean.get(0).getDuracionMedida() + ")");

            layout_direccion_servicio.setVisibility(View.GONE);
        }


        txt_categoria.setText(modelo.productosOferentesBoolean.get(0).getCategoria());
        txt_tipo_de_animal.setText(modelo.productosOferentesBoolean.get(0).getTarget());
        txt_titulo.setText(modelo.productosOferentesBoolean.get(0).getTitulo());
        txt_descripcion.setText(modelo.productosOferentesBoolean.get(0).getDescripcion());
        txt_precio.setText("" + Utility.convertToMoney(modelo.productosOferentesBoolean.get(0).getPrecio()));

        if (modelo.productosOferentesBoolean.get(0).getServicioEnDomicilio() == false) {
            txt_dir_servicio_info.setText("Servicio en el local");
        } else {
            txt_dir_servicio_info.setText("Servicio a domicilio");
        }
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the
            // system to himadle the
            // Back button. This calls finish() on this activity and pops the
            // back stack.


            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }


        if (modelo.pro_oferente_vista == 0) {
            modelo.productosOferentesBoolean.clear();
           /* Intent i = new Intent(getApplicationContext(), MisPublicacionesOferente.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);*/
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            finish();
        } else {
            modelo.pro_oferente_vista = 0;
            Intent intent = new Intent(getApplicationContext(), TapsHome.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            finish();
        }

    }


    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private int WIZARD_PAGES_COUNT = cantidadVistas;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new WizardFragmentos(position);
        }

        @Override
        public int getCount() {
            return WIZARD_PAGES_COUNT;
        }

    }

    private class WizardPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int position) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageScrolled(int position, float positionOffset,
                                   int positionOffsetPixels) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageSelected(int position) {
            updateIndicators(position);


            Log.e("", "Page Selected is ===> " + position);
            for (int i = 0; i < new ViewPagerAdapter(getSupportFragmentManager()).getCount(); i++) {
                if (i != position) {
                    ImageView punto = (ImageView) llDots.findViewWithTag(i);
                    if (punto != null) {
                        punto.setSelected(false);
                    }
                } else {

                    ImageView punto = (ImageView) llDots.findViewWithTag(position);
                    if (punto != null) {
                        punto.setSelected(true);
                    }

                }

            }

        }

    }


    public void updateIndicators(int position) {

        index = position;

        layautensalada1 = (RelativeLayout) findViewById(R.id.layautensalada1);


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int resizeValue = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 11, metrics);
        int defaultValue = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 11, metrics);

        layautensalada1.setVisibility(View.VISIBLE);
        layautensalada1.setVisibility(View.VISIBLE);

        final String nombre1;
        final String nombre2;
        final String nombre3;
        nombre2 = modelo.productosOferentesBoolean.get(0).getFotos().get(position);
        nombre3 = "home" + modelo.eliminarTildes(nombre2).replaceAll("\\s", "");
        nombre1 = modelo.eliminarTildes(nombre2).replaceAll("\\s", "").toLowerCase();
        final int resID2 = getResources().getIdentifier(nombre1, "drawable", this.getPackageName()); // This will return an integer value stating the id for the image you want.


        //pruebas gs://ensaladerapruebas.appspot.com
        //produccion gs://ensaladerastaging.appspot.com
        final FirebaseStorage storage = FirebaseStorage.getInstance();
        //final StorageReference storageRef = storage.getReferenceFromUrl("gs://ensaladerapruebas.appspot.com").child("imagenes/ensaladas/"+nombre3+".jpg");
        final StorageReference storageRef2 = storage.getReference().child("productos/" + uidProducto + "/" + nombre2);
        //get download file url
        //get download file url
        storageRef2.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Log.i("Main", "File uri: " + uri.toString());
            }
        });
        try {
            //download the file

            final File localFile = File.createTempFile("images", "jpg");
            //final File localFile =new File("images","jpg");
            localFile.getAbsolutePath();


            storageRef2.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());

                    BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap);
                    imegenesEnsalada.setBackgroundDrawable(ob);
                    // imegenesEnsalada.setImageBitmap(bitmap);

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(getApplicationContext(), "Error al cargar la imagen", Toast.LENGTH_SHORT).show();
                    imegenesEnsalada.setBackgroundResource(R.drawable.btnfoto);

                }
            });
        } catch (Exception e) {
        }
    }


    public void destacados(View v) {

        if (!modelo.datosOferente.getAprobacionMyPet().equals("Aprobado")) {

            showAlertDestacado();

        } else {
            modelo.pro_posicionServicio = 0;
            Intent i = new Intent(getApplicationContext(), InformacionPublicacion.class);
            i.putExtra("idProducto", modelo.productosOferentesBoolean.get(0).getId());
            startActivity(i);

        }


    }

    public void btn_estado(View v) {
        estado = modelo.productosOferentesBoolean.get(0).getActivo();

        if (modelo.productosOferentesBoolean.get(0).getActivo() == true) {
            txt_activo.setText("Inactivo");
            btn_estado.setBackgroundResource(R.drawable.btninactivogris);
            estado = false;
        } else {
            txt_activo.setText("Activo");
            btn_estado.setBackgroundResource(R.drawable.btnactivoazul);
            estado = true;
        }


        comandoEditarEliminarEstado.setEstadoPublicacion(estado, modelo.productosOferentesBoolean.get(0).getId());
    }


    public void btn_eliminar(View v) {
        progressDialog.setMessage("Validando la información, por favor espere...");
        progressDialog.show();
        comandoEditarEliminarEstado.validarComprasAbiertasCerradas("abiertas", modelo.productosOferentesBoolean.get(0).getId());

    }


    public void btnEditar(View v) {

        final CharSequence[] items = {"Tipo", "Artículo", "Fotos", "Cancelar"};
        AlertDialog.Builder builder = new AlertDialog.Builder(InformacionProductoServicioOferente.this);

        TextView title = new TextView(InformacionProductoServicioOferente.this);
        title.setText("¡Editar Publicacion! ¿Que información de tu publicación deseas editar?");
        title.setGravity(Gravity.CENTER);
        // title.setTextSize(12);
        // title.setBackgroundColor(Color.GRAY);
        title.setTextColor(Color.BLACK);

        builder.setCustomTitle(title);

        // builder.setTitle(getString(R.string.alarmaetapatatitulo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                //Toast toast = Toast.makeText(getApplicationContext(), "Haz elegido la opcion: " + items[item], Toast.LENGTH_SHORT);
                //toast.show();
                dialog.cancel();

                if (items[item].equals("Tipo")) {

                    Intent i = new Intent(getApplicationContext(), MyPublicacionesTipo.class);
                    modelo.publicacion_estado = 1;
                    modelo.pro_posicionServicio = 0;
                    startActivity(i);

                } else if (items[item].equals("Artículo")) {
                    modelo.horarioMyPublicacionesArticulo = "";
                    modelo.servicio_producto = modelo.productosOferentesBoolean.get(0).getServicio();
                    Intent i = new Intent(getApplicationContext(), MyPublicacionesArticulo.class);
                    modelo.publicacion_estado = 1;
                    modelo.pro_posicionServicio = 0;
                    startActivity(i);


                } else if (items[item].equals("Fotos")) {

                    Intent i = new Intent(getApplicationContext(), MyPublicacionesFoto.class);
                    modelo.publicacion_estado = 1;
                    modelo.pro_posicionServicio = 0;
                    startActivity(i);

                } else if (items[item].equals("Cancelar")) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void btnEditar2(View v) {

        final CharSequence[] items = {"Tipo", "Artículo", "Fotos", "Cancelar"};

        AlertDialog.Builder builder = new AlertDialog.Builder(InformacionProductoServicioOferente.this);
        builder.setTitle("¡Editar Publicacion! \n ¿Que información de tu publicación deseas editar?");
        AlertDialog.Builder cancelar = builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                boolean result = Utility.checkPermission(InformacionProductoServicioOferente.this);
                if (items[item].equals("Tipo")) {

                    Intent i = new Intent(getApplicationContext(), MyPublicacionesTipo.class);
                    modelo.publicacion_estado = 1;
                    modelo.pro_posicionServicio = 0;
                    startActivity(i);

                } else if (items[item].equals("Artículo")) {
                    modelo.horarioMyPublicacionesArticulo = "";
                    modelo.servicio_producto = modelo.productosOferentesBoolean.get(0).getServicio();
                    Intent i = new Intent(getApplicationContext(), MyPublicacionesArticulo.class);
                    modelo.publicacion_estado = 1;
                    modelo.pro_posicionServicio = 0;
                    startActivity(i);


                } else if (items[item].equals("Fotos")) {

                    Intent i = new Intent(getApplicationContext(), MyPublicacionesFoto.class);
                    modelo.publicacion_estado = 1;
                    modelo.pro_posicionServicio = 0;
                    startActivity(i);

                } else if (items[item].equals("Cancelar")) {
                    dialog.dismiss();
                }
            }


        });
        builder.show();
    }


    @Override
    public void cargoAbiertas() {
        validacion();
    }

    @Override
    public void cargoCerradas() {

        validacion();
    }

    @Override
    public void cargoTipo() {

    }

    @Override
    public void cargoArticulo() {

    }

    @Override
    public void cargoFoto() {

    }


    @Override
    public void cargoEstado() {


        if (modelo.productosOferentesBoolean.get(0).getActivo() == true) {
            btn_estado.setBackgroundResource(R.drawable.btnactivoazul);
            txt_activo.setText("Activo");
        } else {
            btn_estado.setBackgroundResource(R.drawable.btninactivogris);
            txt_activo.setText("Inactivo");
        }
    }


    public void validacion() {
        int abietas = 0;
        int cerradas = 0;

        for (int i = 0; i <= modelo.pedidoAbiertas.size() - 1; i++) {
            if (modelo.pedidoAbiertas.get(i).getId().equals(modelo.productosOferentesBoolean.get(0).getId())) {
                abietas++;
            }

        }

        for (int i = 0; i <= modelo.pedidoCerardas.size() - 1; i++) {
            if (modelo.pedidoAbiertas.get(i).getId().equals(modelo.productosOferentesBoolean.get(0).getId())) {
                cerradas++;
            }

        }


        if (abietas > 0 || cerradas > 0) {
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "No puede eliminar el producto o servicio", Toast.LENGTH_LONG).show();
        } else {
            comandoEditarEliminarEstado.setEliminar(modelo.productosOferentesBoolean.get(0).getId());
        }
    }

    @Override
    public void cargoEliminarPublicacion() {
        Toast.makeText(getApplicationContext(), " Producto o servicio eliminado...", Toast.LENGTH_LONG).show();
        Intent i = new Intent(getApplicationContext(), TapsHome.class);
        startActivity(i);
        progressDialog.dismiss();

    }


    public void showAlertDestacado() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("Advertencia");

        // set dialog message
        alertDialogBuilder
                .setMessage("Podrás destacar tu publicación cuando seas aprobado")
                .setCancelable(false)
                .setPositiveButton("OK, entendido", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public void onSaveInstanceState(Bundle outState) {
        outState.putString("UID", modelo.uid);
        outState.putInt("indexEnsalada", indexEnsalada);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onStart() {
        super.onStart();

        comandoGetProducto.getProducto(uidProducto, modelo.pro_abiertascerrada);
    }
}
