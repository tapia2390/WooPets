package woopets.com.woopets.Oferente.Fragment;

/**
 * Created by tacto on 3/07/17.
 */

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import woopets.com.woopets.Comandos.ComandoCategorias;
import woopets.com.woopets.MainActivity;
import woopets.com.woopets.Modelo.Categorias;
import woopets.com.woopets.Oferente.Comandos.ComandoComprasAbiertasCerradas;
import woopets.com.woopets.Oferente.Comandos.ComandoComprasTodas;
import woopets.com.woopets.Oferente.Comandos.ComandoDatosCelular;
import woopets.com.woopets.Oferente.Comandos.ComandoEditarOferente;
import woopets.com.woopets.Oferente.Comandos.ComandoGeoFire;
import woopets.com.woopets.Oferente.Comandos.ComandoParams;
import woopets.com.woopets.Oferente.Comandos.ComandoPreguntaPublicaciones;
import woopets.com.woopets.Oferente.Comandos.ComandoRegistroProducto;
import woopets.com.woopets.Oferente.Comandos.ComandoRegistroProductoTodas;
import woopets.com.woopets.Oferente.ComprasAbiertasCerradas;
import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntasFrecuentes;
import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.ClassTerminosYCondiciones;
import woopets.com.woopets.Oferente.ModelOferente.CompraProductoAbiertaCerrada;
import woopets.com.woopets.Oferente.ModelOferente.DatosOferente;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.EditarPerfilOferente;
import woopets.com.woopets.Oferente.LoguinOferente;
import woopets.com.woopets.Oferente.MisPublicacionesOferente;
import woopets.com.woopets.Oferente.ModelOferente.NotificaionesOferente;
import woopets.com.woopets.Oferente.ModelOferente.Params;
import woopets.com.woopets.Oferente.ModelOferente.Pedido;
import woopets.com.woopets.Oferente.ModelOferente.PreguntasPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;
import woopets.com.woopets.Oferente.ModelOferente.Tarjetas;
import woopets.com.woopets.Oferente.PreguntasFrecuentes;
import woopets.com.woopets.Oferente.Preguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.TerminosYCondiciones;
import woopets.com.woopets.R;

public class MyGuarderia extends Fragment implements ComandoEditarOferente.OnComandoRegistroOferenteChangeListener, ComandoDatosCelular.OnComandoDatosCelularChangeListener, ComandoParams.OnComandoParamsChangeListener, ComandoComprasAbiertasCerradas.OnComandoComprasAbiertasCerradasoChangeListener, ComandoPreguntaPublicaciones.OnComandoPreguntaPublicacionesChangeListener, ComandoComprasTodas.OnComandoComprasAbiertasCerradasoChangeListener, ComandoRegistroProductoTodas.OnComandoRegistroProductChangeListener, ComandoCategorias.OnComandoCategoriasChangeListener {

    private static final String TAG = "Tab1Fragment";
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser user = mAuth.getCurrentUser();
    private Modelo modelo = Modelo.getInstance();

    private LinearLayout layoutcerrarceccion;
    private LinearLayout layoutmispublicaciones;
    private LinearLayout layouteditarperfil;
    private LinearLayout layoutmisventas;
    private LinearLayout layoutpreguntasmispublicaciones;
    private LinearLayout layoutpreguntasfrecuentes;
    private LinearLayout terminos_condiciones;
    private Button reloj_arena;
    private Button hea1;
    private Button btn_pmp;
    private Button btn_pmv;
    private TextView txtversion;
    String uid;


    ComandoRegistroProductoTodas comandoRegistroProducto;
    ComandoEditarOferente comandoEditarOferente;
    ComandoDatosCelular comandoDatosCelular;
    ComandoComprasAbiertasCerradas comandoComprasAbiertasCerradas;
    ComandoPreguntaPublicaciones comandoPreguntaPublicaciones;
    ComandoParams comandoParams;
    ComandoComprasTodas comandoComprasTodas;
    ComandoCategorias comandoCategorias;
    int preguntasPublicaciones = 0;
    int misVentas = 0;

    PackageInfo pInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.myguarderia, container, false);
        /// Toast.makeText(getActivity(), "TESTING BUTTON CLICK 1",Toast.LENGTH_SHORT).show();



        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        //datos horario inicioç

        //variables del horario  oferente
        modelo.horariodesemanaIni = "";
        modelo.horariodesemanaFin = "";
        modelo.horariofindesemanaIni = "";
        modelo.horariofindesemanaFin = "";
        modelo.latitud = 0.0;
        ;
        modelo.longitud = 0.0;
        modelo.sinJornadaContinuaSemana = false;
        modelo.sinJornadaContinuaFinDeSemana = false;

        //variables del horario al registrar un articulo Oferente
        modelo.diasDeLaSemanaArticulo.clear();
        modelo.finDeSemanaFestivosArticulo.clear();
        modelo.horariodesemanaIniArticulo = "";
        modelo.horariodesemanaFinArticulo = "";
        modelo.horariofindesemanaIniArticulo = "";
        modelo.horariofindesemanaFinArticulo = "";
        modelo.latitudArticulo = 0.0;
        ;
        modelo.longitudArticulo = 0.0;
        ;
        modelo.sinJornadaContinuaSemanaArticulo = false;
        modelo.sinJornadaContinuaFinDeSemanaArticulo = false;

        // fin

        modelo.diasDeLaSemana.clear();
        modelo.finDeSemanaFestivos.clear();
        //cerrar seccion
        layoutcerrarceccion = (LinearLayout) view.findViewById(R.id.layoutcerrarceccion);
        layoutmispublicaciones = (LinearLayout) view.findViewById(R.id.layoutmispublicaciones);
        layouteditarperfil = (LinearLayout) view.findViewById(R.id.layouteditarperfil);
        layoutmisventas = (LinearLayout) view.findViewById(R.id.layoutmisventas);
        layoutpreguntasmispublicaciones = (LinearLayout) view.findViewById(R.id.layoutpreguntasmispublicaciones);
        layoutpreguntasfrecuentes = (LinearLayout) view.findViewById(R.id.layoutpreguntasfrecuentes);
        terminos_condiciones = (LinearLayout) view.findViewById(R.id.terminos_condiciones);
        reloj_arena = (Button) view.findViewById(R.id.reloj_arena);
        hea1 = (Button) view.findViewById(R.id.hea1);
        btn_pmp = (Button) view.findViewById(R.id.btn_pmp);
        btn_pmv = (Button) view.findViewById(R.id.btn_pmv);

        txtversion = (TextView) view.findViewById(R.id.txtversion);
        txtversion.setText("V- "+pInfo.versionName + " ("+ pInfo.versionCode + ")");

        borrarTipoSharedPreferenceOferente();
        modelo.limiarDatos();


        comandoRegistroProducto = new ComandoRegistroProductoTodas(this);
        comandoRegistroProducto.getProducto(true);
        comandoRegistroProducto.getProductoInactivo(false);

        comandoEditarOferente = new ComandoEditarOferente(this);
        if(modelo.primercarga == 0){
            modelo.primercarga = 1;
            comandoEditarOferente.getDatosOferente();
        }




        String token = FirebaseInstanceId.getInstance().getToken();
        // Toast.makeText(getActivity(),"token"+ token, Toast.LENGTH_LONG).show();
        cargarSharedPreferencePreguntasPublicaciones();


        if (modelo.uid.equals("")) {

            Toast.makeText(getActivity(), "Error con el  logueo por el usuario esta vacio o null", Toast.LENGTH_SHORT).show();
        } else {
            comandoDatosCelular = new ComandoDatosCelular(this);
            comandoDatosCelular.setDatosCelular(token);

        }


        // comandos
        comandoParams = new ComandoParams(this);
        comandoParams.getParams();


        comandoComprasAbiertasCerradas = new ComandoComprasAbiertasCerradas(this);
        comandoComprasAbiertasCerradas.comprasAbiertasCerradas_(modelo.uid, "abiertas");


        comandoPreguntaPublicaciones = new ComandoPreguntaPublicaciones(this);


        comandoComprasTodas = new ComandoComprasTodas(this);
        comandoComprasTodas.comprasAbiertas("abiertas");
        comandoComprasTodas.comprasCerradas("cerradas");

        comandoCategorias = new ComandoCategorias(this);
        comandoCategorias.getCategorias();


        layoutcerrarceccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                // Setting Alert Dialog Title
                alertDialogBuilder.setTitle("¡Atención!");
                // Icon Of Alert Dialog
                // alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
                // Setting Alert Dialog Message
                alertDialogBuilder.setMessage("¿Seguro desea cerrar sesión?");
                alertDialogBuilder.setCancelable(false);

                alertDialogBuilder.setPositiveButton("Si, Cerrar", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.v("ok", "ok");
                        Toast.makeText(getActivity(), "Cerrando Sesión...", Toast.LENGTH_SHORT).show();

                        user = null;
                        modelo.uid = "";
                        mAuth.signOut();
                        modelo.pararListeners();
                        limpiarDatos();
                        modelo.limiarDatos();
                        Intent i = new Intent(getActivity(), MainActivity.class);
                        startActivity(i);
                    }
                });

                alertDialogBuilder.setNegativeButton("No, Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(getApplicationContext(),"You clicked over No",Toast.LENGTH_SHORT).show();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        //mis publicaciones
        layoutmispublicaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MisPublicacionesOferente.class);
                startActivity(i);
            }
        });


        //editar perfil
        layouteditarperfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), EditarPerfilOferente.class);
                startActivity(i);
            }
        });

        // compras abiertas y cerradas
        layoutmisventas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ComprasAbiertasCerradas.class);
                startActivity(i);
            }
        });

        // preguntas a mis publicaciones
        layoutpreguntasmispublicaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Preguntas_A_MisPublicaciones.class);
                startActivity(i);
            }
        });

        //preguntas frecuentes
        layoutpreguntasfrecuentes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), PreguntasFrecuentes.class);
                startActivity(i);
            }
        });

        //terminos y condiciones
        terminos_condiciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), TerminosYCondiciones.class);
                startActivity(i);
            }
        });


        //reloj de arena
        reloj_arena.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                // Setting Alert Dialog Title
                alertDialogBuilder.setTitle("Formulario en validación");
                // Icon Of Alert Dialog
                // alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
                // Setting Alert Dialog Message
                alertDialogBuilder.setMessage("Mientras tus datos son validados, puedes crear publicaciones que serán mostrados a todos los usuarios WooPets cuando seas aprobado ");
                alertDialogBuilder.setCancelable(false);

                alertDialogBuilder.setPositiveButton("OK, ENTENDIDO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.v("ok", "ok");
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });


        return view;
    }



    @Override
    public void informacionRegistroOferente() {

    }

    @Override
    public void errorInformacionRegistroOferente() {

    }

    @Override
    public void cargoDatosOferente() {
        hea1.setText("" + modelo.datosOferente.getRazonSocial());
        if (!modelo.datosOferente.getAprobacionMyPet().equals("Aprobado")) {
            reloj_arena.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void cargoDatosCelular() {

        Log.v("TokenTag", "Datos enviados");

    }



    @Override
    public void getParams() {
        Log.v("TokenTag", "ok tpaga");
    }


    public void limpiarDatos() {
        modelo.abiertaCerrada = "";
        modelo.posicionServicio = 0;
        modelo.idCliente = "";

        //datos detallados de un producto editar,cancelar,eliminar
        modelo.publicacion_estado = 0;
        modelo.pro_uidProducto = "";
        modelo.pro_posicionServicio = 0;
        modelo.pro_abiertascerrada = "";
        modelo.idPreguntasClientePublicaciones = "";
        modelo.idPreguntasClientePublicacionesPosicion = 0;
        ;

        modelo.uid = "";
        modelo.idClienteTpaga = "";

        //producto oferente

        //variables del horario al registrar un  Oferente
        modelo.diasDeLaSemana.clear();
        modelo.finDeSemanaFestivos.clear();

        //variables del horario  oferente
        modelo.horariodesemanaIni = "";
        modelo.horariodesemanaFin = "";
        modelo.horariofindesemanaIni = "";
        modelo.horariofindesemanaFin = "";
        modelo.latitud = 0.0;
        modelo.longitud = 0.0;
        modelo.sinJornadaContinuaSemana = false;
        modelo.sinJornadaContinuaFinDeSemana = false;

        modelo.servicio_producto = false;
        //variables del horario al registrar un articulo Oferente
        modelo.diasDeLaSemanaArticulo.clear();
        modelo.finDeSemanaFestivosArticulo.clear();
        modelo.horariodesemanaIniArticulo = "";
        modelo.horariodesemanaFinArticulo = "";
        modelo.horariofindesemanaIniArticulo = "";
        modelo.horariofindesemanaFinArticulo = "";
        modelo.latitudArticulo = 0;
        modelo.longitudArticulo = 0;
        modelo.sinJornadaContinuaSemanaArticulo = false;
        modelo.sinJornadaContinuaFinDeSemanaArticulo = false;

        //variables para la opcion de selecionar una categorias
        modelo.selectImge = false;
        modelo.selectPosition = -1;
        modelo.selectPositionAnterior = 0;
        modelo.position = 0;
        modelo.versionName = "";

        //tpaga
        modelo.tpprivate = "";
        modelo.tppublic = "";
        modelo.tpurl = "";

        //datos activacion destacado
        modelo.dActivo = false;
        modelo.name_destacado = "";
        modelo.uidProducto = "";
        modelo.valodDestacado = 0;

        //arraylist Oferente

        modelo.productosOferentes.clear();
        modelo.listaProductosOferentes.clear();
        modelo.productosOferentesInactivas.clear();
        modelo.comprasProductosAbiertasCerradases.clear();
        modelo.comprasProductosAbiertasCerradases2.clear();
        modelo.listatadoClintesComprasProducto.clear();
        modelo.preguntasPublicaciones.clear();
        modelo.classpreguntasFrecuentes.clear();
        modelo.classPreguntas_a_misPublicaciones.clear();
        modelo.classPreguntas_a_misPublicacionesDetalle.clear();
        modelo.listaNotifications.clear();

        modelo.tarjetas.clear();

        modelo.pedidoAbiertas.clear();
        modelo.pedidoCerardas.clear();

        //arraylist usuario

    }


    //Prefences
    private void cargarSharedPreferencePreguntasPublicaciones() {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferenciasNotificacion", Context.MODE_PRIVATE);

        preguntasPublicaciones = sharedPreferences.getInt("preguntasPublicaciones", 0);
        misVentas = sharedPreferences.getInt("misVentas", 0);

    }


    public void guardarSharedPreferencepreguntasPublicaciones() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferenciasNotificacion", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("preguntasPublicaciones", 2);
        editor.putInt("misVentas", 3);

        editor.commit();
    }

    @Override
    public void comprasAbiertasCerradasoOferente() {

        int resultadoNotificacionVenta = 0;


        Set<CompraProductoAbiertaCerrada> quipu = new HashSet<CompraProductoAbiertaCerrada>(modelo.comprasProductosAbiertasCerradases2);
        for (CompraProductoAbiertaCerrada key : quipu) {
            System.out.println(key + " : " + Collections.frequency(modelo.comprasProductosAbiertasCerradases2, key));
        }

        resultadoNotificacionVenta = modelo.comprasProductosAbiertasCerradases2.size() - misVentas;


        if (resultadoNotificacionVenta != 0) {
            btn_pmv.setVisibility(View.VISIBLE);
            btn_pmv.setText("" + resultadoNotificacionVenta);
        }

        if (resultadoNotificacionVenta == 0) {
            btn_pmv.setVisibility(View.GONE);
        }
        //Toast.makeText(getActivity(),"-..-"+modelo.comprasProductosAbiertasCerradases2.size(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void preguntasMisPublicaciones() {



        int resultadoNotificacionPreguntas = 0;
        int cantidad = 0;

        Set<ClassPreguntas_A_MisPublicaciones> quipu = new HashSet<ClassPreguntas_A_MisPublicaciones>(modelo.classPreguntas_a_misPublicaciones);
        for (ClassPreguntas_A_MisPublicaciones key : quipu) {

            System.out.println(key + " : " + Collections.frequency(modelo.classPreguntas_a_misPublicaciones, key));

        }

        modelo.adicionarListaPreguntasSinResponder();
        resultadoNotificacionPreguntas = modelo.preguntas_publicaiones.size();

        if (resultadoNotificacionPreguntas != 0) {
            btn_pmp.setVisibility(View.VISIBLE);
            btn_pmp.setText("" + resultadoNotificacionPreguntas);
        }

        if (resultadoNotificacionPreguntas == 0) {
            btn_pmp.setVisibility(View.GONE);
        }

    }

    @Override
    public void preguntasMisPublicacionesNotificacion() {




    }


    //borrar datos articulo
    public void borrarTipoSharedPreferenceOferente() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferencias articulos", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("txt_titulo", "");
        editor.putString("txt_descripcion_art", "");
        editor.putString("txt_precio", "");
        editor.putString("textodiassemana", "");
        editor.putString("textofindiassemana", "");
        editor.putString("cierre_etre_semana", "");
        editor.putString("textodiasfinsemana", "");
        editor.putString("textofindiafinssemana", "");
        editor.putString("cierre_etre_fin_semana", "");
        editor.putString("txt_hora_servicios", "");
        editor.putString("txt_minutos_servicios", "");
        editor.putString("txt_cantidad", "");

        editor.commit();
    }


    @Override
    public void comprasTodas() {
        int ventasAbiertas =  modelo.comprasProductosAbiertas.size();
        int ventasaCerradas =  modelo.comprasProductosCerradas.size();
    }

    @Override
    public void registroProductoOferente() {

    }

    @Override
    public void getProductoOferente() {

    }

    @Override
    public void usuarioNoregistardo() {
        Toast.makeText(getActivity(),"Usuario incorrecto.",Toast.LENGTH_SHORT).show();
        user = null;
        modelo.uid = "";
        mAuth.signOut();
        modelo.pararListeners();
        limpiarDatos();
        modelo.limiarDatos();
        Intent i = new Intent(getActivity(), MainActivity.class);
        startActivity(i);
    }

    @Override
    public void cargoCategorias() {

    }

    @Override
    public void cargoTipoUsuario(String tipo) {

    }

    @Override
    public void actualizoSistema() {

    }


    @Override
    public void onStart() {
        super.onStart();

        if (modelo.uid.equals("")) {
            modelo.uid = uid;
        }
        comandoPreguntaPublicaciones.getPreguntasPublicaciones();
    }


    //Prefences
    private void cargarSharedPreferenceUID() {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferenciasUID", Context.MODE_PRIVATE);

        uid = sharedPreferences.getString("uid", "");

    }
}
