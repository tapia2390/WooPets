package woopets.com.woopets.Oferente.Comandos;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 11/12/17.
 */

public class ComandoComfirmarReprogrmarChat {


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo  = Modelo.getInstance();

    public interface OnComandoComfirmarReprogrmarChatoChangeListener {

        void cmConfirmar();
        void cmReprogramar();
        void cmChat();
    }


    //interface del listener de la actividad interesada
    private ComandoComfirmarReprogrmarChat.OnComandoComfirmarReprogrmarChatoChangeListener mListener;

    public ComandoComfirmarReprogrmarChat(ComandoComfirmarReprogrmarChat.OnComandoComfirmarReprogrmarChatoChangeListener mListener){

        this.mListener = mListener;
    }


    public void confimar(String idCompra){

        final DatabaseReference ref = database.getReference("compras/abiertas/"+idCompra+"/pedido/1/estado" );//ruta path
        ref.setValue("Confirmada");
        mListener.cmConfirmar();


    }


    public void reprogramar(String idCompra, final  String fecha_hora){
        final DatabaseReference ref = database.getReference("compras/abiertas/"+idCompra+"/pedido/1/estado" );//ruta path
        ref.setValue("Reprogramada");

        final DatabaseReference ref2 = database.getReference("compras/abiertas/"+idCompra+"/pedido/1/reprogramada" );//ruta path
        ref2.setValue(true);

        final DatabaseReference ref3 = database.getReference("compras/abiertas/"+idCompra+"/pedido/1/fechaServicio" );//ruta path
        ref3.setValue(""+fecha_hora);


        mListener.cmReprogramar();
    }


    public void  reprogramarNotification(final String nomProducto, final  String fechaReprogramado) {

        DatabaseReference key = referencia.push();
        DatabaseReference ref = database.getReference("mensajes/" + key.getKey() + "/");

        Map<String, Object> reprogramar = new HashMap<String, Object>();
        reprogramar.put("idCliente", modelo.idCliente);
        reprogramar.put("idCompra", modelo.idCompara);
        reprogramar.put("infoAdicional", nomProducto);
        reprogramar.put("mensaje", "Tu servicio "+nomProducto+" ha sido reprogramado para el "+fechaReprogramado+"");
        reprogramar.put("timestamp", ServerValue.TIMESTAMP);
        reprogramar.put("tipo", "compraReprogramada");
        reprogramar.put("titulo", "Servicio reprogramado");

        String token = modelo.token;
        if (token == null || token.equals("")) {
            token = "1";
        }
        Map<String, Object> tokens = new HashMap<String, Object>();
        tokens.put(token, true);
        reprogramar.put("tokens", tokens);

        reprogramar.put("visto", false);
        ref.updateChildren(reprogramar);
    }




    public void  confirmadoNotification(final String nomProducto) {

        DatabaseReference key = referencia.push();
        DatabaseReference ref = database.getReference("mensajes/" + key.getKey() + "/");

        Map<String, Object> confirmar = new HashMap<String, Object>();
        confirmar.put("idCliente", modelo.idCliente);
        confirmar.put("idCompra", modelo.idCompara);
        confirmar.put("infoAdicional", nomProducto);
        confirmar.put("mensaje", "Tu servicio "+nomProducto+" ha sido confirmado");
        confirmar.put("timestamp", ServerValue.TIMESTAMP);
        confirmar.put("tipo", "compraConfirmada");
        confirmar.put("titulo", "Servicio confirmado");

        String token = modelo.token;
        if (token == null || token.equals("")) {
            token = "1";
        }
        Map<String, Object> tokens = new HashMap<String, Object>();
        tokens.put(token, true);
        confirmar.put("tokens", tokens);

        confirmar.put("visto", false);
        ref.updateChildren(confirmar);
    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoComfirmarReprogrmarChatoChangeListener sDummyCallbacks = new OnComandoComfirmarReprogrmarChatoChangeListener()
    {


        @Override
        public void cmConfirmar()
        {}


        @Override
        public void cmReprogramar()
        {}


        @Override
        public void cmChat()
        {}



    };
}
