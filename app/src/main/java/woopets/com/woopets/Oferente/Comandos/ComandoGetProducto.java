package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.ProductosOferente;

/**
 * Created by tacto on 23/08/17.
 */

public class ComandoGetProducto {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    Modelo modelo  = Modelo.getInstance();

    public interface OnComandoGetProductoChangeListener {

        void getProductoOferente();
    }

    //interface del listener de la actividad interesada
    private OnComandoGetProductoChangeListener mListener;

    public ComandoGetProducto(OnComandoGetProductoChangeListener mListener){

        this.mListener = mListener;
    }



    public void getProducto(final String uidProducto, final String inactivo){
        modelo.productosOferentes.clear();
        modelo.productosOferentes2.clear();
        modelo.productosOferentesInactivas.clear();
        DatabaseReference ref = database.getReference("productos/"+uidProducto+"/");//ruta path

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                ProductosOferente productosOferente = new ProductosOferente();


                productosOferente.setId(snap.getKey());
                boolean activo =  (boolean) snap.child("activo").getValue();

                productosOferente.setActivo(activo);
                productosOferente.setCategoria(snap.child("categoria").getValue().toString());


                productosOferente.setDescripcion(snap.child("descripcion").getValue().toString());

                if(snap.child("destacado").getValue() != null){
                    boolean destacado =  (boolean) snap.child("destacado").getValue();
                    productosOferente.setDestacado(destacado);
                }

                boolean servicio =  (boolean) snap.child("servicio").getValue();

                if(servicio == true){
                    //nuevo arbol
                    DataSnapshot snaphorario;
                    snaphorario = (DataSnapshot) snap.child("horario");

                    //nuevo subarbol
                    DataSnapshot snapFinDeSemana;
                    snapFinDeSemana = (DataSnapshot) snaphorario.child("FinDeSemana");

                    if(snapFinDeSemana.child("dias").getValue() != null){

                        if(!snapFinDeSemana.child("dias").getValue().toString().equals("")){

                            String dias = snapFinDeSemana.child("dias").getValue().toString();
                            ArrayList<String> myList = new ArrayList<String>(Arrays.asList(dias.split(",")));
                            modelo.finDeSemanaFestivos = myList;

                        }
                        modelo.horariofindesemanaIni =  snapFinDeSemana.child("horaInicio").getValue().toString();
                        modelo.horariodesemanaFin = snapFinDeSemana.child("horaCierre").getValue().toString();

                        productosOferente.setHorarioDiasFinDeSemana(snapFinDeSemana.child("dias").getValue().toString());
                        productosOferente.setHorarioCierreDiasFinDeSemana(snapFinDeSemana.child("horaCierre").getValue().toString());
                        productosOferente.setHorarioInicioDiasFinDeSemana(snapFinDeSemana.child("horaInicio").getValue().toString());

                    }


                    if(snapFinDeSemana.child("sinJornadaContinua").getValue() !=null){
                        boolean sinJornadaContinuaFinSemana = (boolean) snapFinDeSemana.child("sinJornadaContinua").getValue();
                        productosOferente.setSinJornadaContinuaFinDeSemana(sinJornadaContinuaFinSemana);
                        modelo.sinJornadaContinuaFinDeSemana = sinJornadaContinuaFinSemana;

                    }


                    //fin subarbol
                    //nuevo subarbol
                    DataSnapshot snapSemana;
                    snapSemana = (DataSnapshot) snaphorario.child("Semana");
                    if(snapSemana.child("dias").getValue() != null ){

                        if(!snapSemana.child("dias").getValue().toString().equals("")){
                            String dias = snapSemana.child("dias").getValue().toString();
                            ArrayList<String> myList = new ArrayList<String>(Arrays.asList(dias.split(",")));
                            modelo.diasDeLaSemana = myList;

                        }
                        modelo.horariodesemanaIni = snapSemana.child("horaInicio").getValue().toString();
                        modelo.horariodesemanaFin = snapSemana.child("horaCierre").getValue().toString();


                        productosOferente.setHorarioDiasDeLaSemana(snapSemana.child("dias").getValue().toString());
                        productosOferente.setHorarioCierreDiasDeLaSemana(snapSemana.child("horaCierre").getValue().toString());
                        productosOferente.setHorarioInicioDiasDeLaSemana(snapSemana.child("horaInicio").getValue().toString());


                        if(snapSemana.child("sinJornadaContinua").getValue() !=null){
                            boolean sinJornadaContinuaSemana = (boolean) snapSemana.child("sinJornadaContinua").getValue();
                            productosOferente.setSinJornadaContinuaSemana(sinJornadaContinuaSemana);
                            modelo.sinJornadaContinuaSemana  = sinJornadaContinuaSemana;

                        }

                    }


                    //fin subarbol
                    //fin arbol

                    productosOferente.setDuracion(((Long) snap.child("duracion").getValue()).intValue());
                    productosOferente.setDuracionMedida(snap.child("duracionMedida").getValue().toString());

                    boolean servicioDomicilio = (boolean) snap.child("servicioEnDomicilio").getValue();
                    productosOferente.setServicioEnDomicilio(servicioDomicilio);


                }

                if(servicio == false){
                    if(snap.child("subcategoria").getValue() != null ){
                        productosOferente.setSubcategoria(snap.child("subcategoria").getValue().toString());

                    }
                    if(snap.hasChild("stock")){
                        productosOferente.setCantidad(((Long) snap.child("stock").getValue()).intValue());
                    }

                }
                productosOferente.setServicio(servicio);

                //nuevo arbol
                DataSnapshot snapFotos;
                snapFotos = (DataSnapshot) snap.child("fotos/");
                ArrayList<String> fotosProductosOferentes = new ArrayList<String>();
                for (DataSnapshot foto : snapFotos.getChildren()) {
                    fotosProductosOferentes.add(foto.getValue().toString());
                }

                productosOferente.setFotos(fotosProductosOferentes);

                productosOferente.setIdOferente(snap.child("idOferente").getValue().toString());
                productosOferente.setTitulo(snap.child("nombre").getValue().toString());
                productosOferente.setPrecio(snap.child("precio").getValue().toString());
                productosOferente.setTarget(snap.child("target").getValue().toString());

                if(inactivo.equals("abiertas")){
                    modelo.productosOferentes.add(productosOferente);
                }
                else{
                    modelo.productosOferentes2.add(productosOferente);
                }

                if(modelo.activa_inactiva == true){
                    modelo.productosOferentesBoolean = modelo.productosOferentes;

                }else{
                    modelo.productosOferentesBoolean = modelo.productosOferentes2;

                }


                if (modelo.productosOferentesBoolean.size() > 0){
                    mListener.getProductoOferente();
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoGetProductoChangeListener sDummyCallbacks = new OnComandoGetProductoChangeListener()
    {


        @Override
        public void getProductoOferente()
        {}

    };
}
