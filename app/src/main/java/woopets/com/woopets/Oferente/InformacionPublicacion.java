package woopets.com.woopets.Oferente;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.internal.Util;
import woopets.com.woopets.Oferente.Comandos.ComandoActualizacionProductoOferente;
import woopets.com.woopets.Oferente.ModelOferente.InformacionDestacado;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;
import woopets.com.woopets.Oferente.ModelOferente.Utility;
import woopets.com.woopets.R;
import woopets.com.woopets.Splash;
import woopets.com.woopets.cliente.views.SquareImageView;

public class InformacionPublicacion extends Activity {

    String uidProducto;
    String abiertascerrada;
    int posicionServicio = 0;

    String userChoosenTask = "";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    static final int REQUEST_TAKE_PHOTO = 3;

    Bitmap bm = null;
    File destination = null;
    SquareImageView foto_destacado;
    Uri uriSavedImage1;

    LinearLayout layout_info_des;
    Button btn_examinar;
    TextView txt_titulo;
    TextView txt_precio;
    TextView texthead;
    Button btn_head;
    Button button11;
    Button buttoneditar;
    TextView tex_body;
    RelativeLayout relative_check;
    CheckBox check_destacado;
    Modelo modelo = Modelo.getInstance();
    private ProgressDialog progressDialog;
    final Context context = this;

    String mCurrentPhotoPath = "";
    Uri photoURI = null;
    String ruta1 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_informacion_publicacion);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }

        Bundle bundle2 = getIntent().getExtras();
        uidProducto = bundle2.getString("idProducto");
        posicionServicio = bundle2.getInt("posicionServicio");
        abiertascerrada = bundle2.getString("abiertascerrada");

        modelo.uidProducto = uidProducto;
        foto_destacado = (SquareImageView) findViewById(R.id.foto_destacado);
        btn_examinar = (Button) findViewById(R.id.btn_examinar);
        layout_info_des = (LinearLayout) findViewById(R.id.layout_info_des);
        txt_titulo = (TextView) findViewById(R.id.txt_titulo);
        txt_precio = (TextView) findViewById(R.id.txt_precio);
        texthead = (TextView) findViewById(R.id.texthead);
        btn_head = (Button) findViewById(R.id.btn_head);
        relative_check = (RelativeLayout) findViewById(R.id.relative_check);
        tex_body = (TextView) findViewById(R.id.tex_body);
        check_destacado = (CheckBox) findViewById(R.id.check_destacado);
        button11 = (Button) findViewById(R.id.button11);
        buttoneditar = (Button) findViewById(R.id.buttoneditar);

        progressDialog = new ProgressDialog(this);

        //inicio permiso camara
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

            }
        }


        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.CAMERA)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.CAMERA},
                        2);
            }
        }


        tex_body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modelo.vista = 1;
                Intent i = new Intent(getApplicationContext(), TerminosYCondiciones.class);
                i.putExtra("idProducto", uidProducto);
                i.putExtra("posicionServicio", posicionServicio);
                i.putExtra("abiertascerrada", abiertascerrada);
                startActivity(i);
                //finish();

            }
        });
//fin permiso camara

        btn_examinar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        carga();
    }


    @Override
    public void onBackPressed() {
        cancelar2();
        super.onBackPressed();
    }

    public void cancelar2() {

        finish();
    }

    public void cancelar(View v) {
        cancelar2();
    }

    public void aceptar(View v) {
        if (uriSavedImage1 != null) {

            progressDialog.setMessage("Validando la información, por favor espere...");
            progressDialog.show();

            final String nameFoto = "destacado.jpg";


            FirebaseStorage storage = FirebaseStorage.getInstance();
            //StorageReference storageReference = storage.getReferenceFromUrl("gs://mypetpruebas-6e090.appspot.com").child("productos/"+keyFirebase+"/"+nameFoto);
            final StorageReference storageReference = storage.getReference().child("productos/" + uidProducto + "/" + nameFoto);


            storageReference.putFile(uriSavedImage1)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            modelo.uidProducto = uidProducto;
                            modelo.name_destacado = nameFoto;
                            modelo.dActivo = true;
                            modelo.valodDestacado = Integer.parseInt(modelo.productosOferentesBoolean.get(0).getPrecio());
                            progressDialog.dismiss();

                            if (!modelo.productosOferentesBoolean.get(0).getDestacado()) {
                                Intent i = new Intent(getApplicationContext(), RegistroDatosPersonalesTpaga.class);
                                startActivity(i);
                            }

                            finish();



                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(InformacionPublicacion.this, "Error al cargar la imagen ", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "seleccione una imagen ", Toast.LENGTH_LONG).show();
        }

    }


    public void editar(View v){

        if (uriSavedImage1 != null) {

            progressDialog.setMessage("Validando la información, por favor espere...");
            progressDialog.show();

            final String nameFoto = "destacado.jpg";


            FirebaseStorage storage = FirebaseStorage.getInstance();
            //StorageReference storageReference = storage.getReferenceFromUrl("gs://mypetpruebas-6e090.appspot.com").child("productos/"+keyFirebase+"/"+nameFoto);
            final StorageReference storageReference = storage.getReference().child("productos/" + uidProducto + "/" + nameFoto);


            storageReference.putFile(uriSavedImage1)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(InformacionPublicacion.this, "Imagen actualizada con exito ", Toast.LENGTH_SHORT).show();
                            cancelar2();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(InformacionPublicacion.this, "Error al cargar la imagen ", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "seleccione una imagen ", Toast.LENGTH_LONG).show();
        }
    }


    public void pregunta(View v) {
        Intent i = new Intent(getApplicationContext(), CoashMarckDestacados.class);
        i.putExtra("idProducto", uidProducto);
        i.putExtra("posicionServicio", posicionServicio);
        i.putExtra("abiertascerrada", abiertascerrada);
        startActivity(i);

    }


    //camara
    private void selectImage() {

        final CharSequence[] items = {"Tomar foto", "Selecionar imagen", "Cancelar"};

        AlertDialog.Builder builder = new AlertDialog.Builder(InformacionPublicacion.this);
        builder.setTitle("Seleccionar foto");
        AlertDialog.Builder cancelar = builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                boolean result = Utility.checkPermission(InformacionPublicacion.this);
                if (items[item].equals("Tomar foto")) {
                    userChoosenTask = "Take Photo";
                    if (result) {
                        cameraIntent();
                    }


                } else if (items[item].equals("Selecionar imagen")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {
                        galleryIntent();
                    }

                } else if (items[item].equals("Cancelar")) {
                    dialog.dismiss();
                }
            }


        });
        builder.show();
    }


    //permiso camara v6
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    /// manejo de camara
    private void cameraIntent() {
        //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //startActivityForResult(intent, REQUEST_CAMERA);

        dispatchTakePictureIntent();
    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent, SELECT_FILE);

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix //
                ".jpg",         // suffix //
                storageDir      // directory //
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                ruta1 = photoFile.getAbsolutePath();
                photoURI = FileProvider.getUriForFile(this,
                        //"com.tactoapps.android.fileprovider",
                        getString(R.string.authorities),
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == REQUEST_TAKE_PHOTO ) {
                onCaptureImageResult();

            }
        }
    }



    private void onSelectFromGalleryResult(Intent data) {


        String url1 =  getRealPathFromURI(data.getData());
        setPic(foto_destacado,url1);

        uriSavedImage1 = data.getData();

        String newUrl = compressImage(uriSavedImage1);
        File f = new File(newUrl);
        uriSavedImage1 = Uri.fromFile(f);
        //
        btn_examinar.setText("Editar");
        layout_info_des.setVisibility(View.VISIBLE);
        txt_titulo.setText("" + modelo.productosOferentesBoolean.get(0).getTitulo());
        txt_precio.setText("" + Utility.convertToMoney(Integer.valueOf(modelo.productosOferentesBoolean.get(0).getPrecio())));
    }

    //camara
    private void onCaptureImageResult() {

        setPic(foto_destacado, ruta1);
        galleryAddPic(ruta1);
        String newUrl = compressImageFromCamera(mCurrentPhotoPath);
        File f = new File(newUrl);

        uriSavedImage1 = Uri.fromFile(f);
        btn_examinar.setText("Editar");
        layout_info_des.setVisibility(View.VISIBLE);
        txt_titulo.setText("" + modelo.productosOferentesBoolean.get(0).getTitulo());
        txt_precio.setText("" + Utility.convertToMoney(modelo.productosOferentesBoolean.get(0).getPrecio()));

    }



    public void carga() {
        if (modelo.datosOferente.getDatosTpaga().getIdClienteTpaga() != null && modelo.datosOferente.getTarjetas().getKeyId() != null && modelo.productosOferentesBoolean.get(0).getDestacado() == true) {
            //texthead.setVisibility(View.GONE);
            // btn_head.setVisibility(View.VISIBLE);
            button11.setVisibility(View.GONE);
            buttoneditar.setVisibility(View.VISIBLE);
            layout_info_des.setVisibility(View.VISIBLE);
            txt_titulo.setText("" + modelo.productosOferentesBoolean.get(0).getTitulo());
            txt_precio.setText("" + Utility.convertToMoney(Integer.valueOf(modelo.productosOferentesBoolean.get(0).getPrecio())));
            //relative_check.setVisibility(View.VISIBLE);
            btn_examinar.setText("Editar");
            //tex_body.setVisibility(View.GONE);

            //foto destadcado


            //descargar imagenes firebase

            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference().child("productos/" + modelo.productosOferentesBoolean.get(0).getId() + "/" + "destacado.jpg");

            //get download file url
            storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Log.i("Main", "File uri: " + uri.toString());
                }
            });


            //download the file
            try {
                final File localFile = File.createTempFile("images", "jpg");
                storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                        foto_destacado.setImageBitmap(bitmap);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        int resID2 = getResources().getIdentifier("destacado.jpg", "drawable", getPackageName()); // This will return an integer value stating the id for the image you want.

                        if (resID2 == 0) {
                            // Toast.makeText(mContext, "Error al cargar la imagen", Toast.LENGTH_SHORT).show();
                            foto_destacado.setBackgroundResource(R.drawable.btnfotomascota2);

                        } else {
                            foto_destacado.setBackgroundResource(resID2);
                        }


                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Main", "IOE Exception");
            }


            if (modelo.productosOferentesBoolean.get(0).getDestacado() == true) {
                check_destacado.setChecked(true);
            }
        }

    }



    public String compressImage(Uri imageUri) {

        String filePath = getRealPathFromURI(imageUri);

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }


    public String compressImageFromCamera(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }


    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }


    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }




    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    private void setPic(SquareImageView camara, String ruta) {
        // Get the dimensions of the View
        int targetW = camara.getWidth();
        int targetH = camara.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(ruta, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(ruta, bmOptions);
        camara.setScaleType(ScaleType.CENTER_CROP);

        camara.setImageBitmap(bitmap);

    }




    private void galleryAddPic(String ruta) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(ruta);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }


}
