package woopets.com.woopets.Oferente.tpaga.Model;

/**
 * Created by tacto on 10/08/17.
 */

public class TPPAgo {

    String id;
    String description;
    int iacAmount =0;//porcentaje del ,8
    String creditCard;
    int amount =0; // valor del pago
    int taxAmount =0;
    int installments =1;//numero de cuotas
    String orderId;
    String currency ="COP";
 



    public static  TPPAgo create(String description,int iacAmount, String creditCard, int amount, int taxAmount, int installments, String orderId, String currency){

        TPPAgo tppAgo = new TPPAgo();
        tppAgo.description =description;
        tppAgo.iacAmount = iacAmount;
        tppAgo.creditCard =creditCard;
        tppAgo.amount =amount;
        tppAgo.taxAmount =taxAmount;
        tppAgo.installments =installments;
        tppAgo.orderId =orderId;
        tppAgo.currency =currency;

        return tppAgo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public int getIacAmount() {
        return iacAmount;
    }

    public void setIacAmount(int iacAmount) {
        this.iacAmount = iacAmount;
    }


    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(int taxAmount) {
        this.taxAmount = taxAmount;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }





}
