package woopets.com.woopets.Oferente.Comandos;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Oferente.ModelOferente.ClassPreguntas_A_MisPublicaciones;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 18/12/17.
 */

public class ComandoRespuestaPreguntaPublicaciones {

    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    public interface OnComandoRespuestaPreguntaPublicacionesChangeListener {

        void cargoRespuesta();
    }

    //interface del listener de la actividad interesada
    private ComandoRespuestaPreguntaPublicaciones.OnComandoRespuestaPreguntaPublicacionesChangeListener mListener;

    public ComandoRespuestaPreguntaPublicaciones(ComandoRespuestaPreguntaPublicaciones.OnComandoRespuestaPreguntaPublicacionesChangeListener mListener) {

        this.mListener = mListener;
    }



    public void setRespuestaNotificocaiones(final String idPregunta,final String respuesta){


        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("h:mm a");
        System.out.println("Hora: "+hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: "+dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println("Hora y fecha: "+hourdateFormat.format(date));




        if(!modelo.uid.equals(" ")){

            String string = ""+hourFormat.format(date);
            String[] parts = string.split(" ");
            String part1 = parts[0];
            String part2 = parts[1];

            if(part2.equals("a.") || part2.equals("a.m.")  || part2.equals("a. m.") || part2.equals("A.")){
                part2 = "AM";
            }
            if(part2.equals("p.") || part2.equals("p.m.") || part2.equals("p. m.") || part2.equals("P.")){
                part2 = "PM";
            }


        final DatabaseReference ref = database.getReference("preguntas/"+idPregunta+"/fechaRespuesta/" );//ruta path
        ref.setValue(""+dateFormat.format(date)+" "+part1+" "+part2);

        final DatabaseReference ref2 = database.getReference("preguntas/"+idPregunta+"/respuesta/" );//ruta path
        ref2.setValue(""+respuesta);


            ClassPreguntas_A_MisPublicaciones pregunta  = modelo.getPreguntaById(idPregunta);
            pregunta.setRespuesta(respuesta);
            pregunta.setFechaRespuesta(dateFormat.format(date)+" "+part1+" "+part2);
        //modelo.classPreguntas_a_misPublicaciones.get(modelo.posicionRespuesta).setRespuesta(""+respuesta);
        //modelo.classPreguntas_a_misPublicaciones.get(modelo.posicionRespuesta).setFechaRespuesta(dateFormat.format(date)+" "+part1+" "+part2);



        }

        mListener.cargoRespuesta();

    }



    public  void respuestaNotificacion(final String idPregunta,final String mensaje){


        final DatabaseReference ref = database.getReference("mensajes/"+referencia.push().getKey()+"/");

        if(!modelo.uid.equals("")){

            ClassPreguntas_A_MisPublicaciones pregunta  = modelo.getPreguntaById(idPregunta);
            Map<String, Object> dotosNotificacion = new HashMap<String, Object>();
            dotosNotificacion.put("idCliente",""+pregunta.getListaClienete().get(0).getId());
            dotosNotificacion.put("idPublicacion", ""+pregunta.getIdPublicacion());
            dotosNotificacion.put("infoAdicional", ""+pregunta.getListaProductosOferentes().get(0).getTitulo());
            dotosNotificacion.put("mensaje", mensaje);
            dotosNotificacion.put("timestamp", ServerValue.TIMESTAMP);
            dotosNotificacion.put("tipo", "pregunta-respuesta");
            dotosNotificacion.put("titulo", "Pregunta respondida");


            String token = pregunta.getListaClienete().get(0).getTokens();
            if(token == null ||  token.equals("")) {
                token = "1";
            }

            Map<String,Object> tokens = new HashMap<String,Object>();
            tokens.put(token, true);
            dotosNotificacion.put("tokens",tokens);
            dotosNotificacion.put("visto", false);

            ref.updateChildren(dotosNotificacion);

        }

    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnComandoRespuestaPreguntaPublicacionesChangeListener sDummyCallbacks = new OnComandoRespuestaPreguntaPublicacionesChangeListener() {
        @Override
        public void cargoRespuesta() {
        }


    };

}
