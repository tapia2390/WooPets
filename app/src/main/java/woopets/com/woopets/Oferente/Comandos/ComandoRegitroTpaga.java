package woopets.com.woopets.Oferente.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import woopets.com.woopets.Modelo.cliente.MiniTarjeta;
import woopets.com.woopets.Oferente.ModelOferente.Modelo;

/**
 * Created by tacto on 4/12/17.
 */

public class ComandoRegitroTpaga {

    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnRegitroTpagaChangeListener {


        void registroCustomer();
        void registroCredidCard();
        void registroDatosTp();
    }


    //interface del listener de la actividad interesada
    private ComandoRegitroTpaga.OnRegitroTpagaChangeListener mListener;

    public ComandoRegitroTpaga(ComandoRegitroTpaga.OnRegitroTpagaChangeListener mListener){

        this.mListener = mListener;

    }


    public void setRegistroClienteTpaga(final String apellidos,final String correo,final String token, final String nombres, final String telefono ){
        final DatabaseReference ref = database.getReference("oferentes/"+modelo.uid+"/datosTpaga/");
        Map<String, Object> enviarDatoTpaga = new HashMap<String, Object>();
        enviarDatoTpaga.put("apellidos",apellidos);
        enviarDatoTpaga.put("correo", correo);
        enviarDatoTpaga.put("idClienteTpaga", ""+token);
        enviarDatoTpaga.put("nombres", ""+nombres);
        enviarDatoTpaga.put("telefono", ""+telefono);

        ref.updateChildren(enviarDatoTpaga);
        modelo.idClienteTpaga = token;
        mListener.registroDatosTp();
    }


    public void setRegistroCredidCard(MiniTarjeta mini){

        DatabaseReference key = referencia.push();
        final DatabaseReference ref = database.getReference("oferentes/"+modelo.uid+"/tarjetas/"+key.getKey()+"/" );

        Map<String, Object> enviarCredidCard = new HashMap<String, Object>();
        enviarCredidCard.put("activo",true);
        enviarCredidCard.put("cuotas", mini.cuotas);
        enviarCredidCard.put("franquicia", ""+mini.franquicia);
        enviarCredidCard.put("lastFour", ""+mini.lastFour);
        enviarCredidCard.put("token", ""+mini.token);
        enviarCredidCard.put("direccion", ""+mini.direccion);
        enviarCredidCard.put("ciudad", ""+mini.ciudad);
        enviarCredidCard.put("departamento", ""+mini.departamento);
        enviarCredidCard.put("correo", ""+mini.correo);
        enviarCredidCard.put("telefono", ""+mini.telefono);
        enviarCredidCard.put("cedula", ""+mini.cedula);
        enviarCredidCard.put("nombre", ""+mini.nombre);


        ref.updateChildren(enviarCredidCard);
        mListener.registroCredidCard();
    }

    public void updateEstado(){
        final DatabaseReference ref = database.getReference("productos/"+modelo.pro_uidProducto+"/destacado/" );
        ref.setValue(true);

    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static ComandoRegitroTpaga.OnRegitroTpagaChangeListener sDummyCallbacks = new ComandoRegitroTpaga.OnRegitroTpagaChangeListener()
    {


        public void registroCustomer()
        {}

        public void registroCredidCard()
        {}

        public void registroDatosTp(){}



    };
}
